var d = new Date();
var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'Nopember', 'Desember'];
var arr_month = [];
for (i = 0; i <= d.getMonth(); i++) arr_month.push(months[i]);
$(".bs-select").selectpicker({
    iconBase: "fa",
    tickIcon: "fa-check"
});
$("#widget_select").change(function() {
    data = ["kary_cabang", "kary_baru", "keterlambatan", "ulangtahun", "absen", "jumlah_karyawan"];
    value = $(this).val();
    for (j = 0; j < data.length; j++) $("#" + data[j]).addClass("hidden");
    if (value != null) {
        for (i = 0; i < value.length; i++) {
            a = value[i];
            $("#" + data[a]).removeClass("hidden");
            var dat_categories = arr_month;

            if (a == 3) {
                $("#calendar").width() <= 400 ? ($("#calendar").addClass("mobile"), l = {
                    left: "title, prev, next",
                    center: "",
                    right: "today,month,agendaWeek,agendaDay"
                }) : ($("#calendar").removeClass("mobile"), l = App.isRTL() ? {
                    right: "title",
                    center: "",
                    left: "prev,next,today,month,agendaWeek,agendaDay"
                } : {
                    left: "title",
                    center: "",
                    right: "prev,next,today,month,agendaWeek,agendaDay"
                }), $("#calendar").fullCalendar("destroy"), $("#calendar").fullCalendar({
                    disableDragging: !1,
                    header: l,
                    editable: !0,
                    eventSources: [{
                        url: base_url+"main/birthday",
                        backgroundColor: App.getBrandColor("blue"),
                        allDay: !1
                    }]
                });
            } else if (a == 4) {
                AmCharts.makeChart("absen_scanlog", {
                    "type": "serial",
                    "dataLoader": {
                        "url": base_url+"main/json_absen"
                    },
                    "valueAxes": [{
                        "gridColor": "#FFFFFF",
                        "gridAlpha": 0.2,
                        "dashLength": 0
                    }],
                    "gridAboveGraphs": true,
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "jumlah"
                    }],
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "cabang",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridAlpha": 0,
                        "tickPosition": "start",
                        "tickLength": 20,
                        "labelRotation": 30
                    }
                });
            } else if (a == 0) {
                AmCharts.makeChart("dashboard_amchart_4", {
                    type: "pie",
                    theme: "light",
                    dataLoader: {
                        url: base_url+"main/json_karyawan_cabang"
                    },
                    valueField: "jml",
                    titleField: "cabang",
                    outlineAlpha: .4,
                    depth3D: 15,
                    balloonText: "[[title]]<br><span style=\"font-size:14px\"><b>[[value]]</b> ([[percents]]%)</span>",
                    angle: 30,
                    "export": {
                        enabled: !0
                    }
                });
            } else if (a == 2) {
                AmCharts.makeChart("chartdiv", {
                    "theme": "light",
                    "type": "serial",
                    "startDuration": 2,
                    "dataLoader": {
                        "url": base_url+"main/json_terlambat"
                    },
                    "valueAxes": [{
                        "position": "left",
                        "title": "Jumlah"
                    }],
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "fillColorsField": "color",
                        "fillAlphas": 1,
                        "lineAlpha": 0.1,
                        "type": "column",
                        "valueField": "visits"
                    }],
                    "depth3D": 20,
                    "angle": 30,
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "country",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "labelRotation": 30
                    },
                    "export": {
                        "enabled": true
                    }
                });
            } else if (a == 1) {
                AmCharts.makeChart("site_statistics", {
                    "theme": "light",
                    "type": "serial",
                    "startDuration": 2,
                    "dataLoader": {
                        "url": base_url+"main/json_karyawan_baru"
                    },
                    "valueAxes": [{
                        "position": "left",
                        "title": "Jumlah"
                    }],
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "bullet": "round",
                        "bulletSize": 8,
                        "lineColor": "#d1655d",
                        "lineThickness": 2,
                        "negativeLineColor": "#637bb6",
                        "type": "smoothedLine",
                        "valueField": "jumlah"
                    }],
                    "chartCursor": {
                        "cursorAlpha": 0,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "valueLineAlpha": 0.5,
                        "fullWidth": true
                    },
                    "categoryField": "periode",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "labelRotation": 30
                    },
                    "export": {
                        "enabled": true
                    }
                });
            } else if (a == 5) {
                var options = {
                    chart: {
                        renderTo: "chart_karyawan",
                        type: "column"
                    },
                    title: {
                        text: "Jumlah Karyawan"
                    },
                    subtitle: {
                        text: "Periode "+d.getFullYear()
                    },
                    xAxis: {
                        categories: dat_categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: "Jumlah (orang)"
                        }
                    },
                    tooltip: {
                        headerFormat: "<span style=\"font-size:10px\">{point.key}</span><table>",
                        pointFormat: "<tr><td style=\"color:{series.color};padding:0\">{series.name}: </td>" +
                            "<td style=\"padding:0\"><b>{point.y:.0f} </b></td></tr>",
                        footerFormat: "</table>",
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{}]
                };

                $.getJSON(base_url+"chart/jumlah_karyawan", function(data) {
                    options.series = data;
                    var chart = new Highcharts.Chart(options);
                });
            }
        }
    }
});