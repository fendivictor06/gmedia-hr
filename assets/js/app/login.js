$("#formlogin").submit(function(event){
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url : base_url+"main/login",
        data : formData,
        type : "post",
        dataType : "json",
        async : false,
        cache : false,
        contentType : false,
        processData : false,
        success : function (data) {
            $("#notification").html("");
            if(data.status == true) {
                $("#notification").html("<div class=\"alert alert-success\" id=\"success\"> <button class=\"close\" data-close=\"success\"></button> "+data.message+" </div>");
                setTimeout(function(){ location.reload(); }, 2000);
            } else {
                $("#notification").html("<div class=\"alert alert-danger\" id=\"danger\"> <button class=\"close\" data-close=\"alert\"></button> "+data.message+" </div>");
            }
        },
        error : function(jqXHR, textStatus, errorThrown) {
            $("#notification").html("<div class=\"alert alert-danger\" id=\"danger\"> <button class=\"close\" data-close=\"alert\"></button> Username atau Password anda Salah ! </div>");
        }
    })
});