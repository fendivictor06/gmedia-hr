$(document).ready(function(){
	load_table();
});

function load_table() {
	$.ajax({
		url : base_url+"macaddress/view_macaddress",
		success : function(data) {
			$("#myTable").html(data);
			$("#dataTables-example").DataTable({
				responsive : true
			});
		},
		error : function() {
			bootbox.alert('Gagal Mengambil data');
		}
	});
}

$("#form_data").submit(function(event){
	event.preventDefault();
	formData = new FormData($(this)[0]);
	$.ajax({
		url : base_url+"macaddress/process_mac",
		type : "post",
        data : formData,
        async : false,
        cache : false,
        dataType : "json",
        contentType : false,
        processData : false,
        success : function(data) {
            if(data.status == true) {
                load_table(), clearform();
                $("#myModal").modal('toggle');
            }
            bootbox.alert(data.message);
        },
        error : function() {
        	bootbox.alert('Gagal menyimpan data');
        }
	});

	return false;
});

function get_id(id) {
	$.ajax({
		url : base_url+"macaddress/mac_id/"+id,
		dataType : "json",
		success : function(data) {
			$("#id").val(data.id);
			$("#ssid").val(data.ssid);
			$("#mac").val(data.macaddress);
			$("#ip").val(data.ippublic);
			$("#myModal").modal();
		},
		error : function(){
			bootbox.alert('Gagal mengambil data');
		}
	});
}

function delete_data(id) {
	bootbox.dialog({
        message : "Yakin ingin menghapus data?",
        title : "Hapus Data",
        buttons :{
            danger : {
                label : "Delete",
                className : "red",
                callback : function(){
                    $.ajax({
                        url : base_url+"macaddress/mac_delete/"+id,
                        success : function(data){
                            bootbox.alert({
                                message: "Delete Success",
                                size: "small"
                            });
                            load_table();
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal menghapus data");
                        }
                    });
                }
            },
            main : {
                label : "Cancel",
                className : "blue",
                callback : function(){
                    return true;
                }
            }
        }
    });
}

function clearform() {
	document.getElementById('form_data').reset();
	$("#id").val();
}

$("#add").click(function(){
	$("#myModal").modal();
	clearform();
});