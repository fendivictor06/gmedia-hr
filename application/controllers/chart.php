<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chart extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
	}

	function jumlah_karyawan()
	{
		$this->Main_Model->get_login();
		$arr_jenis = array('Karyawan Baru', 'Karyawan Resign', 'Total Karyawan');

		$result = array();
		for ($i = 0; $i < count($arr_jenis); $i++) {
			$result[$i]['name'] = $arr_jenis[$i];
			$q = $this->Main_Model->period_this_year();
			foreach ($q as $row => $val) {
				$tgl_awal = $val->tgl_awal;
				$tgl_akhir = $val->tgl_akhir;
				if ($i == 0) {
					$w = $this->Main_Model->jml_kary_baru($tgl_awal, $tgl_akhir);
					$jml[$row] = isset($w->jml) ? $w->jml : 0;
				} else if ($i == 1) {
					$w = $this->Main_Model->resign_baru($tgl_awal, $tgl_akhir);
					$jml[$row] = isset($w->jml) ? $w->jml : 0;
				} else {
					$w = $this->Main_Model->jumlah_karyawan($tgl_awal, $tgl_akhir);
					$jml[$row] = isset($w->jml) ? $w->jml : 0;
				}
				$detail[$i][] = (int)$jml[$row];
			}
			$result[$i]['data'] = array_values($detail[$i]);	
		}
		echo json_encode($result);
	}
}

/* End of file chart.php */
/* Location: ./application/controllers/chart.php */