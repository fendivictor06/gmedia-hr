<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Reminder extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Penggajian_Model', '', true);
        $this->load->model('Reminder_Model', '', true);
        $this->load->model('Rest_Model', '', true);
    }

    function index()
    {
        show_404();
    }

    function approval_cuti()
    {
        $q = $this->Reminder_Model->view_reminder();
        foreach ($q as $row) {
            $today = date('d F Y');
            $subject = 'Approval Cuti '.$today;

            $username = isset($row->user) ? $row->user : '';

            $cab = $this->Reminder_Model->id_cabang($username);
            $app = $this->Reminder_Model->approval($username);
            $div = $this->Reminder_Model->divisi($username);

            $data = $this->Absensi_Model->reminder_approval_cuti($cab, $app, $div);

            if (!empty($data)) {
                $message = 'Berikut adalah Daftar Cuti yang butuh diproses';
                $table = '<table border="1"> 
							<tr><th>No</th><th>NIP</th><th>Nama</th><th>Jabatan</th><th>Cabang</th><th>Alasan Cuti</th><th>Alamat Cuti</th><th>No HP</th><th>Tgl Cuti</th></tr>';
                $no = 1;
                $nama = '';
                foreach ($data as $row) {
                    $table .= '<tr><td>'.$no.'</td><td>'.$row->nip.'</td><td>'.$row->nama.'</td><td>'.$row->jab.'</td><td>'.$row->cabang.'</td><td>'.$row->alasan_cuti.'</td><td>'.$row->alamat_cuti.'</td><td>'.$row->no_hp.'</td><td>';

                    $tgl_cuti = $this->Main_Model->view_by_id('cuti_sub_det', array('id_cuti_det' => $row->id_cuti_det), 'result');

                    $nama .= $no++.'. '.$row->nama.' ';
                    foreach ($tgl_cuti as $r) {
                        $table .= $this->Main_Model->format_tgl($r->tgl).'<br>';
                    }
                    $table .= '</td></tr>';
                }
                $table .= '</table>';
                $message .= '<br>'.$table;
                // print_r($message);

                $message_sms = '(HRD) Ada Cuti yg butuh diproses ';
                $link_sms = base_url();
                

                $contact = $this->Main_Model->reminder_contact($username);
                $email = isset($contact['email']) ? $contact['email'] : '';
                $hp = isset($contact['hp']) ? $contact['hp'] : '';
                $user = isset($contact['user']) ? $contact['user'] : '';

                if ($user != '') {
                    $randomstring = $this->Main_Model->generateRandomString(8);
                    // $md5 = md5($randomstring);
                    $url = base_url('rmd').'/'.$randomstring;
                    $link_sms = $url;
                    $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

                    $message .= '<br>'.$link;

                    $usr_id = $this->Main_Model->view_by_id('users', array('usr_name' => $user), 'row');
                    $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                    $now = date('Y-m-d');
                    $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                    $authenticantion = array(
                        'usr_id' => $id_user,
                        'token' => $randomstring,
                        'expiration' => $expiration
                        );

                    $this->Main_Model->process_data('tb_authentication', $authenticantion);
                }
                $message_sms .= 'silahkan verifikasi di '.$link_sms;

                if ($email != '') {
                    $this->Main_Model->send_mail($email, $message, $subject);
                }
                if ($hp != '') {
                    // $this->Main_Model->send_sms($message_sms, $hp);
                }
            }
        }
    }

    function approval_cuti_khusus()
    {
        $q = $this->Reminder_Model->view_reminder();
        foreach ($q as $row) {
            $today = date('d F Y');
            $subject = 'Approval Cuti '.$today;

            $username = isset($row->user) ? $row->user : '';

            $cab = $this->Reminder_Model->id_cabang($username);
            $app = $this->Reminder_Model->approval($username);
            $div = $this->Reminder_Model->divisi($username);

            $data = $this->Absensi_Model->reminder_cuti_khusus($cab, $app, $div);

            if (!empty($data)) {
                $message = 'Berikut adalah Daftar Cuti Khusus yang butuh diproses';
                $table = '<table border="1"> 
							<tr><th>No</th><th>NIP</th><th>Nama</th><th>Jabatan</th><th>Cabang</th><th>Alasan Cuti</th><th>Tgl Cuti</th></tr>';
                $no = 1;
                $nama = '';
                foreach ($data as $row) {
                    $tgl_awal = $this->Main_Model->format_tgl($row->tgl_awal);
                    $tgl_akhir = $this->Main_Model->format_tgl($row->tgl_akhir);

                    $table .= '<tr><td>'.$no.'</td><td>'.$row->nip.'</td><td>'.$row->nama.'</td><td>'.$row->jab.'</td><td>'.$row->cabang.'</td><td>'.$row->tipe.'</td><td>'.$tgl_awal.' s/d '.$tgl_akhir.'</td></tr>';
                }
                $table .= '</table>';
                $message .= '<br>'.$table;
                // print_r($message);

                $message_sms = '(HRD) Ada Cuti Khusus yg butuh diproses ';
                $link_sms = base_url();
                

                $contact = $this->Main_Model->reminder_contact($username);
                $email = isset($contact['email']) ? $contact['email'] : '';
                $hp = isset($contact['hp']) ? $contact['hp'] : '';
                $user = isset($contact['user']) ? $contact['user'] : '';

                if ($user != '') {
                    $randomstring = $this->Main_Model->generateRandomString(8);
                    // $md5 = md5($randomstring);
                    $url = base_url('rmd').'/'.$randomstring;
                    $link_sms = $url;
                    $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

                    $message .= '<br>'.$link;

                    $usr_id = $this->Main_Model->view_by_id('users', array('usr_name' => $user), 'row');
                    $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                    $now = date('Y-m-d');
                    $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                    $authenticantion = array(
                        'usr_id' => $id_user,
                        'token' => $randomstring,
                        'expiration' => $expiration
                        );

                    $this->Main_Model->process_data('tb_authentication', $authenticantion);
                }
                $message_sms .= 'silahkan verifikasi di '.$link_sms;

                if ($email != '') {
                    $this->Main_Model->send_mail($email, $message, $subject);
                }
                if ($hp != '') {
                    // $this->Main_Model->send_sms($message_sms, $hp);
                }
            }
        }
    }

    function approval_klarifikasi()
    {
        $q = $this->Reminder_Model->view_reminder();
        $from = 'hrd.smg@gmedia.co.id';
        foreach ($q as $row) {
            $today = date('Y-m-d', strtotime(date('Y-m-d').'-1 days'));
            $subject = '[Aplikasi HRD] Klarifikasi Absensi '.$this->Main_Model->tgl_indo($today);
            $username = isset($row->user) ? $row->user : '';
            $nip = isset($row->nip) ? $row->nip : '';
            $fcm = isset($row->fcm_id) ? $row->fcm_id : '';

            $data = $this->Absensi_Model->reminder_klarifikasi_absensi($nip);
            if (!empty($data)) {
                $message = 'Dear, '.$row->nama.'<br>';
                $message .= 'Berikut adalah Daftar Absensi yang butuh diklarifikasi (Absensi tanggal '.$this->Main_Model->tgl_indo($today).')';
                $table = '<table border="1">
							<tr>
								<th width="5%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">#</th>
								<th width="20%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">NIK</th>
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Nama</th> 
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Tanggal</th>
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Keterangan</th>
							</tr>';
                $no = 1;
                foreach ($data as $row) {
                    $table .= '<tr>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$no++.'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$row->nip.'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$row->nama.'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$this->Main_Model->tgl_indo($row->tgl).'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$row->ket.'</td>
								</tr>';
                }

                $table .= '</table>';
                $message .= '<br>'.$table;

                $contact = $this->Main_Model->reminder_contact($username);
                $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
                $user = isset($contact['user']) ? $contact['user'] : '';

                if ($user != '') {
                    $randomstring = $this->Main_Model->generateRandomString(8);
                    $url = base_url('rmd').'/'.$randomstring;
                    $link = 'Silahkan <a href="'.$url.'">klik disini</a>';
                    $message .= '<br>'.$link;

                    $usr_id = $this->Main_Model->view_by_id('users', array('usr_name' => $user), 'row');
                    $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                    $now = date('Y-m-d');
                    $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                    $authenticantion = array(
                        'usr_id' => $id_user,
                        'token' => $randomstring,
                        'expiration' => $expiration
                    );

                    $this->Main_Model->process_data('tb_authentication', $authenticantion);
                }

                if ($email != '') {
                    $return = $this->Main_Model->send_mail($email, $message, $subject, $from);
                    // print_r($return); exit;
                    $title = 'Klarifikasi Absensi';
                    $text = 'Ada absensi yg harus diklarifikasi.';
                    $this->Rest_Model->notif_fcm($title, $text, $fcm, $id_user);
                }
            }
        }
    }

    function approval_terlambat()
    {
        $q = $this->Reminder_Model->view_reminder();
        $from = 'hrd.smg@gmedia.co.id';
        foreach ($q as $row) {
            $today = date('Y-m-d', strtotime(date('Y-m-d').'-1 days'));
            $subject = '[Aplikasi HRD] Klarifikasi Terlambat '.$this->Main_Model->tgl_indo($today);
            $username = isset($row->user) ? $row->user : '';
            $nip = isset($row->nip) ? $row->nip : '';
            $fcm = isset($row->fcm_id) ? $row->fcm_id : '';

            $data = $this->Absensi_Model->reminder_klarifikasi_terlambat($nip);
            if (!empty($data)) {
                $message = 'Dear, '.$row->nama.'<br>';
                $message .= 'Berikut adalah Daftar Terlambat yang butuh diklarifikasi (Terlambat tanggal '.$this->Main_Model->tgl_indo($today).')';
                $table = '<table border="1">
							<tr>
								<th width="5%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">#</th>
								<th width="20%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">NIK</th>
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Nama</th> 
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Tanggal</th>
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Jadwal Masuk</th>
								<th width="25%" style="border: 1px solid #ddd; padding: 3px; padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #265180; color: white;">Jam Masuk</th>
							</tr>';
                $no = 1;
                foreach ($data as $row) {
                    $table .= '<tr>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$no++.'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$row->nip.'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$row->nama.'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.$this->Main_Model->tgl_indo($row->tgl).'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.jam($row->jam_masuk).'</td>
									<td style="border: 1px solid #ddd; padding: 3px;">'.jam($row->scan_masuk).'</td>
								</tr>';
                }

                $table .= '</table>';
                $message .= '<br>'.$table;

                $contact = $this->Main_Model->reminder_contact($username);
                $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
                $user = isset($contact['user']) ? $contact['user'] : '';

                if ($user != '') {
                    $randomstring = $this->Main_Model->generateRandomString(8);
                    $url = base_url('rmd').'/'.$randomstring;
                    $link = 'Silahkan <a href="'.$url.'">klik disini</a>';
                    $message .= '<br>'.$link;

                    $usr_id = $this->Main_Model->view_by_id('users', array('usr_name' => $user), 'row');
                    $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                    $now = date('Y-m-d');
                    $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                    $authenticantion = array(
                        'usr_id' => $id_user,
                        'token' => $randomstring,
                        'expiration' => $expiration
                    );

                    $this->Main_Model->process_data('tb_authentication', $authenticantion);
                }

                if ($email != '') {
                    $return = $this->Main_Model->send_mail($email, $message, $subject, $from);
                    // print_r($return); exit;
                    $title = 'Klarifikasi Terlambat';
                    $text = 'Ada terlambat yg harus diklarifikasi.';
                    $this->Rest_Model->notif_fcm($title, $text, $fcm, $id_user);
                }
            }
        }
    }

    function approval_lembur()
    {
        $q = $this->Reminder_Model->view_reminder();
        foreach ($q as $row) {
            $today = date('d F Y', strtotime(date('Y-m-d').'-1 days'));
            $subject = 'Approval Klarifikasi Absensi '.$today;

            $username = isset($row->user) ? $row->user : '';

            $cab = $this->Reminder_Model->id_cabang($username);
            $app = $this->Reminder_Model->approval($username);
            $div = $this->Reminder_Model->divisi($username);

            $data = $this->Karyawan_Model->reminder_approval_lembur($cab, $app, $div);
            if (!empty($data)) {
                $message = 'Berikut adalah Daftar Lembur yang butuh diverifikasi';
                $table = '<table border="1"> 
							<tr><th>No</th><th>NIP</th><th>Nama</th><th>Tanggal</th><th>Jam</th><th>Scan Pulang</th></tr>';
                $no = 1;
                $nama = '';
                foreach ($data as $row) {
                    $tanggal = isset($row->tgl) ? $this->Main_Model->format_tgl($row->tgl) : '';
                    $jam = $row->starttime.' - '.$row->endtime;
                    $table .= '<tr><td>'.$no.'</td><td>'.$row->nip.'</td><td>'.$row->nama.'</td><td>'.$tanggal.'</td><td>'.$jam.'</td><td>'.$row->scan_pulang.'</td></tr>';

                    $nama .= $no++.'. '.$row->nama.' ';
                // $id_cab = $r->id_cab;
                }
                $table .= '</table>';

                $message .= '<br>'.$table;
                // print_r($message);

                $message_sms = '(HRD) Ada Lembur yang butuh diverifikasi ';
                $link_sms = base_url();
                

                $contact = $this->Main_Model->reminder_contact($username);
                $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
                $hp = isset($contact['hp']) ? $contact['hp'] : '08156647203';
                $user = isset($contact['user']) ? $contact['user'] : '';

                if ($user != '') {
                    $randomstring = $this->Main_Model->generateRandomString(8);
                    // $md5 = md5($randomstring);
                    $url = base_url('rmd').'/'.$randomstring;
                    $link_sms = $url;
                    $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

                    $message .= '<br>'.$link;

                    $usr_id = $this->Main_Model->view_by_id('users', array('usr_name' => $user), 'row');
                    $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                    $now = date('Y-m-d');
                    $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                    $authenticantion = array(
                        'usr_id' => $id_user,
                        'token' => $randomstring,
                        'expiration' => $expiration
                        );

                    $this->Main_Model->process_data('tb_authentication', $authenticantion);
                }
                $message_sms .= 'silahkan verifikasi di '.$link_sms;

                
                if ($email != '') {
                    $this->Main_Model->send_mail($email, $message, $subject);
                }
                if ($hp != '') {
                    // $this->Main_Model->send_sms($message_sms, $hp);
                }
            }
        }
    }

    function setting()
    {
        $this->Main_Model->get_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_datepicker()
              .$this->Main_Model->js_bs_select();

        $header = array(
            'menu' => $this->Main_Model->menu_admin('0', '0', '3'),
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
                      .$this->Main_Model->style_datepicker()
                      .$this->Main_Model->style_bs_select()
            );

        $nip = $this->Main_Model->kary_active();
        $nip_opt = array();
        if ($nip) {
            foreach ($nip as $row) {
                $posisi = $this->Main_Model->posisi($row->nip);
                $jab = isset($posisi->jab) ? $posisi->jab : '';
                $nip_opt[$row->nip] = $row->nip.' - '.$row->nama.' - '.$jab;
            }
        }

        // $user = $this->Main_Model->view_by_id('users', array('approval' => 1), 'result');
        $user = $this->Main_Model->view_by_id('users', array(), 'result');
        $user_opt = array();
        if ($user) {
            foreach ($user as $row) {
                $user_opt[$row->usr_name] = $row->usr_name.' - '.$row->nama;
            }
        }

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page(),
            'nip' => $nip_opt,
            'user' => $user_opt
            );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/setting_reminder', $data);
    }

    function view_setting()
    {
        $this->Main_Model->get_login();
        $data = $this->Absensi_Model->view_setting_reminder()->result();

        $template = $this->Main_Model->tbl_temp('tb_setting');
        $this->table->set_heading('No', 'NIP', 'Nama', 'Email', 'HP', 'Cabang', 'User', 'Action');
        $no = 1;
        foreach ($data as $row) {
            // ($row->default == 1) ? $is_default = 'Ya' : $is_default = 'Tidak';
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->mail,
                $row->telp,
                $row->cabang,
                $row->user,
                // $is_default,
                '<div class="btn-group">
	                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                    <i class="fa fa-angle-down"></i>
	                </button>
	                    <ul class="dropdown-menu" role="menu">
	                        <li>
	                            <a href="javascript:;" onclick="get_id(' . $row->id . ');">
	                                <i class="icon-edit"></i> Update </a>
	                        </li>
	                        <li>
	                            <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
	                                <i class="icon-edit"></i> Delete </a>
	                        </li>
	                    </ul>
	            </div>'
            );
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function id_setting($id = '')
    {
        $this->Main_Model->get_login();
        $data = $this->Absensi_Model->view_setting_reminder($id)->row();
        echo json_encode($data);
    }

    function setting_process()
    {
        $this->Main_Model->get_login();
        $nip = $this->input->post('nip');
        $user = $this->input->post('user');
        $id = $this->input->post('id');
        if ($nip == '' || $user == '') {
            $result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
        } else {
            $q = $this->Main_Model->view_by_id('kary', array('nip' => $nip), 'row');
            $email = isset($q->mail) ? $q->mail : '';
            $telp = isset($q->telp) ? $q->telp : '';
            $data = array(
                'nip' => $nip,
                'email' => $email,
                'telp' => $telp,
                'user' => $user
                );

            $condition = ($id != '') ? array('id' => $id) : '';

            $this->Main_Model->process_data('ms_reminder', $data, $condition);

            $result = array('status' => true, 'message' => 'Success!');
        }
        echo json_encode($result);
    }

    function delete_reminder($id = '')
    {
        $this->Main_Model->get_login();
        $this->Main_Model->delete_data('ms_reminder', array('id' => $id));

        echo json_encode(array('status' => true, 'message' => 'Success!'));
    }

    function reminder_absensi()
    {
        $q = $this->db->query("
			SELECT d.`nip`, d.`nama`, d.`telp`, d.`gend`,
			CASE WHEN d.gend = 'L' THEN 'Bpk' ELSE 'Ibu' END AS head,
			IFNULL(scan.scan_date, '') AS scan_date
			FROM sto_rel a
			INNER JOIN pos_sto b ON a.`id_sto` = b.`id_sto`
			INNER JOIN sk c ON c.`id_pos_sto` = b.`id_sto`
			INNER JOIN kary d ON d.`nip` = c.`nip`
			LEFT JOIN (
				SELECT * FROM tb_scanlog 
				WHERE DATE(scan_date) = CURDATE()
				GROUP BY pin
			) AS scan ON scan.pin = d.pin")->result();

        $tgl = date('Y-m-d');
        $check_holiday = $this->db->where('hol_tgl', $tgl)->get('hol')->row();
        $message = '';
        if (!empty($q)) {
            foreach ($q as $row) {
                $telp = $row->telp;
                $scan_date = $row->scan_date;
                if ($telp != '' && $scan_date == '' && empty($check_holiday)) {
                    $message = 'Pagi '.$row->head.' '.$row->nama.', Sprtnya anda blm mlkukan absn';
                    $w = date('w');
                    if ($w != 0) {
                        // $this->Main_Model->send_sms($message, $telp);
                    }
                }
            }
        }
        
        echo json_encode(array('message' => 'Complete'));
    }

    function reminder_kontrak()
    {
        $data = $this->Reminder_Model->reminder_kontrak_habis();

        if (! empty($data)) {
            foreach ($data as $row) {
                $tahun = date('Y');
                $nip = $row->nip;
                $nama = $row->nama;
                $nama_penyetuju = $row->penyetuju;
                $nip_penyetuju = $row->nip_penyetuju;
                $to = $row->email_penyetuju;
                $tanggal_mulai = tgl_indo($row->tgl_awal);
                $tanggal_selesai = tgl_indo($row->tgl_akhir);
                $cc = $this->Main_Model->pecah_cc($row->cc);
                $cc[] = 'anne.margaretha@gmedia.co.id';
                $from = 'hrd.smg@gmedia.co.id';
                $fcm_id = $row->fcm_penyetuju;
                $id_kontrak = $row->id_kontrak;


                $subject = '[Aplikasi HRD] Karyawan Kontrak Habis';
                $message  = '<table>
		            <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama_penyetuju.'</b><br /><b>'.$nip_penyetuju.'</b></td></tr>
		            <tr><td colspan="3">&nbsp;</td></tr>
		            <tr><td colspan="3">Dengan ini memberitahukan bahwa karyawan berikut akan habis masa kontraknya : </td></tr>
		            <tr><td><b>Nama</b></td><td> : </td><td>'.$nama.'</td></tr>
		            <tr><td><b>NIK</b></td><td> : </td><td>'.$nip.'</td></tr>
		            <tr><td><b>Tanggal Mulai</b></td><td> : </td><td>'.$tanggal_mulai.'</td></tr>
		            <tr><td><b>Tanggal Selesai</b></td><td> : </td><td>'.$tanggal_selesai.'</td></tr>

		            <tr><td colspan="3">&nbsp;</td></tr>
		            <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
		            <tr><td colspan="3">&nbsp;</td></tr>

		            <tr><td colspan="3">&nbsp;</td></tr>
		            <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
		        </table>';
                if ($to != '') {
                    $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
                    $title = 'Karyawan Kontak Habis';
                    $text = 'Cek Email untuk keterangan lebih lanjut. ';
                    $this->Rest_Model->notif_fcm($title, $text, $fcm_id);

                    $this->Main_Model->process_data('log_reminder_kontrak', ['nip' => $nip, 'id_kontrak' => $id_kontrak]);
                }
            }
        }
    }
}

/* End of file reminder.php */
/* Location: ./application/controllers/reminder.php */
