<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cuti extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Absensi_Model', '', TRUE);
		$this->load->model('Cuti_Model', '', TRUE);
		$this->load->model('Reminder_Model', '', TRUE);
		$this->load->model('Rest_Model', '', TRUE);
	}

	function header()
	{
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>';

		return $footer;
	}

	function generate_cuti($year='')
    {
        ($year == '') ? $year = date('Y') : $year = $year;
        $this->Main_Model->generate_cuti($year); 
    }

    function generate_cuti_lalu()
    {
        $year = date("Y");
        $year_lalu = $year-1;
        $this->Main_Model->generate_cuti($year_lalu); 
    }

	function cutibiasa()
	{
		$this->Main_Model->get_login();
		// $this->generate_cuti();
        // $this->generate_cuti_lalu();

        $t = $this->Main_Model->view_by_id('ms_tipe_cuti', array(), '');
        $tipe = array();
        foreach($t as $row) {
        	$tipe[$row->id] = $row->tipe;
        }

		$data 	= array(
			'nip' => $this->Main_Model->all_kary_active(),
			'th' => $this->Cuti_Model->th_cutibiasa(),
			'tipe' => $tipe,
			'periode' => $this->Main_Model->periode_opt(),
			'js' => $this->footer()
			);

		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','4')
		);
		$this->load->view('template/header',$header);
		$this->load->view('cuti/cutibiasa',$data);
	}

	function cek_qt($nip='')
	{
		$this->Main_Model->all_login();
		$data = $this->Cuti_Model->cek_qt($nip);
		$qt = isset($data->qt) ? $data->qt : '0';
		echo '<div class="form-group"><label>Sisa Cuti : </label>'.$qt.'</div>';
	}

	function view_cutibiasa()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Cuti_Model->view_cutibiasa($periode);
		$template = $this->Main_Model->tbl_temp();
		$array_heading = array('No', 'NIP', 'Nama', 'Cabang', 'Alasan Cuti', 'Tipe Cuti', 'Status', 'Tanggal Cuti', 'Action');
		$no = 1;
		$this->table->set_heading($array_heading);
		foreach ($data as $row) {
			($row->flag == 1 && $row->approval == 6) ? $action = '	<li> <a href="javascript:;" onclick="approval(' . $row->id_cuti_det . ', 1);"> <i class="icon-delete"></i> Proses </a> </li>' : $action = '';
			$array_data = array(
				$no++, $row->nip, $row->nama, $row->cabang, $row->alasan_cuti, $row->tipe, $row->keterangan, $row->tgl, 
				'<div class="btn-group">
	                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                    <i class="fa fa-angle-down"></i>
	                </button>
	                    <ul class="dropdown-menu" role="menu">
	                    '.$action.'
	                        <li> <a href="javascript:;" onclick="get_id(' . $row->id_cuti_det . ');"> <i class="icon-edit"></i> Update </a> </li>
	 						<li> <a href="javascript:;" onclick="delete_data(' . $row->id_cuti_det . ');"> <i class="icon-delete"></i> Delete </a> </li>
	                    </ul>
	            </div>'
				);

			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function proses_approval($id='', $val='')
	{
		$this->Main_Model->all_login();
		$user = $this->session->userdata('username');
		$date = date('Y-m-d H:i:s');
		$k = $this->Main_Model->view_by_id('cuti_det', array('id_cuti_det' => $id), 'row');
		$kode = isset($k->approval) ? $k->approval : ''; 
		$pola = isset($k->pola) ? $k->pola : '';
		$value = $this->Main_Model->rule_approval($kode, $pola, $val);
		$data = array(
			'approval' => $value,
			'user_update' => $user,
			'update_at' => $date
		);
		
		$this->Cuti_Model->update_cutibiasa($data, $id);
		$this->Reminder_Model->reminder_cuti($value, $id);

		$this->generate_cuti();
        $this->generate_cuti_lalu();
	}

	function proses_approval_khusus($id='', $val='')
	{
		$this->Main_Model->all_login();
		$user = $this->session->userdata('username');
		$date = date('Y-m-d H:i:s');
		$k = $this->Main_Model->view_by_id('cuti_khusus', array('id' => $id), 'row');
		// print_r($k);exit;
		$kode = isset($k->approval) ? $k->approval : ''; 
		$value = $this->Main_Model->rule_approval($kode, 'cuti', $val);
		$data = array(
			'approval' => $value,
			'user_update' => $user,
			'update_at' => $date
			);
		$this->Cuti_Model->process_cuti_khusus($data, $id);
	}

	function cutikhusus()
	{
		$this->Main_Model->get_login();
		$url_load 	= base_url('cuti/view_cutimelahirkan');
		$url_del 	= base_url('cuti/delete_cutimelahirkan');
		$url_save 	= base_url('cuti/process_cuti_khusus');
		$javascript = '
			<script type="text/javascript">
				'.$this->Main_Model->default_datepicker()
				 .$this->Main_Model->default_select2().'
				function reset() {
					$(".select2").val("").trigger("change");
					$(".kosong").val("");
				}

				'.$this->Main_Model->default_loadtable($url_load).'

				function get_id(id) {
					id = {"id" : id}
					$.ajax({
						url : "'.base_url('cuti/get_id_cutimelahirkan').'",
						data : id,
						dataType: "JSON",
						success : function(data){
							$("#myModal").modal();
							$("#id").val(data.id);
							$("#nip").val(data.nip).trigger("change");
							$("#tipe").val(data.id_tipe_cuti).trigger("change");
							$("#tgl_awal").val(data.tglawal);
							$("#tgl_akhir").val(data.tglakhir);
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal mengambil data");
						}
					});
				}
				
				function save() {
					$.ajax({
						url : "'.$url_save.'",
						type : "POST",
						data : $("#cutikhusus").serialize(),
						dataType : "JSON",
						success : function(data){
							if(data.status=="true") {
								load_table();
								'.$this->Main_Model->notif200().'
								reset();
							} else {
								bootbox.alert("Gagal menyimpan data");
							}
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal menyimpan data");
						}
					});
				}

				'.$this->Main_Model->default_delete_data($url_del).'

				function proses(id, val) {
					$.ajax({
						url : "'.base_url('cuti/proses_approval_khusus').'/"+id+"/"+val,
						type : "post"
					})
				} 

				function approval(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses cuti?",
                        title : "Proses Cuti",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses(id, "approve");
                                	bootbox.alert("Cuti telah disetujui !");
                                	load_table();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses(id, "reject");
                                	bootbox.alert("Cuti telah ditolak !");
                                	load_table();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}
			</script>';
		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$data = array(
			'nip' => $this->Main_Model->all_kary_active(),
			'tipe' => $this->Cuti_Model->tipe_cuti()
			);
		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','4')
		);
		$this->load->view('template/header',$header);
		$this->load->view('cuti/cutimelahirkan',$data);
		$this->load->view('template/footer',$footer);
	}

	function view_cutimelahirkan(){
		$this->Main_Model->get_login();
		$data = $this->Cuti_Model->view_cutimelahirkan();
		$template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cabang','Cuti','Tgl Awal Cuti','Tgl Akhir Cuti','Status','Action');
		$no = 1;
        
        foreach ($data as $row) {
        	$q = $this->Main_Model->posisi($row->nip);
        	$cabang = isset($q->cabang) ? $q->cabang : '';
        	$action = array();
        	$a = "'".$row->id."'";
        	$edit = array('label' => 'Update', 'event' => 'onclick=get_id('.$a.')');
        	$delete = array('label' => 'Delete', 'event' => 'onclick=delete_data('.$a.')');
        	$approval = array();
        	if ($row->flag == 1 && $row->approval == 6) {
        		$approval = array('label' => 'Proses', 'event' => 'onclick=approval('.$a.')');
        		$action[] = $approval;
        	}

        	$action[] = $edit;
        	$action[] = $delete;

			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$cabang,
				$row->tipe,
				$this->Main_Model->format_tgl($row->tgl_awal),
				$this->Main_Model->format_tgl($row->tgl_akhir),
				$row->keterangan,
				$this->Main_Model->action($action)
		        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function get_id_cutimelahirkan()
	{
		$this->Main_Model->all_login();
		$id = $this->input->get('id');

		$data = $this->Cuti_Model->get_id_cutimelahirkan($id);
		echo json_encode($data);
	}

	function process_cuti_khusus()
	{
		$this->Main_Model->all_login();
		$id  = $this->input->post('id');
		$nip = $this->input->post('nip');
		$tipe = $this->input->post('tipe');
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');

		if($nip=="" || $tipe=="" || $tgl_awal=="" || $tgl_akhir=="") {
			$result	= array('status'=>'false');
		} else {

			$username = $this->session->userdata('username');
			$timestamp = now();

			$time = ($id == '') ? 'insert_at' : 'update_at';
			$user = ($id == '') ? 'user_insert' : 'user_update';

			// cari kode approval
			$k = $this->Main_Model->kode_approval($nip, 'cuti');
			// $approval = isset($k->kode) ? $k->kode : 1;
			$approval = 12;

			$data 	= array(
				'nip' => $nip,
				'tgl_awal' => $this->Main_Model->convert_tgl($tgl_awal),
				'tgl_akhir' => $this->Main_Model->convert_tgl($tgl_akhir),
				'id_tipe_cuti' => $tipe,
				'approval' => $approval,
				$time => $timestamp,
				$user => $username
				);

			$result = array('status'=>'true');
			$this->Cuti_Model->process_cuti_khusus($data,$id);
		}

		echo json_encode($result);
	}

	function delete_cutimelahirkan()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post("id");
		$this->Cuti_Model->delete_cutimelahirkan($id);
	}

	function add_cutibiasa()
	{
		$this->Main_Model->all_login();
		$nip = $this->input->post('nip');
		$alasan_cuti = $this->input->post('alasan');
		$tipe = $this->input->post('tipe');
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$username = $this->session->userdata('username');
		$acctype = $this->session->userdata('acctype');
		$penyetuju = $this->input->post('penyetuju');

		$t = $this->db->query("
			SELECT MAX(th)
			FROM cuti_dep 
			WHERE nip = '$nip'")->row();
		$th = isset($t->th) ? $t->th : date('Y');

		// cek kuota
		$cek_kuota 	= $this->Cuti_Model->cek_kuotacuti($nip, $th);
		if($cek_kuota) {
			if($tgl_awal == '' || $alasan_cuti == '' || $tgl_akhir == '') {
				$message = '';
				if($tgl_awal == '') $message .= 'Field Tanggal Awal Masih Kosong! <br>';
				if($tgl_akhir == '') $message .= 'Field Tanggal Akhir Masih Kosong! <br>';
				if($alasan_cuti == '') $message .= 'Field Alasan Cuti Masih Kosong! <br>';
				$return = array(
							'status' => false, 
							'message' => $message
						);
			} else {
				$tgl_awal = $this->Main_Model->convert_tgl($tgl_awal);
				$tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);
				if (strtotime($tgl_awal) <= strtotime($tgl_akhir)) {
					$tgl = range_to_date($tgl_awal, $tgl_akhir);

					$jumlah_tgl = count($tgl);
					$jumlah_kuota = isset($cek_kuota->qt) ? $cek_kuota->qt : 0;
					if($jumlah_tgl <= $jumlah_kuota) {
						// generate nomor cuti
						$q = $this->Cuti_Model->generate_nomor_cuti();
						$id_cuti = isset($q->maks) ? $q->maks : 0;

						// cari kode approval
						// $k = $this->Main_Model->kode_approval($nip, 'cuti');
						// $approval = isset($k->kode) ? $k->kode : 1;
						$pola = $this->Main_Model->kode_penyetuju($penyetuju);
						$kode_pola = isset($pola->pola) ? $pola->pola : 0;
						$approval = isset($pola->kode) ? $pola->kode : '';
						$penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
						$cc = isset($pola->cc) ? $pola->cc : '';

						if ($acctype == 'Administrator') {
							$approval = '12';
						}

						$data = array(
							'id_cuti_det' => $id_cuti,
							'nip' => $nip,
							'alasan_cuti' => $alasan_cuti,
							'id_tipe' => $tipe,
							'insert_at' => date('Y-m-d H:i:s'),
							'user_insert' => $username,
							'pola' => $kode_pola, 
							'penyetuju' => $penyetuju, 
							'cc' => $cc,
							'approval' => $approval
						);

						for($i = 0; $i < $jumlah_tgl; $i++) {
							if($tgl[$i] != '') {
								$detail[$i] = array(
									'id_cuti_det' => $id_cuti,
									'tgl' => $tgl[$i]
									);
								$this->Cuti_Model->add_subdet_cuti($detail[$i]);
							}
						}
						$this->Cuti_Model->add_cutibiasa($data);
						if ($acctype != 'Administrator') {
							$this->Rest_Model->reminder_cuti($data, $tgl);
						}
						$return = array(
									'status' => true , 
									'message' => 'Success!'
								);
					} else {
						$return = array(
									'status' => false, 
									'message' => 'Kuota Cuti tidak cukup!'
								);
					}
				} else {
					$return = array(
							'status' => false,
							'message' => 'Invalid date'
						);
				}
			}
		} else {
			$return = array(
						'status' => false, 
						'message' => 'Kuota Cuti tidak cukup!'
					);
		}

		$this->generate_cuti();
		echo json_encode($return);
	}

	function delete_cutibiasa()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$this->Cuti_Model->delete_cutibiasa($id);
	}

	function get_id_cutibiasa()
	{
		$this->Main_Model->all_login();
		$id = $this->input->get('id');
		$data = $this->Cuti_Model->get_id_cutibiasa($id);

		echo json_encode($data);
	}

	function update_cutibiasa()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$tipe = $this->input->post('tipe');
		$nip = $this->input->post('nip');
		$alasan_cuti = $this->input->post('alasan');
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$username = $this->session->userdata('username');
		$acctype = $this->session->userdata('acctype');
		$penyetuju = $this->input->post('penyetuju');

		if ($alasan_cuti == '' || $tgl_awal == '' || $tgl_akhir == '') {
			$message = '';
			if ($alasan_cuti == '') $message .= 'Field Alasan Cuti masih kosong! <br>';
			if ($tgl_awal == '') $message .= 'Field Tanggal Awal masih kosong! <br>';
			if ($tgl_akhir == '') $message .= 'Field Tanggal Akhir masih kosong! <br>';
			$return = array(
				'status' => false,
				'message' => $message
			);
		} else {
			$tgl_awal = $this->Main_Model->convert_tgl($tgl_awal);
			$tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);
			if (strtotime($tgl_awal) <= strtotime($tgl_akhir)) {
				$tgl = range_to_date($tgl_awal, $tgl_akhir);
				$jumlah_tgl = count($tgl);
				$k = $this->Cuti_Model->cek_kuotacuti($nip);
				$kuota = isset($k->qt) ? $k->qt : 0;

				// jumlah cuti lama
				$j = $this->Cuti_Model->hitung_cuti_lama($id);
				$jumlah_lama = isset($j->jml) ? $j->jml : 0;
				$kuota = ($kuota + $jumlah_lama) - $jumlah_tgl;

				if ($kuota >= 0) {

					// cari kode approval
					// $k = $this->Main_Model->kode_approval($nip, 'cuti');
					// $approval = isset($k->kode) ? $k->kode : 1;
					$pola = $this->Main_Model->kode_penyetuju($penyetuju);
					$kode_pola = isset($pola->pola) ? $pola->pola : 0;
					$approval = isset($pola->kode) ? $pola->kode : '';
					$penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
					$cc = isset($pola->cc) ? $pola->cc : '';

					if ($acctype == 'Administrator') {
						$approval = '12';
					}

					$data = array(
							'id_cuti_det' => $id,
							'nip' => $nip,
							'alasan_cuti' => $alasan_cuti,
							'id_tipe' => $tipe,
							'update_at' => date('Y-m-d H:i:s'),
							'user_update' => $username,
							'pola' => $kode_pola, 
							'penyetuju' => $penyetuju, 
							'cc' => $cc,
							'approval' => $approval
						);

					$this->Main_Model->delete_data('cuti_sub_det', array('id_cuti_det' => $id));
					for ($i = 0; $i < $jumlah_tgl; $i++) {
						if ($tgl[$i] != '') {
							$detail[$i] = array(
								'id_cuti_det' => $id,
								'tgl' => $tgl[$i]
							);
							$this->Cuti_Model->add_subdet_cuti($detail[$i]);
						}
					}
					$this->Cuti_Model->update_cutibiasa($data, $id);
					if ($acctype != 'Administrator') {
						$this->Rest_Model->reminder_cuti($data, $tgl);
					}
					$return = array(
						'status' => true, 
						'message' => 'Success!'
					);
				} else {
					$return = array(
						'status' => false,
						'message' => 'Kuota Cuti tidak cukup'
					);
				}
			} else {
				$return = array(
					'status' => false,
					'message' => 'Invalid date'
				);
			}
		}

		$this->generate_cuti();
		echo json_encode($return);
	}

	function del_detail($id)
	{
		$this->Cuti_Model->del_detail($id);
	}

	function verifikasi_cuti()
	{
		$this->Main_Model->guest_login();
		$data = $this->Cuti_Model->view_verifikasi_cuti();
        $is_approval = $this->Main_Model->is_approval();
        $template = $this->Main_Model->tbl_temp('tb_klarifikasi_cuti');
        $array_heading = array('No', 'NIP', 'Nama', 'Tipe Cuti', 'Alasan Cuti', 'Tanggal', 'Status');
        if ($is_approval == TRUE)  $array_heading[] = 'Action';
        $this->table->set_heading($array_heading);
   
        $i = 1;
        foreach ($data as $row) {
        	
			$array_data = array($i++, $row->nip, $row->nama, $row->tipe, $row->alasan_cuti, $row->tgl, $row->keterangan);

			if ($row->flag != 0) $action = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="approval_cuti('.$row->id_cuti_det.');"> <i class="fa fa-check"></i> Verifikasi </a>';
			if ($is_approval == TRUE) $array_data[] = $action;
            
            $this->table->add_row($array_data);
        }
        $this->table->set_template($template);
		echo $this->table->generate();
	}

	function verifikasi_cuti_khusus()
	{
		$this->Main_Model->guest_login();
		$data = $this->Cuti_Model->view_verifikasi_cuti_khusus();
        $is_approval = $this->Main_Model->is_approval();
		$template = $this->Main_Model->tbl_temp('tb_klarifikasi_cuti_khusus');
		$array_heading = array('No', 'NIP', 'Nama', 'Tipe Cuti', 'Tanggal', 'Status');
		if ($is_approval == TRUE) $array_heading[] = 'Action';
		$this->table->set_heading($array_heading);

		$i = 1;
		foreach ($data as $row) { 
			$tgl_awal = $this->Main_Model->format_tgl($row->tgl_awal);
			$tgl_akhir = $this->Main_Model->format_tgl($row->tgl_akhir);

			$array_data = array($i++, $row->nip, $row->nama, $row->tipe, $tgl_awal.' s/d '.$tgl_akhir, $row->keterangan);

			if ($row->flag != 0) $action = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="approval_cuti_khusus('.$row->id.');"> <i class="fa fa-check"></i> Verifikasi </a>';
			if ($is_approval == TRUE) $array_data[] = $action;

			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}
}