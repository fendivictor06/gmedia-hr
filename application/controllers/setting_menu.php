<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_Menu extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
		$this->load->model('Menu_Model');
	}

	function menu($user_name='')
	{
		$this->Main_Model->get_login();
		$menu = $this->Main_Model->menu_admin('0','0','3');
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker()
		      .$this->Main_Model->js_tree();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
				      .$this->Main_Model->style_tree()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'json' => $this->Menu_Model->json_menu($user_name),
			'user' => $user_name
			);

		$this->load->view('template/header', $header);
		$this->load->view('main/setting_menu',$data);
	}

	function json_menu()
	{
		$this->Main_Model->get_login();
		$data = $this->Menu_Model->json_menu();

		echo json_encode($data);
	}

	function menu_process()
	{
		$this->Main_Model->get_login();
		$checked = $this->input->post('checked');
		$user = $this->input->post('user');

		if ($user == '') {
			$status = FALSE;
			$message = 'Username Kosong !';
		} else {
			$this->Main_Model->delete_data('tb_user_menu', array('usr_name' => $user));
			$jml = count($checked);
			for ($i = 0; $i < $jml; $i++) {
				$p = $this->Main_Model->view_by_id('menu', array('id_menu' => $checked[$i]), 'row');
				$parent = isset($p->parent) ? $p->parent : '';
				$data[$i] = array(
					'usr_name' => $user,
					'id_menu' => $checked[$i],
					'parent' => $parent
				);

				$simpan = $this->Main_Model->process_data('tb_user_menu', $data[$i]);
			}

			if ($simpan > 0) {
				$status = TRUE;
				$message = 'Berhasil Menyimpan Data!';
			} else {
				$status = FALSE;
				$message = 'Gagal Menyimpan Data!';
			}
		}

		echo json_encode(array('status' => $status, 'message' => $message));
 	}

}

/* End of file setting_menu.php */
/* Location: ./application/controllers/setting_menu.php */ ?>