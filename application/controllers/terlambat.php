<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Terlambat extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Cuti_Model', '', true);
        $this->load->model('Api_Model', '', true);
        $this->load->model('Terlambat_Model', '', true);
    }

    function index()
    {
        show_404();
    }

    function terlambat_today()
    {
        $this->Main_Model->all_login();
        $session_cabang = $this->Main_Model->session_cabang();

        $view_url = base_url('terlambat/view_terlambat_today');
        $excel_url = base_url('download_excel/terlambat_today');

        (empty($session_cabang)) ? $menu = $this->Main_Model->menu_admin('0', '0', '4') : $menu = $this->Main_Model->menu_user('0', '0', '72');
        (empty($session_cabang)) ? $template_header = 'template/header' : $template_header = 'akun/header';
        (empty($session_cabang)) ? $template_footer = 'template/footer' : $template_footer = 'akun/footer';

        $javascript = '
			<script>

				'.$this->Main_Model->default_loadtable($view_url).'

				$("#download").click(function(){
                    var periode = $("#periode").val();
                    window.location.href="'.$excel_url.'";
                });

			</script>
		';
        $header = array(
                'menu' => $menu,
                'style' => $this->Main_Model->style_datatable()
                          .$this->Main_Model->style_modal()
            );
        $footer = array(
                'javascript' => $javascript,
                'js' => $this->Main_Model->js_datatable()
                                  .$this->Main_Model->js_modal()
                                  .$this->Main_Model->js_bootbox()
            );
        $data   = array(
            'h1' => 'Terlambat Hari ini - '.date('d F Y'),
            'periode' => $this->Main_Model->periode_opt(),
            'isperiode' => '0'
        );

        $this->load->view($template_header, $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view($template_footer, $footer);
    }

    function view_terlambat_today()
    {
        $this->Main_Model->all_login();
        $session_cabang = $this->Main_Model->session_cabang();
        $data = $this->Api_Model->terlambat();
        // (empty($session_cabang)) ? $id_table = 'dataTables-example' : $id_table = 'terlambat';
        $template = $this->Main_Model->tbl_temp($id_table);
        $button = '';
        // (empty($session_cabang)) ? $button = '' : $button = '<div class="form-group"> <button type="button" onclick="unduh()" class="btn btn-default" id="download_1"><i class="fa fa-download"></i> Download</button> </div>';

        $this->table->set_heading('No', 'NIP', 'PIN', 'Nama', 'Cabang', 'Waktu Scan', 'Jam Masuk');
        $no =1;
        
        foreach ($data as $row) {
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->pin,
                $row->nama,
                $row->cabang,
                $row->scan,
                $row->jam_masuk
            );
        }

        $this->table->set_template($template);
        echo $button.$this->table->generate();
    }

    function get_id_terlambat($id = '')
    {
        // $cabang = $this->session->userdata('cabang');
        // ($cabang == 0) ? $this->Main_Model->get_login() : $this->Main_Model->guest_login();
        $this->Main_Model->all_login();
        $data = $this->Absensi_Model->get_id_terlambat($id);

        echo json_encode($data);
    }

    function view_terlambat()
    {
        $this->Main_Model->guest_login();
        $is_approval = $this->Main_Model->is_approval();
        $data = $this->Terlambat_Model->view_terlambat();
        $template = $this->Main_Model->tbl_temp('tb_klarifikasi_terlambat');

        $array_heading = array('No', 'NIP', 'Nama', 'Tanggal', 'Total Menit', 'Scan Masuk', 'Cabang');
        ($is_approval == true) ? $array_heading[] = 'Action' : $array_heading = $array_heading;
        $this->table->set_heading($array_heading);

        $no = 1;
        foreach ($data as $row) {
            $tanggal  = ($row->tgl != '') ? $this->Main_Model->format_tgl($row->tgl) : '';
            $array_data = array($no++, $row->nip, $row->nama, $tanggal, $row->total_menit, $row->scan_masuk, $row->cabang);
            $event = "get_terlambat('".$row->id."')";
            $action = '<button class="btn btn-primary btn-xs" onclick="'.$event.'" ><i class="fa fa-check"></i> Verifikasi</button>';
            ($is_approval == true) ? $array_data[] = $action : $array_data = $array_data;

            $this->table->add_row($array_data);
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function klarifikasi()
    {
        $this->Main_Model->guest_login();
        $id_terlambat = $this->input->post('id_terlambat');
        $id_ab = $this->input->post('id_ab_t');
        $tgl = $this->input->post('tgl_t');
        $scan_masuk = $this->input->post('scan_masuk_t');
        $file_upload = $this->input->post('file_upload_t');
        $klarifikasi = $this->input->post('klarifikasi_t');
        $tanggal = $this->Main_Model->convert_tgl($tgl);
        $acctype = $this->session->userdata('acctype');
        $makan = $this->input->post('makan');

        $check_terlambat = $this->Main_Model->view_by_id('terlambat_temp', array('id' => $id_terlambat), 'row');
        $nip = isset($check_terlambat->nip) ? $check_terlambat->nip : '';
        $filename = 'File Terlambat '.$nip.' '.date('Y-m-d');
        $config = $this->Main_Model->set_upload_options("./assets/presensi/", "*", "5048000", $filename);
        $this->load->library('upload', $config);

        $um = $this->Api_Model->uang_makan($nip);
        $uang_makan = 0;
        $uang_denda = 0;
        if ($makan == 1) {
            $check_holiday = $this->db->where('hol_tgl', $tgl)->get('hol')->row();
            $uang_makan = isset($um->nominal) ? $um->nominal : 0;

            if (date('w', strtotime($tgl)) == 0 || date('w', strtotime($tgl)) == 6 || !empty($check_holiday)) {
                $uang_makan = 0;
            }
        } else {
            $uang_makan = 0;
            $uang_denda = isset($um->nominal) ? $um->nominal : 0;
        }

        if ($scan_masuk == '' || $klarifikasi == '') {
            $result = array('status' => false , 'message' => 'Form masih ada yang kosong!');
        } else {
            $c_exists = array('nip' => $nip, 'tgl' => $tanggal);
            $d_exists = $this->Main_Model->view_by_id('tb_klarifikasi_terlambat', $c_exists, 'row');
            if (!empty($d_exists)) {
                $result = array('status' => false , 'message' => 'Data ini sudah diverifikasi, silahkan refresh halaman anda!');
            } else {
                // if (empty($this->upload->do_upload('file_upload_t'))) {
                //  $result = array('status' => false , 'message' => 'Wajib melampirkan file!');
                // } else {
                    // cari kode approval
                    $k = $this->Main_Model->kode_approval($nip, 'klarifikasi');
                    $approval = isset($k->kode) ? $k->kode : 1;

                    // if ($acctype == 'Administrator') $approval = '12';
                    $approval = '12';
                    
                if ($check_terlambat) {
                    if ($approval == '12') {
                        $log_terlambat = array(
                            'id_terlambat' => $check_terlambat->id,
                            'nip' => $check_terlambat->nip,
                            'tgl' => $check_terlambat->tgl,
                            'total_menit' => $check_terlambat->total_menit,
                            'pin' => $check_terlambat->pin,
                            'scan_masuk' => $check_terlambat->scan_masuk,
                            'shift' => $check_terlambat->shift,
                            'jam_masuk' => $check_terlambat->jam_masuk,
                            'insert_at' => $check_terlambat->insert_at,
                            'update_at' => date('Y-m-d H:i:s'),
                            'user' => $this->session->userdata('username')
                            );
                        $this->Main_Model->process_data('log_terlambat', $log_terlambat);
                            
                        // $this->Main_Model->delete_data('terlambat_temp', array('id' => $id_terlambat));
                    }
                }

                    $check_absensi = $this->Main_Model->view_by_id('tb_absensi', array('id_ab' => $id_ab), 'row');
                if ($check_absensi) {
                    // $this->Main_Model->delete_data('log_absensi', array('id_ab' => $check_absensi->id_ab));
                    if ($approval == '12') {
                        $log_absensi = array(
                            'id_ab' => $check_absensi->id_ab,
                            'nip' => $check_absensi->nip,
                            'tgl' => $check_absensi->tgl,
                            'scan_masuk' => $check_absensi->scan_masuk,
                            'scan_pulang' => $check_absensi->scan_pulang,
                            'keterangan' => $check_absensi->keterangan,
                            'insert_at' => $check_absensi->insert_at,
                            'update_at' => date('Y-m-d H:i:s'),
                            'status' => $check_absensi->status,
                            'pin' => $check_absensi->pin,
                            'flag' => 1,
                            'user' => $this->session->userdata('username')
                            );

                        $this->Main_Model->process_data('log_absensi', $log_absensi);
                    }

                    // $um = $this->Api_Model->uang_makan();
               //          $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                    // $uang_denda = $uang_makan;

                    // if ($makan == 1) {
                    //  $uang_denda = 0;
                    // } else {
                    //  $uang_makan = 0;
                    // }

                    $data_absensi = array(
                        'scan_masuk' => $tanggal.' '.$scan_masuk.':00',
                        // 'scan_pulang' => $scan_pulang.':00',
                        'klarifikasi_terlambat' => $klarifikasi,
                        // 'status' => $ket,
                        'uang_makan' => $uang_makan,
                        'uang_denda' => $uang_denda,
                        'update_at' => date('Y-m-d H:i:s'),
                        'flag' => 1,
                        'user' => $this->session->userdata('username')
                    );

                    $data_klarifikasi = array(
                        'id_terlambat' => $id_terlambat,
                        'tgl' => $tanggal,
                        'klarifikasi' => $klarifikasi,
                        'id_ab' => $id_ab,
                        'approval' => $approval,
                        'user' => $this->session->userdata('username'),
                        'nip' => $check_absensi->nip,
                        'uang_makan' => $makan
                    );
                        
                    if ($this->upload->do_upload('file_upload_t')) {
                        $file = $this->upload->data();
                        $data_absensi['file_terlambat'] = $file['file_name'];
                        $data_klarifikasi['file'] = $file['file_name'];
                    }

                    $this->Main_Model->process_data('tb_klarifikasi_terlambat', $data_klarifikasi);
                    $this->Main_Model->process_data('tb_absensi', $data_absensi, array('id_ab' => $id_ab));
                }
                    $result = array('status' => true , 'message' => 'Data berhasil disimpan!');
                // }
            }
        }
        echo json_encode($result);
    }
}

/* End of file terlambat.php */
/* Location: ./application/controllers/terlambat.php */
