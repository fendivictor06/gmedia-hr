<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
    
class Penggajian extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penggajian_Model', '', true);
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
    }
        
    function staff_rule()
    {
        $this->Main_Model->get_login();
        $id = $this->session->userdata('idp');
        $url_del    = base_url('penggajian/delete_rule_staff');
        $menu   = '
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">';
        $data = array(
            'band' => $this->Penggajian_Model->option_band(),
            'header' => $this->Penggajian_Model->perusahaan_id($id)
        );
        
        $javascript = '<script type="text/javascript">
						'.$this->Main_Model->notif().'

						function reset() {
							$(".kosong").val("");
						}

    					function load_table() {
    						var idp = {"idp":' . $id . '}
    						$.ajax({
    							url	: "' . base_url('penggajian/view_staff_rule') . '",
    							type : "GET",
    							data : idp,
    							success : function(data){
    								$("#myTable").html(data);
    								$("#dataTables-example").DataTable({
        								responsive: true
    								});
    							},
    							error : function(jqXHR, textStatus, errorThrown){
    								'.$this->Main_Model->notif500().'
    							}
    						});
    					}
    					load_table();

    					function save() {
	    					$.ajax({
	    						url 	: "' . base_url('penggajian/staff_rule_process') . '",
	    						type 	: "POST",
	    						data 	: $("#staffrule").serialize(),
	    						dataType: "JSON",
	    						success : function(data){
	    							if(data.status=="true") {
	    								load_table();
	    								reset();
	    							}
	    								notif(data.message);
	    						},
	    						error 	: function(jqXHR, textStatus, errorThrown){
	    							'.$this->Main_Model->notif500().'
	    						}
	    					});
    					}

    					function get_id(id)
    					{
					    	$.ajax({
					    		url 	: "' . base_url('penggajian/staff_rule_id') . '/"+id,
					    		dataType:"JSON",
					    		success : function(data){
									$("#myModal").modal();
									$("#id_ssc").val(data.id_ssc);
									$("#sal_pos").val(data.sal_pos);
									$("#id_band").val(data.id_band);
									$("#td").val(data.td);
									$("#tpj").val(data.tpj);
									$("#tk").val(data.tk);
					    		},
					    		error : function(jqXHR, textStatus, errorThrown){
					    			'.$this->Main_Model->notif500().'
					    		}
					    	});
					    }

					    '.$this->Main_Model->default_delete_data($url_del).'
				</script>';
        
        $footer = array(
            'javascript' => $javascript,
            'js' => '
			<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
			<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
			<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script><script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
			<script src=' . base_url('assets/plugins/bootbox/bootbox.min.js') . '></script>'
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0', '0', '3'),
            'style' => $menu
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('penggajian/staff_rule', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_staff_rule()
    {
        $this->Main_Model->get_login();
        $id = $this->input->get('idp');
        $data = $this->Penggajian_Model->staff_rule($id);
        $template = $this->Main_Model->tbl_temp();

        // $this->table->set_heading('No','Jabatan','Tunjangan Jabatan','Tunjangan Komunikasi','Action');
        $this->table->set_heading('#', 'Jabatan', 'Action');
        $no = 1;
        
        foreach ($data as $row) {
            $this->table->add_row(
                $no++,
                $row->sal_pos,
                // $row->band,
                // $row->td,
                // $this->Main_Model->uang($row->tpj),
                // $this->Main_Model->uang($row->tk),
                $this->Main_Model->default_action($row->id_ssc)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function staff_rule_id($id)
    {
        $this->Main_Model->get_login();
        $result = $this->Penggajian_Model->staff_rule_id($id);
        
        echo json_encode($result);
    }

    function staff_rule_process()
    {
        $this->Main_Model->get_login();
        $id_ssc     = $this->input->post('id_ssc');
        $sal_pos    = $this->input->post('sal_pos');
        $id_band    = $this->input->post('id_band');
        $td         = $this->input->post('td');
        $tpj        = $this->input->post('tpj');
        $tk         = $this->input->post('tk');
        $idp        = $this->session->userdata('idp');

        if ($sal_pos=="") {
            $result = array('status' => 'false', 'message' => 'Please Complete this form!');
        } else {
            $data = array(
                'sal_pos'   => $sal_pos,
                'id_band'   => $id_band,
                'td'        => $td,
                'tpj'       => $tpj,
                'tk'        => $tk,
                'idp'       => $idp
            );
            
            $this->Penggajian_Model->staff_rule_process($data, $id_ssc);
            $result = array('status' => 'true', 'message' => 'Success!');
        }

        echo json_encode($result);
    }
    
    function delete_rule_staff()
    {
        $this->Main_Model->get_login();
        $id_ssc = $this->input->post('id');
        $data   = array('status'=>0);

        $this->Penggajian_Model->staff_rule_process($data, $id_ssc);
        // $this->Penggajian_Model->delete_rule_staff($id_ssc);
    }

    
    function non_staff_rule()
    {
        $this->Main_Model->get_login();
        $id         = $this->session->userdata('idp');
        $url_del    = base_url('penggajian/delete_rule_nonstaff');
        $menu       = array(
            'menu2' => 'active open selected',
            'style' => '
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">'
        );
        $option     = array(
            'BETWEEN'       => 'BETWEEN',
            'BIGGERTHAN'    => 'BIGGERTHAN',
            'EQUAL'         => 'EQUAL',
            'LESSTHAN'      => 'LESSTHAN'
        );
        $data       = array(
            'header'        => $this->Penggajian_Model->perusahaan_id($id),
            'operator'      => $option
        );
        $javascript = '<script type="text/javascript">
						function reset()
						{
							$(".kosong").val("");
						}

    					function load_table()
    					{
    						var idp = {"idp":' . $id . '}
    						$.ajax({
    							url	: "' . base_url('penggajian/view_nonstaff_rule') . '",
    							type : "GET",
    							data : idp,
    							success : function(data){
    								$("#myTable").html(data);
    								$("#dataTables-example").DataTable({
        								responsive: true
    								});
    							},
    							error : function(jqXHR, textStatus, errorThrown){
    								'.$this->Main_Model->notif500().'
    							}
    						});
    					}
    					load_table();

    					function save()
    					{
	    					$.ajax({
	    						url 	: "' . base_url('penggajian/nonstaff_rule_process') . '",
	    						type 	: "POST",
	    						data 	: $("#nonstaffrule").serialize(),
	    						dataType: "JSON",
	    						success : function(data){
	    							if(data.status=="true")
	    							{
		    							'.$this->Main_Model->notif200().'
		    							reset();
		    							load_table();
		    						}
		    						else
		    						{
		    							'.$this->Main_Model->notif400().'
		    						}
	    						},
	    						error 	: function(jqXHR, textStatus, errorThrown){
	    							'.$this->Main_Model->notif500().'
	    						}
	    					});		
    					}

    					function get_id(id)
    					{
					    	$.ajax({
					    		url 	: "' . base_url('penggajian/nonstaff_rule_id') . '/"+id,
					    		dataType:"JSON",
					    		success : function(data){
									$("#myModal").modal();
									$("#id_nsc").val(data.id_nsc);
									$("#sal_pos").val(data.sal_pos);
									$("#perf").val(data.perf);
									$("#operator").val(data.opr);
									$("#val1").val(data.val1);
									$("#val2").val(data.val2);
									$("#gd").val(data.gd);
									$("#tops").val(data.tops);
									$("#bb").val(data.bb);
									$("#btw").val(data.btw);
					    		},
					    		error 	: function(jqXHR, textStatus, errorThrown){
					    			'.$this->Main_Model->notif500().'
					    		}
					    	});
					    }
					    '.$this->Main_Model->default_delete_data($url_del).'
				</script>';
        
        $footer = array(
            'javascript' => $javascript,
            'js' => '
			<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
			<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
			<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
			<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
			<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>'
        );
        
        $this->load->view('template/header', $menu);
        $this->load->view('penggajian/non_staff_rule', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_nonstaff_rule()
    {
        $this->Main_Model->get_login();
        $id         = $this->input->get('idp');
        $data       = $this->Penggajian_Model->non_staff_rule($id);
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'Sal_Pos', 'Perf', 'Operator', 'val1', 'val2', 'gd', 'tops', 'bb', 'btw', 'Action');
        $no =1;
        
        foreach ($data as $row) {
            $this->table->add_row(
                $no++,
                $row->sal_pos,
                $row->perf,
                $row->opr,
                $row->val1,
                $row->val2,
                $this->Main_Model->uang($row->gd),
                $this->Main_Model->uang($row->tops),
                $this->Main_Model->uang($row->bb),
                $this->Main_Model->uang($row->btw),
                $this->Main_Model->default_action($row->id_nsc)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }


    function nonstaff_rule_process()
    {
        $this->Main_Model->get_login();
        $id_nsc     = $this->input->post('id_nsc');
        $sal_pos    = $this->input->post('sal_pos');
        $perf       = $this->input->post('perf');
        $opr        = $this->input->post('operator');
        $val1       = $this->input->post('val1');
        $val2       = $this->input->post('val2');
        $gd         = $this->input->post('gd');
        $tops       = $this->input->post('tops');
        $bb         = $this->input->post('bb');
        $btw        = $this->input->post('btw');
        $idp        = $this->session->userdata('idp');

        if ($sal_pos=="" || $perf=="" || $opr=="" || $val1=="" || $val2=="" || $gd=="" || $tops=="" || $bb=="" || $btw=="" || $idp=="") {
            $result = array('status'=>'false');
        } else {
            $data = array(
                'sal_pos'   => $sal_pos,
                'perf'      => $perf,
                'opr'       => $opr,
                'val1'      => $val1,
                'val2'      => $val2,
                'gd'        => $gd,
                'tops'      => $tops,
                'bb'        => $bb,
                'btw'       => $btw,
                'idp'       => $idp
            );

            $result = array('status'=>'true');
            $this->Penggajian_Model->nonstaff_rule_process($data, $id_nsc);
        }

        echo json_encode($result);
    }
    
    function nonstaff_rule_id($id)
    {
        $this->Main_Model->get_login();
        $result = $this->Penggajian_Model->nonstaff_rule_id($id);
        
        echo json_encode($result);
    }
    
    function delete_rule_nonstaff()
    {
        $this->Main_Model->get_login();
        $id_nsc = $this->input->post('id');
        $this->Penggajian_Model->delete_rule_nonstaff($id_nsc);
    }
    
    function periode_gaji()
    {
        $this->Main_Model->get_login();
        $url_load   = base_url('penggajian/view_periode_gaji');
        $url_del    = base_url('penggajian/delete_qdet');
        $url_save   = base_url('penggajian/process_q');
        $menu       = '
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
            	<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
        $footer     = array(
            'javascript'    => '<script type="text/javascript">
            					'.$this->Main_Model->default_datepicker().'
								var save_method;
    							'.$this->Main_Model->default_loadtable($url_load)
                                 .$this->Main_Model->default_select2().'
		    					function reset() {
		    						save_method = "save";
		    						$(".kosong").val("");
		    						$(".select2").val("").trigger("change");
		    					}

		    					function save() {
			    					$.ajax({
			    						url : "'.$url_save.'",
			    						data : $("#fper_gaji").serialize(),
			    						type : "POST",
			    						dataType : "JSON",
			    						success : function(data){
			    							if(data.status=="true") {
			    								'.$this->Main_Model->notif200().'
			    								load_table();
			    								reset();
			    							} else {
			    								'.$this->Main_Model->notif400().'
			    							}
			    						},	
			    						error : function(jqXHR,textStatus,errorThrown){
			    							'.$this->Main_Model->notif500().'
			    						}
			    					});
				    			}

				    			function get_id(id) {
		    						save_method = "update";
							    	$.ajax({
							    		url : "' . base_url('penggajian/q_det_id') . '/"+id,
							    		dataType : "JSON",
							    		success : function(data){
											$("#myModal").modal();
											$("#id").val(data.qd_id);
											$("#per").val(data.bln);
		    								$("#tgl_awal").val(data.tgl_awal);
		    								$("#tgl_akhir").val(data.tgl_akhir);
		    								$("#q_noncvs").val(data.q_id).trigger("change");
		    								$("#q_cvs").val(data.q_id2).trigger("change");
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			'.$this->Main_Model->notif500().'
							    		}
							    	});
							    }

							    '.$this->Main_Model->default_delete_data($url_del).'
							</script>',
            'js'            => '
            	<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
            	<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
            	<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
            	<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
            	<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>'
        );
        $data       = array(
            'periode'       => array(
                '1' => 'January',
                '2' => 'February',
                '3' => 'March',
                '4' => 'April',
                '5' => 'May',
                '6' => 'June',
                '7' => 'July',
                '8' => 'August',
                '9' => 'September',
                '10'=> 'October',
                '11'=> 'November',
                '12'=> 'December'
                ),
            'quartal' => $this->Penggajian_Model->quartal()
            );
        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '3')
            );
        $this->load->view('template/header', $header);
        $this->load->view('penggajian/periode_gaji', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_periode_gaji()
    {
        $this->Main_Model->get_login();
        $data       = $this->Penggajian_Model->view_periode_gaji();
        $template   = $this->Main_Model->tbl_temp();
        $this->table->set_heading('No', 'Periode', 'Tgl Awal', 'Tgl Akhir', 'Action');
        $no =1;
        
        foreach ($data as $row) {
            $quartal1 = $this->db->query("select q_name from q where q_id='$row->q_id'");
            $q1=isset($quartal1->row()->q_name)?$quartal1->row()->q_name:'-';
            $quartal2 = $this->db->query("select q_name from q where q_id='$row->q_id2'");
            $q2=isset($quartal2->row()->q_name)?$quartal2->row()->q_name:'-';

            $monthName = date('F', mktime(0, 0, 0, $row->bln, 10));

            $this->table->add_row(
                $no++,
                $monthName,
                $this->Main_Model->format_tgl($row->tgl_awal),
                $this->Main_Model->format_tgl($row->tgl_akhir),
                // $q1,
                // $q2,
                $this->Main_Model->default_action($row->qd_id)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function q_det_id($id)
    {
        $this->Main_Model->get_login();
        $data   =$this->Penggajian_Model->q_det_id($id);
        echo json_encode($data);
    }

    function process_q()
    {
        $this->Main_Model->get_login();
        $id         = $this->input->post('id');
        $q_id       = $this->input->post('q_noncvs');
        $q_id2      = $this->input->post('q_cvs');
        $bln        = $this->input->post('per');
        $tgl_awal   = $this->input->post('tgl_awal');
        $tgl_akhir  = $this->input->post('tgl_akhir');

        if ($bln=="" || $tgl_awal=="" || $tgl_akhir=="") {
            $result = array('status'=>'false');
        } else {
            $data = array(
                'q_id'      => '0',
                'q_id2'     => '0',
                'bln'       => $bln,
                'tgl_awal'  => $this->Main_Model->convert_tgl($tgl_awal),
                'tgl_akhir' => $this->Main_Model->convert_tgl($tgl_akhir)
                );
            $result = array('status'=>'true');
            $this->Penggajian_Model->process_q($data, $id);
        }

        echo json_encode($result);
    }

    function delete_qdet()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->post('id');
        $this->Penggajian_Model->delete_qdet($id);
    }
    
    function hari_libur()
    {
        $this->Main_Model->get_login();
        $url_load   = base_url('penggajian/view_hari_libur');
        $menu       = '
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
			<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">';
        
        $javascript = '<script>
							'.$this->Main_Model->default_datepicker().'

							var save_method;

							function reset()
							{
								$("#id").val("");
	    						$("#tgl").val("");
	    						$("#desc").val("");
	    						$("#warning").addClass("hidden");
	    						$("#success").addClass("hidden");
	    						save_method="save";
							}

							'.$this->Main_Model->default_loadtable($url_load).'

	    					function save(){
	    						var id = $("#id").val();
	    						var tgl = $("#tgl").val();
								var desc = $("#desc").val();
								var cuti = $("#cuti").val();


	    						if(tgl=="" || desc==""){
		    						'.$this->Main_Model->notif400().'
	    						}else{
						    		var dat = {
						    			"id" : id,
						    			"hol_tgl" : tgl,
						    			"hol_desc" : desc,
						    			"cuti" : cuti
						    		}

				    					if(save_method=="save"){
					    					$.ajax({
					    						url : "' . base_url('penggajian/add_hari_libur') . '",
					    						type : "POST",
					    						data : dat,
					    						success : function(data){
					    							'.$this->Main_Model->notif200().'
					    							reset();
					    							load_table();
					    						},
					    						error : function(jqXHR, textStatus, errorThrown){
					    							alert("Internal Server Error");
					    						}
					    					});
					    				}else if(save_method=="update"){
					    						$.ajax({
					    							url : "' . base_url('penggajian/update_hari_libur') . '",
					    							type : "POST",
					    							data : dat,
					    							success : function(data){
					    								'.$this->Main_Model->notif200().'
					    								reset();
					    								load_table();
					    							},
					    							error : function(jqXHR, textStatus, errorThrown){
					    								alert("Internal Server Error");
					    							}
					    						});
					    				}else{
					    					alert("Undefinied method!");
					    				}
	    						}
	    					}
	    					function get_id(id){
	    						save_method = "update";
						    	$("#warning").addClass("hidden");
						    	$("#success").addClass("hidden");
						    	id = {"id" : id}
						    	$.ajax({
						    		url : "' . base_url('penggajian/hari_libur_id') . '",
						    		type : "GET",
						    		data : id,
						    		success : function(data){
						    			var dat = jQuery.parseJSON(data);
						    			var date = dat.hol_tgl.split("-");
						    			var NewDate = date[2]+"/"+date[1]+"/"+date[0];
										$("#myModal").modal();
										$("#id").val(dat.id);
										$("#tgl").val(NewDate);
										$("#desc").val(dat.hol_desc);
										$("#cuti").val(dat.cuti)
						    		},
						    		error : function(jqXHR, textStatus, errorThrown){
						    			alert("Internal Server Error");
						    		}
						    	});
						    }
						    function delete_data(id){
						    	id = {"id":id}

						    	bootbox.dialog({
						    		message : "Yakin ingin menghapus data?",
						    		title : "Hapus Data",
						    		buttons :{
						    			danger : {
						    				label : "Delete",
						    				className : "red",
						    				callback : function(){
						    					$.ajax({
										    		url : "' . base_url('penggajian/delete_hari_libur') . '",
										    		type : "POST",
										    		data : id,
										    		success : function(data){
										    			bootbox.alert({
	    													message: "Delete Success",
	    													size: "small"
														});
										    			load_table();
										    		},
										    		error : function(jqXHR, textStatus, errorThrown){
										    			alert("Internal Server Error");
										    		}
										    	});
						    				}
						    			},
						    			main : {
						    				label : "Cancel",
						    				className : "blue",
						    				callback : function(){
						    					return true;
						    				}
						    			}
						    		}
						    	})

						    }
	    					load_table();
						</script>';
        
        $footer     = array(
            'javascript'    => $javascript,
            'js'            => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>'
        );
        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '3')
            );
        $this->load->view('template/header', $header);
        $this->load->view('penggajian/hari_libur');
        $this->load->view('template/footer', $footer);
    }
    
    function view_hari_libur()
    {
        $this->Main_Model->get_login();
        $data       = $this->Penggajian_Model->view_hari_libur();
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'Tanggal', 'Deskripsi', 'Potong Cuti', 'Action');
        $no =1;
        
        foreach ($data as $row) {
            switch ($row->cuti) {
                case '1':
                    $cuti = 'Ya';
                    break;
                case '0':
                    $cuti = 'Tidak';
                    break;
                default:
                    $cuti = '';
                    break;
            }

            $this->table->add_row(
                $no++,
                $this->Main_Model->format_tgl($row->hol_tgl),
                $row->hol_desc,
                $cuti,
                $this->Main_Model->default_action($row->id)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function add_hari_libur()
    {
        $this->Main_Model->get_login();
        // $date_create = date_create($this->input->post('hol_tgl'));
        // $date_format = date_format($date_create,"Y-m-d");
        $date_create = explode("/", $this->input->post('hol_tgl'));
        $date        = $date_create[2] . "-" . $date_create[1] . "-" . $date_create[0];
        $data        = array(
            'hol_tgl' => $date,
            'hol_desc' => $this->input->post('hol_desc'),
            'cuti' => $this->input->post('cuti')
        );
        
        $this->Penggajian_Model->add_hari_libur($data);
    }

    function hari_libur_id()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->get('id');
        $result = $this->Penggajian_Model->hari_libur_id($id);
        
        echo json_encode($result);
    }
    
    function update_hari_libur()
    {
        $this->Main_Model->get_login();
        $date_create = explode("/", $this->input->post('hol_tgl'));
        $date        = $date_create[2] . "-" . $date_create[1] . "-" . $date_create[0];
        $data        = array(
            'hol_tgl' => $date,
            'hol_desc' => $this->input->post('hol_desc'),
            'cuti' => $this->input->post('cuti')
        );
        $id          = $this->input->post('id');
        
        $this->Penggajian_Model->update_hari_libur($data, $id);
    }
    
    function delete_hari_libur()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Penggajian_Model->delete_hari_libur($id);
    }

    function aturan_bpjs_ks()
    {
        $this->Main_Model->get_login();
        $url    = base_url('penggajian/view_aturan_bpjs_ks');
        $url_umk= base_url('penggajian/view_bpjs_ks_umk');
        $menu   = '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">';
        $footer = array(
            'javascript' => '
            	<script>
            		'.$this->Main_Model->default_loadtable($url, 'load_bpjs()', 'bpjs', 'tbBpjs')
                     .$this->Main_Model->default_loadtable($url_umk, 'load_umk()', 'umk', 'tbUmk').'
            		 function get_id(id)
            		 {
            		 	$("#warning").addClass("hidden");
            		 	$("#success").addClass("hidden");
            		 	$.ajax({
            		 		url 	: "'.base_url('penggajian/bpjsks_id').'/"+id,
            		 		dataType: "JSON",
            		 		success : function(data){
            		 			$("#myModal").modal();
            		 			$("#id").val(data.id_bpjsks);
            		 			$("#th").val(data.th);
            		 			$("#tks").val(data.tks);
            		 			$("#pks").val(data.pks);
            		 			$("#kelas").val(data.kelas);
            		 		},
            		 		error 	: function(jqXHR,textStatus,errorThrown){
            		 			bootbox.alert("Oops Something Went Wrong!");
            		 		}
            		 	})
            		 }
            		 function save()
            		 {
            		 	$.ajax({
            		 		url 	: "'.base_url('penggajian/bpjsks_process').'",
            		 		data 	: $("#bpjsks").serialize(),
            		 		type 	: "POST",
            		 		dataType: "JSON",
            		 		success : function(data){
            		 			if(data.status=="true")
            		 			{
            		 				'.$this->Main_Model->notif200().'
            		 				load_bpjs(); load_umk();
            		 			}
            		 			else
            		 			{
            		 				'.$this->Main_Model->notif400().'
            		 			}
            		 		},
            		 		error 	: function(jqXHR,textStatus,errorThrown){
            		 			bootbox.alert("Oops Something Went Wrong!");
            		 		}
            		 	})
            		 }
            	</script>
            ',
            'js' => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>'.$this->Main_Model->js_bootbox()
        );
        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '3')
            );
        $this->load->view('template/header', $header);
        $this->load->view('penggajian/aturan_bpjs_ks');
        $this->load->view('template/footer', $footer);
    }

    function view_aturan_bpjs_ks()
    {
        $this->Main_Model->get_login();
        $data       = $this->Penggajian_Model->view_aturan_bpjs_ks();
        $template   = $this->Main_Model->tbl_temp('tbBpjs');

        $this->table->set_heading('No', 'Tahun', 'PKS', 'TKS', 'Min Kelas 1', 'Action');
        $no=1;
        $this->table->add_row(
            $no++,
            $data->th,
            $data->pks.'%',
            $data->tks.'%',
            $this->Main_Model->uang($data->kelas),
            '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" onclick="get_id(' . $data->id_bpjsks . ');">
	                            <i class="icon-edit"></i> Update </a>
	                    </li>
	                </ul>
	        </div>'
        );

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function view_bpjs_ks_umk()
    {
        $this->Main_Model->get_login();
        $bpjs       = $this->Penggajian_Model->view_aturan_bpjs_ks();
        $umk        = $this->Main_Model->fk_th();
        $template   = $this->Main_Model->tbl_temp('tbUmk');

        $this->table->set_heading('No', 'Lokasi', 'UMK', 'PKS Kelas 1', 'TKS Kelas 1', 'PKS Kelas 2', 'TKS Kelas 2');
        $no=1;
        foreach ($umk as $row) {
            $umk    = $row->umk;
            $pks    = $bpjs->pks*$umk/100;
            $tks    = $bpjs->tks*$umk/100;

            $pks1   = $bpjs->pks*$bpjs->kelas/100;
            $tks1   = $bpjs->tks*$bpjs->kelas/100;

            $this->table->add_row(
                $no++,
                $row->lku,
                $this->Main_Model->uang($umk),
                $this->Main_Model->uang($pks1),
                $this->Main_Model->uang($tks1),
                $this->Main_Model->uang($pks),
                $this->Main_Model->uang($tks)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function bpjsks_id($id)
    {
        $this->Main_Model->get_login();
        $data   =   $this->Penggajian_Model->bpjsks_id($id);
        echo json_encode($data);
    }

    function bpjsks_process()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->post("id");
        $th     = $this->input->post("th");
        $tks    = $this->input->post("tks");
        $pks    = $this->input->post("pks");
        $kelas  = $this->input->post("kelas");

        if ($id=="" || $th=="" || $tks=="" || $pks=="" || $kelas=="") {
            $result = array('status'=>'false');
        } else {
            $data = array(
                'nama'  => 'bpjs '.$th,
                'th'    => $th,
                'tks'   => $tks,
                'pks'   => $pks,
                'kelas' => $kelas
                );
            $this->Penggajian_Model->bpjsks_process($data, $id);
            $result = array('status'=>'true');
        }

        echo json_encode($result);
    }
    
    function aturan_bpjs()
    {
        $this->Main_Model->get_login();
        $menu   = '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">';
        // $url_bpjs    = base_url('penggajian/view_bpjs');
        $url_bpjs = base_url('penggajian/view_bpjs_ketenagakerjaan');
        $url_umk    = base_url('penggajian/view_bpjs_umk');
        $footer = array(
            'javascript' => '<script type="text/javascript">
    							var save_method;
        						'.$this->Main_Model->default_loadtable($url_umk, 'load_umk()', 'umk', 'tbUmk').'

        						function load_bpjs() {
        							var cabang = $("#cabang").val();
        							$.ajax({
        								url : "'.$url_bpjs.'/"+cabang,
        								beforeSend : function() {
											App.blockUI({
							                    boxed: !0
							                });
										},
										complete : function() {
											App.unblockUI();
										},
										success : function(data) {
											$("#bpjs").html(data);
										},
										error : function() {
											toastr.warning("", "Gagal mengambil data");
										}
        							});
        						}

        						$("#unduh").click(function() {
        							var cabang = $("#cabang").val();
        							window.location.href="'.base_url('download_file/download_bpjs').'/"+cabang;
        						});

        						$(document).ready(function() {
        							load_bpjs();
        						});

        						$("#cabang").change(function() {
        							load_bpjs();
        						});

        						function reset()
        						{
        							$("#myModal").modal();
        							save_method="save";
        						}

        						function get_id(id)
        						{
							    	id = {"id" : id}
							    	$.ajax({
							    		url 	: "' . base_url('penggajian/bpjs_id') . '",
							    		type 	: "GET",
							    		data 	: id,
							    		success : function(data){
							    			$("#myModal").modal();
							    			var dat = jQuery.parseJSON(data);
											var id = $("#id").val(dat.id_bpjs);
				    						var th = $("#th").val(dat.th);
				    						var jkk = $("#jkk").val(dat.jkk);
				    						var jkm = $("#jkm").val(dat.jkm);
				    						var jhttk = $("#jhttk").val(dat.jhttk);
				    						var jhtp = $("#jhtp").val(dat.jhtp);
				    						var jptk = $("#jptk").val(dat.jptk);
				    						var jpp = $("#jpp").val(dat.jpp);
							    		},
							    		error 	: function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
							    }

							    '.$this->Main_Model->notif()

                                 .$this->Main_Model->post_data(base_url('penggajian/update_bpjs'), 'save()', '$("#formbpjs").serialize()', '
							    	if(data.status=="true")
							    	{
							    		load_bpjs();
							    	}
							    	notif(data.message);').'
							</script>',
            'js' => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>'.$this->Main_Model->js_bootbox()
        );

        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '2')
        );

        $ms_cabang = $this->Main_Model->view_by_id('ms_cabang', ['status' => 1], 'result');
        $arr_cabang = [];
        if (! empty($ms_cabang)) {
            $arr_cabang[0] = 'Semua Cabang';
            foreach ($ms_cabang as $row) {
                $arr_cabang[$row->id_cab] = $row->cabang;
            }
        }

        $data = array(
            'cabang' => $arr_cabang
        );

        $this->load->view('template/header', $header);
        $this->load->view('penggajian/aturan_bpjs', $data);
        $this->load->view('template/footer', $footer);
    }

    function add_bpjs()
    {
        $this->Main_Model->get_login();
        $data = array(
            'th' => $this->input->post('th'),
            'jkk' => $this->input->post('jkk'),
            'jhttk' => $this->input->post('jhttk'),
            'jhtp' => $this->input->post('jhtp'),
            'jptk' => $this->input->post('jptk'),
            'jpp' => $this->input->post('jpp'),
            'jkm' => $this->input->post('jkm')
            );
        $this->Penggajian_Model->add_bpjs($data);
    }

    function update_bpjs()
    {
        $this->Main_Model->get_login();
        if ($this->input->post('th') == "") {
            $result = array('status'=>'false', 'message'=>'Form masih ada yang kosong!');
        } else {
            $data   = array(
                'th'    => $this->input->post('th'),
                'jkk'   => $this->input->post('jkk'),
                'jhttk' => $this->input->post('jhttk'),
                'jhtp'  => $this->input->post('jhtp'),
                'jptk'  => $this->input->post('jptk'),
                'jpp'   => $this->input->post('jpp'),
                'jkm'   => $this->input->post('jkm')
                );
            $id     = $this->input->post('id');
            $this->Penggajian_Model->update_bpjs($data, $id);
            $result = array('status'=>'true', 'message'=>'Success!');
        }
        
        echo json_encode($result);
    }

    function bpjs_id()
    {
        $this->Main_Model->get_login();
        $id=$this->input->get('id');
        $data=$this->Penggajian_Model->bpjs_id($id);
        echo json_encode($data);
    }

    function view_bpjs_ketenagakerjaan($id_cabang = '')
    {
        $this->Main_Model->get_login();
        $table = '	<table class="table table-striped table-bordered" id="tb_bpjs">
    				<tr>
						<th rowspan="5">No</th>
						<th rowspan="5">Nama Lengkap Karyawan</th>
						<th rowspan="2">Gaji Karyawan</th>
						<th colspan="10">Iuran BPJS Ketenagakerjaan</th>
					</tr>
					<tr>
						<th colspan="2">Jaminan Kecelakaan Kerja</th>
						<th colspan="2">Jaminan Kematian</th>
						<th colspan="2">Jaminan Hari Tua</th>
						<th colspan="2">Jaminan Pensiun</th>
						<th colspan="2" rowspan="2">Total Iuran</th>
					</tr>
					<tr>
						<th>1</th>
						<th>2</th>
						<th>3</th>
						<th>4</th>
						<th>5</th>
						<th>6</th>
						<th>7</th>
						<th>8</th>
						<th>9</th>
					</tr>
					<tr>
						<th> </th>
						<th> Perusahaan (1*2) </th>
						<th>Karyawan</th>
						<th> Perusahaan (1*4) </th>
						<th>Karyawan</th>
						<th>Perusahaan (1*6)</th>
						<th>Karyawan (1*7)</th>
						<th>Perusahaan (1*8)</th>
						<th>Karyawan (1*9)</th>
						<th rowspan="2">Perusahaan (2+4+6+8)</th>
						<th rowspan="2">Karyawan (3+5+7+9)</th>
					</tr>
					<tr>
						<th> </th>
						<th>0.24%</th>
						<th>0</th>
						<th>0.30%</th>
						<th>0</th>
						<th>3.70%</th>
						<th>2.00%</th>
						<th>2.00%</th>
						<th>1.00%</th>
					</tr>';
        $data = $this->Penggajian_Model->bpjs_ketenagakerjaan($id_cabang);
        $total_nominal = 0;
        $total_jkkp = 0;
        $total_jkkk = 0;
        $total_jkmp = 0;
        $total_jkmk = 0;
        $total_jhtp = 0;
        $total_jhtk = 0;
        $total_jpp = 0;
        $total_jpk = 0;
        $total_perusahaan = 0;
        $total_karyawan = 0;
        if (! empty($data)) {
            $no = 1;
            foreach ($data as $row) {
                $table .= '
				<tr><td style="text-align:center;">'.$no.'</td>
					<td>'.$row->nama.'</td>
					<td style="text-align:right;">'.uang($row->nominal).'</td>
					<td style="text-align:right;">'.uang($row->jkkp).'</td>
					<td style="text-align:right;">'.uang($row->jkkk).'</td>
					<td style="text-align:right;">'.uang($row->jkmp).'</td>
					<td style="text-align:right;">'.uang($row->jkmk).'</td>
					<td style="text-align:right;">'.uang($row->jhtp).'</td>
					<td style="text-align:right;">'.uang($row->jhtk).'</td>
					<td style="text-align:right;">'.uang($row->jpp).'</td>
					<td style="text-align:right;">'.uang($row->jpk).'</td>
					<td style="text-align:right;">'.uang($row->total_p).'</td>
					<td style="text-align:right;">'.uang($row->total_k).'</td>
				</tr>';

                $no++;

                $total_nominal = $total_nominal + $row->nominal;
                $total_jkkp = $total_jkkp + $row->jkkp;
                $total_jkkk = $total_jkkk + $row->jkkk;
                $total_jkmp = $total_jkmp + $row->jkmp;
                $total_jkmk = $total_jkmk + $row->jkmk;
                $total_jhtp = $total_jhtp + $row->jhtp;
                $total_jhtk = $total_jhtk + $row->jhtk;
                $total_jpp = $total_jpp + $row->jpp;
                $total_jpk = $total_jpk + $row->jpk;
                $total_perusahaan = $total_perusahaan + $row->total_p;
                $total_karyawan = $total_karyawan + $row->total_k;
            }
        }
        $table .= '	<tr>
						<th colspan="2"> </th>
						<th style="text-align:right;">'.uang($total_nominal).'</th>
						<th style="text-align:right;">'.uang($total_jkkp).'</th>
						<th style="text-align:right;">'.uang($total_jkkk).'</th>
						<th style="text-align:right;">'.uang($total_jkmp).'</th>
						<th style="text-align:right;">'.uang($total_jkmk).'</th>
						<th style="text-align:right;">'.uang($total_jhtp).'</th>
						<th style="text-align:right;">'.uang($total_jhtk).'</th>
						<th style="text-align:right;">'.uang($total_jpp).'</th>
						<th style="text-align:right;">'.uang($total_jpk).'</th>
						<th style="text-align:right;">'.uang($total_perusahaan).'</th>
						<th style="text-align:right;">'.uang($total_karyawan).'</th>
					</tr>';
        $table .= '</table>';
        echo $table;
    }

    function view_bpjs()
    {
        $this->Main_Model->get_login();
        $data = $this->Penggajian_Model->view_bpjs();

        $template   = $this->Main_Model->tbl_temp('tbBpjs');

        $this->table->set_heading('No', 'Tahun', 'JKK', 'JKM', 'JHTTK', 'JHTP', 'JPTK', 'JPP', 'Action');
        $no=1;
        $this->table->add_row(
            $no++,
            $data->th,
            $data->jkk.'%',
            $data->jkm.'%',
            $data->jhttk.'%',
            $data->jhtp.'%',
            $data->jptk.'%',
            $data->jpp.'%',
            '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" onclick="get_id(' . $data->id_bpjs . ');">
	                            <i class="icon-edit"></i> Update </a>
	                    </li>
	                </ul>
	        </div>'
        );

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function view_bpjs_umk()
    {
        $this->Main_Model->get_login();
        $max = $this->db->query("select max(th) as th from fk")->row();
        $data = $this->Penggajian_Model->view_bpjs_umk($max->th);
        $bpjs = $this->Penggajian_Model->view_bpjs();

        $tbl = '
    		<table class="table table-striped table-hover table-checkable order-column" id="tbUmk">
    			<thead>
    				<tr>
    					<th>No</th>
						<th>Lokasi</th>
						<th>UMK</th>
						<th>JKK</th>
						<th>JKM</th>
						<th>JHTTK</th>
						<th>JHTP</th>
						<th>JPTK</th>
						<th>JPP</th>
    				</tr>
    			</thead><tbody>
    	';
        $no=1;
        foreach ($data as $row) {
            $jkk = $bpjs->jkk*$row->umk/100;
            $jkm = $bpjs->jkm*$row->umk/100;
            $jhttk = $bpjs->jhttk*$row->umk/100;
            $jhtp = $bpjs->jhtp*$row->umk/100;
            $jptk = $bpjs->jptk*$row->umk/100;
            $jpp =  $bpjs->jpp*$row->umk/100;
            $tbl .= '
    		<tr>
    			<td>'.$no++.'</td>
    			<td>'.$row->lku.'</td>
    			<td>'.$this->Main_Model->uang($row->umk).'</td>
    			<td>'.$this->Main_Model->uang($jkk).'</td>
    			<td>'.$this->Main_Model->uang($jkm).'</td>
    			<td>'.$this->Main_Model->uang($jhttk).'</td>
    			<td>'.$this->Main_Model->uang($jhtp).'</td>
    			<td>'.$this->Main_Model->uang($jptk).'</td>
    			<td>'.$this->Main_Model->uang($jpp).'</td>
    		</tr>
    	';
        }

        $tbl .= '</tbody></table>';

        echo $tbl;
    }
    
    function faktor_kemahalan()
    {
        $this->Main_Model->get_login();
        $data = array(
            'lku' => $this->Penggajian_Model->view_lku()
            );
        $menu       = '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">';
        $javascript = '<script type="text/javascript">
							function load_table(){	
	    						$.ajax({
	    							url	: "' . base_url('penggajian/view_faktor_kemahalan') . '",
	    							type : "GET",
	    							success : function(data){
	    								$("#myTable").html(data);
	    								$("#dataTables-example").DataTable({
	        								responsive: true
	    								});
	    							},
	    							error : function(jqXHR, textStatus, errorThrown){
	    								alert("Internal Server Error");
	    							}
	    						});
		    				}
		    				function reset(){
		    					$("#warning").addClass("hidden");
		    					$("#success").addClass("hidden");
		    				}
		    				function get_id(id){
						    	$("#warning").addClass("hidden");
						    	$("#success").addClass("hidden");
						    	id = {"id" : id}
						    	$.ajax({
						    		url : "' . base_url('penggajian/fk_id') . '",
						    		type : "GET",
						    		data : id,
						    		success : function(data){
						    			$("#editModal").modal();
						    			var dat = jQuery.parseJSON(data);
										$("#e_th").val(dat.th);
										$("#e_lku").val(dat.id_lku);
										$("#lok").val(dat.lku);
										$("#e_umk").val(dat.umk);
										$("#e_pros").val(dat.pros);
										$("#id_fk").val(dat.id_fk);
						    		},
						    		error : function(jqXHR, textStatus, errorThrown){
						    			alert("Internal Server Error");
						    		}
						    	});
						    }
		    				function save(){
		    					';
        foreach ($data['lku'] as $row) {
            $javascript .=
            'var id_lku'.$row->id_lku.'= $("#id_lku'.$row->id_lku.'").val();
		    					var umk'.$row->id_lku.'= $("#umk'.$row->id_lku.'").val();
		    					var pros'.$row->id_lku.'= $("#pros'.$row->id_lku.'").val();
		    					';
        }
        $javascript .=      'var th = $("#th").val();var dat={';
        foreach ($data['lku'] as $row) {
            $javascript .=          '"id_lku'.$row->id_lku.'" : id_lku'.$row->id_lku.','.
            '"umk'.$row->id_lku.'" : umk'.$row->id_lku.','.
            '"pros'.$row->id_lku.'" : pros'.$row->id_lku.','
            ;
        }

        $javascript .=      '"th" : th	}

								$.ajax({
									url : "'.base_url('penggajian/add_fk').'",
									data : dat,
									type : "POST",
									success : function(data){
										'.$this->Main_Model->notif200().'
										load_table();
									},
									error : function(jqXHR,textStatus,errorThrown){
										alert("Internal Server Error");
									} 
								});
							}
							function update(){
								var id_fk = $("#id_fk").val();
								var e_th = $("#e_th").val();
								var e_lku = $("#e_lku").val();
								var e_umk = $("#e_umk").val();
								var dat = {
									"id_fk" : id_fk,
									"th" : e_th,
									"id_lku" : e_lku,
									"umk" : e_umk
								}
								if(e_th==""||e_lku==""||e_umk==""){
									'.$this->Main_Model->notif400().'
								}else{
									$.ajax({
										url : "'.base_url('penggajian/update_fk').'",
										type : "POST",
										data : dat,
										success : function(data){
											'.$this->Main_Model->notif200().'
											load_table();
										},
										error :function(jqXHR,textStatus,errorThrown){
											alert("Internal Server Error");
										}
									});
								}
							}
							function delete_data(id){
						    	id = {"id":id}

						    	bootbox.dialog({
						    		message : "Yakin ingin menghapus data?",
						    		title : "Hapus Data",
						    		buttons :{
						    			danger : {
						    				label : "Delete",
						    				className : "red",
						    				callback : function(){
						    					$.ajax({
										    		url : "' . base_url('penggajian/delete_fk') . '",
										    		type : "POST",
										    		data : id,
										    		success : function(data){
										    			bootbox.alert({
															message: "Delete Success",
															size: "small"
														});
										    			load_table();
										    		},
										    		error : function(jqXHR, textStatus, errorThrown){
										    			alert("Internal Server Error");
										    		}
										    	});
						    				}
						    			},
						    			main : {
						    				label : "Cancel",
						    				className : "blue",
						    				callback : function(){
						    					return true;
						    				}
						    			}
						    		}
						    	})

						    }
		    				load_table();
						</script>';
        $footer     = array(
            'javascript' => $javascript,
            'js' => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>'
        );
        
        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '3')
            );
        
        $this->load->view('template/header', $header);
        $this->load->view('penggajian/faktor_kemahalan', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_faktor_kemahalan()
    {
        $this->Main_Model->get_login();
        $data = $this->Penggajian_Model->view_faktor_kemahalan();
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Lokasi Utama</th>
							<th>Tahun</th>
							<th>UMK</th>
							<th>Action</th>
						</tr>
					</thead><tbody>';
        $i   = 1;
        foreach ($data as $row) {
            $tbl .= '<tr>
					<td>' . $i++ . '</td>
					<td>' . $row->lku . '</td>
					<td>' . $row->th . '</td>
					<td>' . $this->Main_Model->uang($row->umk) . '</td>
					<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu">
				                    <li>
				                        <a href="javascript:;" onclick="get_id(' . $row->id_fk . ');">
				                            <i class="icon-edit"></i> Update </a>
				                    </li>
				                    <li>
				                        <a href="javascript:;" onclick="delete_data(' . $row->id_fk . ');">
				                            <i class="icon-delete"></i> Delete </a>
				                    </li>
				                </ul>
				        </div>
					</td>
				</tr>';
        }
        $tbl .= '</tbody></table>';
        
        echo $tbl;
    }

    function add_fk()
    {
        $this->Main_Model->get_login();
        $lku = $this->Penggajian_Model->view_lku();
        $th = $this->input->post('th');
        foreach ($lku as $row) {
            $id_lku = 'id_lku'.$row->id_lku;
            $umk = 'umk'.$row->id_lku;
            $idlku[$row->id_lku] = $this->input->post($id_lku);
            $umklku[$row->id_lku] = $this->input->post($umk);

            $data = array(
                'id_lku' => $row->id_lku,
                'th' => $th,
                'umk' => $umklku[$row->id_lku]
                );
            $this->Penggajian_Model->add_fk($data);
        }
    }

    function fk_id()
    {
        $this->Main_Model->get_login();
        $id=$this->input->get('id');
        $data=$this->Penggajian_Model->fk_id($id);
        echo json_encode($data);
    }

    function update_fk()
    {
        $this->Main_Model->get_login();
        $id_fk = $this->input->post('id_fk');
        $data = array(
            'id_lku' => $this->input->post('id_lku'),
            'th' => $this->input->post('th'),
            'umk' => $this->input->post('umk'),
            );
        $this->Penggajian_Model->update_fk($data, $id_fk);
    }

    function delete_fk()
    {
        $this->Main_Model->get_login();
        $id=$this->input->post('id');
        $this->Penggajian_Model->delete_fk($id);
    }

    function quartal()
    {
        $this->Main_Model->get_login();
        $menu   = '
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
            	<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
            	<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
        $footer = array(
            'javascript' => '<script type="text/javascript">
            					'.$this->Main_Model->default_select2().'
								var save_method;
    							function load_table(){
		    						$.ajax({
		    							url	: "' . base_url('penggajian/view_quartal') . '",
		    							type : "GET",
		    							success : function(data){
		    								$("#myTable").html(data);
		    								$("#dataTables-example").DataTable({
		        								responsive: true
		    								});
		    							},
		    							error : function(jqXHR, textStatus, errorThrown){
		    								alert("Internal Server Error");
		    							}
		    						});
		    					}
		    					function reset(){
		    						save_method = "save";
		    						$("#q").val("");
		    						$("#thn").val("").trigger("change");
		    						$("#keterangan").val("");
		    						$("#warning").addClass("hidden");
				    				$("#success").addClass("hidden");
		    					}
		    					function save(){
				    				var id = $("#id").val();
				    				var q = $("#q").val();
		    						var thn = $("#thn").val();
		    						var keterangan = $("#keterangan").val();
				    				if(q==null || thn==null){
				    					'.$this->Main_Model->notif400().'
				    				}else{
				    					var dat = {
				    						"id" : id,
				    						"q" : q,
				    						"thn" : thn,
				    						"keterangan" : keterangan
				    					}
				    					if(save_method=="save"){
				    						var url = "'.base_url('penggajian/add_quartal').'";
				    					}else{
				    						var url = "'.base_url('penggajian/update_quartal').'";
				    					}

					    					$.ajax({
					    						url : url,
					    						data : dat,
					    						type : "POST",
					    						success : function(data){
					    							'.$this->Main_Model->notif200().'
					    							load_table();
					    							$("#q").val("");
		    										$("#thn").val("").trigger("change");
		    										$("#keterangan").val("");
		    										// alert(data);
					    						},	
					    						error : function(jqXHR,textStatus,errorThrown){
					    							bootbox.alert("Internal Server Error");
					    						}
					    					});
				    				}
				    			}
				    			function get_id(id){
		    						save_method = "update";
							    	$("#warning").addClass("hidden");
							    	$("#success").addClass("hidden");
							    	id = {"id" : id}
							    	$.ajax({
							    		url : "' . base_url('penggajian/q_id') . '",
							    		type : "GET",
							    		data : id,
							    		success : function(data){
			    							$("#myModal").modal();
							    			var dat = jQuery.parseJSON(data);
							    			var splitTh = dat.q_name.split(" ");
							    			$("#q").val(splitTh[0]);
		    								$("#thn").val(dat.th).trigger("change");
		    								$("#keterangan").val(dat.inf);
		    								$("#id").val(dat.q_id);
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
							    }
							    function delete_data(id){
							    	id = {"id":id}

							    	bootbox.dialog({
							    		message : "Yakin ingin menghapus data?",
							    		title : "Hapus Data",
							    		buttons :{
							    			danger : {
							    				label : "Delete",
							    				className : "red",
							    				callback : function(){
							    					$.ajax({
											    		url : "' . base_url('penggajian/delete_q') . '",
											    		type : "POST",
											    		data : id,
											    		success : function(data){
											    			bootbox.alert({
																message: "Delete Success",
																size: "small"
															});
											    			load_table();
											    		},
											    		error : function(jqXHR, textStatus, errorThrown){
											    			alert("Internal Server Error");
											    		}
											    	});
							    				}
							    			},
							    			main : {
							    				label : "Cancel",
							    				className : "blue",
							    				callback : function(){
							    					return true;
							    				}
							    			}
							    		}
							    	})

							    }
		    					load_table();
							</script>',
            'js' => '
            	<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
            	<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
            	<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
            	<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
            	<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>'
        );
        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '3')
            );
        
        $this->load->view('template/header', $header);
        $this->load->view('penggajian/quartal');
        $this->load->view('template/footer', $footer);
    }

    function view_quartal()
    {
        $this->Main_Model->get_login();
        $data = $this->Penggajian_Model->view_quartal();
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'Quartal', 'Tahun', 'Keterangan', 'Action');
        $no=1;
        foreach ($data as $row) {
            $this->table->add_row(
                $no++,
                $row->q_name,
                $row->th,
                $row->inf,
                $this->Main_Model->default_action($row->q_id)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function add_quartal()
    {
        $this->Main_Model->get_login();
        $q=$this->input->post("q");
        $thn = $this->input->post("thn");
        $data = array(
            'q_name' => $q." ".$thn,
            'th' => $thn,
            'inf' => $this->input->post("keterangan")
            );

        $this->Penggajian_Model->add_quartal($data);
    }

    function q_id()
    {
        $this->Main_Model->get_login();
        $id = $this->input->get('id');
        $data=$this->Penggajian_Model->q_id($id);
        echo json_encode($data);
    }

    function update_quartal()
    {
        $this->Main_Model->get_login();
        $q=$this->input->post("q");
        $thn = $this->input->post("thn");
        $data = array(
            'q_name' => $q." ".$thn,
            'th' => $thn,
            'inf' => $this->input->post("keterangan")
            );
        $id = $this->input->post("id");

        $this->Penggajian_Model->update_quartal($data, $id);
    }

    function delete_q()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Penggajian_Model->delete_q($id);
    }

    function bpjs_ks()
    {
        $this->Main_Model->get_login();
        $menu   = '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">
				<link href="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css') . '" rel="stylesheet" type="text/css" />';

        $url    = base_url('penggajian/view_bpjs_ks');
        $footer = array(
            'javascript' => minifyjs('<script type="text/javascript">
        						'.$this->Main_Model->default_loadtable($url).'
        						function daftar(nip)
        						{
        							$("#myModal").modal();
        							$("#nip").val(nip);
        						}

        						function save()
        						{
        							$.ajax({
        								url 	: "'.base_url('penggajian/process_bpjs_ks').'",
        								data 	: $("#form_bpjs").serialize(),
        								type 	: "POST",
        								dataType: "JSON",
        								success : function(data){
        									if(data.status=="true")
        									{
        										'.$this->Main_Model->notif200().'
        										$(".kosong").val("");
        										load_table();
        									}
        									else
        									{
        										'.$this->Main_Model->notif400().'
        									}
        								},
        								error 	: function(jqXHR,textStatus,errorThrown){
        									'.$this->Main_Model->notif500().'
        								}
        							});
        						}
        						function edit(nip)
        						{
        							$.ajax({
        								url 	: "'.base_url('penggajian/kelas_bpjs').'/"+nip,
        								dataType: "JSON",
        								success : function(data){
        									$("#myModal").modal();
        									$("#id").val(data.id_bpjs_det);
        									$("#nip").val(data.nip);
        									$("#kelas").val(data.kelas);
        								},
        								error 	: function(jqXHR,textStatus,errorThrown){
        									'.$this->Main_Model->notif500().'
        								} 
        							});
        						}
							</script>'),
            'js' => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js') . '"></script>'
        );
        
        $header = array(
            'style' => $menu,
            'menu' => $this->Main_Model->menu_admin('0', '0', '2')
        );

        $this->load->view('template/header', $header);
        $this->load->view('penggajian/bpjs_ks');
        $this->load->view('template/footer', $footer);
    }

    function view_bpjs_ks()
    {
        $this->Main_Model->get_login();
        $data       = $this->Penggajian_Model->view_bpjs_ks();
        $template   = $this->Main_Model->tbl_temp();
        $th_bpjs    = $this->Main_Model->th_bpjs_ks();
        $bpjs       = $this->Main_Model->bpjs($th_bpjs->th, 'ks');
        $h_tks      = 'TKS ('.$bpjs->tks.'%)';
        $h_pks      = 'PKS ('.$bpjs->pks.'%)';

        $this->table->set_heading('No', 'NIP', 'Nama', 'Cabang', 'Kelas', $h_tks, $h_pks, 'Total', 'Action');
        $no=1;
        foreach ($data as $row) {
            $w = $this->Main_Model->kelas_bpjs($row->nip);
        
            if ($w) {
                $e      = $this->Main_Model->lokasi_kerja($row->nip);
                $id_lku = isset($e->id_lku)?$e->id_lku:'';
                $th_fk  = $this->Main_Model->th_fk();
                $r      = $this->Main_Model->fk_lku($id_lku, $th_fk->th);
            
                $kls    = isset($w->kelas)?$w->kelas:'';

                if ($kls==1) {
                    $umk = isset($bpjs->kelas)?$bpjs->kelas:'0';
                } else {
                    $umk = isset($r->umk)?$r->umk:'0';
                }

                $tks    = $bpjs->tks * $umk / 100;
                $pks    = $bpjs->pks * $umk / 100;
                $btn    = '<button class="btn btn-primary btn-xs" onclick="edit('.$row->nip.');">Edit</button> <a href="'.base_url('karyawan/form_kk').'/'.$row->nip.'" class="btn btn-xs btn-warning">Lihat KK</a>';
            } else {
                $tks    = 0;
                $pks    = 0;
                $kls    = '-';
                $p      = $this->Karyawan_Model->get_kk($row->nip);
                if ($p) {
                    $btn    = '<button class="btn btn-primary btn-xs" onclick="daftar('.$row->nip.');">Daftar</button> <a href="'.base_url('karyawan/form_kk').'/'.$row->nip.'" class="btn btn-xs btn-warning">Lihat KK</a>';
                } else {
                    $btn    = 'KK Belum diinput. Silahkan input <a href="'.base_url('karyawan/form_kk').'/'.$row->nip.'">disini</a>';
                }
            }
            $q = $this->Main_Model->posisi($row->nip);
            $cabang = isset($q->cabang) ? $q->cabang : '';
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $cabang,
                $kls,
                $this->Main_Model->uang($tks),
                $this->Main_Model->uang($pks),
                $this->Main_Model->uang($tks+$pks),
                $btn
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function process_bpjs_ks()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->post("id");
        $nip    = $this->input->post("nip");
        $kelas  = $this->input->post("kelas");
        // $no_kk   = $this->Karyawan_Model->get_kk($nip);

        if (empty($nip) || empty($kelas)) {
            $result = array('status'=>'false');
        } else {
            $data = array(
                // 'no_kk'  => $no_kk->no_kk,
                'nip'   => $nip,
                'kelas' => $kelas
                );

            $this->Penggajian_Model->process_bpjs_ks($data, $id);
            $result = array('status'=>'true');
        }

        echo json_encode($result);
    }

    function kelas_bpjs($nip)
    {
        $this->Main_Model->get_login();
        $data   = $this->Penggajian_Model->bpjs_ks_nip($nip);

        echo json_encode($data);
    }

    function nip_id()
    {
        $id     = $this->input->get("id");
        $data   = $this->Penggajian_Model->nip_id($id);
        echo json_encode($data);
    }

    function bbmp()
    {
        $menu   = array(
            'menu2' => 'active open selected',
            'style' => '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">'
        );
        $url_del= base_url('penggajian/delete_msbbmp');
        $footer = array(
            'javascript' => '<script type="text/javascript">
            					function reset(id)
            					{
            						if(id==1)
            						{
            							$("#notif").html("");
            							$(".kosong").val("");
            							$(".select2").val("").trigger("change");
            						}
            						else if(id==2)
            						{
            							$("#notif").html("");
            						}
            						else
            						{
	            						$(".kosong").val("");
	            						$(".alert").addClass("hidden");
            							$(".select2").val("").trigger("change");
	            					}
            					}
            					function notif(tipe,message)
            					{
            						if(tipe=="error")
            						{
            							var data = "<div class=\"alert alert-danger\">"+message+"</div>";
            						}
            						else
            						{
            							var data = "<div class=\"alert alert-success\">"+message+"</div>";	
            						}
            						$("#notif").html(data);
            					}
    							function load_table()
    							{
    								$.ajax({
    									url 	: "'.base_url('penggajian/view_ms_bbmp').'",
    									success : function(data){
    										$("#myTable").html(data);
    										$("#bbm_tabel").DataTable({
        										responsive: true
    										});
											$("#bbp_tabel").DataTable({
        										responsive: true
    										});
    									},
    									error 	: function(jqXHR,textStatus,errorThrown){
    										bootbox.alert("Oops Something Went Wrong!");
    									}
    								})
    							}
    							load_table();

    							function get_id(id)
        						{
							    	reset(2);
							    	id = {"id" : id}
							    	$.ajax({
							    		url 	: "' . base_url('penggajian/bbmp_id') . '",
							    		type 	: "GET",
							    		data 	: id,
							    		dataType: "JSON",
							    		success : function(data){
							    			$("#myModal").modal();
							    			$("#id").val(data.id_bbmp);
							    			$("#tipe").val(data.tipe);
							    			$("#jab_bbmp").val(data.jab).trigger("change");
							    			$("#nominal").val(data.jumlah);
							    		},
							    		error 	: function(jqXHR, textStatus, errorThrown){
							    			bootbox.alert("Oops Something Went Wrong!");
							    		}
							    	});
							    }
							 	'.$this->Main_Model->default_select2().
                                  $this->Main_Model->default_delete_data($url_del).'
    							function save()
        						{
			    					$.ajax({
			    						url 	: "'.base_url('penggajian/process_msbbmp').'",
			    						data 	: $("#msbbmp").serialize(),
			    						type 	: "POST",
			    						dataType: "JSON",
			    						success : function(data){
			    							if(data.status=="true")
			    							{
			    								reset(1); load_table(); 
			    								'.$this->Main_Model->notif200().'
			    							}
			    							else
			    							{
			    								reset(2); 
			    								'.$this->Main_Model->notif400().'
			    							}
			    						},	
			    						error 	: function(jqXHR,textStatus,errorThrown){
			    							bootbox.alert("Oops Something Went Wrong!");
			    						}
			    					});
				    			}
							</script>',
            'js' => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') . '"></script>'
        );
        $data   = array(
            'opt_bbmp' => $this->Penggajian_Model->opt_bbmp()
            );

        $this->load->view('template/header', $menu);
        $this->load->view('penggajian/bbmp', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_ms_bbmp()
    {
        $this->Main_Model->get_login();
        $bbm = $this->Penggajian_Model->view_ms_bbm();
        $bbp = $this->Penggajian_Model->view_ms_bbp();

        $tabel = '<ul class="nav nav-tabs"><li class="active"><a href="#tab_1_1" data-toggle="tab"> BBM </a></li><li><a href="#tab_1_2" data-toggle="tab"> BBP </a></li></ul><div class="tab-content"><div class="tab-pane fade active in" id="tab_1_1"><table class="table table-striped" id="bbm_tabel">';
        $tabel .= '<thead><tr><th>No.</th><th>Posisi</th><th>Biaya Akomodasi BBM/Bulan</th><th>Action</th></tr></thead><tbody>';
        $i=1;
        foreach ($bbm as $bbm) {
            $tabel .= '<tr><td>'.$i++.'</td><td>'.$bbm->jab.'</td><td>'.$this->Main_Model->uang($bbm->jumlah).'</td><td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu">
				                    <li>
				                        <a href="javascript:;" onclick="get_id(' . $bbm->id_bbmp . ');">
				                            <i class="icon-edit"></i> Update </a>
				                    </li>
				                    <li>
				                        <a href="javascript:;" onclick="delete_data(' . $bbm->id_bbmp . ');">
				                            <i class="icon-delete"></i> Delete </a>
				                    </li>
				                </ul>
				        </div></td></tr>';
        }
        $tabel .='</tbody></table></div><div class="tab-pane fade" id="tab_1_2">';
        $tabel .= '<table class="table table-striped" id="bbp_tabel">';
        $tabel .= '<thead><tr><th>No.</th><th>Posisi</th><th>Biaya BBP</th><th>Action</th></tr></thead><tbody>';
        $i=1;
        foreach ($bbp as $bbp) {
            $tabel .= '<tr><td>'.$i++.'</td><td>'.$bbp->jab.'</td><td>'.$this->Main_Model->uang($bbp->jumlah).'</td><td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu">
				                    <li>
				                        <a href="javascript:;" onclick="get_id(' . $bbp->id_bbmp . ');">
				                            <i class="icon-edit"></i> Update </a>
				                    </li>
				                    <li>
				                        <a href="javascript:;" onclick="delete_data(' . $bbp->id_bbmp . ');">
				                            <i class="icon-delete"></i> Delete </a>
				                    </li>
				                </ul>
				        </div></td></tr>';
        }
        $tabel .='</tbody></table></div></div><div class="clearfix margin-bottom-20"> </div>';

        echo $tabel;
    }

    function bbmp_id()
    {
        $this->Main_Model->get_login();
        $id = $this->input->get("id");
        $data = $this->Penggajian_Model->bbmp_id($id);

        echo json_encode($data);
    }

    function process_msbbmp()
    {
        $this->Main_Model->get_login();
        $id         = $this->input->post("id");
        $tipe       = $this->input->post("tipe");
        $jab        = $this->input->post("jab_bbmp");
        $nominal    = $this->input->post("nominal");

        if ($tipe=="" || $nominal=="") {
            $result = array('status'=>'false','message'=>'Please Complete this form!');
        } else {
            $q      = $this->db->query("SELECT * FROM ms_bbmp WHERE jab = '$jab' AND tipe='$tipe'");
            if ($q->num_rows()>0 && $id=="") {
                $result = array('status'=>'false','message'=>'Jabatan ini sudah ada');
            } else {
                $data = array(
                    'jab'       => $jab,
                    'jumlah'    => $nominal,
                    'tipe'      => $tipe
                    );

                $this->Penggajian_Model->process_msbbmp($data, $id);
                $result = array('status'=>'true','message'=>'Success!');
            }
        }

        echo json_encode($result);
    }

    function delete_msbbmp()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->post("id");
        $this->Penggajian_Model->delete_msbbmp($id);
    }

    function aturan_perdin()
    {
        $url_load   = base_url('penggajian/view_ms_perdin');
        $url_save   = base_url('penggajian/process_msperdin');
        $url_get    = base_url('penggajian/msperdin_id');
        $url_del    = base_url('penggajian/delete_msperdin');
        $onsuccess  = '
						$("#tb_uangsaku").DataTable({
							responsive: true
						});
						$("#tb_akomodasi").DataTable({
							responsive: true
						});
		';
        $menu       = array(
            'menu2' => 'active open selected',
            'style' => '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">'
        );
        
        $footer     = array(
            'javascript' => '<script type="text/javascript">
            					function reset()
            					{
            						$(".kosong").val("");
            						$(".select2").val("").trigger("change");
            					}
            					'.$this->Main_Model->default_select2()
                                 .$this->Main_Model->notif()
                                 .$this->Main_Model->default_loadtable($url_load, 'load_table()', 'myTable', 'dataTables-example', $onsuccess)
                                 .$this->Main_Model->post_data($url_save, 'save()', '$("#msperdin").serialize()', '
            					 	if(data.status=="true")
            					 	{
            					 		load_table();
            					 		reset()
            					 	}
            					 	notif(data.message);')
                                 .$this->Main_Model->get_data(
                                     $url_get,
                                     'get_id(id)',
                                     '$("#myModal").modal();
							    			$("#id").val(data.id_ms_perdin);
							    			$("#tipe").val(data.tipe);
							    			$("#jab").val(data.jab).trigger("change");
							    			$("#nominal").val(data.nominal);'
                                 )
                                 .$this->Main_Model->default_delete_data($url_del).'
							</script>',
            'js'    => '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') . '"></script>'
        );
        $data   = array(
            'opt_bbmp' => $this->Penggajian_Model->opt_bbmp()
            );

        $this->load->view('template/header', $menu);
        $this->load->view('penggajian/aturan_perdin', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_ms_perdin()
    {
        $this->Main_Model->get_login();
        $uangsaku   = $this->Penggajian_Model->view_ms_perdin(1);
        $akomodasi  = $this->Penggajian_Model->view_ms_perdin(2);

        $tabel = '<ul class="nav nav-tabs"><li class="active"><a href="#tab_1_1" data-toggle="tab"> Uang Saku </a></li><li><a href="#tab_1_2" data-toggle="tab"> Max Akomodasi </a></li></ul><div class="tab-content"><div class="tab-pane fade active in" id="tab_1_1"><table class="table table-striped" id="tb_uangsaku">';
        $tabel .= '<thead><tr><th>No.</th><th>Posisi</th><th>Biaya Uang Saku/Hari</th><th>Action</th></tr></thead><tbody>';
        $i=1;
        foreach ($uangsaku as $uangsaku) {
            $tabel .= '<tr><td>'.$i++.'</td><td>'.$uangsaku->jab.'</td><td>'.$this->Main_Model->uang($uangsaku->nominal).'</td><td>
						'.$this->Main_Model->default_action($uangsaku->id_ms_perdin).'</td></tr>';
        }
        $tabel .='</tbody></table></div><div class="tab-pane fade" id="tab_1_2">';
        $tabel .= '<table class="table table-striped" id="tb_akomodasi">';
        $tabel .= '<thead><tr><th>No.</th><th>Posisi</th><th>Biaya Max Akomodasi</th><th>Action</th></tr></thead><tbody>';
        $i=1;
        foreach ($akomodasi as $akomodasi) {
            $tabel .= '<tr><td>'.$i++.'</td><td>'.$akomodasi->jab.'</td><td>'.$this->Main_Model->uang($akomodasi->nominal).'</td><td>
						'.$this->Main_Model->default_action($akomodasi->id_ms_perdin).'</td></tr>';
        }
        $tabel .='</tbody></table></div></div><div class="clearfix margin-bottom-20"> </div>';

        echo $tabel;
    }
    
    function process_msperdin()
    {
        $this->Main_Model->get_login();
        $id         = $this->input->post("id");
        $tipe       = $this->input->post("tipe");
        $jab        = $this->input->post("jab");
        $nominal    = $this->input->post("nominal");

        if ($tipe=="" || $nominal=="") {
            $result = array('status'=>'false','message'=>'Please Complete this form!');
        } else {
            $q      = $this->db->query("SELECT * FROM ms_perdin WHERE jab = '$jab' AND tipe='$tipe' AND status=1");
            if ($q->num_rows()>0 && $id=="") {
                $result = array('status'=>'false','message'=>'Jabatan ini sudah ada');
            } else {
                $data = array(
                    'jab'       => $jab,
                    'nominal'   => $nominal,
                    'tipe'      => $tipe
                    );

                $this->Penggajian_Model->process_msperdin($data, $id);
                $result = array('status'=>'true','message'=>'Success!');
            }
        }

        echo json_encode($result);
    }

    function msperdin_id($id)
    {
        $this->Main_Model->get_login();
        $data   = $this->Penggajian_Model->msperdin_id($id);

        echo json_encode($data);
    }

    function delete_msperdin()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->post("id");
        $data   = array('status'=>0);
        $this->Penggajian_Model->delete_msperdin($id, $data);
    }
}
