<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setup_Gaji extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->guest_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2();

		$header = array(
			'menu' => $this->Main_Model->menu_user('0', '0', '144'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_summernote()
				      .$this->Main_Model->style_select2()
		);


		$job = $this->Karyawan_Model->jab_setup_gaji();
		$job_opt = array();
		if (!empty($job)) {
			foreach ($job as $row) {
				$job_opt[$row->jab] = $row->jab;
			}
		}

		$status = array('KONTRAK' => 'KONTRAK', 'TRAINING' => 'TRAINING');

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'jab' => $job_opt,
			'status' => $status,
			'penutup' => $this->Main_Model->close_page()
		);

		$this->load->view('akun/header', $header);
		$this->load->view('gaji/setup_gaji',$data);
	}

	function view_setup_gaji()
	{
		$this->Main_Model->all_login();
		$data = $this->Main_Model->view_by_id('tb_setup_gaji', array(), 'result');
		$template = $this->Main_Model->tbl_temp('tb_setup_gaji');
		$this->table->set_heading('No', 'Jabatan', 'Status', 'Upah Pokok', 'Tunjangan Jabatan','Pengh. Masa Kerja','Tunj Tidak Tetap', 'Tunjangan Kemahalan','Ins Presensi','Action');

		$no = 1;
		foreach ($data as $row) {
			$delete = array('event' => "delete_data('".$row->id."')", 'label' => 'Delete');
			$edit = array('event' => "get_id('".$row->id."')", 'label' => 'Update');
			$action = array($edit, $delete);

			$this->table->add_row(
				$no++,
				$row->jab,
				$row->category,
				$row->upah_pokok,
				$row->tunj_jabatan,
				$row->pmasa_kerja,
				$row->tunj_tdktetap,
				$row->tunj_kemahalan,
				$row->ins_presensi,
				$this->Main_Model->action($action)
			);
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_setup_gaji()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$jab = $this->input->post('jab');
		$status = $this->input->post('status');
		$upah_pokok = $this->input->post('upah_pokok');
		$tunj_jabatan = $this->input->post('tunj_jabatan');
		$pmasa_kerja = $this->input->post('pmasa_kerja');
		$tunj_tdktetap = $this->input->post('tunj_tdktetap');
		$ins_presensi = $this->input->post('ins_presensi');
		$username = $this->session->userdata('username');

		if ($jab == '' || $status == '') {
			$message = '';
			$status = FALSE;
			if ($jab == '') $message .= 'Field Jabatan Harus diisi! <br>';
			if ($status == '') $message .= 'Field Status Harus diisi! <br>';
		} else {
			$insert = ($id != '') ? 'update_at' : 'insert_at';
			$user = ($id != '') ? 'user_update' : 'user_insert';

			$data = array(
				'jab' => $jab,
				'category' => $status,
				'upah_pokok' => $upah_pokok,
				'tunj_jabatan' => $tunj_jabatan,
				'pmasa_kerja' => $pmasa_kerja,
				'tunj_tdktetap' => $tunj_tdktetap,
				'ins_presensi' => $ins_presensi,
				$insert => date('Y-m-d H:i:s'), 
				$user => $username
			);

			$where = array('jab' => $jab, 'category' => $status);
			$exists = $this->Main_Model->view_by_id('tb_setup_gaji', $where, 'row');
			if (!empty($exists)) {
				$status = FALSE;
				$message = 'Gagal menyimpan, Jabatan dan Status sudah diinput!';
			} else {
				$condition = ($id != '') ? array('id' => $id) : array();
				$simpan = $this->Main_Model->process_data('tb_setup_gaji', $data, $condition);
				if ($simpan > 0) {
					$status = TRUE;
					$message = 'Success!';
				} else {
					$status = FALSE;
					$message = 'Gagal menyimpan data!';
				}
			}
		}
		$res = array('status' => $status, 'message' => $message);
		echo json_encode($res);
	}

	function id_setup_gaji($id='')
	{
		$this->Main_Model->all_login();
		$where = array('id' => $id);
		$data = $this->Main_Model->view_by_id('tb_setup_gaji', $where, 'row');

		echo json_encode($data);
	}

	function delete_setup_gaji($id='')
	{
		$this->Main_Model->all_login();
		$where = array('id' => $id);
		$hapus = $this->Main_Model->delete_data('tb_setup_gaji', $where);
		if ($hapus > 0) {
			$status = TRUE;
			$message = 'Success!';
		} else {
			$status = FALSE;
			$message = 'Gagal menghapus!';
		}
		$res = array('status' => $status, 'message' => $message);
		echo json_encode($res);
	}
}

/* End of file setup_gaji.php */
/* Location: ./application/controllers/setup_gaji.php */ ?>