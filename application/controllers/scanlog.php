<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scanlog extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
		$this->load->model('Absensi_Model');
		$this->load->model('Api_Model');
	}

	function index()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$template = ($cabang == '') ? 'template/header' : 'akun/header';

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
			);

		$this->load->view($template, $header);
		$this->load->view('absensi/form_scanlog',$data);
	}

	function view_scanlog() 
	{
		$this->Main_Model->all_login();
		$date = $this->input->get('date');
		$tgl = $this->Main_Model->convert_tgl($date);
		$data = $this->Absensi_Model->view_scanlog($tgl);

		$template = $this->Main_Model->tbl_temp('tb_scanlog');
		$this->table->set_heading('No', 'NIP', 'PIN', 'Nama', 'Cabang', 'Scan', 'Mesin');
		$no = 1;
		foreach($data as $row) {
			$this->table->add_row(
				$no++,
				$row->nip,
				$row->pin,
				$row->nama,
				$row->cabang,
				$row->tanggal,
				$row->mesin
				);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function tarik()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$template = ($cabang == '') ? 'template/header' : 'akun/header';

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$q = $this->Main_Model->view_by_id('ms_mesin', null, 'result');
		$mesin = array();
		foreach($q as $row) {
			$mesin[$row->id] = $row->description;
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'mesin' => $mesin
			);

		$this->load->view($template, $header);
		$this->load->view('absensi/form_tarik_scanlog',$data);
	}

	function log_mesin()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$template = ($cabang == '') ? 'template/header' : 'akun/header';

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
			);

		$this->load->view($template, $header);
		$this->load->view('absensi/form_log_mesin',$data);
	}

	function view_log_mesin() 
	{
		$this->Main_Model->all_login();
		$date = $this->input->get('date');
		$tgl = $this->Main_Model->convert_tgl($date);
		$data = $this->Api_Model->view_log_mesin($tgl);

		$template = $this->Main_Model->tbl_temp('tb_scanlog');
		$this->table->set_heading('No', 'Mesin', 'Waktu', 'Tipe Scan', 'Keterangan');
		$no = 1;
		foreach($data as $row) {
			$this->table->add_row(
				$no++,
				$row->description,
				$row->datetime,
				$row->scan_log,
				$row->keterangan
				);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function scandate()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$template = ($cabang == '') ? 'template/header' : 'akun/header';

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
			);

		$this->load->view($template, $header);
		$this->load->view('absensi/form_scandate',$data);
	}

	function view_scandate()
	{
		$this->Main_Model->all_login();
		$date = $this->input->get('date');
		$tgl = $this->Main_Model->convert_tgl($date);
		$data = $this->Absensi_Model->view_scandate($tgl);

		$template = $this->Main_Model->tbl_temp('tb_scanlog');
		$this->table->set_heading('#', 'NIK', 'Nama', 'Cabang', 'Scan');
		$no = 1;
		if ($date != '') {
			foreach($data as $row) {
				$this->table->add_row(
					$no++,
					$row->nip,
					$row->nama,
					$row->cabang,
					$row->scan
					);
			}
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function reproses()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$template = ($cabang == '') ? 'template/header' : 'akun/header';

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$q = $this->Main_Model->kary_cabang(null, 1);
		$nip[null] = 'Semua Karyawan';
		if (!empty($q)) {
			foreach ($q as $row) {
				$nip[$row->nip] = $row->nip.' '.$row->nama;
			}
		}

		$data = array(
			'nip' => $nip,
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
			);

		$this->load->view($template, $header);
		$this->load->view('absensi/proses_absensi',$data);
	}
}

/* End of file scanlog.php */
/* Location: ./application/controllers/scanlog.php */ ?>