<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Download_File extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Rekrutmen_Model', '', TRUE);
		$this->load->model('Rest_Model', '', TRUE);
		$this->load->model('Rekap_Absensi_Model', '', TRUE);
		$this->load->model('Shift_Model', '', TRUE);
		$this->load->model('Penggajian_Model', '', TRUE);
	}

	function index()
	{
		show_404();
	}

	function file_karyawan()
	{
		$this->Main_Model->get_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $this->Main_Model->menu_admin('0','0','69'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$option = $this->Main_Model->view_by_id('ms_file', null, 'result');
		foreach ($option as $row) {
			$opt[$row->id] = $row->text;
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'list' => $opt
			);

		$this->load->view('template/header', $header);
		$this->load->view('karyawan/file_karyawan',$data);
	}

	function view_file($id='')
	{
		$this->Main_Model->get_login();
		$q = $this->Main_Model->view_by_id('ms_file', array('id' => $id), 'row');
		if($q) {
			$table = $q->table;
			$column = $q->column;
			$path = $q->path;

			$data = $this->Karyawan_Model->view_file($table, $column);
			$template = $this->Main_Model->tbl_temp('tbl_file');
			$this->table->set_heading('No', 'Nama', 'File');
			$no = 1;
			foreach($data as $row) {
				$this->table->add_row(
					$no++,
					$row->nama,
					'<a href="'.base_url($path).'/'.$row->$column.'" target="_blank">'.$row->$column.'</a>'
					);
			}
			$this->table->set_template($template);
			echo $this->table->generate();
		}
	}

	function cari_nip($id='')
	{
		$this->Main_Model->all_login();
		$data = $this->Rekrutmen_Model->cari_nip($id);

		echo json_encode($data);
	}

	function file_finalisasi()
	{
		$this->Main_Model->all_login();
		$id_file = $this->input->get('id_file');
		$file_cabang = $this->input->get('file_cabang');
		$tgl_mulai = $this->input->get('tgl_mulai');
		$tgl_akhir = $this->input->get('tgl_akhir');
		$nip = $this->input->get('file_nip');
		$file = $this->input->get('file');

		switch ($file) {
			case '1':
				$result = $this->file_ojt($id_file, $file_cabang, $tgl_mulai, $tgl_akhir, $nip);
				break;

			case '2':
				$result = $this->file_pernyataan();
				break;

			case '3':
				$result = $this->file_pertanggungjawaban();
				break;

			case '4':
				$result = $this->file_pasal();
				break;

			case '5':
				$result = $this->file_pkwt($id_file, $file_cabang, $tgl_mulai, $tgl_akhir, $nip);
				break;
			
			default:
				$result = '';
				break;
		}

		$result;

		// $this->file_ojt($id_file, $file_cabang, $tgl_mulai, $tgl_akhir, $nip);

		// $this->file_pernyataan();

		// $this->file_pertanggungjawaban();

		// $this->file_pasal();

		// $this->file_pkwt($id_file, $file_cabang, $tgl_mulai, $tgl_akhir, $nip);
	}

	function file_ojt($id='', $cabang='', $tgl_mulai='', $tgl_akhir='', $nip='')
	{
		$this->Main_Model->all_login();
		$data = $this->Rekrutmen_Model->data_ojt($id, $nip);

		$tgl_mulai = $this->Main_Model->convert_tgl($tgl_mulai);
		$tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

		$diff = $this->Main_Model->diff_month($tgl_mulai, $tgl_akhir);
		$terbilang = $this->Main_Model->terbilang($diff, 2);

		$hari = $this->convert_hari($tgl_mulai);
		$tgl_mulai = $this->Main_Model->tgl_indo($tgl_mulai);
		$tgl_akhir = $this->Main_Model->tgl_indo($tgl_akhir);

		$kop_surat = $this->Main_Model->kop_surat();

		$arr = array(
			'nama' => isset($data->nama) ? $data->nama : '',
			'jk' => isset($data->gend) ? $data->gend : '',
			'tgl_lahir' => isset($data->tgl_lahir) ? $this->Main_Model->tgl_indo($data->tgl_lahir) : '',
			'alamat' => isset($data->alamat) ? $data->alamat : '',
			'tgl_mulai' => $tgl_mulai,
			'tgl_akhir' => $tgl_akhir,
			'hari' => $hari,
			'cabang' => isset($data->cabang) ? $data->cabang : '',
			'description' => isset($data->description) ? $data->description : '',
			'diff' => $diff,
			'terbilang' => $terbilang,
			'kop_surat' => $kop_surat
		);

		$filename = 'OJT '.$arr['nama'].'.pdf';

		$this->load->view('file_download/ojt', $arr);

		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'letter', 'portrait');

	  	// $this->dompdf->load_html($html);
	  	// $this->dompdf->set_paper("letter", "portrait" ); 
	  	// $this->dompdf->render();
	  	// $this->dompdf->stream($filename, array('Attachment'=>0));
	}

	function file_pkwt($id='', $cabang='', $tgl_mulai='', $tgl_akhir='', $nip='')
	{
		$this->Main_Model->all_login();
		$data = $this->Rekrutmen_Model->data_ojt($id, $nip);

		$tgl_mulai = $this->Main_Model->convert_tgl($tgl_mulai);
		$tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

		$diff = $this->Main_Model->diff_month($tgl_mulai, $tgl_akhir);
		$terbilang = $this->Main_Model->terbilang($diff, 2);

		$hari = $this->convert_hari($tgl_mulai);
		$tgl_mulai = $this->Main_Model->tgl_indo($tgl_mulai);
		
		$tgl_akhir = $this->Main_Model->tgl_indo($tgl_akhir);

		$kop_surat = $this->Main_Model->kop_surat();

		$arr = array(
			'nama' => isset($data->nama) ? $data->nama : '',
			'jk' => isset($data->gend) ? $data->gend : '',
			'tgl_lahir' => isset($data->tgl_lahir) ? $this->Main_Model->tgl_indo($data->tgl_lahir) : '',
			'alamat' => isset($data->alamat) ? $data->alamat : '',
			'tgl_mulai' => $tgl_mulai,
			'tgl_akhir' => $tgl_akhir,
			'hari' => $hari,
			'cabang' => isset($data->cabang) ? $data->cabang : '',
			'description' => isset($data->description) ? $data->description : '',
			'diff' => $diff,
			'terbilang' => $terbilang,
			'kop_surat' => $kop_surat
		);

		$filename = 'PKWT '.$arr['nama'].'.pdf';

		$this->load->view('file_download/pkwt', $arr);

		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'letter', 'portrait');

		// $html = $this->output->get_output();
	 //  	$this->load->library('Dompdf_gen');

	 //  	$this->dompdf->load_html($html);
	 //  	$this->dompdf->set_paper("letter", "portrait" ); 
	 //  	$this->dompdf->render();
	 //  	$this->dompdf->stream($filename, array('Attachment'=>0));
	}

	function file_pernyataan()
	{
		$this->Main_Model->all_login();
		$this->load->view('file_download/surat_pernyataan');

		// $html = $this->output->get_output();
	 //  	$this->load->library('Dompdf_gen');

	 //  	$this->dompdf->load_html($html);
	 //  	$this->dompdf->set_paper("letter", "portrait" ); 
	 //  	$this->dompdf->render();
	 //  	$this->dompdf->stream($filename, array('Attachment'=>0));
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, 'Surat Pernyataan', true, 'letter', 'portrait');
	}

	function file_pertanggungjawaban()
	{
		$this->Main_Model->all_login();
		$this->load->view('file_download/surat_pertanggungjawaban');

		// $html = $this->output->get_output();
	 //  	$this->load->library('Dompdf_gen');

	 //  	$this->dompdf->load_html($html);
	 //  	$this->dompdf->set_paper("letter", "portrait" ); 
	 //  	$this->dompdf->render();
	 //  	$this->dompdf->stream($filename, array('Attachment'=>0));
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, 'Surat Pertanggungjawaban', true, 'letter', 'portrait');
	}

	function file_pasal()
	{
		$this->Main_Model->all_login();
		$this->load->view('file_download/pasal');

		// $html = $this->output->get_output();
	 //  	$this->load->library('Dompdf_gen');

	 //  	$this->dompdf->load_html($html);
	 //  	$this->dompdf->set_paper("letter", "portrait" ); 
	 //  	$this->dompdf->render();
	 //  	$this->dompdf->stream($filename, array('Attachment'=>0));
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, 'Pasal pasal', true, 'letter', 'portrait');
	}

	function convert_hari($param='')
	{
		$param = date_create($param);
		$param = date_format($param, 'w');
		switch ($param) {
			case '0':
				$hari = 'Minggu';
				break;
			case '1':
				$hari = 'Senin';
				break;
			case '2':
				$hari = 'Selasa';
				break;
			case '3':
				$hari = 'Rabu';
				break;
			case '4':
				$hari = 'Kamis';
				break;
			case '5':
				$hari = "Jum'at";
				break;
			case '6':
				$hari = "Sabtu";
				break;
			default:
				$hari = '';
				break;
		}

		return $hari;
	}

	function kontrak_pkwt() 
	{
		$this->Main_Model->all_login();
		// $tgl_mulai = $this->input->get('tgl_mulai');
		// $tgl_akhir = $this->input->get('tgl_akhir');
		$nip = $this->input->get('nip');
		$data = $this->Rekrutmen_Model->pwkt_kontrak($nip);

		// $tgl_mulai = $this->Main_Model->convert_tgl($tgl_mulai);
		// $tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

		$tgl_mulai = isset($data->tgl_awal) ? $data->tgl_awal : '';
		$tgl_akhir = isset($data->tgl_akhir) ? $data->tgl_akhir : '';

		$diff = $this->Main_Model->diff_month($tgl_mulai, $tgl_akhir);
		$terbilang = $this->Main_Model->terbilang($diff, 2);

		$hari = $this->convert_hari($tgl_mulai);
		$tgl_mulai = $this->Main_Model->tgl_indo($tgl_mulai);
		$tgl_akhir = $this->Main_Model->tgl_indo($tgl_akhir);

		$kop_surat = $this->Main_Model->kop_surat();

		$arr = array(
			'nama' => isset($data->nama) ? $data->nama : '',
			'jk' => isset($data->gend) ? $data->gend : '',
			'tgl_lahir' => isset($data->tgl_lahir) ? $this->Main_Model->tgl_indo($data->tgl_lahir) : '',
			'alamat' => isset($data->alamat) ? $data->alamat : '',
			'tgl_mulai' => $tgl_mulai,
			'tgl_akhir' => $tgl_akhir,
			'hari' => $hari,
			'cabang' => isset($data->cabang) ? $data->cabang : '',
			'description' => isset($data->description) ? $data->description : '',
			'diff' => $diff,
			'terbilang' => $terbilang,
			'kop_surat' => $kop_surat
		);

		$filename = 'PKWT '.$arr['nama'].'.pdf';

		$this->load->view('file_download/pkwt', $arr);

		// $html = $this->output->get_output();
	 //  	$this->load->library('Dompdf_gen');

	 //  	$this->dompdf->load_html($html);
	 //  	$this->dompdf->set_paper("letter", "portrait" ); 
	 //  	$this->dompdf->render();
	 //  	$this->dompdf->stream($filename, array('Attachment'=>0));
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'letter', 'portrait');
	}

	function kontrak_ojt()
	{
		$this->Main_Model->all_login();
		$nip = $this->input->get('nip');
		$data = $this->Rekrutmen_Model->pwkt_kontrak($nip);

		// $tgl_mulai = $this->Main_Model->convert_tgl($tgl_mulai);
		// $tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

		$tgl_mulai = isset($data->tgl_awal) ? $data->tgl_awal : '';
		$tgl_akhir = isset($data->tgl_akhir) ? $data->tgl_akhir : '';

		$diff = $this->Main_Model->diff_month($tgl_mulai, $tgl_akhir);
		$terbilang = $this->Main_Model->terbilang($diff, 2);

		$hari = $this->convert_hari($tgl_mulai);
		$tgl_mulai = $this->Main_Model->tgl_indo($tgl_mulai);
		$tgl_akhir = $this->Main_Model->tgl_indo($tgl_akhir);

		$kop_surat = $this->Main_Model->kop_surat();

		$arr = array(
			'nama' => isset($data->nama) ? $data->nama : '',
			'jk' => isset($data->gend) ? $data->gend : '',
			'tgl_lahir' => isset($data->tgl_lahir) ? $this->Main_Model->tgl_indo($data->tgl_lahir) : '',
			'alamat' => isset($data->alamat) ? $data->alamat : '',
			'tgl_mulai' => $tgl_mulai,
			'tgl_akhir' => $tgl_akhir,
			'hari' => $hari,
			'cabang' => isset($data->cabang) ? $data->cabang : '',
			'description' => isset($data->description) ? $data->description : '',
			'diff' => $diff,
			'terbilang' => $terbilang,
			'kop_surat' => $kop_surat
		);

		$filename = 'OJT '.$arr['nama'].'.pdf';

		$this->load->view('file_download/ojt', $arr);

		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'letter', 'portrait');

	  	// $this->dompdf->load_html($html);
	  	// $this->dompdf->set_paper("letter", "portrait" ); 
	  	// $this->dompdf->render();
	  	// $this->dompdf->stream($filename, array('Attachment'=>0));
	}

	function download_rekap_absen()
	{
		$nip = $this->input->get('nip');
        $date_start = $this->input->get('tgl_awal');
        $date_end = $this->input->get('tgl_akhir');

        $b = $this->Rest_Model->biodata($nip);
        // biodata
        $nama = isset($b->nama) ? $b->nama : '';
        $cabang = isset($b->cabang) ? $b->cabang : '';
        $divisi = isset($b->divisi) ? $b->divisi : '';

        $date_start = api_tgl($date_start);
        $date_end = api_tgl($date_end);

        $periode = tgl_indo($date_start).' sd '.tgl_indo($date_end);

        // range tgl
        $date_range = date_range($date_start, $date_end);
        $jumlah_range = count($date_range);

        // range tgl header 
        $header_tgl = array();
        $uang_makan = array();
        $uang_denda = array();
        $uang_lembur = array();
        if (!empty($date_range)) {
        	for ($i = 0; $i < $jumlah_range; $i++) {
        		$tanggal = explode(' s/d ', $date_range[$i]);
                $awal = tanggal($tanggal[0]);
                $akhir = tanggal(end($tanggal));
                $header_tgl[$i] = $awal.'<br> s/d <br>'.$akhir;

                // uang makan 
                $um = $this->Rekap_Absensi_Model->uang_makan($tanggal[0], end($tanggal), $nip);
                $uang_makan[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;

                // uang denda
                $ud = $this->Rekap_Absensi_Model->uang_denda($tanggal[0], end($tanggal), $nip);
                $uang_denda[$i] = isset($ud->uang_denda) ? $ud->uang_denda : 0;

                // uang lembur
                $ul = $this->Rekap_Absensi_Model->uang_lembur($tanggal[0], end($tanggal), $nip);
                $uang_lembur[$i] = isset($ul->uang_lembur) ? $ul->uang_lembur : 0;
        	}
        } 

        // cicilan
	    $c = $this->Rekap_Absensi_Model->cicilan($date_start, $date_end, $nip);
	    $cicilan = isset($c->angsuran) ? $c->angsuran : 0;
	    $keterangan = isset($c->keterangan) ? $c->keterangan : '';

	    // bpjs 
	    $b = $this->Rekap_Absensi_Model->bpjs_ketenagakerjaan($nip);
	    $total_k = isset($b->total_k) ? $b->total_k : 0;

	    // detail rekapitulasi
	    $detail = $this->Rekap_Absensi_Model->detail_rekapitulasi($nip, $date_start, $date_end);

        $data = array(
        	'periode' => $periode,
        	'nip' => $nip,
        	'nama' => $nama,
        	'cabang' => $cabang,
        	'divisi' => $divisi,
        	'colspan' => $jumlah_range,
        	'header_tgl' => $header_tgl,
        	'uang_makan' => $uang_makan,
        	'uang_denda' => $uang_denda,
        	'uang_lembur' => $uang_lembur,
        	'cicilan' => $cicilan,
        	'keterangan' => $keterangan,
        	'detail' => $detail,
        	'total_k' => $total_k
        );

        $this->load->view('file_download/rekap_absensi', $data);

        $filename = $nip.' '.$periode;
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
	}

	function view_jadwal_ts()
	{
		// $this->Main_Model->all_login();
		$bulan = $this->input->get('bulan');
		$tahun = $this->input->get('tahun');
		$nip = $this->input->get('nip');

		if (strlen($bulan) == 1) {
			$bulan = '0'.$bulan;
		}

		$tgl_awal = $tahun.'-'.$bulan.'-01';
		$last_day = $this->Main_Model->last_day($bulan, $tahun);
		$tgl_akhir = $last_day;

		$data = $this->Shift_Model->nip_ts($tgl_awal, $tgl_akhir, $nip);

		$tanggal = array();
		while (strtotime($tgl_awal) <= strtotime($tgl_akhir)) {
			$tanggal[] = $tgl_awal;
			$tgl_awal = date("Y-m-d", strtotime("+1 day", strtotime($tgl_awal)));
		}
		
		$html = '<h2>Jadwal '.$bulan.' '.$tahun.'</h2>';
		$html .= '<table border="1" class="jadwal" style="width:100%; font-size:12px;">';
		$html .= '	<tr style="background-color:#3071b5; color:#fff;">
						<td style="padding:2px;">NIK</td>
						<td style="padding:2px;" width="120">NAMA</td>';
			for ($a = 0; $a < count($tanggal); $a++) {
				$html .= '<td style="padding:2px;" align="center"><strong>'.date_format(date_create($tanggal[$a]), 'd').'</strong><br>'.hari(date_format(date_create($tanggal[$a]), 'w')).'</td>';
			}
		$html .=	'</tr>';

			for ($i = 1; $i <= count($data); $i++) {
				$warna = isset($data[$i]['warna']) ? $data[$i]['warna'] : 'silver';
				$nip = isset($data[$i]['nip']) ? $data[$i]['nip'] : '';
				$nama = isset($data[$i]['nama']) ? $data[$i]['nama'] : '';

				$html .= '	<tr>
								<td style="padding:2px;background:'.$warna.';">'.$nip.'</td>
								<td style="padding:2px;background:'.$warna.';">'.$nama.'</td>';
								for ($b = 0; $b < count($tanggal); $b++) {
									$color = isset($data[$i][$tanggal[$b]]['color']) ? $data[$i][$tanggal[$b]]['color'] : 'silver';
									$keterangan = isset($data[$i][$tanggal[$b]]['keterangan']) ? $data[$i][$tanggal[$b]]['keterangan'] : '';
									$jam_masuk = isset($data[$i][$tanggal[$b]]['jam_masuk']) ? $data[$i][$tanggal[$b]]['jam_masuk'] : '';
									$jam_pulang = isset($data[$i][$tanggal[$b]]['jam_pulang']) ? $data[$i][$tanggal[$b]]['jam_pulang'] : '';
									$shift = isset($data[$i][$tanggal[$b]]['shift']) ? $data[$i][$tanggal[$b]]['shift'] : '';

									$html .= '<td style="background-color:'.$color.'" align="center"><span style="cursor: pointer;" title="'.$nama.','.$keterangan.', Masuk : '.$jam_masuk.', Pulang : '.$jam_pulang.'"><strong>'.$shift.'</strong></span></td>';
								}
				$html .=	'</tr>';
			}
		
		$html .= '</table>';

		$data_shift = $this->Shift_Model->master_shift();
		$jumlah = count($data_shift);
		$bagi = $jumlah / 2;
		$master = '';
		if (! empty($data_shift)) {
			$master .= '<h2>Keterangan : </h2><center>';
			$master .= '<table border="0" >';
			$master .= '<tr>';
			$master .= '<td></td>';
			$master .= '<td>Waktu</td>';
			$master .= '<td>Jam Kerja</td>';
			$master .= '<td>Kode</td>';
			$master .= '<td style="padding-left:70px;"></td>';
			$master .= '<td></td>';
			$master .= '<td>Waktu</td>';
			$master .= '<td>Jam Kerja</td>';
			$master .= '<td>Kode</td>';
			$master .= '</tr>';
			$no = 1;
			foreach ($data_shift as $row) {
				if (($no % 2) != 0) {
					$master .= '<tr>';
				}

				$master .= '<td>'.$no++.'</td>';
				$master .= '<td>'.$row->keterangan.'</td>';
				$master .= '<td>'.$row->jam_masuk.' s/d '.$row->jam_pulang.'</td>';
				$master .= '<td style="background-color:'.$row->color.'">'.$row->nama.'</td>';

				if (($no % 2) == 0) {
					$master .= '<td></td>';
				}
			}
			$master .= '</table></center>';
		}

		$data = array(
			'html' => $html,
			'master' => $master
		);

		$this->load->view('file_download/jadwal_ts', $data);
		$filename = 'Jadwal'.$bulan.$tahun.'.pdf';
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
	}

	function download_lembur()
	{
		$this->Main_Model->all_login();
		$cabang = $this->input->get('cabang');
		$date_start = $this->input->get('date_start');
		$date_end = $this->input->get('date_end');

		$date_start = $this->Main_Model->convert_tgl($date_start);
        $date_end = $this->Main_Model->convert_tgl($date_end);

        $periode = tgl_api($date_start).' sd '.tgl_api($date_end);
        $title = 'Rekap Uang Lembur '.$periode;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $tanggal_awal = hari_tgl($date_start);
        $tanggal_akhir = hari_tgl($date_end);

        $karyawan = $this->Rekap_Absensi_Model->kary_lembur($cabang, $date_start, $date_end);
		$tanggal = range_to_date($date_start, $date_end);

		$head = array('TANGGAL', 'SHIFT', 'ABSENSI', 'JAM', 'PER JAM', 'TOTAL', 'KETERANGAN');
		$colspan = count($head);

		$table = '<center><h1 style="color:#265180"> Laporan Uang Lembur </h1></center><br>';
        $table .= '<span style="font-size:14px;">Tanggal <strong>'.$tanggal_awal.'</strong> sampai dengan tanggal <strong>'.$tanggal_akhir.'</strong></span><br><br>';
		$table .= '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
		$table .= '<thead>
					<tr style="background:#265180; color:#fff;">
						<th rowspan="2" style="text-align: center; vertical-align: middle;">#</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">NIK</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">NAMA</th>
			            <th colspan="'.$colspan.'"><center> UANG LEMBUR </center></th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">TANDA TANGAN</th>
			       </tr>';

		$table .= '<tr style="background:#265180; color:#fff;">';
		if (!empty($head)) {
			$baris = 1;
			for ($a = 0; $a < $baris; $a++) {
				for ($i = 0; $i < count($head); $i++) {
					$table .= '<th class="text-center">'.$head[$i].'</th>';
				}
			}
		}
		$table .= '</tr></thead><tbody>';

		$total = 0;
		if (!empty($karyawan)){
			$no = 1;
			$rowspan = 1;
			$subtotal = 0;
			$arr = array();
			foreach ($karyawan as $row) {
				$ovr_nominal = isset($row->ovr_nominal) ? $row->ovr_nominal : 0;
				$id_overtime = isset($row->id_overtime) ? $row->id_overtime : 0;

				$jum = $this->db->query("
							SELECT COUNT(*) AS jumlah
							FROM lembur a
							WHERE a.`nip` = '$row->nip'
							AND a.`tgl` BETWEEN '$date_start' AND '$date_end'")->row();
				$jumlah = isset($jum->jumlah) ? $jum->jumlah : 0;

				if ($id_overtime == 0) {
					$ovr_nominal = 0;
				}

				$span_start = '';
				$span_end = '';

				$color = '#fff';

				if (in_array($row->nip, $arr)) {
					$rowspan++;
					$span_start = '<span style="display:none;">';
					$span_end = '</span>';
					$no--;
				} else {
					$rowspan = 1;
				}

				if ($no % 2 == 0) {
					$color = 'silver';
				}

				$total = $total + $row->lembur;

				$starttime = $row->starttime;
				$endtime = $row->endtime;
				$tgl = $row->tgl;
				$mulai = $tgl.' '.$starttime;
				if (strtotime($starttime) > strtotime($endtime)) {
					$date = date('Y-m-d', strtotime("$tgl + 1 days"));
					$date = $date.' '.$endtime;

					$selesai = $date;
				} else {
					$date = $tgl.' '.$endtime;

					$selesai = $date;
				}

				$query = $this->db->query("
                    SELECT 
                    TIMESTAMPDIFF(HOUR, '$mulai', '$selesai') AS jumlah")->row();

            	$result = isset($query->jumlah) ? $query->jumlah : 0;

				$arr[] = $row->nip;
				$hari = hari(date('w', strtotime($row->tgl)), 2);
				$table .= '<tr style="background:'.$color.';">
								<td>'.$span_start.$no++.$span_end.'</td>
								<td>'.$span_start.$row->nip.$span_end.'</td>
								<td>'.$span_start.$row->nama.$span_end.'</td>
								<td>'.$hari.', '.tanggal($row->tgl).'</td>
								<td>'.$row->shift.'</td>
								<td>'.$row->absensi.'</td>
								<td>'.$result.'</td>
								<td class="text-right">Rp. '.uang($ovr_nominal).'</td>
								<td class="text-right">Rp. '.uang($row->lembur).'</td>
								<td>'.$row->keterangan.'</td>
								<td></td>';

				$table	.=	'</tr>';
				$subtotal = $subtotal + $row->lembur;

				if ($rowspan == $jumlah) {

					$table .= '<tr style="background:'.$color.';">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td class="text-right"><b>Sub Total</b></td>
								<td class="text-right" style="background:cyan;"><b>Rp. '.uang($subtotal).'</b></td>
								<td></td>
								<td></td>';

					$table	.=	'</tr>';
					$subtotal = 0;
				}
			}
		}
		$table .= '<tr style="background:#265180; color:#fff;">
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td class="text-right"><b>Grand Total</b></td>
								<td class="text-right"><b>Rp. '.uang($total).'</b></td>
								<td></td>
								<td></td>';

		$table	.=	'</tr>';
		$table .= '</tbody></table><br><br>';

		$table .= '<table border=0 width=100% align=center>
        <tr><td align="left" colspan=3>Note : Overtime monitoring Rp. 15.000,-/jam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Piket Shift 8 Jam Rp. 80.000,-</td></tr>
        <tr><td align="left" colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Overtime lapangan Rp. 20.000,-/jam&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Piket Hari Raya Rp. 200.000,-</td></tr>
        <tr><td align="left" colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Piket Shift 12 Jam Rp. 125.000,-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lembur OB Rp. 15.000,-/jam</td></tr>
        <!-- <tr><td align="left" colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Piket per shift Rp. 20.000,-&nbsp;(Per 1 April 2012)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Overtime Office Boy Rp. 15.000,-/jam</td></tr> -->
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Max overtime 5 jam per hari</td></tr>
        <tr><td align="left" colspan=3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Overtime dihitung 1 jam setelah jam kerja berakhir (Untuk jam kerja reguler)</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align=center width=30%>Dibuat Oleh,</td><td align=center width=40%>Disetujui Oleh,</td><td align=center width=30%>Diketahui Oleh,</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align=center width=30%>Gita Monicha Rahmawati</td><td align=center width=40%>Franky Yustanto</td><td align=center width=30%>Priyo  Suyono</td></tr>
   		</table>';

		$data = array(
			'html' => $table,
			'title' => 'Download Uang Lembur'
		);

		$this->load->view('file_download/file_lembur', $data);
		$filename = 'Laporan Uang Lembur '.$periode;
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
	}

	function download_uangmakan()
	{
		$this->Main_Model->get_login();
		$cabang = $this->input->get('cabang');
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');

        $date_start = $this->Main_Model->convert_tgl($date_start);
        $date_end = $this->Main_Model->convert_tgl($date_end);

        $periode = tgl_api($date_start).' sd '.tgl_api($date_end);

		$karyawan = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end);
		$tanggal = range_to_date($date_start, $date_end);

		$tanggal_awal = hari_tgl($date_start);
        $tanggal_akhir = hari_tgl($date_end);

		$arr_tgl = array();
		$raw_tgl = array();
		if (!empty($tanggal)) {
			for($a = 0; $a < count($tanggal); $a++) {
				$hari = hari(date('w', strtotime($tanggal[$a])), 2);
				$format_tgl = strtoupper($hari).'<br> '.tanggal($tanggal[$a]);
				$arr_tgl[] = $format_tgl;

				$flag = 0;
				$holiday = $this->Main_Model->view_by_id('hol', array('hol_tgl' => $tanggal[$a]), 'row');
				$week = date_format(date_create($tanggal[$a]), 'w');
				if ($week == 0 || $week == 6 || ! empty($holiday)) {
					$flag = 1;
				}

				$raw_tgl[] = array(
					'tgl' => $tanggal[$a],
					'flag' => $flag
				);
			}
		}

		$colspan = count($arr_tgl);
		$table = '<center><h1 style="color:#265180"> Laporan Uang Makan </h1></center><br>';
        $table .= '<span style="font-size:14px;">Tanggal <strong>'.$tanggal_awal.'</strong> sampai dengan tanggal <strong>'.$tanggal_akhir.'</strong></span><br><br>';
		$table .= '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
		$table .= '<thead><tr>
						<th rowspan="2" style="text-align: center; vertical-align: middle;">#</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">NIK</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">NAMA</th>
			            <th colspan="'.$colspan.'"><center>UANG MAKAN</center></th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;" width="80">JUMLAH UANG</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">TANGGAL AMBIL</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle;">TANDA TANGAN</th>
			       </tr>';

		$table .= '<tr>';
		if (!empty($arr_tgl)) {
			$baris = 1;
			for ($a = 0; $a < $baris; $a++) {
				for ($i = 0; $i < count($arr_tgl); $i++) {
					$color = '';
					if ($raw_tgl[$i]['flag'] == 1) {
						$color = 'style="background:red;"';						
					}

					$table .= '<th class="text-center" '.$color.'><center>'.$arr_tgl[$i].'</center></th>';
				}
			}
		}
		$table .= '</tr></thead><tbody>';


		if (!empty($karyawan)){
			$no = 1;
			$total_uang_makan = 0;
			$total = 0;
			foreach ($karyawan as $row) {
				$table .= '<tr>
								<td>'.$no++.'</td>
								<td>'.$row->nip.'</td>
								<td>'.$row->nama.'</td>';
					// uang makan
					if (!empty($tanggal)) {
						$total_uang_makan = 0;
						for ($i = 0; $i < count($tanggal); $i++) {
							$um = $this->Rekap_Absensi_Model->uang_makan($tanggal[$i], $tanggal[$i], $row->nip);

							$uang_makan[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
							$total_uang_makan = $total_uang_makan + $uang_makan[$i];

							$k = $this->Rekap_Absensi_Model->laporan($row->nip, $tanggal[$i]);
							$laporan = $k;

							$table .= '<td class="text-center">'.$laporan.'</td>';
						}
					}
					$table .= '<td class="text-right">Rp. '.uang($total_uang_makan).'</td>';
					$table .= '<td></td>';
					$table .= '<td></td>';
				$table	.=	'</tr>';
				$total = $total + $total_uang_makan;
			}

			$colspan = $colspan + 3;

			$table .= '<tr style="background:#265180; color:#fff;">
						<td colspan="'.$colspan.'" class="text-right">GRAND TOTAL</td>
						<td class="text-right">Rp. '.uang($total).'</td>
						<td></td>
						<td></td>
					</tr>';
		}

		$table .= '</tbody></table><br><br>';

		$table .= '<table border=0 width=100% align=center>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align=center width=30%>Dibuat Oleh,</td><td align=center width=40%>Disetujui Oleh,</td><td align=center width=30%>Diketahui Oleh,</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align=center width=30%>Gita Monicha Rahmawati</td><td align=center width=40%>Franky Yustanto</td><td align=center width=30%>Priyo  Suyono</td></tr>
   		</table>';

		$data = array(
			'html' => $table,
			'title' => 'Download Uang Makan'
		);

		$this->load->view('file_download/file_lembur', $data);
		$filename = 'Laporan Uang Makan '.$periode;
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
	}

	function download_uangdenda()
	{
		$this->Main_Model->get_login();
		$cabang = $this->input->get('cabang');
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');

        $date_start = $this->Main_Model->convert_tgl($date_start);
        $date_end = $this->Main_Model->convert_tgl($date_end);

        $periode = tgl_api($date_start).' sd '.tgl_api($date_end);
		$karyawan = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end);
		$tanggal = range_to_date($date_start, $date_end);

		$tanggal_awal = hari_tgl($date_start);
        $tanggal_akhir = hari_tgl($date_end);
		
		$arr_tgl = array();
		$raw_tgl = array();
		if (!empty($tanggal)) {
			for($a = 0; $a < count($tanggal); $a++) {
				$hari = hari(date('w', strtotime($tanggal[$a])), 2);
				$format_tgl = strtoupper($hari).'<br> '.tanggal($tanggal[$a]);
				$arr_tgl[] = $format_tgl;

				$flag = 0;
				$holiday = $this->Main_Model->view_by_id('hol', array('hol_tgl' => $tanggal[$a]), 'row');
				$week = date_format(date_create($tanggal[$a]), 'w');
				if ($week == 0 || $week == 6 || ! empty($holiday)) {
					$flag = 1;
				}

				$raw_tgl[] = array(
					'tgl' => $tanggal[$a],
					'flag' => $flag
				);
			}
		}

		$colspan = count($arr_tgl);
		$table = '<center><h1 style="color:#265180"> Laporan Uang Denda </h1></center><br>';
        $table .= '<span style="font-size:14px;">Tanggal <strong>'.$tanggal_awal.'</strong> sampai dengan tanggal <strong>'.$tanggal_akhir.'</strong></span><br><br>';
		$table .= '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
		$table .= '<thead><tr>
						<th rowspan="2">#</th>
			            <th rowspan="2">NIK</th>
			            <th rowspan="2">NAMA</th>
			            <th colspan="'.$colspan.'"><center>UANG DENDA</center></th>
			            <th rowspan="2">JUMLAH UANG</th>
			            <th rowspan="2">KETERANGAN</th> 
			       </tr>';

		$table .= '<tr>';

		if (!empty($arr_tgl)) {
			$baris = 1;
			for ($a = 0; $a < $baris; $a++) {
				for ($i = 0; $i < count($arr_tgl); $i++) {
					$color = '';
					if ($raw_tgl[$i]['flag'] == 1) {
						$color = 'style="background:red;"';						
					}

					$table .= '<th class="text-center" '.$color.'><center>'.$arr_tgl[$i].'</center></th>';
				}
			}
		}
		$table .= '</tr></thead><tbody>';


		if (!empty($karyawan)){
			$no = 1;
			$total = 0;
			foreach ($karyawan as $row) {
				$table .= '<tr>
								<td>'.$no++.'</td>
								<td>'.$row->nip.'</td>
								<td>'.$row->nama.'</td>';
					// uang denda
					if (!empty($tanggal)) {
						$total_uang_denda = 0;
						for ($i = 0; $i < count($tanggal); $i++) {
							$um = $this->Rekap_Absensi_Model->uang_denda($tanggal[$i], $tanggal[$i], $row->nip);

							$uang_denda[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
							$total_uang_denda = $total_uang_denda + $uang_denda[$i];

							$k = $this->Rekap_Absensi_Model->laporan($row->nip, $tanggal[$i]);
							$laporan = $k;

							$string_denda = ($uang_denda[$i] > 0) ? 'Rp. '.uang($uang_denda[$i]) : '-';

							$table .= '<td class="text-center">'.$string_denda.'</td>';
						}
					}
					$table .= '<td class="text-right">Rp. '.uang($total_uang_denda).'</td>';
					$table .= '<td></td>';
				$total = $total + $total_uang_denda;
				$table	.=	'</tr>';
			}

			$colspan = $colspan + 3;

			$table .= '<tr style="background:#265180; color:#fff;">
						<td colspan="'.$colspan.'" class="text-right">GRAND TOTAL</td>
						<td class="text-right">Rp. '.uang($total).'</td>
						<td></td>
					</tr>';
		}

		$table .= '</tbody></table><br><br>';

		$table .= '<table border=0 width=100% align=center>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align=center width=30%>Dibuat Oleh,</td><td align=center width=40%>Disetujui Oleh,</td><td align=center width=30%>Diketahui Oleh,</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align="left" colspan=3>&nbsp;</td></tr>
        <tr><td align=center width=30%>Gita Monicha Rahmawati</td><td align=center width=40%>Franky Yustanto</td><td align=center width=30%>Priyo  Suyono</td></tr>
   		</table>';


		$data = array(
			'html' => $table,
			'title' => 'Download Uang Denda'
		);

		$this->load->view('file_download/file_lembur', $data);
		$filename = 'Laporan Uang Denda '.$periode;
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
	}

	function download_bpjs($cabang='')
	{
		$this->Main_Model->get_login();

		$ms_cabang = $this->Main_Model->view_by_id('ms_cabang', ['id_cab' => $cabang], 'row');
		$nama_cabang = isset($ms_cabang->cabang) ? strtoupper($ms_cabang->cabang) : '';

		$nama_cabang = str_replace('&', 'DAN', $nama_cabang);


		$table = '<center><h1 style="color:#265180"> PERHITUNGAN BPJS KETENAGAKERJAAN GMEDIA '.$nama_cabang.' </h1></center><br>';
		$table .= '	<table class="table table-striped table-bordered" id="tb_rekap">
    				<tr>
						<th rowspan="5" style="text-align: center; vertical-align: middle;">No</th>
						<th rowspan="5" style="text-align: center; vertical-align: middle;">Nama Lengkap Karyawan</th>
						<th rowspan="2" style="text-align: center; vertical-align: middle;">Gaji Karyawan</th>
						<th colspan="10" style="text-align: center; vertical-align: middle;">Iuran BPJS Ketenagakerjaan</th>
					</tr>
					<tr>
						<th colspan="2" style="text-align: center; vertical-align: middle;">Jaminan Kecelakaan Kerja</th>
						<th colspan="2" style="text-align: center; vertical-align: middle;">Jaminan Kematian</th>
						<th colspan="2" style="text-align: center; vertical-align: middle;">Jaminan Hari Tua</th>
						<th colspan="2" style="text-align: center; vertical-align: middle;">Jaminan Pensiun</th>
						<th colspan="2" rowspan="2" style="text-align: center; vertical-align: middle;">Total Iuran</th>
					</tr>
					<tr>
						<th style="text-align: center; vertical-align: middle;">1</th>
						<th style="text-align: center; vertical-align: middle;">2</th>
						<th style="text-align: center; vertical-align: middle;">3</th>
						<th style="text-align: center; vertical-align: middle;">4</th>
						<th style="text-align: center; vertical-align: middle;">5</th>
						<th style="text-align: center; vertical-align: middle;">6</th>
						<th style="text-align: center; vertical-align: middle;">7</th>
						<th style="text-align: center; vertical-align: middle;">8</th>
						<th style="text-align: center; vertical-align: middle;">9</th>
					</tr>
					<tr>
						<th> </th>
						<th style="text-align: center; vertical-align: middle;"> Perusahaan (1*2) </th>
						<th style="text-align: center; vertical-align: middle;">Karyawan</th>
						<th style="text-align: center; vertical-align: middle;"> Perusahaan (1*4) </th>
						<th style="text-align: center; vertical-align: middle;">Karyawan</th>
						<th style="text-align: center; vertical-align: middle;">Perusahaan (1*6)</th>
						<th style="text-align: center; vertical-align: middle;">Karyawan (1*7)</th>
						<th style="text-align: center; vertical-align: middle;">Perusahaan (1*8)</th>
						<th style="text-align: center; vertical-align: middle;">Karyawan (1*9)</th>
						<th rowspan="2" style="text-align: center; vertical-align: middle;">Perusahaan (2+4+6+8)</th>
						<th rowspan="2" style="text-align: center; vertical-align: middle;">Karyawan (3+5+7+9)</th>
					</tr>
					<tr>
						<th> </th>
						<th style="text-align: center; vertical-align: middle;">0.24%</th>
						<th style="text-align: center; vertical-align: middle;">0</th>
						<th style="text-align: center; vertical-align: middle;">0.30%</th>
						<th style="text-align: center; vertical-align: middle;">0</th>
						<th style="text-align: center; vertical-align: middle;">3.70%</th>
						<th style="text-align: center; vertical-align: middle;">2.00%</th>
						<th style="text-align: center; vertical-align: middle;">2.00%</th>
						<th style="text-align: center; vertical-align: middle;">1.00%</th>
					</tr>';
		$data = $this->Penggajian_Model->bpjs_ketenagakerjaan($cabang);
		$total_nominal = 0;
		$total_jkkp = 0;
		$total_jkkk = 0;
		$total_jkmp = 0;
		$total_jkmk = 0;
		$total_jhtp = 0;
		$total_jhtk = 0;
		$total_jpp = 0;
		$total_jpk = 0;
		$total_perusahaan = 0;
		$total_karyawan = 0;
		if (! empty($data)) {
			$no = 1;
			foreach ($data as $row) {
				$table .= '
				<tr><td style="text-align:center;">'.$no.'</td>
					<td>'.$row->nama.'</td>
					<td style="text-align:right;">'.uang($row->nominal).'</td>
					<td style="text-align:right;">'.uang($row->jkkp).'</td>
					<td style="text-align:right;">'.uang($row->jkkk).'</td>
					<td style="text-align:right;">'.uang($row->jkmp).'</td>
					<td style="text-align:right;">'.uang($row->jkmk).'</td>
					<td style="text-align:right;">'.uang($row->jhtp).'</td>
					<td style="text-align:right;">'.uang($row->jhtk).'</td>
					<td style="text-align:right;">'.uang($row->jpp).'</td>
					<td style="text-align:right;">'.uang($row->jpk).'</td>
					<td style="text-align:right;">'.uang($row->total_p).'</td>
					<td style="text-align:right;">'.uang($row->total_k).'</td>
				</tr>';

				$no++;

				$total_nominal = $total_nominal + $row->nominal;
				$total_jkkp = $total_jkkp + $row->jkkp;
				$total_jkkk = $total_jkkk + $row->jkkk;
				$total_jkmp = $total_jkmp + $row->jkmp;
				$total_jkmk = $total_jkmk + $row->jkmk;
				$total_jhtp = $total_jhtp + $row->jhtp;
				$total_jhtk = $total_jhtk + $row->jhtk;
				$total_jpp = $total_jpp + $row->jpp;
				$total_jpk = $total_jpk + $row->jpk;
				$total_perusahaan = $total_perusahaan + $row->total_p;
				$total_karyawan = $total_karyawan + $row->total_k;
			}
		}
		$table .= '	<tr>
						<th colspan="2"> </th>
						<th style="text-align:right;">'.uang($total_nominal).'</th>
						<th style="text-align:right;">'.uang($total_jkkp).'</th>
						<th style="text-align:right;">'.uang($total_jkkk).'</th>
						<th style="text-align:right;">'.uang($total_jkmp).'</th>
						<th style="text-align:right;">'.uang($total_jkmk).'</th>
						<th style="text-align:right;">'.uang($total_jhtp).'</th>
						<th style="text-align:right;">'.uang($total_jhtk).'</th>
						<th style="text-align:right;">'.uang($total_jpp).'</th>
						<th style="text-align:right;">'.uang($total_jpk).'</th>
						<th style="text-align:right;">'.uang($total_perusahaan).'</th>
						<th style="text-align:right;">'.uang($total_karyawan).'</th>
					</tr>';
		$table .= '</table>';

		$data = array(
			'html' => $table,
			'title' => 'PERHITUNGAN BPJS KETENAGAKERJAAN GMEDIA SEMARANG DAN SALATIGA'
		);

		$this->load->view('file_download/file_lembur', $data);
		$filename = 'BPJS';
		$html = $this->output->get_output();
	  	$this->load->library('pdfgenerator');
	  	$this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
	}
}

/* End of file download_file.php */
/* Location: ./application/controllers/download_file.php */ ?>