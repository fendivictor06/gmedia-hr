<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penggajian_Model', '', TRUE);
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Akun_Model', '', TRUE);
    }
    
    function header()
    {
        $menu =  '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">'.$this->Main_Model->style_fullcalendar();
        return $menu;
    }
    
    function footer()
    {
        $footer = '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/flot/jquery.flot.min.js') . '" type="text/javascript"></script>
        		<script src="' . base_url('assets/plugins/flot/jquery.flot.resize.min.js') . '" type="text/javascript"></script>
        		<script src="' . base_url('assets/plugins/flot/jquery.flot.categories.min.js') . '" type="text/javascript"></script>';
        
        return $footer;
    }

    function javascript()
    {
        header('Content-Type: application/javascript');
        $js = $this->session->userdata('js');
        echo minifyjs(file_get_contents($js));
    }
    
    function index()
    {
        if($this->session->userdata('loggedin') == 1) {
            redirect('main/dashboard','refresh');
        } else {
            $javascript = base_url('assets/js/app/login.js');
            $this->session->set_userdata('js', $javascript);
            $this->load->view('login');
        }
    }

    function generate_cuti($year = '', $nip = '')
    {
        if ($year == '') {
            $year = date("Y");
        }
        $data = $this->Main_Model->generate_cuti($year, $nip); 

        echo $data;
    }

    function generate_cuti_lalu()
    {
        $year = date("Y");
        $year_lalu = $year-1;
        $this->Main_Model->generate_cuti($year_lalu); 
    }

    function json_absen()
    {
        $this->Main_Model->get_login();
        $data = $this->Main_Model->jumlah_absen();

        echo json_encode($data);
    }

    function json_karyawan_cabang()
    {
        $this->Main_Model->get_login();
        $data = $this->Main_Model->pie_chart();

        echo json_encode($data);
    }

    function json_terlambat()
    {
        $this->Main_Model->get_login();
        $data_terlambat = $this->Main_Model->jumlah_terlambat();

        echo json_encode($data_terlambat);
    }

    function json_karyawan_baru()
    {
        $this->Main_Model->get_login();
        $data = $this->Main_Model->jumlah_karyawan_baru();

        echo json_encode($data);
    }

    function json_karyawan_resign()
    {
        $this->Main_Model->get_login();
        $data = $this->Main_Model->jumlah_karyawan_resign();

        echo json_encode($data);
    }
    
    function dashboard()
    {
        $this->Main_Model->get_login();
        
        $per = $this->Main_Model->per_skr();
        $q1 = $this->Main_Model->jml_a();
        $q2 = $this->Main_Model->jml_b();
        $slsh = $q1->jmla - $q2->jmlb;
        $period = $this->Main_Model->per();
        $javascript = base_url('assets/js/app/admin_dashboard.js');
        $this->session->set_userdata('js', $javascript);

        $footer = array(
            'js' => $this->footer()
                    .$this->Main_Model->highchart()
                    .$this->Main_Model->js_moment()
                    .$this->Main_Model->js_counter()
                    .$this->Main_Model->js_amchart()
                    .$this->Main_Model->js_fullcalendar()
                    .$this->Main_Model->js_bs_select(),
            'javascript' =>  '<script src="'.base_url('app.js').'"></script>'
        );
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';
        $periode = isset($per->periode) ? $per->periode : '';

        $data   = array(
            'jml_kont_kosong' => $this->Main_Model->jml_kontrak_habis($tgl_awal, $tgl_akhir),
            'jml_tdk_ada' => $this->Main_Model->jml_tidak_ada_kontrak($tgl_awal, $tgl_akhir),
            'jml_baru' => $this->Main_Model->jml_kary_baru($tgl_awal, $tgl_akhir),
            'jml_resign' => $this->Main_Model->resign_baru($tgl_awal, $tgl_akhir),
            'jml_blm_ijazah'=> $this->Main_Model->jml_blm_ijazah(),
            'jml_mutasi' => $this->Main_Model->jml_mutasi($tgl_awal, $tgl_akhir),
            'jml_surat_kom' => $this->Main_Model->jml_surat_komitmen(),
            'jml_surat_kesanggupan' => $this->Main_Model->jml_surat_kesanggupan(),
            'jml_no_sk' => $slsh,
            'periode' => $periode,
            
        );

        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','1'),
            'style' => $this->header()
                      .$this->Main_Model->style_bs_select()
                      .'<style>
                            .grafik {
                                width : 100%;
                                height : 500px;
                                font-size : 11px;
                            }
                        </style>'
            );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/dashboard', $data);
        $this->load->view('template/footer', $footer);
    }

    function birthday()
    {
        $this->Main_Model->get_login();
        $condition = date('Y').'-%m-%d';
        $q = $this->db->query("
            SELECT a.`nama` title,DATE_FORMAT(a.`tgl_lahir`,'$condition') start 
            FROM kary a 
            WHERE a.`tgl_lahir` != '' 
            AND ISNULL(a.`tgl_resign`)")->result();

        $ajax_request = $this->input->is_ajax_request();
        if ($ajax_request) {
            echo json_encode($q);
        } else {
            show_404();
        }
    }
    
    function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $cek = $this->Main_Model->login($username, $password);
        $ip = file_get_contents('https://api.ipify.org');

        $log = array(
            'username' => $username,
            'ip' => $ip 
        );
        $this->db->insert('log_login', $log);
        
        $usr_id = isset($cek->usr_id) ? $cek->usr_id : '';
        $acctype = isset($cek->usr_AccType) ? $cek->usr_AccType : '';
        if (! empty($cek)) {
            $sess_array = array(
                'username'  => isset($cek->usr_name) ? $cek->usr_name : '',
                'loggedin'  => 1,
                'idp' => 1,
                'id_user' => $usr_id,
                'acctype' => $acctype
            );
            $this->session->set_userdata($sess_array);
            
            if ($acctype == "Administrator") {
                $result = array(
                    'status' => true, 
                    'message' => 'Login Berhasil...'
                );
            } elseif ($acctype == "Operator") {
                $session_cabang = $this->Main_Model->session_cabang();
                $session_divisi = $this->Main_Model->session_divisi();
                $session_pengajuan = $this->Main_Model->session_pengajuan();
                if (!empty($session_cabang) && !empty($session_divisi) && !empty($session_pengajuan)) {
                    $result = array(
                        'status' => true, 
                        'message' => 'Login Berhasil...'
                    );
                } else {
                    $result = array(
                        'status' => false, 
                        'message' => 'Oops, Sepertinya akun anda belum disetting !'
                    );
                }
            } else {
                $result = array(
                    'status' => false, 
                    'message' => 'Username atau Password anda Salah !'
                );
            }
        } else {
            $result = array(
                'status' => false, 
                'message' => 'Username atau Password anda Salah !'
            );
        }

        $is_ajax = $this->input->is_ajax_request();
        if ($is_ajax) {
            echo json_encode($result);
        } else {
            show_404();
        }
    }
    
    function logout()
    {
        $this->session->sess_destroy();
        redirect('main', 'refresh');
    }
    
    // UNUSED METHOD START ====>
    function register()
    {
        $usr_name = $this->input->post('usr_name');
        $usr_pass = $this->input->post('usr_pass');
        $nip      = $this->input->post('nip');
        
        $data = "insert into users(usr_name,usr_pass,nip) values('$usr_name',AES_ENCRYPT('$usr_pass','hr'),'$nip')";
        $this->Main_Model->register($data);
    }
    
    function formregister()
    {
        $this->Main_Model->get_login();
        $javascript = '
    		<script>
    			var save_method="save";
    			function register(){
	                var nip = $("#nip").val();
	                var username = $("#uname").val();
	                var password = $("#pwd").val();
	                var rpassword = $("#rpassword").val();
	                var id = $("#id").val();
	                if(nip=="" || username=="" || password=="" || rpassword==""){
	                    $("#notif").html("<span style=color:red;>Form tidak lengkap</span>");
	                }else if(password!=rpassword){
	                    $("#notif").html("<span style=color:red;>Password tidak cocok</span>");
	                }else{
	                    var dat = {
	                    	"id" : id,
	                        "usr_name" : username,
	                        "usr_pass" : password,
	                        "nip" : nip 
	                    }
	                    if(save_method=="save"){
	                    	$.ajax({
		                        url : "' . base_url('main/register') . '",
		                        data : dat,
		                        type : "POST",
		                        success : function(data){
		                            $("#notif").html("<span style=color:blue;>Sukses Mendaftar</span>");
		                            $("#uname").val("");
		                            $("#pwd").val("");
		                            $("#rpassword").val("");
		                            load_table();
		                        },
		                        error : function(jqXHR,textStatus,errorThrown){
		                            $("#notif").html("<span style=color:red;>Internal Server Error</span>");
		                        }
		                    });
	                    }else{
	                    	$.ajax({
		                        url : "' . base_url('main/update_usr') . '",
		                        data : dat,
		                        type : "POST",
		                        success : function(data){
		                            $("#notif").html("<span style=color:blue;>Sukses Mengedit</span>");
		                            save_method="save";
		                            load_table();
		                        },
		                        error : function(jqXHR,textStatus,errorThrown){
		                            $("#notif").html("<span style=color:red;>Internal Server Error</span>");
		                        }
		                    });
	                    }
	                    
	                }
	            }
	            function get_id(id){
					save_method = "update";
			    	$("#warning").addClass("hidden");
			    	$("#success").addClass("hidden");
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('main/user_id') . '",
			    		type : "GET",
			    		data : id,
			    		success : function(data){
			    			var dat = jQuery.parseJSON(data);
			    			$("#id").val(dat.usr_id);
							$("#nip").val(dat.nip).trigger("change");
							$("#uname").val(dat.usr_name);
							$("#pwd").val(dat.usr_pass);
							$("#rpassword").val(dat.usr_pass);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			alert("Internal Server Error");
			    		}
			    	});
			    }
	            function load_table(){
	            	$.ajax({
	            		url : "' . base_url('main/view_user') . '",
	            		type : "GET",
	            		success : function(data){
	            			$("#myTable").html(data);
	            			$("#dataTables-example").DataTable({
        						responsive: true
    						});
	            		},
	            		error : function(jqXHR,textStatus,errorThrown){
	            			bootbox.alert("Internal Server Error");
	            		}
	            	})
	            }
	            load_table();
            	$(document).ready(function() {
  					$(".select2").select2();
				});
				function delete_data(id){
			    	id = {"id":id}

			    	bootbox.dialog({
			    		message : "Yakin ingin menghapus data?",
			    		title : "Hapus Data",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('main/delete_usr') . '",
							    		type : "POST",
							    		data : id,
							    		success : function(data){
							    			bootbox.alert({
												message: "Delete Success",
												size: "small"
											});
							    			load_table();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})

			    }
    		</script>
    	';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );
        $data       = array(
            'kary' => $this->Main_Model->all_kary_active()
        );
        $menu       = array(
            'menu3' => 'active open selected',
            'style' => '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">'
        );
        $this->load->view('template/header', $menu);
        $this->load->view('template/form_register', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_user()
    {
        $this->Main_Model->get_login();
        $data = $this->Main_Model->view_user();
        
        $template = array(
            'table_open' => '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">',
            
            'thead_open'    => '<thead>',
            'thead_close' => '</thead>',
            
            'heading_row_start' => '<tr>',
            'heading_row_end' => '</tr>',
            'heading_cell_start' => '<th>',
            'heading_cell_end' => '</th>',
            
            'tbody_open' => '<tbody>',
            'tbody_close' => '</tbody>',
            
            'row_start' => '<tr>',
            'row_end' => '</tr>',
            'cell_start' => '<td>',
            'cell_end' => '</td>',
            
            'row_alt_start' => '<tr>',
            'row_alt_end' => '</tr>',
            'cell_alt_start' => '<td>',
            'cell_alt_end' => '</td>',
            
            'table_close' => '</table>'
        );
        
        $this->table->set_heading('No', 'NIP', 'Nama', 'Username', 'Password', 'Action');
        $no = 1;
        foreach ($data as $row) {
            $this->table->add_row($no++, $row->nip, $row->nama, $row->usr_name, $row->usr_pass, '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                	<li>
	                		<a href="javascript:;" onclick="get_id(' . $row->usr_id . ');">
	                			<i class="icon-edit"></i> Update </a>
	                	</li>
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->usr_id . ');">
	                            <i class="icon-edit"></i> Delete </a>
	                    </li>
	                </ul>
	        </div>');
        }
        
        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function user_id()
    {
        $this->Main_Model->get_login();
        $id   = $this->input->get('id');
        $data = $this->Main_Model->user_id($id);
        echo json_encode($data);
    }
    
    function update_usr()
    {
        $usr_name = $this->input->post('usr_name');
        $usr_pass = $this->input->post('usr_pass');
        $nip      = $this->input->post('nip');
        $id       = $this->input->post('id');
        
        $data = "update users set usr_name='$usr_name',usr_pass=AES_ENCRYPT('$usr_pass','hr'),nip='$nip' where usr_id='$id'";
        $this->Main_Model->update_usr($data);
    }
    
    function delete_usr()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Main_Model->delete_usr($id);
    }
    // UNUSED METHOD END ====>

    function changept($id)
    {
        $this->session->set_userdata('idp', $id);
    }
    
    function ceknip($id)
    {
        if ($id == 1) {
            $nip    = $this->input->post('nip');
            $result = $this->Main_Model->ceknip($nip);
        } else {
            $nip    = $this->input->post('nip');
            $tgl    = $this->Main_Model->convert_tgl($this->input->post('tgl'));
            $result = $this->Main_Model->ceknip_tgl($nip, $tgl);
        }
        
        echo $result;
    }

    // UNUSED METHOD START ====>
    function sk_kosong()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_sk_kosong');
        $javascript     = '
            <script>
                '.$this->Main_Model->default_loadtable($url_load).'
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan SK Masih Kosong'
        );
        
        $this->load->view('template/header', $this->header());
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_sk_kosong()
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_sk_kosong();
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','JK','Status Karyawan','Action');
        $no=1;
        foreach ($data as $row) {
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            ($row->gend=="L") ? 'Laki-laki':'Perempuan',
            $row->kary_stat,
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="' . base_url('karyawan/sk') . '/' . $row->nip . '">Surat Keputusan </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }
    // UNUSED METHOD END ====>

    function kary_resign()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_kary_resign');
        $javascript     = '
            <script>
                function load_table() {
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    $.ajax({
                        url : "'.$url_load.'",
                        data : {
                            "tahun" : tahun,
                            "bulan" : bulan
                        },
                        beforeSend : function(){
                            $("#tampil").addClass("hidden");
                            $("#unduh").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },
                        complete : function(){
                            $("#tampil").removeClass("hidden");
                            $("#unduh").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },
                        success : function(data){
                            $("#myTable").html(data);
                            '.$this->Main_Model->default_datatable().'
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal mengambil data");
                        }
                    });
                }

                $("#tampil").click(function(){
                    load_table();
                });

                $(document).ready(function(){
                    $(".select2").select2();
                    var thisMonth = "'.date("n").'";
                    var thisYear = "'.date("Y").'";
                    $("#tahun").val(thisYear).trigger("change");
                    $("#bulan").val(thisMonth).trigger("change");
                    load_table();
                });

                $("#unduh").click(function(){
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    window.location.href="'.base_url('download_excel/kary_resign').'/"+tahun+"/"+bulan;
                });
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan Resign',
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => '1'
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','1'),
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_resign()
    {
        $this->Main_Model->get_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data       = $this->Karyawan_Model->view_kary_resign($qd_id);
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cabang','JK','Tanggal Masuk','Tanggal Resign','Rehire','Alasan Resign','Action');
        $no=1;
        foreach ($data as $row) {
        ($row->tipe_resign) ? $alasan_resign = '('.$row->tipe_resign.') '.$row->alasan_resign : $alasan_resign = $row->alasan_resign;
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->cabang,
            ($row->gend=="L") ? 'Laki-laki':'Perempuan',
            ($row->tgl_masuk) ? $this->Main_Model->format_tgl($row->tgl_masuk) : '',
            ($row->tgl_resign) ? $this->Main_Model->format_tgl($row->tgl_resign) : '',
            $row->rehire,
            $alasan_resign,
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="' . base_url('karyawan/bio_kary') . '/' . $row->nip . '">Biodata </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function kary_baru()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_kary_baru');
        $javascript     = '
            <script>
                function load_table() {
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    $.ajax({
                        url : "'.$url_load.'",
                        data : {
                            "tahun" : tahun,
                            "bulan" : bulan
                        },
                        beforeSend : function(){
                            $("#tampil").addClass("hidden");
                            $("#unduh").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },
                        complete : function(){
                            $("#tampil").removeClass("hidden");
                            $("#unduh").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },
                        success : function(data){
                            $("#myTable").html(data);
                            '.$this->Main_Model->default_datatable().'
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal mengambil data");
                        }
                    });
                }

                $(document).ready(function(){
                    $(".select2").select2();
                    var thisMonth = "'.date("n").'";
                    var thisYear = "'.date("Y").'";
                    $("#tahun").val(thisYear).trigger("change");
                    $("#bulan").val(thisMonth).trigger("change");
                    load_table();
                });

                $("#tampil").click(function(){
                    load_table();
                });

                $("#unduh").click(function(){
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    window.location.href="'.base_url('download_excel/kary_baru').'/"+tahun+"/"+bulan;
                });
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan Baru',
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => '1'
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','1'),
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_baru()
    {
        $this->Main_Model->get_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data       = $this->Karyawan_Model->view_kary_baru($qd_id);
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Status Karyawan','Cabang','Posisi','Tanggal Masuk','Action');
        $no=1;
        foreach ($data as $row) {
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->kary_stat,
            $row->cabang,
            $row->jab,
            ($row->tgl_masuk) ? $this->Main_Model->format_tgl($row->tgl_masuk) : '',
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="' . base_url('karyawan/bio_kary') . '/' . $row->nip . '">Biodata </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function kary_ijazah()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_kary_ijazah');
        $javascript     = '
            <script>
                '.$this->Main_Model->default_loadtable($url_load).'
                $("#download").click(function(){
                    window.location.href="'.base_url('download_excel/kary_ijazah').'";
                });
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan Belum Mengumpulkan Ijazah',
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => '0'
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','1'),
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_ijazah()
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_kary_ijazah();
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Status Karyawan','Cabang','Posisi','Status Ijazah','No Ijazah','Action');
        $no=1;
        foreach ($data as $row) {
        $posisi     = $this->Main_Model->posisi($row->nip);
        $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
        $jab        = isset($posisi->jab) ? $posisi->jab : '';
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->kary_stat,
            $cabang,
            $jab,
            $row->status_ijazah,
            $row->no_ijazah,
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="' . base_url('karyawan/biodata') . '/' . $row->nip . '">Update </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function kary_mutasi()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_kary_mutasi');
        $javascript     = '
            <script>
                '.$this->Main_Model->default_loadtable($url_load).'
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan Mutasi'
        );
        
        $this->load->view('template/header', $this->header());
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_mutasi()
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_kary_mutasi();
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','JK','Status Karyawan','Tanggal Masuk','Posisi','Cabang','Action');
        $no=1;
        foreach ($data as $row) {
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            ($row->gend=="L") ? 'Laki-laki':'Perempuan',
            $row->kary_stat,
            ($row->tgl_masuk) ? $this->Main_Model->format_tgl($row->tgl_masuk) : '',
            $row->jab,
            $row->cabang,
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="' . base_url('karyawan/sk') . '/' . $row->nip . '">Surat Keputusan </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function kary_surat_komitmen()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_kary_surat_komitmen');
        $javascript     = '
            <script>
                '.$this->Main_Model->default_loadtable($url_load).'
                $("#download").click(function(){
                    window.location.href="'.base_url('download_excel/kary_komitmen').'";
                });
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan Belum Mengumpulkan Surat Komitmen',
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => 0
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','1'),
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_surat_komitmen()
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_kary_surat_komitmen();
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Status Karyawan','Cabang','Posisi','Surat Komitmen','Action');
        $no=1;
        foreach ($data as $row) {
        $posisi     = $this->Main_Model->posisi($row->nip);
        $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
        $jab        = isset($posisi->jab) ? $posisi->jab : '';
        ($row->file_surat_komitmen == '' && $row->surat_komitmen == 'SUDAH') ? $surat_komitmen = $row->surat_komitmen.' (File tidak ada)' : $surat_komitmen = $row->surat_komitmen;
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->kary_stat,
            $cabang,
            $jab,
            $surat_komitmen,
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="' . base_url('karyawan/biodata') . '/' . $row->nip . '">Update </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function kary_kontrak_habis()
    {
        $this->Main_Model->all_login();
        $sess_cabang = $this->Main_Model->session_cabang();
        $datepicker = $this->Main_Model->default_datepicker();
        $datatable = $this->Main_Model->default_datatable();

        $url_load = base_url('main/view_kary_kontrak_habis');
        $javascript = '
            <script>
                '.$datepicker.'

                $(document).ready(function(){
                    $(".select2").select2();
                    var thisMonth = "'.date("n").'";
                    var thisYear = "'.date("Y").'";
                    $("#tahun").val(thisYear).trigger("change");
                    $("#bulan").val(thisMonth).trigger("change");
                    load_table();
                });

                function load_table() {
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    $.ajax({
                        url : "'.$url_load.'",
                        data : {
                            "tahun" : tahun,
                            "bulan" : bulan
                        },
                        beforeSend : function(){
                            $("#tampil").addClass("hidden");
                            $("#unduh").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },
                        complete : function(){
                            $("#tampil").removeClass("hidden");
                            $("#unduh").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },
                        success : function(data){
                            $("#myTable").html(data);
                            '.$datatable.'
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal mengambil data");
                        }
                    });
                }

                $("#tampil").click(function(){
                    load_table();
                });

                $("#unduh").click(function(){
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    window.location.href="'.base_url('download_excel/habis_kontrak').'/"+tahun+"/"+bulan;
                });

                function unduh_pkwt(nip) {
                    window.open(
                        "'.base_url('download_file/kontrak_pkwt').'?nip="+nip,
                        "_blank"
                    );
                }

                function unduh_ojt(nip) {
                    window.open(
                        "'.base_url('download_file/kontrak_ojt').'?nip="+nip,
                        "_blank"
                    );
                }
            </script>
        ';
        $footer = array(
            'js' => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1' => 'Karyawan Kontrak Habis',
            'periode' => $this->Main_Model->periode_opt(),
            'isperiode' => '1',
            'modal' => $this->modal_kontrak()
        );

        $menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','1') : $this->Main_Model->menu_user('0','0','70');

        $header = array(
            'menu' => $menu,
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_kontrak_habis()
    {
        $this->Main_Model->all_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Karyawan_Model->view_kary_kontrak_habis($qd_id);
        $template = $this->Main_Model->tbl_temp();
        $sess_cabang = $this->Main_Model->session_cabang();

        $this->table->set_heading('No','NIP','Nama','Jabatan','Cabang','Status Karyawan','Awal Kontrak','Akhir Kontrak','Action');
        $no=1;
        foreach ($data as $row) {
            if (empty($sess_cabang)) {
                $action = '<li> <a href="' . base_url('informasi/kontrak_nip') . '/' . $row->nip . '">Status Kepegawaian </a> </li>';
            } else {
                $action = '<li> <a onclick="unduh_pkwt('.$row->nip.')"> Download PKWT </a> </li>';
                $action .= '<li> <a onclick="unduh_ojt('.$row->nip.')"> Download OJT </a> </li>';
            }
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->jab,
                $row->cabang,
                $row->kary_stat,
                ($row->tgl_awal)?$this->Main_Model->format_tgl($row->tgl_awal):'',
                ($row->tgl_akhir)?$this->Main_Model->format_tgl($row->tgl_akhir):'',
                '<div class="btn-group">
                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                        <i class="fa fa-angle-down"></i>
                    </button>
                        <ul class="dropdown-menu" role="menu">
                            '.$action.'
                        </ul>
                </div>'
                );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function modal_kontrak()
    {
        $result = '<div id="Mdownload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                <div class="modal-header">
                    <h4 class="modal-title">Download File</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="fdownload">
                                <input type="hidden" name="nip" id="nip" class="blank">
                                <div class="form-group">
                                    <label>Tgl Mulai Perjanjian</label>
                                        <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_mulai" id="tgl_mulai" />
                                </div>
                                <div class="form-group">
                                    <label>Tgl Akhir Perjanjian</label>
                                        <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                    <button type="button" onclick="simpan();" class="btn btn-primary">Download</button>
                </div>
            </div>';

        return $result;
    }

    function kary_surat_kesanggupan()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_kary_surat_kesanggupan');
        $javascript     = '
            <script>
                '.$this->Main_Model->default_loadtable($url_load).'
                $("#download").click(function(){
                    window.location.href="'.base_url('download_excel/kary_kesanggupan').'";
                });
                
                function upload(id_sp)
                {
                    $("#myModal").modal();
                    $("#id").val(id_sp);
                }
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan Belum Mengumpulkan Surat Kesanggupan',
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => 0,
            'modal'     => '<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false"> <form action="'.base_url('main/upload_surat_kesanggupan').'" method="post" enctype="multipart/form-data"> <div class="modal-header"> <h4 class="modal-title">Upload Surat Kesanggupan</h4> </div> <div class="modal-body"> <div class="row"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> <input type="hidden" name="id" id="id" class="kosong"> <div class="form-group"> <label>File Upload</label> <input type="file" name="file_kesanggupan" id="file_kesanggupan" class="form-control" required /> </div> </div> </div> </div> <div class="modal-footer"> <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button> <button type="submit" class="btn btn-primary">Simpan</button> </div> </form> </div>'
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','1'),
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_surat_kesanggupan()
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_surat_kesanggupan();
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Status Karyawan','Cabang','Posisi','SP','Akhir SP','Alasan SP','Action');
        $no=1;
        foreach ($data as $row) {
        $posisi     = $this->Main_Model->posisi($row->nip);
        $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
        $jab        = isset($posisi->jab) ? $posisi->jab : '';

        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->kary_stat,
            $cabang,
            $jab,
            $row->sp,
            ($row->akhir_sp) ? $this->Main_Model->format_tgl($row->akhir_sp) : '',
            $row->alasan_sp,
            '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="javascript:;" onclick="upload('.$row->id_sp.')">Upload File </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function upload_surat_kesanggupan()
    {
        $this->Main_Model->get_login();
        $this->load->library('upload');
        $id_sp      = $this->input->post('id');
        $sp         = $this->db->where('id_sp',$id_sp)->get('tb_sp')->row();
        $file_name  = 'Surat Kesanggupan '.$sp->nip.'_'.date("Y-m-d");
        $config     = $this->Main_Model->set_upload_options("./assets/surat_peringatan/","*","5048000",$file_name);
        $this->upload->initialize($config);
        if($this->upload->do_upload('file_kesanggupan'))
        {
            $file_surat = $this->upload->data();
        }
        $data   = array(
            'surat_kesanggupan' => $file_surat['file_name']
            );

        $this->Absensi_Model->sp_process($data, $id_sp);
        $this->session->set_flashdata('message_upload', '<script>notif("Success!");</script>');
        redirect('main/kary_surat_kesanggupan','refresh');
    }

    function kary_rekening()
    {
        $this->Main_Model->get_login();
        $url_load = base_url('main/view_kary_rekening');
        $javascript = '
            <script>
                function load_table()
                {
                    $.ajax({
                        url : "'.$url_load.'",
                        beforeSend : function(){
                            $(".btn").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },
                        complete : function(){
                            $(".btn").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },
                        success : function(data){
                            $("#myTable").html(data);
                            '.$this->Main_Model->default_datatable().'
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal mengambil data");
                        }
                    });
                }
                load_table();

                $("#download").click(function(){
                    var periode = $("#periode").val();
                    window.location.href="'.base_url('download_excel/rekening').'";
                });
            </script>
        ';
        $footer = array(
            'js' => $this->footer(),
            'javascript' => minifyjs($javascript)
        );
        $data = array(
            'h1' => 'Karyawan Belum ada Rekening',
            'periode' => $this->Main_Model->periode_opt(),
            'isperiode' => '0'
        );
        $header = array(
            'menu' => $this->Main_Model->menu_admin('0','0','2'),
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_rekening()
    {
        $this->Main_Model->get_login();
        $data = $this->Karyawan_Model->view_kary_rekening();
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Status Karyawan','Cabang','Jabatan','No Rekening','Action');
        $no = 1;
        foreach ($data as $row) {
            $posisi = $this->Main_Model->posisi($row->nip);
            $cabang = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab = isset($posisi->jab) ? $posisi->jab : '';
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->kary_stat,
                $cabang,
                $jab,
                $row->norek,
                '<div class="btn-group">
                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                        <i class="fa fa-angle-down"></i>
                    </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="' . base_url('informasi/kontrak_nip') . '/' . $row->nip . '">Status Kepegawaian </a>
                            </li>
                        </ul>
                </div>'
                );
        }

        $this->table->set_template($template);
        $is_ajax = $this->input->is_ajax_request();
        if ($is_ajax) {
            echo $this->table->generate();
        } else {
            show_404();
        }
    }

    function sudah_absen()
    {
        $this->Main_Model->guest_login();
        $url_view = base_url('main/view_sudah_absen');
        $javascript = '
            <script>'
                .$this->Main_Model->default_loadtable($url_view).
                '$("#download").click(function(){
                    window.location.href="'.base_url('download_excel/sudah_absen').'";
                });
            </script>';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan yang Sudah Absen Hari ini '.date('d F Y'),
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => '0'
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_user('0','0','70')
        );

        $this->load->view('akun/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('akun/footer', $footer);
    }

    function view_sudah_absen()
    {
        $this->Main_Model->guest_login();
        $cabang = $this->session->userdata('cabang');
        $data = $this->Akun_Model->view_sudah_absen($cabang);
        $template = $this->Main_Model->tbl_temp();
        $this->table->set_heading('No','NIP','Nama','Waktu','Cabang');
        $no=1;
        foreach ($data as $row) {
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->waktu,
            $row->cabang
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function mangkir()
    {
        $this->Main_Model->guest_login();
        $url_view = base_url('main/view_mangkir');
        $absen = array(
                'sakit' => 'Sakit',
                'ijin' => 'Ijin',
                'mangkir' => 'Mangkir'
            );
        $content = '<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
                        <div class="modal-header">
                            <h4 class="modal-title">Form Presensi</h4>
                        </div>
                        <form id="form_presensi">
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <input type="hidden" name="id" id="id">
                                        <input type="hidden" name="id_ab" id="id_ab">
                                        <input type="hidden" name="nip" id="nip">
                                        <div class="form-group">
                                            <label>Nama</label>
                                                <input type="text" class="form-control" name="nama" id="nama" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                                <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                                        </div>
                                        <div class="form-group">
                                            <label>Sakit/Ijin/Mangkir</label>
                                                '.form_dropdown('absen', $absen, '', 'class="form-control" id="absen"').'
                                        </div>
                                        <div class="form-group">
                                            <label>Scan Masuk</label>
                                                <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24">
                                        </div>
                                        <div class="form-group">
                                            <label>Scan Pulang</label>
                                                <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24">
                                        </div>
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                                <textarea class="form-control" name="ket" id="ket" rows="5"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>File Upload</label>
                                                <input type="file" name="file_upload" id="file_upload" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Klarifikasi</label>
                                                <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>';
        $javascript = '
            <script>
                '.$this->Main_Model->default_loadtable($url_view).'

                $("#download").click(function(){
                    window.location.href="'.base_url('download_excel/mangkir').'";
                });

                function get_id(id) {
                    $.ajax({
                        url : "'.base_url('absensi/presensi_id').'",
                        data : {"id" : id},
                        dataType : "json",
                        success : function(dat) {
                        
                            $("#myModal").modal();
                            $("#id").val(dat.id);
                            $("#tgl").val(dat.tanggal);
                            $("#ket").val(dat.ket);
                            $("#klarifikasi").val(dat.klarifikasi);
                            if(dat.sakit == 1){
                                $("#absen").val("sakit");
                            }else if(dat.ijin == 1){
                                $("#absen").val("ijin");
                            }else if(dat.mangkir == 1){
                                $("#absen").val("mangkir");
                            }else{
                                $("#absen").val("mangkir");
                            }
                            $("#scan_masuk").val(dat.jam_masuk);
                            $("#scan_pulang").val(dat.jam_pulang);
                            $("#id_ab").val(dat.id_ab);
                            $("#nip").val(dat.nip);
                            $("#nama").val(dat.nama);
                        }
                    });
                }

                $("#form_presensi").submit(function(event){
                    event.preventDefault();
                    var d = new FormData($(this)[0]);
                    $.ajax({
                        url : "'.base_url('absensi/presensi_process').'",
                        type : "post",
                        data : d,
                        dataType : "json",
                        async : false,
                        cache : false,
                        contentType : false,
                        processData : false,
                        success : function(data) {
                            if(data.status == true) {
                                load_table(), reset();
                            }
                            bootbox.alert(data.message);
                        }
                    });
                    return false;
                });
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'Karyawan yang Mangkir Tanggal '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y')))),
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => '0',
            'modal' => $content
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_user('0','0','70')
        );

        $this->load->view('akun/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('akun/footer', $footer);
    }

    function view_mangkir()
    {
        $this->Main_Model->guest_login();
        $data = $this->Akun_Model->view_mangkir();
        $template = $this->Main_Model->tbl_temp('mangkir');

        $is_approval = $this->Main_Model->is_approval();
        $array_header = array('No','NIP','Nama','Tanggal','Keterangan','Cabang');
        if($is_approval == TRUE) $array_header[] = 'Action';
        $this->table->set_heading($array_header);

        $no=1;
        foreach ($data as $row) {
            $array_data = array(
                    $no++,
                    $row->nip,
                    $row->nama,
                    ($row->tgl) ? $this->Main_Model->format_tgl($row->tgl) : '',
                    $row->ket,
                    $row->cabang);

            if($is_approval == TRUE) $array_data[] = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="get_id(' . $row->id . ');"> <i class="fa fa-pencil"></i> Klarifikasi </a>';

            $this->table->add_row($array_data);
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function view_perdin()
    {
        $this->Main_Model->guest_login();
        $data = $this->Akun_Model->view_perdin();
        $template = $this->Main_Model->tbl_temp('perdin');

        $is_approval = $this->Main_Model->is_approval();
        $array_header = array('No', 'No Bukti', 'Berangkat', 'Pulang', 'Tujuan', 'Keperluan', 'Nama', 'Verifikasi');

        ($is_approval == TRUE) ? $array_header[] = 'Action' : $array_header = $array_header;
        $this->table->set_heading($array_header);

        $no = 1;
        foreach($data as $row) {
            $detail = $this->Akun_Model->detail_perdin($row->nobukti);
            $event = "verif('".$row->id_perdin."')";
            $action = ($row->verifikasi == 1) ? '' : '<button class="btn btn-primary btn-xs" onclick="'.$event.'" ><i class="fa fa-check"></i> Verifikasi</button>';
            $nama = '';
            $i = 1;
            foreach($detail as $r) {
                $nama .= $i.'. '.$r->nama.'<br>';
                $i++;
            }
            $verif = ($row->verifikasi == 1) ? 'Sudah' : 'Belum';

            $array_data = array($no++, $row->nobukti, $row->berangkat, $row->pulang, $row->tujuan, $row->keperluan, $nama, $verif);

            ($is_approval == TRUE) ? $array_data[] = $action : $array_data = $array_data;            
            $this->table->add_row($array_data);
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function history_kontrak()
    {
        $this->Main_Model->get_login();
        $url_load       = base_url('main/view_history_kontrak');
        $javascript     = '
            <script>
                $(document).ready(function(){
                    $(".select2").select2();
                });

                function load_table() {
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    $.ajax({
                        url : "'.$url_load.'",
                        data : {
                            "tahun" : tahun,
                            "bulan" : bulan
                        },
                        beforeSend : function(){
                            $("#tampil").addClass("hidden");
                            $("#unduh").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },
                        complete : function(){
                            $("#tampil").removeClass("hidden");
                            $("#unduh").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },
                        success : function(data){
                            $("#myTable").html(data);
                            '.$this->Main_Model->default_datatable().'
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal mengambil data");
                        }
                    });
                }
                load_table();

                $("#tampil").click(function(){
                    load_table();
                });

                $("#unduh").click(function(){
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    window.location.href="'.base_url('download_excel/history_kontrak').'/"+tahun+"/"+bulan;
                });
            </script>
        ';
        $footer = array(
            'js'        => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1'        => 'History Kontrak Karyawan',
            'periode'   => $this->Main_Model->periode_opt(),
            'isperiode' => '1'
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_history_kontrak()
    {
        $this->Main_Model->get_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $p = periode($tahun, $bulan);
        $id_periode = isset($p->qd_id) ? $p->qd_id : '';
        $data       = $this->Karyawan_Model->view_history_kontrak($id_periode);
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Status Karyawan','Awal Kontrak','Akhir Kontrak','Kontrak Lama','Akhir Kontrak Lama');
        $no=1;
        foreach ($data as $row) {
        if($row->status_karyawan == 'TETAP')  {
            $tgl_awal = '';
            $tgl_akhir = '';
        } else {
            $tgl_awal = isset($row->tgl_awal) ? $row->tgl_awal : '';
            $tgl_akhir = isset($row->tgl_akhir) ? $row->tgl_akhir : '';
        }
        $this->table->add_row(
            $no++,
            $row->nip,
            $row->nama,
            $row->status_karyawan,
            ($tgl_awal) ? $this->Main_Model->format_tgl($tgl_awal) : '',
            ($tgl_akhir) ? $this->Main_Model->format_tgl($tgl_akhir) : '',
            $row->tipe_kontrak_lama,
            ($row->tgl_akhir_lama) ? $this->Main_Model->format_tgl($row->tgl_akhir_lama) : ''
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function kary_kontrak_baru()
    {
        $this->Main_Model->all_login();
        $sess_cabang = $this->Main_Model->session_cabang();
        $datepicker = $this->Main_Model->default_datepicker();
        $datatable = $this->Main_Model->default_datatable();

        $url_load = base_url('main/view_kary_kontrak_baru');
        $javascript = '
            <script>
                '.$datepicker.'
                $(document).ready(function(){
                    $(".select2").select2();
                    var thisMonth = "'.date("n").'";
                    var thisYear = "'.date("Y").'";
                    $("#tahun").val(thisYear).trigger("change");
                    $("#bulan").val(thisMonth).trigger("change");
                    load_table();
                });

                function load_table() {
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    $.ajax({
                        url : "'.$url_load.'",
                        data : {
                            "tahun" : tahun,
                            "bulan" : bulan
                        },
                        beforeSend : function(){
                            $("#tampil").addClass("hidden");
                            $("#unduh").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },
                        complete : function(){
                            $("#tampil").removeClass("hidden");
                            $("#unduh").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },
                        success : function(data){
                            $("#myTable").html(data);
                            '.$datatable.'
                        },
                        error : function(jqXHR, textStatus, errorThrown){
                            bootbox.alert("Gagal mengambil data");
                        }
                    });
                }

                $("#tampil").click(function(){
                    load_table();
                });

                $("#unduh").click(function(){
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    window.location.href="'.base_url('download_excel/habis_kontrak').'/"+tahun+"/"+bulan;
                });

                function unduh_pkwt(nip) {
                    window.open(
                        "'.base_url('download_file/kontrak_pkwt').'?nip="+nip,
                        "_blank"
                    );
                }

                function unduh_ojt(nip) {
                    window.open(
                        "'.base_url('download_file/kontrak_ojt').'?nip="+nip,
                        "_blank"
                    );
                }
            </script>
        ';
        $footer = array(
            'js' => $this->footer(),
            'javascript'=> $javascript
        );
        $data   = array(
            'h1' => 'Karyawan Kontrak Baru',
            'periode' => $this->Main_Model->periode_opt(),
            'isperiode' => '1',
            'modal' => $this->modal_kontrak()
        );

        $menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','1') : $this->Main_Model->menu_user('0','0','70');

        $header = array(
            'menu' => $menu,
            'style' => $this->header()
        );
        
        $this->load->view('template/header', $header);
        $this->load->view('main/notif_page', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kary_kontrak_baru()
    {
        $this->Main_Model->all_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Karyawan_Model->view_kary_kontrak_baru($qd_id);
        $template = $this->Main_Model->tbl_temp();
        $sess_cabang = $this->Main_Model->session_cabang();

        $this->table->set_heading('No','NIP','Nama','Jabatan','Cabang','Status Karyawan','Awal Kontrak','Akhir Kontrak','Action');
        $no=1;
        foreach ($data as $row) {
            if (empty($sess_cabang)) {
                $action = '<li> <a href="' . base_url('informasi/kontrak_nip') . '/' . $row->nip . '">Status Kepegawaian </a> </li>';
            } else {
                $action = '<li> <a onclick="unduh_pkwt('.$row->nip.')"> Download PKWT </a> </li>';
                // $action .= '<li> <a onclick="unduh_ojt('.$row->nip.')"> Download OJT </a> </li>';
            }
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->jab,
                $row->cabang,
                $row->kary_stat,
                ($row->tgl_awal)?$this->Main_Model->format_tgl($row->tgl_awal):'',
                ($row->tgl_akhir)?$this->Main_Model->format_tgl($row->tgl_akhir):'',
                '<div class="btn-group">
                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                        <i class="fa fa-angle-down"></i>
                    </button>
                        <ul class="dropdown-menu" role="menu">
                            '.$action.'
                        </ul>
                </div>'
                );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }
}