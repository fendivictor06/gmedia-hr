<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departemen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Penggajian_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript 	= '
			<script>
					function reset()
					{
						$(".blank").val("");
					}
				'
				 .$this->Main_Model->default_loadtable('departemen/view_departemen')

				 .$this->Main_Model->notif()

				 .$this->Main_Model->post_data('departemen/departemen_process','save()','$("#form_departemen").serialize()','
				 	if(data.status=="true")
				 	{
				 		load_table();
				 		reset();
				 	} 
				 	notif(data.message);')

				 .$this->Main_Model->get_data('departemen/departemen_id','get_id(id)','
				 	$("#myModal").modal();
				 	$("#departemen").val(data.departemen);
				 	$("#keterangan").val(data.keterangan);
				 	$("#id").val(data.id_dept);')

				 .$this->Main_Model->default_delete_data('departemen/delete_departemen').'
			</script>
		';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','3'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
			);

		$this->load->view('template/header', $header);
        $this->load->view('karyawan/data_departemen');
        $this->load->view('template/footer', $footer);
	}

	function view_departemen()
	{
		$this->Main_Model->get_login();
        $idp   		= $this->session->userdata('idp');
        $data 		= $this->Karyawan_Model->view_departemen($idp);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Departemen','Keterangan','Action');
		$no =1;
        
        foreach ($data as $row) {
		$this->table->add_row(
			$no++,
			$row->departemen,
			$row->keterangan,
			$this->Main_Model->default_action($row->id_dept)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function departemen_process()
	{
		$this->Main_Model->get_login();
		$id 		= $this->input->post('id');
		$departemen = $this->input->post('departemen');
		$keterangan = $this->input->post('keterangan');
		$idp 		= $this->session->userdata('idp');

		if($departemen == "")
		{
			$result = array('status'=>'false','message'=>'Form masih ada yang kosong!');
		}
		else
		{
			$data 		= array(
				'departemen' 	=> $departemen,
				'keterangan' 	=> $keterangan,
				'idp' 			=> $idp
				);
			$result = array('status'=>'true','message'=>'Success!');
			$this->Karyawan_Model->departemen_process($data,$id);
		}
		echo json_encode($result);
	}

	function departemen_id($id)
	{
		$this->Main_Model->get_login();
		$data =	$this->Karyawan_Model->departemen_id($id);
		echo json_encode($data);
	}

	function delete_departemen()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->post('id');
		$data 	= array(
			'status' => 0
			);
		$this->Karyawan_Model->departemen_process($data,$id);
	}

}

/* End of file departemen.php */
/* Location: ./application/controllers/departemen.php */ ?>