<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
	}

	function index()
	{
		show_404();
	}

	function authentication($token='')
	{
		$today = date('Y-m-d');
		$data = $this->Main_Model->view_by_id('tb_authentication', array('token' => $token, 'expiration >=' => $today), 'row');

		$usr_id = isset($data->usr_id) ? $data->usr_id : '';
		$user = $this->Main_Model->view_by_id('users', array('usr_id' => $usr_id), 'row');

		$username = isset($user->usr_name) ? $user->usr_name : '';
		$nip = isset($user->nip) ? $user->nip : '';
		$id_cabang = isset($user->id_cabang) ? $user->id_cabang : '';
		$usr_AccType = isset($user->usr_AccType) ? $user->usr_AccType : '';


		$sess_array = array(
                'username' => $username,
                'nip' => $nip,
                'loggedin' => 1,
                'idp' => 1,
                'id_user' => $usr_id,
                'cabang' => $id_cabang,
                'acctype' => $usr_AccType
            );

		$this->session->set_userdata($sess_array);

		$ip = file_get_contents('http://api.ipify.org');

        $log = array(
            'username' => $username,
            'ip' => $ip 
            );

        $this->db->insert('log_login', $log);
		// print_r($data);exit;
		redirect(base_url(),'refresh');
	}

	function coba($param='')
	{
		echo 'abc'.$param;
	}

}

/* End of file auth.php */
/* Location: ./application/controllers/auth.php */ ?>