<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cabang extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Penggajian_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript 	= '
			<script>
					function reset()
					{
						$(".blank").val("");
					}
				'
				 .$this->Main_Model->default_loadtable('cabang/view_cabang')

				 .$this->Main_Model->notif()

				 .$this->Main_Model->post_data('cabang/cabang_process','save()','$("#form_cabang").serialize()','
				 	if (data.status == "true") {
				 		load_table();
						reset();
						toastr.success(data.message);
				 	} else {
						toastr.warning(data.message);
					}')

				 .$this->Main_Model->get_data('cabang/cabang_id','get_id(id)','
				 	$("#myModal").modal();
					$("#cabang").val(data.cabang);
					$("#kode").val(data.prefix);
				 	$("#keterangan").val(data.keterangan);
				 	$("#id").val(data.id_cab);')

				 .$this->Main_Model->default_delete_data('cabang/delete_cabang').'
			</script>
		';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','3'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
			);

		$this->load->view('template/header', $header);
        $this->load->view('karyawan/data_cabang');
        $this->load->view('template/footer', $footer);
	}

	function view_cabang()
	{
		$this->Main_Model->get_login();
        $idp   		= $this->session->userdata('idp');
        $data 		= $this->Karyawan_Model->view_cabang($idp);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Kode','Cabang','Keterangan','Action');
		$no =1;
        
        foreach ($data as $row) {
			$this->table->add_row(
				$no++,
				$row->prefix,
				$row->cabang,
				$row->keterangan,
				$this->Main_Model->default_action($row->id_cab)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function cabang_process()
	{
		$this->Main_Model->get_login();
		$id 		= $this->input->post('id');
		$cabang 	= $this->input->post('cabang');
		$keterangan = $this->input->post('keterangan');
		$kode = $this->input->post('kode');
		$idp 		= $this->session->userdata('idp');

		if($cabang == "" || $kode == "")
		{
			$message = '';
			$message .= ($cabang == "") ? 'Masukkan Nama Cabang <br>' : '';
			$message .= ($kode == "") ? 'Masukkan Kode Cabang <br>' : '';

			$result = array('status'=>'false','message'=> $message);
		}
		else
		{
			$data 		= array(
				'cabang' 		=> $cabang,
				'prefix' => $kode,
				'keterangan' 	=> $keterangan,
				'idp' 			=> $idp
				);
			$result = array('status'=>'true','message'=>'Success!');
			$this->Karyawan_Model->cabang_process($data,$id);
		}
		echo json_encode($result);
	}

	function cabang_id($id)
	{
		$this->Main_Model->get_login();
		$data =	$this->Karyawan_Model->cabang_id($id);
		echo json_encode($data);
	}

	function delete_cabang()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->post('id');
		$data 	= array(
			'status' => 0
			);
		$this->Karyawan_Model->cabang_process($data,$id);
	}

}

/* End of file cabang.php */
/* Location: ./application/controllers/cabang.php */ ?>