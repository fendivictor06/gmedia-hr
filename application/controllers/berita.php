<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
	}

	function index()
	{
		$this->Main_Model->all_login();
		$acctype = $this->session->userdata('acctype');
		$menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0','0','69') : $this->Main_Model->menu_user('0','0','160');
		
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
			);

		$this->load->view('template/header', $header);
		$this->load->view('karyawan/form_berita',$data);
	}

	function view_berita()
	{
		$this->Main_Model->all_login();
		$data = $this->Main_Model->view_by_id('tb_news', array(), 'result');
		$template = $this->Main_Model->tbl_temp('tb_berita');
		$this->table->set_heading('#', 'Judul', 'Pesan', 'Tanggal', 'Gambar', 'Action');
		$no = 1;
		foreach($data as $row) {
			$gambar = ($row->gambar != '') ? '<img src="'.base_url('assets/berita/'.$row->gambar).'" width="50">' : '';

			$this->table->add_row(
				$no++,
				$row->judul,
				$row->text,
				$this->Main_Model->format_tgl($row->tanggal),
				$gambar,
				$this->Main_Model->default_action($row->id)
			);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_berita()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$judul = $this->input->post('judul');
		$tgl = $this->input->post('tgl');
		$keterangan = $this->input->post('keterangan');
		$this->load->library('upload');

		if ($judul == '' || $tgl == '' || $keterangan == '') {
			$message = '';
			$status = false;
			if ($judul == '') $message .= 'Field Judul harus diisi <br>';
			if ($tgl == '') $message .= 'Field Tanggal harus diisi <br>';
			if ($keterangan == '') $message .= 'Field Pesan harus diisi <br>';
		} else {
			$condition = array();
			$username = $this->session->userdata('username');
			$time = 'insert_at';
			$user = 'user_insert';
			if ($id != '') {
				$condition['id'] = $id;
				$time = 'update_at';
				$user = 'user_update';
 			}

			$data = array(
				'judul' => $judul,
				'tanggal' => $this->Main_Model->convert_tgl($tgl),
				'text' => $keterangan,
				$time => now(), 
				$user => $username
			);

			$file_name = date('YmdHis');
			$config = $this->Main_Model->set_upload_options("./assets/berita/", "jpg|png|jpeg" ,"5048000",$file_name);
			$this->upload->initialize($config);
			if($this->upload->do_upload('gambar')) {
				$upload_file = $this->upload->data();
				$data['gambar'] = $upload_file['file_name'];
			}
			
			if ($id == '') {
				$this->notif_berita($judul, $keterangan);
			}
			
			$this->Main_Model->process_data('tb_news', $data, $condition);
			$status = true;
			$message = 'Success!';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);
		echo json_encode($result);
	}

	function notif_berita($title='', $text='')
	{
		ini_set('memory_limit', '-1');
        ini_set('MAX_EXECUTION_TIME', '-1');
        set_time_limit(0);
        
		$url = fcm_url();
		$key = fcm_key();

		$list_header[] = "Content-Type:application/json";
		$list_header[] = "Authorization:key=".$key;


		$parameter['notification']['title'] = $title;
		$parameter['notification']['text'] = $text;
		$parameter['notification']['click_action'] = 'ACT_NOTIF';
		$parameter['notification']['sound'] = 'content://settings/system/notification_sound';
		$parameter['to'] = '/topics/gmedia_news';
		$parameter['priority'] = 'high';

		$send_notif = $this->Main_Model->curl_notif($url, $parameter, $list_header);
	}

	function berita_id($id='')
	{
		$this->Main_Model->all_login();
		$condition = array(
			'id' => $id
		);
		$data = $this->db->query("
			SELECT *, 
			DATE_FORMAT(tanggal, '%d/%m/%Y') AS tgl 
			FROM (`tb_news`) 
			WHERE `id` = '$id'")->row();

		echo json_encode($data);
	}

	function delete_berita($id='')
	{
		$this->Main_Model->all_login();
		$condition = array('id' => $id);
		$hapus = $this->Main_Model->delete_data('tb_news', $condition);

		if ($hapus > 0) {
			$status = true;
			$message = 'Success !';
		} else {
			$status = false;
			$message = 'Gagal menghapus data';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);

		echo json_encode($result);
	}
}

/* End of file berita.php */
/* Location: ./application/controllers/berita.php */