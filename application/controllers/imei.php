<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Imei extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
		$this->load->model('Karyawan_Model');
	}

	function index()
	{
		$this->Main_Model->all_login();
		$acctype = $this->session->userdata('acctype');
		$menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0','0','69') : $this->Main_Model->menu_user('0','0','160');
		
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'nip' => $this->Main_Model->kary_cabang('', '', '-'),
			'penutup' => $this->Main_Model->close_page()
		);

		$this->load->view('template/header', $header);
		$this->load->view('karyawan/form_imei',$data);
	}

	function view_imei()
	{
		$this->Main_Model->all_login();
		$data = $this->Karyawan_Model->view_imei();
		$template = $this->Main_Model->tbl_temp('tb_imei');
		$this->table->set_heading('#', 'NIK', 'Nama', 'Imei', 'Action');
		$no = 1;
		foreach($data as $row) {
			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$row->imei,
				$this->Main_Model->default_action($row->id)
				);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_imei()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$nip = $this->input->post('nip');
		$imei = $this->input->post('imei');

		if ($nip == '' || $imei == '') {
			$message = '';
			$status = FASLE;

			if ($nip == '') $message .= 'Field Nama Wajib diisi. <br>';
			if ($imei == '') $message .= 'Field Imei wajib diisi. ';
		} else {
			$data = array(
				'nip' => $nip,
				'imei' => $imei
			);

			$condition = ($id != '') ? ['id' => $id] : [];
			$simpan = $this->Main_Model->process_data('tb_imei', $data, $condition);

			$status = TRUE;
			$message = 'Data berhasil disimpan';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);

		echo json_encode($result);
	}

	function id_imei($id='')
	{
		$this->Main_Model->all_login();

		$data = $this->Main_Model->view_by_id('tb_imei', ['id' => $id], 'row');

		echo json_encode($data);
	}

	function delete_imei($id='')
	{
		$this->Main_Model->all_login();

		$hapus = $this->Main_Model->delete_data('tb_imei', ['id' => $id]);
		if ($hapus > 0) {
			$status = TRUE;
			$message = 'Berhasil menghapus data';
		} else {
			$status = FASLE;
			$message = 'Gagal menghapus data';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);

		echo json_encode($result);
	}
}

/* End of file imei.php */
/* Location: ./application/controllers/imei.php */ ?>