<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Management_User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Penggajian_Model', '', TRUE);
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript 	= '
			<script>
				'.$this->Main_Model->notif().'

				 function load_table() {
				 	$.ajax({
				 		url : "'.base_url('management_user/view_management_user').'",
				 		beforeSend : function() {
				 			$("#imgload").removeClass("hidden");
				 			$("#add_new").addClass("hidden");
				 		},
				 		complete : function() {
				 			$("#imgload").addClass("hidden");
				 			$("#add_new").removeClass("hidden");
				 		},
				 		success : function(data) {
				 			$("#myTable").html(data);
				 			$("#dataTables-example").DataTable({
				 				responsive : true
				 			});
				 		}
				 	})
				 }

				 function reset(){
				 	$(".blank").val("");
				 	$("#level").val("Administrator").trigger("change");
				 	$("#username").attr("readonly", false);
				 	$(".bs-select").selectpicker("refresh");
				 }

				 function change_level() {
				 	level = $("#level").val();
				 	if(level == "Administrator") {
				 		$(".cabang_group").addClass("hidden");
				 		$(".level_group").addClass("hidden");
				 	} else {
				 		$(".cabang_group").removeClass("hidden");
				 	}
				 	change_approval();
				 }

				 function change_approval() {
				 	approval = $("#approval").val();
				 	if(approval == 1) {
				 		$(".level_group").removeClass("hidden");
				 	} else {
				 		$(".level_group").addClass("hidden");
				 	}
				 }

				 $("#level").change(function(){
				 	change_level();
				 });

				 $("#approval").change(function(){
				 	change_approval();
				 });

				 function show_password(id) {
				 	$.ajax({
				 		url : "'.base_url('management_user/user_id').'/"+id,
				 		type : "post",
				 		dataType : "json",
				 		success : function(data){
				 			notif("Password : "+data.pwd);
				 		}	
				 	});
				 }

				 '.$this->Main_Model->post_data(base_url('management_user/process_management_user'),'save()','$("#form_user").serialize()','
				 	if(data.status == true)
				 	{
				 		load_table();
				 		reset();
				 	} notif(data.message);')

				 .$this->Main_Model->get_data(base_url('management_user/user_id'),'get_id(id)','
				 	$("#myModal").modal();
				 	
				 	$("#level").val(data.usr_AccType).trigger("change");
				 	$("#id").val(data.usr_id);
				 	$("#nama").val(data.nama);
				 	$("#username").val(data.usr_name);
				 	$("#password").val(data.pwd);
				 	$("#approval").val(data.approval);
				 	kelas = data.kelas;
				 	if(kelas != null) {
					 	privilege = kelas.split(",");
					 	q = [];
		                for(i=0; i < privilege.length; i++) {
		                    q.push(privilege[i]);
		                }
		                $("#privilege").selectpicker("val", q);
		            } else {
		            	$("#privilege").selectpicker("deselectAll");
		            }
	                cabang = data.cabang;
	                if(cabang != null) {
		                id_cabang = cabang.split(",");
		                w = [];
		                for(i=0; i < id_cabang.length; i++) {
		                	w.push(id_cabang[i]);
		                }
		                $("#cabang").selectpicker("val", w);
		            } else {
		            	$("#cabang").selectpicker("deselectAll");
		            }
		            divisi = data.divisi;
		            if(divisi != null) {
		                id_divisi = divisi.split(",");
		                e = [];
		                for(i=0; i < id_divisi.length; i++) {
		                	e.push(id_divisi[i]);
		                }
		                $("#divisi").selectpicker("val", e);
		            } else {
		            	$("#divisi").selectpicker("deselectAll");
		            }

		            pengajuan = data.pengajuan;
		            if(pengajuan != null) {
		            	p = pengajuan.split(",");
		            	r = [];
		            	for(i=0; i < p.length; i++) {
		            		r.push(p[i]);
		            	}
		            	$("#pengajuan").selectpicker("val", r);
		            } else {
		            	$("#pengajuan").selectpicker("deselectAll");
		            }

		            maninfo_cabang = data.maninfo_cabang;
		            if(maninfo_cabang != null) {
		            	p = maninfo_cabang.split(",");
		            	r = [];
		            	for(i=0; i < p.length; i++) {
		            		r.push(p[i]);
		            	}
		            	$("#cabang_maninfo").selectpicker("val", r);
		            } else {
		            	$("#cabang_maninfo").selectpicker("deselectAll");
		            }
				 	
				 	$("#username").attr("readonly", true);
				 	change_level(); change_approval();')

				 .$this->Main_Model->default_delete_data(base_url('management_user/delete_user'))

				 .$this->Main_Model->default_select2().'

				 $(document).ready(function(){
				 	$(".bs-select").selectpicker({
			            iconBase: "fa",
			            tickIcon: "fa-check"
			        });
			        load_table();
				 });
			</script>
		';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','3'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_select2()
				          .$this->Main_Model->style_bs_select()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_select2()
				                  .$this->Main_Model->js_bs_select()
			);
		$idp = $this->session->userdata('idp');
		$q = $this->Karyawan_Model->view_cabang($idp);
		$w = $this->Penggajian_Model->staff_rule($idp);
		$e = $this->Karyawan_Model->view_divisi($idp);

		$cab = array();
		if($q) {
			foreach($q as $r){
				$cab[$r->id_cab] = $r->cabang;
			}
		}

		$level = array();
		if($w) {
			foreach($w as $r) {
				$level[$r->sal_pos] = $r->sal_pos;
			}
		}

		$divisi = array();
		if($e) {
			foreach($e as $r) {
				$divisi[$r->id_divisi] = $r->divisi;
			}
		}

		$data = array(
				'cabang' => $cab,
				'level' => $level,
				'divisi' => $divisi
			);
		$this->load->view('template/header', $header);
        $this->load->view('karyawan/management_user', $data);
        $this->load->view('template/footer', $footer);
	}

	function view_management_user()
	{
		$this->Main_Model->get_login();
		$data 		= $this->Karyawan_Model->view_management_user();
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Username','Nama','Cabang','Divisi','Level','Approval','Approval Level','Pengajuan Level','Action');
		$no =1;
        
        foreach ($data as $row) {
        ($row->usr_AccType == 'Administrator') ? $level = 'Head Office' : $level = 'Branch Office';
        ($row->approval == 1) ? $approval = 'Ya' : $approval = 'Tidak';

        $ex = '';
        if ($row->usr_AccType != 'Administrator') {
        	$ex = '	<li>
                    	<a href="'.base_url('setting_menu/menu/'.$row->usr_name).'" target="_blank">
                                <i class="icon-edit"></i> Setting Menu </a>
                    </li>';
        }

		$this->table->add_row(
			$no++,
			$row->usr_name,
			$row->nama,
			$row->cabang,
			// $row->cabang_maninfo,
			$row->divisi,
			$level,
			$approval,
			$row->kelas,
			$row->pengajuan,
			'<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                    	<li>
                    		<a href="javascript:;" onclick="show_password(' . $row->usr_id . ');">
                                <i class="icon-edit"></i> Lihat Password </a>
                    	</li>
                    	'.$ex.'
                        <li>
                            <a href="javascript:;" onclick="get_id(' . $row->usr_id . ');">
                                <i class="icon-edit"></i> Update </a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="delete_data(' . $row->usr_id . ');">
                                <i class="icon-delete"></i> Delete </a>
                        </li>
                    </ul>
            </div>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_management_user()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$level = $this->input->post('level');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$appr = $this->input->post('approval');

		//array
		$privilege = $this->input->post('privilege');
		$cabang = $this->input->post('cabang');
		$divisi = $this->input->post('divisi');
		$pengajuan = $this->input->post('pengajuan');
		$cabang_maninfo = $this->input->post('cabang_maninfo');


		if($level == '' || $nama == '' || $username == '' || $password == '') {
			$result = array('status' => false,'message' => 'Form masih ada yang kosong!');
		} else {
			$checkexists = $this->Main_Model->view_by_id('users', array('usr_name' => $username));
			if($checkexists && $id == '') {
				$result = array('status' => false,'message' => 'Username sudah dipakai!');
			} else {
				($level == 'Operator') ? $cabang = $cabang : $cabang = 0;
				($level == 'Operator') ? $approval = $appr : $approval = 1;
				$data 	= array(
					'usr_name' 		=> $username,
					'usr_pass' 		=> $password,
					// 'id_cabang' 	=> $cabang,
					'nama' 			=> $nama,
					'usr_AccType' 	=> $level,
					'approval' 		=> $approval
					);

				$this->Main_Model->delete_data('tb_user_cabang', array('usr_name' => $username));
				$this->Main_Model->delete_data('tb_level_approval', array('usr_name' => $username));
				$this->Main_Model->delete_data('tb_user_divisi', array('usr_name' => $username));
				$this->Main_Model->delete_data('tb_level_pengajuan', array('usr_name' => $username));
				$this->Main_Model->delete_data('tb_maninfo_cabang', array('usr_name' => $username));
				if($level == 'Operator') {
					for($i = 0; $i < count($cabang); $i++) {
						$data[$i] = array(
							'usr_name' => $username,
							'id_cabang' => $cabang[$i]
							); 
						if ($cabang[$i] != '') {
							$this->Main_Model->process_data('tb_user_cabang', $data[$i]);
						}
					}

					for($i = 0; $i < count($divisi); $i++) {
						$data[$i] = array(
							'usr_name' => $username,
							'id_divisi' => $divisi[$i]
							);
						if ($divisi[$i]) {
							$this->Main_Model->process_data('tb_user_divisi', $data[$i]);
						}
					}

					for($i = 0; $i < count($pengajuan); $i++) {
						$data[$i] = array(
							'usr_name' => $username,
							'sal_pos' => $pengajuan[$i]
							);
						if ($pengajuan[$i]) {
							$this->Main_Model->process_data('tb_level_pengajuan', $data[$i]);
						}
					}

					for($i = 0; $i < count($cabang_maninfo); $i++) {
						$data[$i] = array(
							'usr_name' => $username,
							'id_cabang' => $cabang_maninfo[$i]
							);
						if ($cabang_maninfo[$i]) {
							$this->Main_Model->process_data('tb_maninfo_cabang', $data[$i]);
						}
					}

					if($approval == 1) {
						for($i = 0; $i < count($privilege); $i++) {
							$data[$i] = array(
								'usr_name' => $username,
								'sal_pos' => $privilege[$i]
								);

							$this->Main_Model->process_data('tb_level_approval', $data[$i]);
						}
					}
				}

				$this->Karyawan_Model->process_management_user($data, $id);
				$result = array('status' => true, 'message' => 'Success!');
			}
		}
		echo json_encode($result);
	}

	function user_id($id)
	{
		$this->Main_Model->get_login();
		$data =	$this->Karyawan_Model->user_id($id);
		echo json_encode($data);
	}

	function delete_user()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$this->Karyawan_Model->delete_user($id);
	}
}

/* End of file management_user.php */
/* Location: ./application/controllers/management_user.php */ ?>