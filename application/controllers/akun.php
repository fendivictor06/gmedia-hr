<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akun extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Penggajian_Model', '', TRUE);
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Cuti_Model', '', TRUE);
        $this->load->model('Tunjangan_Model', '', TRUE);
        $this->load->model('Akun_Model', '', TRUE);
        $this->load->model('Dashboard_Model', '', TRUE);
    }

    function header(){
		$menu =
			'
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">'
			;
		return $menu;
	}

	function footer(){
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
        		<script src="'.base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'"></script>';

		return $footer;
	}

	function dashboard()
	{	
		$this->Main_Model->guest_login();
		$excel_url = base_url('download_excel/terlambat_today');
		$javascript = '
			<script>
				// download terlambat hari ini 
				function unduh(){
                    window.location.href="'.$excel_url.'";
                }

                // view terlambat hari ini 
				function table_terlambat_today() {
					$.ajax({
						url : "'.base_url('terlambat/view_terlambat_today').'",
						success : function(data) {
							$("#table_terlambat").html(data);
							$("#terlambat").DataTable({
								responsive : true
							});
						}
					})
				}

				// view scan hari ini 
				function table_scandate() {
					$.ajax({
						url : "'.base_url('scanlog/view_scandate').'?date='.date('d/m/Y').'",
						success : function(data) {
							$("#table_scan_date").html(data);
							$("#tb_scanlog").DataTable({
								responsive : true
							});
						}
					});
				}

				// view klarifikasi absensi
				function table_mangkir() {
					$.ajax({
						url : "'.base_url('main/view_mangkir').'",
						success : function(data) {
							$("#table_mangkir").html(data);
							$("#mangkir").DataTable({
								responsive : true
							});
						}
					})
				}

				// view verifikasi perdin
				function table_perdin() {
					$.ajax({
						url : "'.base_url('main/view_perdin').'",
						success : function(data) {
							$("#table_perdin").html(data);
							$("#perdin").DataTable({
								responsive : true
							});
						}
					})
				}

				// view klarifikasi terlambat
				function table_terlambat() {
					$.ajax({
						url : "'.base_url('terlambat/view_terlambat').'",
						success : function(data) {
							$("#table_klarifikasi_terlambat").html(data);
							$("#tb_klarifikasi_terlambat").DataTable({
								responsive : true
							});
						}
					});
				}

				// view lembur
				function table_lembur() {
					$.ajax({
						url : "'.base_url('tunjangan/view_approval_lembur').'",
						success : function(data) {
							$("#table_lembur").html(data);
							$("#tb_lembur").DataTable({
								responsive : true
							});
						}
					});
				}

				// view pengajuan
				function table_pengajuan() {
					$.ajax({
						url : "'.base_url('pengajuan_karyawan/view_approval_pengajuan').'",
						success : function(data) {
							$("#table_pengajuan").html(data);
							$("#tb_pengajuan").DataTable({
								responsive : true
							});
						}
					});
				}

				// view cuti
				function table_cuti() {
					$.ajax({
						url : "'.base_url('cuti/verifikasi_cuti').'",
						success : function(data) {
							$("#table_cuti").html(data);
							$("#tb_klarifikasi_cuti").DataTable({
								responsive : true
							});
						}
					})
				}

				// view cuti khusus
				function table_cuti_khusus() {
					$.ajax({
						url : "'.base_url('cuti/verifikasi_cuti_khusus').'",
						success : function(data) {
							$("#table_cuti_khusus").html(data);
							$("#tb_klarifikasi_cuti_khusus").DataTable({
								responsive : true
							});
						}
					});
				}

				// view finalisasi
				function table_finalisasi() {
					$.ajax({
						url : "'.base_url('pengajuan_karyawan/view_pengajuan_karyawan_dashboard').'",
						success : function(data) {
							$("#table_finalisasi").html(data);
							$("#tb_finalisasi_karyawan").DataTable({
								responsive : true
							});
						}
					});
				}

				// view pulang awal 
				function table_pulang_awal() {
					$.ajax({
						url : "'.base_url('pulang_awal/verifikasi_pulang_awal').'",
						success : function(data) {
							$("#table_pulang_awal").html(data);
							$("#tb_pulang_awal").DataTable({
								responsive : true
							});
						}
					})
				}

				// view keluar kantor
				function table_keluar_kantor() {
					$.ajax({
						url : "'.base_url('akun/verifikasi_keluar_kantor').'",
						success : function(data) {
							$("#table_tinggal_kerja").html(data);
							$("#tb_keluar_kantor").DataTable({
								responsive : true
							});
						}
					})
				}

				// load all view
				$(document).ready(function(){
					load_all();
					$(".form_datetime").datetimepicker({
			            autoclose : !0,
			            format : "dd/mm/yyyy hh:ii",
			            pickerPosition : "bottom-left"
			        });
				});

				function load_all(){
					table_terlambat_today(), table_mangkir(), table_perdin(), table_terlambat(), table_lembur(), table_pengajuan(), table_cuti(), table_scandate(), table_cuti_khusus(), table_finalisasi(), table_pulang_awal(), table_keluar_kantor();
				}


				// show modal klarifikasi terlambat 
				function get_terlambat(id) {
		            $.ajax({
		                url : "../terlambat/get_id_terlambat/"+id,
		                dataType : "json",
		                success : function(data) {
		                    $("#id_terlambat").val(data.id), $("#id_ab_t").val(data.id_ab), $("#nama_t").val(data.nama), $("#tgl_t").val(data.tanggal), $("#scan_masuk_t").val(data.masuk), $("#klarifikasi_modal").modal();
		                }
		            });
		        }

		        // show modal klarifikasi absensi
				function get_id(id){
					$.ajax({
				    		url : "../absensi/presensi_id",
				    		type : "GET",
				    		data : {"id" : id },
				    		dataType : "JSON",
				    		success : function(dat){
				    			var jam_masuk = (dat.jam_masuk == "00/00/0000 00:00") ? "" : dat.jam_masuk;
                				var jam_pulang = (dat.jam_pulang == "00/00/0000 00:00") ? "" : dat.jam_pulang;
								$("#myModal").modal(), $("#nama").val(dat.nama), $("#id").val(dat.id), $("#nip").val(dat.nip), $("#tgl").val(dat.tanggal), $("#klarifikasi").val(dat.klarifikasi), $("#ket").val(dat.ket), $("#scan_masuk").val(jam_masuk), $("#scan_pulang").val(jam_pulang), $("#id_ab").val(dat.id_ab), $("#nip").val(dat.nip);
	                            $("#nama").val(dat.nama), $("#kuota_cuti").html("");

	                            (dat.sakit == 1) ? $("#absen").val("sakit") : ((dat.ijin == 1) ? $("#absen").val("ijin") : ((dat.mangkir == 1) ? $("#absen").val("mangkir") : $("#absen").val("")));

				    		}
				    	});
					}

				// submit terlambat 
				$("#form_klarifikasi").submit(function(event){
					event.preventDefault();
					var d = new FormData($(this)[0]);
					$.ajax({
						url : "../terlambat/klarifikasi",
						type : "post",
						data : d,
						dataType : "json",
						async : false,
						chache : false,
						contentType : false,
						processData : false,
						beforeSend : function() {
			                $("#save_terlambat").attr("disabled", true);
			            },
			            complete : function() {
			                $("#save_terlambat").attr("disabled", false);
			            },
						success : function(data) {
							if(data.status == true) {
								document.getElementById("form_klarifikasi").reset();
								$("#klarifikasi_modal").modal("toggle");
								toastr.success("", data.message);
							} else {
								toastr.warning("", data.message);
							}
							load_all();
							// bootbox.alert(data.message);
						}
					});
					return false;
				});

				// submit presensi
                $("#form_presensi").submit(function(event){
                    event.preventDefault();
                    var d = new FormData($(this)[0]);
                    $.ajax({
                        url : "../absensi/presensi_process",
                        type : "post",
                        data : d,
                        dataType : "json",
                        async : false,
                        cache : false,
                        contentType : false,
                        processData : false,
                        beforeSend : function() {
			                $("#save_button").attr("disabled", true);
			            },
			            complete : function() {
			                $("#save_button").attr("disabled", false);
			            },
                        success : function(data) {
                            if(data.status == true) {
                            	document.getElementById("form_presensi").reset();
                                $("#myModal").modal("toggle");
                                toastr.success("", data.message);
                            } else {
                            	toastr.warning("", data.message);
                            }
                            load_all();
                            // bootbox.alert(data.message);
                        }
                    });
                    return false;
                });

				// verifikasi perdin
				function verif(id) {
			        bootbox.dialog({
			            message : "Yakin ingin memverifikasi data?",
			            title : "Verifikasi Data",
			            buttons :{
			                danger : {
			                    label : "Verifikasi",
			                    className : "red",
			                    callback : function(){
			                        $.ajax({
			                            url : "../perdin/verifikasi/"+id,
			                            dataType : "json",
			                            success : function(data) {
			                                load_all();
			                                bootbox.alert(data.message);
			                            }
			                        });
			                    }
			                },
			                main : {
			                    label : "Cancel",
			                    className : "blue",
			                    callback : function(){
			                        return true;
			                    }
			                }
			            }
			        })
			    }

			    // verifikasi lembur
			    function proses_lembur(id, val) {
					$.ajax({
						url : "../tunjangan/approval_lembur/"+id+"/"+val,
						type : "post"
					})
				}

			    function app_lembur(id) {
					bootbox.dialog({
                        message : "Yakin ingin memverifikasi Lembur?",
                        title : "Verifikasi Lembur",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses_lembur(id, 1);
                                	bootbox.alert("Lembur telah disetujui !");
                                	load_all();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses_lembur(id, 2);
                                	bootbox.alert("Lembur telah ditolak !");
                                	load_all();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

				// verifikasi lembur
			    function proses_pengajuan(id, val) {
					$.ajax({
						url : "../pengajuan_karyawan/approval_pengajuan/"+id+"/"+val,
						type : "post"
					})
				}

			    function app_pengajuan(id) {
					bootbox.dialog({
                        message : "Yakin ingin memverifikasi Pengajuan Karyawan?",
                        title : "Verifikasi Pengajuan Karyawan",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses_pengajuan(id, "approve");
                                	bootbox.alert("Pengajuan telah disetujui !");
                                	load_all();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses_pengajuan(id, "reject");
                                	bootbox.alert("Pengajuan telah ditolak !");
                                	load_all();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

			    // view kuota cuti
			    $("#absen").change(function(){
			        value = $(this).val();
			        if (value == "cuti") {
			            nip = $("#nip").val();
			            $.ajax({
			                url : "'.base_url('cuti/cek_qt').'/"+nip,
			                success : function(data) {
			                    $("#kuota_cuti").html(data);
			                }
			            });
			            $(".masuk").addClass("hidden");
			            $(".cuti-khusus").addClass("hidden");
			            $(".klarifikasi").removeClass("hidden");
			        } else if (value == "cuti_khusus") {
			            $(".masuk").addClass("hidden");
			            $("#kuota_cuti").html("");
			            $(".cuti-khusus").removeClass("hidden");
			            $(".klarifikasi").addClass("hidden");
			        } else if (value == "masuk") {
			            $(".masuk").removeClass("hidden");
			            $("#kuota_cuti").html("");
			            $(".cuti-khusus").addClass("hidden");
			            $(".klarifikasi").removeClass("hidden");
			        } else {
			            $(".masuk").addClass("hidden");
			            $("#kuota_cuti").html("");
			            $(".cuti-khusus").addClass("hidden");
			            $(".klarifikasi").removeClass("hidden");
			        }
			    });

			    // approval cuti
			    function proses_cuti(id, val) {
					$.ajax({
						url : "'.base_url('cuti/proses_approval').'/"+id+"/"+val,
						type : "post",
						success : function() {
							load_all();
						}
					})
				} 

				function approval_cuti(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses cuti?",
                        title : "Proses Cuti",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses_cuti(id, "approve");
                                	bootbox.alert("Cuti telah disetujui !");
                                	load_all();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses_cuti(id, "reject");
                                	bootbox.alert("Cuti telah ditolak !");
                                	load_all();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

				// approval pulang awal 
				function proses_pulang(id, val) {
					$.ajax({
						url : "'.base_url('pulang_awal/proses_approval').'/"+id+"/"+val,
						type : "post",
						success : function() {
							load_all();
						}
					})
				} 

				function approval_pulang(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses pulang awal?",
                        title : "Proses Pulang Awal",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses_pulang(id, "approve");
                                	bootbox.alert("Pulang Awal telah disetujui !");
                                	load_all();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses_pulang(id, "reject");
                                	bootbox.alert("Pulang Awal telah ditolak !");
                                	load_all();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

				// ijin keluar kantor
				function proses_tinggal(id, val) {
					$.ajax({
						url : "'.base_url('absensi/approval_ijinjam').'/"+id+"/"+val,
						type : "post",
						success : function() {
							load_all();
						}
					})
				} 

				function approval_tinggal(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses Keluar Kantor?",
                        title : "Proses Keluar Kantor",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses_tinggal(id, "approve");
                                	bootbox.alert("Keluar Kantor telah disetujui !");
                                	load_all();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses_tinggal(id, "reject");
                                	bootbox.alert("Keluar Kantor telah ditolak !");
                                	load_all();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}


				// approval cuti khusus 
				function proses_cuti_khusus(id, val) {
					$.ajax({
						url : "'.base_url('cuti/proses_approval_khusus').'/"+id+"/"+val,
						type : "post",
						success : function() {
							load_all();
						}
					})
				} 

				function approval_cuti_khusus(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses cuti?",
                        title : "Proses Cuti",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses_cuti_khusus(id, "approve");
                                	bootbox.alert("Cuti telah disetujui !");
                                	load_all();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses_cuti_khusus(id, "reject");
                                	bootbox.alert("Cuti telah ditolak !");
                                	load_all();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

				function change(id, val) {
			        bootbox.dialog({
			            message : "Apakah Calon Karyawan sudah ttd?",
			            title : "TTD Pengajuan Karyawan",
			            buttons :{
			                success : {
			                    label : "Sudah",
			                    className : "green",
			                    callback : function(){
			                        $.ajax({
			                            url : "'.base_url('pengajuan_karyawan/sudah_ttd').'/"+id+"/"+val,
			                            success : function() {
			                                bootbox.alert("Status telah diubah !");
			                                table_finalisasi();
			                            }
			                        })
			                    }    
			                },
			                
			                main : {
			                    label : "Belum",
			                    className : "blue",
			                    callback : function(){
			                        return true;
			                    }
			                }
			            }
			        });
			    }

			    function unduh_file(id) {
			    	$("#Mdownload").modal();
			    	document.getElementById("fdownload").reset();
			    	$("#id_file").val(id);
			    	$("#file_nip").find("option").remove().end();
			    	$.getJSON("'.base_url('download_file/cari_nip').'/"+id, function(jsonResult){
			    		$("#file_nip").attr("enabled","true");
			    		if (jsonResult != null) {
				    		$.each(jsonResult, function(){
				    			$("#file_nip").append(
				                    $("<option></option>").text(this.nama).val(this.nip)
				                );
				    		});
			    		}
			    	});
			    }

			    function simpan() {
			    	tgl_mulai = $("#tgl_mulai").val();
			    	tgl_akhir = $("#tgl_akhir").val();
			    	if (tgl_mulai == "" || tgl_akhir == "") {
			    		bootbox.alert("Tanggal Mulai & Akhir tidak boleh kosong!");
			    	} else {
				    	data = $("#fdownload").serialize();
				    	window.open(
				    		"'.base_url('download_file/file_finalisasi').'?"+data,
				    		"_blank"
				    	);
				    }
			    }

			    $(document).ready(function(){
			    	$(".select2").select2();
			    });

			    '.$this->Main_Model->default_datepicker()
				 .$this->Main_Model->timepicker().'
			</script>
		';
		$header 	= array(
			'style'	=> $this->header(),
			'menu' => $this->Main_Model->menu_user('0','0','70')
			);
		$footer 	= array(
			'js' 	=> $this->footer(),
			'javascript' => $javascript
			);

		// $terlambat_hari_ini = array(
		// 	'angka' => $this->Dashboard_Model->jml_telat_hari_ini(),
		// 	'link' => '#terlambat_today',
		// 	'warna' => 'blue',
		// 	'icon' => 'fa fa-clock-o',
		// 	'label' => 'Terlambat Hari ini'
		// 	);

		// $scandate = array(
		// 	'angka' => $this->Absensi_Model->jumlah_scandate(date('Y-m-d')),
		// 	'link' => '#scandate_today',
		// 	'warna' => 'default',
		// 	'icon' => 'fa fa-clock-o',
		// 	'label' => 'Scan Hari ini'
		// );

		$mangkir = array(
			'angka' => $this->Dashboard_Model->jumlah_mangkir(),
			'link' => '#portlet_mangkir',
			'warna' => 'red',
			'icon' => 'fa fa-times',
			'label' => 'Mangkir '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y')))).' s/d '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y'))))
			);

		$terlambat = array(
			'angka' => $this->Dashboard_Model->jumlah_terlambat(),
			'link' => '#portlet_terlambat',
			'warna' => 'green',
			'icon' => 'fa fa-clock-o',
			'label' => 'Terlambat '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y')))).' s/d '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y'))))
			);

		$lembur = array(
			'angka' => $this->Dashboard_Model->jumlah_lembur(),
			'link' => '#portlet_lembur',
			'warna' => 'grey',
			'icon' => 'fa fa-money',
			'label' => 'Lembur'
			);

		$cuti = array(
			'angka' => $this->Dashboard_Model->jumlah_cuti(),
			'link' => '#portlet_cuti',
			'warna' => 'purple',
			'icon' => 'fa fa-plane',
			'label' => 'Cuti'
			);

		$pulang_awal = array(
			'angka' => $this->Dashboard_Model->jumlah_pulang_awal(),
			'link' => '#portlet_pulang_awal',
			'warna' => 'yellow',
			'icon' => 'fa fa-car',
			'label' => 'Pulang Awal'
			);

		$tinggal_kerja = array(
			'angka' => $this->Dashboard_Model->jumlah_tinggal_kerja(),
			'link' => '#portlet_tinggal_kerja',
			'warna' => 'white',
			'icon' => 'fa fa-group',
			'label' => 'Keluar Kantor'
			);

		$cuti_khusus = array(
			'angka' => $this->Dashboard_Model->jumlah_cuti_khusus(),
			'link' => '#portlet_cuti_khusus',
			'warna' => 'red',
			'icon' => 'fa fa-plane',
			'label' => 'Cuti Khusus'
			);

		$per = $this->Main_Model->per_skr();
		$tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';
        $periode = isset($per->periode) ? $per->periode : '';

		$kontrak = array(
			'angka' => $this->Main_Model->jml_kontrak_baru($tgl_awal, $tgl_akhir)->jml,
			'link' => base_url('main/kary_kontrak_baru'),
			'warna' => 'purple',
			'icon' => 'fa fa-newspaper-o',
			'label' => 'Kontrak Baru Periode '.$periode
		);

		// $finalisasi = array(
		// 	'angka' => $this->Dashboard_Model->jml_karyawan_finalisasi($tgl_awal, $tgl_akhir),
		// 	'link' => '#portlet_finalisasi',
		// 	'warna' => 'blue',
		// 	'icon' => 'fa fa-group',
		// 	'label' => 'Finalisasi Karyawan Periode '.$periode
		// );

		// $counter = array($terlambat_hari_ini, $scandate, $mangkir, $terlambat, $lembur, $cuti, $perdin, $pengajuan, $kontrak, $cuti_khusus, $finalisasi);
		$counter = array($mangkir, $terlambat, $lembur, $cuti, $pulang_awal, $tinggal_kerja, $kontrak, $cuti_khusus);

		$q = $this->Main_Model->session_cabang();
		$cabang = array();
		if($q) {
			foreach($q as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang[$d->id_cab] = $d->cabang;
			}
		}

		$tipe_cuti = $this->Cuti_Model->tipe_cuti();

		$arr_klarifikasi = [];
		$opt_klarifikasi = $this->Main_Model->view_by_id('ms_opt_klarifikasi', ['status' => 1], 'result');
		if (! empty($opt_klarifikasi)) {
			foreach ($opt_klarifikasi as $row) {
				$arr_klarifikasi[$row->tag] = $row->label;
			}
		}

		$data = array(
			'jml_telat' => $this->Akun_Model->jumlah_telat()->jumlah,
			'jml_absen' => $this->Akun_Model->jumlah_absen()->jumlah,
			'jml_mangkir' => $this->Akun_Model->jumlah_mangkir()->jumlah,
			'tipe' => $tipe_cuti,
			'absen' => $arr_klarifikasi,
			'counter' => $counter,
			'cabang' => $cabang
		);

		$this->load->view('akun/header',$header);
		$this->load->view('akun/dashboard',$data);
		$this->load->view('akun/footer',$footer);
	}

	function gaji()
	{
		$this->Main_Model->guest_login();
		$nip 		= $this->session->userdata('nip');
		$javascript = '
				<script>
					function load_table(){
						var nip = {"nip":'.$nip.'}
						$.ajax({
							url 	: "'.base_url('gaji/view_gaji_nip').'",
							data 	: nip,
							type 	: "POST",
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-example").DataTable({
				        			responsive: true
				    			});
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Internal Server Error");
							} 
						});
					}
					load_table();

					function lihat_detail(nip, id_per)
				    {
				    	$.ajax({
				    		url 	: "'.base_url('gaji/payrol_detail_hist').'/"+nip+"/"+id_per,
				    		success	: function(data){
				    			$("#payroll").modal();
				    			$("#target_modal").html(data);
				    		},
				    		error 	: function(jqXHR,textStatus,errorThrown){
				    			'.$this->Main_Model->notif500().'
				    		}
				    	});
				    }
				</script>';
		$header 	= array(
			'style'			=> $this->header(),
			'menu' 		=> $this->Main_Model->menu_user('0','0','71')
			);
		$footer 	= array(
			'js' 			=> $this->footer(),
			'javascript'	=> $javascript
			);
		$data 		= array(
			'periode' 		=> $this->Main_Model->periode_opt()
			);
		$this->load->view('akun/header',$header);
		$this->load->view('akun/gaji',$data);
		$this->load->view('akun/footer',$footer);
	}

	function absensi()
	{
		$this->Main_Model->guest_login();
		$javascript = '
				<script>
					$(document).ready(function(){
						$(".select2").select2();
					});

					function load_table(){
						var tgl_awal = $("#tgl_awal").val();
						var tgl_akhir = $("#tgl_akhir").val();
						var param 	= {
							"tgl_awal" : tgl_awal,
							"tgl_akhir" : tgl_akhir
						}
						$.ajax({
							url : "'.base_url('akun/view_absensi_presensi_nip').'",
							data : param,
							type : "GET",
							beforeSend : function(){
								$("#tampil").addClass("hidden");
								$("#unduh").addClass("hidden");
								$("#unduh_presensi").addClass("hidden");
								$("#imgload").removeClass("hidden");
							},
							complete : function(){
								$("#tampil").removeClass("hidden");
								$("#unduh").removeClass("hidden");
								$("#unduh_presensi").removeClass("hidden");
								$("#imgload").addClass("hidden");
							},
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-absensi").DataTable({
				        			responsive: true
				    			});
								$("#dataTables-presensi").DataTable({
				        			responsive: true
				    			});
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Gagal mengambil data!");
							} 
						});
					}
					// load_table();

					$("#tampil").click(function(){
	                    load_table();
	                });

					function get_id(id){
						id = {
							"id" : id
						}
						$.ajax({
				    		url : "' . base_url('absensi/presensi_id') . '",
				    		type : "GET",
				    		data : id,
				    		dataType : "JSON",
				    		success : function(dat){
								$("#myModal").modal();
								$("#nama").val(dat.nama);
								$("#id").val(dat.id);
								$("#nip").val(dat.nip);
								$("#tgl").val(dat.tanggal);
								$("#klarifikasi").val(dat.klarifikasi);
								$("#ket").val(dat.ket);
								if(dat.sakit == 1) {
									$("#absen").val("sakit");
								} else if(dat.ijin == 1) {
									$("#absen").val("ijin");
								} else if(dat.mangkir == 1){ 
									$("#absen").val("mangkir");
								} else {
									$("#absen").val("alpha");
								}
								$("#scan_masuk").val(dat.jam_masuk);
	                            $("#scan_pulang").val(dat.jam_pulang);
	                            $("#id_ab").val(dat.id_ab);
	                            $("#nip").val(dat.nip);
	                            $("#nama").val(dat.nama);
				    		},
				    		error : function(jqXHR, textStatus, errorThrown){
				    			alert("Internal Server Error");
				    		}
				    	});
					}
					
					$(".date-picker").datepicker({
    					autoclose : true,
    					orientation : "bottom"
    				});

					$("#form_presensi").submit(function(event){
						event.preventDefault();
						var d = new FormData($(this)[0]);
						$.ajax({
							url : "'.base_url('absensi/presensi_process').'",
							type : "post",
							data : d,
							dataType : "json",
							async : false,
							cache : false,
							contentType : false,
							processData : false,
							success : function(data) {
								if(data.status == true) {
									load_table(), $("#myModal").modal("toggle");
								}
								bootbox.alert(data.message);
							}
						});
						return false;
					});

					$("#absen").change(function(){
				    	nip = $("#nip").val();
				    	$.ajax({
				    		url : "../cuti/cek_qt/"+nip,
				    		success : function(data) {
				    			$("#kuota_cuti").html(data);
				    		}
				    	});
				    });

				    $("#unduh").click(function(){
						var tgl_awal = $("#tgl_awal").val();
						var tgl_akhir = $("#tgl_akhir").val();
						window.location.href="'.base_url('download_excel/data_absensi').'?tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir;
					});

					$("#unduh_presensi").click(function(){
						var tgl_awal = $("#tgl_awal").val();
						var tgl_akhir = $("#tgl_akhir").val();
						window.location.href="'.base_url('download_excel/data_presensi_cabang').'?tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir;
					});

					'.$this->Main_Model->timepicker().'
				</script>';
		$header 	= array(
			'style'			=> $this->header(),
			'menu' 		=> $this->Main_Model->menu_user('0','0','72')
			);
		$footer 	= array(
			'js' 			=> $this->footer(),
			'javascript'	=> $javascript
			);
		$data 		= array(
			'periode' 	=> $this->Main_Model->periode_opt(),
			'absen' 	=> array(
                'sakit' 	=> 'Sakit',
                'ijin' 		=> 'Ijin',
                'mangkir' 	=> 'Mangkir',
                'masuk' 	=> 'Masuk',
                'cuti' => 'Cuti'
            	)
			);
		$this->load->view('akun/header', $header);
		$this->load->view('akun/absensi', $data);
		$this->load->view('akun/footer', $footer);
	}

	function presensi_id()
    {
    	$this->Main_Model->guest_login();
        $id   = $this->input->get('id');
        $data = $this->Absensi_Model->presensi_by_id($id);
        
        echo json_encode($data);
    }

    function presensi_process()
    {
    	$this->Main_Model->guest_login();
    	$this->load->library('upload');
    	$id = $this->input->post('id');
    	$tgl = $this->input->post('tgl');
    	$absen = $this->input->post('absen');
    	$ket = $this->input->post('ket');
    	$klarifikasi = $this->input->post('klarifikasi');
    	$nip = $this->input->post('nip');
    	$filename = 'File Presensi '.$nip.' '.date('Y-m-d');

    	$config 	= $this->Main_Model->set_upload_options("./assets/presensi/","*","5048000",$filename);
		$this->upload->initialize($config);

    	if($tgl == '' || $absen == '' || $nip == '') {
    		$result = array('status' => false , 'message' => 'Form masih ada yang kosong!');
    	} else {
    		$data = array(
    			'tgl' => $this->Main_Model->convert_tgl($tgl),
    			'ket' => $ket,
    			'klarifikasi' => $klarifikasi
    			);

    		$data['sakit'] = 0;
    		$data['ijin'] = 0;
    		$data['mangkir'] = 0;
    		$data['alpha'] = 0;

    		if($absen == 'sakit') {
    			$data['sakit'] = 1;
    		} elseif($absen == 'ijin') {
    			$data['ijin'] = 1;
    		} elseif($absen == 'mangkir') {
    			$data['mangkir'] = 1;
    		} else {
    			// $data['mangkir'] = 1;
    		}

    		if($this->upload->do_upload('file_upload')) {
    			$file = $this->upload->data();
				$data['file'] = $file['file_name'];
    		}

    		$this->Absensi_Model->update_presensi($data, $id);
    		$result = array('status' => true , 'message' => 'Success!');
    	}
    	echo json_encode($result);
    }

	function view_absensi_presensi_nip()
	{
		$this->Main_Model->all_login();
		// $tahun = $this->input->get('tahun');
		// $bulan = $this->input->get('bulan');
		$tgl_awal = $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->get('tgl_akhir');
		// $p = periode($tahun, $bulan);
		// $periode = isset($p->qd_id) ? $p->qd_id : '';
        $absensi = $this->Absensi_Model->view_absensi_nip($tgl_awal, $tgl_akhir);
        $presensi = $this->Absensi_Model->view_presensi_cab($tgl_awal, $tgl_akhir);
        // $period = $this->Absensi_Model->view_periode($periode);
        $is_approval = $this->Main_Model->is_approval();
        
        $tbl = '
        	<ul class="nav nav-tabs">
        		<li class="active">
        			<a href="#tab_1_1" data-toggle="tab"> Absensi </a>
        		</li>
        		<li>
        			<a href="#tab_1_2" data-toggle="tab"> Presensi </a>
        		</li>
        	</ul>
        	<div class="tab-content">
        		<div class="tab-pane fade active in" id="tab_1_1"> 
        			<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-absensi">
        				<thead>
        					<tr>
        						<th>No</th>
				        		<th>Tanggal</th>
				        		<th>Nama</th>
				        		<th>Cabang</th>
				        		<th>Hari Kerja</th>
				        		<th>Masuk</th>
				        		<th>Cuti</th>
				        		<th>Sakit</th>
				        		<th>Ijin</th>
				        		<th>Mangkir</th>
				        		<th>Lembur</th>
				        		<th>Jam Lembur</th>
        					</tr>
        				</thead>
        				<tbody>';
        $j = 1;
        foreach ($absensi as $row){
            // $potong = $row->jml_sakit + $row->jml_ijin + $row->jml_mangkir + $row->jml_cuti + $row->jml_potong + $row->jml_resign;
            $potong = $row->jml_sakit + $row->jml_mangkir + $row->jml_cuti + $row->jml_potong + $row->jml_resign;
            if($row->jml_dayoff == 0) {
            	$j_masuk = $row->jml_kerja - $potong;
            	$hari_kerja = $row->jml_kerja;
            } else {
            	$j_masuk = $row->hari - ($potong + $row->jml_dayoff);
            	$hari_kerja = $row->hari;
            }
            $tbl .= '
			<tr>
				<td>'.$j++.'</td>
				<td>'.$row->tgl_awal.' s/d '.$row->tgl_akhir.'</td>
				<td>'.$row->nama.'</td>
				<td>'.$row->cabang.'</td>
				<td>'.$hari_kerja.'</td>
				<td>'.$j_masuk.'</td>
				<td>'.$row->jml_cuti.'</td>
				<td>'.$row->jml_sakit.'</td>
				<td>'.$row->jml_ijin.'</td>
				<td>'.$row->jml_mangkir.'</td>
				<td>'.$row->jml_lembur.'</td>
				<td>'.$row->jmljam_lembur.'</td>
			</tr>';
        }
        
        $tbl .= '
        	</tbody>
        		</table>
        			</div>
        	<div class="tab-pane fade" id="tab_1_2">
        		<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-presensi"><thead>
        				<tr>
        					<th>No</th>
        					<th>Nama</th>
        					<th>Cabang</th>
        					<th>Tanggal</th>
        					<th>Sakit/Ijin/Mangkir</th>
        					<th>Keterangan</th> 
        					<!--<th>Klarifikasi</th>-->';

        // if($is_approval == TRUE) {
       	// 	$tbl .= '<th>Action</th>';
       	// }
        $tbl .=	'</tr>
        			</thead>
        			<tbody>';
        $i = 1;
        foreach ($presensi as $row) {
        	($row->file != '') ? $action = '
        		<li>
        			<a target="_blank" href="'.base_url('assets/presensi').'/'.$row->file.'"><i class="icon-delete"></i> Download </a>' : $action = '';

        	if($row->sakit == 1) {
        		$reason = "Sakit";
        	}
        	elseif($row->ijin == 1) {
        		$reason = "Ijin";
        	}
        	elseif($row->mangkir == 1) {
        		$reason = "Mangkir";
        	} else {
        		// $reason = "Mangkir";
        		$reason = '';
        	}
            
            $tbl .= '
			<tr>
				<td>'.$i++.'</td>
				<td>'.$row->nama.'</td>
				<td>'.$row->cabang.'</td>
				<td>'.$this->Main_Model->format_tgl($row->tgl).'</td>
				<td>'.$reason.'</td>
				<td>'.$row->ket.'</td>
				<!--<td>'.$row->klarifikasi.'</td>-->';

			// if($is_approval == TRUE) {
			// 	$tbl .=	'<td>
			// 				<div class="btn-group">
			// 		            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
			// 		                <i class="fa fa-angle-down"></i>
			// 		            </button>
			// 		                <ul class="dropdown-menu" role="menu">
			// 		                    <!--<li>
			// 		                        <a href="javascript:;" onclick="get_id(' . $row->id . ');">
			// 		                            <i class="icon-edit"></i> Klarifikasi </a>
			// 		                    </li>-->
			// 		                    '.$action.'
			// 		                </ul>
			// 		        </div>
			// 			</td>';
			// }
			$tbl .='</tr>';
        }
        
        $tbl .= '
        	</tbody>
        		</table>
        			</div>
        				</div>';
        echo $tbl;
	}

	function cuti()
	{
		$this->Main_Model->guest_login();
		$this->Main_Model->generate_cuti();
		$t = $this->Main_Model->view_by_id('ms_tipe_cuti', array(), '');
		$ms_penyetuju = $this->Main_Model->master_penyetuju();

		$penyetuju = array();
		foreach ($ms_penyetuju as $row) {
			$penyetuju[$row->pola] = $row->nama;
		}

        $tipe = array();
        foreach($t as $row) {
        	$tipe[$row->id] = $row->tipe;
        }	

		$header = array(
			'style'	=> $this->header(),
			'menu' => $this->Main_Model->menu_user('0','0','72')
		);
		
		$data = array(
			'periode' => $this->Main_Model->periode_opt(),
			'th' => $this->Cuti_Model->th_cutibiasa(),
			'tipe' => $tipe,
			'penyetuju' => $penyetuju,
			'nip' => $this->Main_Model->kary_cabang(),
			'js' => $this->footer()
		);
		$this->load->view('akun/header',$header);
		$this->load->view('akun/cuti',$data);
	}

	function view_cuti_nip()
    {
    	$this->Main_Model->guest_login();
        $tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Cuti_Model->view_cuti_akun($qd_id);
        // $is_approval = $this->Main_Model->is_approval();
        $template = $this->Main_Model->tbl_temp();
        $this->table->set_heading('No', 'NIP', 'Nama', 'Cabang', 'Tipe Cuti', 'Alasan Cuti', 'Tanggal', 'Status', 'Action');

        $i = 1;
        foreach ($data as $row) {
        	$action = '';
        	$username = $this->session->userdata('username');
        	// if($row->user_insert == $username) 
        	$action = $this->Main_Model->default_action($row->id_cuti_det);
        	$this->table->add_row(
        			$i++,
        			$row->nip,
        			$row->nama,
        			$row->cabang,
        			$row->tipe, 
        			$row->alasan_cuti,
        			$row->tgl,
        			$row->keterangan,
        			$action
        		);
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

	function sp()
	{
		$this->Main_Model->guest_login();
		$id_cabang 	= $this->session->userdata('cabang');
		$javascript = '
				<script>
					function load_table() {
						var tahun = $("#tahun").val();
						var bulan = $("#bulan").val();
						$.ajax({
							url : "'.base_url('surat_peringatan/view_sp_nip').'",
							data : {
				                "tahun" : tahun,
				                "bulan" : bulan
				            },
							beforeSend : function(){
								$("#tampil").addClass("hidden");
								$("#imgload").removeClass("hidden");
							},
							complete : function(){
								$("#tampil").removeClass("hidden");
								$("#imgload").addClass("hidden");
							},
							success : function(data){
								$("#myTable").html(data);
								'.$this->Main_Model->default_datatable().'
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Gagal Mengambil data!");
							}
						});
					}

					$(document).ready(function(){
	                    $(".select2").select2();
	                    load_table();
	                });

					$("#tampil").click(function(){
	                    load_table();
	                });
				</script>';
		$header 	= array(
			'style'			=> $this->header(),
			'menu' 		=> $this->Main_Model->menu_user('0','0','79')
			);
		$footer 	= array(
			'js' 			=> $this->footer(),
			'javascript'	=> $javascript
			);
		$data 			= array(
				'periode' 	=> $this->Main_Model->periode_opt()
			);
		$this->load->view('akun/header',$header);
		$this->load->view('karyawan/sp_nip',$data);
		$this->load->view('akun/footer',$footer);
	}

	function ijin_meninggalkan()
	{
		$this->Main_Model->guest_login();
		$id_cabang 	= $this->session->userdata('cabang');
		$url_del = base_url('absensi/delete_ijinjam');
		$month = date('m');
		$month = str_replace('0', '', $month);

		$year = date('Y');

		$ms_penyetuju = $this->Main_Model->master_penyetuju();

		$penyetuju = array();
		foreach ($ms_penyetuju as $row) {
			$penyetuju[$row->pola] = $row->nama;
		}


		$javascript = '
			<script>
				'.$this->Main_Model->default_datepicker().'

				function clearform() {
					$("#penyetuju").val("").trigger("change");
					$("#nip").val("").trigger("change");
					$(".blank").val("");
				}

				$("#form_ijin").submit(function(e){
					e.preventDefault();
					var formData = new FormData($(this)[0]);
					$.ajax({
						url : "'.base_url('absensi/process_ijinjam').'",
						type : "post",
						data : formData,
						async : false,
						cache : false,
						contentType : false,
						processData : false,
						dataType : "json",
						beforeSend : function() {
							$("#simpan").attr("disabled", true);
						},
						complete : function() {
							$("#simpan").attr("disabled", false);
						},
						success : function(data){
							if(data.status == true) {
		    					clearform();
		    					load_table();
		    					$("#myModal").modal("toggle");
		    				}
		    				bootbox.alert(data.message);
						}
					});
				});

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
				 	$.ajax({
				 		url : "'.base_url('akun/view_ijin_meninggalkan').'",
				 		data : {
			                "tahun" : tahun,
			                "bulan" : bulan
			            },
				 		beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#unduh").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#unduh").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						}
				 	});
				}

				$(document).ready(function(){
					'.$this->Main_Model->default_select2().'
					var year = "'.$year.'";
					var month = "'.$month.'";

					$("#tahun").val(year).trigger("change");
					$("#bulan").val(month).trigger("change");
					load_table();
				});

				function proses(id, val) {
					$.ajax({
						url : "'.base_url('absensi/approval_ijinjam').'/"+id+"/"+val,
						type : "post"
					})
				}

			    function approval(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses Meninggalkan Kerja?",
                        title : "Proses Meninggalkan Kerja",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses(id, "approve");
                                	bootbox.alert("Ijin telah disetujui !");
                                	load_table();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses(id, "reject");
                                	bootbox.alert("Ijin telah ditolak !");
                                	load_table();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

				$("#tampil").click(function(){
                    load_table();
                });

				function get_id(id) {
					save_method = "update";
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('absensi/ijinjam_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType : "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
							$("#keterangan").val(data.keterangan);
							$("#jmljam").val(data.jmljam);
							$("#nip").val(data.nip).trigger("change");
							$("#id").val(data.id);
							$("#tgl").val(data.tgl);
							$("#penyetuju").val(data.pola).trigger("change");
							$("#title").html("Form Ijin Meninggalkan Kerja");
							$("#starttime_hour").val(data.starttime_hour);
							$("#starttime_minute").val(data.starttime_minute);
							$("#endtime_hour").val(data.endtime_hour);
							$("#endtime_minute").val(data.endtime_minute);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			bootbox.alert("Oops Something Went Wrong!");
			    		}
			    	});
			    }

			    $("#unduh").click(function(){
			    	var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
			    	window.location.href="'.base_url('download_excel/meninggalkan_kerja').'/"+tahun+"/"+bulan;
			    });

			    '.$this->Main_Model->default_delete_data($url_del).'
			</script>
		';

		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_user('0','0','72')
		);

		$footer = array(
			'js' => $this->footer(),
			'javascript' => $javascript
		);

		$data = array(
			'periode' => $this->Main_Model->periode_opt(),
			'penyetuju' => $penyetuju,
			'nip' => $this->Main_Model->kary_cabang($id_cabang)
		);
		$this->load->view('akun/header',$header);
		$this->load->view('akun/ijin_meninggalkan',$data);
		$this->load->view('akun/footer',$footer);
	}

	function view_ijin_meninggalkan()
	{
		$this->Main_Model->guest_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
		$data 		= $this->Absensi_Model->view_ijinjam($qd_id);
        $template 	= $this->Main_Model->tbl_temp();
        $is_approval = $this->Main_Model->is_approval();
        
		$this->table->set_heading('No','NIP','Nama','Cabang','Tanggal','Dari', 'Sampai','Keterangan','Status','Scanlog','Action');
        $i   = 1;
        foreach ($data as $row) {
	        $link = '';
	        // if($row->flag == 1 && $row->kode != 6 && $is_approval == TRUE) $link = '<li> <a href="javascript:;" onclick="approval(' . $row->id . ');"> <i class="icon-edit"></i> Proses </a> </li>';

	        // $scan = $this->Absensi_Model->scan_from_pin($row->pin, $row->tanggal);
	        // $log = isset($scan->scanlog) ? $scan->scanlog : '';

	        $this->table->add_row(
				$i++,
				$row->nip,
				$row->nama,
				$row->cabang,
				$row->tgl,
				jam($row->dari),
				jam($row->sampai),
				$row->keterangan,
				$row->app_status,
				$row->scanlog,
				'<div class="btn-group dropup">
		            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
		                <i class="fa fa-angle-down"></i>
		            </button>
		                <ul class="dropdown-menu" role="menu">
		                	'.$link.'
		                    <li>
		                        <a href="javascript:;" onclick="get_id(' . $row->id . ');">
		                            <i class="icon-edit"></i> Update </a>
		                    </li>
		                    <li>
		                        <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
		                            <i class="icon-delete"></i> Delete </a>
		                    </li>
		                </ul>
		        </div>'
		        );
           
        }

        $this->table->set_template($template);
		echo $this->table->generate();
	}

	function lembur()
	{
		$this->Main_Model->all_login();
		$url_del = base_url('tunjangan/delete_lembur');
		$id_cabang 	= $this->session->userdata('cabang');
		$month = date('m');
		$month = str_replace('0', '', $month);
		$javascript = '
			<script type="text/javascript">
				var save_method;
				'.$this->Main_Model->default_select2()
				 .$this->Main_Model->default_datepicker().'
				jQuery().timepicker && ($(".timepicker-default").timepicker({
	                autoclose : !0,
	                showSeconds : !0,
	                minuteStep : 1
	            }), $(".timepicker-no-seconds").timepicker({
	                autoclose : !0,
	                minuteStep : 5
	            }), $(".timepicker-24").timepicker({
	                autoclose : !0,
	                minuteStep : 5,
	                showSeconds : !1,
	                showMeridian : !1
	            }), $(".timepicker").parent(".input-group").on("click", ".input-group-btn", function(t) {
	                t.preventDefault(), $(this).parent(".input-group").find(".timepicker").timepicker("showWidget")
	            }), $(document).scroll(function() {
	                $("#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24").timepicker("place")
	            }))

	            $(document).ready(function(){
					var year = "'.date('Y').'";
					var month = "'.$month.'";

					$("#tahun").val(year).trigger("change");
					$("#bulan").val(month).trigger("change");

					load_table();
				});

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('akun/view_lembur').'",
						type : "GET",
						data : {
			                "tahun" : tahun,
			                "bulan" : bulan
			            },
						beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
						success : function(data) {
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			        			responsive: true
			    			});
						},
						error : function(jqXHR, textStatus, errorThrown) {
							'.$this->Main_Model->notif500().'
						} 
					})
				}

				function clearform()
				{
					save_method="save";
					$("#myModal").modal();
					// $(".kosong").val("");
					// $(".select2").val("").trigger("change");
					document.getElementById("flembur").reset();
					$("#nip").val("").trigger("change");
					$("#overtime").val("").trigger("change");
				}

				$("form#flembur").submit(function(event){
 
				  	//disable the default form submission
				  	event.preventDefault();
				 
				  	//grab all form data  
				  	var formData = new FormData($(this)[0]);
				 	
				 	(save_method == "save") ? url = "'.base_url('tunjangan/add_lembur').'" : url = "'.base_url('tunjangan/update_lembur').'";
						
				  	$.ajax({
				    	url: url,
				    	type: "POST",
				    	data: formData,
				    	dataType : "JSON",
				    	async: false,
				    	cache: false,
				    	contentType: false,
				    	processData: false,
				    	beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
				    	success: function (data) {
				      		if(data.status == true){
				      			document.getElementById("flembur").reset();
				      			load_table();
				      			toastr.success("", data.message);
				      			$("#myModal").modal("toggle");
				      		} else {
				      			toastr.warning("", data.message);
				      		}
				      		// bootbox.alert(data.message);
				    	}
				  	});
				 
				  	return false;
				});

				
				function get_id(id) {
					save_method = "update";
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('tunjangan/lembur_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType: "JSON",
			    		beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
			    		success : function(data){
			    			$("#myModal").modal();
			    			$("#id").val(data.id_lembur);
							$("#nip").val(data.nip).trigger("change");
							// $("#islibur").val(data.islibur);
							$("#overtime").val(data.id_overtime).trigger("change");
							$("#keterangan").val(data.keterangan);
							$("#tgl").val(data.tgl);
							// $("#timestart").val(data.starttime);
							// $("#timeend").val(data.endtime);
							$("#starttime_hour").val(data.starttime_hour);
							$("#starttime_minute").val(data.starttime_minute);
							$("#endtime_hour").val(data.endtime_hour);
							$("#endtime_minute").val(data.endtime_minute);
			    		},
			    		error 	: function(jqXHR, textStatus, errorThrown){
			    			'.$this->Main_Model->notif500().'
			    		}
			    	});
			    }

			    $("#tampil").click(function(){
                    load_table();
                });

			    $("#unduh").click(function(){
			  //   	var tahun = $("#tahun").val();
					// var bulan = $("#bulan").val();
			  //   	window.location.href="'.base_url('download_excel/laporan_lembur').'/"+tahun+"/"+bulan;
			    	$("#downloadModal").modal("toggle");
			    });

			    $("#download_laporan").click(function(){
			    	var nik = $("#nik").val();
					var tgl_awal = $("#tgl_awal").val();
					var tgl_akhir = $("#tgl_akhir").val();

					window.open("'.base_url('download_excel/laporan_lembur').'?tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir+"&nip="+nik, "_blank");
			    });

			    '.$this->Main_Model->default_delete_data($url_del).'
			</script>';

		$footer =array(
			'javascript' => $javascript,
			'js' => $this->footer()
		);

		$bulan = $this->Main_Model->arr_bulan_periode();
		$tahun = $this->Main_Model->arr_tahun_periode();

		$data = array(
			'nip' => $this->Main_Model->kary_cabang($id_cabang),
			'periode' => $this->Main_Model->periode_opt(),
			'option' => array('0'=>'Tidak','1'=>'Ya'),
			'bulan' => $bulan,
			'tahun' => $tahun,
			'overtime' => $this->Tunjangan_Model->opt_overtime()
		);

		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_user('0','0','81')
		);

		$this->load->view('akun/header',$header);
		$this->load->view('tunjangan/lembur',$data);
		$this->load->view('akun/footer',$footer);
	}

	function view_lembur()
	{
		$this->Main_Model->all_login();
		$id_cabang = $this->Main_Model->session_cabang();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';
		$tgl_awal = isset($p->tgl_awal) ? $p->tgl_awal : '';
		$tgl_akhir = isset($p->tgl_akhir) ? $p->tgl_akhir : '';

		$data = $this->Tunjangan_Model->view_lembur($tgl_awal, $tgl_akhir, '', $id_cabang);
		$template = $this->Main_Model->tbl_temp();

		$this->table->set_heading('No','NIP','Nama','Cabang','Tanggal','Jenis','Jam','Nominal','Status','Keterangan','Action');
		$no = 1;
		foreach ($data as $row) {

		switch ($row->status) {
        	case '1': $status = 'Disetujui'; break;
        	case '2': $status = 'Ditolak'; break;
        	default: $status = 'Proses'; break;
        }

        $nominal = $row->lembur + $row->uangmakan;
        ($row->status == 1) ? $nominal = $nominal : $nominal = 0;

		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$row->cabang,
			$this->Main_Model->format_tgl($row->tgl),
			$row->jenis,
			$row->jam,
			$this->Main_Model->uang($nominal),
			$status,
			$row->keterangan,
			$this->Main_Model->default_action($row->id_lembur)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function cutikhusus()
	{
		$this->Main_Model->guest_login();
		$url_load = base_url('akun/view_cutimelahirkan');
		$url_del = base_url('cuti/delete_cutimelahirkan');
		$url_save = base_url('cuti/process_cuti_khusus');
		$id_cabang = $this->session->userdata('cabang');
		$javascript = '
			<script type="text/javascript">
				'.$this->Main_Model->default_datepicker()
				 .$this->Main_Model->default_select2().'
				
				function reset() {
					$(".select2").val("").trigger("change");
					$(".kosong").val("");
				}

				'.$this->Main_Model->default_loadtable($url_load).'

				function get_id(id) {
					id = {"id":id}
					$.ajax({
						url : "'.base_url('cuti/get_id_cutimelahirkan').'",
						data : id,
						dataType : "JSON",
						success : function(data){
							$("#myModal").modal();
							$("#id").val(data.id);
							$("#nip").val(data.nip).trigger("change");
							$("#tipe").val(data.id_tipe_cuti).trigger("change");
							$("#tgl_awal").val(data.tglawal);
							$("#tgl_akhir").val(data.tglakhir);
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbot.alert("Gagal mengambil data!");
						}
					});
				}
				
				function save() {
					$.ajax({
						url : "'.$url_save.'",
						type : "POST",
						data : $("#cutikhusus").serialize(),
						dataType: "JSON",
						success : function(data){
							if(data.status=="true") {
								load_table();
								'.$this->Main_Model->notif200().'
								reset();
							} else {
								bootbox.alert("Gagal menyimpan data!");
							}
						},
						error : function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal menyimpan data!");
						}
					});
				}

				'.$this->Main_Model->default_delete_data($url_del).'
			</script>';
		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$data = array(
			'nip' => $this->Main_Model->kary_cabang($id_cabang),
			'tipe' => $this->Cuti_Model->tipe_cuti()
			);
		$header = array(
			'style'	=> $this->header(),
			'menu' => $this->Main_Model->menu_user('0','0','72')
			);
		$this->load->view('akun/header',$header);
		$this->load->view('cuti/cutimelahirkan',$data);
		$this->load->view('akun/footer',$footer);
	}

	function view_cutimelahirkan()
	{
		$this->Main_Model->guest_login();
		$data = $this->Absensi_Model->view_cuti_khusus_cab();
		$template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cuti','Tgl Awal Cuti','Tgl Akhir Cuti','Status','Action');
		$no =1;
        
        foreach ($data as $row) {

		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$row->tipe,
			$this->Main_Model->format_tgl($row->tgl_awal),
			$this->Main_Model->format_tgl($row->tgl_akhir),
			$row->keterangan,
			$this->Main_Model->default_action($row->id)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function terlambat()
	{
		$this->Main_Model->guest_login();

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2();

		$header = array(
			'menu' => $this->Main_Model->menu_user('0','0','72'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'periode' => $this->Main_Model->periode_opt(),
			'penutup' => $this->Main_Model->close_page(),
			);

		$this->load->view('akun/header', $header);
		$this->load->view('akun/terlambat',$data);
	}

	function view_terlambat() 
	{
		$this->Main_Model->guest_login();
		$id_cabang = $this->Main_Model->session_cabang();
        $tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Absensi_Model->view_terlambat($id_periode, $id_cabang);
		$template = $this->Main_Model->tbl_temp();
		$this->table->set_heading('No','NIP','Nama','Cabang','Periode','Total Hari','Total Jam','Total Menit', 'Action');
		$no = 1;
		foreach($data as $row) {
			$quotation = "'".$row->nip."', '".$id_periode."'";
			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$row->cabang,
				$row->periode,
				$row->total_hari,
				round($row->total_menit/60, 2),
				$row->total_menit,
				'<div class="btn-group">
	                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                    <i class="fa fa-angle-down"></i>
	                </button>
	                    <ul class="dropdown-menu" role="menu">
	                        <li>
	                            <a href="javascript:;" onclick="show(' . $quotation . ');">
	                                <i class="icon-edit"></i> Lihat Detail </a>
	                        </li>
	                    </ul>
	            </div>'
			);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function klarifikasi($param = '') 
	{
		$this->Main_Model->guest_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2();

		$header = array(
			'menu' => $this->Main_Model->menu_user('0','0','72'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
			);

		$head = ($param == 'absensi') ? 'Klarifikasi Absensi' : 'Klarifikasi Terlambat';
		$tag = ($param == 'absensi') ? 'klarifikasi_absensi' : 'klarifikasi_terlambat';

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'periode' => $this->Main_Model->periode_opt(),
			'penutup' => $this->Main_Model->close_page(),
			'absen' 	=> array(
                'sakit' 	=> 'Sakit',
                'ijin' 		=> 'Ijin',
                'mangkir' 	=> 'Mangkir',
                'masuk' 	=> 'Masuk',
                'cuti' 		=> 'Cuti'
            	),
			'header' => $head,
			'tag' => $tag
			);

		$this->load->view('akun/header', $header);
		$this->load->view('akun/klarifikasi',$data);
	}

	function view_klarifikasi_absensi()
	{
		$this->Main_Model->guest_login();
		$id_cabang = $this->session->userdata('cabang');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Absensi_Model->view_klarifikasi_absensi($id_periode);
		$template = $this->Main_Model->tbl_temp();

		$acctype = $this->session->userdata('acctype');

		$is_approval = $this->Main_Model->is_approval();

		$array_header = array('No', 'NIP', array('data' => 'Nama', 'style' => 'width:15%'), 'Cabang', 'Tanggal', 'Absensi', array('data' => 'Klarifikasi', 'style' => 'width:20%'), 'Status');
		// ($is_approval == TRUE) ? $array_header[] = 'Action' : $array_header = $array_header;
		if ($acctype == 'Administrator') {
			$array_header[] = 'Action';
		}
		$this->table->set_heading($array_header);

		$no = 1;
		foreach($data as $row) {
			// array('label' => 'Update', 'event' => "get_id('".$row->id_ab."')") 
			$action = array();
			if($row->file != '') { 
				$action[] = array(
					'label' => 'File Presensi', 
					'event' => 'javascript:;', 'url' => base_url('assets/presensi/'.$row->file)
				);
			}

			if($acctype == 'Administrator') {
				if ($row->approval == '6') {
					$action[] = array(
						'label' => 'Proses',
						'event' => "approval('".$row->id."')"
					);
				}

				if ($row->approval != '13') {
					$action[] = array(
						'label' => 'Delete',
						'event' => "delete_data('".$row->id."')"
					);
				}
			}

			$array_data = array(
							$no++, 
							$row->nip, 
							$row->nama, 
							$row->cabang, 
							($row->tgl) ? $this->Main_Model->format_tgl($row->tgl) : '', 
							$row->status, 
							strtoupper('<strong>'.$row->presensi.'</strong>').' ('.$row->klarifikasi.')',
							$row->keterangan
						);

			// ($is_approval == TRUE) ? $array_data[] = $this->Main_Model->action($action) : $array_data = $array_data;
			if ($acctype == 'Administrator') {
				$array_data[] = $this->Main_Model->action($action);
			}
			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function view_klarifikasi_terlambat() 
	{
		$this->Main_Model->guest_login();
		$id_cabang = $this->session->userdata('cabang');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Absensi_Model->view_klarifikasi_terlambat($id_periode);
		$template = $this->Main_Model->tbl_temp();
		$acctype = $this->session->userdata('acctype');
		// $this->table->set_heading('No', 'NIP', 'Nama', 'Tanggal', 'Sakit/Ijin/Mangkir', 'Keterangan', 'Klarifikasi');

		$is_approval = $this->Main_Model->is_approval();

		$array_header = array('No', 'NIP', 'Nama', 'Cabang', 'Tanggal', 'Status', 'Uang Makan', array('data' => 'Klarifikasi', 'style' => 'width:30%'));
		// ($is_approval == TRUE) ? $array_header[] = 'Action' : $array_header = $array_header;
		if ($acctype == 'Administrator') {
			$array_header[] = 'Action';
		}
		$this->table->set_heading($array_header);

		$no = 1;
		foreach($data as $row) {
			// array('label' => 'Update', 'event' => "get_id('".$row->id_ab."')") 
			$action = array();

			if($row->file != '') { 
				array_push($action, array('label' => 'File Terlambat', 'event' => 'javascript:;', 'url' => base_url('assets/presensi/'.$row->file)));
			}

			if($acctype == 'Administrator') {
				if ($row->approval == '6') {
					$action[] = array(
						'label' => 'Proses',
						'event' => "approval_terlambat('".$row->id."')"
					);
				}
			}

			$um = ($row->uang_makan == 1) ? 'Ya' : 'Tidak';

			$array_data = array($no++, $row->nip, $row->nama, $row->cabang, ($row->tgl) ? $this->Main_Model->format_tgl($row->tgl) : '', $row->keterangan, $um, $row->klarifikasi);

			// ($is_approval == TRUE) ? $array_data[] = $this->Main_Model->action($action) : $array_data = $array_data;
			if ($acctype == 'Administrator') {
				$array_data[] = $this->Main_Model->action($action);
			}
			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function klarifikasi_mangkir()
	{
		$this->Main_Model->guest_login();
		$menu = $this->Main_Model->menu_user('0','0','152');
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'absen' => array(
	               	'sakit' => 'Sakit',
	                'ijin' => 'Ijin',
	                // 'mangkir' 	=> 'Mangkir',
	                'masuk' => 'Masuk',
	                'cuti' => 'Cuti'
	            	)
			);

		$this->load->view('template/header', $header);
		$this->load->view('akun/view_mangkir',$data);
	}	

	function data_karyawan()
	{
		$this->Main_Model->all_login();
		$menu = $this->Main_Model->menu_user('0', '0', '160');
		$js = $this->Main_Model->js_datatable();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
		);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
		);

		$this->load->view('template/header', $header);
		$this->load->view('akun/data_karyawan', $data);
	}

	function view_karyawan_aktif()
	{
		$this->Main_Model->all_login();
		$aktif = $this->Karyawan_Model->view_karyawan_aktif();

		$table_aktif = '
			<table class="table table-striped table-bordered table-hover table-checkable order-column" id="active">
				<thead>
					<tr>
						<th>No</th>
						<th>NIP</th>
						<th>PIN</th>
						<th>Nama</th>
						<th>Jabatan</th>
						<th>Cabang</th>
						<th>Tanggal Masuk</th>
						<th>Masa Kerja</th>
						<!--<th>Status</th>-->
					</tr>
				</thead>
				<tbody>';
			$i = 1;
			foreach ($aktif as $row) {
				$table_aktif .= '
					<tr>
						<td>'.$i++.'</td>
						<td>'.$row->nip.'</td>
						<td>'.$row->pin.'</td>
						<td>'.$row->nama.'</td>
						<td>'.$row->jab.'</td>
						<td>'.$row->cabang.'</td>
						<td>'.$row->tgl_masuk.'</td>
						<td>'.$row->tahun.' Tahun '.$row->bulan.' Bulan</td>
						<!--<td>'.$row->file_berkas.'</td>-->
					</tr>';
			}
		$table_aktif .= '
			</tbody>
		</table>';

		echo $table_aktif;
	}

	function verifikasi_keluar_kantor()
	{
		$this->Main_Model->guest_login();
		$data = $this->Absensi_Model->verifikasi_tinggal();
        $is_approval = $this->Main_Model->is_approval();
        $template = $this->Main_Model->tbl_temp('tb_keluar_kantor');
        $array_heading = array('No', 'NIP', 'Nama', 'Tanggal', 'Dari', 'Sampai', 'Alasan', 'Status');
        if ($is_approval == TRUE)  $array_heading[] = 'Action';
        $this->table->set_heading($array_heading);
   
        $i = 1;
        foreach ($data as $row) {
        	
			$array_data = array($i++, $row->nip, $row->nama, $this->Main_Model->format_tgl($row->tgl), jam($row->dari), jam($row->sampai), $row->keterangan, $row->status_approval);

			if ($row->flag != 0) $action = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="approval_tinggal('.$row->id.');"> <i class="fa fa-check"></i> Verifikasi </a>';
			if ($is_approval == TRUE) $array_data[] = $action;
            
            $this->table->add_row($array_data);
        }
        $this->table->set_template($template);
		echo $this->table->generate();
	}
}