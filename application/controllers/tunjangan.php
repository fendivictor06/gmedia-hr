<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tunjangan extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Absensi_Model', '', TRUE);
		$this->load->model('Tunjangan_Model', '', TRUE);
	}

	function header()
	{
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'"></script>';

		return $footer;
	}


	function bbp()
	{
		$this->Main_Model->get_login();
		$url 		= base_url('tunjangan/view_bbp');
		$url_del 	= base_url('tunjangan/delete_bbp');
		$javascript = '
			<script type="text/javascript">
				'.$this->Main_Model->default_datepicker()
				 .$this->Main_Model->default_loadtable($url)
				 .$this->Main_Model->default_select2().'
				function reset(id){
					if(id==1)
					{
						'.$this->Main_Model->notif200().'
	    				$(".kosong").val("");
						$(".select2").val("").trigger("change");
					}
					else if(id==2)
					{
						'.$this->Main_Model->notif400().'
					}
					else
					{
						$(".kosong").val("");
						$(".select2").val("").trigger("change");
						$("#bbp").addClass("hidden");
					}
				}
				
				function save()
				{
					$.ajax({
						url 	: "'.base_url('tunjangan/process_bbmp').'",
						data 	: $("#bbmp").serialize(),
						type 	: "POST",
						dataType: "JSON",
						success : function(data){
							if(data.status=="true")
							{
								reset(1);
								load_table();
							}
							else
							{
								reset(2);
							}
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							'.$this->Main_Model->notif500().'
						}
					});
				}

				function is_bbp(){
					var bbp = $("#isbbp").val();
					if(bbp==0)
					{
						$("#bbp").addClass("hidden");
						$("#family").val("");
					}
					else if(bbp==1)
					{
						$("#bbp").removeClass("hidden");
					}
				}

				function get_id(id){
					id 			= {"id" : id}
			    	$.ajax({
			    		url 	: "' . base_url('tunjangan/bbp_id') . '",
			    		type 	: "GET",
			    		data 	: id,
			    		dataType: "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
			    			$("#id").val(data.id);
							$("#nip").val(data.nip).trigger("change");
							$("#bbmstart").val(data.tgl_start);
							$("#isbbp").val(data.set_bbp);
							$("#lokasi").val(data.id_lku).trigger("change");
							$("#family").val(data.family).trigger("change");
							if(data.set_bbp==1)
							{
								$("#bbp").removeClass("hidden");
							}
							else if(data.set_bbp==0)
							{
								$("#bbp").addClass("hidden");
							}
			    		},
			    		error 	: function(jqXHR, textStatus, errorThrown){
							'.$this->Main_Model->notif500().'
			    		}
			    	});
			    }
				'.$this->Main_Model->default_delete_data($url_del).'
			</script>';
		$footer =
			array(
				'javascript'=> $javascript,
				'js' 		=> $this->footer()
			);
		$data 	= 
			array(
				'option' 	=> 
					array(
						'1' 	=> 'Ya',
						'0' 	=> 'Tidak'
					),
				'lokasi' 	=> $this->Main_Model->lku(),
				'nip' 		=> $this->Main_Model->kary_bbp(),
				'periode' 	=> $this->Main_Model->all_periode_opt()
			);
		$this->load->view('template/header',$this->header());
		$this->load->view('tunjangan/bbp',$data);
		$this->load->view('template/footer',$footer);
	}

	function view_bbp()
	{
		$this->Main_Model->get_login();
		$data 		= $this->Tunjangan_Model->view_bbp();
		$template 	= $this->Main_Model->tbl_temp();

		$this->table->set_heading('No','NIP','Nama','BBM Mulai','BBM Selesai','Jumlah BBM','Jumlah BBP','Total','Action');
		$no=1;
		foreach ($data as $row) {
		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$this->Main_Model->format_tgl($row->bbm_start),
			$this->Main_Model->format_tgl($row->bbm_end),
			$this->Main_Model->uang($row->bbm_value),
			$this->Main_Model->uang($row->bbp_value),
			$this->Main_Model->uang($row->bbm_value+$row->bbp_value),
			$this->Main_Model->default_action($row->id)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_bbmp()
	{
		$this->Main_Model->get_login();
		$id 		= $this->input->post("id");
		$nip 		= $this->input->post("nip");
		$bbm_start 	= $this->input->post("bbmstart");
		$lku 		= $this->input->post("lokasi");
		$set_bbp 	= $this->input->post("isbbp");
		$family 	= $this->input->post("family");
		

		if(empty($nip) || empty($bbm_start) || empty($lku) || $set_bbp=="")
		{
			$result = array('status'=>'false');
		}
		else
		{
			if($family=="" && $set_bbp==1)
			{
				$result = array('status'=>'false');
			}
			else
			{
				$q = $this->db->query("SELECT c.`jab` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos`WHERE a.`aktif`='1' AND a.`nip` = '$nip'")->row();
				//bbm
				$w 		= $this->db->query("SELECT * FROM ms_bbmp a WHERE a.`jab`='$q->jab' AND a.`tipe`='bbm'")->row();
				//bbp
				$e 		= $this->db->query("SELECT * FROM ms_bbmp a WHERE a.`jab`='$q->jab' AND a.`tipe`='bbp'")->row();
				//lku
				$r 		= $this->db->query("SELECT MAX(a.`th`) th,a.`pros` FROM fk a WHERE a.`id_lku`='$lku'")->row();
				$pros 	= $r->pros;

				$bbm_value = $w->jumlah*$pros/100;
				if($set_bbp=='0')
				{
					$bbp_value = 0;
				}
				elseif($set_bbp=='1')
				{
					if($family=='1')
					{
						$bbp_value = $e->jumlah*$pros/100;
					}
					elseif($family=='0') 
					{
						$bbp_value = $e->jumlah*60/100*$pros/100;
						$bbm_value = $w->jumlah*60/100*$pros/100;
					}
				}
				$convertbbm = $this->Main_Model->convert_tgl($bbm_start);
				$bbm_end 	= date('Y-m-d', strtotime('+3 month', strtotime($convertbbm)));
				$data 		= array(
					'nip'		=> $nip,
					'set_bbp' 	=> $set_bbp,
					'bbm_start'	=> $convertbbm,
					'bbm_end' 	=> $bbm_end,
					'bbm_value' => $bbm_value,
					'bbp_value' => $bbp_value,
					'id_lku' 	=> $lku,
					'family' 	=> $family
				);

				$this->Tunjangan_Model->process_bbmp($data,$id);
				$result = array('status'=>'true');
				
			}
		}
		// print_r($result);
		echo json_encode($result);
	}

	function bbp_id()
	{
		$id = $this->input->get('id');
		$data = $this->Tunjangan_Model->bbp_id($id);

		echo json_encode($data);
	}

	function delete_bbp()
	{
		$id = $this->input->post("id");
		$this->Tunjangan_Model->delete_bbp($id);
	}

	function lembur()
	{
		$this->Main_Model->get_login();
		$url_del 	= base_url('tunjangan/delete_lembur');
		$month = date('m');
		// $month = str_replace('0', '', $month);
		$javascript = '
			<script type="text/javascript">
				$(document).ready(function(){
					var year = "'.date('Y').'";
					var month = "'.$month.'";

					$("#tahun").val(year).trigger("change");
					$("#bulan").val(month).trigger("change");

					load_table();
				});


				var save_method;
				'.$this->Main_Model->default_select2()
				 .$this->Main_Model->default_datepicker().'
				jQuery().timepicker && ($(".timepicker-default").timepicker({
	                autoclose : !0,
	                showSeconds : !0,
	                minuteStep : 1
	            }), $(".timepicker-no-seconds").timepicker({
	                autoclose : !0,
	                minuteStep : 5
	            }), $(".timepicker-24").timepicker({
	                autoclose : !0,
	                minuteStep : 5,
	                showSeconds	: !1,
	                showMeridian: !1
	            }), $(".timepicker").parent(".input-group").on("click", ".input-group-btn", function(t) {
	                t.preventDefault(), $(this).parent(".input-group").find(".timepicker").timepicker("showWidget")
	            }), $(document).scroll(function() {
	                $("#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24").timepicker("place")
	            }))

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					var cabang = $("#cabang").val();
					var periode = {
						"tahun" : tahun,
						"bulan" : bulan,
						"cabang" : cabang
					}
					$.ajax({
						url : "'.base_url('tunjangan/view_lembur').'",
						type : "GET",
						data : periode,
						beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
						success : function(data) {
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			        			responsive: true
			    			});
						},
						error : function(jqXHR, textStatus, errorThrown) {
							bootbox.alert("Gagal mengambil data!");
						} 
					})
				}
				

				$("#tampil").click(function(){
					load_table();
				});

				function clearform() {
					save_method="save";
					$("#myModal").modal("show");
					// $(".kosong").val("");
					// $(".select2").val("").trigger("change");
					document.getElementById("flembur").reset();
					$("#overtime").val("").trigger("change");
					$("#nip").val("").trigger("change");
				}

				$("form#flembur").submit(function(event){
 
				  	//disable the default form submission
				  	event.preventDefault();
				 
				  	//grab all form data  
				  	var formData = new FormData($(this)[0]);
				 	
				 	(save_method == "save") ? url = "'.base_url('tunjangan/add_lembur').'" : url = "'.base_url('tunjangan/update_lembur').'";
						
				  	$.ajax({
				    	url: url,
				    	type: "POST",
				    	data: formData,
				    	dataType : "JSON",
				    	// async: false,
				    	cache: false,
				    	beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
				    	contentType: false,
				    	processData: false,
				    	success: function (data) {
				      		if(data.status == true){
				      			document.getElementById("flembur").reset();
				      			load_table();
				      			toastr.success("", data.message);
				      			$("#myModal").modal("toggle");
				      		} else {
				      			toastr.warning("", data.message);
				      		}
				      		// bootbox.alert(data.message);
				    	}
				  	});
				 
				  	return false;
				});

				
				function get_id(id) {
					save_method = "update";
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('tunjangan/lembur_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType : "JSON",
			    		beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
			    		success : function(data){
			    			$("#myModal").modal();
			    			$("#id").val(data.id_lembur);
							$("#nip").val(data.nip).trigger("change");
							// $("#islibur").val(data.islibur);
							$("#overtime").val(data.id_overtime).trigger("change");
							$("#keterangan").val(data.keterangan);
							$("#tgl").val(data.tgl);
							// $("#timestart").val(data.starttime);
							// $("#timeend").val(data.endtime);
							$("#starttime_hour").val(data.starttime_hour);
							$("#starttime_minute").val(data.starttime_minute);
							$("#endtime_hour").val(data.endtime_hour);
							$("#endtime_minute").val(data.endtime_minute);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			'.$this->Main_Model->notif500().'
			    		}
			    	});
			    }

			    function proses(id, val) {
					$.ajax({
						url : "approval_lembur/"+id+"/"+val,
						type : "post"
					})
				}

			    function approval(id) {
					bootbox.dialog({
                        message : "Yakin ingin memverifikasi Lembur?",
                        title : "Verifikasi Lembur",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses(id, 1);
                                	bootbox.alert("Ijin telah disetujui !");
                                	load_table();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses(id, 2);
                                	bootbox.alert("Ijin telah ditolak !");
                                	load_table();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

				$("#unduh").click(function(){
					// var tahun = $("#tahun").val();
					// var bulan = $("#bulan").val();
			  		//window.location.href="'.base_url('download_excel/laporan_lembur').'/"+tahun+"/"+bulan;
					$("#downloadModal").modal("toggle");
			    });

			    $("#download_laporan").click(function(){
			    	var nik = $("#nik").val();
					var tgl_awal = $("#tgl_awal").val();
					var tgl_akhir = $("#tgl_akhir").val();

					window.open("'.base_url('download_excel/laporan_lembur').'?tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir+"&nip="+nik, "_blank");
			    });

			    '.$this->Main_Model->default_delete_data($url_del).'
			</script>';
		
		$footer = array(
			'javascript'=> $javascript,
			'js' => $this->footer()
		);
		
		$opt  = $this->Main_Model->kary_lembur();
		$a = array();
		if (!empty($opt)) {
			foreach ($opt as $row) {
				$a[$row->nip] = $row->nip.' - '.$row->nama;
			}
		}

		$bulan = $this->Main_Model->arr_bulan_periode();
		$tahun = $this->Main_Model->arr_tahun_periode();

		$acctype = $this->session->userdata('acctype');
		$arr_cabang = [];
		if ($acctype == 'Administrator') {
			$arr_cabang[''] = 'Semua Cabang';
			$ms_cabang = $this->Main_Model->view_by_id('ms_cabang', ['status' => 1], 'result');
			if (! empty($ms_cabang)) {
				foreach ($ms_cabang as $row) {
					$arr_cabang[$row->id_cab] = $row->cabang;
				}
			}
		}

		$data = array(
			'nip' => $a,
			'periode' => $this->Main_Model->periode_opt(),
			'option' => array(
				'0' => 'Tidak',
				'1' => 'Ya'
			),
			'overtime' => $this->Tunjangan_Model->opt_overtime(),
			'bulan' => $bulan,
			'tahun' => $tahun,
			'cabang' => $arr_cabang
		);

		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','2')
		);

		$this->load->view('template/header',$header);
		$this->load->view('tunjangan/lembur',$data);
		$this->load->view('template/footer',$footer);
	}

	function approval_lembur($id_lembur='', $value='')
	{
		$this->Main_Model->all_login();
		$user = $this->session->userdata('username');
		$date = date('Y-m-d H:i:s');
		$data = array(
			'status' => $value,
			'user_update' => $user,
			'update_at' => $date
			);
		$this->Tunjangan_Model->update_lembur($data, $id_lembur);
	}

	function view_lembur()
	{
		$this->Main_Model->get_login();
		$tahun 	= $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$cabang = $this->input->get('cabang');

		$arr_cabang = [];
		if ($cabang != '') {
			$arr_cabang[] = $cabang;
		}

		$p = periode($tahun, $bulan);
		// $periode = isset($p->qd_id) ? $p->qd_id : '';
		$tgl_awal = isset($p->tgl_awal) ? $p->tgl_awal : '';
		$tgl_akhir = isset($p->tgl_akhir) ? $p->tgl_akhir : '';
		$data = $this->Tunjangan_Model->view_lembur($tgl_awal, $tgl_akhir, '', $arr_cabang);
		$template = $this->Main_Model->tbl_temp();

		$this->table->set_heading('No','NIP','Nama','Cabang','Tanggal','Jenis','Jam','Nominal','Status','Keterangan','Action');
		$no = 1;
		if (!empty($data)) {
			foreach ($data as $row) {
				($row->file) ? $download = '<li> <a href="../assets/lembur/'.$row->file.'" target="_blank"> <i class="icon-delete"></i> Download File </a> </li>' : $download = '';
				switch ($row->status) {
		        	case '1': $status = 'Disetujui'; break;
		        	case '2': $status = 'Ditolak'; break;
		        	default: $status = 'Proses'; break;
		        }

		        ($row->status != 0) ? $link = '' : $link = '<li> <a href="javascript:;" onclick="approval(' . $row->id_lembur . ');"> <i class="icon-edit"></i> Proses </a> </li>';

		        $nominal = $row->lembur + $row->uangmakan;
		        ($row->status == 1) ? $nominal = $nominal : $nominal = 0;

		        $q = $this->Main_Model->posisi($row->nip);
		        $cabang = isset($q->cabang) ? $q->cabang : '';

				$this->table->add_row(
					$no++,
					$row->nip,
					$row->nama,
					$cabang,
					$this->Main_Model->format_tgl($row->tgl),
					$row->jenis,
					jam($row->starttime).' - '.jam($row->endtime),
					$this->Main_Model->uang($nominal),
					$status,
					$row->keterangan,
					'<div class="btn-group">
		                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
		                    <i class="fa fa-angle-down"></i>
		                </button>
		                    <ul class="dropdown-menu" role="menu">
		                    	'.$download.$link.'
		                        <li>
		                            <a href="javascript:;" onclick="get_id(' . $row->id_lembur . ');">
		                                <i class="icon-edit"></i> Update </a>
		                        </li>
		                        <li>
		                            <a href="javascript:;" onclick="delete_data(' . $row->id_lembur . ');">
		                                <i class="icon-delete"></i> Delete </a>
		                        </li>
		                    </ul>
		            </div>'
			        );
			}
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function add_lembur()
	{
		$this->Main_Model->all_login();
		$this->load->library('upload');
		$nip = $this->input->post("nip");
		$tgl = $this->input->post("tgl");
		// $starttime = $this->input->post("timestart");
		// $endtime = $this->input->post("timeend");
		// $islibur = $this->input->post("islibur");
		$overtime = $this->input->post("overtime");
		$keterangan = $this->input->post("keterangan");

		$starttime_hour = $this->input->post("starttime_hour");
		$starttime_minute = $this->input->post("starttime_minute");

		$endtime_hour = $this->input->post("endtime_hour");
		$endtime_minute = $this->input->post("endtime_minute");

		$starttime = $starttime_hour.':'.$starttime_minute; 
		$endtime = $endtime_hour.':'.$endtime_minute;

		$uangmakan = 0;
		$file_name = 'Lembur '.$this->input->post('nip').' '.date("Y-m-d");
		$config = $this->Main_Model->set_upload_options("./assets/lembur/","*","5048000",$file_name);
		$this->upload->initialize($config);

		if($nip == '' || $tgl == '' || $starttime == '' || $endtime == '' || $overtime == '' || $keterangan == '') {
			$message = '';
			if ($nip == '') $message .= 'Field NIP masih kosong ! <br>';
			if ($overtime == '') $message .= 'Field Jenis Overtime masih kosong ! <br>';
			if ($tgl == '') $message .= 'Field Tanggal masih kosong ! <br>';
			if ($starttime == '') $message .= 'Field Waktu Mulai masih kosong ! <br>';
			if ($endtime == '') $message .= 'Field Waktu Selesai masih kosong ! <br>';
			if ($keterangan == '') $message .= 'Field Keterangan masih kosong ! <br>';

 			$status = array(
 				'status' => false, 
 				'message' => $message
 			);
		} else {
			// if(strtotime($starttime) > strtotime($endtime)) {
			// 	$status = array(
			// 		'status' => false, 
			// 		'message' => 'Format Jam Salah!'
			// 	);
			// } else {
				$tgl = $this->Main_Model->convert_tgl($tgl);
				$mulai = $tgl.' '.$starttime;
				if (strtotime($starttime) > strtotime($endtime)) {
					$date = date('Y-m-d', strtotime("$tgl + 1 days"));
					$date = $date.' '.$endtime;

					$selesai = $date;
				} else {
					$date = $tgl.' '.$endtime;

					$selesai = $date;
				}

				$jml_jam = $this->Tunjangan_Model->hitung_jam($mulai, $selesai);

				// $seconds = strtotime($endtime) - strtotime($starttime);
				// $hours = floor($seconds / 3600);
				// $mins = floor($seconds / 60 % 60);
				// $jumlah = floor($seconds / 1800);
				// $timeFormat = sprintf('%02d:%02d', $hours, $mins);
				// $splitJam = explode(":", $timeFormat);
				// $jml_jam = $splitJam[0];
				
				$o = $this->Tunjangan_Model->id_overtime($overtime);
				$flag = isset($o->flag) ? $o->flag : 0;
				$nominal = isset($o->nominal) ? $o->nominal : 0;
				if ($flag == 1) {
					$total = $nominal * $jml_jam;
				} else {
					$total = $nominal;
				}

				$data = array(
					'nip' => $nip,
					'id_overtime' => $overtime,
					'tgl' => $tgl,
					'jml_jam' => $jml_jam,
					'lembur' => $total,
					'starttime' => $starttime,
					'endtime' => $endtime,
					// 'islibur' => $islibur,
					'status' => 1,
					'uangmakan' => $uangmakan, 
					'keterangan' => $keterangan,
					'insert_at' => now(),
					'user_insert' => username()
				);
				
				if($this->upload->do_upload('upload')) {
					$file_surat = $this->upload->data();
					$data['file'] = $file_surat['file_name'];
				}

				$this->Tunjangan_Model->add_lembur($data);
				$status = array(
					'status' => true, 
					'message' => 'Data telah disimpan!'
				);
			// }
		}

		echo json_encode($status);
	}

	function lembur_id()
	{
		$id = $this->input->get('id');
		$data = $this->Tunjangan_Model->lembur_id($id);

		echo json_encode($data);
	}

	function update_lembur()
	{
		$this->Main_Model->all_login();
		$this->load->library('upload');
		$id_lembur = $this->input->post('id');
		$nip = $this->input->post("nip");
		$tgl = $this->input->post("tgl");
		// $starttime = $this->input->post("timestart");
		// $endtime = $this->input->post("timeend");
		$overtime = $this->input->post("overtime");
		$keterangan = $this->input->post("keterangan");
		// $islibur = $this->input->post("islibur");
		$starttime_hour = $this->input->post("starttime_hour");
		$starttime_minute = $this->input->post("starttime_minute");

		$endtime_hour = $this->input->post("endtime_hour");
		$endtime_minute = $this->input->post("endtime_minute");

		$starttime = $starttime_hour.':'.$starttime_minute; 
		$endtime = $endtime_hour.':'.$endtime_minute;

		$uangmakan 	= 0;

		$file_name 	= 'Lembur '.$this->input->post('nip').' '.date("Y-m-d");
		$config 	= $this->Main_Model->set_upload_options("./assets/lembur/","*","5048000",$file_name);
		$this->upload->initialize($config);
		
		if($nip == '' || $tgl == '' || $starttime == '' || $endtime == '' || $overtime == '' || $keterangan == '') {
			$message = '';
			if ($nip == '') $message .= 'Field NIP masih kosong ! <br>';
			if ($overtime == '') $message .= 'Field Jenis Overtime masih kosong ! <br>';
			if ($tgl == '') $message .= 'Field Tanggal masih kosong ! <br>';
			if ($starttime == '') $message .= 'Field Waktu Mulai masih kosong ! <br>';
			if ($endtime == '') $message .= 'Field Waktu Selesai masih kosong ! <br>';
			if ($keterangan == '') $message .= 'Field Keterangan masih kosong ! <br>';

 			$status = array('status' => false, 'message' => $message);
		} else {
			// if(strtotime($starttime)>strtotime($endtime)) {
			// 	$status = array('status'=>false, 'message'=>'Format Jam Salah!');
			// } else {
	 		// 	$seconds = strtotime($endtime) - strtotime($starttime);
				// $hours = floor($seconds / 3600);
				// $mins = floor($seconds / 60 % 60);
				// $jumlah = floor($seconds / 1800);
				// $timeFormat = sprintf('%02d:%02d', $hours, $mins);
				// $splitJam = explode(":", $timeFormat);
				// $jml_jam = $splitJam[0];
				$tgl = $this->Main_Model->convert_tgl($tgl);
				$mulai = $tgl.' '.$starttime;
				if (strtotime($starttime) > strtotime($endtime)) {
					$date = date('Y-m-d', strtotime("$tgl + 1 days"));
					$date = $date.' '.$endtime;

					$selesai = $date;
				} else {
					$date = $tgl.' '.$endtime;

					$selesai = $date;
				}

				$jml_jam = $this->Tunjangan_Model->hitung_jam($mulai, $selesai);
				
				$o = $this->Tunjangan_Model->id_overtime($overtime);
				$flag = isset($o->flag) ? $o->flag : 0;
				$nominal = isset($o->nominal) ? $o->nominal : 0;
				if ($flag == 1) {
					$total = $nominal * $jml_jam;
				} else {
					$total = $nominal;
				}

				$data 		= array(
					'nip' => $nip,
					'tgl' => $tgl,
					'jml_jam' => $jml_jam,
					'lembur' => $total,
					'starttime' => $starttime,
					'endtime' => $endtime,
					// 'islibur' => $islibur,
					'uangmakan'	=> $uangmakan,
					'id_overtime' => $overtime,
					'keterangan' => $keterangan,
					'user_update' => username(),
					'update_at' => now()
				);

				if($this->upload->do_upload('upload')) {
					$file_surat = $this->upload->data();
					$data['file'] = $file_surat['file_name'];
				}

				$this->Tunjangan_Model->update_lembur($data,$id_lembur);
				$status = array('status'=>true, 'message'=>'Data telah diupdate!');
			// }
		}

		echo json_encode($status);
	}

	function delete_lembur()
	{
		$id = $this->input->post("id");
		$this->Tunjangan_Model->delete_lembur($id);
	}

	function tunj_lainnya()
	{
		$this->Main_Model->get_login();
		$url_del 	= base_url('tunjangan/delete_lainnya');
		$javascript = '
			<script type="text/javascript">
				var save_method;
				'.$this->Main_Model->default_select2()
				 .$this->Main_Model->default_datepicker().'

				function load_table()
				{
					var periode 	= {"periode":$("#periode").val()}
					$.ajax({
						url 		: "'.base_url('tunjangan/view_lainnya').'",
						type 		: "GET",
						data 		: periode,
						beforeSend 	: function()
						{
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete 	: function()
						{
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success 	: function(data)
						{
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			        			responsive: true
			    			});
						},
						error 		: function(jqXHR, textStatus, errorThrown)
						{
							bootbox.alert("Internal Server Error");
						} 
					})
				}
				load_table();

				function reset()
				{
					save_method="save";
					$("#myModal").modal();
					$(".kosong").val("");
					$(".select2").val("").trigger("change");
				}

				function save()
				{
					$.ajax({
						url 	: "'.base_url('tunjangan/process_lainnya').'",
						data 	: $("#tunj_lainnya").serialize(),
						type 	: "POST",
						dataType: "JSON",
						success : function(data){
							load_table();
							if(data.status=="true")
							{
								'.$this->Main_Model->notif200().'
	    						$(".kosong").val("");
								$(".select2").val("").trigger("change");
								load_table();
	    					}
	    					else
	    					{
	    						'.$this->Main_Model->notif400().'
	    					}
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							'.$this->Main_Model->notif500().'
						}
					});
				}

				function get_id(id)
				{
			    	$.ajax({
			    		url 	: "'.base_url('tunjangan/lainnya_id').'/"+id,
			    		dataType: "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
			    			$("#id").val(data.id_t_lain);
							$("#nip").val(data.nip).trigger("change");
							$("#tgl").val(data.tanggal);
							$("#jumlah").val(data.jumlah);
							$("#keterangan").val(data.keterangan);
			    		},
			    		error 	: function(jqXHR, textStatus, errorThrown){
			    			'.$this->Main_Model->notif500().'
			    		}
			    	});
			    }

			    '.$this->Main_Model->default_delete_data($url_del).'
			</script>';
		$footer =
			array(
				'javascript'=> $javascript,
				'js' 		=> $this->footer()
			);
		$data 	= 
			array(
				'option' 	=> 
					array(
						'1' 	=> 'Ya',
						'0' 	=> 'Tidak'
					),
				'nip' 		=> $this->Main_Model->all_kary_active(),
				'periode' 	=> $this->Main_Model->periode_opt()
			);
		$this->load->view('template/header',$this->header());
		$this->load->view('tunjangan/lainnya',$data);
		$this->load->view('template/footer',$footer);
	}

	function view_lainnya()
	{
		$this->Main_Model->get_login();
		$periode 	= $this->input->get('periode');
		$data 		= $this->Tunjangan_Model->view_lainnya($periode);
		$template 	= $this->Main_Model->tbl_temp();

		$this->table->set_heading('No','NIP','Nama','Tanggal','Jumlah','Keterangan','Action');
		$no=1;
		foreach ($data as $row) {
		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$this->Main_Model->format_tgl($row->tgl),
			$this->Main_Model->uang($row->jumlah),
			$row->keterangan,
			$this->Main_Model->default_action($row->id_t_lain)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_lainnya()
	{
		$this->Main_Model->get_login();
		$id 		= $this->input->post("id");
		$nip 		= $this->input->post("nip");
		$tgl 		= $this->input->post("tgl");
		$jumlah 	= $this->input->post("jumlah");
		$keterangan = $this->input->post("keterangan");

		if($nip=="" || $tgl=="" || $jumlah=="")
		{
			$result = array('status'=>'false');
		}
		else
		{
			$data = array( 
				'nip' 		=> $nip,
				'tgl' 		=> $this->Main_Model->convert_tgl($tgl),
				'jumlah' 	=> $jumlah,
				'keterangan'=> $keterangan
				);

			$result = array('status'=>'true');
			$this->Tunjangan_Model->process_lainnya($data,$id);
		}

		echo json_encode($result);
	}

	function delete_lainnya()
	{
		$id = $this->input->post("id");
		$this->Tunjangan_Model->delete_lainnya($id);
	}

	function lainnya_id($id)
	{
		$data = $this->Tunjangan_Model->lainnya_id($id);
		echo json_encode($data);
	}

	function view_approval_lembur()
	{
		$this->Main_Model->all_login();
		$data = $this->Tunjangan_Model->view_approval_lembur();
		$template 	= $this->Main_Model->tbl_temp('tb_lembur');
		$is_approval = $this->Main_Model->is_approval();

		$array_header = array('No','NIP','Nama','Tanggal','Jam','Scan Pulang');
		($is_approval == TRUE) ? $array_header[] = 'Action' : $array_header = $array_header;

		$this->table->set_heading($array_header);
		$no = 1; 
		foreach ($data as $row) {
			$scan_pulang = ($row->scan_pulang == '00:00:00') ? '' : $row->scan_pulang;

			$array_data = array($no++, $row->nip, $row->nama, $this->Main_Model->format_tgl($row->tgl), $row->starttime.' - '.$row->endtime, $scan_pulang);
			($is_approval == TRUE) ? $array_data[] = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="app_lembur(' . $row->id_lembur . ');"> <i class="fa fa-check"></i> Klarifikasi </a>' : $array_data = $array_data;

			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function validasi_lembur($date_start='', $date_end='') 
	{
		$query = $this->db->query("
			SELECT *
			FROM lembur a
			WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
			AND a.jml_jam >= 5")->result();

		if (! empty($query)) {
			foreach ($query as $row) {
				$tgl = $row->tgl;
				$starttime = $row->starttime;
				$endtime = $row->endtime;
				$id_lembur = $row->id_lembur;
				$overtime = $row->id_overtime;
				$mulai = $tgl.' '.$starttime;
				if (strtotime($starttime) > strtotime($endtime)) {
					$date = date('Y-m-d', strtotime("$tgl + 1 days"));
					$date = $date.' '.$endtime;

					$selesai = $date;
				} else {
					$date = $tgl.' '.$endtime;

					$selesai = $date;
				}

				$jml_jam = $this->Tunjangan_Model->hitung_jam($mulai, $selesai);
				
				$o = $this->Tunjangan_Model->id_overtime($overtime);
				$flag = isset($o->flag) ? $o->flag : 0;
				$nominal = isset($o->nominal) ? $o->nominal : 0;
				if ($flag == 1) {
					$total = $nominal * $jml_jam;
				} else {
					$total = $nominal;
				}

				$data = array(
					'tgl' => $tgl,
					'jml_jam' => $jml_jam,
					'lembur' => $total,
					'starttime' => $starttime,
					'endtime' => $endtime
				);

				$this->Tunjangan_Model->update_lembur($data, $id_lembur);
			}
		}
	}

	function laporan_lembur_divisi()
	{
		$this->Main_Model->get_login();

        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_datepicker()
              .$this->Main_Model->js_bs_select()
              .$this->Main_Model->js_button_excel();

        $menu = $this->Main_Model->menu_admin('0','0','2');

        $header = array(
              'menu' => $menu,
              'style' => $this->Main_Model->style_datatable()
                          .$this->Main_Model->style_modal()
                          .$this->Main_Model->style_select2()
                          .$this->Main_Model->style_timepicker()
                          .$this->Main_Model->style_datepicker()
                          .$this->Main_Model->style_bs_select()
              );

        $cabang = [];
        $ms_cabang = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
        if ($ms_cabang) {
        	$cabang[] = 'Semua Cabang';
        	foreach ($ms_cabang as $row) {
        		$cabang[$row->id_cab] = $row->cabang;
        	}
        }

        $divisi = [];
        $ms_divisi = $this->Main_Model->view_by_id('ms_divisi', ['status' => 1], 'result');
        if ($ms_divisi) {
        	$divisi[] = 'Semua Divisi';
        	foreach ($ms_divisi as $row) {
        		$divisi[$row->id_divisi] = $row->divisi;
        	}
        }

        $data = array(
              'footer' => $this->Main_Model->footer($js),
              'penutup' => $this->Main_Model->close_page(),
              'cabang' => $cabang,
              'divisi' => $divisi
        );

        $this->load->view('template/header', $header);
        $this->load->view('tunjangan/laporan-lembur-divisi', $data);
	}

	function view_laporan_lembur_divisi()
	{
		$this->Main_Model->get_login();
		$cabang = $this->input->get('cabang');
		$divisi = $this->input->get('divisi');
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');

        $date_start = $this->Main_Model->convert_tgl($date_start);
        $date_end = $this->Main_Model->convert_tgl($date_end);

        $data = $this->Tunjangan_Model->view_laporan_lembur_divisi($cabang, $divisi, $date_start, $date_end);

        echo $data;
	}
}