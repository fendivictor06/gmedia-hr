<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Cuti_Model', '', TRUE);
	}

	function index()
	{
		show_404();
	}

	function laporan_cuti()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		(empty($cabang)) ? $menu = $this->Main_Model->menu_admin('0','0','4') : $menu = $this->Main_Model->menu_user('0','0','72');

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);
		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'th' => $this->Main_Model->cuti_y()
			);

		(empty($cabang)) ? $template = 'template/header' : $template = 'akun/header';

		$this->load->view($template, $header);
		$this->load->view('cuti/laporan_cuti', $data);
	}

	function laporan_terlambat()
	{
		$this->Main_Model->get_login();
		$javascript = '
			<script>
				'.$this->Main_Model->default_select2().'

				$("#download").click(function(){
					bulan = $("#bulan").val(), tahun = $("#tahun").val();
					link = tahun+"/"+bulan;
					window.location.href = "'.base_url('download_excel/laporan_terlambat').'/"+link;
				})
			</script>
		';
		$tahun = $this->db->query("SELECT DISTINCT(DATE_FORMAT(a.`tgl`,'%Y')) tahun FROM terlambat_temp a")->result();
		foreach($tahun as $row) {
			$th[$row->tahun] = $row->tahun;
		}
		$header = $this->Main_Model->header('menu4');
		$footer = array(
			'js' => $this->Main_Model->footer(),
			'javascript' => $javascript
			);
		$data = array(
			'th' => $th
			);
		$this->load->view('template/header', $header);
		$this->load->view('absensi/laporan_terlambat', $data);
		$this->load->view('template/footer', $footer);
	}

	function laporan_sp()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		(empty($cabang)) ? $menu = $this->Main_Model->menu_admin('0','0','4') : $menu = $this->Main_Model->menu_user('0','0','79');
		
		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$q = $this->db->query("SELECT DISTINCT(YEAR(a.`awal_sp`)) th FROM tb_sp a")->result();
		$th = array();
		foreach($q as $r) {
			$th[$r->th] = $r->th;
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'th' => $th
			);

		(empty($cabang)) ? $template = 'template/header' : $template = 'akun/header';

		$this->load->view($template, $header);
		$this->load->view('absensi/laporan_sp', $data);
	}

	function laporan_absensi()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->Main_Model->session_cabang();
		$javascript = '
			<script>
				'.$this->Main_Model->default_datepicker()

				 .$this->Main_Model->default_select2()

				 .$this->Main_Model->notif().'

				 $("#download").click(function(){
				 	var cabang = $("#cabang").val();
				 	var date_start = $("#date_start").val();
				 	var date_end = $("#date_end").val();
				 	var nip = $("#nama").val();

				 	var a = date_start.split("/");
				 	var b = date_end.split("/");

				 	if (cabang == "" || date_start == "" || date_end == "") {
				 		if (cabang == "") {
				 			notif("Pilih Cabang");
				 		} else if (date_start == "") {
				 			notif("Masukkan tgl Mulai");
				 		} else {
				 			notif("Masukkan tgl Selesai");
				 		}
				 	} else if (new Date(a[1]+"/"+a[0]+"/"+a[2]) > new Date(b[1]+"/"+b[0]+"/"+b[2])) {
				 		notif("Format Tanggal Salah !");
				 	} else {
				 		window.location.href="' . base_url('download_excel/laporan_absensi') . '/"+cabang+"/"+a[2]+"-"+a[1]+"-"+a[0]+"/"+b[2]+"-"+b[1]+"-"+b[0]+"/"+nip;
				 	}	
				 });

				 $(document).ready(function(){
					var idCabang = $("#cabang").val();

					onChangeCabang(idCabang);
				 });

				 function onChangeCabang(idCabang) {
					$("#nama").html("");

					var config = {
						url: "'.base_url('karyawan/select2_karyawan?cabang=').'"+idCabang,
						type: "get",
						dataType: "json"
					}

					$.ajax(config).
					done(function(data) {
						var element = "";

						if (data.length > 0) {
							for (i = 0; i < data.length; i++) {
								element += "<option value=\""+data[i].nik+"\"> "+data[i].nik+ " - " +data[i].nama+" </option>";
							}
						}

						$("#nama").append(element);

						$("#nama").val("").trigger("change");
					}).
					fail(function() {
						toastr.error("Terjadi kesalahan saat memuat data");
					});
				 }

				 $("#cabang").change(function() {
				 	var cabang = $(this).val();

					onChangeCabang(cabang);
				 });
			</script>
		';

		$menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$header = array(
				'menu' => $menu,
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_datepicker()
				          .$this->Main_Model->style_select2()
			);
		$footer = array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_datepicker()
				                  .$this->Main_Model->js_select2()
			);
		$idp 	= $this->session->userdata('idp');

		$cabang_create = array();
		if(!empty($sess_cabang)) {
			foreach($sess_cabang as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang_create[$d->id_cab] = $d->cabang;
			}
		} else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang_create[$row->id_cab] = $row->cabang;
			}
		}

		$data 	= array(
			'cabang' => $cabang_create
			);

		$this->load->view('template/header', $header);
        $this->load->view('absensi/laporan_absensi', $data);
        $this->load->view('template/footer', $footer);
	}
}

/* End of file laporan.php */
/* Location: ./application/controllers/laporan.php */ ?>