<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Refreshment extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Informasi_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript 	= '
			<script>
					function reset()
					{
						$(".blank").val("");
					}
				'
				 .$this->Main_Model->default_loadtable('refreshment/view_refreshment')

				 .$this->Main_Model->default_datepicker()

				 .$this->Main_Model->notif()

				 .$this->Main_Model->post_data('refreshment/refreshment_process','save()','$("#form_refreshment").serialize()','
				 	if(data.status==true)
				 	{
				 		load_table();
				 		reset();
				 	} 
				 	notif(data.message);')

				 .$this->Main_Model->get_data('refreshment/refreshment_id','get_id(id)','
				 	$("#myModal").modal();
				 	$("#nominal").val(data.nominal);
				 	$("#tgl").val(data.tgl);
				 	$("#keterangan").val(data.keterangan);
				 	$("#id").val(data.id);')

				 .$this->Main_Model->default_delete_data('refreshment/delete_refreshment').'
			</script>
		';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','6'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_datepicker()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_datepicker()
			);

		$this->load->view('template/header', $header);
        $this->load->view('informasi/data_refreshment');
        $this->load->view('template/footer', $footer);
	}

	function view_refreshment()
	{
		$this->Main_Model->get_login();
		$idp   		= $this->session->userdata('idp');
        $data 		= $this->Informasi_Model->view_refreshment($idp);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Nominal','Tanggal','Keterangan','Action');
		$no =1;
        
        foreach ($data as $row) {
		$this->table->add_row(
			$no++,
			$this->Main_Model->uang($row->nominal),
			$this->Main_Model->format_tgl($row->tanggal),
			$row->keterangan,
			$this->Main_Model->default_action($row->id)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function refreshment_process()
	{
		$this->Main_Model->get_login();
		$id 		= $this->input->post('id');
		$nominal 	= $this->input->post('nominal');
		$tgl 		= $this->input->post('tgl');
		$keterangan = $this->input->post('keterangan');
		$idp 		= $this->session->userdata('idp');

		if($nominal == "" || $tgl == "")
		{
			$result = array('status'=>false,'message'=>'Form masih ada yang kosong!');
		}
		else
		{
			$data 		= array(
				'nominal' 		=> $nominal,
				'tanggal' 		=> $this->Main_Model->convert_tgl($tgl),
				'keterangan' 	=> $keterangan,
				'idp' 			=> $idp
				);
			$result = array('status'=>true,'message'=>'Success!');
			$this->Informasi_Model->refreshment_process($data,$id);
		}
		echo json_encode($result);
	}

	function refreshment_id($id)
	{
		$this->Main_Model->get_login();
		$data =	$this->Informasi_Model->refreshment_id($id);
		echo json_encode($data);
	}

	function delete_refreshment()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->post('id');
		$this->Informasi_Model->delete_refreshment($id);
	}

}

/* End of file refreshment.php */
/* Location: ./application/controllers/refreshment.php */ ?>