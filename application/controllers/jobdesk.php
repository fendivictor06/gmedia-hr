<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobdesk extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_summernote();

		$header = array(
			'menu' => $this->Main_Model->menu_admin('0', '0', '3'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_summernote()
		);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
		);

		$this->load->view('template/header', $header);
		$this->load->view('rekrutmen/form_jobdesk',$data);
	}

	function form_jobdesk($id='')
	{
		$this->Main_Model->get_login();
		$js = $this->Main_Model->js_modal().$this->Main_Model->js_bootbox().$this->Main_Model->js_ckeditor();

		$header = array(
			'menu' => $this->Main_Model->menu_admin('0', '0', '3'),
			'style' => $this->Main_Model->style_modal()
		);

		$condition = array('id' => $id);
		$q = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'row');

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'q' => $q
		);

		$this->load->view('template/header', $header);
		$this->load->view('rekrutmen/jobdesk_form',$data);
	}

	function view_jobdesk()
	{
		$this->Main_Model->all_login();
		$condition = array('status' => 1);
		$data = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'result');
		$template = $this->Main_Model->tbl_temp('tb_jobdesk');
		$this->table->set_heading('No', 'Posisi', 'Jobdesk', 'Action');

		$no = 1;
		foreach ($data as $row) {
			$delete = array('event' => "delete_data('".$row->id."')", 'label' => 'Delete');
			$edit = array('url' => base_url('jobdesk/form_jobdesk/'.$row->id), 'label' => 'Update');
			$action = array($edit, $delete);

			$this->table->add_row(
				$no++,
				$row->jab,
				$row->description,
				$this->Main_Model->action($action)
			);
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_jobdesk()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$jab = $this->input->post('jab');
		$description = $this->input->post('summernote');
		$username = $this->session->userdata('username');

		if ($jab == '' || $description == '') {
			$status = FALSE;
			$message = '';
			if ($jab == '') $message .= 'Field Jabatan Masih Kosong ! <br>';
			if ($description == '') $message .= 'Field Deskripsi Masih Kosong !';    
		} else {
			$userat = ($id == '') ? 'insert_at' : 'update_at'; 
			$useraction = ($id == '') ? 'user_insert' : 'user_update';
			$data = array(
				'jab' => $jab,
				'description' => $description,
				$userat => date('Y-m-d H:i:s'),
				$useraction => $username
			);

			$condition = ($id == '') ? '' : array('id' => $id);

			$simpan = $this->Main_Model->process_data('ms_jobdesk', $data, $condition);
			if ($simpan > 0) {
				$status = TRUE;
				$message = 'Success!';
			} else {
				$status = FALSE;
				$message = 'Failed!';
			}
		}

		echo json_encode(array('status' => $status, 'message' => $message));
	}

	function id_jobdesk($id='')
	{
		$this->Main_Model->all_login();
		$condition = array('id' => $id);
		$data = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'row');

		echo json_encode($data);
	}

	function delete_jobdesk($id='')
	{
		$this->Main_Model->all_login();
		$condition = array('id' => $id);
		$username = $this->session->userdata('username');
 		$data = array(
			'status' => 0,
			'update_at' => date('Y-m-d H:i:s'),
			'user_update' => $username
		);
		$delete = $this->Main_Model->process_data('ms_jobdesk', $data, $condition);

		if ($delete > 0) {
			$status = TRUE;
			$message = 'Success!';
		} else {
			$status = FALSE;
			$message = 'Failed!';
		}

		echo json_encode(array('status' => $status, 'message' => $message));
	}
}

/* End of file jobdesk.php */
/* Location: ./application/controllers/jobdesk.php */ ?>