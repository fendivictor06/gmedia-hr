<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekrutmen extends CI_Controller 
{

	function __construct()
    {
        parent::__construct();
        $this->load->model('Penggajian_Model', '', TRUE);
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Rekrutmen_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->library('pagination');
    }

    function header()
    {
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">
				<link href="'.base_url('assets/plugins/bootstrap-summernote/summernote.css').'" rel="stylesheet" type="text/css" />';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-summernote/summernote.min.js').'" type="text/javascript"></script>';

		return $footer;
	}

	function dat_loker()
	{
		$this->Main_Model->get_login();
		$url 	= base_url('rekrutmen/view_datloker');
		$url_del= base_url('rekrutmen/delete_loker');
		$footer = array(
			'javascript' 	=> minifyjs('
				<script type="text/javascript">
					'.$this->Main_Model->default_loadtable($url).
					  $this->Main_Model->default_datepicker().'

					$(document).ready(function(){
					 	$(".bs-select").selectpicker({
				            iconBase: "fa",
				            tickIcon: "fa-check"
				        });
					 });

					$("#summernote_1").summernote({
                		height: 300
            		});
					
					function add_new() {
						document.getElementById("form_loker").reset();
						$(".note-editable").html("");
						$("#id").val("");
						$(".bs-select").selectpicker("refresh");
					}

					function get_id(id) {
				    	$.ajax({
				    		url : "'.base_url('rekrutmen/loker_id').'/"+id,
				    		data : id,
				    		dataType : "json",
				    		success : function(data){
				    			$("#posisi").val(data.posisi);
								$("#summernote_1").val(data.deskripsi);
								$(".note-editable").html(data.deskripsi);
								$("#id").val(data.id_loker);
								$("#tgl_awal").val(data.tgl_awal);
								$("#tgl_akhir").val(data.tgl_akhir);

								id_kota = data.id_kota;
							 	if(id_kota != null) {
								 	kota = id_kota.split(",");
								 	q = [];
					                for(i=0; i < kota.length; i++) {
					                    q.push(kota[i]);
					                }
					                $("#kota").selectpicker("val", q);
					            } else {
					            	$("#kota").selectpicker("deselectAll");
					            }
					            $("#myModal").modal();
				    		}
				    	});
				    }

					function save() {
						$.ajax({
							url : "'.base_url('rekrutmen/process_loker').'",
							data : $("#form_loker").serialize(),
							type : "post",
							dataType : "json",
							success : function(data){
								if(data.status == true) {
									add_new(), load_table();
								}
								bootbox.alert(data.message);
							}
						});
					}

					'.$this->Main_Model->default_delete_data($url_del).'
				</script>
			'),
 			'js' => $this->footer().$this->Main_Model->js_bs_select()
			);
		

		$data = array(
			'lku_opt' => $this->Main_Model->lku_idp(),
			'kota' => $this->Main_Model->kota_option(),
			'status' => array(
				'0'	=> 'Tidak',
				'1'	=> 'Ya'
				)
			);

		$header = array(
			'style' => $this->header().$this->Main_Model->style_bs_select(),
			'menu' => $this->Main_Model->menu_admin('0','0','5')
		);

		$this->load->view('template/header',$header);
		$this->load->view('rekrutmen/dat_loker',$data);
		$this->load->view('template/footer',$footer);
	}

	function view_datloker()
	{
		$this->Main_Model->get_login();
		$data 		= $this->Rekrutmen_Model->view_datloker();
		$template 	= $this->Main_Model->tbl_temp();

		// $this->table->set_heading('No','Posisi','Kota','Deskripsi','Tgl Aktif','Action');
		$this->table->set_heading('No', 'Posisi', 'Deskripsi', 'Action');
		$no=1;

		foreach ($data as $row) {
		
		$this->table->add_row(
			$no++,
			$row->posisi,
			// $row->kota,
			substr(strip_tags($row->deskripsi), 0, 90).'...',
			// $row->date_start.' s/d '.$row->date_end,
			$this->Main_Model->default_action($row->id_loker)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_loker()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$posisi = $this->input->post('posisi');
		// $lku = $this->input->post('lku');
		$desc = $this->input->post('summernote');
		$kota = $this->input->post('kota');
		$tgl_awal = $this->input->post('tgl_awal');
		$tgl_akhir = $this->input->post('tgl_akhir');

		// if($posisi == '' || $desc == '' || empty($kota) || $tgl_awal == '' || $tgl_akhir == '') {
		if($posisi == '' || $desc == '') {
			$message = '';
			if($posisi == '') $message .= 'Field Posisi masih kosong! <br>';
			if($desc == '') $message .= 'Field Deskripsi masih kosong! <br>';
			if(empty($kota)) $message .= 'Pilih Salah satu Kota <br>';
			if($tgl_awal == '') $message .= 'Field Tanggal Awal masih kosong! <br>';
			if($tgl_akhir == '') $message .= 'Field Tanggal Akhir masih kosong! <br>';

			$result = array('status' => false, 'message' => $message);
		} else {
			if ($id != '') $this->Main_Model->delete_data('r_loker_detail', array('id_loker' => $id));
			$q = $this->db->query("SELECT MAX(a.`id_loker`) + 1 AS maks FROM r_loker a")->row();
			$id_loker = $q->maks;
			($id == '') ? $id_loker = $id_loker : $id_loker = $id;
			$date_start = isset($tgl_awal) ? $this->Main_Model->convert_tgl($tgl_awal) : '';
			$date_end = isset($tgl_akhir) ? $this->Main_Model->convert_tgl($tgl_akhir) : '';

			$data = array(
				'id_loker' => $id_loker,
				'posisi' => $posisi,
				'deskripsi' => $desc,
				'date_start' => $date_start,
				'date_end' => $date_end
			);

			if (!empty($kota)) {
				$jumlah = count($kota);
				for($i = 0; $i < $jumlah; $i++) {
					$data_detail[$i] = array(
						'id_loker' => $id_loker,
						'id_kota' => $kota[$i]
						);
					$this->Main_Model->process_data('r_loker_detail', $data_detail[$i]);
				}
			}

			($id == '') ? $this->Rekrutmen_Model->add_loker($data) : $this->Rekrutmen_Model->update_loker($data,$id);
			$result = array('status' => true, 'message' => 'Success!');
		}

		echo json_encode($result);
	}

	function loker_id($id='')
	{
		$this->Main_Model->get_login();
		$data  	= $this->Rekrutmen_Model->loker_id($id);

		echo json_encode($data);
	}

	function delete_loker(){
		$this->Main_Model->get_login();
		$id  =	$this->input->post('id');
		$this->Rekrutmen_Model->delete_loker($id);
	}

	function proses_rekrutmen()
	{
		$this->Main_Model->all_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker();
		$acctype = $this->session->userdata('acctype');
		$menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0','0','3') : $this->Main_Model->menu_user('0','0','80');
		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
			);

		$status = array(
			'1' => 'Pasang Iklan',
			'2' => 'Screening',
			'3' => 'Interview Awal',
			'4' => 'Interview User',
			'5' => 'Finalisasi',
			'6' => 'Penyerahan'
		);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'acctype' => $acctype,
			'periode' => $this->Main_Model->periode_opt(),
			'status' => $status
			);

		$this->load->view('template/header', $header);
		// $this->load->view('template/body', $body);
		$this->load->view('rekrutmen/proses_rekrutmen', $data);	
	}

	function view_proses_rekrutmen()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Rekrutmen_Model->view_pengajuan_karyawan($qd_id);
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Cabang','Posisi','Pengajuan','Jml','Spesifikasi','Pemenuhan','Atas Nama','Status',
        	// 'Status HR','Progress','Status TTD',
        	'Action');
		$no =1;
        foreach ($data as $row) {
        	$q = $this->db->query("
        		SELECT a.*,b.nama 
        		FROM d_pengajuan_kary a 
        		JOIN kary b ON a.nip = b.nip 
        		WHERE a.id = '$row->id'")->result();

        	$tgl = '';
        	$nip = '';
        	if($q) {
        		$i = 1;
        		foreach($q as $r) {
        			$tgl .= $this->Main_Model->format_tgl($r->tgl_pemenuhan).'<br>';
        			$nip .= $r->nama.'<br>';
        		}
        	} else {
        		$tgl = 'ON PROGRESS';
        		$nip = '';
        	}

        $action = array();

        $approve = array('label' => 'Approve', 'event' => "approve('".$row->id."')");
        $change = array('label' => 'Ubah Status', 'event' => "change_status('".$row->id."')");

        $iklan = array('label' => 'Pasang Iklan', 'event' => "change('".$row->id."', '1')");
        $screen = array('label' => 'Screening', 'event' => "change('".$row->id."', '2')");
        $interview_awal = array('label' => 'Interview Awal', 'event' => "change('".$row->id."', '3')");
        $interview_user = array('label' => 'Interview User', 'event' => "change('".$row->id."', '4')");
        $finalisasi = array('label' => 'Finalisasi', 'event' => "change('".$row->id."', '5')");
        $penyerahan = array('label' => 'Penyerahan', 'event' => "change('".$row->id."', '6')");


        if ($row->status_hr == 9) {
        	$action[] = $approve; 
        }

        if ($row->status_hr == 7) {
        	// $action[] = $iklan;
        	// $action[] = $screen;
        	// $action[] = $interview_awal;
        	// $action[] = $interview_user;
        	// $action[] = $finalisasi;
        	// $action[] = $penyerahan;

        	$action[] = $change;
        }

        $sal_pos = ($row->sal_pos != '') ? '('.$row->sal_pos.') ' : '';

		$this->table->add_row(
			$no++,
			$row->cabang,
			$sal_pos.$row->posisi,
			
			($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '',
			$row->jumlah,
			array('data' => $row->spesifikasi, 'style' => 'width = 90px'),
			$tgl,
			$nip,
			// $row->keterangan,
			// $row->ket_hr,
			// $row->ket_progress,
			// $row->ket_ttd,
			array('style' => 'width:150px;', 'data' => '<strong>Status</strong> : '.$row->keterangan.'<br>'.
			'<strong>Status HR</strong> : '.$row->ket_hr.'<br>'.
			'<strong>Progress</strong> : '.$row->ket_progress.'<br>'.
			'<strong>TTD</strong> : '.$row->ket_ttd),
			$this->Main_Model->action($action)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function approval_pengajuan($id='', $val='')
	{
		$this->Main_Model->all_login();
		$data = array(
			'status_hr' => $val
			);
		$this->Karyawan_Model->pengajuan_karyawan_process($data, $id);
	}

	function change_status($id='', $val='')
	{
		$this->Main_Model->all_login();
		$data = array(
			'progress' => $val
			);
		$this->Karyawan_Model->pengajuan_karyawan_process($data, $id);
	}

}