<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Absensi_Model', '', TRUE);
		$this->load->library('PHPExcel');
		$this->load->model('Upload_Model', '', TRUE);
	}

	function header()
	{
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">
				<link href="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css').'" rel="stylesheet" type="text/css" />';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/js/jquery.ajaxfileupload.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js').'"></script>';

		return $footer;
	}

	function up_kary()
	{
		$this->Main_Model->get_login();
		$javascript = '';

		$footer = array(
			'javascript' => $javascript,
			'js'         => $this->footer()
			);

		$data = array(
			'title'  => 'Upload Data Karyawan',
			'action' => base_url('upload/kary_process')
			);
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0','0','69')
        );
        
		$this->load->view('template/header',$header);
		$this->load->view('upload/upload_form',$data);
		$this->load->view('template/footer',$footer);
	}

	function kary_process()
	{
		// $this->Main_Model->get_login();
		$config['upload_path'] = './assets/uploads/';
        // $config['allowed_types'] = 'xlsx|xls';
        $config['allowed_types'] = 'xls';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        
        if ( ! $this->upload->do_upload()){
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger">
                    <button class="close" data-close="alert"></button>
                    Format File Salah!
                </div>');
            redirect('upload/up_kary');
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->Upload_Model->upload_data($filename);
            unlink('./assets/uploads/'.$filename);
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success">
                    <button class="close" data-close="alert"></button>
                    Success !
                </div>');
            redirect('upload/up_kary');
        }
    }

    function uploadsps()
    {
    	// $this->Main_Model->get_login();
    	$config['upload_path'] = './assets/uploads/';
        // $config['allowed_types'] = 'xlsx|xls';
        $config['allowed_types'] = 'xls';

        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        $id_per = $this->input->post('qd_id_sps');
        if ( ! $this->upload->do_upload()){
            $this->session->set_flashdata('message', '<script>bootbox.alert("Format File Salah!");</script>');
            redirect('absensi/dataabsensi/success');
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->Upload_Model->upload_sps($filename,$id_per);
            unlink('./assets/uploads/'.$filename);
            
            $this->session->set_flashdata('message', '<script>bootbox.alert("Success!");</script>');
            redirect('absensi/dataabsensi/success');
        }
    }

    function upload_ach()
    {
        // $this->Main_Model->get_login();
        $config['upload_path'] = './assets/uploads/';
        // $config['allowed_types'] = 'xlsx|xls';
        $config['allowed_types'] = 'xls';

        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        $id_per = $this->input->post('qd_id_ach');
        if ( ! $this->upload->do_upload()){
            $this->session->set_flashdata('message', '<script>bootbox.alert("Format File Salah!");</script>');
            redirect('achievement/download_ach');
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->Upload_Model->upload_ach($filename,$id_per);
            unlink('./assets/uploads/'.$filename);
            
            $this->session->set_flashdata('message', '<script>bootbox.alert("Success!");</script>');
            redirect('achievement/download_ach');
        }
    }

    function upload_telat()
    {
    	// $this->Main_Model->get_login();
    	$config['upload_path'] = './assets/uploads/';
        // $config['allowed_types'] = 'xlsx|xls';
        $config['allowed_types'] = 'xls';
        
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload', $config);
        $id_per = $this->input->post('qd_id_sps');
        if ( ! $this->upload->do_upload()){
            $this->session->set_flashdata('message', '<script>bootbox.alert("Format File Salah!");</script>');
            redirect('absensi/terlambat/success');
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->Upload_Model->upload_telat($filename,$id_per);
            unlink('./assets/uploads/'.$filename);
            
            $this->session->set_flashdata('message', '<script>bootbox.alert("Success!");</script>');
            redirect('absensi/terlambat/success');
        }
    }

    function upload_absensi()
    {
        $name = $_FILES['userfile']['name'];       
        $acak = rand(000000,999999);
        $nama_file = $acak.'-'.$name;
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = '*';
        $config['file_name'] = $nama_file;

        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload()) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger"> <button class="close" data-close="alert"></button> Format File Salah! </div>');
            redirect('upload_absensi');
        } else {
            $this->load->helper('file');

            // $absen = fopen(''.base_url().'assets/uploads/'.$nama_file.'', 'r');
            // $data = array();

            $file = fopen(base_url().'assets/uploads/'.$nama_file, "r") or exit("Unable to open file!");
            //Output a line of the file until the end is reached
            while(!feof($file))
            {
                $line = fgets($file);

                // $tanggal = '20'.substr($line, 9, 2).'-'.substr($line, 7, 2).'-'.substr($line, 5, 2);
                // $jam = substr($line, 11, 2).':'.substr($line, 13, 2);
                // $pin = substr($line, 18, 8);
                // $scandate = $tanggal.' '.$jam;

                $tanggal = '20'.substr($line, 7, 2).'-'.substr($line, 5, 2).'-'.substr($line, 3, 2);
                $jam = substr($line, 9, 2).':'.substr($line, 11, 2);
                $pin = substr($line, 16, 8);
                $branch = substr($line, 2, 1);
                $scandate = $tanggal.' '.$jam;

                $pin = substr($pin, 5);

                if($branch == 1){
                    $branch = 1;
                } else if ($branch == 2){
                    $branch = 3;
                }

                $query = $this->get_id_employee($pin, $branch);
                if ($query->num_rows() > 0) {
                    $item = $query->row();
                    $nik = $item->nik;

                    $data = array(
                        'scan_date' => $scandate,
                        'nip' => $nik,
                        'pin' => $pin,
                        'keterangan' => 'Upload Manual'
                    );

                    $this->Main_Model->delete_data('tb_scanlog', array('nip' => $nik, 'scan_date' => $scandate));
                    $this->Main_Model->process_data('tb_scanlog', $data);
                }

                // $pin = substr($pin, 5);

                // $q = $this->Main_Model->view_by_id('kary', array('pin' => $pin), 'row');
                // $q = $this->db->query("
                //         SELECT nip
                //         FROM kary a
                //         WHERE SUBSTR(a.nip, 4, 3) = '$pin' ")->row();

                // $nip = isset($q->nip) ? $q->nip : '';

                
                // if ($q) {
                    // $check_exists = $this->Main_Model->view_by_id('tb_scanlog', array('nip' => $nip, 'scan_date' => $scandate), 'row');
                    // if ($check_exists) {
                        
                    // }

                    
                    // $this->Main_Model->delete_data('tb_scanlog', array('nip' => $nip, 'scan_date' => $scandate));
                    // $this->Main_Model->process_data('tb_scanlog', $data);
                // }
            }
            fclose($file);

            // print_r($data); exit;
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success"> <button class="close" data-close="alert"></button> Success ! </div>');
            redirect('upload_absensi');
        }
    }

    function get_id_employee($badgeno='', $branch='')
    {
        $db = $this->Main_Model->connect_to_absensi();

        $db->from('ms_badge');
        $db->where('status', '1');
        $db->where('badgeno', $badgeno);
        if( $branch ) {
            $db->where('id_branch', $branch);
        }

        return $db->get();
    }

    function upload_rekrutmen()
    {
        $config['upload_path'] = './assets/uploads/';
        // $config['allowed_types'] = 'xlsx|xls';
        $config['allowed_types'] = 'xls';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload())
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger"> <button class="close" data-close="alert"></button> Format File Salah! </div>');
            redirect('karyawan/calon_kary');
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->Upload_Model->upload_rekrutmen($filename);
            unlink('./assets/uploads/'.$filename);
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success"> <button class="close" data-close="alert"></button> Success ! </div>');
            redirect('karyawan/calon_kary');
        }
    }

    function upload_jadwal()
    {
        $this->Main_Model->all_login();
        $config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'xlsx|xls';
        // $config['allowed_types'] = 'xls';
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload()) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" id="danger"> <button class="close" data-close="alert"></button> Format File Salah! </div>');
            redirect('shift/upload_jadwal');
        } else {
            $data = array('upload_data' => $this->upload->data());
            $upload_data = $this->upload->data(); //Mengambil detail data yang di upload
            $filename = $upload_data['file_name'];//Nama File
            $this->Upload_Model->upload_jadwal($filename);
            unlink('./assets/uploads/'.$filename);
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" id="success"> <button class="close" data-close="alert"></button> Success ! </div>');
            redirect('shift/upload_jadwal');
        }
    }
	
}
?>