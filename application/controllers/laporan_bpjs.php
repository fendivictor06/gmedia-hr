<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_Bpjs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Penggajian_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript = '
			<script>
				$("#unduh").click(function(){
                    var tipe = $("#tipe").val();
                    var bulan = $("#bulan").val();
                    var tahun = $("#tahun").val();
					window.location.href="'.base_url('laporan_bpjs/download_laporan').'/"+tipe+"/"+tahun+"/"+bulan;
				});

                $(document).ready(function(){
                    $(".select2").select2();
                });
			</script>
		';
		$header = array(
				'menu' => $this->Main_Model->menu_admin('0','0','2'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
                          .$this->Main_Model->style_select2()
			);
		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				        .$this->Main_Model->js_modal()
				        .$this->Main_Model->js_bootbox()
                        .$this->Main_Model->js_select2()
			);
		$data = array(
				'periode' => $this->Main_Model->periode_opt()
			);
		$this->load->view('template/header', $header);
        $this->load->view('penggajian/laporan_bpjs', $data);
        $this->load->view('template/footer', $footer);
	}

	function download_laporan($tipe='', $tahun='', $bulan='')
	{
		$this->Main_Model->get_login();
        $q = periode($tahun, $bulan);
        $qd_id = isset($q->qd_id) ? $q->qd_id : '';
        if ($qd_id != '') {
    		if($tipe == 'ks') {
    			$this->download_bpjsks($qd_id);
    		} else {
    			$this->download_bpjstk($qd_id);
    		}
        } else {
            redirect('laporan_bpjs');
        }
	}

	function download_bpjstk($qd_id='')
	{
		$this->Main_Model->get_login();
		ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $ambildata = $this->Penggajian_Model->download_bpjstk($qd_id);
        // print_r($ambildata);exit;
        $per = $this->Main_Model->id_periode($qd_id);
        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
        $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
        $objPHPExcel->getProperties()->setTitle("Download BPJKS TK");
        $objPHPExcel->getProperties()->setSubject("Download BPJKS TK");
        $objPHPExcel->getProperties()->setDescription("BPJKS TK");

        $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object

        $objget->setTitle('BPJKS TK'); //sheet title

        $title = 'Rekap BPJS TK Periode '.$per->periode;
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        $objset->setCellValue('A1', $title);

        $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                  )
              )
        );
        //table header
        $cols = array("A","B","C","D","E","F","G","H","I");
        $val = array("NO","NIP","NAMA","POSISI","JKK","JKM","JHTTK","JHTP","TOTAL");

        for ($a=0;$a<9; $a++) 
            {
                $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 4;
            $i=1;
            $total_jkk = 0;
            $total_jkm = 0;
            $total_jhttk = 0;
            $total_jhtp = 0;
            $grandtotal = 0;
            foreach ($ambildata as $frow){
                $q = $this->Main_Model->lastpos($frow->nip);
                $bpjs = $this->Penggajian_Model->view_bpjs();
                $jab = isset($q->jab) ? $q->jab : '';
                $jkk = 0;
                $jkm = 0;
                $jhttk = 0;
                $jhtp = 0;
                
                $th_fk = $this->Main_Model->th_fk();
                
                if(!empty($q))
                {
                    $fk = $this->Main_Model->fk_lku($q->id_lku, $th_fk->th);
                    $umk = isset($fk->umk) ? $fk->umk : 0;
                    $jkk = ($umk * $bpjs->jkk)/100;
                    $jkm = ($umk * $bpjs->jkm)/100;
                    $jhttk = ($umk * $bpjs->jhttk)/100;
                    $jhtp = ($umk * $bpjs->jhtp)/100;
                }

                $total = $jkk + $jkm + $jhttk + $jhtp;
                // print_r($total);exit;
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $i++); 
                $objset->setCellValue("B".$baris, $frow->nip); 
                $objset->setCellValue("C".$baris, $frow->nama);
                $objset->setCellValue("D".$baris, $jab);
                $objset->setCellValue("E".$baris, $jkk);
                $objset->setCellValue("F".$baris, $jkm);
                $objset->setCellValue("G".$baris, $jhttk);
                $objset->setCellValue("H".$baris, $jhtp);
                $objset->setCellValue("I".$baris, $total);
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
                $total_jkk += $jkk;
                $total_jkm += $jkm;
				$total_jhttk += $jhttk;
				$total_jhtp += $jhtp;
                $grandtotal += $total;
            }
            
            $b=$baris-1;
            $objset->setCellValue('D'.$baris, 'Total');
            $objset->setCellValue('E'.$baris, $total_jkk);
            $objset->setCellValue('F'.$baris, $total_jkm);
            $objset->setCellValue('G'.$baris, $total_jhttk);
            $objset->setCellValue('H'.$baris, $total_jhtp);
            $objset->setCellValue('I'.$baris, $grandtotal);
            $objPHPExcel->getActiveSheet()->getStyle('A3:I'.$baris)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('BPJKS TK');

            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$title.'.xls"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
	}

	function download_bpjsks($qd_id='')
    {
        $this->Main_Model->get_login();
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);

        define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $ambildata = $this->Penggajian_Model->download_bpjsks($qd_id);
        // print_r($ambildata);exit;
        $per = $this->Main_Model->id_periode($qd_id);

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download BPJKS KS");
            $objPHPExcel->getProperties()->setSubject("Download BPJKS KS");
            $objPHPExcel->getProperties()->setDescription("BPJKS KS");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('BPJKS KS'); //sheet title

            $title = 'Rekap BPJS KS Periode '.$per->periode;
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );


            //table header
            $cols = array("A","B","C","D","E","F","G","H");
             
            $val = array("NO ","NIP","NAMA","POSISI","KELAS","TKS","PKS","TOTAL");
             
            for ($a=0;$a<8; $a++) 
            {
                $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 4;
            $i=1;
            $total_tks = 0;
            $total_pks = 0;
            $grandtotal = 0;
            foreach ($ambildata as $frow){
                $q = $this->Main_Model->lastpos($frow->nip);
                $bpjs = $this->Penggajian_Model->view_aturan_bpjs_ks();
                $jab = isset($q->jab) ? $q->jab : '';
                $tks = 0;
                $pks = 0;
                if($frow->kelas == 1)
                {
                    $tks = ($bpjs->kelas * $bpjs->tks)/100;
                    $pks = ($bpjs->kelas * $bpjs->pks)/100;
                }
                else
                {
                    $th_fk = $this->Main_Model->th_fk();
                    if($q)
                    {
	                    $fk = $this->Main_Model->fk_lku($q->id_lku, $th_fk->th);
	                    $tks = ($fk->umk * $bpjs->tks)/100;
	                    $pks = ($fk->umk * $bpjs->pks)/100;
	                }
	                else
	                {
	                	$tks = 0;
	                	$pks = 0;
	                }
                }
                $total = $tks + $pks;
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $i++); 
                $objset->setCellValue("B".$baris, $frow->nip); 
                $objset->setCellValue("C".$baris, $frow->nama);
                $objset->setCellValue("D".$baris, $jab);
                $objset->setCellValue("E".$baris, $frow->kelas);
                $objset->setCellValue("F".$baris, $tks);
                $objset->setCellValue("G".$baris, $pks);
                $objset->setCellValue("H".$baris, $total);
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
                $total_tks += $tks;
                $total_pks += $pks;
                $grandtotal += $total;
            }
            
            $b=$baris-1;
            $objset->setCellValue('E'.$baris, 'Total');
            $objset->setCellValue('F'.$baris, $total_tks);
            $objset->setCellValue('G'.$baris, $total_pks);
            $objset->setCellValue('H'.$baris, $grandtotal);
            $objPHPExcel->getActiveSheet()->getStyle('A3:H'.$baris)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('BPJKS KS');

            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$title.'.xls"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }
}

/* End of file laporan_bpjs.php */
/* Location: ./application/controllers/laporan_bpjs.php */ ?>