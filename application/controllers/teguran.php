<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Teguran extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Cuti_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (!empty($cabang)) ? $this->Main_Model->menu_user('0','0','79') : $this->Main_Model->menu_admin('0','0','4');
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'nip' => $this->Main_Model->kary_cabang(null, null, '-'),
			'bln' => list_bulan(),
			'thn' => list_tahun(),
			'teguran' => array(
				'1' => 'Ya',
				'0' => 'Tidak'
				),
			'periode' => $this->Main_Model->periode_opt()
			);

		$this->load->view('template/header', $header);
		$this->load->view('absensi/form_teguran',$data);
	}

	function view_teguran()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Karyawan_Model->view_teguran($id_periode);
		$template = $this->Main_Model->tbl_temp('tb_teguran');
		$array_heading = array('No', 'NIP', 'Nama', 'Jabatan', 'Cabang', 'Periode', 'Pencapaian (%)', 'Pencapaian (Rp)', 'Target(Rp)', 'Teguran', 'Rek SP', 'Action');
		$this->table->set_heading($array_heading);
		$no = 1;
		foreach($data as $row) {
			switch ($row->teguran) {
				case '1':
					$teguran = 'Ya';
					break;
				default:
					$teguran = 'Tidak';
					break;
			}
			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$row->jabatan,
				$row->cabang,
				convert_bulan($row->bulan).' '.$row->tahun,
				$row->persen_pencapaian.'%',
				$this->Main_Model->uang($row->rp_pencapaian),
				$this->Main_Model->uang($row->target),
				$teguran,
				$row->sp,
				$this->Main_Model->default_action($row->id)
				);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function process_teguran()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$nip = $this->input->post('nip');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$persen_pencapaian = $this->input->post('persen_pencapaian');
		$rp_pencapaian = $this->input->post('rp_pencapaian');
		$teguran = $this->input->post('teguran');
		$target = $this->input->post('target');
		$rec_sp = $this->input->post('rec_sp');

		if($nip == '' || $bln == '' || $thn == '' || $persen_pencapaian == '' || $rp_pencapaian == '' || $teguran == '' || $rec_sp == '') {
			$message = '';
			if($nip == '') $message .= 'Field Nama Masih Kosong! <br>';
			if($bln == '') $message .= 'Field Bulan Masih Kosong! <br>';
			if($thn == '') $message .= 'Field Tahun Masih Kosong! <br>';
			if($persen_pencapaian == '') $message .= 'Field Pencapaian (%) Masih Kosong! <br>';
			if($rp_pencapaian == '') $message .= 'Field Pencapaian (Rp) Masih Kosong! <br>';
			if($teguran == '') $message .= 'Field Teguran Masih Kosong! <br>'; 
			if($rec_sp == '') $message .= 'Field Rekomendasi SP Masih Kosong! <br>';

			$result = array('status' => false, 'message' => $message);
		} else {
			$datetime = ($id != '') ? 'update_at' : 'insert_at';
			$user = ($id != '') ? 'user_update' : 'user_insert';

			$q = $this->Main_Model->posisi($nip);
			$jabatan = isset($q->jab) ? $q->jab : '';
			$id_cabang = isset($q->id_cab) ? $q->id_cab : '';

			$data = array(
				'nip' => $nip,
				'jabatan' => $jabatan,
				'id_cabang' => $id_cabang,
				'bulan' => $bln,
				'tahun' => $thn,
				'persen_pencapaian' => $persen_pencapaian,
				'rp_pencapaian' => $rp_pencapaian,
				'target' => $target,
				'teguran' => $teguran,
				'sp' => $rec_sp,
				$datetime => date('Y-m-d H:i:s'),
				$user => $this->session->userdata('username')
				);

			$condition = ($id != '') ? array('id' => $id) : array();
			$insert = $this->Main_Model->process_data('tb_teguran', $data, $condition);

			if($insert > 0) {
				$result = array('status' => true, 'message' => 'Success!');
			} else {
				$result = array('status' => false, 'message' => 'Gagal Menyimpan Data!');
			}
		}
		echo json_encode($result);
	}

	function id_teguran($id='')
	{
		$this->Main_Model->all_login();
		$condition = array('id' => $id);
		$data = $this->Main_Model->view_by_id('tb_teguran', $condition, 'row');

		echo json_encode($data);
	}

	function delete_teguran($id='')
	{
		$this->Main_Model->all_login();
		$condition = array('id' => $id);
		$delete = $this->Main_Model->delete_data('tb_teguran', $condition);

		if($delete > 0) {
			$result = array('status' => true, 'message' => 'Success!');
		} else {
			$result = array('status' => false, 'message' => 'Gagal Menghapus Data!');
		}
		echo json_encode($result);
	}

	function laporan()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		(empty($cabang)) ? $menu = $this->Main_Model->menu_admin('0','0','4') : $menu = $this->Main_Model->menu_user('0','0','79');
		
		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$q = $this->Karyawan_Model->th_teguran();
		$th = array();
		foreach($q as $r) {
			$th[$r->th] = $r->th;
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'th' => $th
			);

		($cabang == 0) ? $template = 'template/header' : $template = 'akun/header';

		$this->load->view($template, $header);
		$this->load->view('absensi/laporan_teguran', $data);
	}
}

/* End of file teguran.php */
/* Location: ./application/controllers/teguran.php */ ?>