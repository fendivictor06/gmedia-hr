<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_Terlambat extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Cuti_Model', '', true);
    }

    function index()
    {
        $this->Main_Model->get_login();
        $javascript     = '
			<script>
				'.$this->Main_Model->default_datepicker()

                 .$this->Main_Model->default_select2()

                 .$this->Main_Model->notif().'

				 $("#download").click(function(){
				 	var cabang = $("#cabang").val();
				 	var date_start = $("#date_start").val();
				 	var date_end = $("#date_end").val();

				 	var a = date_start.split("/");
				 	var b = date_end.split("/");

				 	if (cabang == "" || date_start == "" || date_end == "") {
				 		notif("Form masih kosong !");
				 	} else if (new Date(a[1]+"/"+a[0]+"/"+a[2]) > new Date(b[1]+"/"+b[0]+"/"+b[2])) {
				 		notif("Format Tanggal Salah !")
				 	} else {
				 		window.location.href="' . base_url('rekap_terlambat/laporan_excel') . '/"+cabang+"/"+a[2]+"-"+a[1]+"-"+a[0]+"/"+b[2]+"-"+b[1]+"-"+b[0];
				 	}	
				 });
			</script>
		';
        $header         = array(
                'menu' => $this->Main_Model->menu_admin('0', '0', '4'),
                'style' => $this->Main_Model->style_datatable()
                          .$this->Main_Model->style_modal()
                          .$this->Main_Model->style_datepicker()
                          .$this->Main_Model->style_select2()
            );
        $footer         = array(
                'javascript'    => $javascript,
                'js'            => $this->Main_Model->js_datatable()
                                  .$this->Main_Model->js_modal()
                                  .$this->Main_Model->js_bootbox()
                                  .$this->Main_Model->js_datepicker()
                                  .$this->Main_Model->js_select2()
            );
        $idp    = $this->session->userdata('idp');
        $cabang = $this->Karyawan_Model->view_cabang($idp);
        foreach ($cabang as $row) {
            $a[$row->id_cab] = $row->cabang;
        }
        $data   = array(
            'cabang' => $a
            );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/laporan_terlambat', $data);
        $this->load->view('template/footer', $footer);
    }

    function laporan_excel($cabang = '', $date_start = '', $date_end = '')
    {
        $this->Main_Model->get_login();
        $date_akhir = $date_end.' 23:59:00';
        $date_awal = $date_start;
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');
        // $ambildata = $this->Absensi_Model->download_sps($id_per, $id_lwok);

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Rekap Terlambat");
            $objPHPExcel->getProperties()->setSubject("Rekap Terlambat");
            $objPHPExcel->getProperties()->setDescription("Rekap Terlambat");
 
            $objset     = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget     = $objPHPExcel->getActiveSheet();  //inisiasi get object
            
            $tgl_awal   = $this->Main_Model->format_tgl($date_start);
            $tgl_akhir  = $this->Main_Model->format_tgl($date_end);

            $judul      = 'Rekap Terlambat '.$tgl_awal.' sd '.$tgl_akhir;
            $title      = 'Rekap Terlambat';

            $objget->setTitle($title); //sheet title

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );
    
            $mulaix     = round(strtotime($date_start)/60/60/24);
            $sampaix    = round(strtotime($date_end)/60/60/24);
            $selisih    = $sampaix - $mulaix + 1;
           
            $w2 = $w = 0;
            $arr    = array();

        for ($q = $mulaix; $q <= $sampaix; $q++) {
            $w2++;
            $hr     = date('w', ($q*60*60*24));
            $pls    = $q + (7 - ($hr));
            if ($w2 == 1 or $hr == 1) {
                $t  = strftime("%Y-%m-%d", ($q*60*60*24));
                $tt1= strftime("%Y-%m-%d", ($q*60*60*24));
                $r  = date('w', strtotime($tt1));
                $t3 = strftime("%Y-%m-%d", ($pls*60*60*24));
                $t4 = round(strtotime($t3)/60/60/24);
                    
                if ($r == 0) {
                    $t2 = strftime("%Y-%m-%d", ($q*60*60*24));
                    $tt2= strftime("%Y-%m-%d", ($q*60*60*24));
                } elseif ($t4 > $sampaix) {
                    $t2 = strftime("%Y-%m-%d", ($sampaix*60*60*24));
                    $tt2= strftime("%Y-%m-%d", ($sampaix*60*60*24));
                } else {
                    $t2 = strftime("%Y-%m-%d", ($pls*60*60*24));
                    $tt2= strftime("%Y-%m-%d", ($pls*60*60*24));
                }
                $arr[] = $t.' s/d '.$t2;
            }
            if ($hr == 1) {
                $w++;
            }
        }
            
            $panjang= count($arr);
        if ($panjang == 0) {
            $panjang = 1;
        }

            $week   = array();
        for ($a=0; $a<$panjang; $a++) {
            array_push($week, $arr[$a]);
        }

            // print_r($week);exit;
            
            $cab = $this->db->where('id_cab', $cabang)->get('ms_cabang')->row();
            $objset->setCellValue('A1', $cab->cabang);

            $objset->setCellValue('A3', 'Nama');
            $jumlah_merge1 = count($week) + 1;
            $jumlah_merge2 = (2 * count($week)) + 1;

            $cell_merge1   = 'B3:'.$this->head($jumlah_merge1).'3';
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells($cell_merge1);

            $cell_merge2   = $this->head($jumlah_merge1 + 1).'3:'.$this->head($jumlah_merge2).'3';
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells($cell_merge2);

            $objset->setCellValue('B3', 'Keterlambatan');
            $objset->setCellValue($this->head($jumlah_merge1 + 1).'3', 'Tidak Absen Pagi');
            $objset->setCellValue($this->head($jumlah_merge2 + 1).'3', 'Total Keterlambatan');
            $objset->setCellValue($this->head($jumlah_merge2 + 2).'3', 'Total Tidak Absen Pagi');

            $jumlah_all = (count($week) * 2) + 3;

            $cols = array();
        for ($i=1; $i<=$jumlah_all; $i++) {
            array_push($cols, $this->head($i));
        }
             
            $val = $week;
             
        for ($a=1; $a<=count($week); $a++) {
            $objset->setCellValue($cols[$a].'4', $week[$a-1]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->head($a + 1))->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }

        for ($a=1; $a<=count($week); $a++) {
            $objset->setCellValue($cols[$a+count($week)].'4', $week[$a-1]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension($this->head($a + count($week)+1))->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }

            // print_r(explode('s/d', $week[0]));exit;
            
            $query_nip = $this->db->query("
            	SELECT a.`id`,a.`pin`,a.`tgl`,a.`total_menit`,b.`nama`,b.`nip` 
            	FROM terlambat_temp a 
            	JOIN kary b ON a.`nip`=b.`nip` 
            	JOIN sk c ON b.`nip`=c.`nip` 
            	JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` 
            	JOIN pos e ON d.`id_pos`=e.`id_pos` 
            	WHERE c.`aktif`='1' 
            	AND e.`id_cabang`='$cabang' 
            	AND a.`tgl` BETWEEN '$date_awal' AND '$date_akhir'
            	GROUP BY a.`nip`")->result();
            $baris  = 5;
            $i = 1;



        foreach ($query_nip as $row) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $row->nama);
            $total_a = 0;
            $total_b = 0;

            for ($i=0; $i<count($week); $i++) {
                $split_range = explode("s/d", $week[$i]);
                $range_a = $split_range[0];
                $range_b = $split_range[1];
           
                $cari_terlambat = $this->db->query("
                		SELECT COUNT(a.`tgl`) jml 
                		FROM terlambat_temp a 
                		WHERE a.`tgl` BETWEEN '$range_a' AND '$range_b' 
                		AND a.`pin`='$row->pin'")->row();

                $objset->setCellValue($this->head(2 + $i).$baris, $cari_terlambat->jml);

                    
                $total_a = $total_a + $cari_terlambat->jml;
            }

            for ($i=0; $i<count($week); $i++) {
                $split_range = explode("s/d", $week[$i]);
                $range_a = $split_range[0];
                $range_b = $split_range[1];

                $cari_tidak_absen_masuk = $this->db->query("
                		SELECT count(a.`tgl`) jml 
                		FROM tb_absensi a 
                		WHERE a.`tgl` BETWEEN '$range_a' AND '$range_b' 
                		AND a.`scan_masuk` = '00:00:00' AND a.`scan_pulang` <> '00:00:00' 
                		AND a.`pin`='$row->pin'")->row();

                $objset->setCellValue($this->head(2 + count($week) + $i).$baris, $cari_tidak_absen_masuk->jml);

                $total_b = $total_b + $cari_tidak_absen_masuk->jml;
            }
                
            $objset->setCellValue($this->head((count($week) * 2) + 2).$baris, $total_a);
            $objset->setCellValue($this->head((count($week) * 2) + 3).$baris, $total_b);

            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:'.$this->head($jumlah_all).$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle($title);

            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'.$judul.'.xls"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function head($index)
    {
        $list_header  = array(
                1   => "A",
                2   => "B",
                3   => "C",
                4   => "D",
                5   => "E",
                6   => "F",
                7   => "G",
                8   => "H",
                9   => "I",
                10  => "J",
                11  => "K",
                12  => "L",
                13  => "M",
                14  => "N",
                15  => "O",
                16  => "P",
                17  => "Q",
                18  => "R",
                19  => "S",
                20  => "T",
                21  => "U",
                22  => "V",
                23  => "W",
                24  => "X",
                25  => "Y",
                26  => "Z"
            );

        return $list_header[$index];
    }
}

/* End of file rekap_terlambat.php */
/* Location: ./application/controllers/rekap_terlambat.php */
