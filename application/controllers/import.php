<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
		$this->load->model('Cuti_Model');
	}

	function generate_cuti($year='')
    {
        ($year == '') ? $year = date('Y') : $year = $year;
        $this->Main_Model->generate_cuti($year); 
    }

	function cuti_import()
	{
		$this->db->truncate('cuti_det');
		$this->db->truncate('cuti_sub_det');

		$db = $this->Main_Model->connect_to_absensi();
		$q = $db->get('tr_cuti')->result();

		if (!empty($q)) {
			foreach ($q as $row) {
				$approval = '';
				switch ($row->status) {
					case '0':
						$approval = '6';
						break;
					case '1':
						$approval = '12';
						break;
					case '2':
						$approval = '13';
						break;
					case '3':
						$approval = '12';
						break;
					case '4':
						$approval = '13';
						break;
					default:
						$approval = '6';
						break;
				}

				$q = $this->Cuti_Model->generate_nomor_cuti();
				$id_cuti = isset($q->maks) ? $q->maks : 0;

				$arr = array(
					'id_cuti_det' => $id_cuti,
					'id_tipe' => 1,
					'nip' => $row->id_employee,
					'alasan_cuti' => $row->keterangan,
					'alamat_cuti' => '',
					'no_hp' => '',
					'approval' => $approval,
					'insert_at' => date('Y-m-d H:i:s'),
					'user_insert' => 'admin'
				);
				
				$tanggal = array();
				$date_start = $row->mulai;
				$date_end = $row->sampai;
				while (strtotime($date_start) <= strtotime($date_end)) {
						$tanggal[] = $date_start;
						$date_start = date("Y-m-d", strtotime("+1 day", strtotime($date_start)));
					}

				$jumlah = count($tanggal);
				if ($jumlah > 0) {
					for ($i = 0; $i < $jumlah; $i++) {
						$arr_det[$i] = array(
							'id_cuti_det' => $id_cuti,
							'tgl' => $tanggal[$i]
						);
						$this->Cuti_Model->add_subdet_cuti($arr_det[$i]);
					}
				}
				$this->Cuti_Model->add_cutibiasa($arr);
			}
		}

		$this->generate_cuti();
		echo 'complete!';
	}

	function lembur_import()
	{
		$this->db->truncate('lembur');

		$db = $this->Main_Model->connect_to_absensi();
		$q = $db->query("
				SELECT a.`id_overtime`, a.`id_employee` AS nip, a.`tanggal` AS tgl,
				HOUR(TIMEDIFF(a.`jam_selesai`, a.`jam_mulai`)) AS jml_jam, IFNULL(b.`nominal`, 0) AS lembur,
				a.`jam_mulai` AS starttime, a.`jam_selesai` AS endtime, a.`keterangan`, '1' AS `status`
				FROM tr_overtime a
				LEFT JOIN tr_lembur b ON a.`id_employee` = b.`id_employee` 
				AND a.`tanggal` = b.`tanggal` ")->result();

		if (! empty($q)) {
			foreach ($q as $row) {
				$arr = array(
					'id_overtime' => $row->id_overtime,
					'nip' => $row->nip,
					'tgl' => $row->tgl,
					'jml_jam' => $row->jml_jam,
					'lembur' => $row->lembur,
					'starttime' => $row->starttime,
					'endtime' => $row->endtime,
					'keterangan' => $row->keterangan,
					'status' => $row->status,
					'insert_at' => date('Y-m-d H:i:s'),
					'user_insert' => 'admin'
				);

				$this->db->insert('lembur', $arr);
			}
		}

		echo 'Complete!';
	}

	function scanlog_import()
	{
		$this->db->truncate('tb_scanlog');
		$db = $this->Main_Model->connect_to_absensi();
		$data = $db->query("
				SELECT CONCAT(a.`tanggal`,' ',a.`jam`) AS scanlog, id_employee
				FROM tr_absensi a
				WHERE YEAR(a.tanggal) = '2018'")->result();
		if ($data) {
			foreach ($data as $row) {
				$insert = array(
					'scan_date' => $row->scanlog,
					'nip' => $row->id_employee
				);

				$this->db->insert('tb_scanlog', $insert);
			}
		}

		echo 'complete!';
	}

	function jadwal_import()
	{
		$this->db->where('DATE(tgl)', '2018')
				->delete('tr_shift');

		$db = $this->Main_Model->connect_to_absensi();
		$data = $db->where('DATE(tanggal)', '2018')
					->get('ms_jadwal')
					->result();

		if ($data) {
			foreach ($data as $row) {
				$insert = array(
					'id_shift' => $row->id_range,
					'tgl' => $row->tanggal,
					'nip' => $row->id_employee,
					'keterangan' => $row->keterangan
				);

				$this->db->insert('tr_shift', $insert);
			}
 		}

 		echo 'complete!';
	}
}

/* End of file import.php */
/* Location: ./application/controllers/import.php */ ?>