<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pulang_Awal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Cuti_Model', '', TRUE);
        $this->load->model('Reminder_Model', '', TRUE);
        $this->load->model('Rest_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$month = date('m');
        // $month = str_replace('0', '', $month);
        $month = (int)$month;
        $year = date('Y');

        $ms_penyetuju = $this->Main_Model->master_penyetuju();

		$penyetuju = array();
		foreach ($ms_penyetuju as $row) {
			$penyetuju[$row->pola] = $row->nama;
		}

		$javascript = '
			<script>
				'.$this->Main_Model->notif()

				 .$this->Main_Model->default_datepicker().'

				var year = "'.$year.'";
				var month = "'.$month.'";

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('pulang_awal/view_pulang_awal').'",
						data : {
							"tahun" : tahun,
							"bulan" : bulan
						},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#tambah").addClass("hidden");
							$("#unduh").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#tambah").removeClass("hidden");
							$("#unduh").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						},
						error : function(){
							notif("Gagal Mengambil data!");
						}
					});
				}

				$(document).ready(function(){
					$("#tahun").val(year).trigger("change");
            		$("#bulan").val(month).trigger("change");
					load_table();
				});

				$("#tampil").click(function(){
					load_table();
				});

				function reset()
				{
					$(".blank").val("");
					$(".select2").val("").trigger("change");
					$(".fileinput-filename").html("");
				}
				$(".timepicker-24").timepicker({
	                autoclose	: !0,
	                minuteStep	: 5,
	                showSeconds	: !1,
	                showMeridian: !1
	            });
				$(".timepicker").parent(".input-group").on("click", ".input-group-btn", function(t) {
	                t.preventDefault(), $(this).parent(".input-group").find(".timepicker").timepicker("showWidget")
	            });
				$(function(){
					$("#form_pulang_awal").validate({
						doNotHideMessage: !0,
	                    errorElement: "span",
	                    errorClass: "help-block help-block-error",
						rules : {nip : {required: !0 }, tgl : {required: !0 }, jam_pulang : {required: !0 }, penyetuju : {required: !0 }, alasan : {required: !0 } },
						errorPlacement: function (error, element) {
						    (element.attr("type") == "file") ? error.insertAfter($(".input-group")) : error.insertAfter(element);
						},
						highlight: function(e) {
	                        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
	                    },
	                    unhighlight: function(e) {
	                        $(e).closest(".form-group").removeClass("has-error")
	                    },	
						submitHandler : function(form){
							form.submit();
						}
					});
				});
				
				'.$this->Main_Model->default_select2()

				 .$this->Main_Model->default_delete_data(base_url('pulang_awal/delete_pulang_awal'))

				 .$this->Main_Model->get_data(base_url('pulang_awal/id_pulang_awal'),'get_id(id)','
				 	$("#myModal").modal();
				 	$("#id").val(data.id_pulang_awal);
				 	$("#nip").val(data.nip).trigger("change");
				 	$("#tgl").val(data.tanggal);
				 	$("#jam_pulang").val(data.jam_pulang);
				 	$(".fileinput-filename").html("<i class=\"fa fa-file\"></i> "+data.file_bukti);
				 	$("#alasan").val(data.alasan);
				 	$("#penyetuju").val(data.pola).trigger("change");
				 	$("#userfile").rules("remove");').'

				$("#unduh").click(function(){
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
			    	window.location.href="'.base_url('download_excel/pulang_awal').'/"+tahun+"/"+bulan;
			    });

			    function proses(id, val) {
					$.ajax({
						url : "pulang_awal/proses_approval/"+id+"/"+val,
						type : "post"
					})
				}

			    function approval(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses Pulang Awal?",
                        title : "Proses Pulang Awal",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses(id, "approve");
                                	bootbox.alert("Ijin telah disetujui !");
                                	load_table();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses(id, "reject");
                                	bootbox.alert("Ijin telah ditolak !");
                                	load_table();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}
			</script>
		';
		$header = array(
				'menu' => $this->Main_Model->menu_admin('0','0','4'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_file_upload()
				          .$this->Main_Model->style_select2()
				          .$this->Main_Model->style_datepicker()
				          .$this->Main_Model->style_timepicker()
		);

		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_file_upload()
				                  .$this->Main_Model->js_select2()
				                  .$this->Main_Model->js_validate()
				                  .$this->Main_Model->js_datepicker()
				                  .$this->Main_Model->js_timepicker()
		);

		$data = array(
				'nip' => $this->Main_Model->all_kary_active(),
				'periode' => $this->Main_Model->periode_opt(),
				'penyetuju' => $penyetuju
		);

		$this->load->view('template/header', $header);
        $this->load->view('absensi/data_pulang_awal',$data);
        $this->load->view('template/footer', $footer);
	}

	function view_pulang_awal()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
 		$idp = $this->session->userdata('idp');
        $data = $this->Absensi_Model->view_pulang_awal($idp, $qd_id);
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cabang','Jam Pulang','Status','Alasan','Scanlog','Action');

		$no =1;
        foreach ($data as $row) {
	        $link = '';
	        if($row->flag == 1 && $row->status == 6) $link = '<li> <a href="javascript:;" onclick="approval(' . $row->id_pulang_awal . ');"> <i class="icon-edit"></i> Proses </a> </li>';

	        ($row->file_bukti) ? $download = '<li> <a href="'.base_url('assets/pulang_awal').'/'.$row->file_bukti.'" target="_blank"> <i class="icon-delete"></i> Download File </a> </li>' : $download = '';

			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$row->cabang,
				$row->tgl.' '.jam($row->jam),
				$row->keterangan,
				$row->alasan,
				$row->scanlog,
				'<div class="btn-group dropup">
	                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                    <i class="fa fa-angle-down"></i>
	                </button>
	                    <ul class="dropdown-menu" role="menu">
	                    	'.$download.$link.'
	                        <li>
	                            <a href="javascript:;" onclick="get_id(' . $row->id_pulang_awal . ');">
	                                <i class="icon-edit"></i> Update </a>
	                        </li>
	                        <li>
	                            <a href="javascript:;" onclick="delete_data(' . $row->id_pulang_awal . ');">
	                                <i class="icon-delete"></i> Delete </a>
	                        </li>
	                    </ul>
	            </div>'
		        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function pulang_awal_process($status='')
	{
		$this->Main_Model->all_login();
		$this->load->library('user_agent');
		$this->load->library('upload');
		$id = $this->input->post('id');
		$nip = $this->input->post('nip');
		$tgl = $this->input->post('tgl');
		$penyetuju = $this->input->post('penyetuju');
		$jam_pulang = $this->input->post('jam_pulang');
		$alasan = $this->input->post('alasan');
		$idp = $this->session->userdata('idp');
		$username = $this->session->userdata('username');
		$acctype = $this->session->userdata('acctype');

		$file_pulang_awal 	= 'Pulang Awal '.$nip.' '.$tgl;
		$config_pulang_awal = $this->Main_Model->set_upload_options("./assets/pulang_awal/","*","5048000",$file_pulang_awal);

		$this->upload->initialize($config_pulang_awal);
		if($this->upload->do_upload('userfile')) {
			$upload_file_pulang_awal = $this->upload->data();
		}

		// ($status != '') ? $status = '1' : $status = '2';
		// $k = $this->Main_Model->kode_approval($nip, 'cuti');
		// $approval = isset($k->kode) ? $k->kode : 1;
		$pola = $this->Main_Model->kode_penyetuju($penyetuju);
		$kode_pola = isset($pola->pola) ? $pola->pola : '';
		$approval = isset($pola->kode) ? $pola->kode : '';
		$penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
		$cc = isset($pola->cc) ? $pola->cc : '';

		if ($acctype == 'Administrator') {
			$approval = '12';
		}
		
		$data = array(
			'nip' => $nip,
			'tgl' => $this->Main_Model->convert_tgl($tgl),
			'jam' => $jam_pulang,
			'alasan' => $alasan,
			'status' => $approval,
			'penyetuju' => $penyetuju,
			'pola' => $kode_pola,
			'cc' => $cc,
			'idp' => $idp,
			'user_insert' => $username,
			'insert_at' => now()
		);

		if($this->upload->do_upload('userfile')) {
			$data['file_bukti'] = $upload_file_pulang_awal['file_name'];
		} 

		$this->Absensi_Model->pulang_awal_process($data, $id);
		if ($acctype != 'Administrator') {
			$this->Rest_Model->reminder_pulang_awal($data);
		}
		
		$this->session->set_flashdata('message_upload','<script>notif("Success!");</script>');
		redirect($this->agent->referrer());
	}

	function reminder_pulang_awal($data='')
	{
		$this->Main_Model->all_login();
		$subject = '[Aplikasi HRD] Reminder Pulang Awal';
		$nip = isset($data['nip']) ? $data['nip'] : '';
		$status = isset($data['status']) ? $data['status'] : '';
		$q = $this->Main_Model->kary_nip($nip);
		$nama = isset($q->nama) ? $q->nama : '';

		$w = $this->Main_Model->posisi($nip);
		$id_cabang = isset($w->id_cab) ? $w->id_cab : '0';
		$tgl = $this->Main_Model->format_tgl($data['tgl']);

		$p = $this->Main_Model->posisi($nip);
		$sal_pos = isset($p->sal_pos) ? $p->sal_pos : '';
		$id_divisi = isset($p->id_divisi) ? $p->id_divisi : '';

		$no = 1;
		$message = 'Berikut adalah Daftar Pulang Awal yang butuh diverifikasi';

		$table = '<table border = "1">
                        <tr><th>No</th><th>Nama</th><th>Tanggal</th><th>Jam Pulang</th><th>Status</th><th>Alasan</th></tr>';
        $table .= '<tr><td>'.$no++.'</td><td>'.$nama.'</td><td>'.$tgl.'</td><td>'.$data['jam'].'</td><td>Proses</td><td>'.$data['alasan'].'</td></tr>';
        $table .= '</table>';

        $message .= '<br>'.$table;

        $link_sms = base_url();
        $usr = $this->Main_Model->view_by_id('tb_user_cabang', array('id_cabang' => $id_cabang), 'result');
        foreach ($usr as $row) {
	        $contact = $this->Main_Model->reminder_contact($row->usr_name, $sal_pos, $id_divisi);
	        $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
	        $hp = isset($contact['hp']) ? $contact['hp'] : '08156647203';
	        $user = isset($contact['user']) ? $contact['user'] : '';

	            if($user != '') {
	                $randomstring = $this->Main_Model->generateRandomString(8);
	                // $md5 = md5($randomstring);
	                $url = base_url('rmd').'/'.$randomstring;
	                $link_sms = $url;
	                $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

	                $pesan = $message.'<br>'.$link;

	                $usr_id = $this->Main_Model->view_by_id('users', array('usr_name' => $user), 'row');
	                $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
	                $now = date('Y-m-d');
	                $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

	                $authenticantion = array(
	                    'usr_id' => $id_user,
	                    'token' => $randomstring,
	                    'expiration' => $expiration
	                    );

	                $this->Main_Model->process_data('tb_authentication', $authenticantion);
	            }

	        $message_sms = '(HRD) Ada Pulang Awal butuh diverifikasi, '.$link_sms;

	        if ($email != '') {
                $this->Main_Model->send_mail($email, $pesan, $subject);
            }
            if ($hp != '') {
                // $this->Main_Model->send_sms($message_sms, $hp);
            }
	    }
	}

	function id_pulang_awal($id)
	{
		$data 	= $this->Absensi_Model->id_pulang_awal($id);
		echo json_encode($data);
	}

	function delete_pulang_awal()
	{
		$id 	= $this->input->post('id');
		$this->Absensi_Model->delete_pulang_awal($id);
	}

	function percabang()
	{
		$this->Main_Model->guest_login();
		$month = date('m');
		$month = str_replace('0', '', $month);

		$ms_penyetuju = $this->Main_Model->master_penyetuju();

		$penyetuju = array();
		foreach ($ms_penyetuju as $row) {
			$penyetuju[$row->pola] = $row->nama;
		}

		$year = date('Y');
		$javascript 	= '
			<script>
				'.$this->Main_Model->notif()

				 .$this->Main_Model->default_datepicker().'

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('pulang_awal/view_pulang_awal_percabang').'",
						data : {
			                "tahun" : tahun,
			                "bulan" : bulan
			            },
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#unduh").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#unduh").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						},
						error : function(){
							notif("Gagal Mengambil data!");
						}
					});
				}

				$(document).ready(function(){
					var year = "'.$year.'";
					var month = "'.$month.'";

					$("#tahun").val(year).trigger("change");
					$("#bulan").val(month).trigger("change");
					load_table();
				});


				$("#tampil").click(function(){
                    load_table();
                });

				function reset() {
					$(".blank").val("");
					$(".select2").val("").trigger("change");
					$(".fileinput-filename").html("");
				}

				$(".timepicker-24").timepicker({
	                autoclose	: !0,
	                minuteStep	: 5,
	                showSeconds	: !1,
	                showMeridian: !1
	            });
				$(".timepicker").parent(".input-group").on("click", ".input-group-btn", function(t) {
	                t.preventDefault(), $(this).parent(".input-group").find(".timepicker").timepicker("showWidget")
	            });
				$(function(){
					$("#form_pulang_awal").validate({
						doNotHideMessage: !0,
	                    errorElement: "span",
	                    errorClass: "help-block help-block-error",
						rules : {nip : {required: !0 }, tgl : {required: !0 }, jam_pulang : {required: !0 }, penyetuju : {required: !0 }, alasan : {required: !0 } },
						errorPlacement: function (error, element) {
						    (element.attr("type") == "file") ? error.insertAfter($(".input-group")) : error.insertAfter(element);
						},
						highlight: function(e) {
	                        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
	                    },
	                    unhighlight: function(e) {
	                        $(e).closest(".form-group").removeClass("has-error")
	                    },	
						submitHandler : function(form){
							form.submit();
						}
					});
				});

				$("#unduh").click(function(){
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
			    	window.location.href="'.base_url('download_excel/pulang_awal').'/"+tahun+"/"+bulan;
			    });

				function proses(id, val) {
					$.ajax({
						url : "'.base_url('pulang_awal/proses_approval').'/"+id+"/"+val,
						type : "post"
					})
				}

			    function approval(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses Pulang Awal?",
                        title : "Proses Pulang Awal",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses(id, "approve");
                                	bootbox.alert("Ijin telah disetujui !");
                                	load_table();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses(id, "reject");
                                	bootbox.alert("Ijin telah ditolak !");
                                	load_table();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}
				
				'.$this->Main_Model->default_select2()

				 .$this->Main_Model->default_delete_data(base_url('pulang_awal/delete_pulang_awal'))

				 .$this->Main_Model->get_data(base_url('pulang_awal/id_pulang_awal'),'get_id(id)','
				 	$("#myModal").modal();
				 	$("#id").val(data.id_pulang_awal);
				 	$("#nip").val(data.nip).trigger("change");
				 	$("#tgl").val(data.tanggal);
				 	$("#jam_pulang").val(data.jam_pulang);
				 	$(".fileinput-filename").html("<i class=\"fa fa-file\"></i> "+data.file_bukti);
				 	$("#alasan").val(data.alasan);
				 	$("#penyetuju").val(data.pola).trigger("change");
				 	$("#userfile").rules("remove");').'
			</script>
		';
		$header = array(
				'menu' => $this->Main_Model->menu_user('0','0','72'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_file_upload()
				          .$this->Main_Model->style_select2()
				          .$this->Main_Model->style_datepicker()
				          .$this->Main_Model->style_timepicker()
			);
		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_file_upload()
				                  .$this->Main_Model->js_select2()
				                  .$this->Main_Model->js_validate()
				                  .$this->Main_Model->js_datepicker()
				                  .$this->Main_Model->js_timepicker()
			);
		$data = array(
				'nip' => $this->Main_Model->kary_cabang($this->session->userdata('cabang')),
				'penyetuju' => $penyetuju,
				'periode' => $this->Main_Model->periode_opt()
			);

		$this->load->view('akun/header', $header);
        $this->load->view('absensi/data_pulang_awal',$data);
        $this->load->view('akun/footer', $footer);
	}

	function view_pulang_awal_percabang()
	{
		$this->Main_Model->guest_login();
		$idp = $this->session->userdata('idp');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Absensi_Model->view_pulang_awal($idp, $qd_id);
        $template = $this->Main_Model->tbl_temp();
        $is_approval = $this->Main_Model->is_approval();

        $this->table->set_heading('No','NIP','Nama','Cabang','Jam Pulang','Status','Alasan','Scanlog','Action');
		$no =1;
        
        foreach ($data as $row) {
	        $link = '';
	        // if($row->flag == 1 && $row->status != 6 && $is_approval == TRUE) $link = '<li> <a href="javascript:;" onclick="approval(' . $row->id_pulang_awal . ');"> <i class="icon-edit"></i> Proses </a> </li> ';

	        ($row->file_bukti) ? $download = '<li> <a href="'.base_url('assets/pulang_awal').'/'.$row->file_bukti.'" target="_blank"> <i class="icon-delete"></i> Download File </a> </li>' : $download = '';

			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$row->cabang,
				$row->tgl.' '.jam($row->jam),
				$row->keterangan,
				$row->alasan,
				$row->scanlog,
				'<div class="btn-group dropup">
	                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                    <i class="fa fa-angle-down"></i>
	                </button>
	                    <ul class="dropdown-menu" role="menu">
	                    	'.$download.$link.'
	                    	<li>
	                            <a href="javascript:;" onclick="get_id(' . $row->id_pulang_awal . ');">
	                                <i class="icon-edit"></i> Update </a>
	                        </li>
	                        <li>
	                            <a href="javascript:;" onclick="delete_data(' . $row->id_pulang_awal . ');">
	                                <i class="icon-delete"></i> Delete </a>
	                        </li>
	                        
	                    </ul>
	            </div>'
		        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	// function proses_approval($id='', $val='')
	// {
	// 	$this->Main_Model->all_login();
	// 	$k = $this->Main_Model->view_by_id('tb_pulang_awal', array('id_pulang_awal' => $id), 'row');
	// 	$kode = isset($k->status) ? $k->status : ''; 
	// 	$value = $this->Main_Model->rule_approval($kode, 'cuti', $val);
	// 	$user = $this->session->userdata('username');
	// 	$date = date('Y-m-d H:i:s');
	// 	$data = array(
	// 		'status' => $value,
	// 		'user_update' => $user,
	// 		'update_at' => $date
	// 		);
	// 	$this->Reminder_Model->reminder_pulang_awal($value, $id);
	// 	$this->Absensi_Model->pulang_awal_process($data, $id);
	// }

	function detail_pulang_awal()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker();

		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');
		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$nip = (empty($cabang)) ? $this->Main_Model->all_kary_active() : $this->Main_Model->kary_cabang($cabang, null, 'hide_jab');
		$bulan = array(
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December'
			);

		$t = $this->db->query("SELECT DATE_FORMAT(a.`tgl`,'%Y') th FROM tb_pulang_awal a GROUP BY DATE_FORMAT(a.`tgl`,'%Y')")->result();
		$th = array();
		foreach($t as $r) {
			$th[$r->th] = $r->th;
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'nip' => $nip,
			'bulan' => $bulan,
			'th' => $th
			);
		$template = ($cabang == 0) ? 'template/header' : 'akun/header';
		$this->load->view($template, $header);
		$this->load->view('absensi/detail_pulang_awal',$data);
	}

	function view_detail_pulang_awal()
	{
		$this->Main_Model->all_login();
		$nip = $this->input->post('nip');
		$bulan = $this->input->post('bulan');
		$th = $this->input->post('th');
		$data = $this->Absensi_Model->view_detail_pulang_awal($nip, $bulan, $th);
		$template = $this->Main_Model->tbl_temp('tb_pulang_awal');
		$this->table->set_heading('No', 'Nama', 'Tanggal', 'Jam Pulang', 'Status', 'Alasan');
		$no = 1;
		foreach($data as $row) {
			switch ($row->status) {
				case '1': $status = 'Disetujui'; break;
				case '2': $status = 'Ditolak'; break;
				default: $status = 'Proses'; break;
			}

			$this->table->add_row(
				$no++,
				$row->nama,
				$this->Main_Model->format_tgl($row->tgl),
				$row->jam,
				$row->keterangan,
				$row->alasan
			);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function verifikasi_pulang_awal()
	{
		$this->Main_Model->guest_login();
		$data = $this->Absensi_Model->verifikasi_pulang_awal();
        $is_approval = $this->Main_Model->is_approval();
        $template = $this->Main_Model->tbl_temp('tb_pulang_awal');
        $array_heading = array('No', 'NIP', 'Nama', 'Jam Pulang', 'Alasan', 'Status');
        if ($is_approval == TRUE)  $array_heading[] = 'Action';
        $this->table->set_heading($array_heading);
   
        $i = 1;
        foreach ($data as $row) {
        	
			$array_data = array($i++, $row->nip, $row->nama, $this->Main_Model->format_tgl($row->tgl).' '.jam($row->jam), $row->alasan, $row->keterangan);

			if ($row->flag != 0) $action = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="approval_pulang('.$row->id_pulang_awal.');"> <i class="fa fa-check"></i> Verifikasi </a>';
			if ($is_approval == TRUE) $array_data[] = $action;
            
            $this->table->add_row($array_data);
        }
        $this->table->set_template($template);
		echo $this->table->generate();
	}

	function proses_approval($id='', $val='')
	{
		$this->Main_Model->all_login();
		$user = $this->session->userdata('username');
		$date = date('Y-m-d H:i:s');
		$k = $this->Main_Model->view_by_id('tb_pulang_awal', array('id_pulang_awal' => $id), 'row');
		$kode = isset($k->status) ? $k->status : ''; 
		$pola = isset($k->pola) ? $k->pola : '';
		$value = $this->Main_Model->rule_approval($kode, $pola, $val);
		$data = array(
			'status' => $value,
			'user_update' => $user,
			'update_at' => $date
		);
		
		$this->Absensi_Model->pulang_awal_process($data, $id);
		$this->Reminder_Model->reminder_pulang_awal($value, $id);
	}
}

/* End of file pulang_awal.php */
/* Location: ./application/controllers/pulang_awal.php */ ?>