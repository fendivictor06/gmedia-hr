<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Surat_Peringatan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Cuti_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript  	= '
			<script>
				'.$this->Main_Model->notif().'

				function reset()
				{
					$(".blank").val("");
					$(".select2").val("").trigger("change");
				}

				$("#tambah").click(function(){
					reset();
				});

				$(function(){
					$("#form_sp").validate({
						doNotHideMessage: !0,
	                    errorElement: "span",
	                    errorClass: "help-block help-block-error",
						rules : {
							nip : {required: !0 }, 
							periode : {required: !0 }, 
							keterangan : {required: !0 },
							sp : {required: !0},
							tipe : {required: !0} 
						},
						errorPlacement: function (error, element) {
						    (element.attr("type") == "file") ? error.insertAfter($(".input-group")) : error.insertAfter(element);
						},
						highlight: function(e) {
	                        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
	                    },
	                    unhighlight: function(e) {
	                        $(e).closest(".form-group").removeClass("has-error")
	                    },	
						submitHandler : function(form){
							form.submit();
						}
					});
				});

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('surat_peringatan/view_sp').'",
						type : "GET",
						data : {
							"tahun" : tahun,
							"bulan" : bulan
						},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
							$("#tambah").addClass("hidden");
							$("#proses").addClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
							$("#tambah").removeClass("hidden");
							$("#proses").removeClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal Mengambil data!");
						}
					});
				}
				load_table();

				'.$this->Main_Model->default_select2()

				 .$this->Main_Model->get_data(base_url('surat_peringatan/sp_id'),'get_id(id)','
				 	$("#myModal").modal();
				 	$("#id").val(data.id_sp);
				 	$("#tipe").val(data.tipe_sp);
				 	$("#nip").val(data.nip).trigger("change");
				 	$("#periode").val(data.qd_id).trigger("change");
				 	$("#keterangan").val(data.alasan_sp);
				 	$("#sp").val(data.sp);')

				 .$this->Main_Model->default_delete_data(base_url('surat_peringatan/delete_sp')).'

				$("#proses").click(function(){
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('surat_peringatan/penerbitan_sp').'",
						data : {
							"tahun" : tahun,
							"bulan" : bulan
						},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
							$("#tambah").addClass("hidden");
							$("#proses").addClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
							$("#tambah").removeClass("hidden");
							$("#proses").removeClass("hidden");
						},
						success : function(data){
							notif(data);
							load_table();
						}
					});
				});

				$("#nip").change(function(){
					$.ajax({
						url : "'.base_url('surat_peringatan/info_sp').'/"+$(this).val(),
						dataType : "json",
						success : function(data) {
							$("#jml_sp").html(data.sp);
							$("#akhir_sp").html(data.akhir_sp);
						}
					});
				});

				$("#tampil").click(function(){
					load_table();
				});

				function terbit(id) {
					bootbox.dialog({
			            message : "Yakin ingin menerbitkan SP ?",
			            title : "Terbit SP",
			            buttons : {
			                danger : {
			                    label : "Terbitkan",
			                    className : "red",
			                    callback : function(){
			                        $.ajax({
			                            url : "'.base_url('surat_peringatan/terbit').'/"+id,
			                            dataType : "json",
			                            success : function(data) {
			                                bootbox.alert(data.message);
			                                load_table();
			                            },
			                            error : function(jqXHR, textStatus, errorThrown) {
			                            	bootbox.alert("Gagal menerbitkan sp!");
			                            }
			                        });
			                    }
			                },
			                main : {
			                    label : "Cancel",
			                    className : "blue",
			                    callback : function(){
			                        return true;
			                    }
			                }
			            }
			        });
				}
			</script>
		';

		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','4'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_select2()
				          .$this->Main_Model->style_file_upload()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_select2()
				                  .$this->Main_Model->js_file_upload()
				                  .$this->Main_Model->js_validate()
			);

		$tipe = array(
			'KPI' => 'KPI',
			'Absensi' => 'Absensi',
			'Indisipliner' => 'Indisipliner'
			);

		$data 			= array(
				'karyawan' => $this->Main_Model->all_kary_active(),
				'periode' => $this->Main_Model->periode_opt(),
				'tipe' => $tipe 
			);

		$this->load->view('template/header', $header);
        $this->load->view('absensi/data_sp', $data);
        $this->load->view('template/footer', $footer);
	}

	function terbit($id='')
	{
		$this->Main_Model->all_login();
		$cond = array('id_sp' => $id);
		$data = $this->Main_Model->view_by_id('temp_sp', $cond, 'row');

		$nip = isset($data->nip) ? $data->nip : '';
		$awal_sp = isset($data->awal_sp) ? $data->awal_sp : '';
		$sp = isset($data->sp) ? $data->sp : '';
		$s = $this->db->query("
				SELECT * 
				FROM tb_sp 
				WHERE nip = '$nip' 
				AND 'akhir_sp' >= '$awal_sp' 
				ORDER BY awal_sp DESC")->row();

		if (!empty($s)) {
			$sp_awal = isset($s->sp) ? $s->sp : 0;
			$sp = $sp + $sp_awal;
		}

		$data_sp = array(
			'nip' => $nip,
			'qd_id' => isset($data->qd_id) ? $data->qd_id : '',
			'tgl_input' => isset($data->tgl_input) ? $data->tgl_input : '',
			'awal_sp' => $awal_sp,
			'akhir_sp' => isset($data->akhir_sp) ? $data->akhir_sp : '',
			'aktif' => isset($data->aktif) ? $data->aktif : '',
			'sp' => $sp,
			'alasan_sp' => isset($data->alasan_sp) ? $data->alasan_sp : '',
			'surat_kesanggupan' => isset($data->surat_kesanggupan) ? $data->surat_kesanggupan : '',
			'tipe_sp' => isset($data->tipe_sp) ? $data->tipe_sp : '',
			'sp_terbit' => 1,
			'idp' => isset($data->idp) ? $data->idp : ''
		);

		$id_sp = isset($s->id_sp) ? $s->id_sp : '';
		if ($id_sp != '') {
			$this->db->query("
				UPDATE tb_sp 
				SET aktif = 0 
				WHERE nip = '$nip' 
				AND id_sp != '$id_sp'");
		}

		$this->Main_Model->process_data('tb_sp', $data_sp);
		$this->Main_Model->delete_data('temp_sp', $cond);

		echo json_encode(array('status' => TRUE, 'message' => 'SP Berhasil terbit!'));
	}

	function view_sp()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
		$idp   		= $this->session->userdata('idp');
        $data 		= $this->Absensi_Model->view_sp($idp, $qd_id);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cabang','Masa Berlaku','SP','Status','Alasan','Terbit','Action');
		$no =1;
        
        foreach ($data as $row) {
        $today = date("Y-m-d");
        if($row->aktif == '1') {
        	($today <= $row->akhir_sp) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        }
        else {
        	$status = 'Tidak Aktif';
        }

        if ($row->sp_terbit == 1) { 
        	$terbit = 'Terbit';
        	$a_terbit = '';
        } else {
        	$terbit = 'Belum Terbit';
        	$a_terbit = '<li>
                            <a href="javascript:;" onclick="terbit(' . $row->id_sp . ');">
                                <i class="icon-delete"></i> Terbitkan SP </a>
                        </li>';
        }

        ($row->sp > 3) ? $status_sp = 'SP 4 / PHK' : $status_sp = 'SP '.$row->sp;

        ($row->surat_kesanggupan) ? $link = '<li> <a href="'.base_url('assets/surat_peringatan').'/'.$row->surat_kesanggupan.'" target="_blank"> <i class="icon-edit"></i> Download Surat </a> </li>' : $link = '';

        $q = $this->Main_Model->posisi($row->nip);
        $cabang = isset($q->cabang) ? $q->cabang : '';
		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$cabang,
			$this->Main_Model->format_tgl($row->akhir_sp),
			$status_sp,
			$status,
			$row->alasan_sp,
			$terbit,
			'<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                    	'.$link.$a_terbit.'
                        <li>
                            <a href="javascript:;" onclick="get_id(' . $row->id_sp . ');">
                                <i class="icon-edit"></i> Update </a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="delete_data(' . $row->id_sp . ');">
                                <i class="icon-delete"></i> Delete </a>
                        </li>
                    </ul>
            </div>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function sp_process()
	{
		$this->Main_Model->get_login();
		$this->load->library('upload');

		$id 		= $this->input->post('id');
		$nip 		= $this->input->post('nip');
		$periode 	= $this->input->post('periode');
		$keterangan	= $this->input->post('keterangan');
		$idp 		= $this->session->userdata('idp');
		$sp_input 	= $this->input->post('sp');
		$tipe 		= $this->input->post('tipe');
		$q 	    = $this->db->query("SELECT * FROM q_det WHERE qd_id = '$periode'")->row();
		$month 	= date_format(date_create($q->tgl_akhir), 'Y-m');

		$checkExists = $this->db->query("SELECT * FROM tb_sp a WHERE DATE_FORMAT(a.`awal_sp`,'%Y-%m') = '$month' AND a.`nip` = '$nip'")->row();
		// print_r(count($checkExists));exit;
		// if(count($checkExists) > 0) {
		// 	$this->session->set_flashdata('message_upload', '<script>notif("SP bulan ini sudah terbit!");</script>');
			// print_r('abc');exit;
		// } else {
			$this->session->set_flashdata('message_upload', '<script>notif("Success!");</script>');
			$file_name 	= 'Surat Kesanggupan '.$this->input->post('nip').' '.date("Y-m-d");
			$config 	= $this->Main_Model->set_upload_options("./assets/surat_peringatan/","*","5048000",$file_name);
			$this->upload->initialize($config);
				
			$tgl_sp = $month.'-06';
			$today 	= date("Y-m-d");

			$plus_6_month = date('Y-m-d', strtotime('+ 5 month',strtotime($tgl_sp)));
			$tgl_end = $this->Main_Model->last_day(date_format(date_create($plus_6_month), 'M'), date_format(date_create($plus_6_month), 'Y'));

			// if($sp_input == '')
			// {
				$check 	= $this->db->query("SELECT * FROM tb_sp WHERE nip = '$nip' AND akhir_sp > '$today' ORDER BY sp DESC")->row();
				// ($check) ? $sp = $check->sp + 1 : $sp = 1;

				if($check)
				{
					$data_old = array('aktif' => 0);
					$this->Absensi_Model->sp_process($data_old, $check->id_sp);
				}
			// }
			// else
			// {
			// 	$sp = $sp_input;
			// }

			$data 	= array(
				'nip' 				=> $nip,
				'tgl_input' 		=> date("Y-m-d"),
				'awal_sp' 			=> $tgl_sp,
				'akhir_sp' 			=> $tgl_end,
				'aktif' 			=> 1,
				'sp' 				=> $sp_input,
				'alasan_sp' 		=> $keterangan,
				'qd_id' 			=> $periode,
				'idp' 				=> $idp,
				'tipe_sp'			=> $tipe
				);

			if($this->upload->do_upload('userfile'))
			{
				$file_surat = $this->upload->data();
				$data['surat_kesanggupan'] = $file_surat['file_name'];
			}

			$this->Absensi_Model->sp_process($data, $id);
		// }
		redirect('surat_peringatan','refresh');
	}

	function sp_id($id)
	{
		$this->Main_Model->get_login();
		$data 	= $this->Absensi_Model->sp_id($id);
		echo json_encode($data);
	}

	function delete_sp()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->post('id');
		$this->Absensi_Model->delete_sp($id);
	}

	function delete_rekomendasi_sp()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$this->Main_Model->delete_data('temp_sp', array('id_sp' => $id));
	}

	function sp_nip($nip='')
	{
		$this->Main_Model->get_login();
		$javascript 	= '
			<script>
				function load_table() {
					$.ajax({
						url 	: "'.base_url('surat_peringatan/view_sp_bynip').'/'.$nip.'",
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal Mengambil data!");
						}
					});
				}
				load_table();
			</script>
		';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','69'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_select2()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_select2()
			);
		$data 			= array(
				'karyawan' 		=> $this->Main_Model->kary_nip($nip)
			);
		$this->load->view('template/header', $header);
        $this->load->view('karyawan/sp_nip', $data);
        $this->load->view('template/footer', $footer);
	}

	function view_sp_bynip($nip='')
	{
        $data 		= $this->Absensi_Model->view_sp_nip($nip);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Terbit SP','Masa Berlaku SP','SP','Status','Alasan SP', 'Terbit');
		$no =1;
        
        foreach ($data as $row) {
        $today = date("Y-m-d");
        if($row->aktif == '1') {
        	($today<=$row->akhir_sp) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        } else {
        	$status = 'Tidak Aktif';
        }

        if ($row->sp_terbit == 1) { 
        	$terbit = 'Terbit';
        } else {
        	$terbit = 'Belum Terbit';
        }

        ($row->sp > 3) ? $status_sp = 'By Rekomendasi' : $status_sp = 'SP '.$row->sp;

		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$this->Main_Model->format_tgl($row->awal_sp),
			$this->Main_Model->format_tgl($row->akhir_sp),
			$status_sp,
			$status,
			$row->alasan_sp,
			$terbit
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function view_sp_nip()
	{
		$idp   		= $this->session->userdata('idp');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data 		= $this->Absensi_Model->view_sp($idp, $qd_id);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cabang','Terbit SP','Masa Berlaku SP','SP','Status','Alasan SP');
		$no =1;
        
        foreach ($data as $row) {
        $today = date("Y-m-d");
        if($row->aktif == '1') {
        	($today<=$row->akhir_sp) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        } else {
        	$status = 'Tidak Aktif';
        }

        ($row->sp > 3) ? $status_sp = 'By Rekomendasi' : $status_sp = 'SP '.$row->sp;

		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$row->cabang,
			$this->Main_Model->format_tgl($row->awal_sp),
			$this->Main_Model->format_tgl($row->akhir_sp),
			$status_sp,
			$status,
			$row->alasan_sp
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function penerbitan_sp()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
		$periode = $this->Main_Model->id_periode($qd_id);
		$tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
		$tgl_akhir = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';

		$verif = $this->db->query("
			SELECT * 
			FROM temp_sp a 
			WHERE a.`awal_sp` > '$tgl_akhir'")->num_rows();
		// if($verif == 0) {
			$sp = 1;
			$idp = $this->session->userdata('idp');
			$bulan = isset($periode->bulan) ? $periode->bulan : '';
			$tahun = isset($periode->tahun) ? $periode->tahun : '';

			$terlambat = $this->Absensi_Model->hitung_telat_mangkir($qd_id);
			$month 	= date_format(date_create($tgl_akhir), 'Y-m');
			foreach ($terlambat as $row) {
				$jabatan = $this->Main_Model->posisi($row->nip);
				$sal_pos = isset($jabatan->sal_pos) ? $jabatan->sal_pos : '';

				if($bulan == 3 || $bulan == 6 || $bulan == 9 || $bulan == 12) {
					if($bulan == 3) {
						$condition = "((tb_teguran.`bulan` = '1' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '2' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '3' AND tb_teguran.`tahun` = '$tahun'))";
					} elseif($bulan == 6) {
						$condition = "((tb_teguran.`bulan` = '4' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '5' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '6' AND tb_teguran.`tahun` = '$tahun')) ";
					} elseif($bulan == 9) {
						$condition = "((tb_teguran.`bulan` = '7' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '8' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '9' AND tb_teguran.`tahun` = '$tahun')) ";
					} else {
						// bulan 10 / 11 / 12
						$condition = "((tb_teguran.`bulan` = '10' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '11' AND tb_teguran.`tahun` = '$tahun') OR 
			            (tb_teguran.`bulan` = '12' AND tb_teguran.`tahun` = '$tahun')) ";
					}

					$data = $this->Absensi_Model->hitung_teguran($condition, $row->nip);
					$jumlah_teguran = $data->jumlah_teguran;
				} else {
					$jumlah_teguran = 0;
				}

				$r = $this->Main_Model->view_by_id('sal_staff_class', array('sp' => 0), 'result');
				$restrict = array();
				if (!empty($r)) {
					foreach ($r as $t) {
						$restrict[] = $t->sal_pos;
					}
				} 

				if(!in_array($sal_pos, $restrict)) {
					if($row->jumlah_terlambat >= 7 || $row->jumlah_mangkir >= 3 || $jumlah_teguran >= 2) {
						$checkExists = $this->db->query("
							SELECT * 
							FROM tb_sp a 
							WHERE DATE_FORMAT(a.`awal_sp`,'%Y-%m') = '$month' 
							AND a.`nip` = '$row->nip'")->row();

						$alasan_sp = '';
						$flag = 0;
						if($row->jumlah_terlambat >= 7) {
							$alasan_sp .= 'Terlambat '.$row->jumlah_terlambat.' kali bulan '.$row->bulan;
							$flag = $flag + 1;
							$tipe_sp = 'Absensi';
						} 
						if($row->jumlah_mangkir >= 3) {
							$alasan_sp .= 'Mangkir '.$row->jumlah_mangkir.' kali bulan '.$row->bulan;
							$flag = $flag + 1;
							$tipe_sp = 'Absensi';
						} 
						if($jumlah_teguran >= 2){
							$alasan_sp .= 'Teguran '.$jumlah_teguran.' kali. ';
							$flag = $flag + 1;
							$tipe_sp = 'KPI';
						} 

						if(empty($checkExists)) {
							$tgl_sp = $month.'-06';
							$today 	= date("Y-m-d");

							$plus_6_month = date('Y-m-d', strtotime('+ 5 month',strtotime($tgl_sp)));
							$tgl_end = $this->Main_Model->last_day(date_format(date_create($plus_6_month), 'M'), date_format(date_create($plus_6_month), 'Y'));

							$check 	= $this->db->query("
								SELECT * 
								FROM tb_sp 
								WHERE nip = '$row->nip' 
								AND akhir_sp > '$today' 
								ORDER BY sp DESC")->row();

							($check) ? $sp = $check->sp + $flag : $sp = $flag;

							if($check) {
								$data_old = array('aktif' => 0);
								$this->Absensi_Model->sp_process($data_old, $check->id_sp);
							}

							$data 	= array(
								'nip' => $row->nip,
								'tgl_input' => date("Y-m-d"),
								'awal_sp' => $tgl_sp,
								'akhir_sp' => $tgl_end,
								'aktif' => 1,
								'sp' => $sp,
								'alasan_sp' => $alasan_sp,
								'qd_id' => $qd_id,
								'idp' => $idp,
								'tipe_sp' => $tipe_sp
							);

							// $this->Absensi_Model->sp_process($data, '');
							$this->Main_Model->process_data('temp_sp', $data);
						} else {
							$id_sp = $checkExists->id_sp;
							$tgl_sp = $month.'-06';
							$today 	= date("Y-m-d");

							$plus_6_month = date('Y-m-d', strtotime('+ 5 month',strtotime($tgl_sp)));
							$tgl_end = $this->Main_Model->last_day(date_format(date_create($plus_6_month), 'M'), date_format(date_create($plus_6_month), 'Y'));

							$check 	= $this->db->query("SELECT * FROM tb_sp WHERE nip = '$row->nip' AND akhir_sp > '$today' ORDER BY sp DESC")->row();
							($check) ? $sp = $check->sp + $flag : $sp = $flag;

							// if($check)
							// {
							// 	$data_old = array('aktif' => 0);
							// 	$this->Absensi_Model->sp_process($data_old, $check->id_sp);
							// }
							$data 	= array(
								'nip' => $row->nip,
								'tgl_input' => date("Y-m-d"),
								'awal_sp' => $tgl_sp,
								'akhir_sp' => $tgl_end,
								// 'aktif' => 1,
								// 'sp' => $sp,
								'alasan_sp' => $alasan_sp,
								'qd_id' => $qd_id,
								'idp' => $idp,
								'tipe_sp' => $tipe_sp
							);
							// $this->Absensi_Model->sp_process($data, $id_sp);
							$this->Main_Model->process_data('temp_sp', $data, array('id_sp' => $id_sp));
						}
					}
				}
			}

			echo 'Penerbitan SP Success!';
		// } else {
		// 	echo 'Penerbitan SP Gagal Karena Periode sudah lewat!';
		// }
	}

	function rekomendasi_sp()
	{
		$this->Main_Model->guest_login();
		$javascript = '
			<script>
				'.$this->Main_Model->notif().'
				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('surat_peringatan/view_rekomendasi_sp').'",
						type : "GET",
						data : {
							"tahun" : tahun,
							"bulan" : bulan
						},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
							$("#tambah").addClass("hidden");
							$("#proses").addClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
							$("#tambah").removeClass("hidden");
							$("#proses").removeClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal Mengambil data!");
						}
					});
				}
				load_table();

				$("#proses").click(function(){
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('surat_peringatan/penerbitan_sp').'",
						data : {
							"tahun" : tahun,
							"bulan" : bulan
						},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
							$("#tambah").addClass("hidden");
							$("#proses").addClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
							$("#tambah").removeClass("hidden");
							$("#proses").removeClass("hidden");
						},
						success : function(data){
							notif(data);
							load_table();
						}
					});
				});

				$("#nip").change(function(){
					$.ajax({
						url : "'.base_url('surat_peringatan/info_sp').'/"+$(this).val(),
						dataType : "json",
						success : function(data) {
							$("#jml_sp").html(data.sp);
							$("#akhir_sp").html(data.akhir_sp);
						}
					});
				});

				$("#tampil").click(function(){
					load_table();
				});

				function terbit(id) {
					bootbox.dialog({
			            message : "Yakin ingin menerbitkan SP ?",
			            title : "Terbit SP",
			            buttons : {
			                danger : {
			                    label : "Terbitkan",
			                    className : "red",
			                    callback : function(){
			                        $.ajax({
			                            url : "'.base_url('surat_peringatan/terbit').'/"+id,
			                            dataType : "json",
			                            success : function(data) {
			                                bootbox.alert(data.message);
			                                load_table();
			                            },
			                            error : function(jqXHR, textStatus, errorThrown) {
			                            	bootbox.alert("Gagal menerbitkan sp!");
			                            }
			                        });
			                    }
			                },
			                main : {
			                    label : "Cancel",
			                    className : "blue",
			                    callback : function(){
			                        return true;
			                    }
			                }
			            }
			        });
				}
				'.$this->Main_Model->default_select2().
				  $this->Main_Model->default_delete_data(base_url('surat_peringatan/delete_rekomendasi_sp')).'
			</script>
		';

		$header = array(
				'menu' => $this->Main_Model->menu_admin('0','0','4'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_select2()
				          .$this->Main_Model->style_file_upload()
			);
		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_select2()
				                  .$this->Main_Model->js_file_upload()
				                  .$this->Main_Model->js_validate()
			);

		$tipe = array(
			'KPI' => 'KPI',
			'Absensi' => 'Absensi',
			'Indisipliner' => 'Indisipliner'
			);

		$data = array(
				'karyawan' => $this->Main_Model->all_kary_active(),
				'periode' => $this->Main_Model->periode_opt(),
				'tipe' => $tipe 
			);

		$this->load->view('template/header', $header);
        $this->load->view('absensi/rekomendasi_sp', $data);
        $this->load->view('template/footer', $footer);
	}

	function view_rekomendasi_sp()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
		$idp = $this->session->userdata('idp');
        $data = $this->Absensi_Model->view_rekomendasi_sp($idp, $qd_id);
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','NIP','Nama','Cabang','Masa Berlaku','Rekomendasi SP','Alasan','Action');
		$no =1;
        
        foreach ($data as $row) {
        $today = date("Y-m-d");
        if($row->aktif == '1') {
        	($today <= $row->akhir_sp) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        }
        else {
        	$status = 'Tidak Aktif';
        }

        if ($row->sp_terbit == 1) { 
        	$terbit = 'Terbit';
        	$a_terbit = '';
        } else {
        	$terbit = 'Belum Terbit';
        	$a_terbit = '<li>
                            <a href="javascript:;" onclick="terbit(' . $row->id_sp . ');">
                                <i class="icon-delete"></i> Terbitkan SP </a>
                        </li>';
        }

        ($row->sp > 3) ? $status_sp = 'SP 4 / PHK' : $status_sp = 'SP '.$row->sp;

        ($row->surat_kesanggupan) ? $link = '<li> <a href="'.base_url('assets/surat_peringatan').'/'.$row->surat_kesanggupan.'" target="_blank"> <i class="icon-edit"></i> Download Surat </a> </li>' : $link = '';

        $q = $this->Main_Model->posisi($row->nip);
        $cabang = isset($q->cabang) ? $q->cabang : '';
		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$cabang,
			$this->Main_Model->format_tgl($row->akhir_sp),
			$status_sp,
			$row->alasan_sp,
			'<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                    	'.$link.$a_terbit.'
                        <li>
                            <a href="javascript:;" onclick="delete_data(' . $row->id_sp . ');">
                                <i class="icon-delete"></i> Delete </a>
                        </li>
                    </ul>
            </div>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function history()
	{
		$this->Main_Model->all_login();
		$id_cabang = $this->Main_Model->session_cabang();
		$javascript = '
			<script>
				$(document).ready(function(){
					$(".select2").select2();
				});

				function load_table()
				{
					nip = $("#nip").val();
					$.ajax({
						url 	: "'.base_url('surat_peringatan/view_sp_bynip').'/"+nip,
						beforeSend : function(){
							$("#imgload").removeClass("hidden");
							$("#tampil").addClass("hidden");
						},
						complete : function(){
							$("#imgload").addClass("hidden");
							$("#tampil").removeClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						}
					});
				}
			</script>
		';

		$menu = (empty($id_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','79');
		$karyawan = (empty($id_cabang)) ? $this->Main_Model->all_kary_active() : $this->Main_Model->kary_cabang($id_cabang, null, 'jab_hide');

		$header 		= array(
				'menu' => $menu,
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_select2()
				          .$this->Main_Model->style_file_upload()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_select2()
				                  .$this->Main_Model->js_file_upload()
				                  .$this->Main_Model->js_validate()
			);
		$data 			= array(
				'karyawan' 		=> $karyawan,
				'periode' 		=> $this->Main_Model->periode_opt()
			);

		$template_header = (empty($id_cabang)) ? 'template/header' : 'akun/header';
		$template_footer = (empty($id_cabang)) ? 'template/footer' : 'akun/footer';

		$this->load->view($template_header, $header);
        $this->load->view('absensi/history_sp', $data);
        $this->load->view($template_footer, $footer);
	}

	function info_sp($nip='')
	{
		$this->Main_Model->all_login();
		$condition = array(
			'nip' => $nip,
			'aktif' => 1
			);
		$data = $this->Main_Model->view_by_id('tb_sp', $condition, 'row');

		$result = array(
			'sp' => isset($data->sp) ? $data->sp : '-',
			'akhir_sp' => isset($data->akhir_sp) ? $this->Main_Model->format_tgl($data->akhir_sp) : '-'
			);

		echo json_encode($result);
	}
}

/* End of file surat_peringatan.php */
/* Location: ./application/controllers/surat_peringatan.php */ ?>