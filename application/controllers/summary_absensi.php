<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary_Absensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Penggajian_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript = '
			<script>
				$("#download").click(function(){
                    var tahun = $("#tahun").val();
                    var bulan = $("#bulan").val();
                    window.location.href="'.base_url('summary_absensi/download_summary').'/"+tahun+"/"+bulan;
                });
			</script>
		';
		$header = array(
				'menu' => $this->Main_Model->menu_admin('0','0','4'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
                          .$this->Main_Model->style_select2()
			);
		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				     .$this->Main_Model->js_modal()
				     .$this->Main_Model->js_bootbox()
                     .$this->Main_Model->js_select2()
			);
		$data = array(
				'periode' => $this->Main_Model->periode_opt()
			);

		$this->load->view('template/header', $header);
        $this->load->view('absensi/summary_absensi', $data);
        $this->load->view('template/footer', $footer);
	}

	function download_summary($tahun='', $bulan='')
	{
		$this->Main_Model->get_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
		$periode = $this->Main_Model->id_periode($qd_id);
        $t = isset($periode->periode) ? $periode->periode : '';
		$title = 'Rekap Absensi '.$t;
		$ambildata = $this->Absensi_Model->summary_absensi($qd_id);

		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
			$objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
			$objPHPExcel->getProperties()->setTitle("Download Rekap Absensi");
			$objPHPExcel->getProperties()->setSubject("Download Rekap Absensi");
			$objPHPExcel->getProperties()->setDescription("Download Rekap Absensi");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Absensi'); //sheet title
 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:O1');
 			$objset->setCellValue('A1','Rekap Absensi '.$t);

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:A4');
 			$objset->setCellValue('A3','NO');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B3:B4');
 			$objset->setCellValue('B3','CABANG');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C3:C4');
 			$objset->setCellValue('C3','JUMLAH KARYAWAN');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:E3');
 			$objset->setCellValue('D3','KARYAWAN TERLAMBAT');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F3:G3');
 			$objset->setCellValue('F3','KARYAWAN KENA SP');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H3:I3');
 			$objset->setCellValue('H3','KARYAWAN KENA PHK');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J3:K3');
 			$objset->setCellValue('J3','KARYAWAN IJIN SAKIT');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L3:M3');
 			$objset->setCellValue('L3','KARYAWAN IJIN KELUAR');

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('N3:O3');
 			$objset->setCellValue('N3','LEMBUR');

 			$objset->setCellValue('D4','JUMLAH');
 			$objset->setCellValue('E4','%');

 			$objset->setCellValue('F4','JUMLAH');
 			$objset->setCellValue('G4','%');

 			$objset->setCellValue('H4','JUMLAH');
 			$objset->setCellValue('I4','%');

 			$objset->setCellValue('J4','JUMLAH');
 			$objset->setCellValue('K4','%');

 			$objset->setCellValue('L4','JUMLAH');
 			$objset->setCellValue('M4','%');

 			$objset->setCellValue('N4','JUMLAH');
 			$objset->setCellValue('O4','%');

 			$styleArray = array(
			      	'borders' => array(
			          	'allborders' => array(
			              	'style' => PHPExcel_Style_Border::BORDER_THIN
			          )
			      )
			);

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O");
            // $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","TANGGAL MASUK");
             
            for ($a=0;$a<count($cols); $a++) {
                // $objset->setCellValue($cols[$a].'5', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
             	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(10);

                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris = 6;
            $i = 1;
            $sum_cabang = $sum_telat = $sum_sp = $sum_phk = $sum_sakit = $sum_ijinkeluar = $sum_lembur = 0;
            foreach ($ambildata as $frow){
            	($frow->jml_telat > 0) ? $p_telat = ($frow->jml_telat/$frow->jml_percabang) * 100 : $p_telat = '-';
            	($frow->jml_sp > 0) ? $p_sp = ($frow->jml_sp/$frow->jml_percabang) * 100 : $p_sp = '-';
            	($frow->jml_phk > 0) ? $p_phk = ($frow->jml_phk/$frow->jml_percabang) * 100 : $p_phk = '-';
            	($frow->jml_sakit > 0) ? $p_sakit = ($frow->jml_sakit/$frow->jml_percabang) * 100 :$p_sakit = '-';
            	($frow->jml_ijinkeluar > 0) ? $p_ijinkeluar = ($frow->jml_ijinkeluar/$frow->jml_percabang) * 100 : $p_ijinkeluar = '-';
            	($frow->jml_lembur > 0) ? $p_lembur = ($frow->jml_lembur/$frow->jml_percabang) * 100 : $p_lembur = '-';

            	($p_telat != '-') ? $p_telat = round($p_telat,2).'%' : $p_telat = $p_telat;
            	($p_sp != '-') ? $p_sp = round($p_sp,2).'%' : $p_sp = $p_sp;
            	($p_phk != '-') ? $p_phk = round($p_phk,2).'%' : $p_phk = $p_phk;
            	($p_sakit != '-') ? $p_sakit = round($p_sakit,2).'%' : $p_sakit = $p_sakit;
            	($p_ijinkeluar != '-') ? $p_ijinkeluar = round($p_ijinkeluar,2).'%' : $p_ijinkeluar = $p_ijinkeluar;
            	($p_lembur != '-') ? $p_lembur = round($p_lembur,2).'%' : $p_lembur = $p_lembur;
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $i++); 
                $objset->setCellValue("B".$baris, $frow->cabang); 
                $objset->setCellValue("C".$baris, $frow->jml_percabang);
                $objset->setCellValue("D".$baris, $frow->jml_telat);
                $objset->setCellValue("E".$baris, $p_telat);
                $objset->setCellValue("F".$baris, $frow->jml_sp);
                $objset->setCellValue("G".$baris, $p_sp);
                $objset->setCellValue("H".$baris, $frow->jml_phk);
                $objset->setCellValue("I".$baris, $p_phk);
                $objset->setCellValue("J".$baris, $frow->jml_sakit);
                $objset->setCellValue("K".$baris, $p_sakit);
                $objset->setCellValue("L".$baris, $frow->jml_ijinkeluar);
                $objset->setCellValue("M".$baris, $p_ijinkeluar);
                $objset->setCellValue("N".$baris, $frow->jml_lembur);
                $objset->setCellValue("O".$baris, $p_lembur);
                
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
                $sum_cabang = $sum_cabang + $frow->jml_percabang;
                $sum_telat = $sum_telat + $frow->jml_telat;
                $sum_sp = $sum_sp + $frow->jml_sp;
                $sum_phk = $sum_phk + $frow->jml_phk;
                $sum_sakit = $sum_sakit + $frow->jml_sakit;
                $sum_ijinkeluar = $sum_ijinkeluar + $frow->jml_ijinkeluar;
                $sum_lembur = $sum_lembur + $frow->jml_lembur;
            }
            $b=$baris-1;
            $c=$b+2;

            $objset->setCellValue('B'.$c,'Total');
            $objset->setCellValue('C'.$c, $sum_cabang);
            $objset->setCellValue('D'.$c, $sum_telat);
            $objset->setCellValue('F'.$c, $sum_sp);
            $objset->setCellValue('H'.$c, $sum_phk);
            $objset->setCellValue('J'.$c, $sum_sakit);
            $objset->setCellValue('L'.$c, $sum_ijinkeluar);
            $objset->setCellValue('N'.$c, $sum_lembur);

            ($sum_telat > 0) ? $pres_telat = ($sum_telat/$sum_cabang)*100 : $pres_telat = '-';
            ($sum_sp > 0) ? $pres_sp 	= ($sum_sp/$sum_cabang)*100 : $pres_sp = '-';
            ($sum_phk > 0) ? $pres_phk 	= ($sum_phk/$sum_cabang)*100 : $pres_phk = '-';
            ($sum_sakit > 0) ? $pres_sakit 	= ($sum_sakit/$sum_cabang)*100 : $pres_sakit = '-';
            ($sum_ijinkeluar > 0) ? $pres_ijinkeluar 	= ($sum_ijinkeluar/$sum_cabang)*100 : $pres_ijinkeluar = '-';
            ($sum_lembur > 0) ? $pres_lembur 	= ($sum_lembur/$sum_cabang)*100 : $pres_lembur = '-';

            ($pres_telat != '-') ? $pres_telat = round($pres_telat,2).'%' : $pres_telat = $pres_telat;
            ($pres_sp != '-') ? $pres_sp = round($pres_sp,2).'%' : $pres_sp = $pres_sp;
            ($pres_phk != '-') ? $pres_phk = round($pres_phk,2).'%' : $pres_phk = $pres_phk;
            ($pres_sakit != '-') ? $pres_sakit = round($pres_sakit,2).'%' : $pres_sakit = $pres_sakit;
            ($pres_ijinkeluar != '-') ? $pres_ijinkeluar = round($pres_ijinkeluar,2).'%' : $pres_ijinkeluar = $pres_ijinkeluar;
            ($pres_lembur != '-') ? $pres_lembur = round($pres_lembur,2).'%' : $pres_lembur = $pres_lembur;

            $objset->setCellValue('E'.$c, $pres_telat);
            $objset->setCellValue('G'.$c, $pres_sp);
            $objset->setCellValue('I'.$c, $pres_phk);
            $objset->setCellValue('K'.$c, $pres_sakit);
            $objset->setCellValue('M'.$c, $pres_ijinkeluar);
            $objset->setCellValue('O'.$c, $pres_lembur);


			$objPHPExcel->getActiveSheet()->getStyle('A3:O'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Absensi');


			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename='.$title.'.xls');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
	}

}

/* End of file summary_absensi.php */
/* Location: ./application/controllers/summary_absensi.php */ ?>