<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sto extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Sto_Model', '', TRUE);
	}

	function header()
	{
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/treant/Treant.css').'">
				<style>
					.nodeExample1 {
					    padding: 2px;
					    -webkit-border-radius: 3px;
					    -moz-border-radius: 3px;
					    border-radius: 3px;
					    background-color: #ffffff;
					    border: 1px solid #000;
					    width: 200px;
					    font-family: Tahoma;
					    font-size: 12px;
					}
					.chart { height: 100%; margin: 5px; width: 100%; }
				</style>';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
				<script src="'.base_url('assets/plugins/treant/Treant.js').'"></script>
				<script src="'.base_url('assets/plugins/treant/raphael.js').'"></script>';

		return $footer;
	}

	function datasto()
	{
		$this->Main_Model->get_login();
		$idp = $this->session->userdata('idp');
		$sto = $this->Karyawan_Model->dataposisi($idp);

		$opt_sto = array();
		foreach ($sto as $row) {
			$opt_sto[$row->id_pos] = $row->departemen.' - '.$row->cabang.' - '.$row->jab;
		}

		$data 	= array(
			'idp' => $this->Sto_Model->idp(),
			'lku' => $this->Sto_Model->lku(),
			'lka' => $this->Sto_Model->lka(),
			'sto' => $opt_sto
			);
		
		$javascript = '
			<script>
				var save_method;
				'.$this->Main_Model->notif().'
				function reset()
				{
					$(".blank").val("");
					$(".select2").val("").trigger("change");
					save_method="save";
				}

				$(document).ready(function() {
  					$(".select2").select2();
				});

				function load_table(){
					var idp = {"idp" : '.$idp.'}
					$.ajax({
						url 	: "'.base_url('sto/view_sto').'",
						type 	: "GET",
						data 	: idp,
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
								responsive: true
							});
						},
						error 	: function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Internal Server Error");
						}
					});
				}
				load_table();

				function get_id(id){
					save_method = "update";
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('sto/sto_id') . '",
			    		type : "GET",
			    		data : id,
			    		success : function(data){
			    			$("#myModal").modal();
			    			var dat = jQuery.parseJSON(data);
			    			$("#id").val(dat.id_sto);
							$("#pos").val(dat.id_pos).trigger("change");
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			alert("Internal Server Error");
			    		}
			    	});
			    }
				
			    '.$this->Main_Model->post_data('add_sto','save()','$("#form_sto").serialize()','
			    	if(data.status=="true")
			    	{
			    		load_table();
			    		reset();
			    	}
			    	notif(data.message);').'

				function delete_data(id){
			    	id = {"id":id}

			    	bootbox.dialog({
			    		message : "Yakin ingin menghapus data?",
			    		title : "Hapus Data",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('sto/delete_sto') . '",
							    		type : "POST",
							    		data : id,
							    		success : function(data){
							    			bootbox.alert({
												message: "Delete Success",
												size: "small"
											});
							    			load_table();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})

			    }
			</script>
		';
		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);

		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','3')
		);
		$this->load->view('template/header',$header);
		$this->load->view('sto/datasto',$data);
		$this->load->view('template/footer',$footer);
	}

	function view_sto()
	{
		$this->Main_Model->get_login();
		$idp = $this->input->get('idp');
		$data = $this->Sto_Model->view_sto($idp);

		$tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Id Posisi</th>
							<th>Departemen</th>
							<th>Cabang</th>
							<th>Divisi</th>
							<th>Level</th>
							<th>Jabatan</th>
							<th>Nama</th>
							<th>Action</th>
						</tr>
					</thead><tbody>';
		$i=1;
		foreach ($data as $row) {
			$q = $this->db->query("
				select * 
				from sk 
				join kary on sk.nip=kary.nip 
				where sk.id_pos_sto = '$row->id_sto' 
				and sk.aktif = '1'")->row();
			
			$nip 	= isset($q->nama)?$q->nama:'-';
			$aktif 	= isset($q->aktif)?$q->aktif:'';
			($aktif=='1') ? $nip = $nip : $nip = '-';
		$tbl .= '<tr>
					<td>'.$i++.'</td>
					<td>'.$row->id_stob_pos.'</td>
					<td>'.$row->departemen.'</td>
					<td>'.$row->cabang.'</td>
					<td>'.$row->divisi.'</td>
					<td>'.$row->sal_pos.'</td>
					<td>'.$row->jab.'</td>
					<td>'.$nip.'</td>
					<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu">
				                    <li>
				                    	<a href="javascript:;" onclick="get_id('.$row->id_sto.')">
				                    		Update STO</a>
				                    </li>
				                    <li>
				                    	<a href="javascript:;" onclick="delete_data('.$row->id_sto.')">
				                    		Delete STO</a>
				                    </li>
				                </ul>
				        </div>
					</td>';
		}
		$tbl .= '</tbody></table>';

		echo $tbl;
	}

	function sto_id()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->get('id');
		$data 	= $this->Sto_Model->sto_id($id);
		echo json_encode($data);
	}

	function view_posisi()
	{
		$this->Main_Model->get_login();
		$idp 	= $this->input->get('idp');
		$select = $this->input->get('select');

		$option = $this->Karyawan_Model->dataposisi($idp);
		$result = '<select class="form-control" id="pos" name="pos">';
		foreach ($option as $row) {
		$result.= '<option ';
		if($row->id_pos==$select)
		{
			$result.='selected ';
		}
			$result.= 'value="'.$row->id_pos.'">'.$row->cabang.'-'.$row->jab.'-'.$row->divisi.'</option>'; 
		}
		$result.= '</select>';

		echo $result;
	}

	function add_sto(){
		$this->Main_Model->get_login();
		$id 		= $this->input->post('id');
		$pos 		= $this->input->post('pos');
		$id_stob_pos= $this->db->query("SELECT b.`kode_pin`,b.`id_lku` FROM pos a JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` WHERE a.`id_pos`='$pos'")->row();

		if($pos == "")
		{
			$result = array('status'=>'false','message'=>'Form masih ada yang kosong!');
		}
		else
		{
			$q 		= $this->db->query("SELECT * FROM sk a JOIN pos_sto	b ON a.`id_pos_sto`=b.`id_sto` WHERE a.`aktif`='1' AND b.`id_pos` = '$pos'")->row();
			$data 	= array(
				'id_pos' => $pos,
				'id_lku' => $id_stob_pos->id_lku
				);

			if(empty($q))
			{
				$data['id_stob_pos'] = $id_stob_pos->kode_pin;
			}

			if($id)
			{
				$this->Sto_Model->update_sto($data,$id);
			}
			else
			{
				$this->Sto_Model->add_sto($data);
			}
			
			$result = array('status'=>'true','message'=>'Success!');
		}
		echo json_encode($result);
	}

	function update_sto(){
		$this->Main_Model->get_login();
		$id_sto = $this->input->post('id_sto');
		$data = array(
			'id_pos' => $this->input->post('id_pos'),
			'id_stob_pos' => $this->input->post('id_stob_pos'),
			'id_lku' => $this->input->post('id_lku'),
			'id_lka' => $this->input->post('id_lka')
			);
		$this->Sto_Model->update_sto($data,$id_sto);
	}

	function editsto(){
		$this->Main_Model->get_login();
		$data = array(
			'idp' => $this->Sto_Model->idp(),
			'lka' => $this->Sto_Model->lka()
			);
		$javascript = '
			<script type="text/javascript">
			    $("#dataTables-example").DataTable({
			        responsive: true
			    });
			</script>';
		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$this->load->view('template/header',$this->header());
		$this->load->view('sto/editsto',$data);
		$this->load->view('template/footer',$footer);
	}
	function delete_sto(){
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$this->Sto_Model->delete_sto($id);
		// $data 	= array('status'=>0);
		// $this->Sto_Model->update_sto($data,$id);
	}

	function struktur(){
		$this->Main_Model->get_login();
		
		$javascript = '
			<script type="text/javascript">
				function save(){
					$.ajax({
						url  		: "'.base_url('sto/add_struktur').'",
						data 		: $("#form_sto").serialize(),
						type 		: "POST",
						success 	: function(data){
							location.reload();
						}	
					})
				}
				function delete_data(id){
			    	// id = {"id":$("#del_sto").val()}
			    	id = {"id":id}

			    	bootbox.dialog({
			    		message : "Yakin ingin menghapus data?",
			    		title : "Hapus Data",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('sto/delete_struktur') . '",
							    		type : "POST",
							    		data : id,
							    		success : function(data){
							    			bootbox.alert({
												message: "Delete Success",
												size: "small"
											});
							    			location.reload();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})

			    }
			</script>';
		
		$data = array(
			'sto' 			=> $this->Sto_Model->sto_opt(),
			'sto_rel' 		=> $this->Sto_Model->sto_rel(),
			'sto_rel_opt' 	=> $this->Sto_Model->sto_rel_opt(),
			'sto_rel_tbl' 	=> $this->Sto_Model->sto_rel_tbl(),
			'javascript' 	=> $javascript,
			'js' 			=> $this->footer()
			);
		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','3')
		);
		$this->load->view('template/header',$header);
		$this->load->view('sto/struktur',$data);
		// $this->load->view('template/footer',$footer);
	}

	function add_struktur(){
		$this->Main_Model->get_login();
		$id_parent = $this->input->post("parent");

		// if($id_parent=='')
		// {
		// 	$id_parent = '0';
		// }
		
		$data = array(
			'id_sto'	=> $this->input->post("sto"),
			'id_parent'	=> $id_parent
			);
		$this->Sto_Model->add_struktur($data);
	}

	function delete_struktur(){
		$this->Main_Model->get_login();
		$id = $this->input->post('id');

		$this->Sto_Model->delete_struktur($id);
	}

	
}