<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Shift extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Shift_Model', '', true);
    }

    function index()
    {
        show_404();
    }

    function data_shift()
    {
        $this->Main_Model->all_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker();
        $acctype = $this->session->userdata('acctype');
        $menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0', '0', '3') : $this->Main_Model->menu_user('0', '0', '130');
        $header = array(
            'menu' => $menu,
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
            );

        // $body = array(
        //  'h1' => 'Data Shift'
        //  );

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page(),
            'acctype' => $acctype
            );

        $this->load->view('template/header', $header);
        // $this->load->view('template/body', $body);
        $this->load->view('absensi/form_data_shift', $data);
    }

    function view_data_shift()
    {
        $this->Main_Model->all_login();
        $data = $this->Absensi_Model->view_ms_shift();
        $template = $this->Main_Model->tbl_temp();
        $acctype = $this->session->userdata('acctype');

        $arr_heading = array('No', 'Shift', 'Jam Masuk', 'Jam Pulang', 'Keterangan', 'Default Shift');
        if ($acctype == 'Administrator') {
            $arr_heading[] = 'Action';
        }
        $this->table->set_heading($arr_heading);

        $no = 1;
        foreach ($data as $row) {
            ($row->default_shift == 1) ? $default = 'Ya' : $default = 'Tidak';

            $arr_data = array($no++, $row->nama, $row->jam_masuk, $row->jam_pulang, $row->keterangan, $default);
            if ($acctype == 'Administrator') {
                $arr_data[] =
                '<div class="btn-group">
	                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                    <i class="fa fa-angle-down"></i>
	                </button>
	                    <ul class="dropdown-menu" role="menu">
	                        <li>
	                            <a href="javascript:;" onclick="get_id(' . $row->id_shift . ');">
	                                <i class="icon-edit"></i> Update </a>
	                        </li>
	                        <li>
	                            <a href="javascript:;" onclick="delete_data(' . $row->id_shift . ');">
	                                <i class="icon-delete"></i> Delete </a>
	                        </li>
	                        <li>
	                            <a href="javascript:;" onclick="set_default(' . $row->id_shift . ');">
	                                <i class="icon-delete"></i> Set Default Shift </a>
	                        </li>
	                    </ul>
	            </div>';
            }

            $this->table->add_row($arr_data);
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function default_shift($id = '')
    {
        $this->Main_Model->get_login();
        $q = $this->Absensi_Model->set_default_shift($id);
        ($q == true) ? $result = array('status' => true , 'message' => 'Success!') : $result = array('status' => false, 'message' => 'Failed');
        echo json_encode($result);
    }

    function shift_process()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $shift = $this->input->post('shift');
        $jam_masuk = $this->input->post('jam_masuk');
        $jam_pulang = $this->input->post('jam_pulang');
        $keterangan = $this->input->post('keterangan');

        if ($shift == '' || $jam_masuk == '' || $jam_pulang == '') {
            $result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
        } else {
            // if(strtotime($jam_pulang) < strtotime($jam_masuk)) {
            //  $result = array('status' => false, 'message' => 'Format jam salah!');
            // } else {

                $data = array(
                    'nama' => $shift,
                    'jam_masuk' => $jam_masuk,
                    'jam_pulang' => $jam_pulang,
                    'keterangan' => $keterangan
                    );
                ($id) ? $this->Main_Model->process_data('ms_shift', $data, array('id_shift' => $id)) :  $this->Main_Model->process_data('ms_shift', $data);
                $result = array('status' => true, 'message' => 'Success!');
            // }
        }
        echo json_encode($result);
    }

    function id_shift($id = '')
    {
        $this->Main_Model->get_login();
        $data = $this->Absensi_Model->view_ms_shift($id);
        echo json_encode($data);
    }

    function delete_shift()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $data = array('status' => 0);
        $this->Main_Model->process_data('ms_shift', $data, array('id_shift' => $id));
    }

    function penjadwalan()
    {
        $this->Main_Model->all_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_datepicker();
        $acctype = $this->session->userdata('acctype');
        $menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0', '0', '3') : $this->Main_Model->menu_user('0', '0', '130');
        $header = array(
            'menu' => $menu,
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
                      .$this->Main_Model->style_datepicker()
            );

        $nip = $this->Main_Model->kary_active();
        if ($nip) {
            foreach ($nip as $row) {
                $nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
            }
        } else {
            $nip_opt = array();
        }

        if ($acctype == 'Operator') {
            $nip_opt = $this->Main_Model->kary_cabang(null, null, '-');
        }

        $shift = $this->Absensi_Model->view_ms_shift();
        if ($shift) {
            foreach ($shift as $row) {
                $shift_opt[$row->id_shift] = $row->nama.' '.$row->masuk.' s/d '.$row->pulang;
            }
        } else {
            $shift_opt = array();
        }

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page(),
            'nip' => $nip_opt,
            'shift' => $shift_opt
            );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/penjadwalan', $data);
    }

    function view_penjadwalan()
    {
        $this->Main_Model->all_login();
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_selesai = $this->input->post('tgl_selesai');
        $copy_to = $this->input->post('copy_to');
        $nip_copy = $this->input->post('nip_copy');

        $tgl_awal = $this->Main_Model->convert_tgl($tgl_awal);
        $tgl_selesai = $this->Main_Model->convert_tgl($tgl_selesai);

        $date_range = range_to_date($tgl_awal, $tgl_selesai);

        $arr_heading = array('#', 'NIP', 'Nama');
        if (!empty($date_range)) {
            for ($i = 0; $i < count($date_range); $i++) {
                $arr_heading[] = tanggal($date_range[$i]);
            }
        }

        $data = $this->Shift_Model->karyawan($copy_to, $nip_copy, $tgl_awal, $tgl_selesai);

        $template = $this->Main_Model->tbl_temp();
        $this->table->set_heading($arr_heading);
        $no = 1;

        if (!empty($data)) {
            $arr_data = array();
            for ($i = 1; $i <= count($data); $i++) {
                $arr_data = array(
                    $no++,
                    $data[$i]['nip'],
                    $data[$i]['nama']
                );

                $button = '';
                for ($a = 0; $a < count($date_range); $a++) {
                    $button = '';
                    $id = isset($data[$i][$date_range[$a]]['id']) ? $data[$i][$date_range[$a]]['id'] : '';
                    $shift = isset($data[$i][$date_range[$a]]['shift']) ? $data[$i][$date_range[$a]]['shift'] : '';
                    $jam_masuk = isset($data[$i][$date_range[$a]]['jam_masuk']) ? $data[$i][$date_range[$a]]['jam_masuk'] : '';
                    $jam_pulang = isset($data[$i][$date_range[$a]]['jam_pulang']) ? $data[$i][$date_range[$a]]['jam_pulang'] : '';

                    // $id = $data[$i][$date_range[$a]]['id'] ?? '';
                    
                    if ($id != '') {
                        $button = '<center>'.$shift.'<br>'.jam($jam_masuk).' : '.jam($jam_pulang).'<br>'.'<a href="javascript:;" title="Update" onclick="get_id('.$id.');"><i class="fa fa-pencil fa-lg"></i></a>&nbsp;&nbsp;
						<a href="javascript:;" title="Delete" onclick="delete_data('.$id.');"><i class="fa fa-trash fa-lg"></i></a>&nbsp;&nbsp;&nbsp;
						<a href="javascript:;" title="Tukar" onclick="tukar('.$id.');"><i class="fa fa-refresh fa-lg"></i></a>'.'</center>';
                    }
                    $arr_data[] = $button;
                }
                
                $this->table->add_row($arr_data);
            }
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function jadwal_process()
    {
        $this->Main_Model->all_login();
        $id = $this->input->post('id');
        $nip = $this->input->post('nip');
        $shift = $this->input->post('shift');
        $tgl = ($this->input->post('tanggal')) ? $this->Main_Model->convert_tgl($this->input->post('tanggal')) : '';
        // $tgl_selesai = ($this->input->post('tgl_selesai')) ? $this->Main_Model->convert_tgl($this->input->post('tgl_selesai')) : '';
        $keterangan = $this->input->post('keterangan');
        $default_shift = $this->db->query("select id_shift from ms_shift where default_shift = 1")->row();

        if ($nip == '' || $shift == '' || $tgl == '') {
            $result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
        } else {
            // if(strtotime($tgl_mulai) > strtotime($tgl_selesai)) {
            //  $result = array('status' => false, 'message' => 'Format tanggal salah!');
            // } else {
                // while (strtotime($tgl_mulai) <= strtotime($tgl_selesai)) {
                //  $tanggal[] = $tgl_mulai;
                //  $tgl_mulai = date("Y-m-d", strtotime("+1 day", strtotime($tgl_mulai)));
                // }
                // if($id) {
                //  ($shift == $default_shift->id_shift) ? $id_template = 0 : $id_template = $id;
                //  for($i = 0; $i < count($tanggal); $i++) {
                //      $data = array(
                //          'id_shift' => $shift,
                //          'tgl' => $tanggal[$i],
                //          'nip' => $nip,
                //          'keterangan' => $keterangan,
                //          'template' => $id_template
                //          );
                //      $this->Main_Model->delete_data('tr_shift', array('tgl' => $tanggal[$i] , 'nip' => $nip, 'template' => $id));

                //      $this->Main_Model->process_data('tr_shift',$data);
                //  }
                //  $result = array('status' => true, 'message' => 'Success!');

                // } else {
                //  $maks = $this->db->query("SELECT COALESCE(MAX(DISTINCT(a.`template`)),0) maks_value FROM tr_shift a")->row();
                //  $template = $maks->maks_value + 1;

                //  for($i = 0; $i < count($tanggal); $i++) {
                //      $data = array(
                //          'id_shift' => $shift,
                //          'tgl' => $tanggal[$i],
                //          'nip' => $nip,
                //          'keterangan' => $keterangan,
                //          'template' => $template
                //          );

                //      $this->Main_Model->delete_data('tr_shift', array('tgl' => $tanggal[$i] , 'nip' => $nip, 'template' => 0));

                //      $this->Main_Model->process_data('tr_shift',$data);
                //  }

                //  $karyawan = $this->db->query("select pin, nip from kary where (isnull(tgl_resign) or (tgl_resign >= '$tgl_mulai' and tgl_resign <= '$tgl_selesai')) and kary.pin != '' and nip != '$nip'")->result();

                //  foreach($karyawan as $kary){
                //      for($a=0; $a < count($tanggal); $a++){

                //          $data = array(
                //              'id_shift' => $default_shift->id_shift,
                //              'tgl' => $tanggal[$a],
                //              'nip' => $kary->nip,
                //              'keterangan' => '',
                //              'template' => 0
                //              );

                //          $check = $this->db->where(array('nip' => $kary->nip, 'tgl' => $tanggal[$a]))->get('tr_shift')->row();

                //          if(empty($check)){
                //              $this->Main_Model->delete_data('tr_shift', array('tgl' => $tanggal[$a] , 'nip' => $kary->nip));

                //              $this->Main_Model->process_data('tr_shift',$data);
                //          }
                //      }
                //  }

                //  $result = array('status' => true, 'message' => 'Success!');
                // }

                $data = array(
                    'id_shift' => $shift,
                    'tgl' => $tgl,
                    'nip' => $nip,
                    'keterangan' => $keterangan,
                    'template' => 0
                    );

                // $condition = ($id) ? array('id' => $id) : array();
                $this->Main_Model->delete_data('tr_shift', array('nip' => $nip, 'tgl' => $tgl));
                $this->Main_Model->process_data('tr_shift', $data);
                $this->Main_Model->generate_lembur($nip, $tgl, $shift);
                $result = array('status' => true, 'message' => 'Success!');
            // }
        }
        echo json_encode($result);
    }

    function id_jadwal($id = '')
    {
        $this->Main_Model->all_login();
        $data = $this->Absensi_Model->view_penjadwalan(null, $id);
        echo json_encode($data);
    }

    function delete_jadwal($id = '')
    {
        $this->Main_Model->all_login();
        // $default_shift = $this->db->query("select id_shift from ms_shift where default_shift = 1")->row();
        // $this->Main_Model->process_data('tr_shift', array('id_shift' => $default_shift->id_shift, 'template' => 0), array('template' => $id));

        $this->Main_Model->delete_data('tr_shift', array('id' => $id));

        echo json_encode(array('status' => true, 'message' => 'Success'));
    }

    function copy_jadwal()
    {
        $this->Main_Model->all_login();
        $id_copy = $this->input->post("id_copy");
        $copy_to = $this->input->post("copy_to");
        $nip_copy = $this->input->post("nip_copy");
        $tgl_awal = $this->input->post('tgl_awal');
        $tgl_akhir = $this->input->post('tgl_akhir');
        $acctype = $this->session->userdata('acctype');
        // $keterangan_copy = $this->input->post("keterangan_copy");

        if ($id_copy == '' || $copy_to == '' || $tgl_awal == '' || $tgl_akhir == '') {
            $result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
        } else {
            if ($copy_to == 2 && $nip_copy == '') {
                $result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
            } else {
                $date_start = $this->Main_Model->convert_tgl($tgl_awal);
                $date_end = $this->Main_Model->convert_tgl($tgl_akhir);
                if (strtotime($date_end) < strtotime($date_start)) {
                    $result = array('status' => false, 'message' => 'Invalid date format!');
                } else {
                    $tanggal = array();
                    while (strtotime($date_start) <= strtotime($date_end)) {
                        $tanggal[] = $date_start;
                        $date_start = date("Y-m-d", strtotime("+1 day", strtotime($date_start)));
                    }

                    // $q = $this->Main_Model->view_by_id('tb_template_shift', array('id' => $id_copy), 'row');
                    $q = $this->Absensi_Model->view_template($id_copy);
                    // print_r($q);exit;
                    if ($copy_to == 1) {
                        if ($acctype == 'Administrator') {
                            $karyawan = $this->db->query("select pin, nip from kary where isnull(tgl_resign) and kary.pin != ''")->result();
                        } else {
                            $karyawan = $this->Main_Model->kary_cabang(null, 'q');
                        }
                        foreach ($karyawan as $row) {
                            for ($i = 0; $i < count($tanggal); $i++) {
                                $weekday = date('w', strtotime($tanggal[$i]));
                                $weekday = $this->weekday($weekday);
                                $id_shift = $q->$weekday;
                                $data = array(
                                    'id_shift' => $id_shift,
                                    'tgl' => $tanggal[$i],
                                    'nip' => $row->nip,
                                    'keterangan' => $q->description,
                                    'template' => 0
                                );

                                # cek hari libur
                                $libur = $this->Main_Model->view_by_id('hol', ['hol_tgl' => $tanggal[$i]], 'row');
                                if (!empty($libur)) {
                                    $data['id_shift'] = '7';
                                    $data['keterangan'] = isset($libur->hol_desc) ? $libur->hol_desc : '';
                                }

                                $this->Main_Model->delete_data('tr_shift', array('tgl' => $tanggal[$i] , 'nip' => $row->nip));
                                $this->Main_Model->process_data('tr_shift', $data);
                            }
                        }
                    } else {
                        for ($i = 0; $i < count($nip_copy); $i++) {
                            for ($a = 0; $a < count($tanggal); $a++) {
                                $weekday = date('w', strtotime($tanggal[$a]));
                                $weekday = $this->weekday($weekday);
                                $id_shift = $q->$weekday;
                                $data = array(
                                    'id_shift' => $id_shift,
                                    'tgl' => $tanggal[$a],
                                    'nip' => $nip_copy[$i],
                                    'keterangan' => $q->description,
                                    'template' => 0
                                );

                                # cek hari libur
                                $libur = $this->Main_Model->view_by_id('hol', ['hol_tgl' => $tanggal[$a]], 'row');
                                if (!empty($libur)) {
                                    $data['id_shift'] = '7';
                                    $data['keterangan'] = isset($libur->hol_desc) ? $libur->hol_desc : '';
                                }

                                $this->Main_Model->delete_data('tr_shift', array('tgl' => $tanggal[$a] , 'nip' => $nip_copy[$i]));
                                $this->Main_Model->process_data('tr_shift', $data);
                            }
                        }
                    }
                
                    $result = array('status' => true, 'message' => 'Success!');
                }
            }
        }
        echo json_encode($result);
    }

    function weekday($id = '')
    {
        switch ($id) {
            case '0':
                $hari = 'minggu';
                break;
            case '1':
                $hari = 'senin';
                break;
            case '2':
                $hari = 'selasa';
                break;
            case '3':
                $hari = 'rabu';
                break;
            case '4':
                $hari = 'kamis';
                break;
            case '5':
                $hari = 'jumat';
                break;
            case '6':
                $hari = 'sabtu';
                break;
            
            default:
                $hari = '';
                break;
        }
        return $hari;
    }

    function template()
    {
        $this->Main_Model->all_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_bs_select()
              .$this->Main_Model->js_datepicker();

        $acctype = $this->session->userdata('acctype');
        $menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0', '0', '3') : $this->Main_Model->menu_user('0', '0', '130');

        $header = array(
            'menu' => $menu,
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
                      .$this->Main_Model->style_bs_select()
                      .$this->Main_Model->style_datepicker()
            );

        $nip = $this->Main_Model->kary_active();
        $nip_opt = array();
        if ($nip) {
            foreach ($nip as $row) {
                $nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
            }
        }

        if ($acctype == 'Operator') {
            $nip_opt = $this->Main_Model->kary_cabang(null, null, '-');
        }

        $shift = $this->Absensi_Model->view_ms_shift();
        $shift_opt = array();
        $shift_opt_minggu = array('0' => 'Hari Minggu');
        if ($shift) {
            foreach ($shift as $row) {
                $shift_opt[$row->id_shift] = $row->nama.' '.$row->masuk.' s/d '.$row->pulang;
                $shift_opt_minggu[$row->id_shift] = $row->nama.' '.$row->masuk.' s/d '.$row->pulang;
            }
        }

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page(),
            'nip' => $nip_opt,
            'shift' => $shift_opt,
            'shift_minggu' => $shift_opt_minggu,
            'acctype' => $acctype
            );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/template_shift', $data);
    }

    function view_template()
    {
        $this->Main_Model->all_login();
        $template = $this->Main_Model->tbl_temp();
        $data = $this->Absensi_Model->view_template();
        $acctype = $this->session->userdata('acctype');
        $this->table->set_heading('No', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu', 'Minggu', 'Keterangan', 'Action');
        $i = 1;
        foreach ($data as $row) {
            $edit = array('event' => "get_id('".$row->id."')", 'label' => 'Update');
            $delete = array('event' => "delete_data('".$row->id."')", 'label' => 'Delete');
            $salin = array('event' => "salin('".$row->id."')", 'label' => 'Apply to');

            $action = array($salin);
            if ($acctype == 'Administrator') {
                $action[] = $edit;
                $action[] = $delete;
            }

            $this->table->add_row(
                $i++,
                $row->senin,
                $row->selasa,
                $row->rabu,
                $row->kamis,
                $row->jumat,
                $row->sabtu,
                $row->minggu,
                $row->description,
                $this->Main_Model->action($action)
            );
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function process_template()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $senin = $this->input->post('senin');
        $selasa = $this->input->post('selasa');
        $rabu = $this->input->post('rabu');
        $kamis = $this->input->post('kamis');
        $jumat = $this->input->post('jumat');
        $sabtu = $this->input->post('sabtu');
        $minggu = $this->input->post('minggu');
        $description = $this->input->post('keterangan');

        $data = array(
            '0' => $minggu,
            '1' => $senin,
            '2' => $selasa,
            '3' => $rabu,
            '4' => $kamis,
            '5' => $jumat,
            '6' => $sabtu,
            'description' => $description
            );

        $condition = ($id) ? array('id' => $id) : array();
        $this->Main_Model->process_data('tb_template_shift', $data, $condition);

        $result = array('status' => true, 'message' => 'Success!');
        echo json_encode($result);
    }

    function id_template($id = '')
    {
        $this->Main_Model->get_login();
        $data = $this->Absensi_Model->view_template($id);
        echo json_encode($data);
    }

    function delete_template($id = '')
    {
        $this->Main_Model->get_login();
        $this->Main_Model->delete_data('tb_template_shift', array('id' => $id));

        echo json_encode(array('status' => true, 'message' => 'Success!'));
    }

    function tukar_jadwal()
    {
        $this->Main_Model->all_login();
        $id_tukar = $this->input->post('id_tukar');
        $query = $this->Main_Model->view_by_id('tr_shift', array('id' => $id_tukar), 'row');
        // $nip_tukar = $this->input->post('nip_tukar');
        $nip_tukar = isset($query->nip) ? $query->nip : '';
        $tanggal_tukar_awal = $this->input->post('tanggal_tukar_awal');
        // $tanggal_tukar_akhir = $this->input->post('tanggal_tukar_akhir');
        
        $nip_ditukar = $this->input->post('nip_ditukar');
        $tanggal_ditukar_awal = $this->input->post('tanggal_ditukar_awal');
        // $tanggal_ditukar_akhir = $this->input->post('tanggal_ditukar_akhir');
        $keterangan_tukar = $this->input->post('keterangan_tukar');

        $tanggal_tukar_akhir = $tanggal_tukar_awal;
        $tanggal_ditukar_akhir = $tanggal_ditukar_awal;

        if ($nip_tukar == '' || $tanggal_tukar_awal == '' || $nip_ditukar == '' || $tanggal_ditukar_awal == '') {
            $status = false;
            $message = '';
            if ($nip_tukar == '') {
                $message .= 'Masukkan karyawan yang akan menukar jadwal <br>';
            }
            if ($tanggal_tukar_awal == '') {
                $message .= 'Masukkan tanggal awal tukar <br>';
            }
            if ($tanggal_tukar_akhir == '') {
                $message .= 'Masukkan tanggal akhir tukar <br>';
            }
            if ($nip_ditukar == '') {
                $message .= 'Masukkan karyawan yang akan ditukar jadwalnya <br>';
            }
            if ($tanggal_ditukar_awal == '') {
                $message .= 'Masukkan tanggal awal karyawan yang ditukar <br>';
            }
            if ($tanggal_ditukar_akhir == '') {
                $message .= 'Masukkan tanggal akhir karyawan yang ditukar <br>';
            }
        } else {
            $tanggal_tukar_awal = $this->Main_Model->convert_tgl($tanggal_tukar_awal);
            $tanggal_tukar_akhir = $this->Main_Model->convert_tgl($tanggal_tukar_akhir);

            $tanggal_ditukar_awal = $this->Main_Model->convert_tgl($tanggal_ditukar_awal);
            $tanggal_ditukar_akhir = $this->Main_Model->convert_tgl($tanggal_ditukar_akhir);

            if ((strtotime($tanggal_tukar_akhir) < strtotime($tanggal_tukar_awal)) || (strtotime($tanggal_ditukar_akhir) < strtotime($tanggal_ditukar_awal))) {
                $status = false;
                $message = 'Invalid date format';
            } else {
                $tanggal_tukar = array();
                while (strtotime($tanggal_tukar_awal) <= strtotime($tanggal_tukar_akhir)) {
                    $tanggal_tukar[] = $tanggal_tukar_awal;
                    $tanggal_tukar_awal = date("Y-m-d", strtotime("+1 day", strtotime($tanggal_tukar_awal)));
                }

                $tanggal_ditukar = array();
                while (strtotime($tanggal_ditukar_awal) <= strtotime($tanggal_ditukar_akhir)) {
                    $tanggal_ditukar[] = $tanggal_ditukar_awal;
                    $tanggal_ditukar_awal = date("Y-m-d", strtotime("+1 day", strtotime($tanggal_ditukar_awal)));
                }

                if (count($tanggal_tukar) != count($tanggal_ditukar)) {
                    $status = false;
                    $message = 'Pastikan jumlah tanggal yang ditukar sama';
                } else {
                    $username = $this->session->userdata('username');
                    
                    if (! empty($tanggal_tukar)) {
                        for ($i = 0; $i < count($tanggal_tukar); $i++) {
                            $tukar_condition = array(
                                'nip' => $nip_tukar,
                                'tgl' => $tanggal_tukar[$i]
                            );

                            $penukar = $this->Main_Model->view_by_id('tr_shift', $tukar_condition, 'row');
                            $id_1 = isset($penukar->id) ? $penukar->id : '';
                            $id_shift_1 = isset($penukar->id_shift) ? $penukar->id_shift : '';

                            $arr = array(
                                'id_shift' => $id_shift_1,
                                'tgl' => $tanggal_ditukar[$i],
                                'nip' => $nip_ditukar,
                                'keterangan' => $keterangan_tukar,
                                'template' => 0,
                                'user_insert' => $username
                            );

                            $this->Main_Model->process_data('temp_shift', $arr);
                            $this->Main_Model->delete_data('tr_shift', $tukar_condition);
                            $this->Main_Model->delete_data('lembur', $tukar_condition);
                        }
                    }

                    if (! empty($tanggal_ditukar)) {
                        for ($i = 0; $i < count($tanggal_ditukar); $i++) {
                            $ditukar_condition = array(
                                'nip' => $nip_ditukar,
                                'tgl' => $tanggal_ditukar[$i]
                            );

                            $tertukar = $this->Main_Model->view_by_id('tr_shift', $ditukar_condition, 'row');
                            $id_2 = isset($tertukar->id) ? $tertukar->id : '';
                            $id_shift_2 = isset($tertukar->id_shift) ? $tertukar->id_shift : '';

                            $arr = array(
                                'id_shift' => $id_shift_2,
                                'tgl' => $tanggal_tukar[$i],
                                'nip' => $nip_tukar,
                                'keterangan' => $keterangan_tukar,
                                'template' => 0,
                                'user_insert' => $username
                            );

                            $this->Main_Model->process_data('temp_shift', $arr);
                            $this->Main_Model->delete_data('tr_shift', $ditukar_condition);
                            $this->Main_Model->delete_data('lembur', $ditukar_condition);
                        }
                    }

                    $data = $this->Main_Model->view_by_id('temp_shift', array('user_insert' => $username), 'result');
                    if (! empty($data)) {
                        foreach ($data as $row) {
                            $insert = array(
                                'id_shift' => $row->id_shift,
                                'tgl' => $row->tgl,
                                'nip' => $row->nip,
                                'keterangan' => $row->keterangan,
                                'template' => $row->template,
                                'user_insert' => $row->user_insert
                            );

                            $this->Main_Model->process_data('tr_shift', $insert);
                            $this->Main_Model->generate_lembur($row->nip, $row->tgl, $row->id_shift);
                        }

                        $this->Main_Model->delete_data('temp_shift', array('user_insert' => $username));
                    }

                    $status = true;
                    $message = 'Tukar data berhasil';
                }
            }
        }

        echo json_encode(array('status' => $status, 'message' => $message));
    }

    function upload_jadwal()
    {
        $this->Main_Model->all_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_datepicker()
              .$this->Main_Model->js_fileinput();
        $acctype = $this->session->userdata('acctype');
        $menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0', '0', '3') : $this->Main_Model->menu_user('0', '0', '130');
        $header = array(
            'menu' => $menu,
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
                      .$this->Main_Model->style_datepicker()
                      .$this->Main_Model->style_fileinput()
            );

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page()
            );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/upload_jadwal', $data);
    }

    function jadwal_ts()
    {
        $this->Main_Model->all_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_datepicker()
              .$this->Main_Model->js_fileinput();
        $acctype = $this->session->userdata('acctype');
        $menu = ($acctype == 'Administrator') ? $this->Main_Model->menu_admin('0', '0', '3') : $this->Main_Model->menu_user('0', '0', '130');
        $header = array(
            'menu' => $menu,
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
                      .$this->Main_Model->style_datepicker()
                      .$this->Main_Model->style_fileinput()
            );

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page()
            );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/jadwal_ts', $data);
    }

    function view_jadwal_ts()
    {
        $this->Main_Model->all_login();
        $bulan = $this->input->get('bulan');
        $tahun = $this->input->get('tahun');

        if (strlen($bulan) == 1) {
            $bulan = '0'.$bulan;
        }

        $tgl_awal = $tahun.'-'.$bulan.'-01';
        $last_day = $this->Main_Model->last_day($bulan, $tahun);
        $tgl_akhir = $last_day;

        $data = $this->Shift_Model->nip_ts($tgl_awal, $tgl_akhir);

        // print_r($data); exit;

        $tanggal = array();
        while (strtotime($tgl_awal) <= strtotime($tgl_akhir)) {
            $tanggal[] = $tgl_awal;
            $tgl_awal = date("Y-m-d", strtotime("+1 day", strtotime($tgl_awal)));
        }
        
        $html = '<table border="1" style="width:100%; font-size:12px;">';
        $html .= '	<tr style="background-color:#3071b5; color:#fff;">
						<td style="padding:2px;">NIK</td>
						<td style="padding:2px;">NAMA</td>';
        for ($a = 0; $a < count($tanggal); $a++) {
            $html .= '<td style="padding:2px;" align="center"><strong>'.date_format(date_create($tanggal[$a]), 'd').'</strong><br>'.hari(date_format(date_create($tanggal[$a]), 'w')).'</td>';
        }
        $html .=    '</tr>';

        for ($i = 1; $i <= count($data); $i++) {
            $warna = isset($data[$i]['warna']) ? $data[$i]['warna'] : 'silver';
            $nip = isset($data[$i]['nip']) ? $data[$i]['nip'] : '';
            $nama = isset($data[$i]['nama']) ? $data[$i]['nama'] : '';


            $html .= '	<tr>
								<td style="padding:2px;background:'.$warna.';" >'.$nip.'</td>
								<td style="padding:2px;background:'.$warna.';" >'.$nama.'</td>';
            for ($b = 0; $b < count($tanggal); $b++) {
                $color = isset($data[$i][$tanggal[$b]]['color']) ? $data[$i][$tanggal[$b]]['color'] : 'silver';
                $keterangan = isset($data[$i][$tanggal[$b]]['keterangan']) ? $data[$i][$tanggal[$b]]['keterangan'] : '';
                $jam_masuk = isset($data[$i][$tanggal[$b]]['jam_masuk']) ? $data[$i][$tanggal[$b]]['jam_masuk'] : '';
                $jam_pulang = isset($data[$i][$tanggal[$b]]['jam_pulang']) ? $data[$i][$tanggal[$b]]['jam_pulang'] : '';
                $shift = isset($data[$i][$tanggal[$b]]['shift']) ? $data[$i][$tanggal[$b]]['shift'] : '';

                $html .= '<td style="background-color:'.$color.'" align="center"><span style="cursor: pointer;" title="'.$nama.','.$keterangan.', Masuk : '.$jam_masuk.', Pulang : '.$jam_pulang.'"><strong>'.$shift.'</strong></span></td>';
            }
                $html .=    '</tr>';
        }
        
        $html .= '</table>';

        if (! empty($data)) {
            echo $html;
        } else {
            echo 'Data tidak ditemukan';
        }
    }
}

/* End of file shift.php */
/* Location: ./application/controllers/shift.php */
