<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Download_Excel extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Api_Model', '', true);
        $this->load->model('Akun_Model', '', true);
        $this->load->model('Tunjangan_Model', '', true);
        $this->load->model('Gaji_Model', '', true);
        $this->load->model('Rest_Model', '', true);
        $this->load->model('Rekap_Absensi_Model', '', true);
    }

    function habis_kontrak($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $periode = $this->Main_Model->id_periode($qd_id);
        $per = isset($periode->periode) ? $periode->periode : '';

        $title = 'Rekap Habis Kontrak '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata  = $this->Karyawan_Model->view_kary_kontrak_habis($qd_id);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Habis Kontrak");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Habis Kontrak");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Habis Kontrak");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Habis Kontrak'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', 'Rekap Habis Kontrak Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
            $val = array("NO","NIP","NAMA","POSISI","CABANG","STATUS KARYAWAN","AWAL KONTRAK","AKHIR KONTRAK");
             
        for ($a=0; $a<8; $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->jab);
            $objset->setCellValue("E".$baris, $frow->cabang);
            $objset->setCellValue("F".$baris, $frow->kary_stat);
            $objset->setCellValue("G".$baris, $this->Main_Model->tanggal($frow->tgl_awal));
            $objset->setCellValue("H".$baris, $this->Main_Model->tanggal($frow->tgl_akhir));
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Habis Kontrak');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function kary_resign($tahun = '', $bulan = '')
    {
        $this->Main_Model->get_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $periode = $this->Main_Model->id_periode($qd_id);
        $per = isset($periode->periode) ? $periode->periode : '';

        $title = 'Rekap Karyawan Resign '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata  = $this->Karyawan_Model->view_kary_resign($qd_id);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Karyawan Resign");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Karyawan Resign");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Karyawan Resign");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Karyawan Resign'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
            $objset->setCellValue('A1', 'Rekap Karyawan Resign Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I");
            $val = array("NO","NIP","NAMA","POSISI","CABANG","TANGGAL MASUK","TANGGAL RESIGN","REHIRE","ALASAN RESIGN");
             
        for ($a=0; $a<9; $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i=1;
        foreach ($ambildata as $frow) {
            $sk = $this->db->query("select max(a.`id_sk`) id_sk,c.`jab`,d.`cabang` from sk a join pos_sto b on a.`id_pos_sto`=b.`id_sto` join pos c ON b.`id_pos`=c.`id_pos` JOIN ms_cabang d on d.`id_cab`=c.`id_cabang` where a.`nip` = '$frow->nip'")->row();
            ($frow->tipe_resign) ? $alasan_resign = '('.$frow->tipe_resign.') '.$frow->alasan_resign : $alasan_resign = $frow->alasan_resign;
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $sk->jab);
            $objset->setCellValue("E".$baris, $sk->cabang);
            $objset->setCellValue("F".$baris, $this->Main_Model->tanggal($frow->tgl_masuk));
            $objset->setCellValue("G".$baris, $this->Main_Model->tanggal($frow->tgl_resign));
            $objset->setCellValue("H".$baris, $frow->rehire);
            $objset->setCellValue("I".$baris, $alasan_resign);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:I'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Karyawan Resign');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function kary_baru($tahun = '', $bulan = '')
    {
        $this->Main_Model->get_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $periode = $this->Main_Model->id_periode($qd_id);
        $per = isset($periode->periode) ? $periode->periode : '';

        $title = 'Rekap Karyawan Baru '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Karyawan_Model->view_kary_baru($qd_id);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Karyawan Baru");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Karyawan Baru");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Karyawan Baru");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Karyawan Baru'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
            $objset->setCellValue('A1', 'Rekap Karyawan Baru Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G");
            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","TANGGAL MASUK");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->kary_stat);
            $objset->setCellValue("E".$baris, $frow->jab);
            $objset->setCellValue("F".$baris, $frow->cabang);
            $objset->setCellValue("G".$baris, $this->Main_Model->tanggal($frow->tgl_masuk));
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:G'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Karyawan Baru');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function kary_ijazah()
    {
        $this->Main_Model->get_login();

        $title = 'Rekap Karyawan Belum Mengumpulkan Ijazah';
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Karyawan_Model->view_kary_ijazah();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Rekap Ijazah");
            $objPHPExcel->getProperties()->setSubject("Rekap Ijazah");
            $objPHPExcel->getProperties()->setDescription("Rekap Ijazah");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Ijazah'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', 'Rekap Karyawan Belum Mengumpulkan Ijazah');

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","STATUS IJAZAH","NO IJAZAH");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->kary_stat);
            $objset->setCellValue("E".$baris, $jab);
            $objset->setCellValue("F".$baris, $cabang);
            $objset->setCellValue("G".$baris, $frow->status_ijazah);
            $objset->setCellValue("H".$baris, $frow->no_ijazah);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Ijazah');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function kary_komitmen()
    {
        $this->Main_Model->get_login();
        $title = 'Rekap Karyawan Belum Mengumpulkan Surat Komitmen';
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata  = $this->Karyawan_Model->view_kary_surat_komitmen();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Rekap Surat Komitmen");
            $objPHPExcel->getProperties()->setSubject("Rekap Surat Komitmen");
            $objPHPExcel->getProperties()->setDescription("Rekap Surat Komitmen");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Surat Komitmen'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
            $objset->setCellValue('A1', 'Rekap Karyawan Belum Mengumpulkan Surat Komitmen');

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G");
            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","SURAT KOMITMEN");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            ($frow->file_surat_komitmen == '' && $frow->surat_komitmen == 'SUDAH') ? $surat_komitmen = $frow->surat_komitmen.' (File tidak ada)' : $surat_komitmen = $frow->surat_komitmen;
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->kary_stat);
            $objset->setCellValue("E".$baris, $jab);
            $objset->setCellValue("F".$baris, $cabang);
            $objset->setCellValue("G".$baris, $surat_komitmen);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:G'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Surat Komitmen');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function kary_kesanggupan()
    {
        $this->Main_Model->get_login();
        $title = 'Rekap Karyawan Belum Mengumpulkan Surat Kesanggupan';
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata  = $this->Karyawan_Model->view_surat_kesanggupan();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Rekap Surat Kesanggupan");
            $objPHPExcel->getProperties()->setSubject("Rekap Surat Kesanggupan");
            $objPHPExcel->getProperties()->setDescription("Rekap Surat Kesanggupan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Surat Kesanggupan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
            $objset->setCellValue('A1', 'Rekap Karyawan Belum Mengumpulkan Surat Kesanggupan');

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I");
            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","SP","AKHIR SP","ALASAN SP");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            ($frow->file_surat_komitmen == '' && $frow->surat_komitmen == 'SUDAH') ? $surat_komitmen = $frow->surat_komitmen.' (File tidak ada)' : $surat_komitmen = $frow->surat_komitmen;
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->kary_stat);
            $objset->setCellValue("E".$baris, $jab);
            $objset->setCellValue("F".$baris, $cabang);
            $objset->setCellValue("G".$baris, $frow->sp);
            $objset->setCellValue("H".$baris, $this->Main_Model->format_tgl($frow->akhir_sp));
            $objset->setCellValue("I".$baris, $frow->alasan_sp);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:I'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Surat Kesanggupan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }
    
    function rekening()
    {
        $this->Main_Model->get_login();
        $title = 'Rekap Karyawan Belum Punya Rekening';
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata  = $this->Karyawan_Model->view_kary_rekening();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Rekap Rekening");
            $objPHPExcel->getProperties()->setSubject("Rekap Rekening");
            $objPHPExcel->getProperties()->setDescription("Rekap Rekening");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Rekening'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', 'Rekap Karyawan Belum Punya Rekening Cut off '.date("d F Y"));

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
            $val = array("NO","NAMA","DIVISI","CABANG","JABATAN","STATUS","NIP","NO REK");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            $divisi     = isset($posisi->divisi) ? $posisi->divisi : '';
                
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nama);
            $objset->setCellValue("C".$baris, $divisi);
            $objset->setCellValue("D".$baris, $cabang);
            $objset->setCellValue("E".$baris, $jab);
            $objset->setCellValue("F".$baris, $frow->kary_stat);
            $objset->setCellValue("G".$baris, $frow->nip);
            $objset->setCellValue("H".$baris, $frow->norek);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Rekening');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function meninggalkan_kerja($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $periode = $this->Main_Model->id_periode($qd_id);
        $per = isset($periode->periode) ? $periode->periode : '';

        $title = 'Rekap Ijin Meninggalkan Kerja '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata  = $this->Absensi_Model->view_ijinjam($qd_id);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Ijin Meninggalkan Kerja");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Ijin Meninggalkan Kerja");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Ijin Meninggalkan Kerja");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Ijin Meninggalkan Kerja'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1');
            $objset->setCellValue('A1', 'Rekap Ijin Meninggalkan Kerja Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J");
            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","TANGGAL","JUMLAH MENIT","KETERANGAN","STATUS");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->kary_stat);
            $objset->setCellValue("E".$baris, $frow->jab);
            $objset->setCellValue("F".$baris, $frow->cabang);
            $objset->setCellValue("G".$baris, $frow->tgl);
            $objset->setCellValue("H".$baris, $frow->jmljam);
            $objset->setCellValue("I".$baris, $frow->keterangan);
            $objset->setCellValue("J".$baris, $frow->app_status);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:J'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Ijin Meninggalkan Kerja');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function detail_pulang_awal($nip = '', $bulan = '', $th = '')
    {
        $this->Main_Model->all_login();

        $title = 'Detail Ijin Pulang Awal';
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Absensi_Model->view_detail_pulang_awal($nip, $bulan, $th);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Detail Ijin Pulang Awal");
            $objPHPExcel->getProperties()->setSubject("Download Detail Ijin Pulang Awal");
            $objPHPExcel->getProperties()->setDescription("Download Detail Pulang Awal");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Detail Ijin Pulang Awal'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
            $objset->setCellValue('A1', 'Detail Ijin Pulang Awal');

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G");
            $val = array("NO","NIP","NAMA","TANGGAL","JAM PULANG","STATUS","ALASAN");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            switch ($frow->status) {
                case '1':
                        $status = 'Disetujui';
                    break;
                case '2':
                        $status = 'Ditolak';
                    break;
                default:
                        $status = 'Proses';
                    break;
            }
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $this->Main_Model->tanggal($frow->tgl));
            $objset->setCellValue("E".$baris, $frow->jam);
            $objset->setCellValue("F".$baris, $frow->keterangan);
            $objset->setCellValue("G".$baris, $frow->alasan);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:G'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Detail Ijin Pulang Awal');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function detail_ijinjam($nip = '', $bulan = '', $th = '')
    {
        $this->Main_Model->all_login();

        $title = 'Detail Meninggalkan Kerja';
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Absensi_Model->view_detail_ijinjam($nip, $bulan, $th);
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Detail Meninggalkan Kerja");
            $objPHPExcel->getProperties()->setSubject("Download Detail Meninggalkan Kerja");
            $objPHPExcel->getProperties()->setDescription("Download Detail Meninggalkan Kerja");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Detail Ijin Meninggalkan Kerja'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
            $objset->setCellValue('A1', 'Detail Ijin Meninggalkan Kerja');

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G");
            $val = array("NO","NIP","NAMA","TANGGAL","JUMLAH MENIT","KETERANGAN","STATUS");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            switch ($frow->status) {
                case '1':
                        $status = 'Disetujui';
                    break;
                case '2':
                        $status = 'Ditolak';
                    break;
                default:
                        $status = 'Proses';
                    break;
            }
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $this->Main_Model->tanggal($frow->tgl));
            $objset->setCellValue("E".$baris, $frow->jmljam);
            $objset->setCellValue("F".$baris, $frow->keterangan);
            $objset->setCellValue("G".$baris, $frow->ket);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:G'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Detail Meninggalkan Kerja');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function pulang_awal($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $periode = $this->Main_Model->id_periode($qd_id);
        $per = isset($periode->periode) ? $periode->periode : '';
        $idp = $this->session->userdata('idp');

        $title = 'Rekap Ijin Pulang Awal '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Absensi_Model->laporan_pulang_awal($idp, $qd_id);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Ijin Pulang Awal");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Ijin Pulang Awal");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Pulang Awal");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Ijin Pulang Awal'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
            $objset->setCellValue('A1', 'Rekap Ijin Pulang Awal Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I");
            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","POSISI","CABANG","TANGGAL","JAM PULANG","ALASAN");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            $posisi     = $this->Main_Model->posisi($frow->nip);
            $cabang     = isset($posisi->cabang) ? $posisi->cabang : '';
            $jab        = isset($posisi->jab) ? $posisi->jab : '';
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->kary_stat);
            $objset->setCellValue("E".$baris, $jab);
            $objset->setCellValue("F".$baris, $cabang);
            $objset->setCellValue("G".$baris, $this->Main_Model->tanggal($frow->tgl));
            $objset->setCellValue("H".$baris, $frow->jam);
            $objset->setCellValue("I".$baris, $frow->alasan);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:I'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Ijin Pulang Awal');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function history_sk($date_start = '', $date_end = '')
    {
        $this->Main_Model->get_login();
        $tgl_awal = $this->Main_Model->tanggal($date_start);
        $tgl_akhir = $this->Main_Model->tanggal($date_end);

        $title = 'History SK '.$tgl_awal.' sd '.$tgl_akhir;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Karyawan_Model->history_sk($date_start, $date_end);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download History SK");
            $objPHPExcel->getProperties()->setSubject("Download History SK");
            $objPHPExcel->getProperties()->setDescription("Download History SK");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('History SK'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I");
            $val = array("NO","NIP","NAMA","JABATAN LAMA","LOKASI LAMA","JABATAN BARU","LOKASI BARU", "TGL AKTIF", "TIPE SK");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->jab_lama);
            $objset->setCellValue("E".$baris, $frow->lok_lama);
            $objset->setCellValue("F".$baris, $frow->jab);
            $objset->setCellValue("G".$baris, $frow->cabang);
            $objset->setCellValue("H".$baris, $this->Main_Model->format_tgl($frow->tgl_sk));
            $objset->setCellValue("I".$baris, $frow->tipe);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:I'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('History SK');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function terlambat_today()
    {
        $this->Main_Model->all_login();
        $today = date('d F Y');

        $title = 'Terlambat Tanggal '.$today;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Api_Model->terlambat();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Terlambat");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Terlambat");
            $objPHPExcel->getProperties()->setDescription("Download Terlambat");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Terlambat'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
            $objset->setCellValue('A1', 'Terlambat Tanggal '.$today);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F");
            $val = array("NO","NIP","NAMA", "CABANG", "WAKTU SCAN", "JAM MASUK");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->cabang);
            $objset->setCellValue("E".$baris, $frow->scan);
            $objset->setCellValue("F".$baris, $frow->jam_masuk);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Terlambat');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function sudah_absen()
    {
        $this->Main_Model->guest_login();
        $today = date('d F Y');

        $title = 'Absen Tanggal '.$today;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $id_cabang = $this->session->userdata('cabang');
        $ambildata  = $this->Akun_Model->view_sudah_absen($id_cabang);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Absen");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Absen");
            $objPHPExcel->getProperties()->setDescription("Download Absen");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Absen'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
            $objset->setCellValue('A1', 'Absen Tanggal '.$today);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E");
            $val = array("NO","NIP","NAMA","WAKTU","CABANG");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->waktu);
            $objset->setCellValue("E".$baris, $frow->cabang);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:E'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Absen');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function mangkir()
    {
        $this->Main_Model->guest_login();
        $today = date('d F Y', strtotime("-1 day", strtotime(date('d F Y'))));

        $title = 'Mangkir Tanggal '.$today;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $id_cabang = $this->session->userdata('cabang');
        $ambildata  = $this->Akun_Model->view_mangkir($id_cabang);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Mangkir");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Mangkir");
            $objPHPExcel->getProperties()->setDescription("Download Mangkir");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Mangkir'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
            $objset->setCellValue('A1', 'Mangkir Tanggal '.$today);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F");
            $val = array("NO","NIP","NAMA","TANGGAL","KETERANGAN","CABANG");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->waktu);
            $objset->setCellValue("E".$baris, $frow->cabang);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Mangkir');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function detail_terlambat($nip = '', $periode = '')
    {
        $this->Main_Model->guest_login();
        $q = $this->Main_Model->id_periode($periode);
        $today = isset($q->periode) ? $q->periode : '';

        $title = 'Detail Keterlambatan Periode '.$today;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Absensi_Model->detail_terlambat($nip, $periode);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Detail Keterlambatan");
            $objPHPExcel->getProperties()->setSubject("Download Detail Keterlambatan");
            $objPHPExcel->getProperties()->setDescription("Download Detail Keterlambatan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Detail Keterlambatan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F");
            $val = array("NO","NIP","NAMA","TANGGAL","TOTAL MENIT","SCAN MASUK", "KLARIFIKASI");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $frow) {
            $scan_absen = $frow->scan_absen;
            if ($scan_absen != '' && $frow->klarifikasi != '') {
                $scan_absen = ' ('.$scan_absen.')';
            } else {
                $scan_absen = '';
            }

            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, ($frow->tgl) ? $this->Main_Model->format_tgl($frow->tgl) : '');
            $objset->setCellValue("E".$baris, $frow->total_menit);
            $objset->setCellValue("F".$baris, $frow->scan_masuk.$scan_absen);
            $objset->setCellValue("G".$baris, $frow->klarifikasi);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Keterlambatan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function history_kontrak($tahun = '', $bulan = '')
    {
        $this->Main_Model->guest_login();
        $p = periode($tahun, $bulan);
        $id_periode = isset($p->qd_id) ? $p->qd_id : '';
        $q = $this->Main_Model->id_periode($id_periode);
        $today = isset($q->periode) ? $q->periode : '';

        $title = 'History Kontrak Karyawan '.$today;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Karyawan_Model->view_history_kontrak($id_periode);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download History Kontrak");
            $objPHPExcel->getProperties()->setSubject("Download History Kontrak");
            $objPHPExcel->getProperties()->setDescription("Download History Kontrak");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('History Kontrak'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header

            $val = array("NO","NIP","NAMA","STATUS KARYAWAN","TANGGAL AWAL","TANGGAL AKHIR","KONTRAK LAMA", "TANGGAL AWAL KONTRAK LAMA", "TANGGAL AKHIR KONTRAK LAMA", "KETERANGAN");
            $cols = array();
        for ($i = 1; $i <= count($val); $i++) {
            $cols[] .= $this->alphabet($i);
        }
            
            $awal_tabel = '3';

        for ($a = 0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].$awal_tabel, $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(50);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = '4';
            $j = '1';
        foreach ($ambildata as $frow) {
            if ($frow->status_karyawan == 'TETAP') {
                $tgl_awal = '';
                $tgl_akhir = '';
            } else {
                $tgl_awal = isset($frow->tgl_awal) ? $this->Main_Model->format_tgl($frow->tgl_awal) : '';
                $tgl_akhir = isset($frow->tgl_akhir) ? $this->Main_Model->format_tgl($frow->tgl_akhir) : '';
            }

            if ($frow->tipe_kontrak_lama != '') {
                if ($frow->status_karyawan == $frow->tipe_kontrak_lama) {
                    $keterangan = 'Perpanjangan Masa '.$frow->status_karyawan.' dari '.$tgl_awal.' s/d '.$tgl_akhir;
                } else {
                    $keterangan = 'Perpindahan Status Karyawan dari '.$frow->tipe_kontrak_lama.' ke '.$frow->status_karyawan;
                }
            } else {
                $keterangan = '';
            }

            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $j++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->status_karyawan);
            $objset->setCellValue("E".$baris, $tgl_awal);
            $objset->setCellValue("F".$baris, $tgl_akhir);
            $objset->setCellValue("G".$baris, $frow->tipe_kontrak_lama);
            $objset->setCellValue("H".$baris, ($frow->tgl_awal_lama) ? $this->Main_Model->format_tgl($frow->tgl_awal_lama) : '');
            $objset->setCellValue("I".$baris, ($frow->tgl_akhir_lama) ? $this->Main_Model->format_tgl($frow->tgl_akhir_lama) : '');
            $objset->setCellValue("J".$baris, $keterangan);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b= $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:'.$this->alphabet(count($val)).$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('History Kontrak');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_klarifikasi($param = '', $tahun = '', $bulan = '')
    {
        $this->Main_Model->guest_login();

        $p = periode($tahun, $bulan);
        $id_periode = isset($p->qd_id) ? $p->qd_id : '';

        if ($param == 'klarifikasi_terlambat') {
            $tag = 'Klarifikasi Terlambat';
            $ambildata  = $this->Absensi_Model->view_klarifikasi_terlambat($id_periode);
        } else {
            $tag = 'Klarifikasi Absensi';
            $ambildata  = $this->Absensi_Model->view_klarifikasi_absensi($id_periode);
        }

        $q = $this->Main_Model->id_periode($id_periode);
        $today = isset($q->periode) ? $q->periode : '';

        $title = $tag.' Periode '.$today;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download ".$tag);
            $objPHPExcel->getProperties()->setSubject("Download ".$tag);
            $objPHPExcel->getProperties()->setDescription("Download ".$tag);
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle($tag); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header

            $val = array("NO","NIP","NAMA","CABANG","TANGGAL","KETERANGAN","KLARIFIKASI");
        if ($param == 'klarifikasi_terlambat') {
            $val[] = 'UANG MAKAN';
        }

            $cols = array();
        for ($i = 1; $i <= count($val); $i++) {
            $cols[] .= $this->alphabet($i);
        }
            
            $awal_tabel = '3';

        for ($a = 0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].$awal_tabel, $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = '4';
            $j = '1';
        foreach ($ambildata as $frow) {
            $jam_masuk = isset($frow->jam_masuk) ? 'Jam Masuk '.$frow->jam_masuk : '';
            $presensi = isset($frow->presensi) ? $frow->presensi : '';
            $keterangan = ($param == 'klarifikasi_terlambat') ? $jam_masuk : $presensi;
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $j++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->cabang);
            $objset->setCellValue("E".$baris, ($frow->tgl) ? $this->Main_Model->format_tgl($frow->tgl) : '');
            $objset->setCellValue("F".$baris, $keterangan);
            $objset->setCellValue("G".$baris, $frow->klarifikasi);
            if ($param == 'klarifikasi_terlambat') {
                $um = $frow->uang_makan;
                $uang_makan = ($um == 1) ? 'Ya' : 'Tidak';
                $objset->setCellValue("H".$baris, $uang_makan);
            }
                
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b= $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:'.$this->alphabet(count($val)).$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle($tag);


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_sp($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $a = $this->convert_bulan($bulan);
        ($bulan != '') ? $periode = $a.' '.$tahun : $periode = $tahun;

        $title = 'Laporan SP '.$periode;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $where = (empty($id_cabang) && empty($sal_pos) && empty($divisi)) ? '' : " $cabang $kelas $div";

        $condition = ($bulan) ? "AND DATE_FORMAT(a.`awal_sp`,'%m-%Y') = '$bulan-$tahun'" : "AND DATE_FORMAT(a.`awal_sp`,'%Y') = '$tahun'";

        $cari_data = $this->db->query("
                SELECT a.*,b.`nama`, f.`cabang`, e.`jab`, g.`divisi`  
                FROM tb_sp a 
                JOIN kary b ON a.`nip` = b.`nip`
                JOIN sk c ON c.`nip` = a.`nip`
                JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                JOIN pos e ON e.`id_pos` = d.`id_pos`
                JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
                JOIN ms_divisi g ON e.`id_divisi` = g.`id_divisi`
                WHERE c.`aktif` = '1' 
                $condition
                $where
                ")->result();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $judul = 'Laporan SP';
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download ".$judul);
            $objPHPExcel->getProperties()->setSubject("Download ".$judul);
            $objPHPExcel->getProperties()->setDescription("Download ".$judul);
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle($judul); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M");
            $val = array("NO","NIP","NAMA","CABANG","DIVISI","JABATAN","TGL TERBIT SP","MASA BERLAKU SP","SP","STATUS","ALASAN SP","TIPE SP","TERBIT");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $no = 1;
        foreach ($cari_data as $frow) {
            $today = date("Y-m-d");
            if ($frow->aktif == '1') {
                ($today <= $frow->akhir_sp) ? $status = 'Aktif' : $status = 'Tidak Aktif';
            } else {
                $status = 'Tidak Aktif';
            }

            if ($frow->sp_terbit == 1) {
                $terbit = 'Terbit';
            } else {
                $terbit = 'Belum Terbit';
            }
                
            $awal_sp = isset($frow->awal_sp) ? $this->Main_Model->format_tgl($frow->awal_sp) : '';
            $akhir_sp = isset($frow->akhir_sp) ? $this->Main_Model->format_tgl($frow->akhir_sp) : '';
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $no++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->cabang);
            $objset->setCellValue("E".$baris, $frow->divisi);
            $objset->setCellValue("F".$baris, $frow->jab);
            $objset->setCellValue("G".$baris, $awal_sp);
            $objset->setCellValue("H".$baris, $akhir_sp);
            $objset->setCellValue("I".$baris, $frow->sp);
            $objset->setCellValue("J".$baris, $status);
            $objset->setCellValue("K".$baris, $frow->alasan_sp);
            $objset->setCellValue("L".$baris, $frow->tipe_sp);
            $objset->setCellValue("M".$baris, $terbit);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:M'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle($judul);


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_cuti($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $a = $this->convert_bulan($bulan);
        ($bulan != '') ? $periode = $a.' '.$tahun : $periode = $tahun;

        $title = 'Laporan Cuti '.$periode;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);
        
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= ' (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' f.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " f.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " f.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        // $join = (empty($cabang)) ? '' : '';
        $where = (empty($id_cabang) || $id_cabang == '') ? '' : " AND $cabang $kelas $div";

        $condition = ($bulan) ? "DATE_FORMAT(a.`tgl`,'%m-%Y') = '$bulan-$tahun'" : "DATE_FORMAT(a.`tgl`,'%Y') = '$tahun'";

        $cari_data = $this->db->query("
                SELECT distinct(a.`id_cuti_det`),b.`nip`,b.`alasan_cuti`,b.`alamat_cuti`,b.`no_hp`,c.`nama`, g.`cabang`,
                IFNULL(kuota.qt, 0) sisa_cuti
                FROM cuti_sub_det a 
                JOIN cuti_det b ON a.`id_cuti_det`=b.`id_cuti_det` 
                JOIN kary c ON b.`nip`=c.`nip` 
                JOIN sk d ON c.`nip` = d.`nip` 
                JOIN pos_sto e ON d.`id_pos_sto` = e.`id_sto` 
                JOIN pos f ON f.`id_pos` = e.`id_pos` 
                JOIN ms_cabang g ON g.`id_cab` = f.`id_cabang`
                JOIN ms_status_approval h ON h.`kode` = b.`approval`
                LEFT JOIN (
                    SELECT * FROM cuti_dep 
                ) AS kuota ON kuota.nip = b.`nip` AND kuota.th = YEAR(a.`tgl`)
                WHERE $condition
                AND d.`aktif` = '1' $where AND b.`approval` = '12'
                ");

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $judul = 'Laporan Cuti';
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download ".$judul);
            $objPHPExcel->getProperties()->setSubject("Download ".$judul);
            $objPHPExcel->getProperties()->setDescription("Download ".$judul);
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle($judul); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I");
            // $cols = array("A","B","C","D","E");
            $val = array("NO","NIP","NAMA","CABANG","ALASAN CUTI","TANGGAL","ALAMAT CUTI","NO HP","SISA CUTI");
            // $val = array("NO","NIP","NAMA","ALASAN CUTI","TANGGAL");

             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(5);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $no = 1;
        foreach ($cari_data->result() as $frow) {
            $detail = $this->db->query("SELECT DATE_FORMAT(a.`tgl`,'%d %M %Y') tanggal FROM cuti_sub_det a WHERE a.`id_cuti_det` = '$frow->id_cuti_det'")->result();
            $tanggal = '';
            $i = 1;
            $jumlah = count($detail);
            foreach ($detail as $row) {
                if ($row->tanggal) {
                    $tgl = ($jumlah == $i) ? $row->tanggal.'' : $row->tanggal.', ';
                    $tanggal .= $tgl;
                }
                $i++;
            }

            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $no++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->nama);
            $objset->setCellValue("D".$baris, $frow->cabang);
            $objset->setCellValue("E".$baris, $frow->alasan_cuti);
            $objset->setCellValue("F".$baris, $tanggal);
            $objset->setCellValue("G".$baris, $frow->alamat_cuti);
            $objset->setCellValue("H".$baris, $frow->no_hp);
            $objset->setCellValue("I".$baris, $frow->sisa_cuti);
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:I'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle($judul);


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_teguran($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $a = $this->convert_bulan($bulan);
        ($bulan != '') ? $periode = $a.' '.$tahun : $periode = $tahun;

        $title = 'Laporan Teguran '.$periode;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);
        
        $cari_data = $this->Absensi_Model->laporan_teguran($tahun, $bulan);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $judul = 'Laporan Teguran';
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download ".$judul);
            $objPHPExcel->getProperties()->setSubject("Download ".$judul);
            $objPHPExcel->getProperties()->setDescription("Download ".$judul);
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle($judul); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:K1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K");
            $val = array("NO","NIP","NAMA","JABATAN","CABANG","PERIODE","PENCAPAIAN (%)","PENCAPAIAN (Rp)","TARGET (Rp)", "TEGURAN", "REKOMENDASI SP");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $no = 1;
        if ($cari_data) {
            foreach ($cari_data as $frow) {
                ($frow->teguran == '1') ? $status = 'Ya' : $status = 'Tidak';
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $no++);
                $objset->setCellValue("B".$baris, $frow->nip);
                $objset->setCellValue("C".$baris, $frow->nama);
                $objset->setCellValue("D".$baris, $frow->jabatan);
                $objset->setCellValue("E".$baris, $frow->cabang);
                $objset->setCellValue("F".$baris, $this->convert_bulan($frow->bulan).' '.$frow->tahun);
                $objset->setCellValue("G".$baris, $frow->persen_pencapaian);
                $objset->setCellValue("H".$baris, $frow->rp_pencapaian);
                $objset->setCellValue("I".$baris, $frow->target);
                $objset->setCellValue("J".$baris, $status);
                $objset->setCellValue("K".$baris, $frow->sp);

                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                     
                $baris++;
            }
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:K'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle($judul);


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_lembur()
    {
        $this->Main_Model->all_login();
        // $p = periode($tahun, $bulan);
        // $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        // $periode = $this->Main_Model->id_periode($qd_id);
        $idp = $this->session->userdata('idp');
        // $per = isset($periode->periode) ? $periode->periode : '';

        $tgl_awal = $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->get('tgl_akhir');
        $nip = $this->input->get('nip');

        $tgl_awal = $this->Main_Model->convert_tgl($tgl_awal);
        $tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);
        $per = tgl_api($tgl_awal).' sd '.tgl_api($tgl_akhir);

        $title = 'Laporan Lembur '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $id_cabang = $this->Main_Model->session_cabang();
        $ambildata  = $this->Tunjangan_Model->view_lembur($tgl_awal, $tgl_akhir, $nip, $id_cabang);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Laporan Lembur");
            $objPHPExcel->getProperties()->setSubject("Download Laporan Lembur");
            $objPHPExcel->getProperties()->setDescription("Download Laporan Lembur");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Laporan Lembur'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
            $objset->setCellValue('A1', 'Laporan Lembur Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F");
            $val = array("#","NIK","NAMA","TANGGAL","JAM","NOMINAL");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $row) {
            switch ($row->status) {
                case '1':
                        $status = 'Disetujui';
                    break;
                case '2':
                        $status = 'Ditolak';
                    break;
                default:
                        $status = 'Proses';
                    break;
            }

            $nominal = $row->lembur + $row->uangmakan;
            ($row->status == 1) ? $nominal = $nominal : $nominal = 0;
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $row->nip);
            $objset->setCellValue("C".$baris, $row->nama);
            $objset->setCellValue("D".$baris, tgl_indo($row->tgl));
            $objset->setCellValue("E".$baris, $row->jam);
            $objset->setCellValue("F".$baris, $nominal);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:F'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Laporan Lembur');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_perdin($tahun = '', $bulan = '')
    {
        $this->Main_Model->all_login();
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $periode = $this->Main_Model->id_periode($qd_id);
        $per = isset($periode->periode) ? $periode->periode : '';
        $idp = $this->session->userdata('idp');

        $title = 'Laporan Perdin Periode '.$per;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $id_cabang = $this->Main_Model->session_cabang();
        $ambildata  = $this->Absensi_Model->view_perjalanan_dinas($id_cabang, $qd_id);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Laporan Perdin");
            $objPHPExcel->getProperties()->setSubject("Download Laporan Perdin");
            $objPHPExcel->getProperties()->setDescription("Download Laporan Perdin");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Laporan Perdin'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', 'Laporan Perdin Periode '.$per);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
            $val = array("NO","NO BUKTI","BERANGKAT","PULANG","TUJUAN","KEPERLUAN","NOMINAL","VERIFIKASI");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i=1;
        foreach ($ambildata as $row) {
            $verif = ($row->verifikasi == 1) ? 'Sudah' : 'Belum';
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $row->nobukti);
            $objset->setCellValue("C".$baris, $row->berangkat);
            $objset->setCellValue("D".$baris, $row->pulang);
            $objset->setCellValue("E".$baris, $row->tujuan);
            $objset->setCellValue("F".$baris, $row->keperluan);
            $objset->setCellValue("G".$baris, $row->total);
            $objset->setCellValue("H".$baris, $verif);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Laporan Perdin');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_absensi($id_cabang = '', $date_start = '', $date_end = '', $nip = '')
    {
        $this->Main_Model->all_login();
        $ambildata  = $this->Absensi_Model->laporan_absensi($id_cabang, $date_start, $date_end, $nip);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $date_start = $this->Main_Model->tgl_indo($date_start);
        $date_end = $this->Main_Model->tgl_indo($date_end);

        $range_tgl = $date_start.' sd '.$date_end;

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Laporan Absensi");
            $objPHPExcel->getProperties()->setSubject("Download Laporan Absensi");
            $objPHPExcel->getProperties()->setDescription("Download Laporan Absensi");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Laporan Absensi'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');

            $title = 'Laporan Absensi '.$range_tgl;
            $arr = explode(" ", $title);
            $filename = implode("-", $arr);

            $objset->setCellValue('A1', $title);
            

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H", "I", "J", "K", "L", "M");
            $val = array("NIP","PIN","NAMA","CABANG","TGL","SCAN MASUK","SCAN PULANG","SHIFT", "JAM MASUK", "STATUS", "KLARIFIKASI ABSENSI", "KLARIFIKASI PRESENSI", "KLARIFIKASI TERLAMBAT");
             
        for ($a=0; $a<count($cols); $a++) {
            $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 4;
            $i = 1;
        foreach ($ambildata as $row) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $row->nip);
            $objset->setCellValue("B".$baris, $row->pin);
            $objset->setCellValue("C".$baris, $row->nama);
            $objset->setCellValue("D".$baris, $row->cabang);
            $objset->setCellValue("E".$baris, $this->Main_Model->tgl_indo($row->tgl));
            $objset->setCellValue("F".$baris, $row->scan_masuk);
            $objset->setCellValue("G".$baris, $row->scan_pulang);
            $objset->setCellValue("H".$baris, $row->shift);
            $objset->setCellValue("I".$baris, $row->jam_masuk);
            $objset->setCellValue("J".$baris, $row->status);
            $objset->setCellValue("K".$baris, $row->klarifikasi_absensi);
            $objset->setCellValue("L".$baris, $row->klarifikasi_presensi);
            $objset->setCellValue("M".$baris, $row->klarifikasi_terlambat);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A3:M'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Laporan Absensi');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$filename.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function data_absensi()
    {
        $this->Main_Model->all_login();
        $tgl_awal = $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->get('tgl_akhir');

        $awal = $this->Main_Model->convert_tgl($tgl_awal);
        $akhir = $this->Main_Model->convert_tgl($tgl_akhir);

        $f_awal = $this->Main_Model->format_tgl($awal);
        $f_akhir = $this->Main_Model->format_tgl($akhir);
        $tanggal = $f_awal.' sd '.$f_akhir;

        $title = 'Absensi Karyawan '.$tanggal;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Absensi_Model->view_absensi_nip($tgl_awal, $tgl_akhir);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Absensi Karyawan");
            $objPHPExcel->getProperties()->setSubject("Download Absensi Karyawan");
            $objPHPExcel->getProperties()->setDescription("Download Absensi Karyawan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Absensi Karyawan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:P1');
            $objset->setCellValue('A1', 'Absensi Karyawan '.$tanggal);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P");
            $val = array("NO","TANGGAL","NIP","NAMA","CABANG","HARI KERJA","MASUK","SAKIT","IJIN","MANGKIR","CUTI","TERLAMBAT","LEMBUR","JAM LEMBUR","UANG MAKAN", "UANG DENDA");
             
        for ($a=0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i = 1;
        foreach ($ambildata as $row) {
            $potong = $row->jml_sakit + $row->jml_ijin + $row->jml_mangkir + $row->jml_cuti + $row->jml_potong + $row->jml_resign;
            if ($row->jml_dayoff == 0) {
                $j_masuk = $row->jml_kerja - $potong;
                $hari_kerja = $row->jml_kerja;
            } else {
                $j_masuk = $row->hari - ($potong + $row->jml_dayoff);
                $hari_kerja = $row->hari;
            }

            $hari_kerja = ($hari_kerja - $row->jml_potong + $row->jml_resign);
                 
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $tanggal);
            $objset->setCellValue("C".$baris, $row->nip);
            $objset->setCellValue("D".$baris, $row->nama);
            $objset->setCellValue("E".$baris, $row->cabang);
            $objset->setCellValue("F".$baris, $hari_kerja);
            $objset->setCellValue("G".$baris, $j_masuk);
            $objset->setCellValue("H".$baris, $row->jml_sakit);
            $objset->setCellValue("I".$baris, ($row->jumlah_ijin + $row->jumlah_pulang));
            $objset->setCellValue("J".$baris, $row->jml_mangkir);
            $objset->setCellValue("K".$baris, $row->jml_cuti);
            $objset->setCellValue("L".$baris, $row->jumlah_telat);
            $objset->setCellValue("M".$baris, $row->jml_lembur);
            $objset->setCellValue("N".$baris, $row->jmljam_lembur);
            $objset->setCellValue("O".$baris, $row->uang_makan);
            $objset->setCellValue("P".$baris, $row->uang_denda);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:P'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Absensi Karyawan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function data_presensi()
    {
        $this->Main_Model->all_login();
        // $p = periode($tahun, $bulan);
        // $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        // $periode = $this->Main_Model->id_periode($qd_id);
        // $per = isset($periode->periode) ? $periode->periode : '';
        // $tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        // $tgl_akhir = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';

        $awal = $this->input->get('tgl_awal');
        $akhir = $this->input->get('tgl_akhir');

        $tgl_awal = $this->Main_Model->convert_tgl($awal);
        $tgl_akhir = $this->Main_Model->convert_tgl($akhir);

        $f_awal = $this->Main_Model->format_tgl($tgl_awal);
        $f_akhir = $this->Main_Model->format_tgl($tgl_akhir);

        $tanggal = $f_awal.' sd '.$f_akhir;

        $title = 'Presensi Karyawan '.$tanggal;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        $ambildata = $this->Absensi_Model->view_kary_presensi($tgl_awal, $tgl_akhir);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Presensi Karyawan");
            $objPHPExcel->getProperties()->setSubject("Download Presensi Karyawan");
            $objPHPExcel->getProperties()->setDescription("Download Presensi Karyawan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Presensi Karyawan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', 'Presensi Karyawan '.$tanggal);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
            $val = array("NO","TANGGAL","NIP","NAMA","CABANG","SAKIT","IJIN","MANGKIR");
             
        for ($a=0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i = 1;
        foreach ($ambildata as $row) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $tanggal);
            $objset->setCellValue("C".$baris, $row->nip);
            $objset->setCellValue("D".$baris, $row->nama);
            $objset->setCellValue("E".$baris, $row->cabang);
            $objset->setCellValue("F".$baris, $row->sakit);
            $objset->setCellValue("G".$baris, $row->ijin);
            $objset->setCellValue("H".$baris, $row->mangkir);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Presensi Karyawan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function data_presensi_cabang()
    {
        $this->Main_Model->all_login();
        $awal = $this->input->get('tgl_awal');
        $akhir = $this->input->get('tgl_akhir');

        $tgl_awal = $this->Main_Model->convert_tgl($awal);
        $tgl_akhir = $this->Main_Model->convert_tgl($akhir);

        $f_awal = $this->Main_Model->format_tgl($tgl_awal);
        $f_akhir = $this->Main_Model->format_tgl($tgl_akhir);

        $tanggal = $f_awal.' sd '.$f_akhir;

        $title = 'Presensi Karyawan '.$tanggal;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);
        
        $ambildata = $this->Absensi_Model->view_kary_presensi($tgl_awal, $tgl_akhir);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Presensi Karyawan");
            $objPHPExcel->getProperties()->setSubject("Download Presensi Karyawan");
            $objPHPExcel->getProperties()->setDescription("Download Presensi Karyawan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Presensi Karyawan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
            $objset->setCellValue('A1', 'Presensi Karyawan '.$tanggal);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H");
            $val = array("NO","TANGGAL","NIP","NAMA","CABANG","SAKIT","IJIN","MANGKIR");
             
        for ($a=0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i = 1;
        foreach ($ambildata as $row) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $tanggal);
            $objset->setCellValue("C".$baris, $row->nip);
            $objset->setCellValue("D".$baris, $row->nama);
            $objset->setCellValue("E".$baris, $row->cabang);
            $objset->setCellValue("F".$baris, $row->sakit);
            $objset->setCellValue("G".$baris, $row->ijin);
            $objset->setCellValue("H".$baris, $row->mangkir);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Presensi Karyawan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function jumlah_anak($nip = '')
    {
        $status = $this->Main_Model->view_by_id('kary', ['nip' => $nip], 'row');
        $mariage = isset($status->mar_stat) ? $status->mar_stat : 'LAJANG';

        $kk = [];
        if ($mariage != 'LAJANG') {
            $kk = $this->Main_Model->view_by_id('kk', ['nip' => $nip, 'status' => 'Anak'], 'result');
        }

        return count($kk);
    }

    function download_karyawan_aktif($status = '', $cabang = '')
    {
        $this->Main_Model->all_login();
        
        if ($status == 1) {
            $ambildata = $this->Karyawan_Model->view_karyawan_aktif($cabang);
            $title = 'Data Karyawan Aktif';
        } else if ($status == 0) {
            $ambildata = $this->Karyawan_Model->view_karyawan_resign($cabang);
            $title = 'Data Karyawan Resign';
        } else {
            $ambildata = [];
            $title = '';
        }

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Data Karyawan");
            $objPHPExcel->getProperties()->setSubject("Download Data Karyawan");
            $objPHPExcel->getProperties()->setDescription("Download Data Karyawan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Data Karyawan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:AH1');
            $objset->setCellValue('A1', $title);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG","AH","AI","AJ","AK","AL");
            $val = array("NIP", "PIN", "NAMA", "DEPARTEMEN", "CABANG", "DIVISI", "LEVEL", "JABATAN", "STATUS", "TGL MASUK", "MASA KERJA", "JK", "NO KTP", "AGAMA", "GOLONGAN DARAH", "TTL", "USIA", "NO TELP", "ALAMAT", "EMAIL", "STATUS PERNIKAHAN", "NO REKENING", "BANK", "NAMA REKENING", "STATUS REKENING", "PENDIDIKAN", "NAMA SEKOLAH/UNIVERSITAS", "JURUSAN", "TH LULUS", "IJAZAH", "SURAT KOMITMEN", "NPWP", "EMERGENCY CALL", "IKUT BPJS", "JUMLAH ANAK","TGL RESIGN","ALASAN RESIGN","REHIRE");
             
        for ($a=0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AC')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AD')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AE')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AF')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AG')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AH')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AI')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AJ')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AK')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('AL')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i = 1;
        foreach ($ambildata as $row) {
            $ttl = '';
            if ($row->t_lahir == '') {
                $ttl = $row->tgl_lahir;
            } else {
                $ttl = $row->t_lahir.', '.$row->tgl_lahir;
            }

            ($row->contact_emergency != '') ? $emergency = $row->telp_emergency.'('.$row->contact_emergency.')' : $emergency = $row->contact_emergency;
                 
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $row->nip);
            $objset->setCellValue("B".$baris, $row->pin);
            $objset->setCellValue("C".$baris, $row->nama);
            $objset->setCellValue("D".$baris, $row->departemen);
            $objset->setCellValue("E".$baris, $row->cabang);
            $objset->setCellValue("F".$baris, $row->divisi);
            $objset->setCellValue("G".$baris, $row->sal_pos);
            $objset->setCellValue("H".$baris, $row->jab);
            $objset->setCellValue("I".$baris, $row->kary_stat);
            $objset->setCellValue("J".$baris, $row->tgl_masuk);
            $objset->setCellValue("K".$baris, $row->tahun.' TAHUN '.$row->bulan.' BULAN');
            $objset->setCellValue("L".$baris, $row->jk);
            $objset->setCellValue("M".$baris, $row->ktp);
            $objset->setCellValue("N".$baris, $row->agama);
            $objset->setCellValue("O".$baris, $row->golongan_darah);
            $objset->setCellValue("P".$baris, $ttl);
            $objset->setCellValue("Q".$baris, $row->usia_th.' TAHUN '.$row->usia_bln.' BULAN');
            $objset->setCellValue("R".$baris, $row->telp);
            $objset->setCellValue("S".$baris, $row->alamat);
            $objset->setCellValue("T".$baris, $row->mail);
            $objset->setCellValue("U".$baris, $row->mar_stat);
            $objset->setCellValue("V".$baris, $row->norek);
            $objset->setCellValue("W".$baris, $row->bank);
            $objset->setCellValue("X".$baris, $row->norek_an);
            $objset->setCellValue("Y".$baris, $row->tfinfo);
            $objset->setCellValue("Z".$baris, $row->pend);
            $objset->setCellValue("AA".$baris, $row->nama_sklh);
            $objset->setCellValue("AB".$baris, $row->jurusan);
            $objset->setCellValue("AC".$baris, $row->th_lulus);
            $objset->setCellValue("AD".$baris, $row->no_ijazah);
            $objset->setCellValue("AE".$baris, $row->surat_komitmen);
            $objset->setCellValue("AF".$baris, $row->npwp);
            $objset->setCellValue("AG".$baris, $emergency);
            $objset->setCellValue("AH".$baris, $row->ikut_bpjs);
            $objset->setCellValue("AI".$baris, $this->jumlah_anak($row->nip));
            $objset->setCellValue("AJ".$baris, $row->tgl_resign);
            $objset->setCellValue("AK".$baris, $row->alasan_resign);
            $objset->setCellValue("AL".$baris, $row->rehire);

            //Set number value
            $objPHPExcel->getActiveSheet()->getCell('M'.$baris)->setValueExplicit($row->ktp, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCell('AF'.$baris)->setValueExplicit($row->npwp, PHPExcel_Cell_DataType::TYPE_STRING);
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:AL'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Data Karyawan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function laporan_gaji()
    {
        $this->Main_Model->all_login();
        $cabang = $this->input->get('cabang');
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');

        $p = periode($tahun, $bulan);
        $id_periode = isset($p->qd_id) ? $p->qd_id : '';
        $periode = isset($p->periode) ? $p->periode : '';
        $ambildata = $this->Gaji_Model->view_gaji_after_submit($id_periode, $cabang, 'tb_payroll');

        $title = 'Laporan Gaji Periode '.$periode;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Laporan Gaji Karyawan");
            $objPHPExcel->getProperties()->setSubject("Download Laporan Gaji Karyawan");
            $objPHPExcel->getProperties()->setDescription("Download Laporan Gaji Karyawan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Laporan Gaji Karyawan'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:T1');
            $objset->setCellValue('A1', 'Laporan Gaji Karyawan '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T");
            $val = array("NO","PERIODE","NIP","NAMA","CABANG","DIVISI","POSISI","UPAH POKOK", "TUNJANGAN JABATAN", "PENGH. MASA KERJA", "TUNJ. TIDAK TETAP", "TUNJ. KEMAHALAN", "INS. PRESENSI", "TERLAMBAT", "MANGKIR", "SAKIT", "IJIN", "CUTI", "CUTI KHUSUS", "SISA CUTI");
             
        for ($a=0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i = 1;
        foreach ($ambildata as $row) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $row->periode);
            $objset->setCellValue("C".$baris, $row->nip);
            $objset->setCellValue("D".$baris, $row->nama);
            $objset->setCellValue("E".$baris, $row->cabang);
            $objset->setCellValue("F".$baris, $row->divisi);
            $objset->setCellValue("G".$baris, $row->jab);
            $objset->setCellValue("H".$baris, $row->hari_upah_pokok);
            $objset->setCellValue("I".$baris, $row->hari_tunj_jabatan);
            $objset->setCellValue("J".$baris, $row->hari_pengh_kerja);
            $objset->setCellValue("K".$baris, $row->hari_tidak_tetap);
            $objset->setCellValue("L".$baris, $row->tunj_kemahalan);
            $objset->setCellValue("M".$baris, $row->ins_presensi);
            $objset->setCellValue("N".$baris, $row->telat);
            $objset->setCellValue("O".$baris, $row->mangkir);
            $objset->setCellValue("P".$baris, $row->sakit);
            $objset->setCellValue("Q".$baris, $row->ijin);
            $objset->setCellValue("R".$baris, $row->cuti);
            $objset->setCellValue("S".$baris, $row->cuti_khusus);
            $objset->setCellValue("T".$baris, $row->sisa_cuti);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:T'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Laporan Gaji Karyawan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function rekap_periode()
    {
        $this->Main_Model->all_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');

        $p = periode($tahun, $bulan);
        $id_periode = isset($p->qd_id) ? $p->qd_id : '';
        $periode = isset($p->periode) ? $p->periode : '';
        $ambildata = $this->Absensi_Model->view_rekap_periode($tahun, $bulan);

        $title = 'Rekap Absensi Periode '.$periode;
        $arr = explode(" ", $title);
        $title = implode("_", $arr);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Absensi");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Absensi");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Absensi");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Absensi'); //sheet title
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:N1');
            $objset->setCellValue('A1', 'Rekap Absensi '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            //table header
            $cols = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N");
            $val = array("NO","PERIODE","NIP","NAMA","CABANG","POSISI","HARI", "HARI EFEKTIF", "MASUK", "MANGKIR", "SAKIT", "CUTI", "CUTI KHUSUS", "SISA CUTI");
             
        for ($a=0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 3;
            $i = 1;
        foreach ($ambildata as $row) {
            //cuti khusus
            $cuti_khusus = $this->Gaji_Model->cuti_khusus($row->id_periode, $row->nip);

            //masuk
            $masuk = $row->hari - ($row->jml_potong + $row->jml_resign) - ($row->jml_sakit + $row->jml_mangkir + $row->jml_cuti + $cuti_khusus);
                 
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $row->periode);
            $objset->setCellValue("C".$baris, $row->nip);
            $objset->setCellValue("D".$baris, $row->nama);
            $objset->setCellValue("E".$baris, $row->cabang);
            $objset->setCellValue("F".$baris, $row->jab);
            $objset->setCellValue("G".$baris, $row->hari);
            $objset->setCellValue("H".$baris, $row->jml_kerja);
            $objset->setCellValue("I".$baris, $masuk);
            $objset->setCellValue("J".$baris, $row->jml_mangkir);
            $objset->setCellValue("K".$baris, $row->jml_sakit);
            $objset->setCellValue("L".$baris, $row->jml_cuti);
            $objset->setCellValue("M".$baris, $cuti_khusus);
            $objset->setCellValue("N".$baris, $row->sisa_cuti);
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b = $baris - 1;
            $objPHPExcel->getActiveSheet()->getStyle('A2:N'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Absensi');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function download_rekap_absen()
    {
        $nip = $this->input->get('nip');
        $date_start = $this->input->get('tgl_awal');
        $date_end = $this->input->get('tgl_akhir');
        $cabang = '';
        $b = $this->Rest_Model->biodata($nip);
        $id_divisi = isset($b->id_divisi) ? $b->id_divisi : '';

        $date_start = api_tgl($date_start);
        $date_end = api_tgl($date_end);

        $periode = tgl_indo($date_start).' sd '.tgl_indo($date_end);
            $title = 'Rekapitulasi Absensi '.$periode;
            $arr = explode(" ", $title);
            // $title_file = implode("_", $arr);
            $title_file = 'Rekapitulasi%20Absensi%20'.tgl_api($date_start).'%20sd%20'.tgl_api($date_end);

              ini_set('display_errors', true);
              ini_set('display_startup_errors', true);

              define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

              require_once(APPPATH.'libraries/PHPExcel.php');
              require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekapitulasi Absensi");
            $objPHPExcel->getProperties()->setSubject("Download Rekapitulasi Absensi");
            $objPHPExcel->getProperties()->setDescription("Download Rekapitulasi Absensi");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekapitulasi Absensi'); //sheet title
            $objset->setCellValue('A1', 'Rekapitulasi Absensi '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            // range tgl
            $date_range = date_range($date_start, $date_end);
            $jumlah_range = count($date_range);

            // Header Tabel
            $head_val = array('NO', 'NIP', 'NAMA', 'CABANG', 'DIVISI');
            $jumlah_head_val = count($head_val);
        for ($i = 0; $i < $jumlah_head_val; $i++) {
              $a = $i + 1;
              $objset->setCellValue(alphabet($a).'3', $head_val[$i]);
        }
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG MAKAN');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG LEMBUR');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG DENDA');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'BPJS KETENAGAKERJAAN');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'CICILAN PINJAMAN');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'KETERANGAN');

            // Header Tanggal
            $range_head = count($head_val);
        for ($b = 1; $b <= 3; $b++) {
            for ($i = 0; $i < $jumlah_range; $i++) {
                    $a = $i + 1;
                    $tanggal = explode(' s/d ', $date_range[$i]);
                    $awal = tgl_indo($tanggal[0]);
                    $akhir = tgl_indo(end($tanggal));
                    $awal_akhir[$i] = $awal.' s/d '.$akhir;

                    $jarak = $range_head + $a;
                    $objset->setCellValue(alphabet($jarak).'4', $awal_akhir[$i]);

                    $objPHPExcel->getActiveSheet()->getStyle(alphabet($jarak).'4:'.alphabet($jarak).'4')->getAlignment()->setWrapText(true);
            }

              $range_head = $range_head + $jumlah_range + 1;
        }

            $baris = 5;
            //divisi
            $d = $this->Rekap_Absensi_Model->divisi($id_divisi);
        if (!empty($d)) {
            foreach ($d as $row) {
                    //karyawan
                $data = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end, $row->id_divisi, $nip);
                    $objset->setCellValue("A".$baris, $row->divisi);
                $no = 1;
                $i = 1;
                    $baris = $baris + $i;
                    $total_data = count((array)$data) + 1;
                    $total_uang_makan = 0;
                    $total_uang_lembur = 0;
                    $total_uang_denda = 0;
                foreach ($data as $r) {
                    $objset->setCellValue("A".$baris, $no++);
                          $objset->setCellValue("B".$baris, $r->nip);
                          $objset->setCellValue("C".$baris, $r->nama);
                          $objset->setCellValue("D".$baris, $r->cabang);
                          $objset->setCellValue("E".$baris, $r->divisi);

                          $range_head = count($head_val);
                    for ($b = 1; $b <= 3; $b++) {
                        $total = 0;
                        for ($i = 0; $i < $jumlah_range; $i++) {
                              $a = $i + 1;
                              $tanggal = explode(' s/d ', $date_range[$i]);
                              $awal = $tanggal[0];
                              $akhir = end($tanggal);

                            if ($b == 1) {
                                    $um = $this->Rekap_Absensi_Model->uang_makan($awal, $akhir, $r->nip);
                                    $uang[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                                    $total_uang_makan = $total_uang_makan + $uang[$i];
                            } elseif ($b == 2) {
                                        $um = $this->Rekap_Absensi_Model->uang_lembur($awal, $akhir, $r->nip);
                                        $uang[$i] = isset($um->uang_lembur) ? $um->uang_lembur : 0;
                                        $total_uang_lembur = $total_uang_lembur + $uang[$i];
                            } else {
                                  $um = $this->Rekap_Absensi_Model->uang_denda($awal, $akhir, $r->nip);
                                  $uang[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                                  $total_uang_denda = $total_uang_denda + $uang[$i];
                            }

                                            $total = $total + $uang[$i];

                                            $jarak = $range_head + $a;
                                            $objset->setCellValue(alphabet($jarak).$baris, $uang[$i]);
                        }
                        $objset->setCellValue(alphabet($jarak + 1).$baris, $total);
                        $range_head = $range_head + $jumlah_range + 1;
                    }

                          // cicilan
                          $c = $this->Rekap_Absensi_Model->cicilan($date_start, $date_end, $r->nip);
                          $cicilan = isset($c->angsuran) ? $c->angsuran : 0;
                          $keterangan = isset($c->keterangan) ? $c->keterangan : '';
                          $bpjs_tk = $this->Rekap_Absensi_Model->bpjs_ketenagakerjaan($r->nip);
                          $nominal_bpjs = isset($bpjs_tk->total_k) ? $bpjs_tk->total_k : 0;

                          $jarak_bpjs = (count($head_val) + ($jumlah_range * 3)) + 4;
                          $objset->setCellValue(alphabet($jarak_bpjs).$baris, uang($nominal_bpjs));
                          $jarak_bpjs = $jarak_bpjs + 1;
                          $objset->setCellValue(alphabet($jarak_bpjs).$baris, $cicilan);
                          $jarak_bpjs = $jarak_bpjs + 1;
                          $objset->setCellValue(alphabet($jarak_bpjs).$baris, $keterangan);

                    if ($total_data == $no) {
                            $baris = $baris + 1;
                            $jarak_total = count($head_val) + $jumlah_range;
                            $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                            $jarak_total = $jarak_total + 1;
                            $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_makan);
                            $jarak_total = $jarak_total + $jumlah_range;
                            $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                            $jarak_total = $jarak_total + 1;
                            $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_lembur);
                            $jarak_total = $jarak_total + $jumlah_range;
                            $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                            $jarak_total = $jarak_total + 1;
                            $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_denda);
                    }
                     
                          $baris++;
                }
                    $i++;
            }
        }
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekapitulasi Absensi');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title_file.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function rekap_absen_excel()
    {
        $nip = $this->input->get('nip');
        $date_start = $this->input->get('tgl_awal');
        $date_end = $this->input->get('tgl_akhir');

        $nip = $this->db->escape_str($nip);
        $date_start = $this->db->escape_str($date_start);
        $date_end = $this->db->escape_str($date_end);

        $date_start = api_tgl($date_start);
        $date_end = api_tgl($date_end);

        $periode = tgl_indo($date_start).' sd '.tgl_indo($date_end);
        $title = 'Rekap Absensi '.$periode;
        $arr = explode(" ", $title);
        // $title_file = implode("_", $arr);
        $title_file = 'Rekap%20Absensi%20'.tgl_api($date_start).'%20sd%20'.tgl_api($date_end);

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
        $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
        $objPHPExcel->getProperties()->setTitle("Download Rekap Absensi");
        $objPHPExcel->getProperties()->setSubject("Download Rekap Absensi");
        $objPHPExcel->getProperties()->setDescription("Download Rekap Absensi");
        $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
        $objget->setTitle('Rekap Absensi'); //sheet title
        $objset->setCellValue('A1', 'Rekap Absensi '.$periode);

        $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

        $baris = 0;
        $telat = $this->Rekap_Absensi_Model->rekap_terlambat($date_start, $date_end, $nip);
        if ($telat->num_rows() > 0) {
            $baris = $baris + 5;

            $objset->setCellValue('A3', 'TERLAMBAT');
            $objset->setCellValue('A4', 'NO');
            $objset->setCellValue('B4', 'NIP');
            $objset->setCellValue('C4', 'TANGGAL');
            $objset->setCellValue('D4', 'JAM MASUK');
            $objset->setCellValue('E4', 'SHIFT');
            $objset->setCellValue('F4', 'SCAN MASUK');
            $objset->setCellValue('G4', 'TOTAL MENIT');

            $no = 1;
            foreach ($telat->result() as $row) {
                $objset->setCellValue("A".$baris, $no++);
                $objset->setCellValue("B".$baris, $row->nip);
                $objset->setCellValue("C".$baris, tanggal($row->tgl));
                $objset->setCellValue("D".$baris, $row->jam_masuk);
                $objset->setCellValue("E".$baris, $row->shift);
                $objset->setCellValue("F".$baris, $row->scan_masuk);
                $objset->setCellValue("G".$baris, $row->total_menit);
                $baris++;
            }

            $baris = $baris + 1;
        }

        $pulang_awal = $this->Rekap_Absensi_Model->rekap_pulang_awal($date_start, $date_end, $nip);
        if ($pulang_awal->num_rows() > 0) {
            $tambah = ($baris > 0) ? 1 : 5;
            $baris = $baris + $tambah;

            $objset->setCellValue('A'.$baris, 'PULANG AWAL');
            $baris = $baris + 1;

            $objset->setCellValue('A'.$baris, 'NO');
            $objset->setCellValue('B'.$baris, 'NIP');
            $objset->setCellValue('C'.$baris, 'TANGGAL');
            $objset->setCellValue('D'.$baris, 'JAM PULANG');
            $objset->setCellValue('E'.$baris, 'SHIFT');
            $objset->setCellValue('F'.$baris, 'SCAN PULANG');
            $objset->setCellValue('G'.$baris, 'STATUS');
            $baris = $baris + 1;

            $no = 1;
            foreach ($pulang_awal->result() as $row) {
                $objset->setCellValue("A".$baris, $no++);
                $objset->setCellValue("B".$baris, $row->nip);
                $objset->setCellValue("C".$baris, tanggal($row->tgl));
                $objset->setCellValue("D".$baris, $row->jam_pulang);
                $objset->setCellValue("E".$baris, $row->shift);
                $objset->setCellValue("F".$baris, $row->pulang);
                $objset->setCellValue("G".$baris, $row->status);
                $baris++;
            }

            $baris = $baris + 1;
        }

        $scan_masuk = $this->Rekap_Absensi_Model->rekap_scan_masuk($date_start, $date_end, $nip);
        if ($scan_masuk->num_rows() > 0) {
            $tambah = ($baris > 0) ? 1 : 5;
            $baris = $baris + $tambah;

            $objset->setCellValue('A'.$baris, 'TIDAK SCAN MASUK');
            $baris = $baris + 1;

            $objset->setCellValue('A'.$baris, 'NO');
            $objset->setCellValue('B'.$baris, 'NIP');
            $objset->setCellValue('C'.$baris, 'TANGGAL');
            $objset->setCellValue('D'.$baris, 'JAM MASUK');
            $objset->setCellValue('E'.$baris, 'SHIFT');
            $objset->setCellValue('F'.$baris, 'SCAN MASUK');
            $objset->setCellValue('G'.$baris, 'STATUS');
            $baris = $baris + 1;

            $no = 1;
            foreach ($scan_masuk->result() as $row) {
                $objset->setCellValue("A".$baris, $no++);
                $objset->setCellValue("B".$baris, $row->nip);
                $objset->setCellValue("C".$baris, tanggal($row->tgl));
                $objset->setCellValue("D".$baris, $row->jam_masuk);
                $objset->setCellValue("E".$baris, $row->shift);
                $objset->setCellValue("F".$baris, $row->masuk);
                $objset->setCellValue("G".$baris, $row->status);
                $baris++;
            }

            $baris = $baris + 1;
        }

        $scan_pulang = $this->Rekap_Absensi_Model->rekap_scan_pulang($date_start, $date_end, $nip);
        if ($scan_pulang->num_rows() > 0) {
            $tambah = ($baris > 0) ? 1 : 5;
            $baris = $baris + $tambah;

            $objset->setCellValue('A'.$baris, 'TIDAK SCAN PULANG');
            $baris = $baris + 1;

            $objset->setCellValue('A'.$baris, 'NO');
            $objset->setCellValue('B'.$baris, 'NIP');
            $objset->setCellValue('C'.$baris, 'TANGGAL');
            $objset->setCellValue('D'.$baris, 'JAM PULANG');
            $objset->setCellValue('E'.$baris, 'SHIFT');
            $objset->setCellValue('F'.$baris, 'SCAN PULANG');
            $objset->setCellValue('G'.$baris, 'STATUS');
            $baris = $baris + 1;

            $no = 1;
            foreach ($scan_pulang->result() as $row) {
                $objset->setCellValue("A".$baris, $no++);
                $objset->setCellValue("B".$baris, $row->nip);
                $objset->setCellValue("C".$baris, tanggal($row->tgl));
                $objset->setCellValue("D".$baris, $row->jam_pulang);
                $objset->setCellValue("E".$baris, $row->shift);
                $objset->setCellValue("F".$baris, $row->pulang);
                $objset->setCellValue("G".$baris, $row->status);
                $baris++;
            }

            $baris = $baris + 1;
        }
            
        $objPHPExcel->getActiveSheet()->setTitle('Rekap Absensi');
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename='.$title_file.'.xls');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    function rekap_absen()
    {
        $nip = $this->input->get('nip');
        $date_start = $this->input->get('tgl_awal');
        $date_end = $this->input->get('tgl_akhir');

        $nip = $this->db->escape_str($nip);
        $date_start = $this->db->escape_str($date_start);
        $date_end = $this->db->escape_str($date_end);

        $date_start = api_tgl($date_start);
        $date_end = api_tgl($date_end);

        $periode = tgl_indo($date_start).' sd '.tgl_indo($date_end);
        $title = 'Rekap Absensi '.$periode;
        $arr = explode(" ", $title);

        $title_file = 'Rekap%20Absensi%20'.tgl_api($date_start).'%20sd%20'.tgl_api($date_end);

        $b = $this->Rest_Model->biodata($nip);
        // biodata
        $nama = isset($b->nama) ? $b->nama : '';

        $table = '<center><h1 style="color:#265180"> Rekap Absensi '.$periode.' </h1></center><br><br>';
        $table .= ' <table>
                        <tr>
                            <td width="6%">NIK</td>
                            <td width="2%">:</td>
                            <td>'.$nip.'</td>
                        </tr>
                        <tr>
                            <td>NAMA</td>
                            <td>:</td>
                            <td>'.$nama.'</td>
                        </tr>
                    </table>';

        $telat = $this->Rekap_Absensi_Model->rekap_terlambat($date_start, $date_end, $nip);
        if ($telat->num_rows() > 0) {
            $table .= '<h2 style="color:#265180"> Terlambat </h2>';
            $table .= '<table class="table table-striped table-hover table-bordered tb_rekap">';
            $table .= '<thead><tr>
                            <th style="text-align: center; vertical-align: middle;">#</th>
                            <th style="text-align: center; vertical-align: middle;">NIK</th>
                            <th style="text-align: center; vertical-align: middle;">TANGGAL</th>
                            <th style="text-align: center; vertical-align: middle;">JAM MASUK</th>
                            <th style="text-align: center; vertical-align: middle;">SHIFT</th>
                            <th style="text-align: center; vertical-align: middle;">SCAN MASUK</th>
                            <th style="text-align: center; vertical-align: middle;">TOTAL MENIT</th> 
                       </tr></thead>';
            $table .= '<tbody>';
            $no = 1;
            foreach ($telat->result() as $row) {
                $table .= '<tr>
                                <td style="text-align: center; vertical-align: middle;">'.$no++.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->nip.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.tanggal($row->tgl).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.date('H:i', strtotime($row->jam_masuk)).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->shift.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.date('H:i', strtotime($row->scan_masuk)).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->total_menit.'</td>
                            </tr>';
            }
            $table .= '</tbody></table>';
        }

        $pulang_awal = $this->Rekap_Absensi_Model->rekap_pulang_awal($date_start, $date_end, $nip);
        if ($pulang_awal->num_rows() > 0) {
            $table .= '<h2 style="color:#265180"> Pulang Awal </h2>';
            $table .= '<table class="table table-striped table-hover table-bordered tb_rekap">';
            $table .= '<thead><tr>
                            <th style="text-align: center; vertical-align: middle;">#</th>
                            <th style="text-align: center; vertical-align: middle;">NIK</th>
                            <th style="text-align: center; vertical-align: middle;">TANGGAL</th>
                            <th style="text-align: center; vertical-align: middle;">JAM PULANG</th>
                            <th style="text-align: center; vertical-align: middle;">SHIFT</th>
                            <th style="text-align: center; vertical-align: middle;">SCAN PULANG</th>
                            <th style="text-align: center; vertical-align: middle;">STATUS</th> 
                       </tr></thead>';
            $table .= '<tbody>';
            $no = 1;
            foreach ($pulang_awal->result() as $row) {
                $table .= '<tr>
                                <td style="text-align: center; vertical-align: middle;">'.$no++.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->nip.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.tanggal($row->tgl).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.date('H:i', strtotime($row->jam_pulang)).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->shift.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->pulang.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->status.'</td>
                            </tr>';
            }
            $table .= '</tbody></table>';
        }

        $scan_masuk = $this->Rekap_Absensi_Model->rekap_scan_masuk($date_start, $date_end, $nip);
        if ($scan_masuk->num_rows() > 0) {
            $table .= '<h2 style="color:#265180"> Tidak Absen Masuk </h2>';
            $table .= '<table class="table table-striped table-hover table-bordered tb_rekap">';
            $table .= '<thead><tr>
                            <th style="text-align: center; vertical-align: middle;">#</th>
                            <th style="text-align: center; vertical-align: middle;">NIK</th>
                            <th style="text-align: center; vertical-align: middle;">TANGGAL</th>
                            <th style="text-align: center; vertical-align: middle;">JAM MASUK</th>
                            <th style="text-align: center; vertical-align: middle;">SHIFT</th>
                            <th style="text-align: center; vertical-align: middle;">SCAN MASUK</th>
                            <th style="text-align: center; vertical-align: middle;">STATUS</th> 
                       </tr></thead>';
            $table .= '<tbody>';
            $no = 1;
            foreach ($scan_masuk->result() as $row) {
                $table .= '<tr>
                                <td style="text-align: center; vertical-align: middle;">'.$no++.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->nip.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.tanggal($row->tgl).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.date('H:i', strtotime($row->jam_masuk)).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->shift.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->masuk.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->status.'</td>
                            </tr>';
            }
            $table .= '</tbody></table>';
        }

        $scan_pulang = $this->Rekap_Absensi_Model->rekap_scan_pulang($date_start, $date_end, $nip);
        if ($scan_pulang->num_rows() > 0) {
            $table .= '<h2 style="color:#265180"> Tidak Absen Pulang </h2>';
            $table .= '<table class="table table-striped table-hover table-bordered tb_rekap">';
            $table .= '<thead><tr>
                            <th style="text-align: center; vertical-align: middle;">#</th>
                            <th style="text-align: center; vertical-align: middle;">NIK</th>
                            <th style="text-align: center; vertical-align: middle;">TANGGAL</th>
                            <th style="text-align: center; vertical-align: middle;">JAM PULANG</th>
                            <th style="text-align: center; vertical-align: middle;">SHIFT</th>
                            <th style="text-align: center; vertical-align: middle;">SCAN PULANG</th>
                            <th style="text-align: center; vertical-align: middle;">STATUS</th> 
                       </tr></thead>';
            $table .= '<tbody>';
            $no = 1;
            foreach ($scan_pulang->result() as $row) {
                $table .= '<tr>
                                <td style="text-align: center; vertical-align: middle;">'.$no++.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->nip.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.tanggal($row->tgl).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.date('H:i', strtotime($row->jam_pulang)).'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->shift.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->pulang.'</td>
                                <td style="text-align: center; vertical-align: middle;">'.$row->status.'</td>
                            </tr>';
            }
            $table .= '</tbody></table>';
        }

        $data = array(
            'html' => $table,
            'periode' => $periode
        );

        $this->load->view('file_download/rekap_absen', $data);
        $filename = 'Rekap Absensi '.$periode.'.pdf';
        $html = $this->output->get_output();
        $this->load->library('pdfgenerator');
        $this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
    }

    function lemburan_divisi()
    {
        $this->Main_Model->all_login();
        $cabang = $this->input->get('cabang');
        $date_start = $this->input->get('date_start');
        $date_end = $this->input->get('date_end');
        $divisi = $this->input->get('divisi');
        $date_start = $this->Main_Model->convert_tgl($date_start);
        $date_end = $this->Main_Model->convert_tgl($date_end);

        $title = 'Rekap Lembur '.$date_start.' s/d '.$date_end;
        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);
        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

        $objPHPExcel = new PHPExcel();
        // Set properties
        $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
        $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
        $objPHPExcel->getProperties()->setTitle("Download Rekap Lembur");
        $objPHPExcel->getProperties()->setSubject("Download Rekap Lembur");
        $objPHPExcel->getProperties()->setDescription("Download Rekap Lembur");

        $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
        $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object

        $objget->setTitle('Rekap Lembur'); //sheet title
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
        $objset->setCellValue('A1', $title);

        $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                  )
              )
        );

        //table header
        $cols = array("A","B","C","D","E","F","G","H","I","J","K","L");
        $val = array("#","NIP","NAMA","CABANG","DIVISI","TANGGAL", "SHIFT", "ABSENSI", "JAM", "PER JAM", "TOTAL", "KETERANGAN");
            
        for ($a = 0; $a < count($cols); $a++) {
            $objset->setCellValue($cols[$a].'2', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25);

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }

        $condition = [];
        if ($divisi == 0) {
            $condition = ['status' => 1];
        } else {
            $condition = ['id_divisi' => $divisi];
        }

             
        $baris  = 3;
        $i = 1;
        $ms_divisi = $this->Main_Model->view_by_id('ms_divisi', $condition, 'result');
        if ($ms_divisi) {
            foreach ($ms_divisi as $row) {
                $objset->setCellValue("A".$baris, $row->divisi);
                $objPHPExcel->getActiveSheet()->mergeCells('A'.$baris.':L'.$baris);
                $baris++;
                $lembur = $this->Tunjangan_Model->lemburan_divisi($cabang, $row->id_divisi, $date_start, $date_end);
                foreach ($lembur as $val) {
                    $ovr_nominal = isset($val->ovr_nominal) ? $val->ovr_nominal : 0;
                    $id_overtime = isset($val->id_overtime) ? $val->id_overtime : 0;

                    if ($id_overtime == 0) {
                        $ovr_nominal = 0;
                    }

                    $hari = hari(date('w', strtotime($val->tgl)), 2);

                    $objset->setCellValue("A".$baris, $i++);
                    $objset->setCellValue("B".$baris, $val->nip);
                    $objset->setCellValue("C".$baris, $val->nama);
                    $objset->setCellValue("D".$baris, $val->cabang);
                    $objset->setCellValue("E".$baris, $val->divisi);
                    $objset->setCellValue("F".$baris, $hari.', '.tanggal($val->tgl));
                    $objset->setCellValue("G".$baris, $val->shift);
                    $objset->setCellValue("H".$baris, $val->absensi);
                    $objset->setCellValue("I".$baris, $val->jam);
                    $objset->setCellValue("J".$baris, $ovr_nominal);
                    $objset->setCellValue("K".$baris, $val->lembur);
                    $objset->setCellValue("L".$baris, $val->keterangan);

                    $baris++;
                }
            } 
        }

        $b = $baris - 1;
        $objPHPExcel->getActiveSheet()->getStyle('A2:L'.$b)->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setTitle('Rekap Lembur');


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename=Rekap Lembur '.$date_start.' '.$date_end.'.xls');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    function alphabet($jumlah = '')
    {
        switch ($jumlah) {
            case '1':
                $result = 'A';
                break;
            case '2':
                $result = 'B';
                break;
            case '3':
                $result = 'C';
                break;
            case '4':
                $result = 'D';
                break;
            case '5':
                $result = 'E';
                break;
            case '6':
                $result = 'F';
                break;
            case '7':
                $result = 'G';
                break;
            case '8':
                $result = 'H';
                break;
            case '9':
                $result = 'I';
                break;
            case '10':
                $result = 'J';
                break;
            case '11':
                $result = 'K';
                break;
            case '12':
                $result = 'L';
                break;
            case '13':
                $result = 'M';
                break;
            case '14':
                $result = 'N';
                break;
            case '15':
                $result = 'O';
                break;
            case '16':
                $result = 'P';
                break;
            case '17':
                $result = 'Q';
                break;
            case '18':
                $result = 'R';
                break;
            case '19':
                $result = 'S';
                break;
            case '20':
                $result = 'T';
                break;
            case '21':
                $result = 'U';
                break;
            case '22':
                $result = 'V';
                break;
            case '23':
                $result = 'W';
                break;
            case '24':
                $result = 'X';
                break;
            case '25':
                $result = 'Y';
                break;
            case '26':
                $result = 'Z';
                break;
            default:
                $result = '';
                break;
        }
        return $result;
    }

    function convert_bulan($a = '')
    {
        switch ($a) {
            case '01':
                $bulan = 'Januari';
                break;
            case '02':
                $bulan = 'Februari';
                break;
            case '03':
                $bulan = 'Maret';
                break;
            case '04':
                $bulan = 'April';
                break;
            case '05':
                $bulan = 'Mei';
                break;
            case '06':
                $bulan = 'Juni';
                break;
            case '07':
                $bulan = 'Juli';
                break;
            case '08':
                $bulan = 'Agustus';
                break;
            case '09':
                $bulan = 'September';
                break;
            case '10':
                $bulan = 'Oktober';
                break;
            case '11':
                $bulan = 'November';
                break;
            case '12':
                $bulan = 'Desember';
                break;
            default:
                $bulan = '';
                break;
        }
        return $bulan;
    }
}

/* End of file download_excel.php */
/* Location: ./application/controllers/download_excel.php */
