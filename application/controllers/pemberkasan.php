<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pemberkasan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
	}

	function data_pemberkasan($nip='')
	{
		$this->Main_Model->get_login();
		$javascript = '';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','69'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
			);
		$pemberkasan = $this->Karyawan_Model->view_pemberkasan($nip);
		$lamaran = isset($pemberkasan->lamaran) ? $pemberkasan->lamaran : '';
		$ojt = isset($pemberkasan->ojt) ? $pemberkasan->ojt : '';
		$pertanggungjawaban = isset($pemberkasan->pertanggungjawaban) ? $pemberkasan->pertanggungjawaban : '';
		$kesanggupan = isset($pemberkasan->kesanggupan) ? $pemberkasan->kesanggupan : '';
		$ijazah = isset($pemberkasan->ijazah) ? $pemberkasan->ijazah : '';
		$upah = isset($pemberkasan->upah) ? $pemberkasan->upah : '';
		$rekening = isset($pemberkasan->rekening) ? $pemberkasan->rekening : '';

		$file_lamaran = isset($pemberkasan->file_lamaran) ? $pemberkasan->file_lamaran : '';
		$file_ojt = isset($pemberkasan->file_ojt) ? $pemberkasan->file_ojt : '';
		$file_pertanggungjawaban = isset($pemberkasan->file_pertanggungjawaban) ? $pemberkasan->file_pertanggungjawaban : '';
		$file_kesanggupan = isset($pemberkasan->file_kesanggupan) ? $pemberkasan->file_kesanggupan : '';
		$file_ijazah = isset($pemberkasan->file_ijazah) ? $pemberkasan->file_ijazah : '';
		$file_upah = isset($pemberkasan->file_upah) ? $pemberkasan->file_upah : '';
		$file_rekening = isset($pemberkasan->file_rekening) ? $pemberkasan->file_rekening : '';

		$data = array(
			'lamaran' => $lamaran,
			'ojt' => $ojt,
			'pertanggungjawaban' => $pertanggungjawaban,
			'kesanggupan' => $kesanggupan,
			'ijazah' => $ijazah,
			'upah' => $upah,
			'rekening' => $rekening,
			'file_lamaran' => $file_lamaran,
			'file_ojt' => $file_ojt,
			'file_pertanggungjawaban' => $file_pertanggungjawaban,
			'file_kesanggupan' => $file_kesanggupan,
			'file_ijazah' => $file_ijazah,
			'file_upah' => $file_upah,
			'file_rekening' => $file_rekening,
			'action' => base_url('pemberkasan/process_pemberkasan').'/'.$nip
			);

		$this->load->view('template/header', $header);
        $this->load->view('karyawan/data_pemberkasan', $data);
        $this->load->view('template/footer', $footer);
	}

	function process_pemberkasan($nip='')
	{
		$this->Main_Model->get_login();
		$checkKaryawan = $this->Main_Model->kary_nip($nip);

		$a 	= 'Lamaran '.$nip.' '.date("Y-m-d");
		$config_a 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$a);
		// $this->upload->initialize($config_a);
		$this->load->library('upload', $config_a, 'lamaran');

		$b 	= 'OJT '.$nip.' '.date("Y-m-d");
		$config_b 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$b);
		// $this->upload->initialize($config_b);
		$this->load->library('upload', $config_b, 'ojt');

		$c 	= 'Pertanggungjawaban '.$nip.' '.date("Y-m-d");
		$config_c 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$c);
		// $this->upload->initialize($config_c);
		$this->load->library('upload', $config_c, 'pertanggungjawaban');

		$d 	= 'Ijazah '.$nip.' '.date("Y-m-d");
		$config_d 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$d);
		// $this->upload->initialize($config_d);
		$this->load->library('upload', $config_d, 'ijazah');

		$e 	= 'Upah '.$nip.' '.date("Y-m-d");
		$config_e 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$e);
		// $this->upload->initialize($config_e);
		$this->load->library('upload', $config_e, 'upah');

		$f 	= 'Kesanggupan '.$nip.' '.date("Y-m-d");
		$config_f 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$f);
		// $this->upload->initialize($config_f);
		$this->load->library('upload', $config_f, 'kesanggupan');

		$g 	= 'Rekening '.$nip.' '.date("Y-m-d");
		$config_g 	= $this->Main_Model->set_upload_options("./assets/pemberkasan/","*","5048000",$g);
		// $this->upload->initialize($config_g);
		$this->load->library('upload', $config_g, 'rekening');

		if($checkKaryawan)
		{
			$data = array(
				'lamaran' => $this->input->post('lamaran'),
				'ojt' => $this->input->post('ojt'),
				'pertanggungjawaban' => $this->input->post('pertanggungjawaban'),
				'ijazah' => $this->input->post('ijazah'),
				'upah' => $this->input->post('upah'),
				'kesanggupan' => $this->input->post('kesanggupan'),
				'rekening' => $this->input->post('rekening')
				);

			if($this->lamaran->do_upload('file_lamaran'))
			{
				$file_a = $this->lamaran->data();
				$data['file_lamaran'] = $file_a['file_name'];
			}

			if($this->ojt->do_upload('file_ojt'))
			{
				$file_b = $this->ojt->data();
				$data['file_ojt'] = $file_b['file_name'];
			}

			if($this->pertanggungjawaban->do_upload('file_pertanggungjawaban'))
			{
				$file_c = $this->pertanggungjawaban->data();
				$data['file_pertanggungjawaban'] = $file_c['file_name'];
			}

			if($this->ijazah->do_upload('file_ijazah'))
			{
				$file_d = $this->ijazah->data();
				$data['file_ijazah'] = $file_d['file_name'];
			}

			if($this->upah->do_upload('file_upah'))
			{
				$file_e = $this->upah->data();
				$data['file_upah'] = $file_e['file_name'];
			}

			if($this->kesanggupan->do_upload('file_kesanggupan'))
			{
				$file_f = $this->kesanggupan->data();
				$data['file_kesanggupan'] = $file_f['file_name'];
			}

			if($this->rekening->do_upload('file_rekening'))
			{
				$file_g = $this->rekening->data();
				$data['file_rekening'] = $file_g['file_name'];
			}


			$q = $this->Karyawan_Model->view_pemberkasan($nip);
			if($q)
			{
				$this->Karyawan_Model->process_pemberkasan($data, $nip);
			}
			else
			{
				$data['nip'] = $nip;
				$this->Karyawan_Model->process_pemberkasan($data);
			}
			$this->session->set_flashdata('message_upload', '<script>bootbox.alert("Success!");</script');
		}
		else
		{
			$this->session->set_flashdata('message_upload', '<script>bootbox.alert("Gagal Menyimpan data!");</script');
		}
		redirect('pemberkasan/data_pemberkasan/'.$nip, 'refresh');
	}

}

/* End of file pemberkasan.php */
/* Location: ./application/controllers/pemberkasan.php */ ?>