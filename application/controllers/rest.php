<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model');
        $this->load->model('Rest_Model');
        $this->load->model('Rekap_Absensi_Model');
        $this->load->model('Cuti_Model');
        $this->load->model('Shift_Model');
        $this->load->model('Reminder_Model');
        header('Content-Type: application/json');
        header('Access-Control-Allow-Origin: *');
        date_default_timezone_set('Asia/Jakarta');
    }

    function authentication()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        $username = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $params = json_decode(file_get_contents('php://input'), true);
                $username = isset($params['username']) ? $params['username'] : '';
                $password = isset($params['password']) ? $params['password'] : '';

                $response = $this->Rest_Model->login($username, $password);
            }
        }

        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function scan()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $flag = isset($params['flag']) ? $params['flag'] : 0;
                    $mac = isset($params['macaddress']) ? $params['macaddress'] : '';
                    $ip = isset($params['ippublic']) ? $params['ippublic'] : '';
                    $imei = isset($params['imei']) ? $params['imei'] : '';
                    $nip = nip_api();

                    $today = today();
                    $now = now();

                    $q = $this->db->query("
							SELECT * 
							FROM tb_scanlog 
							WHERE DATE(scan_date) = '$today'")->row();

                    $scanlog = array(
                            'scan_date' => $now,
                            'nip' => $nip,
                            'insert_at' => $now,
                            'flag' => $flag,
                            'keterangan' => 'Via App'
                        );

                    // $shift = $this->Shift_Model->jadwal($today, $nip);
                    // $jam_masuk = isset($shift->jam_masuk) ? $shift->jam_masuk : 0;
                    // $jadwal_masuk = $today.' '.$jam_masuk;
                    // $valid_pulang = date('Y-m-d H:i:s', strtotime('+ 4 hours', strtotime($jadwal_masuk)));

                    // print_r($valid_pulang); exit;

                    $check_ip = $this->Rest_Model->check_ip($ip, $mac);
                    $check_imei = $this->Rest_Model->check_imei($nip, $imei);
                    // $check_imei = TRUE;

                    if ($check_ip) {
                        if ($check_imei == true) {
                            // if ($flag == 1) {
                            //  if (strtotime($valid_pulang) <= strtotime($now)) {
                            //      $response = $this->Rest_Model->check_in($scanlog, $flag);
                            //      eksekusi_absen($today, $today, $nip);
                            //  } else {
                            //      $response = array(
                            //          'response' => '',
                            //          'metadata' => array(
                            //              'status' => 0,
                            //              'message' => 'Check Out Failed, Check out bisa dilakukan setelah 4 jam bekerja'
                            //          )
                            //      );
                            //  }
                            // } else {
                            //  $response = $this->Rest_Model->check_in($scanlog, $flag);
                            //  eksekusi_absen($today, $today, $nip);
                            // }

                            $response = $this->Rest_Model->check_in($nip, $flag);
                            eksekusi_absen($today, $today, $nip);
                        } else {
                            $message = ($flag == 0) ? 'Check in' : 'Check out';
                            $response = array(
                                'response' => '',
                                'metadata' => array(
                                    'status' => 0,
                                    'message' => $message.' Failed, Device tidak terdaftar'
                                )
                            );
                        }
                    } else {
                        $message = ($flag == 0) ? 'Check in' : 'Check out';
                        $response = array(
                            'response' => '',
                            'metadata' => array(
                                'status' => 0,
                                'message' => $message.' Failed, Pastikan anda '.$message.' dijaringan yang telah ditentukan'
                            )
                        );
                    }
                }
            }
        }

        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function uang()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $nip = nip_api();
                    $today = today();
                    $m = date('m');
                    $y = date('Y');

                    $startdate = isset($params['startdate']) ? $params['startdate'] : '';
                    $enddate = isset($params['enddate']) ? $params['enddate'] : '';
                    $type = isset($params['type']) ? $params['type'] : '';

                    $periode = periode_sekarang();
                    $awal = isset($periode['tgl_awal']) ? $periode['tgl_awal'] : '';
                    $akhir = isset($periode['tgl_akhir']) ? $periode['tgl_akhir'] : '';

                    // jika startdate kosong
                    if ($startdate == '') {
                        // $startdate = $y.'-'.$m.'-01';
                        $startdate = $awal;
                    }

                    // jika enddate kosong
                    if ($enddate == '') {
                        // $enddate = $this->Main_Model->last_day($m, $y);
                        $enddate = $akhir;
                    }

                    $arr_type = array('makan', 'lembur');
                    $date_range = date_range($startdate, $enddate);
                    $arr_data = array();
                    if (!empty($date_range) && in_array($type, $arr_type)) {
                        for ($i = 0; $i < count($date_range); $i++) {
                            $no = $i + 1;
                            $tgl = explode(' s/d ', $date_range[$i]);

                            // tanggal
                            $awal = $tgl[0];
                            $akhir = end($tgl);

                            // data yg akan dipush ke array
                            $minggu = 'Minggu Ke-'.$no;
                            $format_tgl = tanggal($awal).' - '.tanggal($akhir);

                            $detail = array();
                            if ($type == 'makan') {
                                $q = $this->Rekap_Absensi_Model->uang_makan($awal, $akhir, $nip);
                                $nominal = isset($q->uang_makan) ? $q->uang_makan : 0;
                            } else {
                                $q = $this->Rekap_Absensi_Model->uang_lembur($awal, $akhir, $nip);
                                $nominal = isset($q->uang_lembur) ? $q->uang_lembur : 0;

                                $rd = range_to_date($awal, $akhir);
                                for ($a = 0; $a < count($rd); $a++) {
                                    $hari = hari(date('w', strtotime($rd[$a])));
                                    $tanggal = tanggal($rd[$a]);
                                    $w = $this->Rekap_Absensi_Model->uang_lembur($rd[$a], $rd[$a], $nip);
                                    $keterangan = isset($w->keterangan) ? $w->keterangan : '';
                                    if ($keterangan != '') {
                                        $hasil = isset($w->uang_lembur) ? $w->uang_lembur : 0;

                                        $detail[] = array(
                                            'hari' => $hari.', '.$tanggal,
                                            'nominal' => $hasil
                                        );
                                    }
                                }
                            }

                            $arr_data[$i] = array(
                                'minggu' => $minggu,
                                'range' => $format_tgl,
                                'tgl_awal' => tanggal($awal),
                                'tgl_akhir' => tanggal($akhir),
                                'nominal' => $nominal
                            );
                            if ($type == 'lembur') {
                                $arr_data[$i]['detail'] = $detail;
                            }
                            $code = 1;
                            $message = 'Success!';
                        }
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }
                    $response = array(
                        'response' => $arr_data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function lembur_detail()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);

                    $startdate = isset($params['startdate']) ? $params['startdate'] : '';
                    $enddate = isset($params['enddate']) ? $params['enddate'] : '';
                    $nip = nip_api();

                    $arr_tgl = array();
                    $arr_data = array();
                    if ($startdate != '' && $enddate != '') {
                        if ($startdate > $enddate) {
                            $code = 0;
                            $message = 'Format tanggal salah!';
                        } else {
                            $today = today();
                            $endlembur = $enddate;
                            if (strtotime($enddate) >= strtotime($today)) {
                                $endlembur = $today;
                            }

                            $arr_tgl = range_to_date($startdate, $endlembur);
                            for ($i = 0; $i < count($arr_tgl); $i++) {
                                $hari = hari(date('w', strtotime($arr_tgl[$i])));
                                $tanggal = tanggal($arr_tgl[$i]);
                                $q = $this->Rekap_Absensi_Model->uang_lembur($tanggal, $tanggal, $nip);
                                $nominal = isset($q->uang_lembur) ? $q->uang_lembur : 0;

                                $arr_data[$i] = array(
                                    'hari' => $hari.', '.$tanggal,
                                    'nominal' => $nominal
                                );
                            }
                            $code = 1;
                            $message = 'Success!';
                        }
                    } else {
                        $code = 0;
                        $message = 'Masukkan Range tanggal!';
                    }
                    $response = array(
                        'response' => $arr_data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function rekap_absen()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);

                    $startdate = isset($params['startdate']) ? $params['startdate'] : '';
                    $enddate = isset($params['enddate']) ? $params['enddate'] : '';
                    $nip = nip_api();

                    $arr_tgl = array();
                    $arr_data = array();
                    if ($startdate != '' && $enddate != '') {
                        $startdate = api_tgl($startdate);
                        $enddate = api_tgl($enddate);
                        if ($startdate > $enddate) {
                            $code = 0;
                            $message = 'Format tanggal salah!';
                        } else {
                            while (strtotime($startdate) <= strtotime($enddate)) {
                                $arr_tgl[] = $startdate;
                                $startdate = date("Y-m-d", strtotime("+1 day", strtotime($startdate)));
                            }

                            for ($i = 0; $i < count($arr_tgl); $i++) {
                                $hari = hari(date('w', strtotime($arr_tgl[$i])));
                                $tanggal = tanggal($arr_tgl[$i]);
                                $q = $this->Rekap_Absensi_Model->rekap_absensi($nip, $arr_tgl[$i]);
                                $jam_masuk = isset($q->masuk) ? $q->masuk : '';
                                $jam_pulang = isset($q->pulang) ? $q->pulang : '';
                                $telat = isset($q->total_menit) ? $q->total_menit : 0;
                                $flag_libur = isset($q->flag_libur) ? $q->flag_libur : 0;

                                $arr_data[$i] = array(
                                    'hari' => $hari.', '.$tanggal,
                                    'jam_masuk' => $jam_masuk,
                                    'jam_pulang' => $jam_pulang,
                                    'telat' => $telat,
                                    'flag_libur' => $flag_libur
                                );
                            }
                            $code = 1;
                            $message = 'Success!';
                        }
                    } else {
                        $code = 0;
                        $message = 'Masukkan Range tanggal!';
                    }
                    $response = array(
                        'response' => $arr_data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function cuti()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);

                    $startdate = isset($params['startdate']) ? $params['startdate'] : '';
                    $enddate = isset($params['enddate']) ? $params['enddate'] : '';
                    $keterangan = isset($params['keterangan']) ? $params['keterangan'] : '';
                    $approval = isset($params['approval']) ? $params['approval'] : '';
                    $id = isset($params['id']) ? $params['id'] : '';
                    $nip = nip_api();

                    // cari nama
                    $karyawan = $this->Main_Model->kary_nip($nip);
                    $nama = isset($karyawan->nama) ? $karyawan->nama : '';


                    if ($startdate != '' && $enddate != '' && $keterangan != '' && $approval != '') {
                        $startdate = api_tgl($startdate);
                        $enddate = api_tgl($enddate);
                        if ($startdate > $enddate) {
                            $code = 0;
                            $message = 'Format tanggal salah!';
                        } else {
                            $today = today();
                            $min_tanggal = date('Y-m-d', strtotime('-2 day', strtotime($startdate)));
                            if (strtotime($min_tanggal) > strtotime($today)) {
                                while (strtotime($startdate) <= strtotime($enddate)) {
                                    $arr_tgl[] = $startdate;
                                    $startdate = date("Y-m-d", strtotime("+1 day", strtotime($startdate)));
                                }

                                $jumlah = count($arr_tgl);
                                $sisa_cuti = sisa_cuti($nip);
                                if ($sisa_cuti >= $jumlah) {
                                    // generate nomor cuti
                                    $q = $this->Cuti_Model->generate_nomor_cuti();
                                    $id_cuti = isset($q->maks) ? $q->maks : 0;

                                    $time = 'insert_at';
                                    $user = 'user_insert';
                                    $update = array();
                                    if ($id != '') {
                                        $id_cuti = $id;
                                        $condition = array('id_cuti_det' => $id_cuti);
                                        $this->Main_Model->delete_data('cuti_sub_det', $condition);
                                        $update = array(
                                            'id_cuti_det' => $id_cuti
                                        );

                                        $time = 'update_at';
                                        $user = 'user_update';
                                    }

                                    // cari kode approval
                                    // $k = $this->Main_Model->kode_approval($nip, 'cuti');
                                    // $approval = isset($k->kode) ? $k->kode : 6;
                                    $pola = $this->Main_Model->kode_penyetuju($approval);
                                    $kode_pola = isset($pola->pola) ? $pola->pola : '';
                                    $approval = isset($pola->kode) ? $pola->kode : '';
                                    $penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
                                    $cc = isset($pola->cc) ? $pola->cc : '';

                                    // cari fcm id
                                    $fcm = $this->Main_Model->cari_fcm_id($penyetuju);
                                    $usr_id = isset($fcm->usr_id) ? $fcm->usr_id : '';
                                    $fcm_id = isset($fcm->fcm_id) ? $fcm->fcm_id : '';

                                    $data = array(
                                        'id_cuti_det' => $id_cuti,
                                        'nip' => $nip,
                                        'alasan_cuti' => $keterangan,
                                        'alamat_cuti' => '',
                                        'no_hp' => '',
                                        'id_tipe' => 1,
                                        'pola' => $kode_pola,
                                        'penyetuju' => $penyetuju,
                                        'cc' => $cc,
                                        $time => now(),
                                        $user => $nip,
                                        'approval' => $approval
                                    );

                                    $d = 0;
                                    $detail = array();
                                    for ($i = 0; $i < $jumlah; $i++) {
                                        if ($arr_tgl[$i] != '') {
                                            $detail[$i] = array(
                                                'id_cuti_det' => $id_cuti,
                                                'tgl' => $arr_tgl[$i]
                                                );
                                            $d = $this->Main_Model->process_data('cuti_sub_det', $detail[$i]);
                                            $d = $d + $d;
                                        }
                                    }

                                    if ($d == 0) {
                                        $code = 0;
                                        $message = 'Gagal menyimpan cuti';
                                        $condition = array('id_cuti_det' => $id_cuti);
                                        $this->Main_Model->delete_data('cuti_sub_det', $condition);
                                    } else {
                                        $mail = $this->Rest_Model->reminder_cuti($data, $arr_tgl);

                                        if (!empty($mail)) {
                                            $code = 1;
                                            $message = (! empty($update)) ? 'Update Cuti berhasil' : 'Pengajuan Cuti berhasil';
                                            $simpan = $this->Main_Model->process_data('cuti_det', $data, $update);

                                            // send notification
                                            $title_notif = 'Pengajuan Cuti';
                                            $text_notif = $nama.' telah mengajukan cuti.';
                                            $data_index = ['tipe'];
                                            $data_value = ['cuti'];
                                            $this->Rest_Model->notif_fcm($title_notif, $text_notif, $fcm_id, $usr_id, 'ACT_NOTIF', $data_index, $data_value);
                                        } else {
                                            $code = 0;
                                            $message = 'Pengajuan Cuti Gagal, Email tidak terkirim';
                                            $this->Main_Model->delete_data('cuti_sub_det', array('id_cuti_det' => $id_cuti));
                                        }
                                    }
                                } else {
                                    $code = 0;
                                    $message = 'Kuota Cuti tidak cukup!';
                                }
                            } else {
                                $code = 0;
                                $message = 'Pengajuan Cuti Minimal H-3';
                            }
                        }
                    } else {
                        $code = 0;
                        $message = 'Masukkan ';
                        if ($startdate == '') {
                            $message .= 'Tanggal Mulai \n';
                        }

                        if ($enddate == '') {
                            $message .= 'Tanggal Selesai \n';
                        }

                        if ($keterangan == '') {
                            $message .= 'Keterangan Cuti \n';
                        }

                        if ($approval == '') {
                            $message .= 'Approval';
                        }
                    }

                    $response = array(
                        'response' => '',
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function batal_cuti()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $id_cuti = isset($params['id_cuti']) ? $params['id_cuti'] : '';
                    $nip = nip_api();

                    if ($id_cuti != '') {
                        $data = array('approval' => 14);
                        $condition = array('id_cuti_det' => $id_cuti);
                        $simpan = $this->Main_Model->process_data('cuti_det', $data, $condition);
                        if ($simpan > 0) {
                            $code = 1;
                            $message = 'Success!';

                            // reminder batal cuti
                            $remind = $this->Rest_Model->reminder_batal_cuti($id_cuti);
                        } else {
                            $code = 0;
                            $message = 'Gagal menyimpan cuti';
                        }
                    } else {
                        $code = 0;
                        $message = 'Invalid id cuti';
                    }
                    $response = array(
                        'response' => '',
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function view_cuti()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $flag = isset($params['flag']) ? $params['flag'] : 0;
                    $id = isset($params['id']) ? $params['id'] : '';

                    $q = $this->Rest_Model->view_cuti($flag, $id);
                    $cuti = array();
                    if (!empty($q)) {
                        $no = 1;
                        foreach ($q as $row) {
                            $hari_mulai = hari(date('w', strtotime($row->mulai)));
                            $hari_selesai = hari(date('w', strtotime($row->selesai)));
                            $tgl_mulai = tanggal($row->mulai);
                            $tgl_selesai = tanggal($row->selesai);
                            $cuti[] = array(
                                'no' => $no++,
                                'id_cuti' => $row->id_cuti_det,
                                'approval' => $row->pola,
                                'mulai' => $hari_mulai.', '.$tgl_mulai,
                                'selesai' => $hari_selesai.', '.$tgl_selesai,
                                'tgl_mulai' => tgl_api($row->mulai),
                                'tgl_selesai' => tgl_api($row->selesai),
                                'alasan' => $row->alasan_cuti,
                                'status' => $row->keterangan
                            );
                        }
                        $code = 1;
                        $message = '';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $cuti,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function sisa_cuti()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $sisa_cuti = sisa_cuti($nip);
                    $code = 1;
                    $message = 'Success!';

                    $response = array(
                        'response' => array(
                            'sisa_cuti' => $sisa_cuti
                        ),
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function dashboard()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $imei = isset($params['imei']) ? $params['imei'] : '';
                    $nip_req = isset($params['nip']) ? $params['nip'] : '';
                    $fcm_id = isset($params['fcm_id']) ? $params['fcm_id'] : '';

                    if ($fcm_id != '') {
                        $this->Main_Model->process_data('kary', array('fcm_id' => $fcm_id), array('nip' => $nip_req));
                    }
                    
                    // $arr_imei = explode(',', $imei);
                    // if (! empty($arr_imei)) {
                    //  for ($i = 0; $i < count($arr_imei); $i++) {
                    //      $imei_string = isset($arr_imei[$i]) ? $arr_imei[$i] : '';
                    //      $imei_string = str_replace('[', '', $imei_string);
                    //      $imei_string = str_replace(']', '', $imei_string);
                    //      $imei_string = str_replace(' ', '', $imei_string);
                            
                    //      $data = array(
                    //          'nip' => $nip_req,
                    //          'imei' => $imei_string
                    //      );

                    //      $cek = $this->Main_Model->view_by_id('tb_imei', array('imei' => $imei_string, 'nip' => $nip_req), 'row');
                    //      if (empty($cek)) {
                    //          $this->Main_Model->process_data('tb_imei', $data);
                    //      }
                    //  }
                    // }

                    $nip = nip_api();
                    $today = today();
                    $m = date('m');
                    $y = date('Y');

                    $startdate = isset($params['startdate']) ? $params['startdate'] : '';
                    $enddate = isset($params['enddate']) ? $params['enddate'] : '';

                    $periode = periode_sekarang();
                    $awal = isset($periode['tgl_awal']) ? $periode['tgl_awal'] : '';
                    $akhir = isset($periode['tgl_akhir']) ? $periode['tgl_akhir'] : '';

                    // jika startdate kosong
                    if ($startdate == '') {
                        // $startdate = $y.'-'.$m.'-01';
                        $startdate = $awal;
                    }

                    // jika enddate kosong
                    if ($enddate == '') {
                        // $enddate = $this->Main_Model->last_day($m, $y);
                        $enddate = $akhir;
                    }

                    // sisa cuti
                    $sisa_cuti = sisa_cuti($nip);
                    $arr_cuti = array(
                        'label' => 'Sisa Cuti',
                        'value' => $sisa_cuti.' Hari'
                    );

                    // terlambat
                    $t = $this->Rekap_Absensi_Model->terlambat($startdate, $enddate, $nip);
                    $terlambat = isset($t->jml) ? $t->jml : 0;
                    $arr_telat = array(
                        'label' => 'Total Terlambat',
                        'value' => $terlambat.' Hari'
                    );

                    // uang makan
                    $endlembur = $enddate;
                    if (strtotime($enddate) >= strtotime($today)) {
                        $endlembur = $today;
                    }
                    $m = $this->Rekap_Absensi_Model->uang_makan($startdate, $endlembur, $nip);
                    $uang_makan = isset($m->uang_makan) ? $m->uang_makan : 0;
                    $arr_makan = array(
                        'label' => 'Total Uang Makan',
                        'value' => format_dashboard($uang_makan)
                    );

                    // uang lembur
                    $l = $this->Rekap_Absensi_Model->uang_lembur($startdate, $enddate, $nip);
                    $uang_lembur = isset($l->uang_lembur) ? $l->uang_lembur : 0;
                    $arr_lembur = array(
                        'label' => 'Total Uang Lembur',
                        'value' => format_dashboard($uang_lembur)
                    );

                    # pin gaji 
                    $pin = $this->Rest_Model->flag_pin($nip);

                    $arr = array($arr_cuti, $arr_telat, $arr_makan, $arr_lembur, ['flag_pin' => $pin]);
                    $code = 1;
                    $message = 'Success!';

                    $response = array(
                        'response' => $arr,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function biodata()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $code = 1;
                    $message = 'Success!';

                    $b = $this->Rest_Model->biodata($nip);
                    $nama = isset($b->nama) ? $b->nama : '';
                    $no_ktp = isset($b->ktp) ? $b->ktp : '';
                    $tempat_lahir = isset($b->t_lahir) ? $b->t_lahir : '';
                    $tgl_lahir = isset($b->tgl_lahir) ? tgl_indo($b->tgl_lahir) : '';
                    $agama = isset($b->agama) ? $b->agama : '';
                    $jenis_kelamin = isset($b->gend) ? $b->gend : '';
                    $jenis_kelamin = ($jenis_kelamin == 'L') ? 'Laki-laki' : 'Perempuan';
                    $golongan_darah = isset($b->golongan_darah) ? $b->golongan_darah : '';
                    $telp = isset($b->telp) ? $b->telp : '';
                    $email = isset($b->mail) ? $b->mail : '';
                    $alamat = isset($b->alamat) ? $b->alamat : '';
                    $status_menikah = isset($b->mar_stat) ? $b->mar_stat : '';
                    $nama_bank = isset($b->bank) ? $b->bank : '';
                    $no_rekening = isset($b->norek) ? $b->norek : '';
                    $pendidikan = isset($b->pend) ? $b->pend : '';
                    $nama_sekolah = isset($b->nama_sklh) ? $b->nama_sklh : '';
                    $jurusan = isset($b->jurusan) ? $b->jurusan : '';
                    $tahun_lulus = isset($b->th_lulus) ? $b->th_lulus : '';
                    $pin = isset($b->pin) ? $b->pin : '';
                    $cabang = isset($b->cabang) ? $b->cabang : '';
                    $divisi = isset($b->divisi) ? $b->divisi : '';
                    $bagian = isset($b->jab) ? $b->jab : '';
                    $jabatan = isset($b->sal_pos) ? $b->sal_pos : '';
                    $status_karyawan = isset($b->kary_stat) ? $b->kary_stat : '';
                    $departemen = isset($b->departemen) ? $b->departemen : '';
                    $tgl_masuk = isset($b->tgl_masuk) ? tgl_indo($b->tgl_masuk) : '';
                    
                    $response = array(
                        'response' => array(
                            'nip' => $nip,
                            'nama' => strtoupper($nama),
                            'no_ktp' => $no_ktp,
                            'tempat_lahir' => $tempat_lahir,
                            'tgl_lahir' => $tgl_lahir,
                            'agama' => $agama,
                            'jenis_kelamin' => $jenis_kelamin,
                            'golongan_darah' => $golongan_darah,
                            'telp' => $telp,
                            'email' => $email,
                            'alamat' => $alamat,
                            'status_menikah' => $status_menikah,
                            'nama_bank' => $nama_bank,
                            'no_rekening' => $no_rekening,
                            'pendidikan' => $pendidikan,
                            'nama_sekolah' => $nama_sekolah,
                            'jurusan' => $jurusan,
                            'tahun_lulus' => $tahun_lulus,
                            'pin' => $pin,
                            'cabang' => $cabang,
                            'divisi' => $divisi,
                            'bagian' => $bagian,
                            'jabatan' => $jabatan,
                            'status_karyawan' => $status_karyawan,
                            'departemen' => $departemen,
                            'tgl_masuk' => $tgl_masuk
                        ),
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function news()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $id = isset($params['id']) ? $params['id'] : '';
                    $nip = nip_api();

                    $n = $this->Rest_Model->news($id);
                    $news = array();
                    if (!empty($n)) {
                        if ($id == '') {
                            foreach ($n as $row) {
                                // $gambar = ($row->gambar != '') ? base_url('assets/berita/'.$row->gambar) : base_url('assets/berita/blank.png');
                                $gambar = ($row->gambar != '') ? base_url('assets/berita/'.$row->gambar) : '';

                                $hari = hari(date('w', strtotime($row->tanggal)));
                                $tanggal = tanggal($row->tanggal);
                                $news[] = array(
                                    'id' => $row->id,
                                    'judul' => $row->judul,
                                    'berita' => $row->text,
                                    'tanggal' => $hari.', '.$tanggal,
                                    'gambar' => $gambar
                                );
                            }
                        } else {
                            $tanggal = isset($n->tanggal) ? $n->tanggal : '';
                            $hari = hari(date('w', strtotime($tanggal)));
                            $tanggal = tanggal($tanggal);

                            $news = array(
                                'id' => isset($n->id) ? $n->id : '',
                                'judul' => isset($n->judul) ? $n->judul : '',
                                'berita' => isset($n->text) ? $n->text : '',
                                'tanggal' => $hari.', '.$tanggal
                            );
                        }

                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $news,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function penjadwalan()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $startdate = isset($params['startdate']) ? api_tgl($params['startdate']) : '';
                    $enddate = isset($params['enddate']) ? api_tgl($params['enddate']) : '';
                    $mode = 3;
                    $nip_arr[] = $nip;

                    $data = $this->Shift_Model->karyawan($mode, $nip_arr, $startdate, $enddate);
                    $date_range = range_to_date($startdate, $enddate);

                    $jadwal = array();
                    if (!empty($data)) {
                        for ($i = 1; $i <= count($data); $i++) {
                            $jadwal['nip'] = $data[$i]['nip'];
                            $jadwal['nama'] = $data[$i]['nama'];
                            for ($a = 0; $a < count($date_range); $a++) {
                                $jadwal['jadwal'][$a] = array(
                                    'tanggal' => tgl_api($date_range[$a]),
                                    'shift' => isset($data[$i][$date_range[$a]]['shift']) ? $data[$i][$date_range[$a]]['shift'] : '',
                                    'jam_masuk' => isset($data[$i][$date_range[$a]]['jam_masuk']) ? jam($data[$i][$date_range[$a]]['jam_masuk']) : '',
                                    'jam_pulang' => isset($data[$i][$date_range[$a]]['jam_pulang']) ? jam($data[$i][$date_range[$a]]['jam_pulang']) : '',
                                    'keterangan' => isset($data[$i][$date_range[$a]]['keterangan']) ? $data[$i][$date_range[$a]]['keterangan'] : '',
                                    'flag_libur' => isset($data[$i][$date_range[$a]]['flag_libur']) ? $data[$i][$date_range[$a]]['flag_libur'] : ''
                                );
                            }
                        }

                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $jadwal,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function reset_password()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $password_lama = isset($params['password_lama']) ? $params['password_lama'] : '';
                    $password_baru = isset($params['password_baru']) ? $params['password_baru'] : '';
                    $re_password = isset($params['re_password']) ? $params['re_password'] : '';

                    $message = '';
                    if ($password_lama == '' || $password_baru == '' || $re_password == '') {
                        $message .= 'Masukkan';
                        $code = 0;
                        if ($password_lama == '') {
                            $message .= ' Password Lama \n';
                        }
                        if ($password_baru == '') {
                            $message .= ' Password Baru \n';
                        }
                        if ($re_password == '') {
                            $message .= ' Retype Password \n';
                        }
                    } else {
                        if ($password_baru != $re_password) {
                            $code = 0;
                            $message .= 'Password tidak cocok';
                        } else {
                            $db = $this->Main_Model->connect_to_absensi();
                            $q = $db->where('id_employee', $nip)
                                ->where('password', md5($password_lama))
                                ->where('status', 1)
                                ->get('ms_user')
                                ->row();

                            if ($q) {
                                $db->where('id_employee', $nip)
                                ->update('ms_user', array('password' => md5($password_baru)));
                                $code = 1;
                                $message .= 'Success';
                            } else {
                                $code = 0;
                                $message .= 'Password Lama salah';
                            }
                        }
                    }

                    $response = array(
                        'response' => '',
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        $log = array(
            'request' => '',
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function detail_terlambat()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $nip = nip_api();
                    $startdate = isset($params['startdate']) ? $params['startdate'] : '';
                    $enddate = isset($params['enddate']) ? $params['enddate'] : '';

                    $periode = periode_sekarang();
                    $awal = isset($periode['tgl_awal']) ? $periode['tgl_awal'] : '';
                    $akhir = isset($periode['tgl_akhir']) ? $periode['tgl_akhir'] : '';

                    // jika startdate kosong
                    if ($startdate == '') {
                        $startdate = $awal;
                    }

                    // jika enddate kosong
                    if ($enddate == '') {
                        $enddate = $akhir;
                    }

                    $h = $this->Rest_Model->header_terlambat($startdate, $enddate, $nip);
                    $terlambat = array();
                    if (!empty($h)) {
                        $periode = tgl_api($startdate).' s/d '.tgl_api($enddate);
                        $total_menit = isset($h->total_menit) ? $h->total_menit : 0;
                        $total_hari = isset($h->total_hari) ? $h->total_hari : 0;

                        $d = $this->Rest_Model->view_terlambat($startdate, $enddate, $nip);
                        $detail = array();
                        foreach ($d as $row) {
                            $hari = hari(date('w', strtotime($row->tgl)));
                            $tanggal = tgl_api($row->tgl);
                            $detail[] = array(
                                'tanggal' => $hari.', '.$tanggal,
                                'jam_masuk' => $row->jam_masuk,
                                'scan_masuk' => $row->scan_masuk,
                                'total_menit' => $row->total_menit
                            );
                        }

                        $terlambat = array(
                            'periode' => $periode,
                            'total_menit' => $total_menit,
                            'total_hari' => $total_hari,
                            'detail' => $detail
                        );

                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $terlambat,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function absensi()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $startdate = isset($params['startdate']) ? api_tgl($params['startdate']) : '';
                    $enddate = isset($params['enddate']) ? api_tgl($params['enddate']) : '';

                    $periode = periode_sekarang();
                    $awal = isset($periode['tgl_awal']) ? $periode['tgl_awal'] : '';
                    $akhir = isset($periode['tgl_akhir']) ? $periode['tgl_akhir'] : '';

                    // jika startdate kosong
                    if ($startdate == '') {
                        $startdate = $awal;
                    }

                    // jika enddate kosong
                    if ($enddate == '') {
                        $enddate = $akhir;
                    }

                    $format_tgl = tanggal($startdate).' - '.tanggal($enddate);
                    // Total terlambat
                    $telat = $this->Rekap_Absensi_Model->terlambat($startdate, $enddate, $nip);
                    $hari_telat = isset($telat->jml) ? $telat->jml : 0;
                    $menit_telat = isset($telat->jml_menit) ? $telat->jml_menit : 0;
                    // Pulang awal
                    $pulang_awal = $this->Rekap_Absensi_Model->pulang_awal($startdate, $enddate, $nip);
                    $hari_pulang_awal = isset($pulang_awal->jumlah) ? $pulang_awal->jumlah : 0;
                    // Tidak absen masuk
                    $scan_masuk = $this->Rekap_Absensi_Model->scan_masuk($startdate, $enddate, $nip);
                    $hari_scan_masuk = isset($scan_masuk->jumlah) ? $scan_masuk->jumlah : 0;
                    // Tidak absen pulang
                    $scan_pulang = $this->Rekap_Absensi_Model->scan_pulang($startdate, $enddate, $nip);
                    $hari_scan_pulang = isset($scan_pulang->jumlah) ? $scan_pulang->jumlah : 0;

                    $data = array(
                        'range' => $format_tgl,
                        'tgl_awal' => tgl_api($startdate),
                        'tgl_akhir' => tgl_api($enddate),
                        'jumlah_terlambat' => $hari_telat,
                        'jumlah_menit_terlambat' => $menit_telat,
                        'jumlah_pulang_awal' => $hari_pulang_awal,
                        'tidak_absen_masuk' => $hari_scan_masuk,
                        'tidak_absen_pulang' => $hari_scan_pulang
                        // 'startdate' => $range_awal,
                        // 'enddate' => $range_akhir
                    );

                    $code = 1;
                    $message = 'Success!';
                    
                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function create_pulang_awal()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);

                    $tanggal = isset($params['tanggal']) ? $params['tanggal'] : '';
                    $jam = isset($params['jam']) ? $params['jam'] : '';
                    $keterangan = isset($params['keterangan']) ? $params['keterangan'] : '';
                    $id = isset($params['id']) ? $params['id'] : '';
                    $approval = isset($params['approval']) ? $params['approval'] : '';

                    // cari nama
                    $karyawan = $this->Main_Model->kary_nip($nip);
                    $nama = isset($karyawan->nama) ? $karyawan->nama : '';

                    $tanggal = api_tgl($tanggal);
                    $result = array();
                    if ($tanggal != '' && $keterangan != '' && $jam != '' && $approval != '') {
                        if ($id != '') {
                            $condition = array(
                                'id_pulang_awal' => $id
                            );
                            $time = 'update_at';
                            $user = 'user_update';
                        } else {
                            $condition = array();
                            $time = 'insert_at';
                            $user = 'user_insert';
                        }
                        
                        // $k = $this->Main_Model->kode_approval($nip, 'cuti');
                        // $approval = isset($k->kode) ? $k->kode : 1;
                        $pola = $this->Main_Model->kode_penyetuju($approval);
                        $kode_pola = isset($pola->pola) ? $pola->pola : '';
                        $approval = isset($pola->kode) ? $pola->kode : '';
                        $penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
                        $cc = isset($pola->cc) ? $pola->cc : '';

                        // cari fcm id
                        $fcm = $this->Main_Model->cari_fcm_id($penyetuju);
                        $usr_id = isset($fcm->usr_id) ? $fcm->usr_id : '';
                        $fcm_id = isset($fcm->fcm_id) ? $fcm->fcm_id : '';

                        $data = array(
                            'nip' => $nip,
                            'tgl' => $tanggal,
                            'jam' => $jam,
                            'alasan' => $keterangan,
                            'status' => $approval,
                            'penyetuju' => $penyetuju,
                            'cc' => $cc,
                            'pola' => $kode_pola,
                            'idp' => 1,
                            $time => now(),
                            $user => $nip
                        );

                        $email = $this->Rest_Model->reminder_pulang_awal($data);
                        if ($email == true) {
                            $simpan = $this->Main_Model->process_data('tb_pulang_awal', $data, $condition);
                            $code = 1;
                            $message = (! empty($condition)) ? 'Update Pulang Awal berhasil' : 'Pengajuan Pulang Awal berhasil';
                            // send notification
                            $title_notif = 'Pengajuan Pulang Awal';
                            $text_notif = $nama.' telah mengajukan pulang awal.';
                            $data_index = ['tipe'];
                            $data_value = ['pulang_awal'];
                            $this->Rest_Model->notif_fcm($title_notif, $text_notif, $fcm_id, $usr_id, 'ACT_NOTIF', $data_index, $data_value);
                        } else {
                            $code = 0;
                            $message = 'Pengajuan Gagal, Email tidak terkirim';
                        }
                    } else {
                        $code = 0;
                        $message = 'Masukkan ';
                        if ($tanggal == '') {
                            $message .= 'Tanggal, ';
                        }
                        if ($keterangan == '') {
                            $message .= 'Keterangan, ';
                        }
                        if ($jam == '') {
                            $message .= 'Jam, ';
                        }
                        if ($approval == '') {
                            $message .= 'Approval, ';
                        }
                    }

                    $response = array(
                        'response' => $result,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function view_pulang_awal()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $id = isset($params['id']) ? $params['id'] : '';
                    $tgl_awal = isset($params['startdate']) ? $params['startdate'] : '';
                    $tgl_akhir = isset($params['enddate']) ? $params['enddate'] : '';

                    $tgl_awal = api_tgl($tgl_awal);
                    $tgl_akhir = api_tgl($tgl_akhir);
                    $pulang_awal = array();

                    $data = $this->Rest_Model->view_pulang_awal($tgl_awal, $tgl_akhir, $id);
                    if (! empty($data)) {
                        foreach ($data as $row => $val) {
                            $pulang_awal[$row] = array(
                                'id' => $val->id_pulang_awal,
                                'nip' => isset($val->nip) ? $val->nip : '',
                                'nama' => isset($val->nama) ? $val->nama : '',
                                'cabang' => isset($val->cabang) ? $val->cabang : '',
                                'tgl' => tgl_api($val->tgl),
                                'jam' => isset($val->jam) ? $val->jam : '',
                                'status' => isset($val->keterangan) ? $val->keterangan : '',
                                'alasan' => isset($val->alasan) ? $val->alasan : ''
                            );
                        }
                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $pulang_awal,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function batal_pulang_awal()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $id = isset($params['id']) ? $params['id'] : '';
                    $nip = nip_api();

                    if ($id != '') {
                        $data = array('status' => 14);
                        $condition = array('id_pulang_awal' => $id);
                        $simpan = $this->Main_Model->process_data('tb_pulang_awal', $data, $condition);
                        if ($simpan > 0) {
                            $code = 1;
                            $message = 'Pengajuan Pulang Awal telah dibatalkan!';
                            
                            $this->Rest_Model->reminder_batal_pulang($id);
                        } else {
                            $code = 0;
                            $message = 'Gagal menyimpan Pulang Awal';
                        }
                    } else {
                        $code = 0;
                        $message = 'Invalid id Pulang Awal';
                    }
                    $response = array(
                        'response' => '',
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function create_keluar_kantor()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);

                    $tanggal = isset($params['tanggal']) ? $params['tanggal'] : '';
                    $jml_menit = isset($params['jml_menit']) ? $params['jml_menit'] : '0';
                    $keterangan = isset($params['keterangan']) ? $params['keterangan'] : '';
                    $id = isset($params['id']) ? $params['id'] : '';
                    $approval = isset($params['approval']) ? $params['approval'] : '';
                    $dari = isset($params['dari']) ? $params['dari'] : '';
                    $sampai = isset($params['sampai']) ? $params['sampai'] : '';

                    // cari nama
                    $karyawan = $this->Main_Model->kary_nip($nip);
                    $nama = isset($karyawan->nama) ? $karyawan->nama : '';

                    $tanggal = api_tgl($tanggal);
                    $result = array();
                    if ($tanggal != '' && $keterangan != '' && $approval != '' && $dari != '' && $sampai != '') {
                        if ($id != '') {
                            $condition = array(
                                'id' => $id
                            );
                            $time = 'update_at';
                            $user = 'user_update';
                        } else {
                            $condition = array();
                            $time = 'insert_at';
                            $user = 'user_insert';
                        }
                        
                        // $k = $this->Main_Model->kode_approval($nip, 'cuti');
                        // $approval = isset($k->kode) ? $k->kode : 1;
                        $pola = $this->Main_Model->kode_penyetuju($approval);
                        $kode_pola = isset($pola->pola) ? $pola->pola : '';
                        $approval = isset($pola->kode) ? $pola->kode : '';
                        $penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
                        $cc = isset($pola->cc) ? $pola->cc : '';

                        // cari fcm id
                        $fcm = $this->Main_Model->cari_fcm_id($penyetuju);
                        $usr_id = isset($fcm->usr_id) ? $fcm->usr_id : '';
                        $fcm_id = isset($fcm->fcm_id) ? $fcm->fcm_id : '';

                        $data = array(
                            'nip' => $nip,
                            'tgl' => $tanggal,
                            'jmljam' => $jml_menit,
                            'keterangan' => $keterangan,
                            'status' => $approval,
                            'pola' => $kode_pola,
                            'penyetuju' => $penyetuju,
                            'dari' => $dari,
                            'sampai' => $sampai,
                            'cc' => $cc,
                            $time => now(),
                            $user => $nip
                        );

                        $email = $this->Rest_Model->reminder_keluar_kantor($data);
                        if ($email == true) {
                            $simpan = $this->Main_Model->process_data('ijinjam', $data, $condition);
                            $code = 1;
                            $message = (! empty($condition)) ? 'Update Ijin Keluar Kantor berhasil' : 'Pengajuan Ijin Keluar Kantor berhasil';

                            // send notification
                            $title_notif = 'Pengajuan Keluar Kantor';
                            $text_notif = $nama.' telah mengajukan Keluar Kantor.';
                            $data_index = ['tipe'];
                            $data_value = ['keluar_kantor'];
                            $this->Rest_Model->notif_fcm($title_notif, $text_notif, $fcm_id, $usr_id, 'ACT_NOTIF', $data_index, $data_value);
                        } else {
                            $code = 0;
                            $message = 'Pengajuan Gagal, Email tidak terkirim';
                        }
                    } else {
                        $code = 0;
                        $message = 'Masukkan ';
                        if ($tanggal == '') {
                            $message .= 'Tanggal, ';
                        }
                        if ($keterangan == '') {
                            $message .= 'Keterangan, ';
                        }
                        // if ($jml_menit == '') $message .= 'Jumlah Menit, ';
                        if ($approval == '') {
                            $message .= 'Approval, ';
                        }
                        if ($dari == '') {
                            $message .= 'Waktu Mulai, ';
                        }
                        if ($sampai == '') {
                            $message .= 'Waktu Selesai, ';
                        }
                    }

                    $response = array(
                        'response' => $result,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function view_keluar_kantor()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $id = isset($params['id']) ? $params['id'] : '';
                    $tgl_awal = isset($params['startdate']) ? $params['startdate'] : '';
                    $tgl_akhir = isset($params['enddate']) ? $params['enddate'] : '';

                    $tgl_awal = api_tgl($tgl_awal);
                    $tgl_akhir = api_tgl($tgl_akhir);
                    $pulang_awal = array();

                    $data = $this->Rest_Model->view_keluar_kantor($tgl_awal, $tgl_akhir, $id);
                    if (! empty($data)) {
                        foreach ($data as $row => $val) {
                            $pulang_awal[$row] = array(
                                'id' => $val->id,
                                'nip' => isset($val->nip) ? $val->nip : '',
                                'nama' => isset($val->nama) ? $val->nama : '',
                                'cabang' => isset($val->cabang) ? $val->cabang : '',
                                'tgl' => tgl_api($val->tgl),
                                'jmljam' => isset($val->jmljam) ? $val->jmljam : '',
                                'dari' => isset($val->dari) ? jam($val->dari) : '',
                                'sampai' => isset($val->sampai) ? jam($val->sampai) : '',
                                'status' => isset($val->app_status) ? $val->app_status : '',
                                'alasan' => isset($val->keterangan) ? $val->keterangan : ''
                            );
                        }
                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $pulang_awal,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function batal_keluar_kantor()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $id = isset($params['id']) ? $params['id'] : '';
                    $nip = nip_api();

                    if ($id != '') {
                        $data = array('status' => 14);
                        $condition = array('id' => $id);
                        $simpan = $this->Main_Model->process_data('ijinjam', $data, $condition);
                        if ($simpan > 0) {
                            $code = 1;
                            $message = 'Pengajuan Ijin Keluar Kantor telah dibatalkan!';

                            $this->Rest_Model->reminder_batal_keluar($id);
                        } else {
                            $code = 0;
                            $message = 'Gagal menyimpan Ijin Keluar Kantor';
                        }
                    } else {
                        $code = 0;
                        $message = 'Invalid id Ijin Keluar Kantor';
                    }
                    $response = array(
                        'response' => '',
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }
        
        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function log($date = '')
    {
        if ($date == '') {
            $date = date('Y-m-d');
        }
        $data = $this->Rest_Model->view_log($date);

        echo json_encode($data);
    }

    function app_version()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'GET') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $data = $this->Rest_Model->check_version();

                    $build_version = '';
                    $link_update = '';

                    if (! empty($data)) {
                        $code = 1;
                        $message = 'Success';

                        $build_version = isset($data->build_version) ? $data->build_version : '';
                        $link_update = isset($data->link_update) ? $data->link_update : '';
                    } else {
                        $code = 0;
                        $message = 'Not Found';
                    }

                    $response = array(
                        'response' => array(
                            'build_version' => $build_version,
                            'link_update' => $link_update
                        ),
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        echo json_encode($response);
    }

    function ms_bulan()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'GET') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $data = $this->Main_Model->arr_bulan_periode();

                    $arr_data = [];
                    if (! empty($data)) {
                        $code = 1;
                        $message = 'Success';

                        $arr_data = $data;
                    } else {
                        $code = 0;
                        $message = 'Not Found';
                    }

                    $response = array(
                        'response' => $arr_data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        echo json_encode($response);
    }

    function ms_tahun()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'GET') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $data = $this->Main_Model->arr_tahun_periode();

                    $arr_data = [];
                    if (! empty($data)) {
                        $code = 1;
                        $message = 'Success';

                        $arr_data = $data;
                    } else {
                        $code = 0;
                        $message = 'Not Found';
                    }

                    $response = array(
                        'response' => $arr_data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        echo json_encode($response);
    }

    function scanlog()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $tgl = isset($params['date']) ? $params['date'] : '';
                    
                    $scan = [];
                    $data = $this->Rest_Model->view_scanlog($tgl);
                    if (! empty($data)) {
                        foreach ($data as $row => $val) {
                            $scan[$row] = array(
                                'nip' => isset($val->nip) ? $val->nip : '',
                                'nama' => isset($val->nama) ? $val->nama : '',
                                'tanggal' => isset($val->tanggal) ? $val->tanggal : '',
                                'jam' => isset($val->jam) ? $val->jam : '',
                                'type' => isset($val->type) ? $val->type : ''
                            );
                        }
                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $scan,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function master_approval()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    
                    $data = $this->Main_Model->master_app_penyetuju();
                    $scan = [];
                    if (! empty($data)) {
                        foreach ($data as $row => $val) {
                            $scan[$row] = array(
                                'kode' => isset($val->pola) ? $val->pola : '',
                                'nama' => isset($val->nama) ? $val->nama : ''
                            );
                        }
                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $scan,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function view_approval()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $tipe = isset($params['tipe']) ? $params['tipe'] : '';

                    if ($tipe == 'cuti') {
                        $data = $this->Rest_Model->view_app_cuti();
                    } elseif ($tipe == 'pulang') {
                        $data = $this->Rest_Model->view_app_pulang();
                    } else {
                        $data = $this->Rest_Model->view_app_keluar();
                    }
                    
                    if (! empty($data)) {
                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response),
        //     'header' => json_encode(getallheaders())
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function approve_action()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $tipe = isset($params['tipe']) ? $params['tipe'] : '';
                    $approval = isset($params['approval']) ? $params['approval'] : ''; // 1 setuju, 0 tolak
                    $id = isset($params['id']) ? $params['id'] : '';

                    if ($tipe == 'cuti') {
                        $data = $this->approve_cuti($id, $approval);
                    } elseif ($tipe == 'pulang') {
                        $data = $this->approve_pulang($id, $approval);
                    } else {
                        $data = $this->approve_keluar($id, $approval);
                    }
                    
                    if ($data == true) {
                        $code = 1;
                        $message = 'Berhasil menyimpan data!';
                    } else {
                        $code = 0;
                        $message = 'Gagal menyimpan data!';
                    }

                    $response = array(
                        'response' => [],
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        $log = array(
            'request' => json_encode($params),
            'response' => json_encode($response),
            'header' => json_encode(getallheaders())
        );
        $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function approve_cuti($id = '', $approve = '')
    {
        $data = $this->Main_Model->view_by_id('cuti_det', array('id_cuti_det' => $id), 'row');
        if (! empty($data)) {
            $nip = isset($data->nip) ? $data->nip : '';
            $kode = isset($data->approval) ? $data->approval : '';
            $pola = isset($data->pola) ? $data->pola : '';

            $value = ($approve == 1) ? 'approve' : 'reject';
            $user = $this->input->get_request_header('Nip', true);

            $result = $this->Main_Model->rule_approval($kode, $pola, $value);
            $update = array(
                'approval' => $result,
                'user_update' => $user,
                'update_at' => now()
            );

            $simpan = $this->Main_Model->process_data('cuti_det', $update, array('id_cuti_det' => $id));
            if ($simpan > 0) {
                // start reminder
                $this->Reminder_Model->reminder_cuti($result, $id);
                $tahun = date('Y');
                $tahun_lalu = $tahun - 1;

                // hitung ulang cuti
                $this->Main_Model->generate_cuti($tahun, $nip);
                $this->Main_Model->generate_cuti($tahun_lalu, $nip);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function approve_pulang($id = '', $approve = '')
    {
        $data = $this->Main_Model->view_by_id('tb_pulang_awal', array('id_pulang_awal' => $id), 'row');
        if (! empty($data)) {
            $kode = isset($data->status) ? $data->status : '';
            $pola = isset($data->pola) ? $data->pola : '';

            $value = ($approve == 1) ? 'approve' : 'reject';
            $user = $this->input->get_request_header('Nip', true);

            $result = $this->Main_Model->rule_approval($kode, $pola, $value);
            $update = array(
                'status' => $result,
                'user_update' => $user,
                'update_at' => now()
            );

            $simpan = $this->Main_Model->process_data('tb_pulang_awal', $update, array('id_pulang_awal' => $id));
            if ($simpan > 0) {
                // start reminder
                $this->Reminder_Model->reminder_pulang_awal($result, $id);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function approve_keluar($id = '', $approve = '')
    {
        $data = $this->Main_Model->view_by_id('ijinjam', array('id' => $id), 'row');
        if (! empty($data)) {
            $kode = isset($data->status) ? $data->status : '';
            $pola = isset($data->pola) ? $data->pola : '';

            $value = ($approve == 1) ? 'approve' : 'reject';
            $user = $this->input->get_request_header('Nip', true);

            $result = $this->Main_Model->rule_approval($kode, $pola, $value);
            $update = array(
                'status' => $result,
                'user_update' => $user,
                'update_at' => now()
            );

            $simpan = $this->Main_Model->process_data('ijinjam', $update, array('id' => $id));
            if ($simpan > 0) {
                // start reminder
                $this->Reminder_Model->reminder_keluar_kantor($result, $id);

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function changelog()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $params = json_decode(file_get_contents('php://input'), true);
                    $data = $this->Rest_Model->view_changelog();
                    
                    if (! empty($data)) {
                        $code = 1;
                        $message = 'Success!';
                    } else {
                        $code = 0;
                        $message = 'Data Not Found!';
                    }

                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response)
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function asuransi()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $startdate = isset($params['startdate']) ? api_tgl($params['startdate']) : '';
                    $enddate = isset($params['enddate']) ? api_tgl($params['enddate']) : '';
                    
                    $bulan = isset($params['bulan']) ? $params['bulan'] : '';
                    $tahun = isset($params['tahun']) ? $params['tahun'] : '';

                    $periode = $this->Rest_Model->periode($bulan, $tahun);
                    $awal = isset($periode['tgl_awal']) ? $periode['tgl_awal'] : '';
                    $akhir = isset($periode['tgl_akhir']) ? $periode['tgl_akhir'] : '';

                    // jika startdate kosong
                    if ($startdate == '') {
                        $startdate = $awal;
                    }

                    // jika enddate kosong
                    if ($enddate == '') {
                        $enddate = $akhir;
                    }

                    $format_tgl = tanggal($startdate).' - '.tanggal($enddate);

                    # potongan asuransi
                    $potongan = $this->Rekap_Absensi_Model->asuransi($startdate, $enddate, $nip);

                    # nominal
                    $nominal = isset($potongan->nominal) ? $potongan->nominal : 0;

                    # keterangan
                    $keterangan = isset($potongan->keterangan) ? $potongan->keterangan : '';

                    $data = array(
                        'range' => $format_tgl,
                        'tgl_awal' => tgl_api($startdate),
                        'tgl_akhir' => tgl_api($enddate),
                        'nominal' => $nominal,
                        'keterangan' => $keterangan
                    );

                    $code = 1;
                    $message = 'Success!';
                    
                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response)
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function hutang()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $startdate = isset($params['startdate']) ? api_tgl($params['startdate']) : '';
                    $enddate = isset($params['enddate']) ? api_tgl($params['enddate']) : '';

                    $bulan = isset($params['bulan']) ? $params['bulan'] : '';
                    $tahun = isset($params['tahun']) ? $params['tahun'] : '';

                    $periode = $this->Rest_Model->periode($bulan, $tahun);
                    $awal = isset($periode['tgl_awal']) ? $periode['tgl_awal'] : '';
                    $akhir = isset($periode['tgl_akhir']) ? $periode['tgl_akhir'] : '';

                    // jika startdate kosong
                    if ($startdate == '') {
                        $startdate = $awal;
                    }

                    // jika enddate kosong
                    if ($enddate == '') {
                        $enddate = $akhir;
                    }

                    $format_tgl = tanggal($startdate).' - '.tanggal($enddate);

                    # potongan cicilan
                    $potongan = $this->Rekap_Absensi_Model->cicilan($startdate, $enddate, $nip);

                    # nominal
                    $nominal = isset($potongan->angsuran) ? $potongan->angsuran : 0;

                    # keterangan
                    $keterangan = isset($potongan->keterangan) ? $potongan->keterangan : '';

                    $data = array(
                        'range' => $format_tgl,
                        'tgl_awal' => tgl_api($startdate),
                        'tgl_akhir' => tgl_api($enddate),
                        'nominal' => $nominal,
                        'keterangan' => $keterangan
                    );

                    $code = 1;
                    $message = 'Success!';
                    
                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        // $log = array(
        //     'request' => json_encode($params),
        //     'response' => json_encode($response)
        // );
        // $this->Rest_Model->create_log($log);
        echo json_encode($response);
    }

    function payroll()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $pin = isset($params['pin']) ? $params['pin'] : '';
                    $data = [];
                    
                    if ($pin == '') {
                        $message = 'Masukkan PIN';
                        $code = 0;
                    } else {
                        $exists = $this->Main_Model->view_by_id('tb_master_key', ['user' => $nip, 'flag' => 1], 'row');
                        if ($exists) {
                            $ms_key = $this->Main_Model->view_by_id('tb_master_key', [
                                'user' => $nip, 
                                'flag' => 1, 
                                'key' => md5($pin.'&gmediapayroll&secretkey')
                            ], 'row');

                            if ($ms_key) {
                                $data = $this->Rest_Model->detail_gaji($nip, $pin);

                                $code = 1;
                                $message = 'Success';
                            } else {
                                $code = 0;
                                $message = 'PIN anda salah';
                            }
                        } else {
                            $code = 0;
                            $message = 'User tidak ditemukan, silahkan daftarkan PIN terlebih dahulu';
                        }
                    }   
                    
                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        echo json_encode($response);
    }

    function create_pin()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $pin = isset($params['pin']) ? $params['pin'] : '';
                    $confirm_pin = isset($params['confirm_pin']) ? $params['confirm_pin'] : '';
                    $data = [];
                    
                    if ($pin == '') {
                        $message = 'Masukkan PIN';
                        $code = 0;
                    } else if ($confirm_pin == '') {
                        $message = 'Masukkan Kembali PIN';
                        $code = 0;
                    } else if($pin != $confirm_pin) {
                        $message = 'PIN tidak cocok';
                        $code = 0;
                    } else if (strlen($pin) != 6) {
                        $message = 'PIN Harus 6 karakter';
                        $code = 0;
                    } else {
                        $exists = $this->Main_Model->view_by_id('tb_master_key', ['user' => $nip, 'flag' => 1], 'row');
                        if ($exists) {
                            $message = 'User sudah terdaftar';
                            $code = 0;
                        } else {
                            $simpan = $this->Rest_Model->create_pin($nip, $pin);

                            if ($simpan) {
                                $code = 1;
                                $message = 'Success';
                            } else {
                                $code = 0;
                                $message = 'Gagal menyimpan data';
                            }
                        }
                    }   
                    
                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        echo json_encode($response);
    }

    function update_pin()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $params = '';
        if ($method != 'POST') {
            $response = bad_request();
        } else {
            $response = check_auth();
            if ($response['metadata']['status'] == 1) {
                $response = auth_login();
                if ($response['metadata']['status'] == 1) {
                    $nip = nip_api();
                    $params = json_decode(file_get_contents('php://input'), true);
                    $pin = isset($params['pin']) ? $params['pin'] : '';
                    $pin_baru = isset($params['pin_baru']) ? $params['pin_baru'] : '';
                    $confirm_pin = isset($params['confirm_pin']) ? $params['confirm_pin'] : '';
                    $data = [];
                    
                    if ($pin == '') {
                        $message = 'Masukkan PIN';
                        $code = 0;
                    } else if ($confirm_pin == '') {
                        $message = 'Masukkan Kembali PIN';
                        $code = 0;
                    } else if ($pin_baru == '') {
                        $message = 'Masukkan PIN baru';
                        $code = 0;
                    } else if ($confirm_pin != $pin_baru) {
                        $message = 'PIN tidak cocok';
                        $code = 0;
                    } else if (strlen($pin_baru) != 6) {
                        $message = 'PIN Harus 6 karakter';
                        $code = 0;
                    } else {
                        $exists = $this->Main_Model->view_by_id('tb_master_key', ['user' => $nip, 'flag' => 1], 'row');

                        $ms_key = $this->Main_Model->view_by_id('tb_master_key', [
                                'user' => $nip, 
                                'flag' => 1, 
                                'key' => md5($pin.'&gmediapayroll&secretkey')
                            ], 'row');

                        if ($exists) {
                            if ($ms_key) {
                                $update = $this->Rest_Model->update_pin($pin, $pin_baru, $nip);

                                if ($update) {
                                    $code = 1;
                                    $message = 'Success';
                                } else {
                                    $code = 0;
                                    $message = 'Gagal Update';
                                }
                            } else {
                                $code = 0;
                                $message = 'PIN lama salah';
                            }
                        } else {
                            $code = 0;
                            $message = 'User tidak ditemukan silahkan mendaftar terlebih dahulu';
                        }
                    }   
                    
                    $response = array(
                        'response' => $data,
                        'metadata' => array(
                            'status' => $code,
                            'message' => $message
                        )
                    );
                }
            }
        }

        echo json_encode($response);
    }

    function reminder()
    {
        $arr = ['03.085.2016'];

        $jam = jam(now());

        if ($arr && (($jam >= '08:00' && $jam <= '09:00') || ($jam >= '17:30' && $jam <= '18:30'))) {
            for ($i = 0; $i < count($arr); $i++) {
                $karyawan = $this->Main_Model->view_by_id('kary', ['nip' => $arr[$i]], 'row');

                $fcm_id = isset($karyawan->fcm_id) ? $karyawan->fcm_id : '';

                $this->Rest_Model->notif_fcm('Absen', 'Absen bos', $fcm_id);
            }
        }
    }
}

/* End of file rest.php */
/* Location: ./application/controllers/rest.php */
