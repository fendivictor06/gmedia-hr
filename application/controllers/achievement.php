<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Achievement extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Absensi_Model', '', TRUE);
		$this->load->model('Achievement_Model', '', TRUE);
	}

	function header(){
		$menu = array(
			'menu2' => 'active open selected',
			'style' => '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">
				<link href="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css').'" rel="stylesheet" type="text/css" />'
			);
		return $menu;
	}

	function footer(){
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js').'"></script>';

		return $footer;
	}

	function download_ach(){
		$this->Main_Model->get_login();
		$javascript = '
			<script type="text/javascript">
				function download()
				{
					var lwok 	= $("#lwok").val();
					var qd_id 	= $("#qd_id").val();
					window.location.href="' . base_url('achievement/download_list') . '/"+qd_id+"/"+lwok;
				}
			</script>';
		// $footer =array(
		// 	'javascript' => $javascript,
		// 	'js' => $this->footer()
		// 	);
		$data       = array(
            'lwok'	 		=> $this->Absensi_Model->lwok_opt(),
            'periode' 		=> $this->Main_Model->periode_opt(),
            'javascript' 	=> $javascript,
			'js' 			=> $this->footer()
        );
		$this->load->view('template/header',$this->header());
		$this->load->view('achievement/download',$data);
		// $this->load->view('template/footer',$footer);
	}

	function dataachievement()
	{
		$this->Main_Model->get_login();
		$data = array(
			'periode' => $this->Main_Model->periode_opt(),
			'kary_list' => $this->Main_Model->all_kary(),
			'js' => $this->footer()
			);
		$menu = array(
			'menu2' => 'active open selected',
			'style' => '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">'
			);
		$this->load->view('template/header',$menu);
		$this->load->view('achievement/dataachievement',$data);
		// $this->load->view('template/footer',$footer);
	}

	function view_achievement()
	{
		$this->Main_Model->get_login();
		$periode=$this->input->get('periode');
		$tgl_akhir = $this->db->query("select qd_id,DATE_FORMAT(tgl_akhir,'%M %Y') AS bln,tgl_akhir from q_det where qd_id='$periode'")->row();
		$data = $this->Achievement_Model->view_achievement($tgl_akhir->tgl_akhir);
		$template = $this->Main_Model->tbl_temp();

		$this->table->set_heading('No','Bln-Th','Nip','Nama','Lokasi','Achievement','Performance','Action');
		$no=1;
		foreach ($data as $row) {
		$q = $this->db->query("SELECT d.`lka` FROM sk a LEFT JOIN kary b ON a.`nip`=b.`nip` JOIN pos_sto c ON a.`id_pos_sto`=c.`id_sto` JOIN lka d ON d.`id_lka`=c.`id_lka` WHERE a.`nip` = '$row->nip'")->row();
		$lokasi = isset($q->lka)?$q->lka:'';
		$w = $this->db->query("SELECT * FROM ach WHERE nip = '$row->nip' AND id_per = '$periode'")->row();
		$ach = isset($w->ach)?$w->ach:'';
		$perf = isset($w->perf)?$w->perf:'';
		$this->table->add_row(
			$no++,
			$tgl_akhir->bln,
			$row->nip,
			$row->nama,
			$lokasi,
			$ach,
			$perf,
			'<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" onclick="get_id(' . $row->nip . ',' . $periode . ');">
	                            <i class="icon-edit"></i> Add Achievement </a>
	                    </li>
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->nip . ',' . $periode . ');">
	                            <i class="icon-delete"></i> Delete Achievement </a>
	                    </li>
	                </ul>
	        </div>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function cari_ach()
	{
		$nip 	= $this->input->get("nip");
		$data 	= $this->Achievement_Model->cari_ach($nip);

		foreach ($data as $row) {
			$result[$row->id_nsc] = $row->perf;
		}

		echo form_dropdown('perf',$result,'','id="perf" class="form-control"');
	}

	function add_achievement()
	{
		$this->Main_Model->get_login();
		$nip = $this->input->post('nip');
		$id_per = $this->input->post('id_per');
		$ach = $this->input->post('ach');

		$cek = $this->db->query("select * from ach where id_per='$id_per' and nip='$nip'")->row();
		if($cek){
			$this->delete_achievement($nip,$id_per);
			$data = array(
				'nip' => $nip,
				'id_per' => $id_per,
				'ach' => $ach,
				);
			$this->Achievement_Model->add_achievement($data);
		}else{
			$data = array(
				'nip' => $nip,
				'id_per' => $id_per,
				'ach' => $ach,
				);
			$this->Achievement_Model->add_achievement($data);
		}	
	}

	function achievement_id()
	{
		$this->Main_Model->get_login();
		$nip = $this->input->get('nip');
		$id_per = $this->input->get('id_per');
		$data = $this->Achievement_Model->achievement_id($nip,$id_per);

		echo json_encode($data);
	}

	function delete_achievement($nip,$id_per)
	{
		$this->Main_Model->get_login();

		$this->Achievement_Model->delete_achievement($nip,$id_per);
	}

	function ach()
	{
		$data = array(
			'js' 		=> $this->footer(),
			'periode' 	=> $this->Main_Model->periode_opt(),
			'kary_list' => $this->Main_Model->all_kary()
			);
		$this->load->view('template/header',$this->header());
		$this->load->view('achievement/ach',$data);
		// $this->load->view('template/footer',$footer);
	}

	function tb_ach()
	{
		$this->Main_Model->get_login();
		$periode 	=$this->input->get('periode');
		$tgl_akhir 	= $this->db->query("select qd_id,DATE_FORMAT(tgl_akhir,'%M %Y') AS bln,tgl_akhir,tgl_awal from q_det where qd_id='$periode'")->row();
		$data 		= $this->Achievement_Model->view_achievement($tgl_akhir->tgl_akhir, $tgl_akhir->tgl_awal);
		$template 	= $this->Main_Model->tbl_temp('sample_editable_1');

		$this->table->set_heading('No','Bln-Th','Nip','Nama','Lokasi','Achievement','Performance','Action');
		$no=1;
		foreach ($data as $row) {
		$q 			= $this->db->query("SELECT d.`lka` FROM sk a LEFT JOIN kary b ON a.`nip`=b.`nip` JOIN pos_sto c ON a.`id_pos_sto`=c.`id_sto` JOIN lka d ON d.`id_lka`=c.`id_lka` WHERE a.`nip` = '$row->nip'")->row();
		$lokasi 	= isset($q->lka)?$q->lka:'';
		$w 			= $this->db->query("SELECT * FROM ach WHERE nip = '$row->nip' AND id_per = '$periode'")->row();
		$id_ach 	= isset($w->id_ach)?$w->id_ach:'';
		$ach 		= isset($w->ach)?$w->ach:'';
		$perf 		= isset($w->perf)?$w->perf:'';
		$this->table->add_row(
			$no++,
			$tgl_akhir->bln.'<input type="hidden" id="peri" value="'.$periode.'" >',
			$row->nip,
			$row->nama,
			$lokasi,
			$ach,
			$perf,
			'<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" class="edit">
	                            <i class="icon-edit"></i> Edit Achievement </a>
	                    </li>
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->nip . ',' . $periode . ');">
	                            <i class="icon-delete"></i> Delete Achievement </a>
	                    </li>
	                </ul>
	        </div>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function download_list($id_per, $id_lwok)
	{
		$this->Main_Model->get_login();
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');
        $ambildata = $this->Achievement_Model->download_list($id_per, $id_lwok);

        	$objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
			$objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
			$objPHPExcel->getProperties()->setTitle("Download Achievement");
			$objPHPExcel->getProperties()->setSubject("Download Achievement");
			$objPHPExcel->getProperties()->setDescription("List Karyawan Non Staff");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Achievement'); //sheet title

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:F1');
 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:F2');
 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:F3');

 			$objset->setCellValue('A2','** Dilarang Merubah Format Excel / mengganti NIK, Apabila ada List Karyawan Yang kurang, Harap hubungi HRD');

 			$styleArray = array(
			      	'borders' => array(
			          	'allborders' => array(
			              	'style' => PHPExcel_Style_Border::BORDER_THIN
			          )
			      )
			);

			//table header
            $cols = array("A","B","C","D","E","F");
             
            $val = array("NO ","NIP","NAMA","POSISI","LOKASI","ACHIVEMENT(tanpa%)");

            for ($a=0;$a<6; $a++) 
            {
                $objset->setCellValue($cols[$a].'4', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
             
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 5;
            $i=1;
            foreach ($ambildata as $frow){
                 
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $i++); 
                $objset->setCellValue("B".$baris, $frow->nip); 
                $objset->setCellValue("C".$baris, $frow->nama);
                $objset->setCellValue("D".$baris, $frow->posisi);
                // $objset->setCellValue("E".$baris, $frow->jab);
                $objset->setCellValue("E".$baris, $frow->lku);
                $objset->setCellValue("F".$baris, '');
                 
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
            }
            $b=$baris-1;
			$objPHPExcel->getActiveSheet()->getStyle('A1:F'.$b)->applyFromArray($styleArray);

            $title 		= "form_ach".date('Y-m-d');
            $objPHPExcel->getActiveSheet()->setTitle($title);
 
            // $objPHPExcel->setActiveSheetIndex(0);  
            // $filename = urlencode("Data".date("Y-m-d H:i:s").".xls");
               
            // header('Content-Type: application/vnd.ms-excel'); //mime type
            // header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            // header('Cache-Control: max-age=0'); //no cache
            // header("Pragma: public");

            // Rename first worksheet
			// $objPHPExcel->getActiveSheet()->setTitle('LaporanAbsensi');

			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$title.'.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
	}
}