<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Klarifikasi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
		$this->load->model('Absensi_Model');
		$this->load->model('Terlambat_Model');
		$this->load->model('Cuti_Model');
		$this->load->model('Klarifikasi_Model');
	}

	function index($param='')
	{
		$this->Main_Model->get_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2();

		$header = array(
			'menu' => $this->Main_Model->menu_admin('0','0','4'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
			);

		$head = ($param == 'absensi') ? 'Klarifikasi Absensi' : 'Klarifikasi Terlambat';
		$tag = ($param == 'absensi') ? 'klarifikasi_absensi' : 'klarifikasi_terlambat';

		$arr_klarifikasi = [];
		$opt_klarifikasi = $this->Main_Model->view_by_id('ms_opt_klarifikasi', ['status' => 1], 'result');
		if (! empty($opt_klarifikasi)) {
			foreach ($opt_klarifikasi as $row) {
				$arr_klarifikasi[$row->tag] = $row->label;
			}
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'periode' => $this->Main_Model->periode_opt(),
			'penutup' => $this->Main_Model->close_page(),
			'absen' => $arr_klarifikasi,
			'header' => $head,
			'tag' => $tag
		);

		$this->load->view('template/header', $header);
		$this->load->view('akun/klarifikasi',$data);
	}

	function verifikasi_absensi()
	{
		$this->Main_Model->get_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datetimepicker()
		      .$this->Main_Model->js_datepicker();

		$header = array(
			'menu' => $this->Main_Model->menu_admin('0','0','4'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datetimepicker()
				      .$this->Main_Model->style_datepicker()
			);

		$tipe_cuti = $this->Cuti_Model->tipe_cuti();

		$arr_klarifikasi = [];
		$opt_klarifikasi = $this->Main_Model->view_by_id('ms_opt_klarifikasi', ['status' => 1], 'result');
		if (! empty($opt_klarifikasi)) {
			foreach ($opt_klarifikasi as $row) {
				$arr_klarifikasi[$row->tag] = $row->label;
			}
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'periode' => $this->Main_Model->periode_opt(),
			'penutup' => $this->Main_Model->close_page(),
			'tipe' => $tipe_cuti,
			'absen' => $arr_klarifikasi
		);

		$this->load->view('template/header', $header);
		$this->load->view('klarifikasi/form_absensi',$data);
	}

	function verifikasi_terlambat()
	{
		$this->Main_Model->get_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker();

		$header = array(
			'menu' => $this->Main_Model->menu_admin('0','0','4'),
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'periode' => $this->Main_Model->periode_opt(),
			'penutup' => $this->Main_Model->close_page(),
			'absen' 	=> array(
                'sakit' 	=> 'Sakit',
                'ijin' 		=> 'Ijin',
                'mangkir' 	=> 'Mangkir',
                'masuk' 	=> 'Masuk',
                'cuti' 		=> 'Cuti'
            	)
			);

		$this->load->view('template/header', $header);
		$this->load->view('klarifikasi/form_terlambat',$data);
	}

	function view_klarifikasi_absensi()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Absensi_Model->view_all_mangkir_klarifikasi($id_periode);
        $template = $this->Main_Model->tbl_temp('mangkir');

        $this->table->set_heading('No','NIP','Nama','Cabang','Tanggal','Keterangan','Action');
        
        $no = 1;
        foreach ($data as $row) {
        	$this->table->add_row(
	            $no++,
	            $row->nip,
	            $row->nama,
	            $row->cabang,
	            isset($row->tgl) ? $this->Main_Model->format_tgl($row->tgl) : '',
	            $row->ket,
	            '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="get_id(' . $row->id . ');"> <i class="fa fa-pencil"></i> Klarifikasi </a>'
	        );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
	}

	function view_klarifikasi_terlambat()
	{
		$this->Main_Model->get_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Terlambat_Model->view_terlambat_klarifikasi($id_periode);
		$template = $this->Main_Model->tbl_temp('terlambat');

		$array_heading = array('No', 'NIP', 'Nama', 'Cabang', 'Tanggal', 'Total Menit', 'Scan Masuk','Action');

		$this->table->set_heading($array_heading);

		$no = 1;
		foreach($data as $row) {
			$event = "get_terlambat('".$row->id."')";
			$action = '<button class="btn btn-primary btn-xs" onclick="'.$event.'" ><i class="fa fa-check"></i> Verifikasi</button>';
			$tanggal  = ($row->tgl != '') ? $this->Main_Model->format_tgl($row->tgl) : '';
			$array_data = array($no++, $row->nip, $row->nama, $row->cabang, $tanggal, $row->total_menit, $row->scan_masuk, $action);

			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function approval_klarifikasi($id='', $val='')
	{
		$this->Main_Model->all_login();
		$k = $this->Main_Model->view_by_id('tb_klarifikasi_absensi', array('id' => $id), 'row');
		$kode = isset($k->approval) ? $k->approval : '';
		$id_presensi = isset($k->id_presensi) ? $k->id_presensi : '';
		$id_ab = isset($k->id_ab) ? $k->id_ab : '';
		$presensi = isset($k->presensi) ? $k->presensi : '';
		$nip = isset($k->nip) ? $k->nip : '';
		$klarifikasi = isset($k->klarifikasi) ? $k->klarifikasi : '';
		$tanggal = isset($k->tgl) ? $k->tgl : '';

		$value = $this->Main_Model->rule_approval($kode, 'klarifikasi', $val);

		// approval disetujui
		if ($value == '12') {
			$check_presensi = $this->Main_Model->view_by_id('presensi', array('id' => $id_presensi), 'row');
			$this->Main_Model->delete_data('log_presensi', array('id' => $id_presensi));
			$log_presensi = array(
				'id_presensi' => $check_presensi->id,
				'nip' => $check_presensi->nip,
				'tgl' => $check_presensi->tgl,
				'sakit' => $check_presensi->sakit,
				'ijin' => $check_presensi->ijin,
				'mangkir' => $check_presensi->mangkir,
				'alpha' => $check_presensi->alpha,
				'ket' => $check_presensi->ket,
				'klarifikasi' => $check_presensi->klarifikasi,
				'file' => $check_presensi->file,
				'insert_at' => $check_presensi->insert_at,
				'update_at' => date('Y-m-d H:i:s'),
				'user' => $this->session->userdata('username'),
				'flag' => 1
				);
			$this->Main_Model->process_data('log_presensi', $log_presensi);

			if ($presensi == 'cuti') {
				$q = $this->db->query("select max(id_cuti_det) id from cuti_det")->row();
				$old_id = isset($q->id) ? $q->id : 0;
				$new_id = $old_id + 1;

				$data = array(
					'id_cuti_det' => $new_id,
					'nip' => $nip,
					'alasan_cuti' => $klarifikasi,
					'alamat_cuti' => '',
					'no_hp' => '',
					'id_tipe' => 1,
					'approval' => 1,
					'insert_at' => date('Y-m-d H:i:s'),
					'user_insert' => $this->session->userdata('username') 
				);

				$data_tgl = array(
					'id_cuti_det' => $new_id,
					'tgl' => $tanggal,
				);

				$this->Cuti_Model->add_subdet_cuti($data_tgl);
				$this->Cuti_Model->add_cutibiasa($data);
				$this->Main_Model->generate_cuti();
			} else {
				if ($presensi == 'sakit') {
					$data = array(
						'sakit' => 1,
						'ijin' => 0,
						'mangkir' => 0,
						'alpha' => 0,
						'klarifikasi' => $klarifikasi,
						'update_at' => date('Y-m-d H:i:s'),
					    'user' => $this->session->userdata('username')
					);
					$this->Main_Model->process_data('presensi', $data, array('id' => $id_presensi));
				} else {
					$this->Main_Model->delete_data('presensi', array('id' => $id_presensi));
				}
			}
		}

		$data = array(
			'approval' => $value
			);
		$this->Main_Model->process_data('tb_klarifikasi_absensi', $data, array('id' => $id));
	}

	function view_edit_klarifikasi($id='')
	{
		$this->Main_Model->get_login();

		$data = $this->Absensi_Model->view_edit_klarifikasi($id);
		echo json_encode($data);
	}

	function edit_klarifikasi_process()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$id_ab = $this->input->post('id_ab');
		$id_presensi = $this->input->post('id_presensi');
		$nip = $this->input->post('nip');
		$tgl = $this->input->post('tgl');
		$tgl = $this->Main_Model->convert_tgl($tgl);
		$absen = $this->input->post('absen');
		$scan_masuk = $this->input->post('scan_masuk');
		$scan_pulang = $this->input->post('scan_pulang');
		$ket = $this->input->post('ket');
		$klarifikasi = $this->input->post('klarifikasi');

		$th = date('Y');
    	$cek_kuota 	= $this->Cuti_Model->cek_kuotacuti($nip, $th);
    	$kuota_cuti = isset($cek_kuota->qt) ? $cek_kuota->qt : 0;
    	// cari cuti
    	$old_cuti = $this->db->query("
    			SELECT * FROM cuti_det a 
    			INNER JOIN cuti_sub_det b ON a.id_cuti_det = b.id_cuti_det
    			WHERE b.tgl = '$tgl'
    			AND a.nip = '$nip'")->row();
    	$id_cuti_det = isset($old_cuti->id_cuti_det) ? $old_cuti->id_cuti_det : '';

    	$check_cuti = $this->db->query("
    		SELECT COUNT(b.`tgl`) jml 
    		FROM cuti_det a 
    		JOIN cuti_sub_det b ON a.`id_cuti_det` = b.`id_cuti_det` 
    		WHERE DATE_FORMAT(b.`tgl`,'%m %Y') = DATE_FORMAT('$tgl','%m %Y')
    		AND a.id_cuti_det != '$id_cuti_det'
    		AND a.nip = '$nip'")->row();

    	$filename = 'File Presensi '.$nip.' '.date('Y-m-d');
    	$this->load->library('upload');
    	$config = $this->Main_Model->set_upload_options("./assets/presensi/","*","5048000",$filename);
		$this->upload->initialize($config);
		

		$message = '';
		if ($absen == '' || $klarifikasi == '' || $tgl == '') {
			$result = array('status' => false , 'message' => 'Form masih ada yang kosong!');
		} else {
			if ($absen == 'sakit' && empty($this->upload->do_upload('file_upload'))) {
				$result = array('status' => false, 'message' => 'Wajib Melampirkan File ketika sakit');
			} else {
				if (strtotime($scan_masuk) > strtotime($scan_pulang)) {
					$result = array('status' => false, 'message' => 'Invalid date format!');
				} elseif ($absen == 'cuti' &&  $kuota_cuti == 0) {
					$result = array('status' => false, 'message' => 'Kuota Cuti tidak cukup!');
				} elseif ($absen == 'cuti' && $check_cuti->jml >= 2) {
					$result = array('status' => false, 'message' => 'Cuti bulan ini sudah mencapai batas maksimum!');
				} else {
					$data_klarifikasi = $this->Main_Model->view_by_id('tb_klarifikasi_absensi', array('id' => $id), 'row');
					$old_presensi = isset($data_klarifikasi->presensi) ? $data_klarifikasi->presensi : '';
					if ($old_presensi == 'cuti') {

						$this->Main_Model->delete_data('cuti_sub_det', array('id_cuti_det' => $id_cuti_det));
						$this->Main_Model->delete_data('cuti_det', array('id_cuti_det' => $id_cuti_det));

						$this->Main_Model->generate_cuti();
					}

					$check_presensi = $this->Main_Model->view_by_id('presensi', array('id' => $id_presensi), 'row');
					if (!empty($check_presensi)) {
						$log_presensi = array(
	    					'id_presensi' => $check_presensi->id,
	    					'nip' => $check_presensi->nip,
	    					'tgl' => $check_presensi->tgl,
	    					'sakit' => $check_presensi->sakit,
	    					'ijin' => $check_presensi->ijin,
	    					'mangkir' => $check_presensi->mangkir,
	    					'alpha' => $check_presensi->alpha,
	    					'ket' => $check_presensi->ket,
	    					'klarifikasi' => $check_presensi->klarifikasi,
	    					'file' => $check_presensi->file,
	    					'insert_at' => $check_presensi->insert_at,
	    					'update_at' => date('Y-m-d H:i:s'),
	    					'user' => $this->session->userdata('username'),
	    					'flag' => 1
	    					);
	    				$this->Main_Model->process_data('log_presensi', $log_presensi);
	    			}

    				$check_absensi = $this->Main_Model->view_by_id('tb_absensi', array('id_ab' => $id_ab), 'row');
    				$pin = isset($check_absensi->pin) ? $check_absensi->pin : '';
    				if (!empty($check_absensi)) {
	    				$log_absensi = array(
		    				'id_ab' => $check_absensi->id_ab,
		    				'nip' => $check_absensi->nip,
		    				'tgl' => $check_absensi->tgl,
		    				'scan_masuk' => $check_absensi->scan_masuk,
		    				'scan_pulang' => $check_absensi->scan_pulang,
		    				'keterangan' => $check_absensi->keterangan,
		    				'insert_at' => $check_absensi->insert_at,
		    				'update_at' => date('Y-m-d H:i:s'),
		    				'status' => $check_absensi->status,
		    				'pin' => $check_absensi->pin,
		    				'flag' => 1,
		    				'user' => $this->session->userdata('username')
		    				);

		    			$this->Main_Model->process_data('log_absensi', $log_absensi);
		    		}

					if ($absen == 'sakit' || $absen == 'mangkir') {
						$update = array(
							'nip' => $nip,
							'tgl' => $tgl,
							'ijin' => 0,
							'alpha' => 0,
							'ket' => $ket,
							'pin' => $pin,
							'klarifikasi' => $klarifikasi,
							'insert_at' => date('Y-m-d H:i:s'),
							'user' => $this->session->userdata('username'),
							'update_at' => date('Y-m-d H:i:s')
						);

						if ($absen == 'sakit') {
							$update['sakit'] = 1;
							$update['mangkir'] = 0;
						} else {
							$update['sakit'] = 0;
							$update['mangkir'] = 1;
						}

						$where = (empty($check_presensi)) ? array() : array('id' => $id_presensi);
						$this->Main_Model->process_data('presensi', $update, $where);
					} elseif ($absen == 'masuk') {
						$this->Main_Model->delete_data('presensi', array('id' => $id_presensi));
					} else {
						$q 	= $this->db->query("select max(id_cuti_det) id from cuti_det")->row();
						$old_id = isset($q->id) ? $q->id : 0;
						$new_id = $old_id + 1;

						$data = array(
							'id_cuti_det' => $new_id,
							'nip' => $nip,
							'alasan_cuti' => $klarifikasi,
							'alamat_cuti' => '',
							'no_hp' => '',
							'id_tipe' => 1,
							'approval' => 1,
							'insert_at' => date('Y-m-d H:i:s'),
							'user_insert' => $this->session->userdata('username') 
						);

						$data_tgl = array(
							'id_cuti_det' => $new_id,
							'tgl' => $tgl,
						);

						$this->Cuti_Model->add_subdet_cuti($data_tgl);
						$this->Cuti_Model->add_cutibiasa($data);
						$this->Main_Model->generate_cuti();
						$this->Main_Model->delete_data('presensi', array('id' => $id_presensi));
					}

					$data_absensi = array(
	    				'scan_masuk' => $scan_masuk.':00',
	    				'scan_pulang' => $scan_pulang.':00',
	    				'keterangan' => $klarifikasi,
	    				'status' => $ket,
	    				'update_at' => date('Y-m-d H:i:s'),
	    				'flag' => 1,
	    				'user' => $this->session->userdata('username')
	    			);

	    			$data_klarifikasi = array(
	    				'id_presensi' => $id,
						'tgl' => $tgl,
						'presensi' => $absen,
						'klarifikasi' => $klarifikasi,
						'id_ab' => $id_ab,
						'user' => $this->session->userdata('username'),
						'nip' => $nip
	    			);
	    			
	    			if($this->upload->do_upload('file_upload')) {
		    			$file = $this->upload->data();
						$data_absensi['file'] = $file['file_name'];
						$data_klarifikasi['file'] = $file['file_name'];
		    		}

		    		$this->Main_Model->process_data('tb_klarifikasi_absensi', $data_klarifikasi, array('id' => $id));
	    			$this->Main_Model->process_data('tb_absensi', $data_absensi, array('id_ab' => $id_ab));

	    			$result = array('status' => true , 'message' => 'Success!');
				}
			}
		}
		echo json_encode($result);
	}

	function approval_klarifikasi_terlambat($id='', $val='')
	{
		$this->Main_Model->get_login();
		$k = $this->Main_Model->view_by_id('tb_klarifikasi_terlambat', array('id' => $id), 'row');
		$kode = isset($k->approval) ? $k->approval : '';
		$id_terlambat = isset($k->id_terlambat) ? $k->id_terlambat : '';
		$id_ab = isset($k->id_ab) ? $k->id_ab : '';
		$nip = isset($k->nip) ? $k->nip : '';
		$klarifikasi = isset($k->klarifikasi) ? $k->klarifikasi : '';
		$tanggal = isset($k->tgl) ? $k->tgl : '';

		$value = $this->Main_Model->rule_approval($kode, 'klarifikasi', $val);

		if ($value == '12') {
			$check_terlambat = $this->Main_Model->view_by_id('terlambat_temp', array('id' => $id_terlambat), 'row');
			if($check_terlambat) {
				$log_terlambat = array(
					'id_terlambat' => $check_terlambat->id,
					'nip' => $check_terlambat->nip,
					'tgl' => $check_terlambat->tgl,
					'total_menit' => $check_terlambat->total_menit,
					'pin' => $check_terlambat->pin,
					'scan_masuk' => $check_terlambat->scan_masuk,
					'shift' => $check_terlambat->shift,
					'jam_masuk' => $check_terlambat->jam_masuk,
					'insert_at' => $check_terlambat->insert_at,
					'update_at' => date('Y-m-d H:i:s'),
					'user' => $this->session->userdata('username')
					);
				$this->Main_Model->process_data('log_terlambat', $log_terlambat);
			}
			$check_absensi = $this->Main_Model->view_by_id('tb_absensi', array('id_ab' => $id_ab), 'row');
			if($check_absensi) {
				$log_absensi = array(
    				'id_ab' => $check_absensi->id_ab,
    				'nip' => $check_absensi->nip,
    				'tgl' => $check_absensi->tgl,
    				'scan_masuk' => $check_absensi->scan_masuk,
    				'scan_pulang' => $check_absensi->scan_pulang,
    				'keterangan' => $check_absensi->keterangan,
    				'insert_at' => $check_absensi->insert_at,
    				'update_at' => date('Y-m-d H:i:s'),
    				'status' => $check_absensi->status,
    				'pin' => $check_absensi->pin,
    				'flag' => 1,
    				'user' => $this->session->userdata('username')
    				);

    			$this->Main_Model->process_data('log_absensi', $log_absensi);
			}
					
			$this->Main_Model->delete_data('terlambat_temp', array('id' => $id_terlambat));
		}

		$data = array(
			'approval' => $value
			);
		$this->Main_Model->process_data('tb_klarifikasi_terlambat', $data, array('id' => $id));
	}

	function delete_klarifikasi($id='')
	{
		$this->Main_Model->get_login();

		$data_klarifikasi = $this->Main_Model->view_by_id('tb_klarifikasi_absensi', ['id' => $id], 'row');
		$nip = isset($data_klarifikasi->nip) ? $data_klarifikasi->nip : '';
		$tgl = isset($data_klarifikasi->tgl) ? $data_klarifikasi->tgl : '';
		$id_presensi = isset($data_klarifikasi->id_presensi) ? $data_klarifikasi->id_presensi : '';

		$presensi = isset($data_klarifikasi->presensi) ? $data_klarifikasi->presensi : '';

		// get data presensi nya
		$data_presensi = $this->Main_Model->view_by_id('log_presensi', ['id' => $id_presensi], 'row');

		if ($presensi == 'masuk') {
			/*
				masuk 
				1. update tb_absensi flag = 0;
				2. insert into presensi where id_presensi from log_presensi
				3. delete klarifikasi absensi
			*/

			// do update absensi
			$update_absensi = array(
				'flag' => 0
			);

			$this->Main_Model->process_data('tb_absensi', $update_absensi, ['nip' => $nip, 'tgl' => $tgl]);

			// insert into presensi
			$this->Klarifikasi_Model->insert_presensi_from_log($id_presensi);

			// hapus klarifikasi
			$this->Main_Model->delete_data('tb_klarifikasi_absensi', ['id' => $id]);

			$status = 1;
			$message = 'Berhasil menghapus data';

		} else if ($presensi == 'sakit') {
			/*
				sakit
				1. update tb_absensi flag = 0
				2. delete presensi where tgl and nip 
				3. insert into presensi where id_presensi from log_presensi
				4. delete klarifikasi absensi
			*/

			// do update absensi
			$update_absensi = array(
				'flag' => 0
			);

			$this->Main_Model->process_data('tb_absensi', $update_absensi, ['nip' => $nip, 'tgl' => $tgl]);

			// delete presensi aktif
			$this->Main_Model->delete_data('presensi', ['tgl' => $tgl, 'nip' => $nip]);

			// insert into presensi
			$this->Klarifikasi_Model->insert_presensi_from_log($id_presensi);

			// hapus klarifikasi
			$this->Main_Model->delete_data('tb_klarifikasi_absensi', ['id' => $id]);

			$status = 1;
			$message = 'Berhasil menghapus data';

		} else if ($presensi == 'cuti') {
			/*
				cuti
				1. update tb_absensi flag = 0
				2. delete cuti_det and cuti_sub_det 
				3. insert into presensi where id_presensi form log_presensi
				4. delete klarifikasi absensi
			*/

			// do update absensi
			$update_absensi = array(
				'flag' => 0
			);

			$this->Main_Model->process_data('tb_absensi', $update_absensi, ['nip' => $nip, 'tgl' => $tgl]);

			// delete cuti 
			$this->Klarifikasi_Model->delete_cuti($nip, $tgl);

			// insert into presensi
			$this->Klarifikasi_Model->insert_presensi_from_log($id_presensi);

			// hapus klarifikasi
			$this->Main_Model->delete_data('tb_klarifikasi_absensi', ['id' => $id]);

			$status = 1;
			$message = 'Berhasil menghapus data';

		} else if ($presensi == 'ijin_cuti') {
			/*
				cuti
				1. update tb_absensi flag = 0
				2. delete cuti_det and cuti_sub_det 
				3. insert into presensi where id_presensi form log_presensi
				4. delete klarifikasi absensi
			*/

			// do update absensi
			$update_absensi = array(
				'flag' => 0
			);

			$this->Main_Model->process_data('tb_absensi', $update_absensi, ['nip' => $nip, 'tgl' => $tgl]);

			// delete cuti 
			$this->Klarifikasi_Model->delete_cuti($nip, $tgl);

			// insert into presensi
			$this->Klarifikasi_Model->insert_presensi_from_log($id_presensi);

			// hapus klarifikasi
			$this->Main_Model->delete_data('tb_klarifikasi_absensi', ['id' => $id]);

			$status = 1;
			$message = 'Berhasil menghapus data';
		} else if ($presensi == 'cuti_khusus') {
			/*
				cuti khusus 
				1. update tb_absensi flag = 0
				2. delete cuti_khusus where tgl awal or tgl akhir = tgl
				3. insert into presensi where id_presensi form log_presensi
				4. delete klarifikasi absensi
			*/

			// do update absensi
			$update_absensi = array(
				'flag' => 0
			);

			$this->Main_Model->process_data('tb_absensi', $update_absensi, ['nip' => $nip, 'tgl' => $tgl]);

			// delete cuti 
			$this->Klarifikasi_Model->delete_cuti_khusus($nip, $tgl);

			// insert into presensi
			$this->Klarifikasi_Model->insert_presensi_from_log($id_presensi);

			// hapus klarifikasi
			$this->Main_Model->delete_data('tb_klarifikasi_absensi', ['id' => $id]);

			$status = 1;
			$message = 'Berhasil menghapus data';
		} else {
			// do nothing

			$status = 0;
			$message = 'Gagal menghapus data, ulangi beberapa saat lagi';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);

		echo json_encode($result);
	}
}

/* End of file klarifikasi.php */
/* Location: ./application/controllers/klarifikasi.php */ ?>