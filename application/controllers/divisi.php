<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Divisi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Penggajian_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript 	= '
			<script>
					function reset()
					{
						$(".blank").val("");
					}
				'
				 .$this->Main_Model->default_loadtable('divisi/view_divisi')

				 .$this->Main_Model->notif()

				 .$this->Main_Model->post_data('divisi/divisi_process','save()','$("#form_divisi").serialize()','
				 	if(data.status=="true")
				 	{
				 		load_table();
				 		reset();
				 	} 
				 	notif(data.message);')

				 .$this->Main_Model->get_data('divisi/divisi_id','get_id(id)','
				 	$("#myModal").modal();
				 	$("#divisi").val(data.divisi);
				 	$("#keterangan").val(data.keterangan);
				 	$("#id").val(data.id_divisi);')

				 .$this->Main_Model->default_delete_data('divisi/delete_divisi').'
			</script>
		';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','3'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
			);

		$this->load->view('template/header', $header);
        $this->load->view('karyawan/data_divisi');
        $this->load->view('template/footer', $footer);
	}

	function view_divisi()
	{
		$this->Main_Model->get_login();
        $idp   		= $this->session->userdata('idp');
        $data 		= $this->Karyawan_Model->view_divisi($idp);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Divisi','Keterangan','Action');
		$no =1;
        
        foreach ($data as $row) {
		$this->table->add_row(
			$no++,
			$row->divisi,
			$row->keterangan,
			$this->Main_Model->default_action($row->id_divisi)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function divisi_process()
	{
		$this->Main_Model->get_login();
		$id 		= $this->input->post('id');
		$divisi 	= $this->input->post('divisi');
		$keterangan = $this->input->post('keterangan');
		$idp 		= $this->session->userdata('idp');

		if($divisi == "")
		{
			$result = array('status'=>'false','message'=>'Form masih ada yang kosong!');
		}
		else
		{
			$data 		= array(
				'divisi' 		=> $divisi,
				'keterangan' 	=> $keterangan,
				'idp' 			=> $idp
				);
			$result = array('status'=>'true','message'=>'Success!');
			$this->Karyawan_Model->divisi_process($data,$id);
		}
		echo json_encode($result);
	}

	function divisi_id($id)
	{
		$this->Main_Model->get_login();
		$data =	$this->Karyawan_Model->divisi_id($id);
		echo json_encode($data);
	}

	function delete_divisi()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->post('id');
		$data 	= array(
			'status' => 0
			);
		$this->Karyawan_Model->divisi_process($data,$id);
	}

}

/* End of file cabang.php */
/* Location: ./application/controllers/cabang.php */ ?>