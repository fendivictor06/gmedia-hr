<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Informasi extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Absensi_Model', '', TRUE);
		$this->load->model('Tunjangan_Model', '', TRUE);
		$this->load->model('Informasi_Model', '', TRUE);
	}

	function header()
	{
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>';

		return $footer;
	}

	function habiskontrak()
	{
		$this->Main_Model->get_login();
		$javascript = '
			<script type="text/javascript">
    			function load_table(){
					$.ajax({
						url : "'.base_url('informasi/view_kont').'",
						type : "GET",
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			        			responsive: true
			    			});
						},
						error : function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Internal Server Error");
						} 
					})
				}
				load_table();
			</script>';
		$footer =array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','69')
		);
		$this->load->view('template/header',$header);
		$this->load->view('informasi/habiskontrak');
		$this->load->view('template/footer',$footer);
	}

	function view_kont()
	{
		$this->Main_Model->get_login();
		$data 		= $this->Informasi_Model->view_kont();
		$not_exists = $this->Informasi_Model->view_kont_not_exists();
		$template 	= array(
		        'table_open'            => '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">',

		        'thead_open'            => '<thead>',
		        'thead_close'           => '</thead>',

		        'heading_row_start'     => '<tr>',
		        'heading_row_end'       => '</tr>',
		        'heading_cell_start'    => '<th>',
		        'heading_cell_end'      => '</th>',

		        'tbody_open'            => '<tbody>',
		        'tbody_close'           => '</tbody>',

		        'row_start'             => '<tr>',
		        'row_end'               => '</tr>',
		        'cell_start'            => '<td>',
		        'cell_end'              => '</td>',

		        'row_alt_start'         => '<tr>',
		        'row_alt_end'           => '</tr>',
		        'cell_alt_start'        => '<td>',
		        'cell_alt_end'          => '</td>',

		        'table_close'           => '</table>'
		);

		$this->table->set_heading('No','NIP','Nama','Status','No Kontrak','Akhir Kontrak','Action');
		$no=1;
		foreach ($data as $row) {
		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$row->kary_stat,
			$row->no_kontrak,
			$this->Main_Model->format_tgl($row->tgl_akhir),
			'<a class="btn btn-primary btn-xs" href="'.base_url('informasi/kontrak_nip').'/'.$row->nip.'">Pilih</a>'
	        );
		}

		foreach ($not_exists as $row) {
		$this->table->add_row(
			$no++,
			$row->nip,
			$row->nama,
			$row->kary_stat,
			'',
			'',
			'<a class="btn btn-primary btn-xs" href="'.base_url('informasi/kontrak_nip').'/'.$row->nip.'">Pilih</a>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function karyaktif()
	{
		$this->Main_Model->get_login();
		$javascript = '
			<script type="text/javascript">
    			$("#dataTables-example").DataTable({
        			responsive: true
    			});
			</script>';
		$footer =array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$this->load->view('template/header',$this->header());
		$this->load->view('informasi/karyaktif');
		$this->load->view('template/footer',$footer);
	}

	function kontrak_nip($nip='')
	{
		$this->Main_Model->get_login();
		$nip_quote = "'".$nip."'";
		$javascript = '
				<script>
					var save_method;
					'.$this->Main_Model->default_datepicker().'
					function reset() {
						save_method="save";
						$("#tgltetap").addClass("hidden");
					}

					function status_change() {
						if($("#status").val()=="TETAP") {
							$("#tgltetap").removeClass("hidden");
							$(".tanggal").addClass("hidden");
						} else {
							$("#tgltetap").addClass("hidden");
							$(".tanggal").removeClass("hidden");
						}
					}

					function save() {
						var nip = "'.$nip_quote.'";
						var tgl_awal = $("#tgl_awal_u").val();
						var tgl_akhir = $("#tgl_akhir_u").val();
						var id 	= $("#id_u").val();
						var status 	= $("#status_u").val();
						if(tgl_awal == "" || tgl_akhir == "" || status == "") {
							bootbox.alert("Form masih ada yang kosong!");
						} else {
							var dat = {
								"id" : id,
								"nip" : nip,
								"tgl_awal" : tgl_awal,
								"tgl_akhir" : tgl_akhir,
								"status" : status
							}
							var url = "'.base_url('informasi/update_kont').'";
							$.ajax({
								url : url,
								data : dat,
								type : "POST",
								success : function(data){
									bootbox.alert("Success!");
									load_table();
								},
								error : function(jqXHR,textStatus,errorThrown){
									bootbox.alert("Gagal menyimpan data!");
								}
							});
						}
					}

					function get_id(id) {
						save_method = "update";
				    	id = {"id" : id}
				    	$.ajax({
				    		url : "'.base_url('informasi/kontrak_id').'",
				    		type : "GET",
				    		data : id,
				    		success : function(data){
				    			$("#myModal").modal();
				    			var dat = jQuery.parseJSON(data);

				    			var date1 = dat.tgl_awal.split("-");
						    	var NewDate1 = date1[2]+"/"+date1[1]+"/"+date1[0];

						    	var date2 = dat.tgl_akhir.split("-");
						    	var NewDate2 = date2[2]+"/"+date2[1]+"/"+date2[0];

								$("#id_u").val(dat.id_kontrak);
								$("#tgl_awal_u").val(NewDate1);
								$("#tgl_akhir_u").val(NewDate2);
								$("#status_u").val(dat.tipe);
				    		},
				    		error : function(jqXHR, textStatus, errorThrown){
				    			bootbox.alert("Gagal mengambil data!");
				    		}
				    	});
				    }

				    '.$this->Main_Model->default_delete_data(base_url('informasi/delete_kont')).'
				    
					function load_table(){
						var nip = {"nip":'.$nip_quote.'}
						$.ajax({
							url : "'.base_url('informasi/view_kontrak_nip').'",
							data : nip,
							type : "POST",
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-example").DataTable({
				        			responsive: true
				    			});
							},
							error : function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Gagal mengambil data!");
							} 
						});
					}
					load_table();
				</script>
					';
		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','69')
		);
		$this->load->view('template/header',$header);
		$this->load->view('informasi/informasi_kontrak');
		$this->load->view('template/footer',$footer);
	}

	function view_kontrak_nip(){
		$this->Main_Model->get_login();
		$nip 	= $this->input->post('nip');
		$data 	= $this->Informasi_Model->view_kontrak_nip($nip);

		$tbl 	= '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>No Kontrak</th>
							<th>Tanggal Awal</th>
							<th>Tanggal Akhir</th>
							<th>Aktif</th>
							<th>Jenis</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>';
		$i=1;
		foreach ($data as $row) {
		($row->aktif==1) ? $aktif='Ya' : $aktif='Tidak';
		
		$tbl.='
			<tr>
				<td>'.$i++.'</td>
				<td>'.$row->no_kontrak.'</td>
				<td>'.$this->Main_Model->format_tgl($row->tgl_awal).'</td>
				<td>'.$this->Main_Model->format_tgl($row->tgl_akhir).'</td>
				<td>'.$aktif.'</td>
				<td>'.$row->tipe.'</td>
				<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu">
				                    <li>
				                        <a href="javascript:;" onclick="get_id('.$row->id_kontrak.');">
				                            <i class="icon-edit"></i> Update </a>
				                    </li>
				                    <li>
				                        <a href="javascript:;" onclick="delete_data('.$row->id_kontrak.');">
				                            <i class="icon-delete"></i> Delete </a>
				                    </li>
				                </ul>
				        </div>
				</td>
			</tr>';
		}

		$tbl .= '</tbody></table>';
		echo $tbl;
	}	

	function add_kont()
	{
		$this->Main_Model->get_login();
		$nip = $this->input->post('nip');
		$tgl_awal 	= $this->Main_Model->convert_tgl($this->input->post('tgl_awal'));
		$tgl_akhir 	= $this->Main_Model->convert_tgl($this->input->post('tgl_akhir'));

		$last 		= $this->db->query("SELECT * FROM kont ORDER BY id_kontrak DESC")->row();
		$last_no_kontrak = $last->no_kontrak;
		$maks 		= explode("-", $last_no_kontrak);
		$maks_new = $maks[0]+1;
		$no_kontrak = $maks_new.'-'.$nip;

		$cek = $this->db->query("select * from kont where nip='$nip' and aktif='1'")->row();

		if($cek!=""){
			$edit = array(
				'aktif' => 0
				);
			$this->Informasi_Model->edit_aktif($edit,$nip);
			// print_r($cek);
			$data = array(
				'no_kontrak' => $no_kontrak,
				'nip' => $nip,
				'tgl_awal' => $tgl_awal,
				'tgl_akhir' => $tgl_akhir,
				'aktif' => 1
				);
			$this->Informasi_Model->add_kont($data);
		}else{
			$data = array(
				'no_kontrak' => $no_kontrak,
				'nip' => $nip,
				'tgl_awal' => $tgl_awal,
				'tgl_akhir' => $tgl_akhir,
				'aktif' => 1
				);
			$this->Informasi_Model->add_kont($data);
		}
	}

	function kontrak_id()
	{
		$this->Main_Model->get_login();
		$id 	= $this->input->get('id');
		$data 	= $this->Informasi_Model->kontrak_id($id);
		echo json_encode($data);
	}

	function update_kont()
	{
		$this->Main_Model->get_login();
		$tgl_awal 	= $this->Main_Model->convert_tgl($this->input->post('tgl_awal'));
		$tgl_akhir 	= $this->Main_Model->convert_tgl($this->input->post('tgl_akhir'));
		$status 	= $this->input->post('status');
		$nip 		= $this->input->post('nip');
		$data 		= array(
				'tgl_awal' 	=> $tgl_awal,
				'tgl_akhir' => $tgl_akhir,
				'tipe' 		=> $status
			);
		$id 		= $this->input->post('id');
		$data_stat 	= array('kary_stat'=>$status);
		$this->Informasi_Model->ubah_status($data_stat,$nip);
		$this->Informasi_Model->update_kont($data,$id);
	}

	function delete_kont()
	{
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$this->Informasi_Model->delete_kont($id);
	}

	function ubah_status()
	{
		$this->Main_Model->get_login();
		$this->load->library('user_agent');
		$nip 				= $this->input->post('nip');
		$status 			= $this->input->post('status');
		$tgl 				= $this->input->post('tgl_tetap');

		$tgl_awal 			= $this->Main_Model->convert_tgl($this->input->post('tgl_awal'));
		$tgl_akhir 			= $this->Main_Model->convert_tgl($this->input->post('tgl_akhir'));

		$last 				= $this->db->query("SELECT * FROM kont ORDER BY id_kontrak DESC")->row();
		$last_no_kontrak 	= isset($last->no_kontrak) ? $last->no_kontrak : '';
		$maks 				= explode("-", $last_no_kontrak);
		$maks_new 			= $maks[0]+1;
		$no_kontrak 		= $maks_new.'-'.$nip;

		$cek 				= $this->db->query("select * from kont where nip='$nip' and aktif='1'")->row();
		$edit 	= array('aktif' 	=> 0 );

		if($status != "TETAP")
		{
			if($this->input->post('tgl_awal') == "" || $this->input->post('tgl_akhir') == "")
			{
				$this->session->set_flashdata('message_upload', '<script>bootbox.alert("Tgl Tidak boleh kosong!");</script>');
			}
			else
			{
				if($cek!="")
				{
					$this->Informasi_Model->edit_aktif($edit,$nip);
				}
					$data 		= array(
						'no_kontrak'=> $no_kontrak,
						'nip' 		=> $nip,
						'tgl_awal' 	=> $tgl_awal,
						'tgl_akhir' => $tgl_akhir,
						'tipe' 		=> $status,
						'aktif' 	=> 1
						);
					$data_stat 	= array('kary_stat'=>$status);
				$this->Informasi_Model->add_kont($data);
				$this->Informasi_Model->ubah_status($data_stat,$nip);
				$this->session->set_flashdata('message_upload', '<script>bootbox.alert("Success!");</script>');
			}
		}
		else
		{
			if($this->input->post('tgl_tetap') == "")
			{
				$this->session->set_flashdata('message_upload', '<script>bootbox.alert("Tgl Tidak boleh kosong!");</script>');
			}
			else
			{
				if($cek!="")
				{
					$this->Informasi_Model->edit_aktif($edit,$nip);
				}
				$data 	= array('kary_stat'=>$status,'tgl_tetap'=>$this->Main_Model->convert_tgl($tgl));

				$this->Informasi_Model->ubah_status($data,$nip);
				$this->session->set_flashdata('message_upload', '<script>bootbox.alert("Success!");</script>');
			}
		}

		redirect($this->agent->referrer());
	}

	function biaya_rekrut()
	{
		$this->Main_Model->get_login();
		$javascript = '
			<script>
				var save_method;
				$(document).ready(function() {
  					$(".select2").select2({
						width: "100%"
  					});
				});

				$(".date-picker").datepicker({
                	rtl: App.isRTL(),
                	orientation: "left",
                	autoclose: !0
            	});

				function reset() {
					$("#jenis").val("");
					$("#tgl").val("");
					$("#jumlah").val("");
					$("#keterangan").val("");
					$("#nip").val("").trigger("change");
					$("#tgl_kontrak").val("");
					$("#durasi").val("");
					$("#tgl_akhir").val("");
					save_method = "save";
				}

				$("#simpan-data").click(function(){
					let formData = $("#rekrutasi").serialize();
					let urlSimpan = (save_method == "save") ? "'.base_url('informasi/add_rekrut').'" : "'.base_url('informasi/update_rekrut').'";

					$.ajax({
						url: urlSimpan,
						type: "post",
						data: formData,
						dataType: "json",
						success: function(data) {
							if (data.status == 1) {
								load_table();
								toastr.success(data.message);
								reset();
								$("#myModal").modal("toggle");
							} else {
								toastr.error(data.message);
							}
						},
						error: function() {
							toastr.error("Terjadi kesalahan saat menyimpan data");
						}
					});
				});

				function delete_data(id) {
			    	id = {
			    		"id": id
			    	}

			    	bootbox.dialog({
			    		message : "Yakin ingin menghapus data?",
			    		title : "Hapus Data",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('informasi/delete_rekrut') . '",
							    		type : "POST",
							    		data : id,
							    		success : function(data){
							    			bootbox.alert({
												message: "Delete Success",
												size: "small"
											});
							    			load_table();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})

			    }

				function get_id(id){
					save_method = "update";
			    	id = {
			    		"id": id
			    	}

			    	$.ajax({
			    		url: "'.base_url('informasi/biaya_rekrut_id') . '",
			    		type: "get",
			    		data: id,
			    		dataType: "json",
			    		success : function(data){
			    			$("#myModal").modal();
							$("#jenis").val(data.jenis);
							$("#tgl").datepicker("setDate", data.tgl);
							$("#nip").val(data.nip).trigger("change");
							$("#tgl_akhir").datepicker("setDate", data.tgl_akhir);
							$("#tgl_kontrak").datepicker("setDate", data.tgl_kontrak);
							$("#keterangan").val(data.keterangan);
							$("#durasi").val(data.durasi);
							$("#jumlah").val(data.jumlah);
							$("#id").val(data.id);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			alert("Internal Server Error");
			    		}
			    	});
			    }

				function load_table(){
					$.ajax({
						url : "'.base_url('informasi/view_rekrut').'",
						type : "GET",
						success :  function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
        						responsive: true
    						});
							$("#warning").addClass("hidden");
							$("#success").removeClass("hidden");
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Internal Server Error");
						}
					});
				}
				load_table();

				$("#durasi, #tgl_kontrak").change(function(){
					let tgl = $("#tgl_kontrak").val();
					let durasi = $("#durasi").val();

					$.ajax({
						url: "'.base_url('informasi/hitung_akhir_kontrak').'?tgl="+tgl+"&durasi="+durasi,
						dataType: "json",
						success: function(data) {
							$("#tgl_akhir").val(data.hasil);
						},
						error: function() {
							toastr.error("Terjadi kesalahan saat menghitung tanggal kontrak");
						}
					});
				});
			</script>';

		$data = array(
			'nip' => $this->Main_Model->all_kary_active(),
			'jenis' => array(
					'1' => 'Rekrutasi',
					'2' => 'Training & Workshop'
				)
		);

		$footer =array(
			'javascript' => $javascript,
			'js' => $this->footer()
		);

		$menu = array(
			'menu' => $this->Main_Model->menu_admin('0','0','6'),
			'style' => '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">'
		);

		$this->load->view('template/header',$menu);
		$this->load->view('informasi/biaya_rekrut',$data);
		$this->load->view('template/footer',$footer);
	}

	function hitung_akhir_kontrak()
	{
		$tgl = $this->input->get('tgl');
		$durasi = $this->input->get('durasi');
		$durasi = ($durasi == '') ? 0 : $durasi;

		$hasil = date_interval($tgl, $durasi, 'MONTH', '+');

		echo json_encode(['hasil' => tanggal($hasil)]);
	}

	function view_rekrut() 
	{
		$this->Main_Model->get_login();
		$data = $this->Informasi_Model->view_rekrut();
		$template = $this->Main_Model->tbl_temp();

		$this->table->set_heading('No','Tanggal','Jenis', 'Nama', 'Tgl Awal', 'Tgl Akhir', 'Jumlah','Keterangan','Action');
		$no = 1;
		foreach ($data as $row) {
			$jenis = ($row->jenis == 1) ? 'Rekrutasi' : 'Training & Workshop';
			$tgl = ($row->tgl) ? tanggal($row->tgl) : '';
			$tgl_kontrak = ($row->tgl_kontrak) ? tanggal($row->tgl_kontrak) : '';
			$tgl_akhir = ($row->tgl_akhir) ? tanggal($row->tgl_akhir) : '';

			$this->table->add_row(
				$no++,
				$tgl,
				$jenis,
				$row->nama, 
				$tgl_kontrak,
				$tgl_akhir,
				$this->Main_Model->uang($row->jumlah),
				$row->keterangan,
				'<div class="btn-group">
		            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
		                <i class="fa fa-angle-down"></i>
		            </button>
		                <ul class="dropdown-menu" role="menu">
		                    <li>
		                        <a href="javascript:;" onclick="get_id(' . $row->id . ');">
		                            <i class="icon-edit"></i> Update </a>
		                    </li>
		                    <li>
		                        <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
		                            <i class="icon-delete"></i> Delete </a>
		                    </li>
		                </ul>
		        </div>'
		    );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function add_rekrut(){
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$tgl = $this->input->post('tgl');
		$tgl_kontrak = $this->input->post('tgl_kontrak');
		$durasi = $this->input->post('durasi');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$nip = $this->input->post('nip');
		$jenis = $this->input->post('jenis');
		$username = username();

		# convert to Y-m-d
		$tgl = $this->Main_Model->convert_tgl($tgl);
		$tgl_kontrak = $this->Main_Model->convert_tgl($tgl_kontrak);
		$tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

		$data = array(
			'jenis' => $jenis,
			'nip' => $nip,
			'tgl' => $tgl,
			'tgl_kontrak' => $tgl_kontrak,
			'durasi' => $durasi,
			'tgl_akhir' => $tgl_akhir,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'user_insert' => $username
		);

		$simpan = $this->Informasi_Model->add_rekrut($data);
		if ($simpan > 0) {
			$status = 1;
			$message = 'Data berhasil disimpan';
		} else {
			$status = 0;
			$message = 'Gagal menyimpan data';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);

		echo json_encode($result);
	}

	function update_rekrut(){
		$this->Main_Model->get_login();
		$id = $this->input->post('id');
		$tgl = $this->input->post('tgl');
		$tgl_kontrak = $this->input->post('tgl_kontrak');
		$durasi = $this->input->post('durasi');
		$tgl_akhir = $this->input->post('tgl_akhir');
		$jumlah = $this->input->post('jumlah');
		$keterangan = $this->input->post('keterangan');
		$nip = $this->input->post('nip');
		$jenis = $this->input->post('jenis');
		$username = username();
		$now = now();

		# convert to Y-m-d
		$tgl = $this->Main_Model->convert_tgl($tgl);
		$tgl_kontrak = $this->Main_Model->convert_tgl($tgl_kontrak);
		$tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

		$data = array(
			'jenis' => $jenis,
			'nip' => $nip,
			'tgl' => $tgl,
			'tgl_kontrak' => $tgl_kontrak,
			'durasi' => $durasi,
			'tgl_akhir' => $tgl_akhir,
			'jumlah' => $jumlah,
			'keterangan' => $keterangan,
			'user_update' => $username,
			'update_at' => $now
		);

		$simpan = $this->Informasi_Model->update_rekrut($data, $id);
		if ($simpan) {
			$status = 1;
			$message = 'Data berhasil disimpan';
		} else {
			$status = 0;
			$message = 'Gagal menyimpan data';
		}

		$result = array(
			'status' => $status,
			'message' => $message
		);

		echo json_encode($result);
	}

	function biaya_rekrut_id(){
		$this->Main_Model->get_login();
		$id = $this->input->get('id');
		$data = $this->Informasi_Model->biaya_rekrut_id($id);

		echo json_encode($data);
	}

	function delete_rekrut(){
 		$this->Main_Model->get_login();
 		$id = $this->input->post('id');
 		$this->Informasi_Model->delete_rekrut($id);
 	}
}