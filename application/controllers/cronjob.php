<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');

class Cronjob extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Cronjob_Model');
		$this->load->model('Main_Model');
	}

	function index()
	{
		$cronjob = $this->Main_Model->view_by_id('ms_cronjob', ['active' => 1], 'result');

		if ($cronjob) {

			foreach ($cronjob as $row) {
				$url = $row->url;
				$tipe = $row->tipe;
				$jam = $row->jam;
				$menit = $row->menit;
				$tgl = $row->tgl;
				$bulan = $row->bulan;
				$time = date('H:i:s');
				$minute = date('i');
				$date = date('d');
				$today = date('Y-m-d');
				$now = date('Y-m-d H:i:s');
				$output = '';

				switch ($tipe) {
					case 'jam':
						$waktu_eksekusi = $today.' '.$jam;
						$is_execute = $this->Cronjob_Model->is_execute($url, $waktu_eksekusi);

						if ($is_execute == false && strtotime($time) >= strtotime($jam)) {
							$ch = curl_init(); 
						    curl_setopt($ch, CURLOPT_URL, $url);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						    $output = curl_exec($ch); 
						    curl_close($ch); 

						    $this->Main_Model->process_data('log_trx_api', [
						    	'request' => $url, 
						    	'response' => $output, 
						    	'header' => '', 
						    	'user' => 'CRONJOB', 
						    	'nip' => 'CRONJOB'
						    ]); 
						}

						break;

					case 'permenit':
						if ((int) $minute % $menit == 0) {
							$ch = curl_init(); 
						    curl_setopt($ch, CURLOPT_URL, $url);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						    $output = curl_exec($ch); 
						    curl_close($ch); 

						    $this->Main_Model->process_data('log_trx_api', [
						    	'request' => $url, 
						    	'response' => $output, 
						    	'header' => '', 
						    	'user' => 'CRONJOB', 
						    	'nip' => 'CRONJOB'
						    ]); 
						}

						break;

					case 'pertgl':
						$waktu_eksekusi = $today.' '.$jam;
						$is_execute = $this->Cronjob_Model->is_execute($url, $waktu_eksekusi);

						if ($date == $tgl && $is_execute == false) {
							$ch = curl_init(); 
						    curl_setopt($ch, CURLOPT_URL, $url);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
						    $output = curl_exec($ch); 
						    curl_close($ch); 

						    $this->Main_Model->process_data('log_trx_api', [
						    	'request' => $url, 
						    	'response' => $output, 
						    	'header' => '', 
						    	'user' => 'CRONJOB', 
						    	'nip' => 'CRONJOB'
						    ]); 
						}

						break;
					
					default:
						echo 'No Cronjob to execute';
						break;

				}    

			}
		}
	}

}

/* End of file cronjob.php */
/* Location: ./application/controllers/cronjob.php */ ?>