<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perdin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Absensi_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->Main_Model->session_cabang();

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker()
		      .$this->Main_Model->js_bs_select()
		      .$this->Main_Model->style_datetimepicker();

		$menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','6') : $this->Main_Model->menu_user('0','0','81');

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
				      .$this->Main_Model->style_bs_select()
				      .$this->Main_Model->js_datetimepicker()
			);
		$nip_opt = array();
		// $nip = (empty($sess_cabang)) ? $this->Main_Model->kary_active() : $this->Main_Model->kary_cabang($sess_cabang, 'q');
		// if($nip) {
		// 	foreach($nip as $row) {
		// 		$nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
		// 	} 
		// }

		$cabang_create = array();
		if(!empty($sess_cabang)) {
			foreach($sess_cabang as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang_create[$d->id_cab] = $d->cabang;
			}
		} else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang_create[$row->id_cab] = $row->cabang;
			}
		}
		
		$cabang_opt = array();
		// if(empty($sess_cabang)) {
		// 	$cabang = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
		// 	foreach($cabang as $row) {
		// 		$cabang_opt[$row->id_cab] = $row->cabang;
		// 	}
		// } else {
		// 	foreach($sess_cabang as $r) {
		// 		$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
		// 		$cabang_opt[$d->id_cab] = $d->cabang;
		// 	}
		// }

		$cabang = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
		if($cabang) {
			foreach($cabang as $row) {
				$cabang_opt[$row->id_cab] = $row->cabang;
			}
		}

		// $cabang = (empty($sess_cabang)) ? $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result') : $this->Main_Model->view_by_id('ms_cabang', array('status' => 1, 'id_cab !=' => $sess_cabang), 'result');
		// if($cabang) {
		// 	foreach($cabang as $row) {
		// 		$cabang_opt[$row->id_cab] = $row->cabang;
		// 	}
		// } else {
		// 	$cabang_opt = array();
		// }

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'nip' => $nip_opt,
			'cabang' => $cabang_opt,
			'cabang_create' => $cabang_create,
			'periode' => $this->Main_Model->periode_opt()
			);

		($sess_cabang == 0) ? $this->load->view('template/header', $header) : $this->load->view('akun/header', $header);
		$this->load->view('absensi/form_perdin',$data);
	}

	function hitung_nominal() 
	{
		$sess_cabang = $this->session->userdata('cabang');
		$this->Main_Model->all_login();

		$start = $this->input->post('start');
		$end = $this->input->post('end');

		$ex_start = explode(' ', $start);
		$ex_end = explode(' ', $end);

		$tgl_start = $ex_start[0];
		$tgl_end = $ex_end[0];

		$jam_start = end($ex_start);
		$jam_end = end($ex_end);

		$q = $this->Main_Model->view_by_id('ms_perdin', array('default' => 1), 'row');
		$fixed_nominal = isset($q->nominal) ? $q->nominal : '0';

		if($tgl_start == $tgl_end) {
			if(strtotime($jam_start) < strtotime('08:00')) {
				$w = $this->Main_Model->view_by_id('ms_perdin', array('jam' => '08:00:00'), 'row');
				$nominal_pagi = isset($w->nominal) ? $w->nominal : '0';
				$fixed_nominal = $fixed_nominal + $nominal_pagi;
			}

			if(strtotime($jam_end) > strtotime('18:00')) {
				$e = $this->Main_Model->view_by_id('ms_perdin', array('jam' => '18:00:00'), 'row');
				$nominal_sore = isset($e->nominal) ? $e->nominal : '0';
				$fixed_nominal = $fixed_nominal + $nominal_sore;
	 		}
		} else {
			if(strtotime($jam_start) < strtotime('08:00')) {
				$w = $this->Main_Model->view_by_id('ms_perdin', array('jam' => '08:00:00'), 'row');
				$nominal_pagi = isset($w->nominal) ? $w->nominal : '0';
				$fixed_nominal = $fixed_nominal + $nominal_pagi;
			}

			$e = $this->Main_Model->view_by_id('ms_perdin', array('jam' => '18:00:00'), 'row');
			$nominal_sore = isset($e->nominal) ? $e->nominal : '0';
			$fixed_nominal = $fixed_nominal + $nominal_sore;

	 		$r = $this->Main_Model->view_by_id('ms_perdin', array('jam' => '24:00:00'), 'row');
 			$nominal_malam = isset($r->nominal) ? $r->nominal : '0';
 			$fixed_nominal = $fixed_nominal + $nominal_malam;
		}

 		$result = array('nominal' => $fixed_nominal);

 		echo json_encode($result);
	}

	function view_perdin()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->session->userdata('cabang');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$id_periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Absensi_Model->view_perjalanan_dinas($sess_cabang, $id_periode);

		$template = $this->Main_Model->tbl_temp('tb_perdin');
		$this->table->set_heading('No', 'Nama', 'No Bukti', 'Berangkat', 'Pulang', 'Tujuan', 'Keperluan', 'Nominal', 'Action');
		$no = 1;
		foreach($data as $row) {
			$action = array(
					// array('label' => 'Detail', 'event' => "detail('".$row->nobukti."')"),
					array('label' => 'Update', 'event' => "get_id('".$row->nobukti."')"),
					array('label' => 'Delete', 'event' => "delete_data('".$row->id_perdin."')")
				);

			// if($row->id_cabang == 0 && $row->verifikasi == 0) {
			// 	$action[] = array('label' => 'Verifikasi', 'event' => "verif('".$row->id_perdin."')");
			// }

			$verif = ($row->verifikasi == 1) ? 'Sudah' : 'Belum';
			$this->table->add_row(
				$no++,
				$row->nama,
				$row->nobukti,
				$row->berangkat,
				$row->pulang,
				$row->tujuan,
				$row->keperluan,
				$this->Main_Model->uang($row->total),
				$this->Main_Model->action($action)
			);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function perdin_process() 
	{
		$this->Main_Model->all_login();
		// $sess_cabang = $this->session->userdata('cabang');

		$id = $this->input->post('id');
		$nip = $this->input->post('nip');
		$waktu_berangkat = $this->input->post('waktu_berangkat');
		$waktu_pulang = $this->input->post('waktu_pulang');
		$nominal = $this->input->post('nominal');
		$tujuan = $this->input->post('tujuan');
		$cabang = $this->input->post('cabang');
		$keperluan = $this->input->post('keperluan');
		$dinas = $this->input->post('dinas');
		$idp = $this->session->userdata('idp');
		$cabang_create = $this->input->post('cabang_create');

		if($nip == '' || $waktu_berangkat == '' || $waktu_pulang == '' || $nominal == '' || $keperluan == '') {
			$result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
		} else {
			if($dinas == 0 && $cabang == '') {
				$result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
			} elseif($dinas == 1 && $tujuan == '') {
				$result = array('status' => false, 'message' => 'Form masih ada yang kosong!');
			} else {

				$cab = $this->Main_Model->view_by_id('perdin', array('id_perdin' => $id), 'row');
				$cabang_lama = isset($cab->id_cabang) ? $cab->id_cabang : '';

				if($id == '') {
					// insert
					if($tujuan == '') {
						$nobukti = $this->Main_Model->bukti_perdin($cabang);
					} else {
						$nobukti = $this->Main_Model->bukti_perdin();
					}
				} else {
					// update 
					if($tujuan == '') {
						if($cabang_lama == $cabang) {
							$nobukti = $this->input->post('nobukti');
						} else {
							$nobukti = $this->Main_Model->bukti_perdin($cabang);
						}
					} else {
						$nobukti = $this->input->post('nobukti');
					}
				}

				// $tgl_a = explode(' ', $waktu_berangkat);
				// $tgl_b = explode(' ', $waktu_pulang);

				if($tujuan != '') {
					$real_tujuan = $tujuan;
					$cabang = 0;
				} else {
					$r = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $cabang), 'row');
					$real_tujuan = isset($r->cabang) ? $r->cabang : '';
					$cabang = $cabang;
				}

				for($i=0; $i<count($nip); $i++) {
					$n[$i] = $nip[$i];
				}

				$jumlah_orang = count($n);
				$total = $nominal * $jumlah_orang;

				$at = ($id) ? 'update_at' : 'insert_at';
				$us = ($id) ? 'user_update' : 'user_insert';

				$user = $this->session->userdata('username');
 				$waktu_berangkat = $this->Main_Model->convert_tgl($waktu_berangkat);
 				$waktu_pulang = $this->Main_Model->convert_tgl($waktu_pulang);

				$perdin_h = array(
					'nobukti' => $nobukti,
					'tgl_brgkt' => $waktu_berangkat,
					'tgl_pulang' => $waktu_pulang,
					'tujuan' => $real_tujuan,
					'keperluan' => $keperluan,
					'nominal' => $nominal,
					'idp' => $idp,
					'id_cabang' => $cabang,
					'total' => $total,
					'cabang_create' => $cabang_create,
					'verifikasi' => 1,
					$at => date('Y-m-d H:i:s'),
					$us => $user
 				);

				($id) ? $condition = array('id_perdin' => $id) : $condition = array();

				$this->Main_Model->process_data('perdin', $perdin_h, $condition);

				if($id) {
					$this->Main_Model->delete_data('perdin_det', array('nobukti' => $this->input->post('nobukti')));
				}

				for($l=0; $l<count($n); $l++) {
					$data_d[$l] = array(
						'nobukti' => $nobukti,
						'nip' => $n[$l]
					);
					$this->Main_Model->process_data('perdin_det', $data_d[$l]);
					eksekusi_absen($waktu_berangkat, $waktu_pulang, $n[$l]);
				}

				// $this->Main_Model->approval_perdin($nobukti);
				
				$result = array('status' => true, 'message' => 'Success!');
			}
		}
		echo json_encode($result);
	}

	function delete_perdin($id='')
	{
		$sess_cabang = $this->session->userdata('cabang');
		$this->Main_Model->all_login();

		$check = $this->Main_Model->view_by_id('perdin', array('id_perdin' => $id), 'row');
		// if($check->verifikasi == 1) {
		// 	$result = array('status' => false, 'message' => 'Data yang sudah diverifikasi tidak boleh dihapus!');
		// } else {
			$nobukti = isset($check->nobukti) ? $check->nobukti : '';

			$this->Main_Model->delete_data('perdin', array('id_perdin' => $id));
			$this->Main_Model->delete_data('perdin_det', array('nobukti' => $nobukti));

			$result = array('status' => true, 'message' => 'Success!');
		// }
		echo json_encode($result);
	}

	function perdin_id()
	{
		$this->Main_Model->all_login();
		$nobukti = $this->input->get('nobukti');
		$data = $this->Absensi_Model->perdin_detail($nobukti);
		echo json_encode($data);
	}

	function view_detail_perdin()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->session->userdata('cabang');
		$id = $this->input->get('nobukti');
		

		$data = $this->Absensi_Model->perdin_detail($id);

		$template = $this->Main_Model->tbl_temp('tb_perdin_detail');
		$this->table->set_heading('No', 'No Bukti', 'NIP', 'Nama', 'Nominal');
		$no = 1;
		foreach($data as $row) {
			$this->table->add_row(
				$no++,
				$row->nobukti,
				$row->nip,
				$row->nama,
				$this->Main_Model->uang($row->nominal)
			);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function verifikasi($id='')
	{
		$cabang = $this->session->userdata('cabang');
		$this->Main_Model->all_login();

		if($id) {
			$condition = array('id_perdin' => $id);
			$this->Main_Model->process_data('perdin', array('verifikasi' => 1), array('id_perdin' => $id));

			// $this->Main_Model->verif_perdin($id);

			$result = array('status' => true, 'message' => 'Verifikasi Berhasil!');
		} else {
			$result = array('status' => false, 'message' => 'Verifikasi Gagal!');
		}

		echo json_encode($result);
	}

	function carikaryawan($id_cabang='') 
	{
		$this->Main_Model->all_login();
		$data = $this->Main_Model->json_karyawan($id_cabang);

		echo json_encode($data);
	}

}

/* End of file perdin.php */
/* Location: ./application/controllers/perdin.php */ ?>