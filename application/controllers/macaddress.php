<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Macaddress extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model');
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript = base_url('assets/js/app/setting/macaddress.js');
        $this->session->set_userdata('js', $javascript);

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2();

		$menu = $this->Main_Model->menu_admin('0','0','3');
		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
		);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'base_url' => base_url()
		);

		$this->load->view('template/header', $header);
		$this->load->view('setting/form_macaddress',$data);
	}

	function view_macaddress() 
	{
		$this->Main_Model->get_login();
		$is_ajax = $this->input->is_ajax_request();
		if ($is_ajax) {
			$data = $this->Main_Model->view_by_id('ms_allowed_ip', array('status' => 1), 'result');

			$template = $this->Main_Model->tbl_temp();
			$arr_heading = array('#','SSID', 'Mac Address', 'IP', 'Action');
			$this->table->set_heading($arr_heading);

			$no = 1;
			foreach ($data as $row) {
				$arr_data = array(
					$no++,
					$row->ssid,
					$row->macaddress,
					$row->ippublic,
					$this->Main_Model->default_action($row->id)
				);

				$this->table->add_row($arr_data);
			}

			$this->table->set_template($template);
			echo $this->table->generate();
		} else {
			show_404();
		}
	}

	function process_mac()
	{
		$this->Main_Model->get_login();
		$is_ajax = $this->input->is_ajax_request();
		if ($is_ajax) {
			$id = $this->input->post('id');
			$ssid = $this->input->post('ssid');
			$mac = $this->input->post('mac');
			$ip = $this->input->post('ip');

			$message = '';
			if ($ssid == '' || $mac == '' || $ip == '') {
				if ($ssid == '') $message .= ' Field SSID harus diisi <br>';
				if ($mac == '') $message .= ' Field Mac Address harus diisi <br>';
				if ($ip == '') $message .= ' Field IP Public harus diisi <br>';
				$status = false; 
			} else {
				$time = ($id) ? 'update_at' : 'insert_at';
				$user = ($id) ? 'user_update' : 'user_insert';
				$data = array(
					'ssid' => $ssid,
					'macaddress' => $mac,
					'ippublic' => $ip,
					$time => now(),
					$user => username()
				);

				$condition = ($id) ? array('id' => $id) : '';
				$simpan = $this->Main_Model->process_data('ms_allowed_ip', $data, $condition);
				if ($id) {
					$status = true;
					$message = 'Success!';
				} else {
					if ($simpan > 0) {
						$status = true;
						$message = 'Success!';
					} else {
						$status = false;
						$message = 'Gagal menyimpan data';
					}
				}
			}

			$result = array('status' => $status, 'message' => $message);
			echo json_encode($result);
		} else {
			show_404();
		}
	}

	function mac_id($id='')
	{
		$this->Main_Model->get_login();
		$is_ajax = $this->input->is_ajax_request();
		if ($is_ajax) {
			$data = $this->Main_Model->view_by_id('ms_allowed_ip', array('id' => $id), 'row');
			echo json_encode($data);
		} else {
			show_404();
		}
	}

	function mac_delete($id='')
	{
		$this->Main_Model->get_login();
		$is_ajax = $this->input->is_ajax_request();
		if ($is_ajax) {
			$data = array(
				'status' => 0,
				'update_at' => now(),
				'user_update' => username()
			);
			
			$delete = $this->Main_Model->process_data('ms_allowed_ip', $data, array('id' => $id));
			if ($delete > 0) {
				$status = true;
				$message = 'Success!';
			} else {
				$status = false;
				$message = 'Gagal menghapus data';
			}

			$result = array('status' => $status, 'message' => $message);
			echo json_encode($result);
		} else {
			show_404();
		}
	}
}

/* End of file macaddress.php */
/* Location: ./application/controllers/macaddress.php */