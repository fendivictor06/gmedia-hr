<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_Absensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Cuti_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->get_login();
		$javascript 	= '';
		$header 		= array(
				'menu' => $this->Main_Model->menu_admin('0','0','4'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_fileinput()
			);
		$footer 		= array(
				'javascript' 	=> $javascript,
				'js' 			=> $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_fileinput()
				                  .$this->Main_Model->js_bootbox()
			);

		$this->load->view('template/header', $header);
        $this->load->view('absensi/upload_absensi');
        $this->load->view('template/footer', $footer);
	}

}

/* End of file upload_absensi.php */
/* Location: ./application/controllers/upload_absensi.php */ ?>