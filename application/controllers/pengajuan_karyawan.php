<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengajuan_Karyawan extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Penggajian_Model', '', TRUE);
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->guest_login();
		$javascript = '
			<script>
					function reset() {
						$(".blank").val("");
						$(".select2").val("").trigger("change");
					}

					function load_table() {
						var tahun = $("#tahun").val();
						var bulan = $("#bulan").val();
						$.ajax({
							url : "'.base_url('pengajuan_karyawan/view_pengajuan_karyawan').'",
							data : {
				                "tahun" : tahun,
				                "bulan" : bulan
				            },
							beforeSend : function(){
								$("#tampil").addClass("hidden");
								$("#imgload").removeClass("hidden");
							},
							complete : function(){
								$("#tampil").removeClass("hidden");
								$("#imgload").addClass("hidden");
							},
							success : function(data){
								$("#myTable").html(data);
								'.$this->Main_Model->default_datatable().'
							}
						});
					}
					load_table();

					$("#tampil").click(function(){
	                    load_table();
	                });'

				 .$this->Main_Model->default_datepicker()

				 .$this->Main_Model->notif()

				 .$this->Main_Model->post_data('pengajuan_karyawan/pengajuan_karyawan_process','save()','$("#form_pengajuan_karyawan").serialize()','
				 	if(data.status==true)
				 	{
				 		load_table();
				 		reset();
				 	} 
				 	notif(data.message);')

				 .$this->Main_Model->get_data('pengajuan_karyawan/id_pengajuan_kary','get_id(id)','
				 	$("#myModal").modal();
				 	$("#posisi").val(data.posisi).trigger("change");
				 	$("#tgl_pengajuan").val(data.tanggal_pengajuan);
				 	$("#cabang").val(data.id_cabang).trigger("change");
				 	$("#sal_pos").val(data.sal_pos).trigger("change");
				 	$("#jumlah").val(data.jumlah);
				 	$("#id").val(data.id);
				 	$("#spesifikasi").val(data.spesifikasi);')

				 .$this->Main_Model->default_delete_data('pengajuan_karyawan/delete_pengajuan_kary').'

				 $(document).ready(function(){
				 	$(".select2").select2();
				 });

				 function change(id, val) {
			        bootbox.dialog({
			            message : "Apakah Calon Karyawan sudah ttd?",
			            title : "TTD Pengajuan Karyawan",
			            buttons :{
			                success : {
			                    label : "Sudah",
			                    className : "green",
			                    callback : function(){
			                        $.ajax({
			                            url : "'.base_url('pengajuan_karyawan/sudah_ttd').'/"+id+"/"+val,
			                            success : function() {
			                                bootbox.alert("Status telah diubah !");
			                                load_table();
			                            }
			                        })
			                    }    
			                },
			                
			                main : {
			                    label : "Belum",
			                    className : "blue",
			                    callback : function(){
			                        return true;
			                    }
			                }
			            }
			        });
			    }

			    function unduh(id) {
			    	$("#Mdownload").modal();
			    	document.getElementById("fdownload").reset();
			    	$("#id_file").val(id);
			    	$("#file_nip").find("option").remove().end();
			    	$.getJSON("'.base_url('download_file/cari_nip').'/"+id, function(jsonResult){
			    		$("#file_nip").attr("enabled","true");
			    		if (jsonResult != null) {
				    		$.each(jsonResult, function(){
				    			$("#file_nip").append(
				                    $("<option></option>").text(this.nama).val(this.nip)
				                );
				    		});
			    		}
			    	});
			    }

			    function simpan() {
			    	tgl_mulai = $("#tgl_mulai").val();
			    	tgl_akhir = $("#tgl_akhir").val();
			    	if (tgl_mulai == "" || tgl_akhir == "") {
			    		bootbox.alert("Tanggal Mulai & Akhir tidak boleh kosong!");
			    	} else {
				    	data = $("#fdownload").serialize();
				    	window.open(
				    		"'.base_url('download_file/file_finalisasi').'?"+data,
				    		"_blank"
				    	);
				    }
			    }
			</script>
		';
		$header = array(
				'menu' => $this->Main_Model->menu_user('0','0','80'),
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_datepicker()
				          .$this->Main_Model->style_select2()
			);
		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				                  .$this->Main_Model->js_modal()
				                  .$this->Main_Model->js_bootbox()
				                  .$this->Main_Model->js_datepicker()
				                  .$this->Main_Model->js_select2()
			);
		$idp = $this->session->userdata('idp');
		$q = $this->Main_Model->session_cabang();
		$cabang = array();
		if($q) {
			foreach($q as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang[$d->id_cab] = $d->cabang;
			}
		}

		$w = $this->Main_Model->session_pengajuan();
		$level = array();
		if($w) {
			foreach($w as $r) {
				$level[$r] = $r;
			}
		}

		$condition = array('status' => 1);
		$job = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'result');
		$job_opt = array();
		if (!empty($job)) {
			foreach ($job as $row) {
				$job_opt[$row->jab] = $row->jab;
			}
		}

		$data = array(
				'periode' => $this->Main_Model->periode_opt(),
				'sal_pos' => $level,
				'cabang' => $cabang,
				'job' => $job_opt
			);

		$this->load->view('akun/header', $header);
        $this->load->view('akun/pengajuan_karyawan', $data);
        $this->load->view('akun/footer', $footer);
	}

	function view_pengajuan_karyawan()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Karyawan_Model->view_pengajuan_karyawan($qd_id);
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Cabang','Posisi','Pengajuan','Jml','Spesifikasi','Pemenuhan','Atas Nama','Status',
        	// 'Status HR','Progress','Status TTD',
        	'Action');
		$no =1;
        foreach ($data as $row) {
        	$q = $this->db->query("
        		SELECT a.*,b.nama 
        		FROM d_pengajuan_kary a 
        		JOIN kary b ON a.nip = b.nip 
        		WHERE a.id = '$row->id'")->result();

        	$tgl = '';
        	$nip = '';
        	if($q) {
        		$i = 1;
        		foreach($q as $r) {
        			$tgl .= $this->Main_Model->format_tgl($r->tgl_pemenuhan).'<br>';
        			$nip .= $r->nama.'<br>';
        		}
        	} else {
        		$tgl = 'ON PROGRESS';
        		$nip = '';
        	}

    	    // switch ($row->status) {
	        // 	case '1': $status = 'Disetujui'; break;
	        // 	case '2': $status = 'Ditolak'; break;
	        // 	default: $status = 'Proses'; break;
	        // }
        $edit = array('label' => 'Update', 'event' => "get_id('".$row->id."')");
        $delete = array('label' => 'Delete', 'event' => "delete_data('".$row->id."')");
	    $action = array($edit);

	    $acctype = $this->session->userdata('acctype');
	    if ($acctype == 'Administrator') $action[] = $delete;

	    $ttd = array('label' => 'Sudah TTD ?', 'event' => "change('".$row->id."', '10')");
	    $download = array('label' => 'Download File', 'event' => "unduh('".$row->id."')");
	    if ($row->progress == '5') {
	    	$action[] = $ttd;
	    	$action[] = $download;
	    }
	    $sal_pos = ($row->sal_pos != '') ? '('.$row->sal_pos.') ' : '';

		$this->table->add_row(
			$no++,
			$row->cabang,
			$sal_pos.$row->posisi,
			($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '',
			$row->jumlah,
			$row->spesifikasi,
			$tgl,
			$nip,
			array('style' => 'width:150px;', 'data' => '<strong>Status</strong> : '.$row->keterangan.'<br>'.
			'<strong>Status HR</strong> : '.$row->ket_hr.'<br>'.
			'<strong>Progress</strong> : '.$row->ket_progress.'<br>'.
			'<strong>TTD</strong> : '.$row->ket_ttd),
			$this->Main_Model->action($action)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function pengajuan_karyawan_process()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$posisi = $this->input->post('posisi');
		$tgl_pengajuan = $this->input->post('tgl_pengajuan');
		$jumlah = $this->input->post('jumlah');
		$cabang = $this->input->post('cabang');
		$spesifikasi = $this->input->post('spesifikasi');
		$sal_pos = $this->input->post('sal_pos');

		if($cabang) {
			$cabang = $cabang;
		}
		else {
			if($this->input->post('idcabang')) {
				$cabang = $this->input->post('idcabang');
			} else {
				$cabang = $this->session->userdata('cabang');
			}
		}

		if($posisi == '' || $tgl_pengajuan == '' || $jumlah == '' || $sal_pos == '' || $cabang == '') {
			$result = array('status' => false, 'message' => 'Form masih ada yang kosong!'); 
		} else {
			// cari kode approval
			$k = $this->Main_Model->kode_level($sal_pos, 'pengajuan');
			$approval = isset($k->kode) ? $k->kode : 1;

			$data = array(
				'id_cabang' => $cabang,
				'posisi' => $posisi,
				'tgl_pengajuan' => $this->Main_Model->convert_tgl($tgl_pengajuan),
				'jumlah' => $jumlah,
				'spesifikasi' => $spesifikasi,
				'insert_at' => date('Y-m-d H:i:s'),
				'user_insert' => $this->session->userdata('username'),
				'sal_pos' => $sal_pos
				
				);

			if($this->input->post('tgl_pemenuhan') == '' && $this->input->post('nip') == '') $data['status'] = $approval;

			if($this->input->post('tgl_pemenuhan') != '') $data['tgl_pemenuhan'] = $this->Main_Model->convert_tgl($this->input->post('tgl_pemenuhan'));

			if($this->input->post('nip') != '') $data['nip'] = $this->input->post('nip');

			$result = array('status'=>true,'message'=>'Success!');
			$this->Karyawan_Model->pengajuan_karyawan_process($data, $id);

			if($id == '') $this->Main_Model->reminder_pengajuan_karyawan($data, $approval);
			
		}
		echo json_encode($result);
	}

	function id_pengajuan_kary($id)
	{
		$this->Main_Model->guest_login();
		$data =	$this->Karyawan_Model->id_pengajuan_kary($id);
		echo json_encode($data);
	}

	function delete_pengajuan_kary()
	{
		$this->Main_Model->guest_login();
		$id 	= $this->input->post('id');
		$this->Karyawan_Model->delete_pengajuan_kary($id);
	}

	function pengajuan_all()
	{
		$this->Main_Model->all_login();
		$cabang = $this->Main_Model->session_cabang();
		$menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0','0','5') : $this->Main_Model->menu_user('0','0','80');

		$javascript = '
			<script>
				mode = "pengajuan";
				function reset() {
					$(".blank").val("");
					$(".select2").val("").trigger("change");
					$("#pemenuhan").addClass("hidden");
					$("#tgl_pengajuan").addClass("date-picker");
					'.$this->Main_Model->default_datepicker().'
					$("#cabang").attr("disabled", false);
					$("#sal_pos").attr("disabled", false);
				 	$("#posisi").attr("disabled", false);
				 	$("#tgl_pengajuan").attr("readonly", false);
				 	$("#jumlah").attr("readonly", false);
				 	$("#pengajuan").removeClass("hidden");
				 	mode = "pengajuan";
				}

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('pengajuan_karyawan/view_pengajuan_karyawan_all').'",
						data : {
						          	"tahun" : tahun,
						            "bulan" : bulan
								},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
							$("#unduh").addClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
							$("#unduh").removeClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
                    			responsive: true
                			});
						}
					});
				} load_table();

				$("#tampil").click(function(){
					load_table();
				});'

				 .$this->Main_Model->notif()

				 .$this->Main_Model->default_select2()

				 .$this->Main_Model->default_datepicker()

				 .$this->Main_Model->get_data(base_url('pengajuan_karyawan/id_pengajuan_kary'),'get_id(id)','
				 	$("#myModal").modal();
				 	$("#cabang").val(data.id_cabang).trigger("change");
				 	$("#posisi").val(data.posisi).trigger("change");
				 	$("#tgl_pengajuan").val(data.tanggal_pengajuan);
				 	$("#jumlah").val(data.jumlah);
				 	$("#sal_pos").val(data.sal_pos).trigger("change");
				 	$("#id").val(data.id);
				 	$("#nip").val("").trigger("change");
				 	$("#idcabang").val(data.id_cabang);
				 	$("#pemenuhan").removeClass("hidden");
				 	$("#cabang").attr("disabled", true);
				 	$("#sal_pos").attr("disabled", true);
				 	$("#posisi").attr("disabled", true);
				 	$("#tgl_pengajuan").attr("readonly", true);
				 	$("#jumlah").attr("readonly", true);
				 	$("#pengajuan").addClass("hidden");
				 	mode = "pemenuhan"')

				 .$this->Main_Model->default_delete_data(base_url('pengajuan_karyawan/delete_pengajuan_kary'))

				 .'

				function save() {
					(mode == "pengajuan") ? link = "pengajuan_karyawan_process" : link = "add_detail";
					$.ajax({
						url : link,
						type : "post",
						data : $("#form_pengajuan_karyawan").serialize(),
						dataType : "json",
						success : function(data) {
							if(data.status==true) {
						 		load_table();
						 		reset();
						 	} 
						 	notif(data.message);
						}
					});
				}

				function delete_detail(id) {
                    bootbox.dialog({
                        message : "Yakin ingin menghapus data?",
                        title : "Hapus Data",
                        buttons :{
                            danger : {
                                label : "Delete",
                                className : "red",
                                callback : function(){
                                    $.ajax({
                                        url : "' . base_url('pengajuan_karyawan/delete_det_pengajuan') . '/"+id,
                                        success : function(data){
                                            bootbox.alert({
                                                message: "Delete Success",
                                                size: "small"
                                            });
                                            load_table();
                                        },
                                        error : function(jqXHR, textStatus, errorThrown){
                                            bootbox.alert("Oops Something Went Wrong!");
                                        }
                                    });
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    })
                }

                $("#unduh").click(function(){
                	var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
                	window.location.href="'.base_url('pengajuan_karyawan/download_pengajuan').'/"+tahun+"/"+bulan;
                });

                function load_karyawan() {
                	var jenis = $("#jenis").val();
                	var id_pengajuan = $("#id").val();

                	$.ajax({
                		url : "'.base_url('pengajuan_karyawan/data_karyawan').'?jenis="+jenis+"&id="+id_pengajuan,
                		dataType : "json",
                		success : function(data) {
                			$("#nip option").remove();
                			var karyawan = data;
		                	var data = $.map(karyawan, function(el) { return el; })
		                	$("#nip").select2({
		                		data : data
		                	});
                		}
                	});
                }

                $(document).ready(function(){
                	$("#nip").select2();
                	load_karyawan();
                });

                $("#jenis").change(function(){
                	load_karyawan();
                });
			</script>
		';
		$header = array(
				'menu' => $menu,
				'style' => $this->Main_Model->style_datatable()
				          .$this->Main_Model->style_modal()
				          .$this->Main_Model->style_datepicker()
				          .$this->Main_Model->style_select2()
			);
		$footer = array(
				'javascript' => $javascript,
				'js' => $this->Main_Model->js_datatable()
				       .$this->Main_Model->js_modal()
				       .$this->Main_Model->js_bootbox()
				       .$this->Main_Model->js_datepicker()
				       .$this->Main_Model->js_select2()
			);
		$idp 	= $this->session->userdata('idp');
		$a 		= $this->Karyawan_Model->view_cabang($idp);
		foreach ($a as $row) {
			$opt_cabang[$row->id_cab] = $row->cabang;
		}
		$w = $this->Penggajian_Model->staff_rule($idp);
		$level = array();
		if($w) {
			foreach($w as $r) {
				$level[$r->sal_pos] = $r->sal_pos;
			}
		}

		$condition = array('status' => 1);
		$job = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'result');
		$job_opt = array();
		if (!empty($job)) {
			foreach ($job as $row) {
				$job_opt[$row->jab] = $row->jab;
			}
		}

		$data = array(
				'nip' => $this->Main_Model->all_kary_active(),
				'cabang' => $opt_cabang,
				'sal_pos' => $level,
				'job' => $job_opt,
				'periode' => $this->Main_Model->periode_opt()
 			);

		$this->load->view('template/header', $header);
        $this->load->view('karyawan/data_pengajuan_karyawan', $data);
        $this->load->view('template/footer', $footer);
	}

	function data_karyawan()
	{
		$this->Main_Model->all_login();
		$jenis = $this->input->get('jenis');
		$id = $this->input->get('id');
		$data = $this->Karyawan_Model->data_karyawan($jenis, $id);

		echo $data;
	}

	function view_pengajuan_karyawan_all()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data 		= $this->Karyawan_Model->view_pengajuan_karyawan($qd_id);
        $template 	= $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Cabang','Posisi','Pengajuan','Jml','Spesifikasi','Pemenuhan','Atas Nama','Status',
        	// 'Status HR','Progress','Status TTD',
        	'Action');
		$no = 1;
        foreach ($data as $row) {

        	$q = $this->db->query("
        		SELECT a.*,b.nama 
        		FROM d_pengajuan_kary a 
        		LEFT JOIN kary b ON a.nip = b.nip 
        		WHERE a.id = '$row->id'")->result();

        	$tgl = '';
        	$nip = '';
        	$delete = '';
        	if($q) {
        		$i = 1;
        		foreach($q as $r) {
        			$tgl .= $this->Main_Model->format_tgl($r->tgl_pemenuhan).'<br>';
        			$nip .= $r->nama.'<br>';
        			$delete .= '<li>
                            <a href="javascript:;" onclick="delete_detail(' . $r->id_det . ');">
                                <i class="icon-delete"></i> Delete '.$r->nama.'</a>
                        </li>';
        		}
        	} else {
        		$tgl = 'ON PROGRESS';
        		$nip = '';
        		$delete = '';
        	}

        	$jumlah = $this->db->query("
        		SELECT COUNT(id_det) jml 
        		FROM d_pengajuan_kary 
        		WHERE id = '$row->id'")->row();

        	($jumlah->jml < $row->jumlah) ? $link = '<li> <a href="javascript:;" onclick="get_id(' . $row->id . ');"> <i class="icon-edit"></i> Input Pemenuhan </a> </li>' : $link = '';

        $sal_pos = ($row->sal_pos != '') ? '('.$row->sal_pos.') ' : '';
		$this->table->add_row(
			$no++,
			$row->cabang,
			$sal_pos.$row->posisi,
			
			($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '',
			$row->jumlah,
			$row->spesifikasi,
			$tgl,
			$nip,
			// $row->keterangan,
			// $row->ket_hr,
			// $row->ket_progress,
			// $row->ket_ttd,
			array('style' => 'width:150px;', 'data' => '<strong>Status</strong> : '.$row->keterangan.'<br>'.
			'<strong>Status HR</strong> : '.$row->ket_hr.'<br>'.
			'<strong>Progress</strong> : '.$row->ket_progress.'<br>'.
			'<strong>TTD</strong> : '.$row->ket_ttd),
			'<div class="btn-group dropup">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        '.$link.$delete.'
                        <li>
                            <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
                                <i class="icon-delete"></i> Delete </a>
                        </li>
                    </ul>
            </div>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function delete_det_pengajuan($id='')
	{
		$this->Main_Model->all_login();
		$this->Karyawan_Model->delete_det_pengajuan($id);
	}

	function add_detail()
	{
		$this->Main_Model->all_login();
		$tgl_pemenuhan = $this->input->post('tgl_pemenuhan');
		$nip = $this->input->post('nip');
		$id = $this->input->post('id');
		$jenis = $this->input->post('jenis');

		if($tgl_pemenuhan == '' || $nip == '') {
			$result = array('status'=>false, 'message'=>'Form masih ada yang kosong!');
		} else {
			$data = array(
				'id' => $id,
				'nip' => $nip,
				'tgl_pemenuhan' => $this->Main_Model->convert_tgl($tgl_pemenuhan),
				'jenis' => $jenis,
				'user' => $this->session->userdata('username')
				);

			$this->Karyawan_Model->add_detail_pengajuan($data);
			$result = array('status'=>true, 'message'=>'Success!');
		}

		echo json_encode($result);
	}

	function download_pengajuan($tahun='', $bulan='')
	{
		$this->Main_Model->all_login();
		ini_set('display_errors', TRUE);
		ini_set('display_startup_errors', TRUE);

		define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');
			$p = periode($tahun, $bulan);
			$qd_id = isset($p->qd_id) ? $p->qd_id : '';
        	$periode = $this->Main_Model->id_periode($qd_id);
        	$per = isset($periode->periode) ? $periode->periode : '';
        	$title = 'Pengajuan Karyawan Periode '.$per;

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
			$objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
			$objPHPExcel->getProperties()->setTitle("Download Pengajuan Karyawan");
			$objPHPExcel->getProperties()->setSubject("Download Pengajuan Karyawan");
			$objPHPExcel->getProperties()->setDescription("Download Pengajuan Karyawan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Pengajuan Karyawan'); //sheet title

 			$styleArray = array(
			      	'borders' => array(
			          	'allborders' => array(
			              	'style' => PHPExcel_Style_Border::BORDER_THIN
			          )
			      )
			);

 			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:H1');
 			$objset->setCellValue('A1', $title);
            //table header
            $cols = array("A","B","C","D","E","F","G","H");
             
            $val = array("NO","CABANG","POSISI","TGL PENGAJUAN","JUMLAH","SPESIFIKASI","TGL PEMENUHAN","ATAS NAMA");
             
            for ($a=0;$a<count($val); $a++) {
                $objset->setCellValue($cols[$a].'3', $val[$a]);
                 
                //Setting lebar cell
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25); 
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
             
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    )
                );
                $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
            }
             
            $baris  = 4;
            $i=1;
            $ambildata = $this->Karyawan_Model->view_pengajuan_karyawan('', $qd_id);
            foreach ($ambildata as $frow){
                $q = $this->db->query("
                	SELECT a.*,b.nama 
                	FROM d_pengajuan_kary a 
                	LEFT JOIN kary b ON a.nip = b.nip 
                	WHERE a.id = '$frow->id'")->result();
	        	$tgl = '';
	        	$nip = '';
	        	$delete = '';
	        	if($q) {
	        		$j = 1;
	        		foreach($q as $r) {
	        			$tgl .= $this->Main_Model->format_tgl($r->tgl_pemenuhan).' ';
	        			$nip .= $r->nama.' ';
	        		}
	        	} else {
	        		$tgl = 'ON PROGRESS';
	        		$nip = '';
	        	}
	        	($frow->tgl_pengajuan) ? $tgl_pengajuan = $this->Main_Model->format_tgl($frow->tgl_pengajuan) : $tgl_pengajuan = '';
                //pemanggilan sesuaikan dengan nama kolom tabel
                $objset->setCellValue("A".$baris, $i++); 
                $objset->setCellValue("B".$baris, $frow->cabang); 
                $objset->setCellValue("C".$baris, $frow->posisi);
                $objset->setCellValue("D".$baris, $tgl_pengajuan);
                $objset->setCellValue("E".$baris, $frow->jumlah);
                $objset->setCellValue("F".$baris, $frow->spesifikasi);
                $objset->setCellValue("G".$baris, $tgl);
                $objset->setCellValue("H".$baris, $nip);
                 
                //Set number value
                $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
                $baris++;
            }
            $b=$baris-1;
			$objPHPExcel->getActiveSheet()->getStyle('A3:H'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Pengajuan Karyawan');

			// Redirect output to a client’s web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="'.$title.'.xls"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
	}

	function view_approval_pengajuan()
	{
		$this->Main_Model->guest_login();
		$data = $this->Karyawan_Model->view_approval_pengajuan();
		$template 	= $this->Main_Model->tbl_temp('tb_pengajuan');
		$is_approval = $this->Main_Model->is_approval();

		$array_header = array('No','Posisi','Cabang','Level','Tgl Pengajuan','Jml','Spesifikasi','Status');
		($is_approval == TRUE) ? $array_header[] = 'Action' : $array_header = $array_header;

		$this->table->set_heading($array_header);
		$no = 1;
		foreach($data as $row) {
			$array_data = array($no++, $row->posisi,$row->cabang, $row->sal_pos, ($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '', $row->jumlah, $row->spesifikasi, $row->keterangan);
			($is_approval == TRUE) ? $array_data[] = '<a href="javascript:;" class="btn btn-primary btn-xs" onclick="app_pengajuan(' . $row->id . ');"> <i class="fa fa-check"></i> Verifikasi </a>' : $array_data = $array_data;

			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function approval_pengajuan($id='', $val='')
	{
		$this->Main_Model->guest_login();
		$user = $this->session->userdata('username');
		$date = date('Y-m-d H:i:s');
		$k = $this->Main_Model->view_by_id('pengajuan_kary', array('id' => $id), 'row');
		$kode = isset($k->status) ? $k->status : ''; 
		$value = $this->Main_Model->rule_approval($kode, 'pengajuan', $val);
		$data = array(
			'status' => $value,
			'user_update' => $user,
			'update_at' => $date
			);
		$this->Karyawan_Model->pengajuan_karyawan_process($data, $id);
	}

	function sudah_ttd($id='', $val='')
	{
		$this->Main_Model->all_login();
		$data = array(
			'status_ttd' => $val
			);
		$this->Karyawan_Model->pengajuan_karyawan_process($data, $id);
	}

	function blm_terpenuhi()
	{
		$this->Main_Model->guest_login();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker()
		      .$this->Main_Model->js_bs_select()
		      .$this->Main_Model->js_button_excel();

		$menu = $this->Main_Model->menu_user('0','0','80');

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
				      .$this->Main_Model->style_bs_select()
			);

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page()
		);

		$this->load->view('template/header', $header);
		$this->load->view('akun/blm_terpenuhi', $data);
	}

	function view_cek_pengajuan()
	{
		$this->Main_Model->guest_login();
		$data = $this->Karyawan_Model->cek_pengajuan_karyawan();
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No','Cabang','Posisi','Pengajuan','Jml','Spesifikasi','Pemenuhan','Atas Nama','Status');
		$no = 1;
        foreach ($data as $row) {

        	$q = $this->db->query("select a.*,b.nama from d_pengajuan_kary a join kary b on a.nip = b.nip where a.id = '$row->id'")->result();
        	$tgl = '';
        	$nip = '';
        	if($q) {
        		$i = 1;
        		foreach($q as $r) {
        			$tgl .= $this->Main_Model->format_tgl($r->tgl_pemenuhan).'<br>';
        			$nip .= $r->nama.'<br>';
        		}
        	} else {
        		$tgl = 'ON PROGRESS';
        		$nip = '';
        	}

    	    // switch ($row->status) {
	        // 	case '1': $status = 'Disetujui'; break;
	        // 	case '2': $status = 'Ditolak'; break;
	        // 	default: $status = 'Proses'; break;
	        // }
        $edit = array('label' => 'Update', 'event' => "get_id('".$row->id."')");
        $delete = array('label' => 'Delete', 'event' => "delete_data('".$row->id."')");
	    $action = array($edit, $delete);

	    $ttd = array('label' => 'Sudah TTD ?', 'event' => "change('".$row->id."', '10')");
	    $download = array('label' => 'Download File', 'event' => "unduh('".$row->id."')");
	    if ($row->progress == '5') {
	    	$action[] = $ttd;
	    	$action[] = $download;
	    }
	    $sal_pos = ($row->sal_pos != '') ? '('.$row->sal_pos.') ' : '';

		$this->table->add_row(
			$no++,
			$row->cabang,
			$sal_pos.$row->posisi,
			($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '',
			$row->jumlah,
			$row->spesifikasi,
			$tgl,
			$nip,
			array('style' => 'width:150px;', 'data' => '<strong>Status</strong> : '.$row->keterangan.'<br>'.
			'<strong>Status HR</strong> : '.$row->ket_hr.'<br>'.
			'<strong>Progress</strong> : '.$row->ket_progress.'<br>'.
			'<strong>TTD</strong> : '.$row->ket_ttd)
			// $this->Main_Model->action($action)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function pengajuan_mutasi()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->Main_Model->session_cabang();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker()
		      .$this->Main_Model->js_bs_select()
		      .$this->Main_Model->js_button_excel();

		$menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','5') : $this->Main_Model->menu_user('0','0','80');

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
				      .$this->Main_Model->style_bs_select()
			);

		$nip_opt = array();
		$nip = (empty($sess_cabang)) ? $this->Main_Model->kary_active() : $this->Main_Model->kary_cabang($sess_cabang, 'q');
		if($nip) {
			foreach($nip as $row) {
				$nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
			} 
		}

		$cabang = array();

		// if(!empty($sess_cabang)) {
		// 	foreach($sess_cabang as $r) {
		// 		$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
		// 		$cabang[$d->id_cab] = $d->cabang;
		// 	}
		// } else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang[$row->id_cab] = $row->cabang;
			}
		// }

		$w = $this->Main_Model->session_pengajuan();
		$level = array();
		if($w) {
			foreach($w as $r) {
				$level[$r] = $r;
			}
		} else {
			$sal = $this->Main_Model->view_by_id('sal_staff_class', array('status' => 1), 'result');
			foreach ($sal as $row) {
				$level[$row->sal_pos] = $row->sal_pos;
			}
		}

		$condition = array('status' => 1);
		$job = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'result');
		$job_opt = array();
		if (!empty($job)) {
			foreach ($job as $row) {
				$job_opt[$row->jab] = $row->jab;
			}
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'nip' => $nip_opt,
			'cabang' => $cabang,
			'periode' => $this->Main_Model->periode_opt(),
			'sal_pos' => $level,
			'cabang' => $cabang,
			'job' => $job_opt
		);

		$this->load->view('template/header', $header);
		$this->load->view('akun/pengajuan_mutasi', $data);
	}

	function pengajuan_promosi()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->Main_Model->session_cabang();
		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker()
		      .$this->Main_Model->js_bs_select()
		      .$this->Main_Model->js_button_excel();

		$menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','5') : $this->Main_Model->menu_user('0','0','80');

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
				      .$this->Main_Model->style_bs_select()
			);

		$nip_opt = array();
		$nip = (empty($sess_cabang)) ? $this->Main_Model->kary_active() : $this->Main_Model->kary_cabang($sess_cabang, 'q');
		if($nip) {
			foreach($nip as $row) {
				$nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
			} 
		}

		$cabang = array();

		// if(!empty($sess_cabang)) {
		// 	foreach($sess_cabang as $r) {
		// 		$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
		// 		$cabang[$d->id_cab] = $d->cabang;
		// 	}
		// } else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang[$row->id_cab] = $row->cabang;
			}
		// }

		$w = $this->Main_Model->session_pengajuan();
		$level = array();
		if($w) {
			foreach($w as $r) {
				$level[$r] = $r;
			}
		} else {
			$sal = $this->Main_Model->view_by_id('sal_staff_class', array('status' => 1), 'result');
			foreach ($sal as $row) {
				$level[$row->sal_pos] = $row->sal_pos;
			}
		}

		$condition = array('status' => 1);
		$job = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'result');
		$job_opt = array();
		if (!empty($job)) {
			foreach ($job as $row) {
				$job_opt[$row->jab] = $row->jab;
			}
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'nip' => $nip_opt,
			'cabang' => $cabang,
			'periode' => $this->Main_Model->periode_opt(),
			'sal_pos' => $level,
			'cabang' => $cabang,
			'job' => $job_opt
		);

		$this->load->view('template/header', $header);
		$this->load->view('akun/pengajuan_promosi', $data);
	}

	function view_pengajuan($tipe='')
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Karyawan_Model->view_pengajuan($tipe, $periode);
		$template 	= $this->Main_Model->tbl_temp();

		$array_header = array(
							'No',
							'Nama',
							'Posisi',
							'Cabang',
							'Level',
							'Tgl Pengajuan',
							'Tgl Pemenuhan',
							'Keterangan',
							'Action');

		$this->table->set_heading($array_header);
		$no = 1;
		foreach($data as $row) {
			$edit = array(
					'label' => 'Update', 
					'event' => "get_id('".$row->id."')");
	        $delete = array(
	        		'label' => 'Delete', 
	        		'event' => "delete_data('".$row->id."')");
		    $action = array($edit);

		    $acctype = $this->session->userdata('acctype');
		    if ($acctype == 'Administrator') $action[] = $delete;

			$array_data = array(
					$no++, 
					$row->nama, 
					$row->posisi, 
					$row->cabang, 
					$row->sal_pos, 
					($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '',
					($row->tgl_pemenuhan) ? $this->Main_Model->format_tgl($row->tgl_pemenuhan) : 'ON PROGRESS', 
					$row->keterangan, 
					$this->Main_Model->action($action));
			
			$this->table->add_row($array_data);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function add_pengajuan($tipe='')
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$posisi = $this->input->post('posisi');
		$tgl_pengajuan = $this->input->post('tgl_pengajuan');
		// $jumlah = $this->input->post('jumlah');
		$cabang = $this->input->post('cabang');
		$keterangan = $this->input->post('keterangan');
		$sal_pos = $this->input->post('sal_pos');
		$nip = $this->input->post('nip');

		if ($tipe == 'mutasi') {
			if($posisi == '' || $tgl_pengajuan == '' || $sal_pos == '' || $cabang == '') {
				$result = array('status' => false, 'message' => 'Form masih ada yang kosong!'); 
			} else {
				// cari kode approval
				// $k = $this->Main_Model->kode_level($sal_pos, 'pengajuan');
				// $approval = isset($k->kode) ? $k->kode : 1;

				$data = array(
					'id_cabang' => $cabang,
					'posisi' => $posisi,
					'tgl_pengajuan' => $this->Main_Model->convert_tgl($tgl_pengajuan),
					// 'jumlah' => $jumlah,
					'keterangan' => $keterangan,
					'tag' => $tipe,
					'insert_at' => date('Y-m-d H:i:s'),
					'user_insert' => $this->session->userdata('username'),
					'sal_pos' => $sal_pos,
					'nip' => $nip
					);

				// if($this->input->post('tgl_pemenuhan') == '' && $this->input->post('nip') == '') $data['status'] = $approval;

				// if($this->input->post('tgl_pemenuhan') != '') $data['tgl_pemenuhan'] = $this->Main_Model->convert_tgl($this->input->post('tgl_pemenuhan'));

				// if($this->input->post('nip') != '') $data['nip'] = $this->input->post('nip');
				$condition = array();
				if ($id != '') $condition['id'] = $id;
				$this->Main_Model->process_data('tb_pengajuan', $data, $condition);

				// if($id == '') $this->Main_Model->reminder_pengajuan_karyawan($data);
				$result = array('status' => true, 'message' => 'Success!');
			}
		} else {
			if($posisi == '' || $tgl_pengajuan == '' || $sal_pos == '' ) {
				$result = array('status' => false, 'message' => 'Form masih ada yang kosong!'); 
			} else {
				// cari kode approval
				// $k = $this->Main_Model->kode_level($sal_pos, 'pengajuan');
				// $approval = isset($k->kode) ? $k->kode : 1;
				$c = $this->Main_Model->posisi($nip);

				$data = array(
					'id_cabang' => isset($c->id_cabang) ? $c->id_cabang : '',
					'posisi' => $posisi,
					'tgl_pengajuan' => $this->Main_Model->convert_tgl($tgl_pengajuan),
					// 'jumlah' => $jumlah,
					'keterangan' => $keterangan,
					'tag' => $tipe,
					'insert_at' => date('Y-m-d H:i:s'),
					'user_insert' => $this->session->userdata('username'),
					'sal_pos' => $sal_pos,
					'nip' => $nip
					);

				// if($this->input->post('tgl_pemenuhan') == '' && $this->input->post('nip') == '') $data['status'] = $approval;

				// if($this->input->post('tgl_pemenuhan') != '') $data['tgl_pemenuhan'] = $this->Main_Model->convert_tgl($this->input->post('tgl_pemenuhan'));

				// if($this->input->post('nip') != '') $data['nip'] = $this->input->post('nip');
				$condition = array();
				if ($id != '') $condition['id'] = $id;
				$this->Main_Model->process_data('tb_pengajuan', $data, $condition);

				// if($id == '') $this->Main_Model->reminder_pengajuan_karyawan($data);
				$result = array('status' => true, 'message' => 'Success!');
			}
		}
		
		echo json_encode($result);
	}

	function id_pengajuan($id='')
	{
		$this->Main_Model->all_login();
		$data =	$this->Karyawan_Model->id_pengajuan($id);
		echo json_encode($data);
	}

	function delete_pengajuan()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$this->Main_Model->delete_data('tb_pengajuan', array('id' => $id));

		echo json_encode(array('status' => true, 'message' => 'Success!'));
	}

	function view_pengajuan_karyawan_dashboard()
	{
		$this->Main_Model->all_login();
		// $tahun = $this->input->get('tahun');
		// $bulan = $this->input->get('bulan');
		// $p = periode($tahun, $bulan);
		$p = $this->Main_Model->per_skr();
		$tgl_awal = isset($p->tgl_awal) ? $p->tgl_awal : '';
		$tgl_akhir = isset($p->tgl_akhir) ? $p->tgl_akhir : '';
        $data = $this->Karyawan_Model->view_karyawan_finalisasi($tgl_awal, $tgl_akhir);
        $template = $this->Main_Model->tbl_temp('tb_finalisasi_karyawan');

        $this->table->set_heading('No','Cabang','Posisi','Pengajuan','Jml','Spesifikasi','Pemenuhan','Atas Nama','Status', 'Action');
		$no =1;
        foreach ($data as $row) {
        	$q = $this->db->query("
        		SELECT a.*,b.nama 
        		FROM d_pengajuan_kary a 
        		JOIN kary b ON a.nip = b.nip 
        		WHERE a.id = '$row->id'")->result();

        	$tgl = '';
        	$nip = '';
        	if($q) {
        		$i = 1;
        		foreach($q as $r) {
        			$tgl .= $this->Main_Model->format_tgl($r->tgl_pemenuhan).'<br>';
        			$nip .= $r->nama.'<br>';
        		}
        	} else {
        		$tgl = 'ON PROGRESS';
        		$nip = '';
        	}

        $edit = array('label' => 'Update', 'event' => "get_id('".$row->id."')");
        $delete = array('label' => 'Delete', 'event' => "delete_data('".$row->id."')");
	    $action = array();

	    $acctype = $this->session->userdata('acctype');
	    if ($acctype == 'Administrator') $action[] = $delete;

	    $ttd = array('label' => 'Sudah TTD ?', 'event' => "change('".$row->id."', '10')");
	    $download = array('label' => 'Download File', 'event' => "unduh_file('".$row->id."')");
	    if ($row->progress == '5') {
	    	$action[] = $ttd;
	    	$action[] = $download;
	    }
	    $sal_pos = ($row->sal_pos != '') ? '('.$row->sal_pos.') ' : '';

		$this->table->add_row(
			$no++,
			$row->cabang,
			$sal_pos.$row->posisi,
			($row->tgl_pengajuan) ? $this->Main_Model->format_tgl($row->tgl_pengajuan) : '',
			$row->jumlah,
			$row->spesifikasi,
			$tgl,
			$nip,
			array('style' => 'width:150px;', 'data' => '<strong>Status</strong> : '.$row->keterangan.'<br>'.
			'<strong>Status HR</strong> : '.$row->ket_hr.'<br>'.
			'<strong>Progress</strong> : '.$row->ket_progress.'<br>'.
			'<strong>TTD</strong> : '.$row->ket_ttd),
			$this->Main_Model->action($action)
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}
}

/* End of file pengajuan_karyawan.php */
/* Location: ./application/controllers/pengajuan_karyawan.php */ ?>