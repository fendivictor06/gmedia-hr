<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Potongan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Potongan_Model', '', true);
    }

    function header()
    {
        $menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
        return $menu;
    }

    function footer()
    {
        $footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'"></script>';

        return $footer;
    }

    function piutang()
    {
        $this->Main_Model->get_login();
        $url = base_url('potongan/view_piutang');
        $url_del = base_url('potongan/delete_piutang');
        $url_save = base_url('potongan/process_piutang');
        $javascript = '
			<script type="text/javascript">
				function reset() {
					$(".select2").val("").trigger("change");
					$(".kosong").val("");
				}
				'.$this->Main_Model->default_loadtable($url).
              $this->Main_Model->default_datepicker().'

    			function save() {
					$.ajax({
						url : "'.$url_save.'",
						data : $("#fpiutang").serialize(),
						type : "POST",
						dataType: "JSON",
						success : function(data){
							if(data.status=="true") {
								'.$this->Main_Model->notif200().'
								load_table();
								reset();
							} else {
								'.$this->Main_Model->notif400().'
							}
						},	
						error 	: function(jqXHR,textStatus,errorThrown){
							'.$this->Main_Model->notif500().'
						}
					});
    			}

    			function get_id(id) {
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('potongan/piutang_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType : "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
							$("#ket").val(data.ket);
							$("#tot_pot").val(data.tot_pot);
							$("#nip").val(data.nip).trigger("change");
							$("#tgl_hutang").val(data.tanggal);
							$("#jml_cicilan").val(data.cicilan);
							$("#id").val(data.id_pot);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			'.$this->Main_Model->notif500().'
			    		}
			    	});
			    }

			    function cicil(id) {
			    	$("#cicil").modal();
			    	$("#id_pot").val(id);
			    	$(".kosong").val("");
			    }

			    function save_cicil() {
		    		$.ajax({
						url : "'.base_url('potongan/add_cicil').'",
						data : $("#fcicilan").serialize(),
						type : "POST",
						dataType : "JSON",
						success : function(data){
							if(data.status=="true") {
								'.$this->Main_Model->notif200().'
								load_table();
								reset();
							} else {
								'.$this->Main_Model->notif400().'
							}
						},	
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal menyimpan data!");
						}
					});
			    }
				'.$this->Main_Model->default_select2()
             .$this->Main_Model->default_delete_data($url_del).'
			</script>';

        $footer = array(
        'javascript' => $javascript,
        'js' => $this->footer()
        );

        $data   = array(
            'nip' => $this->Main_Model->all_kary_active(),
            'periode' => $this->Main_Model->periode_opt(),
            'option' => array(
                '1' => 'Ya',
                '0' => 'Tidak'
        )
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '2')
        );

        $this->load->view('template/header', $header);
        $this->load->view('potongan/piutang', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_piutang()
    {
        $this->Main_Model->get_login();
        $data=$this->Potongan_Model->view_piutang();
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'NIP', 'Nama', 'Total Piutang', 'Keterangan', 'Sisa Piutang', 'Cicilan', 'Action');
        $no = 1;
        foreach ($data as $row) {
            // $angsuran=$this->db->query("select sum(ags) as angsuran from pot_det where id_pot='$row->id_pot'")->row();
            // $sisa = $row->tot_pot-$angsuran->angsuran;
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $this->Main_Model->uang($row->tot_pot),
                $row->ket,
                $this->Main_Model->uang($row->sisa),
                $row->jml_cicilan.'/'.$row->cicilan,
                '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                	<li>
	                		<a href="javascript:;" onclick="cicil(' . $row->id_pot . ');"> Cicil </a>
	                	</li>
	                	<li>
	                		<a href="'.base_url('potongan/lihat_cicilan').'/'.$row->id_pot.'/'.$row->nip.'"> Lihat Cicilan </a>
	                	</li>
	                    <li>
	                        <a href="javascript:;" onclick="get_id(' . $row->id_pot . ');"> Update </a>
	                    </li>
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->id_pot . ');"> Delete </a>
	                    </li>
	                </ul>
	        </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function piutang_id()
    {
        $this->Main_Model->get_login();
        $id = $this->input->get('id');
        $data = $this->Potongan_Model->piutang_id($id);
        echo json_encode($data);
    }

    function process_piutang()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $nip = $this->input->post('nip');
        $tot_pot = $this->input->post('tot_pot');
        $ket = $this->input->post('ket');
        $tgl_hutang = $this->input->post('tgl_hutang');
        $jml_cicilan = $this->input->post('jml_cicilan');
        $username = $this->session->userdata('username');

        $user = ($id != '') ? 'user_update' : 'user_insert';
        $time = ($id != '') ? 'update_at' : 'insert_at';

        if ($nip == "" || $tot_pot == "") {
            $result = array(
            'status' => 'false'
            );
        } else {
            $data = array(
            'nip' => $nip,
            'tot_pot' => $tot_pot,
            'ket' => $ket,
            'tgl' => $this->Main_Model->convert_tgl($tgl_hutang),
            'cicilan' => $jml_cicilan,
            $user => $username,
            $time => now()
            );

            $this->Potongan_Model->process_piutang($data, $id);
            $result = array(
            'status' => 'true'
            );
        }

        echo json_encode($result);
    }

    function delete_piutang()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Potongan_Model->delete_piutang($id);
    }

    function add_cicil()
    {
        $this->Main_Model->get_login();
        $ags = $this->input->post('ags');
        $tgl = $this->input->post('tgl');
        $inc = $this->input->post('inc');
        $keterangan = $this->input->post('keterangan');
        $id_pot = $this->input->post('id_pot');
        $username = $this->session->userdata('username');

        if ($ags == "" || $tgl=="" || $inc=="" || $id_pot=="") {
            $message = '';
            if ($ags == '') {
                $message .= 'Masukkan Jumlah Angsuran <br>';
            }
            if ($tgl == '') {
                $message .= 'Masukkan Tanggal cicilan <br>';
            }
            if ($inc == '') {
                $message .= 'Cicilan include payroll ? <br>';
            }
            
            $result = array(
            'status' => 'false'
            );
        } else {
            $data = array(
            "id_pot" => $id_pot,
            "ags" => $ags,
            "tgl" => $this->Main_Model->convert_tgl($tgl),
            "inc_payroll" => $inc,
            "ket" => $keterangan,
            'insert_at' => now(),
            'user_insert' => $username
            );
            $this->Potongan_Model->add_cicil($data);
            $result = array('status'=>'true');
        }

        echo json_encode($result);
    }

    function jabatan()
    {
        $this->Main_Model->get_login();
        $url        = base_url('potongan/view_jabatan');
        $javascript = '
			<script type="text/javascript">
				'.$this->Main_Model->default_loadtable($url).'
			</script>';
        $footer =array(
        'javascript' => $javascript,
        'js' => $this->footer()
            );
        $this->load->view('template/header', $this->header());
        $this->load->view('potongan/jabatan');
        $this->load->view('template/footer', $footer);
    }

    function view_jabatan()
    {
        $this->Main_Model->get_login();
        $data = $this->Potongan_Model->view_jabatan();
        $template = array(
            'table_open'            => '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">',

            'thead_open'            => '<thead>',
            'thead_close'           => '</thead>',

            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th>',
            'heading_cell_end'      => '</th>',

            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',

            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td>',
            'cell_end'              => '</td>',

            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td>',
            'cell_alt_end'          => '</td>',

            'table_close'           => '</table>'
        );

        $this->table->set_heading('No', 'NIP', 'Nama', 'Status Jabatan', 'Jumlah Potongan');
        $no=1;
        foreach ($data as $row) {
            $sk     = $this->db->query("select * from sk where nip='$row->nip' and aktif='1'")->row();
            $q      = $this->Karyawan_Model->hitung_thp($sk->id_sk);
            $thp    = isset($q->thp)?$q->thp:'';
            $tj     = isset($q->tpj)?$q->tpj:'';
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->stat_jab,
                $this->Main_Model->uang($tj*20/100)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function lainnya()
    {
        $this->Main_Model->all_login();
        $url_del = base_url('potongan/delete_lainnya');
        $url_save = base_url('potongan/process_lainnya');
        $url_get = base_url('potongan/lainnya_id');
        $javascript = '
			<script type="text/javascript">
				var save_method;
				'.$this->Main_Model->default_select2().'

    			function load_table(){
					var periode = {"periode":$("#periode").val()}
					$.ajax({
						url : "'.base_url('potongan/view_lainnya').'",
						type : "GET",
						data : periode,
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			        			responsive: true
			    			});
						},
						error : function(jqXHR, textStatus, errorThrown){
							'.$this->Main_Model->notif500().'
						} 
					})
				} 
				load_table();

				function reset()
				{
					save_method="save";
					$("#myModal").modal();
					$(".select2").val("").trigger("change");
					$(".kosong").val("");
				}

				function get_id(id)
				{
					save_method = "update";
			    	$.ajax({
			    		url 	: "' . $url_get . '/"+id,
			    		type 	: "GET",
			    		dataType: "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
							$("#keterangan").val(data.keterangan);
							$("#jumlah").val(data.jumlah);
							$("#qd_id").val(data.id_per).trigger("change");
							$("#nip").val(data.nip).trigger("change");
							$("#id").val(data.id);
			    		},
			    		error 	: function(jqXHR, textStatus, errorThrown){
			    			'.$this->Main_Model->notif500().'
			    		}
			    	});
			    }

				function save()
				{
					$.ajax({
						url 	: "'.$url_save.'",
						data 	: $("#flainnya").serialize(),
						type 	: "POST",
						dataType: "JSON",
						success : function(data){
							if(data.status=="true")
							{
								'.$this->Main_Model->notif200().'
								load_table();
								reset();
							}
							else
							{
								'.$this->Main_Model->notif400().'
							}
							
						},	
						error 	: function(jqXHR,textStatus,errorThrown){
							'.$this->Main_Model->notif500().'
						}
					});
    			}

    			'.$this->Main_Model->default_delete_data($url_del).'
			</script>';
        $footer =array(
        'javascript' => $javascript,
        'js' => $this->footer()
            );
        $data = array(
            'periode' => $this->Main_Model->periode_opt(),
            'nip' => $this->Main_Model->all_kary_active()
            );
        $this->load->view('template/header', $this->header());
        $this->load->view('potongan/lainnya', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_lainnya()
    {
        $this->Main_Model->get_login();
        $periode = $this->input->get('periode');
        $data = $this->Potongan_Model->view_lainnya($periode);
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'NIP', 'Nama', 'Periode', 'Jumlah Potongan', 'Keterangan', 'Action');
        $no=1;
        foreach ($data as $row) {
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->periode,
                $row->jumlah,
                $row->keterangan,
                '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" onclick="get_id(' . $row->id . ');">
	                            <i class="icon-edit"></i> Update </a>
	                    </li>
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
	                            <i class="icon-delete"></i> Delete </a>
	                    </li>
	                </ul>
	        </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function lainnya_id($id)
    {
        $this->Main_Model->get_login();
        $data   = $this->Potongan_Model->lainnya_id($id);
        echo json_encode($data);
    }

    function process_lainnya()
    {
        $this->Main_Model->get_login();
        $id             = $this->input->post('id');
        $nip            = $this->input->post('nip');
        $id_per         = $this->input->post('qd_id');
        $jumlah         = $this->input->post('jumlah');
        $keterangan     = $this->input->post('keterangan');

        if ($nip=="" || $id_per=="" || $jumlah=="") {
            $result     = array('status'=>'false');
        } else {
            $data       = array(
            'nip'       => $nip,
            'id_per'    => $id_per,
            'jumlah'    => $jumlah,
            'keterangan'=> $keterangan
            );
            $this->Potongan_Model->process_lainnya($data, $id);
            $result     = array('status'=>'true');
        }

        echo json_encode($result);
    }

    function delete_lainnya()
    {
        $this->Main_Model->get_login();
        $id=$this->input->post('id');
        $this->Potongan_Model->delete_lainnya($id);
    }

    function lihat_cicilan($id = '', $nip = '')
    {
        $this->Main_Model->get_login();
        $url = base_url('potongan/view_cicilan')."/".$id;
        $url_del = base_url('potongan/delete_cicilan');
        $nip = isset($nip) ? $nip : '';
        $javascript = '
			<script>
				'.$this->Main_Model->default_loadtable($url)
             .$this->Main_Model->default_delete_data($url_del).'
			</script>
		';
        $footer = array(
        'javascript' => $javascript,
        'js' => $this->footer()
            );
        $data   = array(
            'kary' => $this->Main_Model->kary_nip($nip),
            'pot' => $this->Potongan_Model->piutang_id($id)
            );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '2')
            );

        $this->load->view('template/header', $header);
        $this->load->view('potongan/lihat_cicilan', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_cicilan($id = '')
    {
        $this->Main_Model->get_login();
        $data = $this->Potongan_Model->view_cicilan($id);

        $template = $this->Main_Model->tbl_temp();
        $this->table->set_heading('No', 'Tanggal Bayar', 'Angsuran', 'Payroll', 'Keterangan', 'Action');
        $no=1;
        foreach ($data as $row) {
            if ($row->inc_payroll=='1') {
                $inc_payroll = 'Ya';
            } else {
                $inc_payroll = 'Tidak';
            }
            $this->table->add_row(
                $no++,
                $this->Main_Model->format_tgl($row->tgl),
                $this->Main_Model->uang($row->ags),
                $inc_payroll,
                $row->ket,
                '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->id_pot_det . ');"> Delete </a>
	                    </li>
	                </ul>
	        </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function delete_cicilan()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->post('id');
        $this->Potongan_Model->delete_cicilan($id);
    }

    # add modul potongan asuransi
    # 2018 10 12
    function asuransi()
    {
        $this->Main_Model->get_login();
        $url = base_url('potongan/view_asuransi');
        $url_del = base_url('potongan/delete_asuransi');
        $url_save = base_url('potongan/process_asuransi');
        $javascript = '
			<script type="text/javascript">
				function reset() {
					$(".select2").val("").trigger("change");
					$(".kosong").val("");
				}

				'.$this->Main_Model->default_loadtable($url).
                $this->Main_Model->default_datepicker().'

    			function save() {
					$.ajax({
						url : "'.$url_save.'",
						data : $("#fpiutang").serialize(),
						type : "POST",
						dataType: "JSON",
						success : function(data){
							if (data.status == true) {
								toastr.success(data.message);
								load_table();
								reset();
								$("#myModal").modal("toggle");
							} else {
								toastr.warning(data.message);
							}
						},	
						error : function(jqXHR,textStatus,errorThrown){
							toastr.warning("Terjadi kesalahan saat menyimpan data");
						}
					});
    			}

    			function get_id(id) {
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('potongan/asuransi_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType : "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
							$("#id").val(data.id);
							$("#nip").val(data.nip).trigger("change");
							$("#bulan_awal").val(data.awal_bulan).trigger("change");
							$("#tahun_awal").val(data.awal_tahun).trigger("change");
							$("#bulan_akhir").val(data.akhir_bulan).trigger("change");
							$("#tahun_akhir").val(data.akhir_tahun).trigger("change");
							$("#nominal").val(data.nominal);
							$("#ket").val(data.keterangan);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			toastr.warning("Terjadi kesalahan saat memuat data");
			    		}
			    	});
			    }

			   
				'.$this->Main_Model->default_select2()
             .$this->Main_Model->default_delete_data($url_del).'
			</script>';

        $footer = array(
        'javascript' => $javascript,
        'js' => $this->footer()
        );

        $tahun = [];
        for ($i = '2018'; $i <= '2030'; $i++) {
            $tahun[$i] = $i;
        }

        $data   = array(
        'nip' => $this->Main_Model->all_kary_active(),
        'periode' => $this->Main_Model->periode_opt(),
        'bulan' => $this->Main_Model->arr_bulan_periode(),
        'tahun' => $tahun
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '2')
        );

        $this->load->view('template/header', $header);
        $this->load->view('potongan/asuransi', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_asuransi()
    {
        $this->Main_Model->get_login();
        $data = $this->Potongan_Model->view_asuransi();
        $template = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'NIP', 'Nama', 'Mulai', 'Selesai', 'Nominal', 'Keterangan', 'Action');
        $no = 1;
        foreach ($data as $row) {
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                convert_bulan($row->awal_bulan).' '.$row->awal_tahun,
                convert_bulan($row->akhir_bulan).' '.$row->akhir_tahun,
                uang($row->nominal),
                $row->keterangan,
                '<div class="btn-group">
	            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
	                <i class="fa fa-angle-down"></i>
	            </button>
	                <ul class="dropdown-menu" role="menu">
	                    <li>
	                        <a href="javascript:;" onclick="get_id(' . $row->id . ');"> Update </a>
	                    </li>
	                    <li>
	                        <a href="javascript:;" onclick="delete_data(' . $row->id . ');"> Delete </a>
	                    </li>
	                </ul>
	        </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function process_asuransi()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $nip = $this->input->post('nip');
        $bulan_awal = $this->input->post('bulan_awal');
        $tahun_awal = $this->input->post('tahun_awal');
        $bulan_akhir = $this->input->post('bulan_akhir');
        $tahun_akhir = $this->input->post('tahun_akhir');
        $nominal = $this->input->post('nominal');
        $ket = $this->input->post('ket');

        if ($nip == '' || $bulan_awal == '' || $bulan_akhir == '' || $tahun_awal == '' || $tahun_akhir == '' || $nominal == '') {
            $status = false;
            $message = '';

            if ($nip == '') {
                $message .= 'Pilih Karyawan <br>';
            }
            if ($bulan_awal == '' || $tahun_awal == '') {
                $message .= 'Masukkan Waktu Mulai <br>';
            }
            if ($bulan_akhir == '' || $tahun_akhir == '') {
                $message .= 'Masukkan Waktu Selesai <br>';
            }
            if ($nominal == '') {
                $message .= 'Masukkan Nominal <br>';
            }
        } else {
            $time = ($id != '') ? 'update_at' : 'insert_at';
            $user = ($id != '') ?  'user_update' : 'user_insert';

            $condition = ($id != '') ? ['id' => $id] : [];

            $data = array(
            'nip' => $nip,
            'awal_bulan' => $bulan_awal,
            'awal_tahun' => $tahun_awal,
            'akhir_bulan' => $bulan_akhir,
            'akhir_tahun' => $tahun_akhir,
            'nominal' => $nominal,
            'keterangan' => $ket,
            $time => date('Y-m-d H:i:s'),
            $user => username()
            );

            $simpan = $this->Main_Model->process_data('tb_asuransi', $data, $condition);
            $status = true;
            $message = 'Data berhasil disimpan';
        }

        $result = array(
        'status' => $status,
        'message' => $message
        );

        echo json_encode($result);
    }

    function asuransi_id()
    {
        $this->Main_Model->get_login();
        $id = $this->input->get('id');
        $data = $this->Main_Model->view_by_id('tb_asuransi', ['id' => $id], 'row');

        echo json_encode($data);
    }

    function delete_asuransi()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        
        $this->Main_Model->delete_data('tb_asuransi', ['id' => $id]);
    }
}
