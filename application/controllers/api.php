<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Api extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Cuti_Model', '', true);
        $this->load->model('Api_Model', '', true);
        $this->load->model('Rekap_Absensi_Model', '', true);
    }

    function webservice($port, $url, $parameter)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => $port,
            CURLOPT_URL => "http://".$url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $parameter,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded"
                ),
            CURLOPT_FRESH_CONNECT => true
            ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            $response = ("Error #:" . $err);
        } else {
            $response;
        }
        
        return $response;
    }

    function scanlog_all($port = '8800', $url = '10.90.50.14', $sn = '13061016030082')
    {
        $parameter = "sn=".$sn;
        $url = $url."/scanlog/all";
        $result = $this->webservice($port, $url, $parameter);

        $content = json_decode($result);
        if ($content->Result == true) {
            foreach ($content->Data as $row) {
                $check_exists = $this->Api_Model->check_scan($row->ScanDate, $row->PIN);
                if (empty($check_exists)) {
                    $data = array(
                        'scan_date' => $row->ScanDate,
                        'pin' => $row->PIN,
                        'verifymode' => $row->VerifyMode,
                        'iomode' => $row->IOMode,
                        'workcode' => $row->WorkCode,
                        'sn' => $sn
                        );
                    $this->Api_Model->add_scan($data);
                }
            }
            $status = array('status' => 200, 'message' => 'Success!');
        } else {
            $status = array('status' => 400, 'message' => 'Failed!');
        }

        echo json_encode($status);
    }

    function scanlog_new($port = '8800', $url = '10.90.50.14', $sn = '13061016030082')
    {
        $parameter = "sn=".$sn;
        $url = $url."/scanlog/new";
        $result = $this->webservice($port, $url, $parameter);

        $content = json_decode($result);
        if ($content->Result == true) {
            foreach ($content->Data as $row) {
                $check_exists = $this->Api_Model->check_scan($row->ScanDate, $row->PIN);
                if (empty($check_exists)) {
                    $data = array(
                        'scan_date' => $row->ScanDate,
                        'pin' => $row->PIN,
                        'verifymode' => $row->VerifyMode,
                        'iomode' => $row->IOMode,
                        'workcode' => $row->WorkCode,
                        'sn' => $sn
                        );
                    $this->Api_Model->add_scan($data);
                }
            }
            $status = array('status' => 200, 'message' => 'Success!');
        } else {
            $status = array('status' => 400, 'message' => 'Failed!');
        }

        echo json_encode($status);
    }

    function scanlog_del($port = '8800', $url = '10.90.50.14', $sn = '13061016030082')
    {
        $parameter = "sn=".$sn;
        $url = $url."/scanlog/del";
        $result = $this->webservice($port, $url, $parameter);

        $content = json_decode($result);
        ($content->Result == true) ? $status = array('status' => 200, 'message' => 'Success!') : $status = array('status' => 400, 'message' => 'Failed!');

        echo json_encode($status);
    }

    function user_all($port = '8800', $url = '10.90.50.14', $sn = '13061016030082')
    {
        $parameter = "sn=".$sn;
        $url = $url."/user/all";
        $result = $this->webservice($port, $url, $parameter);

        $content = json_decode($result);
        if ($content->Result == true) {
            foreach ($content->Data as $row) {
                $check_exists = $this->Api_Model->check_user($row->PIN);
                if (empty($check_exists)) {
                    $data = array(
                        'pin' => $row->PIN,
                        'nama' => $row->Name,
                        'pwd' => $row->Password,
                        'rfid' => $row->RFID,
                        'privilege' => $row->Privilege
                        // 'finger_idx' => $row->Template->idx,
                        // 'alg_ver' => $row->Template->alg_ver,
                        // 'template' => $row->Template->template
                        );
                    $this->Api_Model->add_user($data);
                }
            }
            $status = array('status' => 200, 'message' => 'Success!');
        } else {
            $status = array('status' => 400, 'message' => 'Failed!');
        }

        echo json_encode($status);
    }

    function dev_info($port = '8800', $url = '10.90.50.14', $sn = '13061016030082')
    {
        $parameter = "sn=".$sn;
        $url = $url."/dev/info";
        $result = $this->webservice($port, $url, $parameter);

        print_r($result);
    }

    function proses_ts()
    {
        $today = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));

        $karyawan = $this->db->query("
						SELECT a.`nip`, c.`jam_masuk`, c.`jam_pulang`
						FROM tb_jadwal_color a
						INNER JOIN tr_shift b ON a.`nip` = b.`nip`
						INNER JOIN ms_shift c ON c.`id_shift` = b.`id_shift`
						WHERE b.`tgl` = '$yesterday'
						AND c.`jam_pulang` < c.`jam_masuk`")->result();

        if (! empty($karyawan)) {
            foreach ($karyawan as $row) {
                eksekusi_absen($yesterday, $yesterday, $row->nip);
            }
        }
    }

    function absensiproses20181207($date_start = '', $date_end = '', $nip = '')
    {
        ini_set('memory_limit', '-1');
        ini_set('MAX_EXECUTION_TIME', '-1');
        set_time_limit(0);
        $sekarang = date('Y-m-d');

        if ($date_start == '' || $date_end == '') {
            $date_start = $sekarang;
            $date_end = $sekarang;
        }

        // $ip = file_get_contents('http://api.ipify.org');
        // $log = array(
        //  'ip' => $ip,
        //  'keterangan' => 'tgl '.$date_start.' s/d '.$date_end
        // );

        // $this->Main_Model->process_data('log_proses_absensi', $log);

        if ($nip == '') {
            $w_nip = '';
        } else {
            $w_nip = " AND a.nip = '$nip' ";
        }

        $karyawan = $this->db->query("
            SELECT a.pin, a.nip 
            FROM kary a
            WHERE (isnull(a.tgl_resign) OR (a.tgl_resign >= '$date_start' AND a.tgl_resign <= '$date_end'))
            AND a.tgl_masuk <= '$date_start' 
            $w_nip ")->result();

        $tanggal = array();
        while (strtotime($date_start) <= strtotime($date_end)) {
                $tanggal[] = $date_start;
                $date_start = date("Y-m-d", strtotime("+1 day", strtotime($date_start)));
        }
        foreach ($karyawan as $kary) {
            for ($i = 0; $i < count($tanggal); $i++) {
                $nextDay = $tanggal[$i];

                //is head
                $ishead = $this->db->where('penyetuju', $kary->nip)
                            ->get('ms_rule_approval')
                            ->row();

                //cari scanlog
                $tbScanlog = $this->Api_Model->view_scanlog($nextDay, $kary->nip);

                //cari shift
                $shift = $this->Api_Model->cari_shift($nextDay, $kary->nip);
                $default_shift = $this->Api_Model->default_shift();

                if (!empty($shift)) {
                    $masuk = strtotime($shift->jam_masuk);
                    $jam_masuk = $shift->jam_masuk;
                    $pulang = strtotime($shift->jam_pulang);
                    $nama_shift = $shift->nama;
                    $keterangan_shift = $shift->keterangan;
                    $jam_pulang = $shift->jam_pulang;
                } else {
                    $masuk = strtotime($default_shift->jam_masuk);
                    $jam_masuk  = $default_shift->jam_masuk;
                    $nama_shift = $default_shift->nama;
                    $keterangan_shift = $default_shift->keterangan;
                    $pulang = strtotime($default_shift->jam_pulang);
                    $jam_pulang = $default_shift->jam_pulang;
                }

                $flag = 0;
                $tgl = $nextDay;
                $pin = $kary->pin;
                $nip = $kary->nip;
                $scan_masuk = '';
                $scan_pulang = '';

                $tgl_masuk = $tgl;
                if (strtotime($jam_pulang) < strtotime($jam_masuk)) {
                    $tgl_pulang = date("Y-m-d", strtotime("+1 day", strtotime($tgl_masuk)));
                    $range_tgl = " CAST(tb_scanlog.scan_date AS DATE) BETWEEN '$tgl_masuk' AND '$tgl_pulang' ";
                } else {
                    $tgl_pulang = $tgl_masuk;
                    $range_tgl = " CAST(tb_scanlog.scan_date AS DATE) = '$tgl_masuk' ";
                }

                $tgl_masuk = $tgl_masuk.' '.$jam_masuk;
                $tgl_pulang = $tgl_pulang.' '.$jam_pulang;


                if ($tbScanlog) {
                    // tambah 3 jam untuk range masuk
                    $min_masuk = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($tgl_masuk)));
                    $plus_masuk = date('Y-m-d H:i:s', strtotime('+4 hour', strtotime($tgl_masuk)));
                    $plus_pulang = date('Y-m-d H:i:s', strtotime('+8 hour', strtotime($tgl_pulang)));
                    // $plus_pulang = date_format(date_create($tgl_pulang), 'Y-m-d').' '.'00:00:00';

                    if ($jam_masuk == '00:00:00' && $jam_pulang == '00:00:00') {
                        $checkDate = $this->db->query("
                            SELECT tb_scanlog.`nip`, tb_scanlog.pin, 
                                IFNULL(jam_masuk.jam,'') AS jam_masuk, 
                                IFNULL(jam_pulang.jam,'') AS jam_pulang 
                            FROM tb_scanlog 
                            LEFT JOIN (
                                SELECT MIN(scan_date) jam, nip
                                FROM tb_scanlog
                                WHERE DATE(scan_date) = DATE('$tgl_masuk')
                                GROUP BY nip
                            ) AS jam_masuk ON jam_masuk.nip = tb_scanlog.`nip`
                            LEFT JOIN (
                                SELECT MAX(scan_date) jam, nip
                                FROM tb_scanlog
                                WHERE DATE(scan_date) = DATE('$tgl_pulang')
                                GROUP BY nip
                            ) AS jam_pulang ON jam_pulang.nip = tb_scanlog.`nip`
                            WHERE tb_scanlog.nip = '$tbScanlog->nip' 
                            AND $range_tgl
                            GROUP BY tb_scanlog.`nip`")->row();
                    } else {
          //            $checkDate = $this->db->query("
          //                SELECT tb_scanlog.`nip`, tb_scanlog.pin,
          //                    IFNULL(jam_masuk.jam,'') AS jam_masuk,
          //                    IFNULL(jam_pulang.jam,'') AS jam_pulang
          //                FROM tb_scanlog
                            // LEFT JOIN (
                            //  SELECT scan_date AS jam, nip
                            //  FROM tb_scanlog
                            //  WHERE scan_date BETWEEN '$min_masuk' AND '$plus_masuk'
                            // ) AS jam_masuk ON jam_masuk.nip = tb_scanlog.`nip`
                            // LEFT JOIN (
                            //  SELECT scan_date AS jam, nip
                            //  FROM tb_scanlog
                            //  WHERE scan_date >= '$tgl_pulang'
                            //  AND DATE(scan_date) = DATE('$tgl_pulang')
                            // ) AS jam_pulang ON jam_pulang.nip = tb_scanlog.`nip`
                            // WHERE tb_scanlog.nip = '$tbScanlog->nip'
                            // AND $range_tgl
                            // GROUP BY tb_scanlog.`nip`")->row();

                        $checkDate = $this->db->query("
                            SELECT tb_scanlog.`nip`, tb_scanlog.pin, 
                                IFNULL(jam_masuk.jam,'') AS jam_masuk, 
                                IFNULL(jam_pulang.jam,'') AS jam_pulang 
                            FROM tb_scanlog 
                            LEFT JOIN (
                                SELECT scan_date AS jam, nip 
                                FROM tb_scanlog 
                                WHERE scan_date BETWEEN '$min_masuk' AND '$plus_masuk'
                            ) AS jam_masuk ON jam_masuk.nip = tb_scanlog.`nip`
                            LEFT JOIN (
                                SELECT scan_date AS jam, nip 
                                FROM tb_scanlog 
                                WHERE scan_date >= '$tgl_pulang' 
                                AND scan_date BETWEEN '$plus_masuk' AND '$plus_pulang'
                            ) AS jam_pulang ON jam_pulang.nip = tb_scanlog.`nip`
                            WHERE tb_scanlog.nip = '$tbScanlog->nip' 
                            AND $range_tgl
                            GROUP BY tb_scanlog.`nip`")->row();
                    }
                    
                    $scan_masuk = isset($checkDate->jam_masuk) ? $checkDate->jam_masuk : '';
                    $scan_pulang = isset($checkDate->jam_pulang) ? $checkDate->jam_pulang : '';

                    if ($scan_pulang == '') {
                        $a = $this->db->query("
                                SELECT MAX(scan_date) AS jam, nip 
                                FROM tb_scanlog 
                                WHERE scan_date BETWEEN '$scan_masuk' AND '$tgl_pulang'
                                AND scan_date <> '$scan_masuk'
                                AND scan_date >= '$plus_masuk'
                                AND nip = '$tbScanlog->nip'")->row();

                        $scan_pulang = isset($a->jam) ? $a->jam : '';
                    }

                    $this->db->where('scan_date', $tbScanlog->scan_date)->where('nip', $tbScanlog->nip)->update('tb_scanlog', array('process' => 1));
                }

                $checkTbAbsensi = $this->db->query("
                    SELECT * 
                    FROM tb_absensi 
                    WHERE tgl = '$tgl' 
                    AND nip = '$nip' 
                    AND flag = 0")->row();

                if ($checkTbAbsensi) {
                    $this->db->where('nip', $nip)->where('tgl', $tgl)->where('flag', 0)->delete('tb_absensi');
                }
                $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('presensi');

                $status = '';
                //hari libur
                $check_holiday = $this->db->where('hol_tgl', $tgl)->get('hol')->row();
                //absensi yg sudah diklarifikasi
                $check_absensi = $this->db->where(array('nip' => $nip, 'tgl' => $tgl, 'flag' => 1))->get('tb_absensi')->row();
                //cuti biasa
                $check_cuti_biasa = $this->db->query("
                                        SELECT * 
                                        FROM cuti_det a 
                                        JOIN cuti_sub_det b ON a.id_cuti_det = b.id_cuti_det 
                                        WHERE b.tgl = '$tgl' 
                                        AND a.nip = '$nip'
                                        AND a.approval = 12")->row();
                //cuti khusus
                $check_cuti_khusus = $this->db->query("
                                        SELECT a.*,b.tipe 
                                        FROM cuti_khusus a 
                                        JOIN tipe_cuti_khusus b ON a.`id_tipe_cuti`=b.`id_tipe` 
                                        WHERE a.`nip` = '$nip' 
                                        AND '$tgl' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`
                                        AND a.approval = 12")->row();
                //perdin
                $perdin = $this->db->query("
                            SELECT b.`nip`, a.`keperluan`
                            FROM perdin a
                            INNER JOIN perdin_det b ON a.`nobukti` = b.`nobukti`
                            WHERE b.`nip` = '$nip'
                            AND '$tgl' BETWEEN a.`tgl_brgkt` AND a.`tgl_pulang`")->row();

                //ijin meninggalkan
                $keluar_kantor = $this->db->query("
                                    SELECT *
                                    FROM ijinjam a 
                                    WHERE a.tgl = '$tgl'
                                    AND a.nip = '$nip' 
                                    AND a.status = 12 ")->row();

                //uang makan
                $um = $this->Api_Model->uang_makan($nip);
                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                $uang_denda = 0;

                $day_off = strtolower($nama_shift);
                if (empty($check_absensi)) {
                    // jika ada jadwal
                    if (!empty($shift)) {
                        // jika jadwal libur
                        if ($jam_masuk == '00:00:00' && $jam_pulang = '00:00:00') {
                            $uang_makan = 0;
                            $uang_denda = 0;
                            $flag = 0;

                            if (!empty($check_holiday)) {
                                $status = isset($check_holiday->hol_desc) ? $check_holiday->hol_desc : '';
                            } else {
                                $status = $keterangan_shift;
                            }
                        // jika jadwal tidak libur
                        } else {
                            // jika cuti biasa
                            if (!empty($check_cuti_biasa)) {
                                $uang_makan = 0;
                                $uang_denda = 0;
                                $flag = 0;
                                $status = isset($check_cuti_biasa->alasan_cuti) ? $check_cuti_biasa->alasan_cuti : '';
                            // jika cuti khusus
                            } elseif (!empty($check_cuti_khusus)) {
                                $uang_makan = 0;
                                $uang_denda = 0;
                                $flag = 0;
                                $status = isset($check_cuti_khusus->tipe) ? $check_cuti_khusus->tipe : '';
                            // jika perdin
                            } elseif (! empty($perdin)) {
                                $uang_denda = 0;
                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                $flag = 0;
                                $status = isset($perdin->keperluan) ? $perdin->keperluan : '';
                            // jika ijin keluar kantor
                            // } else if (! empty($keluar_kantor)) {
                            //  $uang_denda = 0;
                            //  $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                            //  $flag = 0;
                            //  $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                            // jika tidak cuti
                            } else {
                                if ($scan_masuk == '' && $scan_pulang == '') {
                                    $status = 'Tidak Masuk';
                                    $uang_makan = 0;
                                    $uang_denda = 0;
                                    $flag = 1;

                                    if (! empty($keluar_kantor)) {
                                        $uang_denda = 0;
                                        $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                        $flag = 0;
                                        $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                    }
                                } else {
                                    if ($scan_masuk == '' && $scan_pulang != '') {
                                        $status = 'Tidak Absen Masuk';
                                        $uang_makan = 0;
                                        $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                        $flag = 1;

                                        if (! empty($ishead)) {
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $uang_denda = 0;
                                        } else {
                                            if (! empty($keluar_kantor)) {
                                                $uang_denda = 0;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 0;
                                                $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                            }
                                        }
                                    } elseif ($scan_masuk != '' && $scan_pulang == '') {
                                        $status = 'Tidak Absen Pulang';
                                        $uang_makan = 0;
                                        $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                        $flag = 1;

                                        if (! empty($ishead)) {
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $uang_denda = 0;
                                        } else {
                                            if (! empty($keluar_kantor)) {
                                                $uang_denda = 0;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 0;
                                                $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                            }
                                        }
                                    } else {
                                        if (strtotime($scan_pulang) < strtotime($tgl_pulang) && $scan_pulang != '') {
                                            // cek pulang awal
                                            $plg_awal = $this->Absensi_Model->cek_pulang_awal($nip, $tgl);
                                            if (! empty($plg_awal)) {
                                                $jam_pulang_awal = isset($plg_awal->jam) ? jam($plg_awal->jam) : '';
                                                $alasan_pulang = isset($plg_awal->alasan) ? $plg_awal->alasan : '';

                                                $status = $jam_pulang_awal.' '.$alasan_pulang;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $uang_denda = 0;
                                                $flag = 0;
                                            } else {
                                                $status = 'Pulang Awal';
                                                $uang_makan = 0;
                                                $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 1;

                                                if (! empty($ishead)) {
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $uang_denda = 0;
                                                }
                                            }
                                        } else {
                                            $uang_denda = 0;
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;
                                        }
                                    }
                                }
                            }
                        }
                    // jika tidak ada jadwal
                    } else {
                        $uang_makan = 0;
                        $uang_denda = 0;
                        $flag = 0;

                        // jika hari libur
                        if (!empty($check_holiday)) {
                            $uang_makan = 0;
                            $uang_denda = 0;
                            $flag = 0;
                            $status = isset($check_holiday->hol_desc) ? $check_holiday->hol_desc : '';
                        } else {
                            // jika hari minggu
                            if (date('w', strtotime($tgl)) == 0) {
                                $status = 'Hari Minggu';
                            // jika hari sabtu
                            } elseif (date('w', strtotime($tgl)) == 6) {
                                $status = 'Hari Sabtu';
                            // jika tidak jadwal
                            } else {
                                // jika cuti biasa
                                if (!empty($check_cuti_biasa)) {
                                    $uang_makan = 0;
                                    $uang_denda = 0;
                                    $flag = 0;
                                    $status = isset($check_cuti_biasa->alasan_cuti) ? $check_cuti_biasa->alasan_cuti : '';
                                // jika cuti khusus
                                } elseif (!empty($check_cuti_khusus)) {
                                    $uang_makan = 0;
                                    $uang_denda = 0;
                                    $flag = 0;
                                    $status = isset($check_cuti_khusus->tipe) ? $check_cuti_khusus->tipe : '';
                                // jika perdin
                                } elseif (! empty($perdin)) {
                                    $uang_denda = 0;
                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                    $flag = 0;
                                    $status = isset($perdin->keperluan) ? $perdin->keperluan : '';
                                // jika ijin keluar kantor
                                // } else if (! empty($keluar_kantor)) {
                                //  $uang_denda = 0;
                                //  $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                //  $flag = 0;
                                //  $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                // jika tidak cuti
                                } else {
                                    if ($scan_masuk == '' && $scan_pulang == '') {
                                        $status = 'Tidak Masuk';
                                        $uang_makan = 0;
                                        $uang_denda = 0;
                                        $flag = 1;

                                        if (! empty($keluar_kantor)) {
                                            $uang_denda = 0;
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;
                                            $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                        }
                                    } else {
                                        if ($scan_masuk == '' && $scan_pulang != '') {
                                            $status = 'Tidak Absen Masuk';
                                            $uang_makan = 0;
                                            $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;

                                            if (! empty($ishead)) {
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $uang_denda = 0;
                                            } else {
                                                if (! empty($keluar_kantor)) {
                                                    $uang_denda = 0;
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $flag = 0;
                                                    $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                                }
                                            }
                                        } elseif ($scan_masuk != '' && $scan_pulang == '') {
                                            $status = 'Tidak Absen Pulang';
                                            $uang_makan = 0;
                                            $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;

                                            if (! empty($ishead)) {
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $uang_denda = 0;
                                            } else {
                                                if (! empty($keluar_kantor)) {
                                                    $uang_denda = 0;
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $flag = 0;
                                                    $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                                }
                                            }
                                        } else {
                                            if (strtotime($scan_pulang) < strtotime($tgl_pulang) && $scan_pulang != '') {
                                                // cek pulang awal
                                                $plg_awal = $this->Absensi_Model->cek_pulang_awal($nip, $tgl);
                                                if (! empty($plg_awal)) {
                                                    $jam_pulang_awal = isset($plg_awal->jam) ? jam($plg_awal->jam) : '';
                                                    $alasan_pulang = isset($plg_awal->alasan) ? $plg_awal->alasan : '';

                                                    $status = $jam_pulang_awal.' '.$alasan_pulang;
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $uang_denda = 0;
                                                    $flag = 0;
                                                } else {
                                                    $status = 'Pulang Awal';
                                                    $uang_makan = 0;
                                                    $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                                    $flag = 1;

                                                    if (! empty($ishead)) {
                                                        $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                        $uang_denda = 0;
                                                    }
                                                }
                                            } else {
                                                $uang_denda = 0;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if ($scan_masuk == '') {
                        $scan_masuk = '0000-00-00 00:00:00';
                    }
                    if ($scan_pulang == '') {
                        $scan_pulang = '0000-00-00 00:00:00';
                    }

                    if (!empty($check_holiday) || date('w', strtotime($tgl)) == 0 || date('w', strtotime($tgl)) == 6) {
                        $uang_makan = 0;
                        $uang_denda = 0;
                    }

                    $ins = array(
                        "nip" => $nip,
                        "pin" => $pin,
                        "tgl" => $tgl,
                        "scan_masuk" => $scan_masuk,
                        "scan_pulang" => $scan_pulang,
                        "status" => $status,
                        "shift" => $nama_shift,
                        "jam_masuk" => $jam_masuk,
                        "uang_makan" => $uang_makan,
                        "uang_denda" => $uang_denda
                    );


                    $this->db->insert('tb_absensi', $ins);

                    // jika flag = 1 maka isi presensi
                    if ($flag == 1 || $uang_denda > 0) {
                        // cek data presensi jika ada yang sama hapus dulu
                        $check_presensi = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('presensi')->row();
                        if ($check_presensi) {
                            $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('presensi');
                        }

                        $ins_presensi   = array(
                            'nip' => $nip,
                            'tgl' => $tgl,
                            'mangkir' => 1,
                            'ket' => $status,
                            'pin' => $pin
                            );

                        $this->db->insert('presensi', $ins_presensi);
                    } else {
                        $check_presensi = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('presensi')->row();
                        if ($check_presensi) {
                            $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('presensi');
                        }
                    }

                    $check_terlambat = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('terlambat_temp')->row();
                    if ($check_terlambat) {
                        $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('terlambat_temp');
                    }
                } else {
                    $ins = array(
                        "shift" => $nama_shift,
                        "jam_masuk" => $jam_masuk
                       );

                    $this->db->where('tgl', $tgl)->where('nip', $nip)->update('tb_absensi', $ins);
                }
            }
        }

        for ($i = 0; $i < count($tanggal); $i++) {
            if (empty($check_holiday)) {
                if (date('w', strtotime($tanggal[$i])) != 0) {
                    if (date('w', strtotime($tanggal[$i])) != 6) {
                        $this->Api_Model->generate_terlambat($tanggal[$i], $w_nip);
                    }
                }
            }
        }
        echo json_encode(array('message' => 'Completed!'));
    }

    function absensiproses($date_start = '', $date_end = '', $nip = '')
    {
        ini_set('memory_limit', '-1');
        ini_set('MAX_EXECUTION_TIME', '-1');
        set_time_limit(0);
        $sekarang = date('Y-m-d');

        if ($date_start == '' || $date_end == '') {
            $date_start = $sekarang;
            $date_end = $sekarang;
        }

        // $ip = file_get_contents('http://api.ipify.org');
        // $log = array(
        //  'ip' => $ip,
        //  'keterangan' => 'tgl '.$date_start.' s/d '.$date_end
        // );

        // $this->Main_Model->process_data('log_proses_absensi', $log);

        if ($nip == '') {
            $w_nip = '';
        } else {
            $w_nip = " AND a.nip = '$nip' ";
        }

        $karyawan = $this->db->query("
			SELECT a.pin, a.nip 
			FROM kary a
			WHERE (isnull(a.tgl_resign) OR (a.tgl_resign >= '$date_start' AND a.tgl_resign <= '$date_end'))
			AND a.tgl_masuk <= '$date_start' 
			$w_nip ")->result();

        $tanggal = array();
        while (strtotime($date_start) <= strtotime($date_end)) {
                $tanggal[] = $date_start;
                $date_start = date("Y-m-d", strtotime("+1 day", strtotime($date_start)));
        }
        foreach ($karyawan as $kary) {
            for ($i = 0; $i < count($tanggal); $i++) {
                $nextDay = $tanggal[$i];

                //is head
                $ishead = $this->db->where('penyetuju', $kary->nip)
                            ->get('ms_rule_approval')
                            ->row();

                //cari scanlog
                $tbScanlog = $this->Api_Model->view_scanlog($nextDay, $kary->nip);

                //cari shift
                $shift = $this->Api_Model->cari_shift($nextDay, $kary->nip);
                $default_shift = $this->Api_Model->default_shift();

                if (!empty($shift)) {
                    $masuk = strtotime($shift->jam_masuk);
                    $jam_masuk = $shift->jam_masuk;
                    $pulang = strtotime($shift->jam_pulang);
                    $nama_shift = $shift->nama;
                    $keterangan_shift = $shift->keterangan;
                    $jam_pulang = $shift->jam_pulang;
                    $flag_lembur = $shift->lembur;
                } else {
                    $masuk = strtotime($default_shift->jam_masuk);
                    $jam_masuk  = $default_shift->jam_masuk;
                    $nama_shift = $default_shift->nama;
                    $keterangan_shift = $default_shift->keterangan;
                    $pulang = strtotime($default_shift->jam_pulang);
                    $jam_pulang = $default_shift->jam_pulang;
                    $flag_lembur = $default_shift->lembur;
                }

                $flag = 0;
                $tgl = $nextDay;
                $pin = $kary->pin;
                $nip = $kary->nip;
                $scan_masuk = '';
                $scan_pulang = '';

                $tgl_masuk = $tgl;
                if (strtotime($jam_pulang) < strtotime($jam_masuk)) {
                    $tgl_pulang = date("Y-m-d", strtotime("+1 day", strtotime($tgl_masuk)));
                    $range_tgl = " CAST(tb_scanlog.scan_date AS DATE) BETWEEN '$tgl_masuk' AND '$tgl_pulang' ";
                } else {
                    $tgl_pulang = $tgl_masuk;
                    $range_tgl = " CAST(tb_scanlog.scan_date AS DATE) = '$tgl_masuk' ";
                }

                $tgl_masuk = $tgl_masuk.' '.$jam_masuk;
                $tgl_pulang = $tgl_pulang.' '.$jam_pulang;


                if ($tbScanlog) {
                    // tambah 3 jam untuk range masuk
                    $min_masuk = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($tgl_masuk)));
                    $plus_masuk = date('Y-m-d H:i:s', strtotime('+4 hour', strtotime($tgl_masuk)));
                    $plus_pulang = date('Y-m-d H:i:s', strtotime('+8 hour', strtotime($tgl_pulang)));
                    // $plus_pulang = date_format(date_create($tgl_pulang), 'Y-m-d').' '.'00:00:00';

                    if ($jam_masuk == '00:00:00' && $jam_pulang == '00:00:00') {
                        $checkDate = $this->db->query("
	            			SELECT tb_scanlog.`nip`, tb_scanlog.pin, 
	            				IFNULL(jam_masuk.jam,'') AS jam_masuk, 
	            				IFNULL(jam_pulang.jam,'') AS jam_pulang 
	            			FROM tb_scanlog 
							LEFT JOIN (
								SELECT MIN(scan_date) jam, nip
								FROM tb_scanlog
								WHERE DATE(scan_date) = DATE('$tgl_masuk')
								GROUP BY nip
							) AS jam_masuk ON jam_masuk.nip = tb_scanlog.`nip`
							LEFT JOIN (
								SELECT MAX(scan_date) jam, nip
								FROM tb_scanlog
								WHERE DATE(scan_date) = DATE('$tgl_pulang')
								GROUP BY nip
							) AS jam_pulang ON jam_pulang.nip = tb_scanlog.`nip`
							WHERE tb_scanlog.nip = '$tbScanlog->nip' 
							AND $range_tgl
							GROUP BY tb_scanlog.`nip`")->row();
                    } else {
                        $checkDate = $this->db->query("
	            			SELECT tb_scanlog.`nip`, tb_scanlog.pin, 
	            				IFNULL(jam_masuk.jam,'') AS jam_masuk, 
	            				IFNULL(jam_pulang.jam,'') AS jam_pulang 
	            			FROM tb_scanlog 
							LEFT JOIN (
								SELECT scan_date AS jam, nip 
								FROM tb_scanlog 
								WHERE scan_date BETWEEN '$min_masuk' AND '$plus_masuk'
							) AS jam_masuk ON jam_masuk.nip = tb_scanlog.`nip`
							LEFT JOIN (
								SELECT scan_date AS jam, nip 
								FROM tb_scanlog 
								WHERE scan_date >= '$tgl_pulang' 
								AND scan_date BETWEEN '$plus_masuk' AND '$plus_pulang'
							) AS jam_pulang ON jam_pulang.nip = tb_scanlog.`nip`
							WHERE tb_scanlog.nip = '$tbScanlog->nip' 
							AND $range_tgl
							GROUP BY tb_scanlog.`nip`")->row();
                    }
                    
                    $scan_masuk = isset($checkDate->jam_masuk) ? $checkDate->jam_masuk : '';
                    $scan_pulang = isset($checkDate->jam_pulang) ? $checkDate->jam_pulang : '';

                    if ($scan_pulang == '') {
                        $a = $this->db->query("
            					SELECT MAX(scan_date) AS jam, nip 
            					FROM tb_scanlog 
            					WHERE scan_date BETWEEN '$scan_masuk' AND '$tgl_pulang'
            					AND scan_date <> '$scan_masuk'
            					AND scan_date >= '$plus_masuk'
            					AND nip = '$tbScanlog->nip'")->row();

                        $scan_pulang = isset($a->jam) ? $a->jam : '';
                    }

                    $this->db->where('scan_date', $tbScanlog->scan_date)->where('nip', $tbScanlog->nip)->update('tb_scanlog', array('process' => 1));
                }

                $checkTbAbsensi = $this->db->query("
                	SELECT * 
                	FROM tb_absensi 
                	WHERE tgl = '$tgl' 
                	AND nip = '$nip' 
                	AND flag = 0")->row();

                if ($checkTbAbsensi) {
                    $this->db->where('nip', $nip)->where('tgl', $tgl)->where('flag', 0)->delete('tb_absensi');
                }
                $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('presensi');

                $status = '';
                //hari libur
                $check_holiday = $this->db->where('hol_tgl', $tgl)->get('hol')->row();
                //absensi yg sudah diklarifikasi
                $check_absensi = $this->db->where(array('nip' => $nip, 'tgl' => $tgl, 'flag' => 1))->get('tb_absensi')->row();
                //cuti biasa
                $check_cuti_biasa = $this->db->query("
					                	SELECT * 
					                	FROM cuti_det a 
					                	JOIN cuti_sub_det b ON a.id_cuti_det = b.id_cuti_det 
					                	WHERE b.tgl = '$tgl' 
					                	AND a.nip = '$nip'
					                	AND a.approval = 12")->row();
                //cuti khusus
                $check_cuti_khusus = $this->db->query("
                						SELECT a.*,b.tipe 
                						FROM cuti_khusus a 
                						JOIN tipe_cuti_khusus b ON a.`id_tipe_cuti`=b.`id_tipe` 
                						WHERE a.`nip` = '$nip' 
                						AND '$tgl' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`
                						AND a.approval = 12")->row();
                //perdin
                $perdin = $this->db->query("
                			SELECT b.`nip`, a.`keperluan`
							FROM perdin a
							INNER JOIN perdin_det b ON a.`nobukti` = b.`nobukti`
							WHERE b.`nip` = '$nip'
							AND '$tgl' BETWEEN a.`tgl_brgkt` AND a.`tgl_pulang`")->row();

                //ijin meninggalkan
                $keluar_kantor = $this->db->query("
                					SELECT *
                					FROM ijinjam a 
                					WHERE a.tgl = '$tgl'
                					AND a.nip = '$nip' 
                					AND a.status = 12 ")->row();

                //uang makan
                $um = $this->Api_Model->uang_makan($nip);
                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                $uang_denda = 0;

                $day_off = strtolower($nama_shift);
                if (empty($check_absensi)) {
                    // jika ada jadwal
                    if (!empty($shift)) {
                        // jika jadwal libur
                        if ($jam_masuk == '00:00:00' && $jam_pulang == '00:00:00') {
                            $uang_makan = 0;
                            $uang_denda = 0;
                            $flag = 0;

                            if (!empty($check_holiday)) {
                                $status = isset($check_holiday->hol_desc) ? $check_holiday->hol_desc : '';
                            } else {
                                $status = $keterangan_shift;
                            }
                        // jika jadwal tidak libur
                        } else {
                            // jika cuti biasa
                            if (!empty($check_cuti_biasa)) {
                                $uang_makan = 0;
                                $uang_denda = 0;
                                $flag = 0;
                                $status = isset($check_cuti_biasa->alasan_cuti) ? $check_cuti_biasa->alasan_cuti : '';
                            // jika cuti khusus
                            } elseif (!empty($check_cuti_khusus)) {
                                $uang_makan = 0;
                                $uang_denda = 0;
                                $flag = 0;
                                $status = isset($check_cuti_khusus->tipe) ? $check_cuti_khusus->tipe : '';
                            // jika perdin
                            } elseif (! empty($perdin)) {
                                $uang_denda = 0;
                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                $flag = 0;
                                $status = isset($perdin->keperluan) ? $perdin->keperluan : '';
                            // jika ijin keluar kantor
                            // } else if (! empty($keluar_kantor)) {
                            //  $uang_denda = 0;
                            //  $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                            //  $flag = 0;
                            //  $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                            // jika tidak cuti
                            } else {
                                if ($scan_masuk == '' && $scan_pulang == '') {
                                    $status = 'Tidak Masuk';
                                    $uang_makan = 0;
                                    $uang_denda = 0;
                                    $flag = 1;

                                    if (! empty($keluar_kantor)) {
                                        $uang_denda = 0;
                                        $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                        $flag = 0;
                                        $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                    }
                                } else {
                                    if ($scan_masuk == '' && $scan_pulang != '') {
                                        $status = 'Tidak Absen Masuk';
                                        $uang_makan = 0;
                                        $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                        $flag = 1;

                                        if (! empty($ishead)) {
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $uang_denda = 0;
                                        } else {
                                            if (! empty($keluar_kantor)) {
                                                $uang_denda = 0;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 0;
                                                $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                            }
                                        }
                                    } elseif ($scan_masuk != '' && $scan_pulang == '') {
                                        $status = 'Tidak Absen Pulang';
                                        $uang_makan = 0;
                                        $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                        $flag = 1;

                                        if (! empty($ishead)) {
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $uang_denda = 0;
                                        } else {
                                            if (! empty($keluar_kantor)) {
                                                $uang_denda = 0;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 0;
                                                $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                            }
                                        }
                                    } else {
                                        if (strtotime($scan_pulang) < strtotime($tgl_pulang) && $scan_pulang != '') {
                                            // cek pulang awal
                                            $plg_awal = $this->Absensi_Model->cek_pulang_awal($nip, $tgl);
                                            if (! empty($plg_awal)) {
                                                $jam_pulang_awal = isset($plg_awal->jam) ? jam($plg_awal->jam) : '';
                                                $alasan_pulang = isset($plg_awal->alasan) ? $plg_awal->alasan : '';

                                                $status = $jam_pulang_awal.' '.$alasan_pulang;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $uang_denda = 0;
                                                $flag = 0;
                                            } else {
                                                $status = 'Pulang Awal';
                                                $uang_makan = 0;
                                                $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 1;

                                                if (! empty($ishead)) {
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $uang_denda = 0;
                                                }
                                            }
                                        } else {
                                            $uang_denda = 0;
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;
                                        }
                                    }
                                }
                            }
                        }
                    // jika tidak ada jadwal
                    } else {
                        $uang_makan = 0;
                        $uang_denda = 0;
                        $flag = 0;

                        // jika hari libur
                        if (!empty($check_holiday)) {
                            $uang_makan = 0;
                            $uang_denda = 0;
                            $flag = 0;
                            $status = isset($check_holiday->hol_desc) ? $check_holiday->hol_desc : '';
                        } else {
                            // jika hari minggu
                            if (date('w', strtotime($tgl)) == 0) {
                                $status = 'Hari Minggu';
                            // jika hari sabtu
                            } elseif (date('w', strtotime($tgl)) == 6) {
                                $status = 'Hari Sabtu';
                            // jika tidak jadwal
                            } else {
                                // jika cuti biasa
                                if (!empty($check_cuti_biasa)) {
                                    $uang_makan = 0;
                                    $uang_denda = 0;
                                    $flag = 0;
                                    $status = isset($check_cuti_biasa->alasan_cuti) ? $check_cuti_biasa->alasan_cuti : '';
                                // jika cuti khusus
                                } elseif (!empty($check_cuti_khusus)) {
                                    $uang_makan = 0;
                                    $uang_denda = 0;
                                    $flag = 0;
                                    $status = isset($check_cuti_khusus->tipe) ? $check_cuti_khusus->tipe : '';
                                // jika perdin
                                } elseif (! empty($perdin)) {
                                    $uang_denda = 0;
                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                    $flag = 0;
                                    $status = isset($perdin->keperluan) ? $perdin->keperluan : '';
                                // jika ijin keluar kantor
                                // } else if (! empty($keluar_kantor)) {
                                //  $uang_denda = 0;
                                //  $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                //  $flag = 0;
                                //  $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                // jika tidak cuti
                                } else {
                                    if ($scan_masuk == '' && $scan_pulang == '') {
                                        $status = 'Tidak Masuk';
                                        $uang_makan = 0;
                                        $uang_denda = 0;
                                        $flag = 1;

                                        if (! empty($keluar_kantor)) {
                                            $uang_denda = 0;
                                            $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;
                                            $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                        }
                                    } else {
                                        if ($scan_masuk == '' && $scan_pulang != '') {
                                            $status = 'Tidak Absen Masuk';
                                            $uang_makan = 0;
                                            $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;

                                            if (! empty($ishead)) {
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $uang_denda = 0;
                                            } else {
                                                if (! empty($keluar_kantor)) {
                                                    $uang_denda = 0;
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $flag = 0;
                                                    $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                                }
                                            }
                                        } elseif ($scan_masuk != '' && $scan_pulang == '') {
                                            $status = 'Tidak Absen Pulang';
                                            $uang_makan = 0;
                                            $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                            $flag = 0;

                                            if (! empty($ishead)) {
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $uang_denda = 0;
                                            } else {
                                                if (! empty($keluar_kantor)) {
                                                    $uang_denda = 0;
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $flag = 0;
                                                    $status = isset($keluar_kantor->keterangan) ? $keluar_kantor->keterangan : '';
                                                }
                                            }
                                        } else {
                                            if (strtotime($scan_pulang) < strtotime($tgl_pulang) && $scan_pulang != '') {
                                                // cek pulang awal
                                                $plg_awal = $this->Absensi_Model->cek_pulang_awal($nip, $tgl);
                                                if (! empty($plg_awal)) {
                                                    $jam_pulang_awal = isset($plg_awal->jam) ? jam($plg_awal->jam) : '';
                                                    $alasan_pulang = isset($plg_awal->alasan) ? $plg_awal->alasan : '';

                                                    $status = $jam_pulang_awal.' '.$alasan_pulang;
                                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                    $uang_denda = 0;
                                                    $flag = 0;
                                                } else {
                                                    $status = 'Pulang Awal';
                                                    $uang_makan = 0;
                                                    $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                                    $flag = 1;

                                                    if (! empty($ishead)) {
                                                        $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                        $uang_denda = 0;
                                                    }
                                                }
                                            } else {
                                                $uang_denda = 0;
                                                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                                $flag = 0;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if ($scan_masuk == '') {
                        $scan_masuk = '0000-00-00 00:00:00';
                    }
                    if ($scan_pulang == '') {
                        $scan_pulang = '0000-00-00 00:00:00';
                    }

                    if (!empty($check_holiday) || date('w', strtotime($tgl)) == 0 || date('w', strtotime($tgl)) == 6) {
                        if ($flag_lembur == 1) {
                            $uang_makan = 0;
                            $uang_denda = 0;
                        } else {
                            if (! empty($shift) && ($jam_masuk != '00:00:00' || $jam_pulang != '00:00:00')) {
                                if ($flag == 0) {
                                    $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                                    $uang_denda = 0;
                                } else {
                                    if (($scan_masuk != '0000-00-00 00:00:00' || $scan_pulang != '0000-00-00 00:00:00')) {
                                        $uang_makan = 0;
                                        $uang_denda = isset($um->nominal) ? $um->nominal : 0;
                                    } else {
                                        $uang_makan = 0;
                                        $uang_denda = 0;
                                    }
                                }
                            } else {
                                $uang_makan = 0;
                                $uang_denda = 0;
                            }
                        }
                    }

                    $ins = array(
                        "nip" => $nip,
                        "pin" => $pin,
                        "tgl" => $tgl,
                        "scan_masuk" => $scan_masuk,
                        "scan_pulang" => $scan_pulang,
                        "status" => $status,
                        "shift" => $nama_shift,
                        "jam_masuk" => $jam_masuk,
                        "uang_makan" => $uang_makan,
                        "uang_denda" => $uang_denda
                    );


                    $this->db->insert('tb_absensi', $ins);

                    // jika flag = 1 maka isi presensi
                    if ($flag == 1 || $uang_denda > 0) {
                        // cek data presensi jika ada yang sama hapus dulu
                        $check_presensi = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('presensi')->row();
                        if ($check_presensi) {
                            $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('presensi');
                        }

                        $ins_presensi   = array(
                            'nip' => $nip,
                            'tgl' => $tgl,
                            'mangkir' => 1,
                            'ket' => $status,
                            'pin' => $pin
                            );

                        $this->db->insert('presensi', $ins_presensi);
                    } else {
                        $check_presensi = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('presensi')->row();
                        if ($check_presensi) {
                            $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('presensi');
                        }
                    }

                    $check_terlambat = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('terlambat_temp')->row();
                    if ($check_terlambat) {
                        $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('terlambat_temp');
                    }
                } else {
                    $ins = array(
                        "shift" => $nama_shift,
                        "jam_masuk" => $jam_masuk
                       );

                    $this->db->where('tgl', $tgl)->where('nip', $nip)->update('tb_absensi', $ins);
                }
            }
        }

        for ($i = 0; $i < count($tanggal); $i++) {
            if (empty($check_holiday)) {
                if (date('w', strtotime($tanggal[$i])) != 0) {
                    if (date('w', strtotime($tanggal[$i])) != 6) {
                        $this->Api_Model->generate_terlambat($tanggal[$i], $w_nip);
                    }
                }
            }
        }
        echo json_encode(array('message' => 'Completed!'));
    }

    function scan()
    {
        $this->Main_Model->all_login();
        $mode = $this->input->post('mode');
        $mesin = $this->input->post('mesin');

        $scan_mode = ($mode == 'new') ? '/scanlog/new' : '/scanlog/all';
        $q = $this->Main_Model->view_by_id('ms_mesin', array('id' => $mesin), 'row');
        $sn = $q->sn;
        $ip = $q->ip;
        $port = $q->port;

        $parameter = 'sn='.$sn;
        $url = $ip.$scan_mode;
        $result = $this->webservice($port, $url, $parameter);
        $content = json_decode($result);
        if ($content->Result == true) {
            foreach ($content->Data as $row) {
                $check_exists = $this->Api_Model->check_scan($row->ScanDate, $row->PIN);
                if (empty($check_exists)) {
                    $data = array(
                        'scan_date' => $row->ScanDate,
                        'pin' => $row->PIN,
                        'verifymode' => $row->VerifyMode,
                        'iomode' => $row->IOMode,
                        'workcode' => $row->WorkCode,
                        'sn' => $sn
                        );
                    $this->Api_Model->add_scan($data);
                }
            }
            $jumlah = count($content->Data);
            $status = array('status' => 200, 'message' => 'Sukses menarik '.$jumlah.' data!');
        } else {
            $status = array('status' => 400, 'message' => 'Tidak ada data yang berhasil ditarik!');
        }

        echo json_encode($status);
    }

    function json_mesin()
    {
        $mode = $this->input->post('mode');
        $mesin = $this->input->post('mesin');
        
        $q = $this->Main_Model->view_by_id('ms_mesin', array('id' => $mesin), 'row');
        $scan = ($mode == 'new') ? 'scanlog_new' : 'scanlog_all';
        $port = isset($q->port) ? $q->port : '8800';
        $ip = isset($q->ip) ? $q->ip : '10.90.50.14';
        $sn = isset($q->sn) ? $q->sn : '';
        $url = 'http://appsmg.gmedia.net.id:2180/apipsp/index.php/api/'.$scan.'/'.$port.'/'.$ip.'/'.$sn;
        $res = array('url' => $url);
        echo json_encode($res);
    }
}

/* End of file surat_peringatan.php */
/* Location: ./application/controllers/surat_peringatan.php */
