<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Absensi extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Absensi_Model', '', true);
        $this->load->model('Cuti_Model', '', true);
        $this->load->model('Gaji_Model', '', true);
        $this->load->model('Api_Model', '', true);
        $this->load->model('Rest_Model', '', true);
        $this->load->model('Reminder_Model', '', true);
    }
    
    function header()
    {
        $menu = '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">
				<link href="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css') . '" rel="stylesheet" type="text/css" />';
        return $menu;
    }
    
    function footer()
    {
        $footer = '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js') . '"></script>';
        
        return $footer;
    }
    
    function dataabsensi($id = '')
    {
        $this->Main_Model->get_login();
        $javascript = '
			<script type="text/javascript">
				function load_table() {
					var tgl_awal = $("#tgl_awal").val();
					var tgl_akhir = $("#tgl_akhir").val();
					$.ajax({
						url : "'.base_url('absensi/view_absensi').'",
						type : "GET",
						data : {
							"tgl_awal" : tgl_awal,
							"tgl_akhir" : tgl_akhir
 						},
						beforeSend : function() {
							$("#tampil").addClass("hidden");
							$("#unduh").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function() {
							$("#tampil").removeClass("hidden");
							$("#unduh").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#absensi").html(data);
							$("#absensi-tbl").DataTable({
        						responsive: true
    						});
						}, 
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal mengambil data!");
						}
					});	
				}
    			
    			$("#tampil").click(function(){
    				load_table();
    			});

    			$("#unduh").click(function(){
					var tgl_awal = $("#tgl_awal").val();
					var tgl_akhir = $("#tgl_akhir").val();
					window.location.href="'.base_url('download_excel/data_absensi').'?tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir;
				});


    			$(document).ready(function(){
    				$(".select2").select2();
    				$(".date-picker").datepicker({
    					autoclose : true,
    					orientation : "bottom"
    				});
    			});
			</script>';
        $data = array(
            'periode' => $this->Main_Model->periode_opt(),
            'javascript' => minifyjs($javascript),
            'js' => $this->footer()
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '4')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/dataabsensi', $data);
    }
    
    function datapresensi()
    {
        $this->Main_Model->get_login();
        $javascript = '
			<script type="text/javascript">

				function hilang() {
					$("#tampil").addClass("hidden");
					$("#imgload").removeClass("hidden");
					$("#unduh").addClass("hidden");
				}

				function muncul() {
					$("#tampil").removeClass("hidden");
					$("#imgload").addClass("hidden");
					$("#unduh").removeClass("hidden");
				}

				function load_table() {
					var tgl_awal = $("#tgl_awal").val();
					var tgl_akhir = $("#tgl_akhir").val();
					$.ajax({
						url : "'.base_url('absensi/view_presensi').'",
						type : "GET",
						data : {
							"tgl_awal" : tgl_awal,
							"tgl_akhir" : tgl_akhir
						},
						beforeSend : function(){
							hilang();
						},
						complete : function(){
							muncul();
						},
						success : function(data){
							$("#presensi").html(data);
							$("#presensi-tbl").DataTable({
        						responsive: true
    						});
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal mengambil data!");
						}
					});
				}

				$(document).ready(function(){
					$(".select2").select2(), load_table();
					$(".date-picker").datepicker({
    					autoclose : true,
    					orientation : "bottom"
    				});
				});

				$("#tampil").click(function(){
					load_table();
				});

				$("#unduh").click(function(){
					var tgl_awal = $("#tgl_awal").val();
					var tgl_akhir = $("#tgl_akhir").val();
					window.location.href="'.base_url('download_excel/data_presensi').'?tgl_awal="+tgl_awal+"&tgl_akhir="+tgl_akhir;
				});

			</script>';
        $footer = array(
            'javascript' => minifyjs($javascript),
            'js' => $this->footer()
        );
        $data = array(
            'periode' => $this->Main_Model->periode_opt()
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '4')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/datapresensi', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function dataijinperjam()
    {
        $this->Main_Model->get_login();
        $url_table = base_url('absensi/view_ijinjam');
        $url_del = base_url('absensi/delete_ijinjam');

        $month = date('m');
        // $month = str_replace('0', '', $month);
        $month = (int)$month;

        $year = date('Y');

        $ms_penyetuju = $this->Main_Model->master_penyetuju();

        $penyetuju = array();
        foreach ($ms_penyetuju as $row) {
            $penyetuju[$row->pola] = $row->nama;
        }

        $javascript = '
			<script type="text/javascript">
				var save_method;
				var nip = $("#nip");
				var tgl = $("#tgl");
				var jmljam = $("#jmljam");
				var keterangan = $("#keterangan");
				var id_ijin = $("#id");

				function set_form() {
					nip.val("").trigger("change");
					tgl.val("");
					jmljam.val("");
					keterangan.val("");
					id_ijin.val("");
					$("#penyetuju").val("").trigger("change");
				}

				function clear_form() {
					set_form();
					save_method="save";
					$("#title").html("Form Ijin Meninggalkan Kerja");
					$("#cutibtn").removeClass("hidden");
				}

				'.$this->Main_Model->default_select2()

                 .$this->Main_Model->default_datepicker().'

				function load_table() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.$url_table.'",
						data : {
							"tahun" : tahun,
							"bulan" : bulan
						},
						type : "POST",
						beforeSend 	: function(){
							$("#tampil").addClass("hidden");
							$("#unduh").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#unduh").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							'.$this->Main_Model->default_datatable().'
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal mengambil data!");
						}
					});
				}

				$(document).ready(function(){
					var year = "'.$year.'";
					var month = "'.$month.'";

					$("#tahun").val(year).trigger("change");
					$("#bulan").val(month).trigger("change");

					load_table();
				});

				$("#tampil").click(function(){
					load_table();
				});

				$("#form_ijin").submit(function(e){
					e.preventDefault();
					var formData = new FormData($(this)[0]);
					$.ajax({
						url : "'.base_url('absensi/process_ijinjam').'",
						type : "post",
						data : formData,
						dataType : "json",
						async : false,
						cache : false,
						contentType : false,
						processData : false,
						success : function(data){
							if(data.status == true) {
		    					set_form();
		    					load_table();
		    					$("#myModal").modal("toggle");
		    				}
		    				bootbox.alert(data.message);
						}
					});
				});

				function get_id(id) {
					save_method = "update";
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('absensi/ijinjam_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType : "JSON",
			    		success : function(data){
			    			$("#myModal").modal();
							keterangan.val(data.keterangan);
							jmljam.val(data.jmljam);
							nip.val(data.nip).trigger("change");
							id_ijin.val(data.id);
							tgl.val(data.tgl);
							$("#penyetuju").val(data.pola).trigger("change");
							$("#title").html("Form Ijin Meninggalkan Kerja");
							$("#starttime_hour").val(data.starttime_hour);
							$("#starttime_minute").val(data.starttime_minute);
							$("#endtime_hour").val(data.endtime_hour);
							$("#endtime_minute").val(data.endtime_minute);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			bootbox.alert("Oops Something Went Wrong!");
			    		}
			    	});
			    }

			    function proses(id, val) {
					$.ajax({
						url : "'.base_url('absensi/approval_ijinjam').'/"+id+"/"+val,
						type : "post"
					})
				}

			    function approval(id) {
					bootbox.dialog({
                        message : "Yakin ingin memproses Meninggalkan Kerja?",
                        title : "Proses Meninggalkan Kerja",
                        buttons :{
                        	success : {
                                label : "Setuju",
                                className : "green",
                                callback : function(){
                                	proses(id, "approve");
                                	bootbox.alert("Ijin telah disetujui !");
                                	load_table();
                                }    
                            },
                            danger : {
                                label : "Tolak",
                                className : "red",
                                callback : function(){
                                	proses(id, "reject");
                                	bootbox.alert("Ijin telah ditolak !");
                                	load_table();
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    });
				}

			    $("#unduh").click(function(){
			    	var tahun = $("#tahun").val();
			    	var bulan = $("#bulan").val();
			    	window.location.href="'.base_url('download_excel/meninggalkan_kerja').'/"+tahun+"/"+bulan;
			    });

			    '.$this->Main_Model->default_delete_data($url_del).'
			</script>';

        $footer = array(
            'javascript' => minifyjs($javascript),
            'js' => $this->footer()
        );

        $data = array(
            'nip' => $this->Main_Model->all_kary_active(),
            'penyetuju' => $penyetuju,
            'periode' => $this->Main_Model->periode_opt()
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '4')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/dataijinperjam', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_ijinjam()
    {
        $this->Main_Model->get_login();
        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $p = periode($tahun, $bulan);
        $qd_id = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Absensi_Model->view_ijinjam($qd_id);
        $template = $this->Main_Model->tbl_temp();
        
        $this->table->set_heading('No', 'NIP', 'Nama', 'Cabang', 'Tanggal', 'Dari', 'Sampai', 'Keterangan', 'Status', 'Scanlog', 'Action');
        $i   = 1;
        foreach ($data as $row) {
            // ($row->kode != 6) ? $link = '' : $link = '<li> <a href="javascript:;" onclick="approval(' . $row->id . ');"> <i class="icon-edit"></i> Proses </a> </li>';
            $link = '';

            $this->table->add_row(
                $i++,
                $row->nip,
                $row->nama,
                $row->cabang,
                $row->tgl,
                jam($row->dari),
                jam($row->sampai),
                $row->keterangan,
                $row->app_status,
                $row->scanlog,
                '<div class="btn-group">
		            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
		                <i class="fa fa-angle-down"></i>
		            </button>
		                <ul class="dropdown-menu" role="menu">
		                	'.$link.'
		                    <li>
		                        <a href="javascript:;" onclick="get_id(' . $row->id . ');">
		                            <i class="icon-edit"></i> Update </a>
		                    </li>
		                    <li>
		                        <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
		                            <i class="icon-delete"></i> Delete </a>
		                    </li>
		                </ul>
		        </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function process_ijinjam()
    {
        $this->Main_Model->all_login();
        $id = $this->input->post('id');
        $nip = $this->input->post('nip');
        $penyetuju = $this->input->post('penyetuju');
        $tgl = $this->input->post('tgl');
        // $jml_menit = $this->input->post('jmljam');
        
        $starttime_hour = $this->input->post('starttime_hour');
        $starttime_minute = $this->input->post('starttime_minute');
        $endtime_hour = $this->input->post('endtime_hour');
        $endtime_minute = $this->input->post('endtime_minute');

        $keterangan = $this->input->post('keterangan');
        $username = $this->session->userdata('username');
        $acctype = $this->session->userdata('acctype');
        $now = now();

        if ($nip == '' || $penyetuju == '' || $tgl == '' || $keterangan == '' || $starttime_hour == '' || $starttime_minute == '' || $endtime_hour == '' || $endtime_minute == '') {
            $message = '';
            if ($nip == '') {
                $message .= " Nama Karyawan wajib diisi  <br>";
            }
            if ($penyetuju == '') {
                $message .= " Penyetuju wajib diisi <br>";
            }
            if ($tgl == '') {
                $message .= 'Tanggal wajib diisi <br>';
            }
            if ($jml_menit == '') {
                $message .= 'Jumlah menit wajib diisi <br>';
            }
            if ($keterangan == '') {
                $message .= 'Keterangan wajib diisi ';
            }
            if ($starttime_hour == '' || $starttime_minute == '') {
                $message .= 'Waktu Mulai wajib diisi';
            }
            if ($endtime_hour == '' || $endtime_minute == '') {
                $message .= 'Waktu Selesai wajib diisi';
            }
            $status = false;
        } else {
            $pola = $this->Main_Model->kode_penyetuju($penyetuju);
            $kode_pola = isset($pola->pola) ? $pola->pola : '';
            $approval = isset($pola->kode) ? $pola->kode : '';
            $penyetuju = isset($pola->penyetuju) ? $pola->penyetuju : '';
            $cc = isset($pola->cc) ? $pola->cc : '';

            $user = ($id != '') ? 'user_update' : 'user_insert';
            $time = ($id != '') ? 'update_at' : 'insert_at';

            if ($acctype == 'Administrator') {
                $approval = 12;
            }

            $data = array(
                'nip' => $nip,
                'tgl' => $this->Main_Model->convert_tgl($tgl),
                'jmljam' => 0,
                'dari' => $starttime_hour.':'.$starttime_minute,
                'sampai' => $endtime_hour.':'.$endtime_minute,
                'status' => $approval,
                'keterangan' => $keterangan,
                'pola' => $kode_pola,
                'penyetuju' => $penyetuju,
                'cc' => $cc,
                $user => $username,
                $time => $now
            );

            $condition = [];
            if ($id != '') {
                $condition['id'] = $id;
            }

            $simpan = $this->Main_Model->process_data('ijinjam', $data, $condition);
            $message = 'Success';
            $status = true;

            if ($acctype != 'Administrator') {
                $this->Rest_Model->reminder_keluar_kantor($data);
            }
        }

        $response = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($response);
    }

    function approval_ijinjam($id = '', $val = '')
    {
        $this->Main_Model->all_login();
        $user = $this->session->userdata('username');
        $date = date('Y-m-d H:i:s');
        $k = $this->Main_Model->view_by_id('ijinjam', array('id' => $id), 'row');
        $kode = isset($k->status) ? $k->status : '';
        $pola = isset($k->pola) ? $k->pola : '';
        $value = $this->Main_Model->rule_approval($kode, $pola, $val);

        $data = array(
            'status' => $value,
            'user_update' => $user,
            'update_at' => $date
            );

        $this->Absensi_Model->update_ijinjam($id, $data);
        $this->Reminder_Model->reminder_keluar_kantor($value, $id);
    }

    function presensi($nip = '')
    {
        $this->Main_Model->get_login();
        $nip_quote = "'".$nip."'";
        $month = date('m');
        $month = str_replace('0', '', $month);

        $year = date('Y');
        $javascript = '
			<script type="text/javascript">
    			$(".date-picker").datepicker({
    				rtl: App.isRTL(),
    				orientation: "left",
    				autoclose: !0
				});

				'.$this->Main_Model->timepicker().'

				function reset(){
					document.getElementById("form_presensi").reset();
				}

				function load_table(){
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					var nip = {
						"nip" : '.$nip_quote.', 
						"tahun" : tahun, 
						"bulan" : bulan
					}
					$.ajax({
						url : "' . base_url('absensi/view_presensi_nip') . '",
						data : nip,
						type : "POST",
						beforeSend : function(){
							$("#tampil").addClass("hidden"), $("#tambah").addClass("hidden"), $("#imgload").removeClass("hidden");
						},	
						complete : function(){
							$("#tampil").removeClass("hidden"), $("#tambah").removeClass("hidden"), $("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			        			responsive: true,
			        			stateSave : true
			    			});
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal mengambil data!");
						} 
					});
				}

				function get_id(id){
					$("#save_button").attr("disabled", false);
					save_method = "update";
					id = {"id" : id}
					$.ajax({
			    		url : "' . base_url('absensi/presensi_id') . '",
			    		type : "GET",
			    		data : id,
			    		dataType : "json",
			    		success : function(dat){
			    			var jam_masuk = (dat.jam_masuk == "00/00/0000 00:00") ? "" : dat.jam_masuk;
            				var jam_pulang = (dat.jam_pulang == "00/00/0000 00:00") ? "" : dat.jam_pulang;
							$("#myModal").modal(), $("#nama").val(dat.nama), $("#id").val(dat.id), $("#nip").val(dat.nip), $("#tgl").val(dat.tanggal), $("#klarifikasi").val(dat.klarifikasi), $("#ket").val(dat.ket), $("#scan_masuk").val(jam_masuk), $("#scan_pulang").val(jam_pulang), $("#id_ab").val(dat.id_ab), $("#nip").val(dat.nip);
                            $("#nama").val(dat.nama), $("#kuota_cuti").html("");

                            (dat.sakit == 1) ? $("#absen").val("sakit") : ((dat.ijin == 1) ? $("#absen").val("ijin") : ((dat.mangkir == 1) ? $("#absen").val("mangkir") : $("#absen").val("")));
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			bootbox.alert("Gagal mengambil data!");
			    		}
			    	});
				}	

				$("#form_presensi").submit(function(event){
                    event.preventDefault();
                    var d = new FormData($(this)[0]);
                    $.ajax({
                        url : "'.base_url().'absensi/presensi_process",
                        type : "post",
                        data : d,
                        dataType : "json",
                        async : false,
                        cache : false,
                        contentType : false,
                        processData : false,
                        beforeSend : function() {
			                $("#save_button").attr("disabled", true);
			            },
			            complete : function() {
			                $("#save_button").attr("disabled", false);
			            },
                        success : function(data) {
                            if(data.status == true) {
                            	document.getElementById("form_presensi").reset();
                                $("#myModal").modal("toggle");
                                toastr.success("", data.message);
                            } else {
                            	toastr.warning("", data.message);
                            }
                            load_table();
                            // bootbox.alert(data.message);
                        }
                    });
                    return false;
                });

				function delete_data(id){
			    	id = {"id" : id}
			    	bootbox.dialog({
			    		message : "Yakin ingin menghapus data?",
			    		title : "Hapus Data",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('absensi/delete_presensi') . '",
							    		type : "POST",
							    		data : id,
							    		success : function(data){
							    			bootbox.alert({
												message: "Delete Success",
												size: "small"
											});
							    			load_table();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			bootbox.alert("Gagal menghapus data!");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})
			    }

			    $(document).ready(function(){

			    	var year = "'.$year.'";
			        var month = "'.$month.'";

			        $("#tahun").val(year).trigger("change");
			        $("#bulan").val(month).trigger("change");

					load_table();
					$(".select2").select2();

					$(".form_datetime").datetimepicker({
			            autoclose : !0,
			            format : "dd/mm/yyyy hh:ii",
			            pickerPosition : "bottom-left"
			        });
			    });

			    $("#tampil").click(function(){
			    	load_table();
			    });

			    // view kuota cuti
			    $("#absen").change(function(){
			        value = $(this).val();
			        if (value == "cuti") {
			            nip = $("#nip").val();
			            $.ajax({
			                url : "'.base_url('cuti/cek_qt').'/"+nip,
			                success : function(data) {
			                    $("#kuota_cuti").html(data);
			                }
			            });
			            $(".masuk").addClass("hidden");
			            $(".cuti-khusus").addClass("hidden");
			            $(".klarifikasi").removeClass("hidden");
			        } else if (value == "cuti_khusus") {
			            $(".masuk").addClass("hidden");
			            $("#kuota_cuti").html("");
			            $(".cuti-khusus").removeClass("hidden");
			            $(".klarifikasi").addClass("hidden");
			        } else if (value == "masuk") {
			            $(".masuk").removeClass("hidden");
			            $("#kuota_cuti").html("");
			            $(".cuti-khusus").addClass("hidden");
			            $(".klarifikasi").removeClass("hidden");
			        } else {
			            $(".masuk").addClass("hidden");
			            $("#kuota_cuti").html("");
			            $(".cuti-khusus").addClass("hidden");
			            $(".klarifikasi").removeClass("hidden");
			        }
			    });
			</script>';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );
        $tipe_cuti = $this->Cuti_Model->tipe_cuti();

        $arr_klarifikasi = [];
        $opt_klarifikasi = $this->Main_Model->view_by_id('ms_opt_klarifikasi', ['status' => 1], 'result');
        if (! empty($opt_klarifikasi)) {
            foreach ($opt_klarifikasi as $row) {
                $arr_klarifikasi[$row->tag] = $row->label;
            }
        }

        $data       = array(
            'tipe' => $tipe_cuti,
            'absen' => $arr_klarifikasi,
            'periode' => $this->Main_Model->periode_opt()
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/presensi_nip', $data);
        $this->load->view('template/footer', $footer);
    }

    function presensi_process()
    {
        $acctype = $this->session->userdata('acctype');
        ($acctype == 'Operator') ? $this->Main_Model->guest_login() : $this->Main_Model->get_login();
        $this->load->library('upload');
        $id = $this->input->post('id');
        $tgl = $this->input->post('tgl');
        $absen = $this->input->post('absen');
        $ket = $this->input->post('ket');
        $klarifikasi = $this->input->post('klarifikasi');
        $nip = $this->input->post('nip');
        $filename = 'FilePresensi'.date('Ymd');
        $cuti_khusus = $this->input->post('cuti_khusus');
        $awal_cuti_khusus = $this->input->post('awal_cuti_khusus');
        $akhir_cuti_khusus = $this->input->post('akhir_cuti_khusus');

        $awal_cuti_khusus = $this->Main_Model->convert_tgl($awal_cuti_khusus);
        $akhir_cuti_khusus = $this->Main_Model->convert_tgl($akhir_cuti_khusus);

        $scan_masuk = $this->input->post('scan_masuk');
        $scan_pulang = $this->input->post('scan_pulang');

        if ($absen == 'cuti_khusus') {
            $tipe_cuti = $this->Main_Model->view_by_id('tipe_cuti_khusus', array('id_tipe' => $cuti_khusus), 'row');
            $klarifikasi = isset($tipe_cuti->tipe) ? $tipe_cuti->tipe : '';
        }

        if ($scan_masuk != '') {
            $s_masuk = explode(' ', $scan_masuk);
            $scan_masuk = $this->Main_Model->convert_tgl($s_masuk[0]).' '.$s_masuk[1];
        } else {
            $scan_masuk = '0000-00-00 00:00';
        }

        if ($scan_pulang != '') {
            $s_pulang = explode(' ', $scan_pulang);
            $scan_pulang = $this->Main_Model->convert_tgl($s_pulang[0]).' '.$s_pulang[1];
        } else {
            $scan_pulang = '0000-00-00 00:00';
        }

        $id_ab = $this->input->post('id_ab');
        $th = date('Y');
        $cek_kuota  = $this->Cuti_Model->cek_kuotacuti($nip, $th);
        $tanggal = $this->Main_Model->convert_tgl($tgl);
        $id_cuti_dep = isset($cek_kuota->id_cuti) ? $cek_kuota->id_cuti : '';
        $kuota_cuti = isset($cek_kuota->qt) ? $cek_kuota->qt : 0;
        $check_cuti = $this->db->query("
    		SELECT COUNT(b.`tgl`) jml 
    		FROM cuti_det a 
    		JOIN cuti_sub_det b ON a.`id_cuti_det`=b.`id_cuti_det`  
    		WHERE DATE_FORMAT(b.`tgl`,'%m %Y') = DATE_FORMAT('$tanggal','%m %Y')
    		AND a.nip = '$nip'")->row();

        $um = $this->Api_Model->uang_makan($nip);
        $uang_makan = 0;
        if ($absen == 'masuk') {
            $check_holiday = $this->db->where('hol_tgl', $tanggal)->get('hol')->row();
            $uang_makan = isset($um->nominal) ? $um->nominal : 0;

            if (date('w', strtotime($tanggal)) == 0 || date('w', strtotime($tanggal)) == 6 || !empty($check_holiday)) {
                $uang_makan = 0;
            }
        } else {
            $uang_makan = 0;
        }

        $config = $this->Main_Model->set_upload_options("./assets/presensi/", "*", "0", $filename);
        $this->upload->initialize($config);
        $this->upload->do_upload('file_upload');
        $file = $this->upload->data();
        $file_name = isset($file['file_name']) ? $file['file_name'] : '';
        $file_ext = isset($file['file_ext']) ? $file['file_ext'] : '';

        # check is uploaded files
        $is_uploaded = false;
        $path = './assets/presensi/';
        $full_path = '';
        if ($file_name != '') {
            $full_path = $path.$file_name;
        }

        if (file_exists($full_path)) {
            $is_uploaded = true;
        }


        if ($klarifikasi == '' || $ket == '' || $absen == '') {
            $result = array('status' => false , 'message' => 'Form masih ada yang kosong!');
        } else {
            if ($absen == 'sakit' && $is_uploaded == false) {
                if ($file_ext == '') {
                    $result = array('status' => false, 'message' => 'File tidak ada / melebihi batas maksimal');
                } else {
                    $result = array('status' => false, 'message' => 'Wajib Melampirkan File ketika sakit');
                }
            } else {
                $c_exists = array('nip' => $nip, 'tgl' => $tanggal);
                $d_exists = $this->Main_Model->view_by_id('tb_klarifikasi_absensi', $c_exists, 'row');
                if (!empty($d_exists)) {
                    $result = array('status' => false , 'message' => 'Data ini sudah diverifikasi, silahkan refresh halaman anda!');
                } else {
                    if (strtotime($scan_masuk) > strtotime($scan_pulang) && $absen == 'masuk') {
                        $result = array('status' => false, 'message' => 'Invalid date format!');
                    } elseif ($absen == 'cuti' &&  $kuota_cuti == 0) {
                        $result = array('status' => false, 'message' => 'Kuota Cuti tidak cukup!');
                    } elseif ($absen == 'cuti' && $check_cuti->jml >= 2) {
                        $result = array('status' => false, 'message' => 'Cuti bulan ini sudah mencapai batas maksimum!');
                    } else {
                        $check_presensi = $this->Main_Model->view_by_id('presensi', array('id' => $id), 'row');
                        // update flag
                        $flag_update = array('flag' => 1);
                        $this->Main_Model->process_data('presensi', $flag_update, array('id' => $id));
                        // cari kode approval
                        $k = $this->Main_Model->kode_approval($nip, 'klarifikasi');
                        $approval = isset($k->kode) ? $k->kode : 12;

                        // if ($acctype == 'Administrator') $approval = '12';
                        $approval = '12';

                        if ($check_presensi) {
                            // approval disetujui
                            if ($approval == '12') {
                                $this->Main_Model->delete_data('log_presensi', array('id' => $id));
                                $log_presensi = array(
                                    'id_presensi' => $check_presensi->id,
                                    'nip' => $check_presensi->nip,
                                    'tgl' => $check_presensi->tgl,
                                    'sakit' => $check_presensi->sakit,
                                    'ijin' => $check_presensi->ijin,
                                    'mangkir' => $check_presensi->mangkir,
                                    'alpha' => $check_presensi->alpha,
                                    'ket' => $check_presensi->ket,
                                    'klarifikasi' => $check_presensi->klarifikasi,
                                    'file' => $check_presensi->file,
                                    'insert_at' => $check_presensi->insert_at,
                                    'update_at' => date('Y-m-d H:i:s'),
                                    'user' => $this->session->userdata('username'),
                                    'flag' => 1
                                    );
                                $this->Main_Model->process_data('log_presensi', $log_presensi);
                            }
                        }

                        if ($absen == 'cuti' || $absen == 'ijin_cuti') {
                            // approval disetujui
                            if ($approval == '12') {
                                $q  = $this->db->query("select max(id_cuti_det) id from cuti_det")->row();
                                $old_id = isset($q->id) ? $q->id : 0;
                                $new_id = $old_id + 1;

                                $id_tipe = ($absen == 'cuti') ? '1' : '2';

                                $data = array(
                                    'id_cuti_det' => $new_id,
                                    'nip' => $nip,
                                    'alasan_cuti' => $klarifikasi,
                                    'alamat_cuti' => '',
                                    'no_hp' => '',
                                    'id_tipe' => $id_tipe,
                                    'cuti_dep' => 0,
                                    'approval' => 12,
                                    'insert_at' => date('Y-m-d H:i:s'),
                                    'user_insert' => $this->session->userdata('username')
                                );

                                $data_tgl = array(
                                    'id_cuti_det' => $new_id,
                                    'tgl' => $tanggal,
                                );

                                $this->Cuti_Model->add_subdet_cuti($data_tgl);
                                // $this->Cuti_Model->update_qty($id_cuti_dep);
                                $this->Cuti_Model->add_cutibiasa($data);
                                $this->Main_Model->generate_cuti($th, $nip);
                            }
                        } elseif ($absen == 'cuti_khusus') {
                            $data = array(
                                'nip' => $nip,
                                'tgl_awal' => $awal_cuti_khusus,
                                'tgl_akhir' => $akhir_cuti_khusus,
                                'id_tipe_cuti' => $cuti_khusus,
                                'approval' => 12,
                                'insert_at' => date('Y-m-d H:i:s'),
                                'user_insert' => $this->session->userdata('username')
                            );

                            $this->Main_Model->process_data('cuti_khusus', $data);
                        } else {
                            // approval disetujui
                            if ($approval == '12') {
                                $data = array(
                                    'nip' => $nip,
                                    'tgl' => $tanggal,
                                    'ket' => $ket,
                                    'klarifikasi' => $klarifikasi,
                                    'update_at' => date('Y-m-d H:i:s'),
                                    'user' => $this->session->userdata('username'),
                                    'flag' => 1
                                );

                                $data['sakit'] = 0;
                                $data['ijin'] = 0;
                                $data['mangkir'] = 0;
                                $data['alpha'] = 0;

                                if ($absen == 'sakit') {
                                    $data['sakit'] = 1;
                                } elseif ($absen == 'ijin') {
                                    $data['ijin'] = 1;
                                } elseif ($absen == 'mangkir') {
                                    $data['mangkir'] = 1;
                                } else {
                                    $data['mangkir'] = 1;
                                }

                                ($id) ?  $this->Absensi_Model->update_presensi($data, $id) : $this->Absensi_Model->add_presensi($data);
                            }
                        }

                        $check_absensi = $this->Main_Model->view_by_id('tb_absensi', array('id_ab' => $id_ab), 'row');
                        if ($check_absensi) {
                            // approval disetujui
                            if ($approval == '12') {
                                $log_absensi = array(
                                    'id_ab' => $check_absensi->id_ab,
                                    'nip' => $check_absensi->nip,
                                    'tgl' => $check_absensi->tgl,
                                    'scan_masuk' => $check_absensi->scan_masuk,
                                    'scan_pulang' => $check_absensi->scan_pulang,
                                    'keterangan' => $check_absensi->keterangan,
                                    'insert_at' => $check_absensi->insert_at,
                                    'update_at' => date('Y-m-d H:i:s'),
                                    'status' => $check_absensi->status,
                                    'pin' => $check_absensi->pin,
                                    'uang_makan' => $check_absensi->uang_makan,
                                    'uang_denda' => $check_absensi->uang_denda,
                                    'flag' => 1,
                                    'user' => $this->session->userdata('username')
                                    );

                                $this->Main_Model->process_data('log_absensi', $log_absensi);
                            }

                            $data_absensi = array(
                                'scan_masuk' => $scan_masuk.':00',
                                'scan_pulang' => $scan_pulang.':00',
                                'keterangan' => $klarifikasi,
                                'uang_makan' => $uang_makan,
                                'uang_denda' => 0,
                                'status' => $ket,
                                'update_at' => date('Y-m-d H:i:s'),
                                'flag' => 1,
                                'user' => $this->session->userdata('username')
                            );

                            $data_klarifikasi = array(
                                'id_presensi' => $id,
                                'tgl' => $tanggal,
                                'presensi' => $absen,
                                'klarifikasi' => $klarifikasi,
                                'id_ab' => $id_ab,
                                'approval' => $approval,
                                'user' => $this->session->userdata('username'),
                                'nip' => $nip
                            );
                            
                            if ($is_uploaded == true) {
                                $data_absensi['file'] = $file_name;
                                $data_klarifikasi['file'] = $file_name;
                            }

                            $this->Main_Model->process_data('tb_klarifikasi_absensi', $data_klarifikasi);
                            $this->Main_Model->process_data('tb_absensi', $data_absensi, array('id_ab' => $id_ab));
                        }

                        if ($absen == 'masuk' || $absen == 'cuti' || $absen == 'cuti_khusus' || $absen == 'ijin_cuti') {
                            if ($approval == '12') {
                                if ($absen == 'cuti_khusus') {
                                    // hapus presensi between tanggal
                                    $this->db->query("
			    						DELETE FROM presensi 
			    						WHERE nip = '$nip' 
			    						AND tgl BETWEEN '$awal_cuti_khusus' AND '$akhir_cuti_khusus'");
                                } else {
                                    $this->Absensi_Model->delete_presensi($id);
                                }

                                if ($absen == 'cuti_khusus') {
                                    eksekusi_absen($awal_cuti_khusus, $akhir_cuti_khusus, $nip);
                                }
                            }
                        }

                        $result = array('status' => true , 'message' => 'Data berhasil disimpan!');
                    }
                }
            }
        }

        echo json_encode($result);
        $log = array(
            'request' => json_encode(['path' => $full_path, 'is_upload' => $is_uploaded]),
            'response' => json_encode($file)
        );
        $this->Rest_Model->create_log($log);
    }
    
    function add_presensi()
    {
        $absen = $this->input->post('absen');
        $tgl   = $this->Main_Model->convert_tgl($this->input->post('tgl'));
        if ($absen == "sakit") {
            $a = '1';
            $b = '';
            $c = '';
            $d = '';
        } elseif ($absen == "ijin") {
            $a = '';
            $b = '1';
            $c = '';
            $d = '';
        } elseif ($absen == "mangkir") {
            $a = '';
            $b = '';
            $c = '1';
            $d = '';
        } else {
            $a = '';
            $b = '';
            $c = '';
            $d = '1';
        }
        
        $data = array(
            'nip' => $this->input->post('nip'),
            'tgl' => $tgl,
            'sakit' => $a,
            'ijin' => $b,
            'mangkir' => $c,
            'alpha' => $d,
            'klarifikasi' => $this->input->post('klarifikasi'),
            'ket' => $this->input->post('ket')
        );
        $this->Absensi_Model->add_presensi($data);
    }
    
    function update_presensi()
    {
        $absen = $this->input->post('absen');
        $tgl   = $this->Main_Model->convert_tgl($this->input->post('tgl'));
        if ($absen == "sakit") {
            $a = '1';
            $b = '';
            $c = '';
            $d = '';
        } elseif ($absen == "ijin") {
            $a = '';
            $b = '1';
            $c = '';
            $d = '';
        } elseif ($absen == "mangkir") {
            $a = '';
            $b = '';
            $c = '1';
            $d = '';
        } else {
            $a = '';
            $b = '';
            $c = '';
            $d = '1';
        }
        
        $data = array(
            'nip' => $this->input->post('nip'),
            'tgl' => $tgl,
            'sakit' => $a,
            'ijin' => $b,
            'mangkir' => $c,
            'alpha' => $d,
            'klarifikasi' => $this->input->post('klarifikasi'),
            'ket' => $this->input->post('ket')
        );
        $id   = $this->input->post('id');
        $this->Absensi_Model->update_presensi($data, $id);
    }
    
    function presensi_id()
    {
        $id   = $this->input->get('id');
        $data = $this->Absensi_Model->presensi_id($id);
        
        echo json_encode($data);
    }

    function absensi_id()
    {
        $id = $this->input->get('id');
        $data = $this->Absensi_Model->absensi_id($id);

        echo json_encode($data);
    }
    
    function view_presensi_nip()
    {
        $this->Main_Model->get_login();
        $nip  = $this->input->post('nip');
        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $p = periode($tahun, $bulan);
        $periode = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Absensi_Model->view_presensi_nip($nip, $periode);
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Sakit/Ijin/Mangkir</th>
							<th>Keterangan</th>
							<th>Shift</th>
							<th>Scan</th>
							<th>Klarifikasi</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>';
        $i   = 1;
        foreach ($data as $row) {
            if ($row->sakit == 1) {
                $reason = "Sakit";
            } elseif ($row->ijin == 1) {
                $reason = "Ijin";
            } elseif ($row->mangkir == 1) {
                $reason = "Mangkir";
            } else {
                $reason = "Mangkir";
            }
            $action = '';
            ($row->file) ? $action .= '<li><a target="_blank" href="'.base_url('assets/presensi').'/'.$row->file.'"><i class="icon-delete"></i> Download </a>' : $action .= '';

            ($row->flag == 0) ? $action .= '<li> <a href="javascript:;" onclick="get_id(' . $row->id . ');"> <i class="icon-edit"></i> Klarifikasi </a> </li>' : $action .= '';

            $tbl .= '
			<tr>
				<td>'.$i++.'</td>
				<td>'.$this->Main_Model->format_tgl($row->tgl).'</td>
				<td>'.$reason.'</td>
				<td>'.$row->ket.'</td>
				<td>'.$row->shift.'</td>
				<td>'.$row->scan_masuk.' - '.$row->scan_pulang.'</td>
				<td>'.$row->klarifikasi.'</td>
				<td>
					<div class="btn-group">
			            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
			                <i class="fa fa-angle-down"></i>
			            </button>
			                <ul class="dropdown-menu" role="menu">
			                    '.$action.'
			                </ul>
			        </div>
				</td>
			</tr>';
        }
        
        $tbl .= '</tbody></table>';
        echo $tbl;
    }
    
    function delete_presensi()
    {
        $id = $this->input->post('id');
        $this->Absensi_Model->delete_presensi($id);
    }
    
    function view_absensi()
    {
        $this->Main_Model->get_login();
        $tgl_awal = $this->input->get('tgl_awal');
        $tgl_akhir = $this->input->get('tgl_akhir');
        $data = $this->Absensi_Model->view_absensi_nip($tgl_awal, $tgl_akhir);
        $tbl = '
			<table class="table table-striped table-bordered table-hover table-checkable order-column" id="absensi-tbl">
				<thead>
					<tr>
						<th>No</th>
						<th>Tanggal</th>
						<th>NIP</th>
						<th>Nama</th>
						<th>Cabang</th>
						<th>Hari<br>Kerja</th>
						<th>Masuk</th>
						<th>Sakit</th>
						<th>Ijin</th>
						<th>Mangkir</th>
						<th>Cuti</th>
						<th>Terlambat</th>
						<th>Lembur</th>
						<th>Jam<br>Lembur</th>
						<th>Uang<br>Makan</th>
						<th>Denda</th>
					</tr>
				</thead>
				<tbody>';
        $no  = 1;
        foreach ($data as $row) {
            $potong = $row->jml_sakit + $row->jml_mangkir + $row->jml_cuti + $row->jml_potong + $row->jml_resign;
            if ($row->jml_dayoff == 0) {
                $j_masuk = $row->jml_kerja - $potong;
                $hari_kerja = $row->jml_kerja;
            } else {
                $j_masuk = $row->hari - ($potong + $row->jml_dayoff);
                $hari_kerja = $row->hari;
            }

            $hari_kerja = ($hari_kerja - $row->jml_potong + $row->jml_resign);

            $tbl .= '
					<tr>
						<td>'.$no++.'</td>
						<td>'.$row->tgl_awal.' s/d '.$row->tgl_akhir.'</td>
						<td>'.$row->nip.'</td>
						<td>'.$row->nama.'</td>
						<td>'.$row->cabang.'</td>
						<td>'.$hari_kerja.'</td>
						<td>'.$j_masuk.'</td>
						<td>'.$row->jml_sakit.'</td>
						<td>'.($row->jumlah_ijin + $row->jumlah_pulang).'</td>
						<td>'.$row->jml_mangkir.'</td>
						<td>'.$row->jml_cuti.'</td>
						<td>'.$row->jumlah_telat.'</td>
						<td class="text-right">'.uang($row->jml_lembur).'</td>
						<td>'.$row->jmljam_lembur.'</td>
						<td class="text-right">'.uang($row->uang_makan).'</td>
						<td class="text-right">'.uang($row->uang_denda).'</td>
					</tr>';
        }
        
        $tbl .= '</tbody></table>';
        echo $tbl;
    }

    function absensi_periode()
    {
        $this->Main_Model->get_login();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2();

        $header = array(
            'menu' => $this->Main_Model->menu_admin('0', '0', '4'),
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
        );

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page()
        );

        $this->load->view('template/header', $header);
        $this->load->view('absensi/rekap_periode', $data);
    }

    function view_rekap_periode()
    {
        $this->Main_Model->get_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');

        $data = $this->Absensi_Model->view_rekap_periode($tahun, $bulan);
        $template = $this->Main_Model->tbl_temp('tb_rekap');
        $this->table->set_heading('No', 'Periode', 'NIP', 'Nama', 'Cabang', 'Hari', 'Hari Efektif', 'Masuk', 'Sakit', 'Mangkir', 'Cuti', 'Cuti Khusus');
        $no = 1;
        foreach ($data as $row) {
            if ($row->tgl_resign >= $row->tgl_awal_ab && $row->tgl_resign <= $row->tgl_akhir_ab) {
                $td_style = 'background-color:#ff6f69 !important; color:white;';
            } elseif ($row->tgl_masuk >= $row->tgl_awal_ab && $row->tgl_masuk <= $row->tgl_akhir_ab) {
                $td_style = 'background-color:#96ceb4 !important; color:white;';
            } else {
                $td_style = '';
            }

            //cuti khusus
            $cuti_khusus = $this->Gaji_Model->cuti_khusus($row->id_periode, $row->nip);

            //masuk
            $masuk = $row->hari - ($row->jml_potong + $row->jml_resign) - ($row->jml_sakit + $row->jml_mangkir + $row->jml_cuti + $cuti_khusus);

            $this->table->add_row(
                array( 'style' => $td_style, 'data' => $no++),
                array( 'style' => $td_style, 'data' => $row->periode),
                array( 'style' => $td_style, 'data' => $row->nip),
                array( 'style' => $td_style, 'data' => $row->nama),
                array( 'style' => $td_style, 'data' => $row->cabang),
                array( 'style' => $td_style, 'data' => $row->hari),
                array( 'style' => $td_style, 'data' => $row->jml_kerja),
                array( 'style' => $td_style, 'data' => $masuk),
                array( 'style' => $td_style, 'data' => $row->jml_sakit),
                array( 'style' => $td_style, 'data' => $row->jml_mangkir),
                array( 'style' => $td_style, 'data' => $row->jml_cuti),
                array( 'style' => $td_style, 'data' => $cuti_khusus)
            );
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function view_absensi_sps($qd_id)
    {
        $this->Main_Model->get_login();
        $data = $this->Absensi_Model->view_absensi_sps($qd_id);
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="absensi-sps">
				<thead>
					<tr>
						<th>No</th>
						<th>Bln-Th</th>
						<th>Nip</th>
						<th>Nama</th>
						<th>Lokasi</th>
						<th>Masuk Biasa</th>
						<th>Masuk Libur</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>';
        $no  = 1;
        foreach ($data as $row) {
            $tbl .= '<tr>
					<td>' . $no++ . '</td>
					<td>' . $row->bln_th . '</td>
					<td>' . $row->nip . '</td>
					<td>' . $row->nama . '</td>
					<td>' . $row->lku . '</td>
					<td>' . $row->j_m_biasa . '</td>
					<td>' . $row->j_m_libur . '</td>
					<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu">
				                    <li>
				                        <a href="javascript:;" onclick="get_id(' . $row->id_ab . ');">
				                            <i class="icon-edit"></i> Update </a>
				                    </li>
				                    <li>
				                        <a href="javascript:;" onclick="delete_data(' . $row->id_ab . ');">
				                            <i class="icon-delete"></i> Delete </a>
				                    </li>
				                </ul>
				        </div>
					</td>
				</tr>';
        }
        $tbl .= '</tbody></table>';
        
        echo $tbl;
    }
    
    function view_presensi()
    {
        $awal = $this->input->get('tgl_awal');
        $akhir = $this->input->get('tgl_akhir');

        $tgl_awal = $this->Main_Model->convert_tgl($awal);
        $tgl_akhir= $this->Main_Model->convert_tgl($akhir);

        $f_awal = $this->Main_Model->format_tgl($tgl_awal);
        $f_akhir = $this->Main_Model->format_tgl($tgl_akhir);

        $per = $f_awal.' s/d '.$f_akhir;

        $nip = $this->Absensi_Model->view_kary_presensi($tgl_awal, $tgl_akhir);
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="presensi-tbl">
				<thead>
					<tr>
						<th>No</th>
						<th>NIP</th>
						<th>Nama</th>
						<th>Tanggal</th>
						<th>Cabang</th>
						<th>Sakit</th>
						<th>Ijin</th>
						<th>Mangkir</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>';
        $no = 1;
        foreach ($nip as $row) {
            $c_lokasi = $this->Absensi_Model->cari_lokasi($row->nip);
            $lokasi = isset($c_lokasi->cabang) ? $c_lokasi->cabang : '';
            $tbl .= '<tr>
            		<td>'.$no++.'</td>
					<td>'.$row->nip.'</td>
					<td>'.$row->nama.'</td>
					<td>'.$per.'</td>
					<td>'.$lokasi.'</td>
					<td>'.$row->sakit.'</td>
					<td>'.$row->ijin.'</td>
					<td>'.$row->mangkir.'</td>
					<td>
						<a class="btn btn-primary btn-xs" href="'.base_url('absensi/presensi').'/'.$row->nip.'">Pilih</a>
					</td>
				</tr>';
        }
        $tbl .= '</tbody></table>';
        
        echo $tbl;
    }
    
    function absensi_nip($nip = '')
    {
        $this->Main_Model->get_login();
        $nip_quote = "'".$nip."'";
        $javascript = '
				<script>
					function load_table(){
						var nip = {
							"nip" : '.$nip_quote.'
						}
						$.ajax({
							url : "' . base_url('absensi/view_absensi_nip') . '",
							data : nip,
							type : "POST",
							beforeSend : function(){
								$("#imgload").removeClass("hidden");
								$("#myTable").html();
							},
							complete : function(){
								$("#imgload").addClass("hidden");
							},
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-example").DataTable({
				        			responsive: true
				    			});
							},
							error : function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Internal Server Error");
							} 
						});
					}
					load_table();
				</script>
					';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/absensi_nip');
        $this->load->view('template/footer', $footer);
    }
    
    function view_absensi_nip()
    {
        $this->Main_Model->get_login();
        $nip  = $this->input->post('nip');
        $data = $this->Absensi_Model->view_absensi_nip2($nip);
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>#</th>
							<th>Periode</th>
							<th>Hari Kerja</th>
			        		<th>Masuk</th>
			        		<th>Cuti</th>
			        		<th>Sakit</th>
			        		<th>Ijin</th>
			        		<th>Mangkir</th>
			        		<th>Lembur</th>
						</tr>
					</thead>
					<tbody>';
        $i = 1;
        foreach ($data as $row) {
            $potong = $row->jml_sakit + $row->jml_ijin + $row->jml_mangkir + $row->jml_cuti;
            $j_masuk = $row->jml_kerja - $potong;
            $hari_kerja = $row->jml_kerja;
            
            $tbl .= '
			<tr>
				<td>'.$i++.'</td>
				<td>'.$row->periode.'</td>
				<td>'.$hari_kerja.'</td>
				<td>'.$j_masuk.'</td>
				<td>'.$row->jml_cuti.'</td>
				<td>'.$row->jml_sakit.'</td>
				<td>'.$row->jml_ijin.'</td>
				<td>'.$row->jml_mangkir.'</td>
				<td>'.$row->jml_lembur.'</td>
			</tr>';
        }
        
        $tbl .= '</tbody></table>';
        echo $tbl;
    }

    function generate_cuti()
    {
        $year = date("Y");
        $this->Main_Model->generate_cuti($year);
    }

    function generate_cuti_lalu()
    {
        $year = date("Y");
        $year_lalu = $year-1;
        $this->Main_Model->generate_cuti($year_lalu);
    }
    
    function cuti($nip = '')
    {
        $this->Main_Model->get_login();
        $this->generate_cuti();
        $nip_quote = "'".$nip."'";
        // $this->generate_cuti_lalu();
        $javascript = '
				<script>
					function load_table(){
						var nip = {"nip":' . $nip_quote . '}
						$.ajax({
							url : "'.base_url('absensi/view_cuti_nip').'",
							data : nip,
							type : "POST",
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-example").DataTable({
				        			responsive: true
				    			});
							},
							error : function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Gagal mengambil data!");
							} 
						});
					}
					load_table();
				</script>
					';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/cuti_nip');
        $this->load->view('template/footer', $footer);
    }
    
    function view_cuti_nip()
    {
        // $this->Main_Model->get_login();
        $nip  = $this->input->post('nip');
        $data = $this->Absensi_Model->view_cuti_nip($nip);
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>No</th>
							<th>Tipe Cuti</th>
							<th>Alasan Cuti</th>
							<th>Tanggal</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>';
        $i   = 1;
        foreach ($data as $row) {
            $tbl .= '
			<tr>
				<td>' . $i++ . '</td>
				<td>'.$row->tipe.'</td>
				<td>' . $row->alasan_cuti . '</td><td>';
            $q  = $this->db->query("select * from cuti_sub_det where id_cuti_det = '$row->id_cuti_det' order by tgl asc")->result();
            foreach ($q as $q) {
                $tbl .= $this->Main_Model->format_tgl($q->tgl).'<br>';
            }
            $tbl .= '</td><td>' . $row->keterangan . '</td></tr>';
        }
        
        $tbl .= '</tbody></table>';
        echo $tbl;
    }
    
    function perdin()
    {
        $this->Main_Model->get_login();
        $menu = array(
            'menu6' => 'active open selected',
            'style' => '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">
				<link href="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css') . '" rel="stylesheet" type="text/css" />'
        );
        $javascript = '';
        $footer     = array(
            'javascript'    => $javascript,
            'js'            => $this->footer()
        );
        $data       = array(
            'nip'           => $this->Main_Model->all_kary_active(),
            'option'        => array('1' => 'Ya', '0' => 'Tidak' ),
            'js'            => $this->footer(),
            'nip'           => $this->Main_Model->kary_staff(),
            'periode'       => $this->Main_Model->periode_opt()
        );
        $this->load->view('template/header', $menu);
        $this->load->view('absensi/perdin', $data);
    }
    
    function view_perdin($qd_id)
    {
        $this->Main_Model->get_login();
        $data = $this->Absensi_Model->view_perdin($qd_id);
        $template = $this->Main_Model->tbl_temp();
        
        $this->table->set_heading(
            'No',
            'Tanggal Berangkat',
            'Tanggal Pulang',
            'Tujuan',
            'Keperluan',
            'Detail',
            'Action'
        );
        $no = 1;
        foreach ($data as $row) {
            $q = $this->db->query("SELECT * FROM perdin_det a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON c.`nip`=a.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON e.`id_pos`=d.`id_pos` WHERE a.`id_perdin`='$row->id_perdin' AND c.`aktif`='1'")->result();
            $tb = '<table width="100%">
        				<tr>
        					<th>NIP</th>
        					<th>Nama</th>
        					<th>Jabatan</th>
        					<th>Nominal</th>
        				</tr>';
            foreach ($q as $q) {
                // $l = $this->db->query("SELECT COUNT(b.`gend`='L') L FROM perdin_det a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON c.`nip`=a.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON e.`id_pos`=d.`id_pos` WHERE a.`id_perdin`='$row->id_perdin' AND c.`aktif`='1'")->row();
                // $p = $this->db->query("SELECT COUNT(b.`gend`='P') P FROM perdin_det a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON c.`nip`=a.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON e.`id_pos`=d.`id_pos` WHERE a.`id_perdin`='$row->id_perdin' AND c.`aktif`='1'")->row();
                // $max = $this->db->query("SELECT MAX(a.`akomodasi`) maks FROM perdin_det a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON c.`nip`=a.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON e.`id_pos`=d.`id_pos` WHERE a.`id_perdin`='$row->id_perdin' AND c.`aktif`='1'")->row();

                // if(count($q)==count($l) || count($q)==count($p))
                // {
                //  $akomodasi = $max->maks;
                // }
                // else
                // {
                    $akomodasi = $q->akomodasi;
                // }

                $nominal = $q->uang_saku+$q->transportasi+$akomodasi+$q->mitra;
                $tb .= '<tr>
        					<td>'.$q->nip.'</td>
        					<td>'.$q->nama.'</td>
        					<td>'.$q->jab.'</td>
        					<td>'.$this->Main_Model->uang($nominal).'</td>
        				</tr>';
            }
            $tb .='</table>';

            $this->table->add_row(
                $no++,
                $row->tgl_brgkt,
                $row->tgl_pulang,
                $row->tujuan,
                $row->keperluan,
                $tb,
                '<div class="btn-group">
		            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
		                <i class="fa fa-angle-down"></i>
		            </button>
		                <ul class="dropdown-menu" role="menu">
		                    <li>
		                        <a href="javascript:;" onclick="get_id(' . $row->id_perdin . ');">
		                            <i class="icon-edit"></i> Update </a>
		                    </li>
		                    <li>
		                        <a href="javascript:;" onclick="delete_data(' . $row->id_perdin . ');">
		                            <i class="icon-delete"></i> Delete </a>
		                    </li>
		                </ul>
		        </div>'
            );
        }
        
        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function add_perdin()
    {
        $this->Main_Model->get_login();

        $q = $this->db->query("SELECT MAX(a.`id_perdin`) maks FROM perdin a")->row();


        $id_perdin  = $q->maks + 1;
        $tgl_brgkt  = $this->input->post('tgl_berangkat');
        $tgl_pulang = $this->input->post('tgl_pulang');
        $tujuan     = $this->input->post('tujuan');
        $keperluan  = $this->input->post('keperluan');
        $ismitra    = $this->input->post('ismitra');
        $idp        = $this->session->userdata('idp');


        //array
        $nip        = $this->input->post('nip');
        $trans      = $this->input->post('trans');
        $akomo      = $this->input->post('akomo');
        $mitra      = 0;

        if ($tgl_brgkt == '' || $tgl_pulang == '' || $nip == '') {
            $status = 'false';
        } elseif (strtotime($tgl_brgkt)>strtotime($tgl_pulang)) {
            $status = 'invalid';
        } else {
            $status = 'true';
        
            $header = array(
            'id_perdin'     => $id_perdin,
            'tgl_brgkt'     => $this->Main_Model->convert_tgl($tgl_brgkt),
            'tgl_pulang'    => $this->Main_Model->convert_tgl($tgl_pulang),
            'tujuan'        => $tujuan,
            'keperluan'     => $keperluan,
            'ismitra'       => $ismitra,
            'idp'           => $idp
            );

            $tgl1   = explode('-', $this->Main_Model->convert_tgl($tgl_brgkt));
            $tgl2   = explode('-', $this->Main_Model->convert_tgl($tgl_pulang));
            $jd1    = GregorianToJD($tgl1[1], $tgl1[2], $tgl1[0]);
            $jd2    = GregorianToJD($tgl2[1], $tgl2[2], $tgl2[0]);

            $selisih = $jd2 - $jd1 + 1;

            for ($i=0; $i<count($nip); $i++) {
                $w = $this->db->query("SELECT * FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_perdin d ON c.`sal_pos`=d.`jab` WHERE a.`nip` = '$nip[$i]' AND a.`aktif` = '1' AND d.`tipe` = 'uangsaku'")->row();

                $e = $this->db->query("SELECT * FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_perdin d ON c.`sal_pos`=d.`jab` WHERE a.`nip` = '$nip[$i]' AND a.`aktif` = '1' AND d.`tipe` = 'akomodasi'")->row();

                // $r = $this->db->query("select * from kary where nip = '$nip[$i]'")->row();

                // $gend[] = $r->gend;

                if ($akomo[$i]>$e->nominal) {
                    $akomo[$i] = $e->nominal;
                }

                if ($ismitra=='1') {
                    $mitra = $e->nominal*10/100;
                } else {
                    $mitra = $mitra;
                }
            

                $detail = array(
                'id_perdin'         => $id_perdin,
                'nip'               => $nip[$i],
                'uang_saku'         => $w->nominal*$selisih,
                'transportasi'      => $trans[$i],
                'akomodasi'         => $akomo[$i],
                'mitra'             => $mitra,
                );
                $this->Absensi_Model->add_perdin_det($detail);

                // print_r($detail);
            }
            $this->Absensi_Model->add_perdin($header);
        }

        echo $status;
    }
    
    function perdin_id()
    {
        $this->Main_Model->get_login();
        $id   = $this->input->get('id');
        $data = $this->Absensi_Model->perdin_id($id);
        
        echo json_encode($data);
    }
    
    function update_perdin()
    {
        $this->Main_Model->get_login();
        $id_perdin  = $this->input->post('id');
        $tgl_brgkt  = $this->input->post('tgl_berangkat');
        $tgl_pulang = $this->input->post('tgl_pulang');
        $tujuan     = $this->input->post('tujuan');
        $keperluan  = $this->input->post('keperluan');
        $ismitra    = $this->input->post('ismitra');
        $idp        = $this->session->userdata('idp');


        //array
        $nip        = $this->input->post('nip');
        $trans      = $this->input->post('trans');
        $akomo      = $this->input->post('akomo');
        $mitra      = 0;
        $det_perdin = $this->input->post('id_det_perdin');

        if ($tgl_brgkt == '' || $tgl_pulang == '' || $nip == '') {
            $status = 'false';
        } elseif (strtotime($tgl_brgkt)>strtotime($tgl_pulang)) {
            $status = 'invalid';
        } else {
            $status = 'true';

            $header = array(
            'id_perdin'     => $id_perdin,
            'tgl_brgkt'     => $this->Main_Model->convert_tgl($tgl_brgkt),
            'tgl_pulang'    => $this->Main_Model->convert_tgl($tgl_pulang),
            'tujuan'        => $tujuan,
            'keperluan'     => $keperluan,
            'ismitra'       => $ismitra,
            'idp'           => $idp
            );

            $tgl1   = explode('-', $this->Main_Model->convert_tgl($tgl_brgkt));
            $tgl2   = explode('-', $this->Main_Model->convert_tgl($tgl_pulang));
            $jd1    = GregorianToJD($tgl1[1], $tgl1[2], $tgl1[0]);
            $jd2    = GregorianToJD($tgl2[1], $tgl2[2], $tgl2[0]);

            $selisih = $jd2 - $jd1 + 1;

            for ($i=0; $i<count($nip); $i++) {
                $w = $this->db->query("SELECT * FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_perdin d ON c.`sal_pos`=d.`jab` WHERE a.`nip` = '$nip[$i]' AND a.`aktif` = '1' AND d.`tipe` = 'uangsaku'")->row();

                $e = $this->db->query("SELECT * FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_perdin d ON c.`sal_pos`=d.`jab` WHERE a.`nip` = '$nip[$i]' AND a.`aktif` = '1' AND d.`tipe` = 'akomodasi'")->row();

                if ($akomo[$i]>$e->nominal) {
                    $akomo[$i] = $e->nominal;
                }

                if ($ismitra=='1') {
                    $mitra = $e->nominal*10/100;
                } else {
                    $mitra = $mitra;
                }
                

                $detail = array(
                    'id_perdin'     => $id_perdin,
                    'nip'           => $nip[$i],
                    'uang_saku'     => $w->nominal*$selisih,
                    'transportasi'  => $trans[$i],
                    'akomodasi'     => $akomo[$i],
                    'mitra'         => $mitra,
                    // 'id_det_perdin' => $det_perdin[$i]
                );
                if ($det_perdin[$i]=="baru") {
                    $this->Absensi_Model->add_perdin_det($detail);
                } else {
                    $this->Absensi_Model->update_perdin_det($detail, $det_perdin[$i]);
                }

                // print_r($detail);
            }
        // print_r($header);
                $this->Absensi_Model->update_perdin($header, $id_perdin);
        }

        echo $status;
    }
    
    function delete_perdin()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Absensi_Model->delete_perdin($id);
    }

    function delete_det_perdin($id)
    {
        $this->Main_Model->get_login();
        $this->Absensi_Model->delete_det_perdin($id);
    }
    
    function download_sps($id_per, $id_lwok)
    {
        $this->Main_Model->get_login();

        ini_set('display_errors', true);
        ini_set('display_startup_errors', true);

        define('EOL', (PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

        require_once(APPPATH.'libraries/PHPExcel.php');
        require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');
        $ambildata = $this->Absensi_Model->download_sps($id_per, $id_lwok);

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download SPS");
            $objPHPExcel->getProperties()->setSubject("Download SPS");
            $objPHPExcel->getProperties()->setDescription("List Karyawan SPS");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Absensi SPS'); //sheet title

            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:I2');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:I3');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:I4');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:I5');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:I6');

            $objset->setCellValue('A2', '** Sakit wajib melampirkan surat dari Puskesmas/Rumah Sakit/Dokter Spesialis');
            $objset->setCellValue('A3', '** Cuti wajib melampirkan form cuti yang sudah di ttd BM/GM');
            $objset->setCellValue('A4', '** Ijin tanpa surat sakit atau form cuti atau melebihi 6 jam dalam sebulan akan dipotongkan Cuti Tahunan secara Otomatis atau Potong Gaji apabila belum memiliki Cuti ');
            $objset->setCellValue('A5', '** Dilarang Merubah Format Excel / mengganti NIP, Apabila ada List Karyawan Yang kurang, Harap hubungi HRD');

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );


            //table header
            $cols = array("A","B","C","D","E","F","G","H","I");
             
            $val = array("NO ","NIP","PIN","NAMA","JABATAN","CABANG","MASUK BIASA","MASUK LIBUR","KETERANGAN");
             
        for ($a=0; $a<9; $a++) {
            $objset->setCellValue($cols[$a].'7', $val[$a]);
                 
            //Setting lebar cell
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
             
            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                )
            );
            $objPHPExcel->getActiveSheet()->getStyle($cols[$a].'1')->applyFromArray($style);
        }
             
            $baris  = 8;
            $i=1;
        foreach ($ambildata as $frow) {
            //pemanggilan sesuaikan dengan nama kolom tabel
            $objset->setCellValue("A".$baris, $i++);
            $objset->setCellValue("B".$baris, $frow->nip);
            $objset->setCellValue("C".$baris, $frow->pin);
            $objset->setCellValue("D".$baris, $frow->nama);
            $objset->setCellValue("E".$baris, $frow->jab);
            $objset->setCellValue("F".$baris, $frow->cabang);
            $objset->setCellValue("G".$baris, '');
            $objset->setCellValue("H".$baris, '');
            $objset->setCellValue("I".$baris, '');
                 
            //Set number value
            $objPHPExcel->getActiveSheet()->getStyle('C1:C'.$baris)->getNumberFormat()->setFormatCode('0');
                 
            $baris++;
        }
            $b=$baris-1;
            $objPHPExcel->getActiveSheet()->getStyle('A1:I'.$b)->applyFromArray($styleArray);
            
            $objPHPExcel->getActiveSheet()->setTitle('Absensi SPS');
 
            // $objPHPExcel->setActiveSheetIndex(0);
            // $filename = urlencode("Data".date("Y-m-d H:i:s").".xls");
               
            // header('Content-Type: application/vnd.ms-excel'); //mime type
            // header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
            // header('Cache-Control: max-age=0'); //no cache
            // header("Pragma: public");

            // Rename first worksheet
            // $objPHPExcel->getActiveSheet()->setTitle('LaporanAbsensi');

            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Absensi SPS.xls"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }

    function absensi_sps_id()
    {
        $this->Main_Model->get_login();
        $id = $this->input->get('id');
        $data = $this->Absensi_Model->absensi_sps_id($id);
        echo json_encode($data);
    }

    function update_absensi_sps()
    {
        $this->Main_Model->get_login();
        $data = array(
            'nip' => $this->input->post('nip'),
            'j_m_biasa' => $this->input->post('j_m_biasa'),
            'j_m_libur' => $this->input->post('j_m_libur')
            );
        $id_ab = $this->input->post('id_ab');
        $this->Absensi_Model->update_absensi_sps($data, $id_ab);
    }

    function delete_absensi_sps()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Absensi_Model->delete_absensi_sps($id);
    }

    function ijinjam_id()
    {
        $this->Main_Model->all_login();
        $id     = $this->input->get("id");
        $data   = $this->Absensi_Model->ijinjam_id($id);

        echo json_encode($data);
    }

    function delete_ijinjam()
    {
        $this->Main_Model->all_login();
        $id = $this->input->post('id');
        $this->Absensi_Model->delete_ijinjam($id);
    }

    function terlambat($id = '')
    {
        $this->Main_Model->get_login();
        $month = date('m');
        // $month = str_replace('0', '', $month);
        $month = (int)$month;
        $year = date('Y');

        $javascript = '
    			<script>
					var year = "'.$year.'";
					var month = "'.$month.'";
	
    				var save_method;
    				'.$this->Main_Model->default_select2().'
					function reset() {
						$("#warning").addClass("hidden");
						$("#success").addClass("hidden");
						$("#nip").val("").trigger("change");
						$("#periode").val("").trigger("change");
						$("#total_menit").val("");
						$("#total_hari").val("");
						save_method = "save";
					}

					$(document).ready(function(){
						$("#tahun").val(year).trigger("change");
	            		$("#bulan").val(month).trigger("change");
						load_table();
					});

					function load_table() {
						var dat = {
							"tahun" : $("#tahun").val(),
							"bulan" : $("#bulan").val()
						}
						$.ajax({
							url : "'.base_url('absensi/view_terlambat').'",
							type : "GET",
							data : dat,
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-example").DataTable({
	        						responsive: true
	    						});
								$("#warning").addClass("hidden");
								$("#success").removeClass("hidden");
							},
							beforeSend 	: function(){
								$("#tampil").addClass("hidden");
								$("#btn").addClass("hidden");
								$("#imgload").removeClass("hidden");
							},
							complete : function(){
								$("#tampil").removeClass("hidden");
								$("#btn").removeClass("hidden");
								$("#imgload").addClass("hidden");
							},
							error : function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Gagal mengambil data!");
							}
						});
					}

					$("#tampil").click(function(){
						load_table();
					});

					function save() {
						id 				= $("#id").val();
						nip 			= $("#nip").val();
						periode 		= $("#periode").val();
						total_menit 	= $("#total_menit").val();
						total_hari 		= $("#total_hari").val();

						if(nip=="" || periode=="" || total_menit=="" || total_hari=="")
						{
							'.$this->Main_Model->notif400().'
						}
						else
						{
							var dat = {
								"nip" 			: nip,
								"periode" 		: periode,
								"total_menit" 	: total_menit,
								"total_hari" 	: total_hari,
								"id" 			: id
							}

							(save_method=="save") ? url = "'.base_url('absensi/add_terlambat').'" : url = "'.base_url('absensi/update_terlambat').'";

							$.ajax({
	    						url 	: url,
	    						data 	: dat,
	    						type 	: "POST",
	    						success : function(data){
	    							'.$this->Main_Model->notif200().'
	    							load_table();
	    						},	
	    						error 	: function(jqXHR,textStatus,errorThrown){
	    							bootbox.alert("Internal Server Error");
	    						}
	    					});
						}
					}
					function get_id(id)
					{
						save_method = "update";
				    	$("#warning").addClass("hidden");
				    	$("#success").addClass("hidden");

				    	id = {
				    		"id" : id
				    	}
				    	$.ajax({
				    		url 		: "' . base_url('absensi/terlambat_id') . '",
				    		type 		: "GET",
				    		data 		: id,
				    		dataType 	: "JSON",
				    		success 	: function(data){
				    			$("#myModal").modal();
				    			$("#nip").val(data.nip).trigger("change");
								$("#periode").val(data.id_per).trigger("change");
								$("#total_menit").val(data.total_menit);
								$("#total_hari").val(data.total_hari);
								$("#id").val(data.id_telat);
				    		},
				    		error 		: function(jqXHR, textStatus, errorThrown){
				    			bootbox.alert("Internal Server Error");
				    		}
				    	});
				    }
				    function delete_data(id) {
				    	id = {
				    		"id"	:	id
				    	}

				    	bootbox.dialog({
				    		message 		: "Yakin ingin menghapus data?",
				    		title 			: "Hapus Data",
				    		buttons 		: {
				    			danger 			: {
				    				label 		: "Delete",
				    				className 	: "red",
				    				callback 	: function(){
				    					$.ajax({
								    		url 	: "' . base_url('absensi/delete_terlambat') . '",
								    		type 	: "POST",
								    		data 	: id,
								    		success : function(data){
								    			bootbox.alert({
													message 	: "Delete Success",
													size 		: "small"
												});
								    			load_table();
								    		},
								    		error 	: function(jqXHR, textStatus, errorThrown){
								    			alert("Internal Server Error");
								    		}
								    	});
				    				}
				    			},
				    			main 		: {
				    				label 		: "Cancel",
				    				className 	: "blue",
				    				callback 	: function(){
				    					return true;
				    				}
				    			}
				    		}
				    	})
				    }
					load_table();

					function show(nip, periode) {
						$.ajax({
							url : "'.base_url('absensi/show_detail_telat').'/"+nip+"/"+periode,
							success : function(data) {
								$("#modaldetail").modal();
								$("#detailtitle").html("Detail Keterlambatan Periode "+ $("#qd_id option:selected").text());
								$("#detailresult").html(data);
								'.$this->Main_Model->default_datatable('table_detail').'
							}
						})
					}

					function download(nip, periode) {
						window.location.href="'.base_url('download_excel/detail_terlambat').'/"+nip+"/"+periode;
					}
    			</script>
    	';
        // $footer = array(
        //  'js' => $this->footer(),
        //  'javascript' => $javascript
        //  );
        $data = array(
            'nip'           => $this->Main_Model->all_kary_active(),
            'periode'       => $this->Main_Model->periode_opt(),
            'success'       => $id,
            'js'            => $this->footer(),
            'javascript'    => $javascript
            );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '4')
        );
        $this->load->view('template/header', $header);
        $this->load->view('absensi/terlambat', $data);
        // $this->load->view('template/footer', $footer);
    }

    function view_terlambat()
    {
        $this->Main_Model->get_login();
        $tahun = $this->input->get('tahun');
        $bulan = $this->input->get('bulan');
        $p = periode($tahun, $bulan);
        $id_per = isset($p->qd_id) ? $p->qd_id : '';
        $data = $this->Absensi_Model->view_terlambat($id_per);
        $period = $this->Absensi_Model->view_periode($id_per);

        $template = array(
                'table_open'            => '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">',

                'thead_open'            => '<thead>',
                'thead_close'           => '</thead>',

                'heading_row_start'     => '<tr>',
                'heading_row_end'       => '</tr>',
                'heading_cell_start'    => '<th>',
                'heading_cell_end'      => '</th>',

                'tbody_open'            => '<tbody>',
                'tbody_close'           => '</tbody>',

                'row_start'             => '<tr>',
                'row_end'               => '</tr>',
                'cell_start'            => '<td>',
                'cell_end'              => '</td>',

                'row_alt_start'         => '<tr>',
                'row_alt_end'           => '</tr>',
                'cell_alt_start'        => '<td>',
                'cell_alt_end'          => '</td>',

                'table_close'           => '</table>'
        );

        $this->table->set_heading('No', 'NIP', 'Nama', 'Cabang', 'Periode', 'Total Hari', 'Total Jam', 'Total Menit', 'Action');
        $no=1;
        foreach ($data as $row) {
            $c_lokasi   = $this->Absensi_Model->cari_lokasi($row->nip);
            $lokasi     = isset($c_lokasi->cabang) ? $c_lokasi->cabang : '';
            $quotation = "'".$row->nip."', '".$id_per."'";
            $this->table->add_row(
                $no++,
                $row->nip,
                isset($row->nama)?$row->nama:'',
                $lokasi,
                isset($period->per)?$period->per:'',
                $row->total_hari,
                round($row->total_menit / 60, 2),
                $row->total_menit,
                '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="javascript:;" onclick="show(' . $quotation . ');">
                                <i class="icon-edit"></i> Lihat Detail </a>
                        </li>
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function show_detail_telat($nip = '', $periode = '')
    {
        $acctype = $this->session->userdata('acctype');
        ($acctype == 'Operator') ? $this->Main_Model->guest_login() : $this->Main_Model->get_login();
        $q = "'".$nip."','".$periode."'";
        $button = '<div class="form-group"> <button type="button" class="btn btn-default" id="download" onclick="download('.$q.')"><i class="fa fa-download"></i> Download</button> </div>';
        $data = $this->Absensi_Model->detail_terlambat($nip, $periode);

        $template = $this->Main_Model->tbl_temp('table_detail');
        $this->table->set_heading('No', 'NIP', 'Nama', 'Tanggal', 'Total Menit', 'Scan Masuk', 'Shift', 'Jam Masuk', 'Klarifikasi');
        $no = 1;
        foreach ($data as $row) {
            $scan_absen = $row->scan_absen;
            if ($scan_absen != '' && $row->klarifikasi != '') {
                $scan_absen = ' ('.$scan_absen.')';
            } else {
                    $scan_absen = '';
            }

            $action = array(
                array('label' => 'Update', 'event' => "klarifikasi('".$row->id."')")
            );

            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $this->Main_Model->format_tgl($row->tgl),
                $row->total_menit,
                $row->scan_masuk.$scan_absen,
                $row->shift,
                $row->jam_masuk,
                $row->klarifikasi
                // $this->Main_Model->action($action)
            );
        }
        $this->table->set_template($template);
        echo $button.$this->table->generate();
    }

    function add_terlambat()
    {
        $this->Main_Model->get_login();
        $data = array(
            'nip'           => $this->input->post('nip'),
            'id_per'        => $this->input->post('periode'),
            'total_menit'   => $this->input->post('total_menit'),
            'total_hari'    => $this->input->post('total_hari')
            );
        $this->Absensi_Model->add_terlambat($data);
    }

    function terlambat_id()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->get("id");
        $data   = $this->Absensi_Model->terlambat_id($id);
        echo json_encode($data);
    }

    function update_terlambat()
    {
        $this->Main_Model->get_login();
        $data   = array(
            'nip'           => $this->input->post('nip'),
            'id_per'        => $this->input->post('periode'),
            'total_menit'   => $this->input->post('total_menit'),
            'total_hari'    => $this->input->post('total_hari')
            );
        $id     =   $this->input->post('id');
        $this->Absensi_Model->update_terlambat($data, $id);
    }

    function delete_terlambat()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Absensi_Model->delete_terlambat($id);
    }

    function process_cuti()
    {
        $this->Main_Model->get_login();
        $id             = $this->input->post('id');
        $q              = $this->db->query("select * from cuti_det order by id_cuti_det desc")->row();
        $old_id         = $q->id_cuti_det;
        $new_id         = $old_id+1;
        $th             = date("Y");
        $nip            = $this->input->post('nip');
        $cek_kuota      = $this->Cuti_Model->cek_kuotacuti($nip, $th);
        $id_cuti_dep    = $cek_kuota->id_cuti;

        if ($id!="") {
            $this->Absensi_Model->delete_ijinjam($id);
        }

        $data = array(
            'id_cuti_det'   => $new_id,
            'nip'           => $nip,
            'alasan_cuti'   => $this->input->post('ket'),
            'cuti_dep'      => $id_cuti_dep
            );
        $this->Cuti_Model->add_cutibiasa($data);
        $this->Cuti_Model->update_qty($id_cuti_dep);
        $data_tgl = array(
            'id_cuti_det'   => $new_id,
            'tgl'           => $this->Main_Model->convert_tgl($this->input->post('tgl'))
            );
        $this->Cuti_Model->add_subdet_cuti($data_tgl);
    }

    function detail_ijinjam()
    {
        $this->Main_Model->all_login();
        $cabang = $this->Main_Model->session_cabang();
        $js = $this->Main_Model->js_datatable()
              .$this->Main_Model->js_modal()
              .$this->Main_Model->js_bootbox()
              .$this->Main_Model->js_select2()
              .$this->Main_Model->js_timepicker()
              .$this->Main_Model->js_datepicker();

        $menu = (empty($cabang)) ? $this->Main_Model->menu_admin('0', '0', '4') : $this->Main_Model->menu_user('0', '0', '72');
        $header = array(
            'menu' => $menu,
            'style' => $this->Main_Model->style_datatable()
                      .$this->Main_Model->style_modal()
                      .$this->Main_Model->style_select2()
                      .$this->Main_Model->style_timepicker()
                      .$this->Main_Model->style_datepicker()
            );

        $nip = (empty($cabang)) ? $this->Main_Model->all_kary_active() : $this->Main_Model->kary_cabang($cabang, null, 'hide_jab');
        $bulan = array(
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December'
            );

        $t = $this->db->query("SELECT DATE_FORMAT(a.`tgl`,'%Y') th FROM ijinjam a GROUP BY DATE_FORMAT(a.`tgl`,'%Y')")->result();
        $th = array();
        foreach ($t as $r) {
            $th[$r->th] = $r->th;
        }

        $data = array(
            'footer' => $this->Main_Model->footer($js),
            'penutup' => $this->Main_Model->close_page(),
            'nip' => $nip,
            'bulan' => $bulan,
            'th' => $th
            );
        $template = (empty($cabang)) ? 'template/header' : 'akun/header';
        $this->load->view($template, $header);
        $this->load->view('absensi/detail_ijinjam', $data);
    }

    function view_detail_ijinjam()
    {
        $this->Main_Model->all_login();
        $nip = $this->input->post('nip');
        $bulan = $this->input->post('bulan');
        $th = $this->input->post('th');
        $data = $this->Absensi_Model->view_detail_ijinjam($nip, $bulan, $th);
        $template = $this->Main_Model->tbl_temp('tb_ijinjam');
        $this->table->set_heading('No', 'Nama', 'Tanggal', 'Jumlah Menit', 'Keterangan', 'Status');
        $no = 1;
        foreach ($data as $row) {
            switch ($row->status) {
                case '1':
                    $status = 'Disetujui';
                    break;
                case '2':
                    $status = 'Ditolak';
                    break;
                default:
                    $status = 'Proses';
                    break;
            }

            $this->table->add_row(
                $no++,
                $row->nama,
                $this->Main_Model->format_tgl($row->tgl),
                $row->jmljam,
                $row->keterangan,
                $row->ket
            );
        }
        $this->table->set_template($template);
        echo $this->table->generate();
    }
}
