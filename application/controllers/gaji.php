<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gaji extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Karyawan_Model', '', TRUE);
		$this->load->model('Penggajian_Model', '', TRUE);
		$this->load->model('Sto_Model', '', TRUE);
		$this->load->model('Gaji_Model', '', TRUE);
	}

	function header()
	{
		$menu = '
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/datatables.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2.min.css').'">
				<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/select2/css/select2-bootstrap.min.css').'">';
		return $menu;
	}

	function footer()
	{
		$footer = '
				<script src="'.base_url('assets/plugins/datatables/datatables.min.js').'"></script>
				<script src="'.base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js').'"></script>
				<script src="'.base_url('assets/plugins/bootbox/bootbox.min.js').'"></script>
				<script src="'.base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'"></script>
				<script src="'.base_url('assets/plugins/select2/js/select2.full.min.js').'"></script>';

		return $footer;
	}

	function prosesgaji()
	{
		$this->Main_Model->all_login();
		$javascript 	= '
			<script type="text/javascript">
    			$(document).ready(function(){
    				$(".select2").select2();
    			});

				// function select_lokasi(grouplokasi) {
				// 	dat = {"grouplokasi" : grouplokasi}
				// 	$.ajax({
				// 		url 	: "'.base_url('gaji/grouplokasi').'",
				// 		data 	: dat,
				// 		type 	: "GET",
				// 		success : function(data){
				// 			$("#target").html(data);
				// 			// view_gaji_after_submit($("#periode").val(), $("#grouplokasi").val(), $("#lokasi").val());
				// 		}
				// 	});
				// }

				function proses() {
					var dat = {
						"tahun" : $("#tahun").val(),
						"bulan" : $("#bulan").val(),
						"fk" : $("#fk").val(),
						"cabang" : $("#cabang").val()
					}

					$.ajax({
						url : "'.base_url('gaji/proses_gaji').'",
						type : "POST",
						data : dat,
						dataType : "JSON",
						beforeSend : function(){
							$("#proses").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function(){
							$("#proses").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							bootbox.dialog({
		                        message : data.message,
		                        buttons :{
		                            main : {
		                                label : "OK",
		                                className : "blue",
		                                callback : function(){
		                                    return true;
		                                }
		                            }
		                        }
		                    })
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal Proses Gaji!");
						}
					});
				}

				// select_lokasi($("#grouplokasi").val());

			</script>';
		$header = array(
			'menu' => $this->Main_Model->menu_user('0','0','144'),
			'style' => $this->header()
		);

		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);

		$sess_cabang = $this->Main_Model->session_cabang();
		$cabang_create = array();
		$cabang_create[] = 'Semua Cabang';
		if(!empty($sess_cabang)) {
			foreach($sess_cabang as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang_create[$d->id_cab] = $d->cabang;
			}
		} else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang_create[$row->id_cab] = $row->cabang;
			}
		}
		

		$data = array(
			'periode' => $this->Main_Model->periode_opt(),
			'fk_y' => $this->Main_Model->th_fk(),
			'cuti_y' => $this->Main_Model->cuti_y(),
			'cabang' => $cabang_create
			);

		$this->load->view('template/header',$header);
		$this->load->view('gaji/prosesgaji',$data);
		$this->load->view('template/footer',$footer);
	}

	function proses_gaji()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');
		$fk = $this->input->post('fk');
		$cabang = $this->input->post('cabang');
		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';
		$tutup = isset($p->tutup) ? $p->tutup : '';

		if ($tutup == 0) {
			$this->Main_Model->generate_cuti();
			$data = $this->Gaji_Model->generate_gaji($periode, $cabang, $fk);

			foreach($data as $row) {
				$cuti_khusus = $this->Gaji_Model->cuti_khusus($periode, $row->nip);
				$tunj_penghargaan = $this->Gaji_Model->tunj_penghargaan($row->tgl_masuk);

				$hari_pengh_kerja = 0;
				if ($tunj_penghargaan == TRUE) {
					$hari_pengh_kerja = ($row->hari - ($row->jml_potong  + $row->jml_resign)) - ($row->jml_mangkir + $row->jml_dayoff);

					if ($row->jml_potong == 0 && $row->jml_resign == 0) {
						$hari_pengh_kerja = 30;
					} else {
						$hari_pengh_kerja = $hari_pengh_kerja;
					}
				}

				$kemahalan = 0;
				$tunj_kemahalan = $this->Main_Model->view_by_id('tb_tunj_kemahalan', array('nip' => $row->nip), 'row');
				if (!empty($tunj_kemahalan)) {
					$kemahalan = 1;
				}

				$ins_presensi = 1;
				if ($row->jml_mangkir > 0) {
					$ins_presensi = 0;
				}

				if ($row->jml_potong > 0 || $row->jml_resign > 0) {
					$ins_presensi = 0;
				}

				$hari_upah_pokok = 0;
				if ($row->jml_potong == 0 && $row->jml_resign == 0) {
					$hari_upah_pokok = 30;
				} else {
					$hari_upah_pokok = ($row->hari - $row->jml_dayoff) - ($row->jml_potong  + $row->jml_resign);
				}	

				$hari_tunj_jabatan = 0;
				if ($row->jml_potong == 0 && $row->jml_resign == 0) {
					$hari_tunj_jabatan = 30;
				} else {
					$hari_tunj_jabatan = ($row->hari - ($row->jml_potong  + $row->jml_resign)) - ($row->jml_mangkir + $row->jml_dayoff);
				}

				$jml_kerja = $row->jml_kerja;
				if ($row->jml_potong + $row->jml_resign > 0) {
					$jml_kerja = $row->hari - ($row->jml_potong + $row->jml_resign);
				}

				$hari_tidak_tetap = $jml_kerja - ($row->jml_cuti + $cuti_khusus + $row->jml_sakit + $row->jml_mangkir + $row->jml_dayoff);
				
				// status karyawan
				$status = $row->kary_stat;
				$jab = $row->jab;
				// setup gaji
				$set = $this->Main_Model->view_by_id('tb_setup_gaji', array('jab' => $jab, 'category' => $status), 'row');
				//nominal gaji
				$mangkir = $row->jml_mangkir;
				$upahPokok = isset($set->upah_pokok) ? $set->upah_pokok : 0;
				$tunjJabatan = isset($set->tunj_jabatan) ? $set->tunj_jabatan : 0;
				$pmasaKerja = isset($set->pmasa_kerja) ? $set->pmasa_kerja : 0;
				$tunjTdkTetap = isset($set->tunj_tdktetap) ? $set->tunj_tdktetap : 0;
				$tunjKemahalan = isset($set->tunj_kemahalan) ? $set->tunj_kemahalan : 0;
				$insPresensi = isset($set->ins_presensi) ? $set->ins_presensi : 0;

				// $val_pokok = round(($upahPokok / 30) * $hari_upah_pokok);
				// $val_masakerja = round(($pmasaKerja / 30) * $hari_pengh_kerja);
				// $val_tdktetap = round(($tunjTdkTetap / 30) * $hari_tidak_tetap);

				if ($upahPokok > 0) {
					$val_pokok = round($upahPokok - (($upahPokok / $hari_upah_pokok) * $mangkir));
				} else {
					$val_pokok = 0;
				}

				if ($pmasaKerja > 0) {
					$val_masakerja = round($pmasaKerja - (($pmasaKerja / $hari_pengh_kerja) * $mangkir));
				} else {
					$val_masakerja = 0;
				}

				if ($tunjTdkTetap > 0) {
					$val_tdktetap = round($tunjTdkTetap - (($tunjTdkTetap / $hari_tidak_tetap) * $mangkir));
				} else {
					$val_tdktetap = 0;
				}

				$val_kemahalan = round($tunjKemahalan * $kemahalan);
				$val_presensi = round($insPresensi * $ins_presensi);

				$ins = array(
					'nip' => $row->nip,
					'id_sk' => $row->id_sk,
					'id_periode' => $row->id_periode,
					'hari' => $row->hari,
					'hari_kerja' => $jml_kerja,
					'hari_upah_pokok' => $hari_upah_pokok,
					'hari_tunj_jabatan' => $hari_tunj_jabatan,
					'hari_pengh_kerja' => $hari_pengh_kerja,
					'hari_tidak_tetap' => $hari_tidak_tetap,
					'tunj_kemahalan' => $kemahalan,
					'ins_presensi' => $ins_presensi,
					'telat' => $row->jml_telat,
					'mangkir' => $row->jml_mangkir,
					'cuti' => $row->jml_cuti,
					'sakit' => $row->jml_sakit,
					'ijin' => $row->jml_ijin,
					'sisa_cuti' => $row->sisa_cuti,
					'cuti_khusus' => $cuti_khusus,
					'val_pokok' => $val_pokok,
					'val_masakerja' => $val_masakerja,
					'val_tdktetap' => $val_tdktetap,
					'val_kemahalan' => $val_kemahalan,
					'val_presensi' => $val_presensi,
					'pot_masuk' => $row->jml_potong,
					'pot_resign' => $row->jml_resign,
					'insert_at' => now(),
					'user_insert' => $this->session->userdata('username')
				);

				$condition = array('nip' => $row->nip, 'id_periode' => $row->id_periode);
				$exists = $this->Main_Model->view_by_id('temp_payroll', $condition, 'row');
				if (!empty($exists)) {
					$this->Main_Model->delete_data('temp_payroll', $condition);
				}

				$this->Main_Model->process_data('temp_payroll', $ins);
			}

			$res = array('message' => 'Proses Payroll Berhasil');
		} else {
			$res = array('message' => 'Proses Payroll Gagal, Periode sudah ditutup');
		}
		echo json_encode($res);
	}

	function kemahalan()
	{
		$this->Main_Model->all_login();
		$javascript = '
			<script>
				function load_table() {
					$.ajax({
						url : "'.base_url('gaji/view_kemahalan').'",
						success : function(data) {
							$("#myTable").html(data);
							$("#tb_kemahalan").DataTable({
								responsive : true
							});
						}
					});
				}

				$(document).ready(function(){
					load_table();
					$(".select2").select2();
				});

				function clear_form() {
					$("#nip").val("").trigger("change");
				}

				$("#add_new").click(function(){
					clear_form();
				});

				$("#save").click(function(){
					$.ajax({
						url : "'.base_url('gaji/kemahalan_process').'",
						data : $("#fkemahalan").serialize(),
						type : "post",
						dataType : "json",
						success : function(data) {
							if (data.status == true) {
								load_table();
								clear_form();
							}
							bootbox.alert(data.message);
						}
					});
				});

				function delete_data(id) {
			        bootbox.dialog({
			            message : "Yakin ingin menghapus data?",
			            title : "Hapus Data",
			            buttons :{
			                danger : {
			                    label : "Delete",
			                    className : "red",
			                    callback : function(){
			                        $.ajax({
			                            url : "'.base_url('gaji/delete_kemahalan').'/"+id,
			                            type : "POST",
			                            success : function(data){
			                                bootbox.alert({
			                                    message: "Delete Success",
			                                    size: "small"
			                                });
			                                load_table();
			                            }
			                        });
			                    }
			                },
			                main : {
			                    label : "Cancel",
			                    className : "blue",
			                    callback : function(){
			                        return true;
			                    }
			                }
			            }
			        })
			    }
			</script>
		';

		$header = array(
			'menu' => $this->Main_Model->menu_user('0','0','144'),
			'style' => $this->header()
		);

		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);

		$data = array(
			'nip' => $this->Main_Model->kary_cabang()
		);

		$this->load->view('template/header',$header);
		$this->load->view('gaji/kemahalan',$data);
		$this->load->view('template/footer',$footer);
	}

	function view_kemahalan()
	{
		$this->Main_Model->all_login();
		$data = $this->Gaji_Model->view_kemahalan();
		$template = $this->Main_Model->tbl_temp('tb_kemahalan');
		$array_heading = array('No', 'NIP', 'Nama', 'Cabang', 'Divisi', 'Posisi', 'Action');
		$this->table->set_heading($array_heading);
		$no = 1;
		foreach($data as $row) {
			$delete = array('event' => "delete_data('".$row->id."')", 'label' => 'Delete');
			$action = array($delete);
			$this->table->add_row(
				$no++,
				$row->nip,
				$row->nama,
				$row->cabang,
				$row->divisi,
				$row->jab, 
				$this->Main_Model->action($action)
			);
		}
		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function kemahalan_process()
	{
		$this->Main_Model->all_login();
		$nip = $this->input->post('nip');
		if ($nip == '') {
			$status = FALSE;
			$message = 'Field Nama Karyawan masih kosong!';
		} else {
			$exists = $this->Main_Model->view_by_id('tb_tunj_kemahalan', array('nip' => $nip), 'row');
			if (!empty($exists)) {
				$status = FALSE;
				$message = 'Karyawan sudah terdaftar!';
			} else {
				$data = array(
					'nip' => $nip,
					'insert_at' => now(),
					'user_insert' => $this->session->userdata('username')
				);

				$simpan = $this->Main_Model->process_data('tb_tunj_kemahalan', $data);
				if ($simpan > 0) {
					$status = TRUE;
					$message = 'Success!';
				} else {
					$simpan = FALSE;
					$message = 'Gagal Menyimpan data!';
				}
			}
		}

		echo json_encode(array('status' => $status, 'message' => $message));
	}

	function delete_kemahalan($id='')
	{
		$this->Main_Model->all_login();
		$this->Main_Model->delete_data('tb_tunj_kemahalan', array('id' => $id));
	}

	function proses_gaji_old()
	{
		$this->Main_Model->all_login();
		$periode 	= $this->input->post('periode');// periode
		$fk 		= $this->input->post('fk');// faktor kemahalan / umk
		$grouplokasi= $this->input->post('grouplokasi');// grouplokasi
		$lokasi 	= $this->input->post('lokasi');// id_lokasi
		$q_id 		= $this->db->query("select * from q_det where qd_id='$periode'")->row();// cari tanggal periode
		$tgl_awal 	= isset($q_id->tgl_awal)?$q_id->tgl_awal:'';// tgl awal periode
		$tgl_akhir	= isset($q_id->tgl_akhir)?$q_id->tgl_akhir:'';// tgl akhir periode
		$idp 		= $this->session->userdata('idp');// id perusahaan

		// cari karyawan aktif dan karyawan resign di periode ini berdasarkan lokasi
		$kary 		= $this->Gaji_Model->gaji_all_kary($tgl_awal, $tgl_akhir, $grouplokasi, $lokasi); 

		foreach ($kary as $row) {
		// hapus jika ada data yang sama
		$this->Gaji_Model->delete_p_temp($row->nip,$periode);
		// menghitung absen
		$j 			= $this->db->query("SELECT * FROM ab a WHERE a.`id_per` = '$periode' AND a.`nip` = '$row->nip'")->row();
		// menghitung cuti
		$k 	  		= $this->db->query("SELECT COUNT(a.`id`) jml FROM cuti_sub_det a JOIN cuti_det b ON a.`id_cuti_det`=b.`id_cuti_det`WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND b.`nip` = '$row->nip'")->row();
        // jumlah cuti
        $cuti 		= isset($k->jml) ? $k->jml : '0';

        $j_hr_kerja	= isset($j->j_hr_kerja)?$j->j_hr_kerja:'0';// jumlah hari kerja - ijin -sakit - mangkir
        $j_mangkir	= isset($j->j_mangkir)?$j->j_mangkir:'0';// jumlah mangkir
        $j_sakit	= isset($j->j_sakit)?$j->j_sakit:'0';// jumlah sakit

        $j_masuk 	= $j_hr_kerja - $cuti;// jumlah hari masuk kerja

        $i 			= $this->db->query("SELECT udf_jml_hr_kerja('$tgl_awal','$tgl_akhir') jml")->row(); // hari kerja normal

		// query gross
		if($row->sns=="STAFF") // jika staff
		{
			$q 			= $this->db->query("SELECT * FROM sal_staff_class a WHERE a.`id_band`='$row->id_band' AND a.`sal_pos`='$row->sal_pos' AND a.`idp`='$idp'")->row();
			$gd 		= $row->gd;

			$td 		= isset($q->td)?$q->td:'0';
			$tpj 		= isset($q->tpj)?$q->tpj:'0';
			$tk 		= isset($q->tk)?$q->tk:'0';
			$tprof 		= $row->tprof;
			$tops 		= 0;
			$bb 		= 0;
			$btw 		= 0;

			// cari tgl pertama kerja 
			// jika masuk ditengah periode
			if($row->tgl_masuk>=$tgl_awal && $row->tgl_masuk<=$tgl_akhir)
			{
				// jumlah hari
				$qq 	= $this->db->query("SELECT udf_jml_hr_kerja('$row->tgl_masuk','$tgl_akhir') jml")->row();

				$gd 	= ($qq->jml/$i->jml)*$gd;
				// $td  	= ($qq->jml/$i->jml)*$td;
				$tpj 	= ($qq->jml/$i->jml)*$tpj;
				$tprof 	= ($qq->jml/$i->jml)*$tprof;
				$tk 	= ($qq->jml/$i->jml)*$tk;
			}

			// jika keluar / resign ditengah periode
			if($row->tgl_resign>=$tgl_awal && $row->tgl_resign<=$tgl_akhir)
			{
				// jumlah hari
				$qq 	= $this->db->query("SELECT udf_jml_hr_kerja('$tgl_awal','$row->tgl_resign') jml")->row();

				$gd 	= ($qq->jml/$i->jml)*$gd;
				// $td  	= ($qq->jml/$i->jml)*$td;
				$tpj 	= ($qq->jml/$i->jml)*$tpj;
				$tprof 	= ($qq->jml/$i->jml)*$tprof;
				$tk 	= ($qq->jml/$i->jml)*$tk;
			} 

			//jika sedang cuti melahirkan
			$query_hamil= $this->db->query("SELECT * FROM cuti_khusus a WHERE a.`tgl_awal` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`tgl_akhir` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`id_tipe_cuti`='1' AND a.`nip`='$row->nip'")->row();
			if($query_hamil)
			{
				$tpj 	= 0;
			}
		}
		else // jika nonstaff
		{
			$ach 		= $this->db->query("SELECT * FROM ach a WHERE a.`nip` = '$row->nip' AND a.`id_per` = '$periode'")->row();
			$perf 		= isset($ach->perf)?$ach->perf:'';
			$snonstaff 	= $this->db->query("SELECT * FROM sal_nonstaff_class a WHERE a.`sal_pos` = '$row->sal_pos' AND a.`perf` = '$perf' AND a.`idp` = '$idp'")->row();
			$gd 		= isset($snonstaff->gd)?$snonstaff->gd:'0';
			$tops 		= isset($snonstaff->tops)?$snonstaff->tops:'0';
			$bb 		= isset($snonstaff->bb)?$snonstaff->bb:'0';
			$tprof 		= $row->tprof;
			$td 		= 0;
			$tpj 		= 0;
			$tk 		= 0;

			//cari tgl pertama kerja 
			// jika karyawan masuk ditengah periode
			if($row->tgl_masuk>=$tgl_awal && $row->tgl_masuk<=$tgl_akhir)
			{
				// jumlah hari
				$qq 	= $this->db->query("SELECT udf_jml_hr_kerja('$row->tgl_masuk','$tgl_akhir') jml")->row();

				$gd 	= ($qq->jml/$i->jml)*$gd;
			}

			// kehadiran 80 %
			$ab_80 		= $j_masuk*80/100;
			if($j_masuk<$ab_80)// jika kehadiran kurang dari 80%
			{
				// jumlah hari
				$qq 	= $j_masuk;

				$bb 	= ($qq->jml/$i->jml)*$bb;
			}

			// jika karyawan keluar / resign ditengah periode
			if($row->tgl_resign>=$tgl_awal && $row->tgl_resign<=$tgl_akhir)
			{
				// jumlah hari
				$qq 	= $this->db->query("SELECT udf_jml_hr_kerja('$tgl_awal','$row->tgl_resign') jml")->row();

				$bb 	= ($qq->jml/$i->jml)*$bb;
			}

			$q4 		= date_create($tgl_akhir);
			$quartal 	= date_format($q4,"m");

			// periode keluar btw
			if($quartal==1 || $quartal==4 || $quartal==7 || $quartal==10)
			{
				$btw 	= isset($snonstaff->btw)?$snonstaff->btw:'0';
			}
			else
			{
				$btw 	= 0;
			}
		}
		

		// query bbm bbp
		$w 			= $this->db->query("SELECT * FROM kary_bbmp a WHERE (a.`bbm_start`>= '$tgl_awal' OR a.`bbm_start`>='$tgl_awal') AND (a.`bbm_end`>='$tgl_akhir' OR a.`bbm_end`>='$tgl_akhir') AND a.`nip`='$row->nip'")->row();
		// query lembur
		$e 			= $this->db->query("SELECT (a.`lembur`+a.`uangmakan`) total FROM lembur a WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`nip`='$row->nip'")->row();
		// query tunjangan lainnya
		$f 			= $this->db->query("SELECT * FROM tunj_lainnya a WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`nip` = '$row->nip'")->row();
		// lokasi kerja karyawan
		$lok_kerja 	= $this->Main_Model->lokasi_kerja($row->nip);
		// faktor kemahalan yg dipakai
		$fk_mahal 	= $this->Main_Model->fk_lku($lok_kerja->id_lku, $fk);
		// umk dari faktor kemahalan
        $umk 		= isset($fk_mahal->umk)?$fk_mahal->umk:'';
        // table bpjs_tk
        $tbl_tk 	= $this->Penggajian_Model->view_bpjs();
        // table bpjs_ks
        $tbl_ks 	= $this->Karyawan_Model->bpjs_ks();
        // cari kelas bpjs
        $kelas 		= $this->Main_Model->kelas_bpjs($row->nip);
        $kls_bpjs 	= isset($kelas->kelas)?$kelas->kelas:'';

        // jika kelas 1
        $tot_bpjs_ks= 0;
        $tks 		= 0;
        $pks 		= 0; 
        if($kelas)
        {
	        if($kls_bpjs == '1')
			{
				$umkks 	= $tbl_ks->kelas;
			}
			else
			{
				$umkks 	= $umk;
			}

			// perhitungan bpjs ks
			$tks 		= $tbl_ks->tks*$umkks/100;
			$pks 		= $tbl_ks->pks*$umkks/100;
			$tot_bpjs_ks= $tks+$pks;
		}

        // perhitungan bpjs tk
        $jkk 		= $tbl_tk->jkk*$umk/100;
		$jkm 		= $tbl_tk->jkm*$umk/100;
		$jhttk  	= $tbl_tk->jhttk*$umk/100;
		$jhtp  		= $tbl_tk->jhtp*$umk/100;
		$jptk  		= $tbl_tk->jptk*$umk/100;
		$jpp 		= $tbl_tk->jpp*$umk/100;

		//potongan ketenagakerjaan
		$ptk 		= $jkk+$jkm+$jhttk+$jhtp+$jptk+$jpp;
		//tunjangan ketenagakerjaan	
		if($row->sns == "NON-STAFF")
		{
			$ttk 	= $jkk+$jkm+$jhttk+$jhtp+$jptk+$jpp;
		}
		else
		{
			$ttk 	= $jkk+$jkm+$jhttk+$jhtp;
		}

		// potongan lain 
		$g 			= $this->db->query("SELECT * FROM pot_lain a JOIN q_det b ON a.`id_per`=b.`qd_id` WHERE b.`tgl_awal` = '$tgl_awal' AND b.`tgl_akhir` = '$tgl_akhir' AND a.`nip` = '$row->nip'")->row();


		
		$lembur 	= isset($e->total)?$e->total:'0';
		$tunj_lain 	= isset($f->jumlah)?$f->jumlah:'0';
		$pot_lain 	= isset($g->jumlah)?$g->jumlah:'0';
		

		

		$h 			= $this->db->query("SELECT * FROM pot_det a JOIN q_det b ON a.`id_per`=b.`qd_id` JOIN pot c ON a.`id_pot`=c.`id_pot` WHERE b.`tgl_awal`='$tgl_awal' AND b.`tgl_akhir`='$tgl_akhir' AND c.`nip`='$row->nip' AND a.`inc_payroll` ='1'")->row();
		$pot_piutang= isset($h->ags)?$h->ags:'0';

		if($w)
		{
			$bbm 	= $w->bbm_value;
			if($w->bbm_end<$tgl_awal || $w->bbm_end<$tgl_akhir)
			{
				$bbp= $w->bbp_value;
			}
			else
			{
				$bbp= 0;
			}
		}
		else
		{
			$bbm 	= 0;
			$bbp 	= 0;
		}


		$gross 		= $gd+(($td/100)*$gd)+$tpj+$tk+$row->tprof+$tops;

		$pot_mangkir= $j_mangkir * ((150/100 * $gross) / 24);
		

		$pj 		= 0;
		if($row->stat_jab == "PJ" || $row->stat_jab == "TRAINING")
		{
			$pj 	= $tpj*20/100;
		}

		$total_tunj	= $bbm+$bbp+$lembur+$tunj_lain+$ttk+$tks+$bb+$btw;
		$total_pot	= $pks+$pot_lain+$pj+$pot_piutang+$ptk;
		$thp 		= $gross+$total_tunj-$total_pot;
		

        $l 			= $this->db->query("SELECT COALESCE(SUM(a.`jmljam`),0) jmljam FROM ijinjam a WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`nip` = '$row->nip'")->row();
        $j_ijinjam 	= isset($l->jmljam)?$l->jmljam:'0';

        $m 			= $this->db->query("SELECT COALESCE(SUM(a.`jml_jam`),0) jmljam FROM lembur a WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`nip` = '$row->nip'")->row();
        $j_jamlembur= isset($m->jmljam)?$m->jmljam:'0';

        $j_terlambat= $this->db->query("SELECT * FROM terlambat WHERE nip = '$row->nip' AND id_per = '$periode'")->row();
        $pot_telat = 0;
        if($j_terlambat)
        {
        	$pot_telat = 10000;
        	if($j_terlambat->total_menit>120 && $j_terlambat->total_menit<360)
        	{
        		$qa 			= $this->db->query("SELECT udf_jml_hr_kerja('$tgl_awal','$tgl_akhir') jml")->row();
    			$jml_jam_telat 	= number_format($j_terlambat->total_menit/120, 2);
    			$ex_jml_jam 	= explode(".", $jml_jam_telat);
    			if($ex_jml_jam[1]>80)
    			{
    				$val_jam_telat = $ex_jml_jam[0] + 1;
    			}
    			else
    			{
					$val_jam_telat = $ex_jml_jam[0];
    			}

        		if($row->sns=="STAFF")
        		{
        			$pot_telat = ($val_jam_telat/$q->jml)*$gross; 
        		}
        		else
        		{
        			$pot_telat = ($val_jam_telat/$q->jml)*$umk; 
        		}
        	}
        	elseif($j_terlambat->total_menit>360)
        	{
        		$pot_telat = 3 * ((150/100 * $gross) / 24);
        	}
        }
        
        	
        

			$data 	= array(
				'nip' 			=> $row->nip,
				'id_sk' 		=> $row->id_sk,
				'id_per'		=> $periode,
				'hr_kerja' 		=> $i->jml,
				'hr_kerja_kary'	=> $j_masuk,
				'tot_mangkir'	=> $j_mangkir,
				'tot_hr_sakit'	=> $j_sakit,
				'tot_jam_ijin' 	=> $j_ijinjam,
				'jam_lembur' 	=> $j_jamlembur,
				'jml_cuti' 		=> $cuti,
				//gross
				'gd' 			=> $gd,
				'td'			=> ($td/100)*$gd,
				'tpj'			=> $tpj,
				'tk'			=> $tk,
				'tprof' 		=> $tprof,
				// 'gross'			=> $this->Main_Model->uang($gross),
				//tunjangan
				'tlembur'		=> $lembur,
				'bbm' 			=> $bbm,
				'bbp'			=> $bbp,
				'tunj_lain'		=> $tunj_lain,
				'ttk' 			=> $ttk,
				'tks'			=> $tks,
				// non-staff -->
				'bb' 			=> $bb,
				'btw' 			=> $btw,
				'tops' 			=> $tops,
				// --> non-staff
				// 'total_tunj'	=> $this->Main_Model->uang($total_tunj),
				//potongan
				'pks'			=> $pks,
				'ptk' 			=> $ptk,
				'pl'			=> $pot_lain,
				'pjab'			=> $pj,
				'pot_ags'		=> $pot_piutang,
				'pot_mangkir'	=> $pot_mangkir,
				'pot_telat' 	=> $pot_telat,
				// 'total_pot'		=> $this->Main_Model->uang($total_pot),
				//thp
				'thp' 			=> ($gd+(($td/100)*$gd)+$tpj+$tk+$tprof+$tops)+($lembur+$bbm+$bbp+$tunj_lain+$ttk+$tks+$bb+$btw)-($pks+$ptk+$pot_lain+$pj+$pot_piutang+$pot_mangkir),
				'bank'			=> $row->bank,
				'norek'			=> $row->norek,
				'norek_an'		=> $row->norek_an,
				'tf_info' 		=> $row->tfinfo
				);
			// print_r($kary);
			$this->Gaji_Model->add_p_temp($data);
		}		
	}

	function view_gaji_after_submit_old($periode='', $cabang='')
	{
		$this->Main_Model->all_login();
		$data = $this->Gaji_Model->view_gaji_after_submit($periode, $grouplokasi, $lokasi);
		$template = $this->Main_Model->tbl_temp();
		$detail_gross = array('data'=>'Detail Gross','class'=>'none');
		$detail_tunjangan = array('data'=>'Detail Tunjangan','class'=>'none');
		$detail_potongan = array('data'=>'Detail Potongan','class'=>'none');
		$detail_thp = array('data'=>'Detail THP','class'=>'none');


		$this->table->set_heading('<center><input type="checkbox" name="all" id="all" /></center>','Nama','NIP','Periode','Gross','Tunjangan','Potongan','THP','Action');		

		foreach ($data as $row) {
		$gross 		= $row->gd+$row->td+$row->tpj+$row->tk+$row->tops+$row->tprof;
		$tunjangan	= $row->tlembur+$row->bbm+$row->bbp+$row->bb+$row->btw+$row->ttk+$row->tks+$row->tunj_lain;
		$potongan 	= $row->ptk+$row->pks+$row->pot_ags+$row->pjab+$row->pl+$row->pot_mangkir+$row->pot_telat;
		

		if($row->tgl_resign > $row->tgl_awal && $row->tgl_resign < $row->tgl_akhir)
		{
			$td_style 	= 'background-color:#ff6f69; color:white;';
		}
		elseif($row->tgl_masuk > $row->tgl_awal && $row->tgl_masuk < $row->tgl_akhir)
		{
			$td_style 	= 'background-color:#96ceb4; color:white;';
		}
		else
		{
			$td_style 	= '';
		}

		$checkbox 		= array('data'=>'<input type="checkbox" name="idgaji[]" id="idgaji" value="'.$row->id_per.'|'.$row->nip.'"/>','class'=>'text-center', 'style'=>$td_style);
		$row_nama 		= array('data'=>$row->nama, 'style'=>$td_style);
		$row_nip 		= array('data'=>$row->nip, 'style'=>$td_style);
		$row_periode 	= array('data'=>$row->periode, 'style'=>$td_style);
		$row_gross 		= array('data'=>$this->Main_Model->uang($gross), 'style'=>$td_style);
		$row_tunjangan 	= array('data'=>$this->Main_Model->uang($tunjangan), 'style'=>$td_style);
		$row_potongan 	= array('data'=>$this->Main_Model->uang($potongan), 'style'=>$td_style);
		$row_thp 		= array('data'=>$this->Main_Model->uang($row->thp), 'style'=>$td_style);
		$row_button 	= array('data'=>'<div class="btn-group"><button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions<i class="fa fa-angle-down"></i></button><ul class="dropdown-menu" role="menu"><li><a href="javascript:;" onclick="lihat_detail('.$row->nip.','.$row->id_per.')"><i class="icon-edit"></i> Detail </a></li><li><a href="javascript:;" onclick="edit_detail('.$row->nip.','.$row->id_per.')"><i class="icon-delete"></i> Edit </a></li></ul></div>');


		$this->table->add_row(
			$checkbox,
			$row_nama,
			$row_nip,
			$row_periode,
			$row_gross,
			$row_tunjangan,
			$row_potongan,
			$row_thp,
			$row_button 
	        );
		}

		$this->table->set_template($template);
		
		$table 	  = '<div class="row"><div class="portlet light"><div class="portlet-body"><div class="btn-group"><button class="btn btn-primary" type="button" data-toggle="modal" onclick="validate();" data-target="#myModal">Validasi Gaji</button></div></div><br>';
		$table 	 .= $this->table->generate();
		$table 	 .= '</div></div></div><script>
				    $("#all").click(function(){
				        $("input:checkbox").not(this).prop("checked", this.checked);
				    });
					function validate()
					{
						var data = { "idgaji[]" : []};
						$("#idgaji:checked").each(function() {
						  data["idgaji[]"].push($(this).val());
						});
						// console.log(data);
						$.ajax({
							url 	: "'.base_url('gaji/validate').'",
							data 	: data,
							type 	: "POST",
							success : function(data){
								// console.log(data);
								bootbox.dialog({
			                        message : "Proses Validasi Berhasil!",
			                        buttons :{
			                            main : {
			                                label : "OK",
			                                className : "blue",
			                                callback : function(){
			                                    view_gaji_after_submit($("#periode").val(), $("#grouplokasi").val(), $("#lokasi").val());
			                                }
			                            }
			                        }
			                    })
								
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								'.$this->Main_Model->notif500().'
							}
						});
					}
				    </script>';

		echo $table;
	}

	function view_gaji_after_submit()
	{
		$this->Main_Model->all_login();
		$cabang = $this->input->get('cabang');
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';
		$data = $this->Gaji_Model->view_gaji_after_submit($periode, $cabang);
		$template = $this->Main_Model->tbl_temp();

		$this->table->set_heading(
			'<center><input type="checkbox" name="all" id="all" /></center>',
			'NIP',
			'Nama',
			'Periode',
			'Cabang',
			'Divisi',
			'Posisi', 
			array(
				'data' => 'Upah Pokok', 
				'style' => 'width:50px;'), 
			'Tunj. Jabatan', 
			'Pengh. Masa Kerja', 
			'Tunj. Tidak Tetap', 
			'Tunj. Kemahalan', 
			'Ins. Presensi',
			'Action');		

		foreach ($data as $row) {

			if($row->tgl_resign >= $row->tgl_awal_ab && $row->tgl_resign <= $row->tgl_akhir_ab) {
				$td_style = 'background-color:#ff6f69 !important; color:white;';
			} elseif($row->tgl_masuk >= $row->tgl_awal_ab && $row->tgl_masuk <= $row->tgl_akhir_ab) {
				$td_style = 'background-color:#96ceb4 !important; color:white;';
			} else {
				$td_style = '';
			}

			$checkbox = array(
				'data' => '<input type="checkbox" name="idgaji[]" id="idgaji" value="'.$row->id.'"/>',
				'class' => 'text-center', 'style' => $td_style
			);
			$row_nama = array(
				'data' => $row->nama, 
				'style' => $td_style
			);
			$row_nip = array(
				'data' => $row->nip, 
				'style' => $td_style
			);
			$row_periode = array(
				'data' => $row->periode, 
				'style' => $td_style
			);
			$row_cabang = array(
				'data' => $row->cabang,
				'style' => $td_style
			);
			$row_divisi = array(
				'data' => $row->divisi,
				'style' => $td_style
			);
			$row_posisi = array(
				'data' => $row->jab,
				'style' => $td_style
			);
			$row_upah_pokok = array(
				'data' => $row->hari_upah_pokok,
				'style' => $td_style
			);
			$row_tunj_jabatan = array(
				'data' => $row->hari_tunj_jabatan,
				'style' => $td_style
			);
			$row_pengh_masa_kerja = array(
				'data' => $row->hari_pengh_kerja,
				'style' => $td_style
			);
			$row_tunj_tdk_tetap = array(
				'data' => $row->hari_tidak_tetap,
				'style' => $td_style
			);
			$row_tunj_kemahalan = array(
				'data' => $row->tunj_kemahalan,
				'style' => $td_style
			);
			$row_ins_presensi = array(
				'data' => $row->ins_presensi,
				'style' => $td_style
			);
			$edit = '<a href="javascript:;" onclick="edit_detail('.$row->id.')"><i class="fa fa-pencil fa-lg"></i></a>';
			$detail = '<a href="javascript:;" onclick="lihat_detail('.$row->id.')"><i class="fa fa-eye fa-lg"></i></a>';
			$row_button = array(
				'data' => $edit.'&nbsp;&nbsp;'.$detail
			);


			$this->table->add_row(
				$checkbox,
				$row_nip,
				$row_nama,
				$row_periode,
				$row_cabang,
				$row_divisi,
				$row_posisi,
				$row_upah_pokok,
				$row_tunj_jabatan,
				$row_pengh_masa_kerja,
				$row_tunj_tdk_tetap,
				$row_tunj_kemahalan,
				$row_ins_presensi,
				$row_button
		        );
		}

		$this->table->set_template($template);
		
		$table = '<div class="row">
					<div class="portlet light">
						<div class="portlet-body">
							<div class="btn-group">
								<button class="btn btn-primary" type="button" data-toggle="modal" onclick="validate();" data-target="#myModal">Validasi Gaji</button>
							</div>
						</div> <br>';
		$table .= $this->table->generate();
		$table .= '</div>
						</div>
							</div>
					<script>
					    $("#all").click(function(){
					        $("input:checkbox").not(this).prop("checked", this.checked);
					    });

						function validate() {
							var data = { 
								"idgaji[]" : []
							};

							$("#idgaji:checked").each(function() {
							  data["idgaji[]"].push($(this).val());
							});

							$.ajax({
								url : "'.base_url('gaji/validate').'",
								data : data,
								type : "POST",
								success : function(data){
									bootbox.dialog({
				                        message : "Proses Validasi Berhasil!",
				                        buttons :{
				                            main : {
				                                label : "OK",
				                                className : "blue",
				                                callback : function(){
				                                    view_gaji_after_submit($("#periode").val(), $("#cabang").val());
				                                }
				                            }
				                        }
				                    })
									
								}
							});
						}
				    </script>';

		echo $table;
	}

	function validasigaji()
	{
		$this->Main_Model->all_login();
		$javascript = '
			<script type="text/javascript">
				$(document).ready(function(){
					$(".select2").select2();
				});

				function view_gaji_after_submit() {
					var cabang = $("#cabang").val();
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					$.ajax({
						url : "'.base_url('gaji/view_gaji_after_submit').'",
						data : {
							"cabang" : cabang,
							"tahun" : tahun,
							"bulan" : bulan
						},
						beforeSend : function(){
							$("#tampil").addClass("hidden");
							$("#imgload").removeClass("hidden");
							$("#tutup").addClass("hidden");
						},
						complete : function(){
							$("#tampil").removeClass("hidden");
							$("#imgload").addClass("hidden");
							$("#tutup").removeClass("hidden");
						},
						success	: function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
			                    responsive : true,
								columnDefs: [
								    { "orderable": false, "targets": 0 }
								  ],
								aaSorting: [[13, "asc"]]
			                });
							$("#dataTables-example th").click(function(){
								$(".sorting_1").each(function(i, e){
									$(this).removeClass("sorting_1");
								});
							});
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal Menampilkan data!");
						}
					});
				}

				function tutup_periode() {
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();

			    	bootbox.dialog({
			    		message : "Yakin ingin menutup periode <b>"+ $("#bulan option[value="+bulan+"]").text() +" "+ $("#tahun option[value="+tahun+"]").text()+"</b> ?",
			    		title : "Tutup Periode",
			    		buttons :{
			    			danger : {
			    				label : "Tutup",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('gaji/tutup_periode') . '",
							    		type : "POST",
							    		data : {
							    			"tahun" : tahun,
							    			"bulan" : bulan
							    		},
							    		success : function(data){
							    			bootbox.alert({
												message: "Periode telah ditutup!",
												size: "small",
												callback : function(){
													location.reload();
												}
											});
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})
			    }

			    function lihat_detail(id) {
			    	$.ajax({
			    		url : "'.base_url('gaji/payrol_detail').'/"+id,
			    		success	: function(data){
			    			$("#payroll").modal();
			    			$("#target_modal").html(data);
			    		},
			    		error : function(jqXHR,textStatus,errorThrown){
			    			bootbox.alert("Gagal mengambil data!");
			    		}
			    	});
			    }

			    function edit_detail(id) {
			    	$.ajax({
			    		url : "'.base_url('gaji/edit_payrol_detail').'/"+id,
			    		success	: function(data){
			    			$("#edit_payroll").modal();
			    			$("#target_modal_edit").html(data);
			    		},
			    		error : function(jqXHR,textStatus,errorThrown){
			    			bootbox.alert("Gagal mengambil data!");
			    		}
			    	});
			    }

			    function save() {
			    	$.ajax({
			    		url : "'.base_url('gaji/process_edit_payroll').'",
			    		data : $("#form_edit_payroll").serialize(),
			    		type : "post",
			    		dataType : "json",
			    		success : function(data){
			    			if (data.status == true) {
				    			bootbox.alert({
									message: data.message,
									size: "small",
									callback : function(){
										view_gaji_after_submit($("#periode").val(), $("#grouplokasi").val(), $("#lokasi").val());
										$("#edit_payroll").modal("hide");
									}
								});
							} else {
								bootbox.alert(data.message);
							}
			    		},
			    		error 	: function(jqXHR,textStatus,errorThrown){
			    			bootbox.alert("Gagal mengupdate data!");
			    		}
			    	});
			    }
			</script>';

		$sess_cabang = $this->Main_Model->session_cabang();
		$cabang_create = array();
		$cabang_create[] = 'Semua Cabang';
		if(!empty($sess_cabang)) {
			foreach($sess_cabang as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang_create[$d->id_cab] = $d->cabang;
			}
		} else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang_create[$row->id_cab] = $row->cabang;
			}
		}

		$header = array(
			'menu' => $this->Main_Model->menu_user('0','0','144'),
			'style' => $this->header()
		);

		$footer = array(
			'javascript'=> $javascript,
			'js' => $this->footer()
			);

		$data = array(
			'periode' => $this->Main_Model->periode_opt(),
			'idp' => $this->Main_Model->all_bu(),
			'cabang' => $cabang_create
			);

		$this->load->view('template/header',$header);
		$this->load->view('gaji/validasigaji',$data);
		$this->load->view('template/footer',$footer);
	}

	function edit_payrol_detail($id='')
	{
		$this->Main_Model->all_login();
		$q = $this->Gaji_Model->view_detail_gaji($id);
		$nip = is_value($q->nip);
		$nama = is_value($q->nama);
		$jab = is_value($q->jab);
		$cabang = is_value($q->cabang);
		$divisi = is_value($q->divisi);
		$periode = is_value($q->periode);
		$hari_upah_pokok = is_value($q->hari_upah_pokok);
		$hari_tunj_jabatan = is_value($q->hari_tunj_jabatan);
		$hari_pengh_kerja = is_value($q->hari_pengh_kerja);
		$hari_tidak_tetap = is_value($q->hari_tidak_tetap);
		$tunj_kemahalan = is_value($q->tunj_kemahalan);
		$ins_presensi = is_value($q->ins_presensi);

		$telat = is_value($q->telat); 
		$mangkir = is_value($q->mangkir);
		$cuti = is_value($q->cuti);
		$sakit = is_value($q->sakit);
		$ijin = is_value($q->ijin);
		$sisa_cuti = is_value($q->sisa_cuti);
		$cuti_khusus = is_value($q->cuti_khusus);
		$keterangan = is_value($q->keterangan);
		$flag = is_value($q->flag);

		$val_pokok = is_value($q->val_pokok);
		$val_jabatan = is_value($q->val_jabatan);
		$val_masakerja = is_value($q->val_masakerja);
		$val_tdktetap = is_value($q->val_tdktetap);
		$val_kemahalan = is_value($q->val_kemahalan);
		$val_presensi = is_value($q->val_presensi);

		$modal 	= '
		<form id="form_edit_payroll">
			<table width="100%">
		        <tr>
		            <td>
		                <table border="1" class="table table-hover table-bordered">
		                    <tr> 
		                        <td width="20%">Nama </td>
		                        <td width="2%">:</td>
		                        <td>'.$nama.'</td>
		                    </tr>
		                    <tr>
		                        <td>Posisi</td>
		                        <td>:</td>
		                        <td>'.$jab.'</td>
		                    </tr>
		                    <tr>
		                        <td>Cabang</td>
		                        <td>:</td>
		                        <td>'.$cabang.'</td>
		                    </tr>
		                    <tr>
		                        <td>Periode</td>
		                        <td>:</td>
		                        <td>'.$periode.'</td>
		                    </tr>
		                </table>
		            </td>
		        </tr>
		        <tr>
		            <td>
		            	<table border="1" class="table table-hover table-bordered">
	                        <!--<tr>
	                            <td colspan="3" width="50%"><b>FAKTOR GAJI</b></td>
	                            <td colspan="3" width="50%"><b>FAKTOR POTONGAN</b></td>
	                        </tr>-->
	                        <tr>
	                            <td>Upah Pokok</td>
	                            <td width="2%">:</td>
	                            <td><input type="hidden" name="id" value="'.$id.'" ><input type="text" name="hup" id="hup" class="form-control" value="'.$hari_upah_pokok.'" /></td>
	                            <td>Terlambat</td>
	                            <td width="2%">:</td>
	                            <td>'.$telat.'</td>
	                        </tr>
	                        <tr>
	                            <td>Tunj. Jabatan</td>
	                            <td>:</td>
	                            <td><input type="text" name="htj" id="htj" class="form-control" value="'.$hari_tunj_jabatan.'" /></td>
	                            <td>Mangkir</td>
	                            <td>:</td>
	                            <td>'.$mangkir.'</td>
	                        </tr>
	                        <tr>
	                            <td>Pengh. Masa Kerja</td>
	                            <td>:</td>
	                            <td><input type="text" name="hpk" id="hpk" class="form-control" value="'.$hari_pengh_kerja.'" /></td>
	                            <td>Sakit</td>
	                            <td>:</td>
	                            <td>'.$sakit.'</td>
	                        </tr>
	                        <tr>
	                            <td>Tunj. Tidak Tetap</td>
	                            <td>:</td>
	                            <td><input type="text" name="htt" id="htt" class="form-control" value="'.$hari_tidak_tetap.'" /></td>
	                            <td>Ijin</td>
	                            <td>:</td>
	                            <td>'.$ijin.'</td>
	                        </tr>
	                        <tr>
	                            <td>Tunj. Kemahalan</td>
	                            <td>:</td>
	                            <td><input type="text" name="tk" id="tk" class="form-control" value="'.$tunj_kemahalan.'" /></td>
	                            <td>Cuti</td>
	                            <td>:</td>
	                            <td>'.$cuti.'</td>
	                        </tr>
	                        <tr>
	                            <td>Ins. Presensi</td>
	                            <td>:</td>
	                            <td><input type="text" name="ip" id="ip" class="form-control" value="'.$ins_presensi.'" /></td>
	                            <td>Cuti Khusus</td>
	                            <td>:</td>
	                            <td>'.$cuti_khusus.'</td>
	                        </tr>
	                        
	                        <tr>
	                            <td>Upah Pokok (Rp)</td>
	                            <td>:</td>
	                            <td><input type="text" name="rup" id="rup" class="form-control" value="'.$val_pokok.'" /></td>
	                            <td>Sisa Cuti</td>
	                            <td>:</td>
	                            <td>'.$sisa_cuti.'</td>
	                        </tr>
	                        <tr>
	                            <td>Tunj Jabatan (Rp)</td>
	                            <td>:</td>
	                            <td><input type="text" name="rtj" id="rtj" class="form-control" value="'.$val_jabatan.'" /></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td>Pengh Masa Kerja (Rp)</td>
	                            <td>:</td>
	                            <td><input type="text" name="rpk" id="rpk" class="form-control" value="'.$val_masakerja.'" /></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td>Tunj Tidak Tetap (Rp)</td>
	                            <td>:</td>
	                            <td><input type="text" name="rtt" id="rtt" class="form-control" value="'.$val_tdktetap.'" /></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td>Tunj Kemahalan (Rp)</td>
	                            <td>:</td>
	                            <td><input type="text" name="rtk" id="rtk" class="form-control" value="'.$val_kemahalan.'" /></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td>Ins Presensi (Rp)</td>
	                            <td>:</td>
	                            <td><input type="text" name="rip" id="rip" class="form-control" value="'.$val_presensi.'" /></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                        </tr>
	                        <tr>
	                            <td>Keterangan</td>
	                            <td>:</td>
	                            <td><textarea class="form-control" name="keterangan" id="keterangan" rows="6">'.$keterangan.'</textarea></td>
	                            <td></td>
	                            <td></td>
	                            <td></td>
	                        </tr>
	                    </table>
	                </td>
	            </tr>
		    </table>
	    </form>';
		echo $modal;
	}

	function payrol_detail($id='', $table='')
	{
		$this->Main_Model->all_login();
		$q = $this->Gaji_Model->view_detail_gaji($id, $table);
		$nip = is_value($q->nip);
		$nama = is_value($q->nama);
		$jab = is_value($q->jab);
		$cabang = is_value($q->cabang);
		$divisi = is_value($q->divisi);
		$periode = is_value($q->periode);
		$hari_upah_pokok = is_value($q->hari_upah_pokok);
		$hari_tunj_jabatan = is_value($q->hari_tunj_jabatan);
		$hari_pengh_kerja = is_value($q->hari_pengh_kerja);
		$hari_tidak_tetap = is_value($q->hari_tidak_tetap);
		$tunj_kemahalan = is_value($q->tunj_kemahalan);
		$ins_presensi = is_value($q->ins_presensi);

		$telat = is_value($q->telat); 
		$mangkir = is_value($q->mangkir);
		$cuti = is_value($q->cuti);
		$sakit = is_value($q->sakit);
		$ijin = is_value($q->ijin);
		$sisa_cuti = is_value($q->sisa_cuti);
		$cuti_khusus = is_value($q->cuti_khusus);
		$keterangan = is_value($q->keterangan);
		$flag = is_value($q->flag);

		// value
		$val_pokok = is_value($q->val_pokok);
		$val_jabatan = is_value($q->val_jabatan);
		$val_masakerja = is_value($q->val_masakerja);
		$val_tdktetap = is_value($q->val_tdktetap);
		$val_kemahalan = is_value($q->val_kemahalan);
		$val_presensi = is_value($q->val_presensi);

		$total = $val_pokok + $val_jabatan + $val_masakerja + $val_tdktetap + $val_kemahalan + $val_presensi;

		$modal 	= '
		<table width="100%">
	        <tr>
	            <td>
	                <table border="1" class="table table-hover table-bordered">
	                    <tr> 
	                        <td width="20%">Nama </td>
	                        <td width="2%">:</td>
	                        <td>'.$nama.'</td>
	                    </tr>
	                    <tr>
	                        <td>Posisi</td>
	                        <td>:</td>
	                        <td>'.$jab.'</td>
	                    </tr>
	                    <tr>
	                        <td>Cabang</td>
	                        <td>:</td>
	                        <td>'.$cabang.'</td>
	                    </tr>
	                    <tr>
	                        <td>Periode</td>
	                        <td>:</td>
	                        <td>'.$periode.'</td>
	                    </tr>
	                </table>
	            </td>
	        </tr>
	        <tr>
	            <td>
	            	<table border="1" class="table table-hover table-bordered">
                        <!--<tr>
                            <td colspan="3" width="50%"><b>FAKTOR GAJI</b></td>
                            <td colspan="3" width="50%"><b>FAKTOR POTONGAN</b></td>
                        </tr>-->
                        <tr>
                            <td>Upah Pokok</td>
                            <td width="2%">:</td>
                            <td>'.$hari_upah_pokok.'</td>
                            <td>Terlambat</td>
                            <td width="2%">:</td>
                            <td>'.$telat.'</td>
                        </tr>
                        <tr>
                            <td>Tunj. Jabatan</td>
                            <td>:</td>
                            <td>'.$hari_tunj_jabatan.'</td>
                            <td>Mangkir</td>
                            <td>:</td>
                            <td>'.$mangkir.'</td>
                        </tr>
                        <tr>
                            <td>Pengh. Masa Kerja</td>
                            <td>:</td>
                            <td>'.$hari_pengh_kerja.'</td>
                            <td>Sakit</td>
                            <td>:</td>
                            <td>'.$sakit.'</td>
                        </tr>
                        <tr>
                            <td>Tunj. Tidak Tetap</td>
                            <td>:</td>
                            <td>'.$hari_tidak_tetap.'</td>
                            <td>Ijin</td>
                            <td>:</td>
                            <td>'.$ijin.'</td>
                        </tr>
                        <tr>
                            <td>Tunj. Kemahalan</td>
                            <td>:</td>
                            <td>'.$tunj_kemahalan.'</td>
                            <td>Cuti</td>
                            <td>:</td>
                            <td>'.$cuti.'</td>
                        </tr>
                        <tr>
                            <td>Ins. Presensi</td>
                            <td>:</td>
                            <td>'.$ins_presensi.'</td>
                            <td>Cuti Khusus</td>
                            <td>:</td>
                            <td>'.$cuti_khusus.'</td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td>'.$keterangan.'</td>
                            <td>Sisa Cuti</td>
                            <td>:</td>
                            <td>'.$sisa_cuti.'</td>
                        </tr>
                        <tr><td colspan="6"><strong>Nominal</strong></td></tr>
                        <tr>
                        	<td>Upah Pokok</td>
                            <td>:</td>
                            <td>'.$this->Main_Model->uang($val_pokok).'</td>
                            <td>Tunj. Jabatan</td>
                            <td>:</td>
                            <td>'.$this->Main_Model->uang($val_jabatan).'</td>
                        </tr>
                        <tr>
                        	<td>Pengh Masa Kerja</td>
                            <td>:</td>
                            <td>'.$this->Main_Model->uang($val_masakerja).'</td>
                            <td>Tunj. Tidak Tetap</td>
                            <td>:</td>
                            <td>'.$this->Main_Model->uang($val_tdktetap).'</td>
                        </tr>
                        <tr>
                        	<td>Tunj. Kemahalan</td>
                            <td>:</td>
                            <td>'.$this->Main_Model->uang($val_kemahalan).'</td>
                            <td>Ins. Presensi</td>
                            <td>:</td>
                            <td>'.$this->Main_Model->uang($val_presensi).'</td>
                        </tr>
                        <tr>
                        	<td><strong>Total</strong></td>
                            <td>:</td>
                            <td colspan="4">'.$this->Main_Model->uang($total).'</td>
                        </tr>
                    </table>
                </td>
            </tr>
	    </table>';
		echo $modal;
	}

	function process_edit_payroll()
	{
		$this->Main_Model->all_login();
		$id = $this->input->post('id');
		$hup = $this->input->post('hup');
		$htj = $this->input->post('htj');
		$hpk = $this->input->post('hpk');
		$htt = $this->input->post('htt');
		$tk = $this->input->post('tk');
		$ip = $this->input->post('ip');
		$rup = $this->input->post('rup');
		$rtj = $this->input->post('rtj');
		$rpk = $this->input->post('rpk');
		$rtt = $this->input->post('rtt');
		$rtk = $this->input->post('rtk');
		$rip = $this->input->post('rip');
		$keterangan = $this->input->post('keterangan');

		if ($keterangan == '') {
			$status = FALSE;
			$message = 'Field Keterangan harus diisi!';
		} else {
			// insert into log
			$this->Gaji_Model->log_payroll($id);

			$data = array(
				'hari_upah_pokok' => $hup,
				'hari_tunj_jabatan' => $htj,
				'hari_pengh_kerja' => $hpk,
				'hari_tidak_tetap' => $htt,
				'tunj_kemahalan' => $tk,
				'ins_presensi' => $ip,
				'val_pokok' => $rup,
				'val_jabatan' => $rtj,
				'val_masakerja' => $rpk,
				'val_tdktetap' => $rtt,
				'val_kemahalan' => $rtk,
				'val_presensi' => $rip,
				'keterangan'  => $keterangan,
				'flag' => 1, 
				'update_at' => now(),
				'user_update' => $this->session->userdata('username')
			);

			$status = TRUE;
			$message = 'Success!';
			// print_r($data);
			$this->Gaji_Model->process_edit_payroll($data, $id);
		}

		echo json_encode(array('status' => $status, 'message' => $message));
	}

	function payrol_detail_hist($nip, $id_per)
	{
		// $this->Main_Model->all_login();
		$cek_staff_nonstaff 	= $this->db->query("SELECT d.`sns` FROM p_hist a JOIN sk b ON a.`id_sk`=b.`id_sk` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN pos d ON c.`id_pos`=d.`id_pos` WHERE a.`nip`='$nip' AND a.`id_per`='$id_per'")->row();
		if($cek_staff_nonstaff->sns == 'STAFF')
		{
			$data 	= $this->db->query("SELECT b.`nama`,e.`posisi`,f.`lka`,DATE_FORMAT(g.`tgl_akhir`,'%M %Y') periode, a.`thp`, a.`gd`,a.`td`,a.`tpj`,a.`tprof`,a.`tk`,a.`tunj_lain`,a.`bbm`,a.`bbp`,a.`tlembur`,a.`ttk`,a.`tks`,(a.`gd`+a.`td`+a.`tpj`+a.`tprof`+a.`tk`) gross,a.`ptk`,a.`pks`,a.`pot_ags`,a.`pl`,a.`pjab`,a.`pot_mangkir`,a.`pot_telat`,(a.`ptk`+a.`pks`+a.`pot_ags`+a.`pl`+a.`pjab`+a.`pot_mangkir`+a.`pot_telat`) potongan,(a.`bbm`+a.`bbp`+a.`tlembur`+a.`ttk`+a.`tks`+a.`tunj_lain`) tunjangan FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`id_sk`=c.`id_sk` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON d.`id_pos`=e.`id_pos` JOIN lka f ON d.`id_lka`=f.`id_lka` JOIN q_det g ON a.`id_per`=g.`qd_id` WHERE a.`nip` = '$nip' AND a.`id_per` = '$id_per'")->row();
			$gross_tunjangan = $data->gross + $data->tunjangan;
			$modal 	= '
			<table width="100%">
		        <tr>
		            <td>
		                <table border="1" class="table table-hover table-bordered">
		                    <tr> 
		                        <td width="30%">Nama </td>
		                        <td width="2%">:</td>
		                        <td>'.$data->nama.'</td>
		                    </tr>
		                    <tr>
		                        <td>Posisi</td>
		                        <td>:</td>
		                        <td>'.$data->posisi.'</td>
		                    </tr>
		                    <tr>
		                        <td>Area</td>
		                        <td>:</td>
		                        <td>'.$data->lka.'</td>
		                    </tr>
		                    <tr>
		                        <td>Payroll Periode</td>
		                        <td>:</td>
		                        <td>'.$data->periode.'</td>
		                    </tr>
		                    <!--<tr>
		                        <td>Total Gaji Gross</td>
		                        <td>:</td>
		                        <td>'.$this->Main_Model->uang($data->gross).'</td>
		                    </tr>-->
		                    <tr>
		                        <td>Total Penerimaan Gaji(THP)</td>
		                        <td>:</td>
		                        <td>'.$this->Main_Model->uang($data->thp).'</td>
		                              
		                    </tr>
		                </table>
		            </td>
		        </tr>
		        <tr>
		            <td>
		            	<table border="1" class="table table-hover table-bordered">
                            <tr>
                                <td colspan="3" width="50%"><b>FAKTOR GAJI</b></td>
                                <td colspan="3" width="50%"><b>FAKTOR POTONGAN</b></td>
                            </tr>
                            <tr>
                                <td>Gaji Dasar</td>
                                <td width="2%">:</td>
                                <td>'.$this->Main_Model->uang($data->gd).'</td>
                                <td>Potongan Ketenagakerjaan</td>
                                <td width="2%">:</td>
                                <td>'.$this->Main_Model->uang($data->ptk).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Dasar</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->td).'</td>
                                <td>Potongan Kesehatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pks).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Jabatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tpj).'</td>
                                <td>Potongan Pinjaman</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pot_ags).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Komunikasi</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tk).'</td>
                                <td>Potongan Lainnya</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pl).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Profesional</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tprof).'</td>
                                <td>Potongan Jabatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pjab).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Mutasi (BBM)</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->bbm).'</td>
                                <td>Potongan Telat</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pot_telat).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Perumahan (BPP)</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->bbp).'</td>
                                <td>Potongan Mangkir</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pot_mangkir).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Lembur</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tlembur).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Tunjangan Ketenagakerjaan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->ttk).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Tunjangan Kesehatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tks).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Tunjangan Lainnya</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tunj_lain).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>TOTAL</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($gross_tunjangan).'</td>
                                <td>TOTAL</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->potongan).'</td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                
		    </table>';
		}
		else
		{
			$data 	= $this->db->query("SELECT a.`bb`,a.`btw`,b.`nama`,e.`posisi`,f.`lka`,DATE_FORMAT(g.`tgl_akhir`,'%M %Y') periode, a.`thp`, a.`gd`,a.`tops`,a.`bbm`,a.`bbp`,a.`tlembur`,a.`ttk`,a.`tks`,(a.`gd`+a.`tops`+a.`bb`+a.`btw`) gross,a.`ptk`,a.`pks`,a.`pot_ags`,a.`pl`,a.`pjab`,a.`tunj_lain`,a.`pot_mangkir`,a.`pot_telat`,(a.`ptk`+a.`pks`+a.`pot_ags`+a.`pl`+a.`pjab`+a.`pot_mangkir`+a.`pot_telat`) potongan,(a.`bbm`+a.`bbp`+a.`tlembur`+a.`ttk`+a.`tks`+a.`tunj_lain`) tunjangan FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`id_sk`=c.`id_sk` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON d.`id_pos`=e.`id_pos` JOIN lka f ON d.`id_lka`=f.`id_lka` JOIN q_det g ON a.`id_per`=g.`qd_id` WHERE a.`nip` = '$nip' AND a.`id_per` = '$id_per'")->row();
			$gross_tunjangan = $data->gross + $data->tunjangan;
			$modal = 
			'<table width="100%">
		        <tr>
		            <td>
		                <table border="1" class="table table-hover table-bordered">
		                    <tr> 
		                        <td width="30%">Nama </td>
		                        <td width="2%">:</td>
		                        <td>'.$data->nama.'</td>
		                    </tr>
		                    <tr>
		                        <td>Posisi</td>
		                        <td>:</td>
		                        <td>'.$data->posisi.'</td>
		                    </tr>
		                    <tr>
		                        <td>Area</td>
		                        <td>:</td>
		                        <td>'.$data->lka.'</td>
		                    </tr>
		                    <tr>
		                        <td>Payroll Periode</td>
		                        <td>:</td>
		                        <td>'.$data->periode.'</td>
		                    </tr>
		                    <!--<tr>
		                        <td>Total Gaji Gross</td>
		                        <td>:</td>
		                        <td>'.$this->Main_Model->uang($data->gross).'</td>
		                    </tr>-->
		                    <tr>
		                        <td>Total Penerimaan Gaji(THP)</td>
		                        <td>:</td>
		                        <td>'.$this->Main_Model->uang($data->thp).'</td>
		                              
		                    </tr>
		                </table>
		            </td>
		        </tr>
		        <tr>
		            <td>
		            	<table border="1" class="table table-hover table-bordered">
                            <tr>
                                <td colspan="3" width="50%"><b>FAKTOR GAJI</b></td>
                                <td colspan="3" width="50%"><b>FAKTOR POTONGAN</b></td>
                            </tr>
                            <tr>
                                <td>Gaji Dasar</td>
                                <td width="2%">:</td>
                                <td>'.$this->Main_Model->uang($data->gd).'</td>
                                <td>Potongan Ketenagakerjaan</td>
                                <td width="2%">:</td>
                                <td>'.$this->Main_Model->uang($data->ptk).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Operasional</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tops).'</td>
                                <td>Potongan Kesehatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pks).'</td>
                            </tr>
                            <tr>
                                <td>Bonus Bulanan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->bb).'</td>
                                <td>Potongan Pinjaman</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pot_ags).'</td>
                            </tr>
                            <tr>
                                <td>Bonus Triwulan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->btw).'</td>
                                <td>Potongan Lainnya</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pl).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Mutasi (BBM)</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->bbm).'</td>
                                <td>Potongan Jabatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pjab).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Perumahan (BPP)</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->bbp).'</td>
                                <td>Potongan Telat</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pot_telat).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Lembur</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tlembur).'</td>
                                <td>Potongan Mangkir</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->pot_mangkir).'</td>
                            </tr>
                            <tr>
                                <td>Tunjangan Ketenagakerjaan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->ttk).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Tunjangan Kesehatan</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tks).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Tunjangan Lainnya</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->tunj_lain).'</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>TOTAL</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($gross_tunjangan).'</td>
                                <td>TOTAL</td>
                                <td>:</td>
                                <td>'.$this->Main_Model->uang($data->potongan).'</td>
                            </tr>
                        </table>
                    </td>
                    
                </tr>
                
		    </table>';
		}

		echo $modal;
	}

	function validate()
	{
		$this->Main_Model->all_login();
		$idgaji = $this->input->post('idgaji');

		for ($i = 0; $i < count($idgaji); $i++) { 
			$id = $idgaji[$i];
			$q 	= $this->db->query("SELECT * FROM temp_payroll WHERE id = '$id'")->row();
			if($q) {
				$data[$i] 	= array(
					'nip' => $q->nip,
					'id_periode' => $q->id_periode,
					'id_sk'	=> $q->id_sk,
					'hari_upah_pokok' => $q->hari_upah_pokok,
					'hari_tunj_jabatan' => $q->hari_tunj_jabatan,
					'hari_pengh_kerja' => $q->hari_pengh_kerja,
					'hari_tidak_tetap' => $q->hari_tidak_tetap,
					'tunj_kemahalan' => $q->tunj_kemahalan,
					'ins_presensi' => $q->ins_presensi,
					'telat' => $q->telat,
					'mangkir' => $q->mangkir,
					'cuti' => $q->cuti,
					'sakit' => $q->sakit,
					'ijin' => $q->ijin,
					'sisa_cuti' => $q->sisa_cuti,
					'cuti_khusus' => $q->cuti_khusus,
					'keterangan' => $q->keterangan,
					'val_pokok' => $q->val_pokok,
					'val_jabatan' => $q->val_jabatan,
					'val_masakerja' => $q->val_masakerja,
					'val_tdktetap' => $q->val_tdktetap,
					'val_kemahalan' => $q->val_kemahalan,
					'val_presensi' => $q->val_presensi,
					'flag' => $q->flag,
					'user_insert' => $this->session->userdata('username'),
					'insert_at' => now()
					);
					
					$cek_p_hist = $this->db->query("SELECT * FROM tb_payroll a WHERE a.`nip`='$q->nip' AND a.`id_periode`='$q->id_periode'")->row();

					if($cek_p_hist) {
						$this->Gaji_Model->delete_p_hist($q->nip, $q->id_periode);
					}
				
					$this->Gaji_Model->insert_p_hist($data[$i]);
					$this->Gaji_Model->delete_p_temp($id);
			}
		}
	}

	function historygaji()
	{
		$this->Main_Model->all_login();
		$javascript = '
			<script type="text/javascript">
    			
				$("#download").click(function(){
					var cabang = $("#cabang").val();
					var tahun = $("#tahun").val();
					var bulan = $("#bulan").val();
					window.location.href="'.base_url('download_excel/laporan_gaji').'?cabang="+cabang+"&tahun="+tahun+"&bulan="+bulan;
				});

				$(document).ready(function(){
					$(".select2").select2();
				});

				function load_table(){
					var tahun = $("#tahun").val();
					var cabang = $("#cabang").val();
					var bulan = $("#bulan").val();

					var dat = {
						"tahun" : tahun,
						"bulan" : bulan,
						"cabang" : cabang
					}
					$.ajax({
						url : "'.base_url('gaji/view_gaji').'",
						data : dat,
						type : "GET",
						beforeSend : function() {
							$("#tampil").addClass("hidden");
							$("#download").addClass("hidden");
							$("#imgload").removeClass("hidden");
						},
						complete : function() {
							$("#tampil").removeClass("hidden");
							$("#download").removeClass("hidden");
							$("#imgload").addClass("hidden");
						},
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
        						responsive: true
    						});
						},
						error : function(jqXHR,textStatus,errorThrown){
							bootbox.alert("Gagal mengambil data!");
						}
					});
				}

				function cetak(per,nip) {
					window.open("'.base_url('gaji/slip_gaji_nip').'/"+nip+"/"+per);
					return false;
				}

				function lihat_detail(id) {
			    	$.ajax({
			    		url : "'.base_url('gaji/payrol_detail').'/"+id+"/tb_payroll",
			    		success	: function(data){
			    			$("#payroll").modal();
			    			$("#target_modal").html(data);
			    		},
			    		error : function(jqXHR,textStatus,errorThrown){
			    			bootbox.alert("Gagal mengambil data!");
			    		}
			    	});
			    }
			</script>';
		$header = array(
			'menu' => $this->Main_Model->menu_user('0','0','144'),
			'style' => $this->header()
		);

		$footer =array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);

		$sess_cabang = $this->Main_Model->session_cabang();
		$cabang_create = array();
		$cabang_create[] = 'Semua Cabang';
		if(!empty($sess_cabang)) {
			foreach($sess_cabang as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang_create[$d->id_cab] = $d->cabang;
			}
		} else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang_create[$row->id_cab] = $row->cabang;
			}
		}

		$data = array(
			'periode' => $this->Main_Model->periode_opt_this_year(),
			'cabang' => $cabang_create
			);

		$this->load->view('template/header',$header);
		$this->load->view('gaji/historygaji',$data);
		$this->load->view('template/footer',$footer);
	}

	function grouplokasi()
	{
		$this->Main_Model->all_login();
		$id 	= $this->input->get('grouplokasi');
		if($id==3) {
			echo '';
		} else {
			$data 	= $this->Gaji_Model->grouplokasi($id);
			echo '
				<div class="form-group">
					<label>Lokasi</label>
						'.form_dropdown('lokasi',$data,'','id="lokasi" class="form-control"').'
				</div>';
		}
	}

	function view_gaji()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->get('tahun');
		$bulan = $this->input->get('bulan');
		$cabang = $this->input->get('cabang');
		// $grouplokasi 	= $this->input->get('grouplokasi');
		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';

		$data = $this->Gaji_Model->view_gaji_after_submit($periode, $cabang, 'tb_payroll');
		$template = $this->Main_Model->tbl_temp();
		// $this->table->set_heading('No','NIP','Nama','Posisi','Total THP Gross','Benefit','Total Potongan','THP','Action');
		$this->table->set_heading('No','NIP','Nama','Periode','Cabang','Divisi','Posisi', 
			array('data' => 'Upah Pokok', 'style' => 'width:50px;'), 'Tunj. Jabatan', 'Pengh. Masa Kerja', 'Tunj. Tidak Tetap', 'Tunj. Kemahalan', 'Ins. Presensi', 'Action');

		$no = 1;
		foreach ($data as $row) {
			if($row->tgl_resign >= $row->tgl_awal_ab && $row->tgl_resign <= $row->tgl_akhir_ab) {
				$td_style = 'background-color:#ff6f69 !important; color:white;';
			} elseif($row->tgl_masuk >= $row->tgl_awal_ab && $row->tgl_masuk <= $row->tgl_akhir_ab) {
				$td_style = 'background-color:#96ceb4 !important; color:white;';
			} else {
				$td_style = '';
			}

			$row_no = array(
				'data' => $no++,
				'class' => $td_style
			);

			$checkbox = array(
				'data' => '<input type="checkbox" name="idgaji[]" id="idgaji" value="'.$row->id.'"/>',
				'class' => 'text-center', 'style' => $td_style
			);
			$row_nama = array(
				'data' => $row->nama, 
				'style' => $td_style
			);
			$row_nip = array(
				'data' => $row->nip, 
				'style' => $td_style
			);
			$row_periode = array(
				'data' => $row->periode, 
				'style' => $td_style
			);
			$row_cabang = array(
				'data' => $row->cabang,
				'style' => $td_style
			);
			$row_divisi = array(
				'data' => $row->divisi,
				'style' => $td_style
			);
			$row_posisi = array(
				'data' => $row->jab,
				'style' => $td_style
			);
			$row_upah_pokok = array(
				'data' => $row->hari_upah_pokok,
				'style' => $td_style
			);
			$row_tunj_jabatan = array(
				'data' => $row->hari_tunj_jabatan,
				'style' => $td_style
			);
			$row_pengh_masa_kerja = array(
				'data' => $row->hari_pengh_kerja,
				'style' => $td_style
			);
			$row_tunj_tdk_tetap = array(
				'data' => $row->hari_tidak_tetap,
				'style' => $td_style
			);
			$row_tunj_kemahalan = array(
				'data' => $row->tunj_kemahalan,
				'style' => $td_style
			);
			$row_ins_presensi = array(
				'data' => $row->ins_presensi,
				'style' => $td_style
			);
			
			$detail = '<a href="javascript:;" onclick="lihat_detail('.$row->id.')"><i class="fa fa-eye fa-lg"></i></a>';
			$row_button = array(
				'data' => $detail
			);

			$this->table->add_row(
				$row_no,
				$row_nip,
				$row_nama,
				$row_periode,
				$row_cabang,
				$row_divisi,
				$row_posisi,
				$row_upah_pokok,
				$row_tunj_jabatan,
				$row_pengh_masa_kerja,
				$row_tunj_tdk_tetap,
				$row_tunj_kemahalan,
				$row_ins_presensi,
				$row_button
		    );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}

	function laporan_gaji($periode,$grouplokasi,$lokasi)
	{
		$this->Main_Model->all_login();
		$q = $this->Gaji_Model->view_gaji($periode,$lokasi,$grouplokasi);
		$tabel = '<table border="1px">
					<tr>
						<th>No</th>
						<th>NIP</th>
						<th>Nama</th>
						<th>Posisi</th>
						<th>Total THP Gross</th>
						<th>Benefit</th>
						<th>Total Potongan</th>
						<th>THP</th>
					</tr>';
		$no=1;
		foreach ($q as $row) {
		$periode = $row->bln;
		$tabel .= 	'<tr>
						<td>'.$no++.'</td>
						<td>'.$row->nip.'</td>
						<td>'.$row->nama.'</td>
						<td>'.$row->posisi.'</td>
						<td>'.$row->gross.'</td>
						<td>'.$row->tass.'</td>
						<td>'.$row->potongan.'</td>
						<td>'.$row->thp.'</td>
					</tr>';
		}
		$tabel .='</table>';
		$title = 'Laporan Gaji '.$periode.' - '.$lokasi;	
		$data = array(
			'tabel' => $tabel,
			'title' => $title
			);
		$this->load->view('gaji/export_gaji',$data);
	}

	function gaji_nip($nip='')
	{
		$this->Main_Model->all_login();
		$nip_quote = "'".$nip."'";
		$javascript = '
				<script>
					function load_table(){
						var nip = {"nip":'.$nip_quote.'}
						$.ajax({
							url 	: "'.base_url('gaji/view_gaji_nip').'",
							data 	: nip,
							type 	: "POST",
							success : function(data){
								$("#myTable").html(data);
								$("#dataTables-example").DataTable({
				        			responsive: true
				    			});
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								bootbox.alert("Internal Server Error");
							} 
						});
					}
					load_table();

					function lihat_detail(nip, id_per)
				    {
				    	$.ajax({
				    		url 	: "'.base_url('gaji/payrol_detail_hist').'/"+nip+"/"+id_per,
				    		success	: function(data){
				    			$("#payroll").modal();
				    			$("#target_modal").html(data);
				    		},
				    		error 	: function(jqXHR,textStatus,errorThrown){
				    			'.$this->Main_Model->notif500().'
				    		}
				    	});
				    }

				</script>
					';
		$footer = array(
			'javascript' => $javascript,
			'js' => $this->footer()
			);
		$header = array(
			'style' => $this->header(),
			'menu' => $this->Main_Model->menu_admin('0','0','69')
		);
		$this->load->view('template/header',$header);
		$this->load->view('gaji/gaji_nip');
		$this->load->view('template/footer',$footer);
	}

	function view_gaji_nip()
	{
		// $this->Main_Model->all_login();
		$nip 	= $this->input->post('nip');
		$data 	= $this->Gaji_Model->view_gaji_nip($nip);

		$template = array(
                'table_open'            => '<table class="table table-striped table-bordered table-hover dt-responsive" id="dataTables-example">',

                'thead_open'            => '<thead>',
                'thead_close'           => '</thead>',

                'heading_row_start'     => '<tr>',
                'heading_row_end'       => '</tr>',
                'heading_cell_start'    => '<th>',
                'heading_cell_end'      => '</th>',

                'tbody_open'            => '<tbody>',
                'tbody_close'           => '</tbody>',

                'row_start'             => '<tr>',
                'row_end'               => '</tr>',
                'cell_start'            => '<td>',
                'cell_end'              => '</td>',

                'row_alt_start'         => '<tr>',
                'row_alt_end'           => '</tr>',
                'cell_alt_start'        => '<td>',
                'cell_alt_end'          => '</td>',

                'table_close'           => '</table>'
        );

		$detail_gross 		= array('data'=>'Detail Gross','class'=>'none');
		$detail_tunjangan	= array('data'=>'Detail Tunjangan','class'=>'none');
		$detail_potongan 	= array('data'=>'Detail Potongan','class'=>'none');
		$detail_thp 		= array('data'=>'Detail THP','class'=>'none');

		$this->table->set_heading('No','Periode','Gross','Tunjangan','Potongan','THP','Action');
		$no=1;		

		foreach ($data as $row) {
		$gross 		= $row->gd+$row->td+$row->tpj+$row->tk+$row->tops+$row->tprof;
		$tunjangan	= $row->tlembur+$row->bbm+$row->bbp+$row->bb+$row->btw+$row->ttk+$row->tks+$row->tunj_lain;
		$potongan 	= $row->ptk+$row->pks+$row->pot_ags+$row->pjab+$row->pl+$row->pot_telat+$row->pot_mangkir;

		$this->table->add_row(
			$no++,
			$row->periode,
			$this->Main_Model->uang($gross),
			$this->Main_Model->uang($tunjangan),
			$this->Main_Model->uang($potongan),
			$this->Main_Model->uang($row->thp),
			'<button class="btn btn-primary btn-xs" onclick="lihat_detail('.$row->nip.','.$row->id_per.')" >Detail</button>'
	        );
		}

		$this->table->set_template($template);
		echo $this->table->generate();
	}	

	function slip_gaji_nip($nip,$periode)
	{
		$this->Main_Model->all_login();

		$data = array(
			'kary' => $this->Main_Model->kary_nip($nip),
			'posisi' => $this->Main_Model->posisi($nip),
			'slip' => $this->Gaji_Model->slip_gaji($nip,$periode)
			);

		$this->load->view('gaji/cetak_gaji',$data);

		$html = $this->output->get_output();
    	$this->load->library('Dompdf_gen');
    	$this->dompdf->load_html($html);
    	$this->dompdf->set_paper("a5", "landscape" ); 
    	$this->dompdf->render();
    	$this->dompdf->stream("Slip Gaji.pdf",array('Attachment'=>0));
	}

	function tutup_periode()
	{
		$this->Main_Model->all_login();
		$tahun = $this->input->post('tahun');
		$bulan = $this->input->post('bulan');

		$p = periode($tahun, $bulan);
		$periode = isset($p->qd_id) ? $p->qd_id : '';

		$data = array('tutup' => 1);
		$this->Gaji_Model->tutup_periode($periode, $data);
	}
}