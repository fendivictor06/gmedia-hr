<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Karyawan extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', true);
        $this->load->model('Karyawan_Model', '', true);
        $this->load->model('Penggajian_Model', '', true);
        $this->load->model('Informasi_Model', '', true);
        $this->load->model('Sto_Model', '', true);
        $this->load->model('Rekrutmen_Model', '', true);
    }
    
    function header()
    {
        $menu = '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">';
        return $menu;
    }
    
    function footer()
    {
        $footer = '
				<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') . '"></script>
				<script src="' . base_url('assets/plugins/jquery-validation/js/jquery.validate.min.js') . '" type="text/javascript"></script>
        		<script src="' . base_url('assets/plugins/jquery-validation/js/additional-methods.min.js') . '" type="text/javascript"></script>
        		<script src="' . base_url('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js') . '" type="text/javascript"></script>';
        
        return $footer;
    }
    
    function datakaryawan()
    {
        $this->Main_Model->get_login();
        $javascript = '
			<script>
				'.$this->Main_Model->default_datepicker().'
				function option() {
					var control = $("#control").val();
					var opt = {"opt":control}
					$.ajax({
						url : "' . base_url('karyawan/option') . '",
						type : "GET",
						data : opt,
						success : function(data){
							$("#option").html(data);
						},
						error : function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal mengambil data");
						}
					});
				}

				/* function cari() {
					var cabang = $("#cabang").val();
					dat = {
						"cabang" : cabang
					}

					$.ajax({
						url : "'.base_url('karyawan/search_karyawan').'",
						type : "GET",
						data : dat,
						beforeSend : function() {
							App.blockUI({
			                    boxed: !0
			                });
						},
						complete : function() {
							App.unblockUI();
						},
						success : function(data){
							$("#myTable").html(data);
							$("#resign").DataTable({
        						responsive : true,
        						stateSave : true
    						});
							$("#active").DataTable({
        						responsive : true,
        						stateSave : true
    						});
							$("#mutasi").DataTable({
        						responsive : true,
        						stateSave : true
    						});
						},
						error : function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal Mengambil Data!");
						}
					});
				} */

				$("#cabang").change(function(){
					
				});

				$(document).ready(function(){
	                var tableActive = $("#active").DataTable({
					    processing : true,
					    serverSide : true,
					    ajax : "'.base_url('karyawan/dt_karyawan_aktif').'",
					    stateSave: true,
					    language: {
					        aria: {
					            sortAscending: ": activate to sort column ascending",
					            sortDescending: ": activate to sort column descending"
					        },
					        emptyTable: "No data available in table",
					        info: "Showing _START_ to _END_ of _TOTAL_ entries",
					        infoEmpty: "No entries found",
					        infoFiltered: "(filtered1 from _MAX_ total entries)",
					        lengthMenu: "_MENU_ entries",
					        search: "Search:",
					        zeroRecords: "No matching records found",
					        processing: "<i class=\"fa fa-spinner fa-spin fa-3x fa-fw\" style=\"font-size:36px;\"></i><span class=\"sr-only\"></span>"
					    },
					    buttons: [{
					        extend: "print",
					        className: "btn dark btn-outline"
					    }, {
					        extend: "pdf",
					        className: "btn green btn-outline"
					    }, {
					        extend: "excel",
					        className: "btn yellow btn-outline "
					    }, {
					        extend: "colvis",
					        className: "btn dark btn-outline",
					        text: "Columns"
					    }],
					    responsive: !0,
					    order: [
					        [0, "asc"]
					    ],
					    lengthMenu: [
					        [5, 10, 15, 20, -1],
					        [5, 10, 15, 20, "All"]
					    ],
					    pageLength: 10
					});
				});
				

				function download() {
					var cabang = $("#cabang").val();
					var status = $("#pilihan").val();

					// window.location.href="' . base_url('karyawan/download_karyawan') . '/"+status+"/"+cabang;
					window.location.href="' . base_url('download_excel/download_karyawan_aktif') . '/"+status+"/"+cabang;
				}

				function resign(nip) {
					$("#myModal").modal();
					$("#nip").val(nip);
					$("#tgl").val("");
				}

				$("#form_resign").submit(function(event){
				  	event.preventDefault();
				  	var formData = new FormData($(this)[0]);
				 
				  	$.ajax({
				    	url: "'.base_url('karyawan/resign').'",
				    	type: "post",
				    	data: formData,
				    	async: false,
				    	cache: false,
				    	contentType: false,
				    	processData: false,
				    	success: function (returndata) {
				      		'.$this->Main_Model->notif_action('"Success!"', 'OK', 'blue', 'location.reload();').'
				    	}
				  	});
				  	return false;
				});

				function delete_data(nip) {
			    	nip = {
			    		"nip" : nip
			    	}
			    	bootbox.dialog({
			    		message : "Yakin ingin menghapus data?",
			    		title : "Hapus Data",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('karyawan/delete_karyawan') . '",
							    		type : "POST",
							    		data : nip,
							    		success : function(data){
							    			bootbox.alert({
												message: "Delete Success",
												size: "small"
											});
							    			cari();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})
			    }

			    function reset_password(nip) {
			    	bootbox.dialog({
			    		message : "Yakin ingin mereset password?",
			    		title : "Reset Password",
			    		buttons :{
			    			danger : {
			    				label : "Reset",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('karyawan/reset_password') . '/"+nip,
							    		type : "POST",
							    		success : function(data){
							    			bootbox.alert({
												message: "Password telah direset",
												size: "small"
											});
							    			cari();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Terjadi kesalahan");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})
			    }
			</script>
		';

        $opt_cabang = [];
        $arr_cabang = $this->Main_Model->view_by_id('ms_cabang', ['status' => 1], 'result');
        if (! empty($arr_cabang)) {
            $opt_cabang[0] = 'Semua Cabang';
            foreach ($arr_cabang as $row) {
                $opt_cabang[$row->id_cab] = $row->cabang;
            }
        }


        $footer = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );

        $data = array(
            'cabang' => $opt_cabang
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/karyawan', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function cari_sns($id = '')
    {
        $id_sto = $this->input->post("id_sto");
        $idp    = $this->input->post("idp");
        
        $sal_pos = $this->Karyawan_Model->sal_pos($id_sto);
        if ($id) {
            $select = '
				<div class="form-group">
					<label class="control-label col-md-3">Gaji Dasar <span class="required"> * </span></label>
						<div class="col-md-4"><input type="text" name="gd" id="gd" class="form-control mask_number" value="0"></div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Tunjangan Profesional </label>
						<div class="col-md-4"><input type="text" name="tprof" id="tprof" class="form-control mask_number" value="0"></div>
				</div>
				<div class="form-group">
					<label class="control-label col-md-3">Band <span class="required"> * </span></label>
						<div class="col-md-4"><select class="form-control" id="band" name="band">
			';
            $band   = $this->Karyawan_Model->cari_band($sal_pos->sal_pos, $idp);
            foreach ($band as $row) {
                $select .= '<option value="' . $row->id_band . '">' . $row->band . '</option>';
            }
            $select .= '</select></div></div>
					<div class="form-group">
						<label class="control-label col-md-3">Status Jabatan <span class="required"> * </span></label>
						<div class="col-md-4">
							<select name="sjab" id="sjab" class="form-control">
								<option value="FULL">FULL</option>
								<option value="PJ">PJ</option>
								<option value="TRAINING">TRAINING</option>
							</select>
						</div>
					</div>';
        } else {
            $select = '
				<div class="form-group">
					<label>Gaji Dasar</label>
						<input type="text" name="gd" id="gd" class="form-control">
				</div>
				<div class="form-group">
					<label>Tunjangan Profesional</label>
						<input type="text" name="tprof" id="tprof" class="form-control">
				</div>
				<div class="form-group">
					<label>Band</label>
						<select class="form-control" id="band" name="band">
			';
            $band   = $this->Karyawan_Model->cari_band($sal_pos->sal_pos, $idp);
            foreach ($band as $row) {
                $select .= '<option value="' . $row->id_band . '">' . $row->band . '</option>';
            }
            $select .= '</select></div>
					<div class="form-group">
						<label>Status Jabatan</label>
							<select name="sjab" id="sjab" class="form-control">
								<option value="FULL">FULL</option>
								<option value="PJ">PJ</option>
								<option value="TRAINING">TRAINING</option>
							</select>
					</div>';
        }
        
        
        echo $select;
    }
    
    function lokasi($id = '')
    {
        $idp    = $this->input->post('idp');
        $data   = $this->Karyawan_Model->lokasi($idp);

        if ($data) {
            $select = form_dropdown('selectarea', $data, '', 'id="selectarea" class="form-control" onchange="cari_jab();"');
            
            if ($id) {
                echo '<label class="control-label col-md-3">Area<span class="required"> * </span></label><div class="col-md-4">'.$select.'</div>';
            } else {
                echo "<label>Area</label>".$select;
            }
        } else {
            echo "";
        }
    }
    
    function cari_jab($id = '')
    {
        $idp    = $this->input->post('idp');
        $lokasi = $this->input->post('lokasi');
        $option = $this->Karyawan_Model->cari_jab($idp, $lokasi);

        if ($option) {
            if ($id) {
                $result = '<label class="control-label col-md-3">Posisi-Jabatan<span class="required"> * </span></label>
							<div class="col-md-4"><select class="select2" style="width:100%;" id="posisi" name="posisi" onchange="sns(this.value);">';
                foreach ($option as $row) {
                    $result .= '<option value="' . $row->id_sto;
                    
                    if ($row->sns == "STAFF") {
                        $result .= '-1"';
                    } else {
                        $result .= '-0"';
                    }
                    $result .= '>' . $row->id_stob_pos . ' - ' . $row->jab . ' - ' . $row->posisi . '</option>';
                }
                $result .= '</select></div>';
            } else {
                $result = '<label>Posisi-Jabatan</label>
							<select class="form-control" style="width:100%;" id="posisi" name="posisi" onchange="sns(this.value);">';
                foreach ($option as $row) {
                    $result .= '<option value="' . $row->id_sto;
                    
                    if ($row->sns == "STAFF") {
                        $result .= '-1"';
                    } else {
                        $result .= '-0"';
                    }
                    $result .= '>' . $row->id_stob_pos . ' - ' . $row->jab . ' - ' . $row->posisi . '</option>';
                }
                $result .= '</select>';
            }
        } else {
            $result = '';
        }
        
        echo $result;
    }
    
    function option()
    {
        $this->Main_Model->get_login();
        $opt = $this->input->get('opt');
        if ($opt == 1) {
            $option = "";
        } elseif ($opt == 2) {
            $result = $this->Karyawan_Model->lwok();
            $option = '
			<div class="form-group">
				<label>Lokasi</label>
					<select class="form-control" id="lokasi">';
            foreach ($result->result() as $row) {
                $option .= '<option value="' . $row->id_lwok . '">' . $row->wok . '</option>';
            }
            $option .= '</select></div>';
        } elseif ($opt == 3) {
            $result = $this->Karyawan_Model->lku();
            $option = '
			<div class="form-group">
				<label>Lokasi</label>
					<select class="form-control" id="lokasi">';
            foreach ($result->result() as $row) {
                $option .= '<option value="' . $row->id_lku . '">' . $row->lku . '</option>';
            }
            $option .= '</select></div>';
        } elseif ($opt == 4) {
            $result = $this->Karyawan_Model->lka();
            $option = '
			<div class="form-group">
				<label>Lokasi</label>
					<select class="form-control" id="lokasi">';
            foreach ($result->result() as $row) {
                $option .= '<option value="' . $row->id_lka . '">' . $row->lka . '</option>';
            }
            $option .= '</select></div>';
        } else {
            $option = "";
        }
        
        echo $option;
    }
    
    function search_karyawan()
    {
        $this->Main_Model->get_login();
        $cabang = $this->input->get('cabang');

        $result = $this->Karyawan_Model->search_karyawan($cabang);

        $active = isset($result['active']) ? $result['active'] : '';
        $resign = isset($result['resign']) ? $result['resign'] : '';
        $mutasi = isset($result['mutasi']) ? $result['mutasi'] : '';

        $return = $this->tabel_karyawan($active, $resign, $mutasi);
     
        echo $return;
    }
    
    function download_karyawan($status = '', $cabang = 0)
    {
        $this->Main_Model->get_login();

        $result = $this->Karyawan_Model->search_karyawan($cabang);
        $title = 'All Karyawan';
        $active = isset($result['active']) ? $result['active'] : '';
        $resign = isset($result['resign']) ? $result['resign'] : '';
        $mutasi = isset($result['mutasi']) ? $result['mutasi'] : '';
        $data = $this->download_tabel_karyawan($active, $resign, $title, $status, $mutasi);

        echo $data;
    }

    function jumlah_anak($nip = '')
    {
        $status = $this->Main_Model->view_by_id('kary', ['nip' => $nip], 'row');
        $mariage = isset($status->mar_stat) ? $status->mar_stat : 'LAJANG';

        $kk = [];
        if ($mariage != 'LAJANG') {
            $kk = $this->Main_Model->view_by_id('kk', ['nip' => $nip, 'status' => 'Anak'], 'result');
        }

        return count($kk);
    }
    
    function download_tabel_karyawan($active, $resign, $title, $status, $mutasi)
    {
        $this->Main_Model->get_login();
        $tbl_active = '<h3>Data Karyawan Aktif</h3><table border="1"><thead><tr>
							<th>NIP</th>
							<th>PIN</th>
							<th>NAMA</th>
							<th>DEPARTEMEN</th>
							<th>CABANG</th>
							<th>DIVISI</th>
							<th>LEVEL</th>
							<th>JABATAN</th>
							<th>STATUS</th>
							<th>TGL MASUK</th>
							<th>MASA KERJA</th>
							<th>JK</th>
							<th>NO KTP</th>
							<th>AGAMA</th>
							<th>GOLONGAN DARAH</th>
							<th>TTL</th>
							<th>USIA</th>
							<th>NO TELP</th>
							<th>ALAMAT</th>
							<th>EMAIL</th>
							<th>STATUS PERNIKAHAN</th>
							<th>NO REKENING</th>
							<th>BANK</th>
							<th>NAMA REKENING</th>
							<th>STATUS REKENING</th>
							<th>PENDIDIKAN</th>
							<th>NAMA SEKOLAH/UNIVERSITAS</th>
							<th>JURUSAN</th>
							<th>TH LULUS</th>
							<th>IJAZAH</th>
							<th>SURAT KOMITMEN</th>
							<th>NPWP</th>
							<th>EMERGENCY CALL</th>
							<th>IKUT BPJS</th>
							<th>JUMLAH ANAK</th>
						</tr></thead><tbody>';

        foreach ($active as $row) {
            $gend = ($row->gend == "L") ? 'Laki-laki' : 'Perempuan';
            $id_pend = $row->pendidikan;
            $ikut_asuransi = ($row->ikut_asuransi == "1") ? "Ya" : "Tidak";
            ($row->t_lahir) ? $ttl = $row->t_lahir . ', '.$this->Main_Model->tanggal($row->tgl_lahir) : $ttl = $this->Main_Model->tanggal($row->tgl_lahir);
            $now = date("Y-m-d");
            $usia = $this->Main_Model->Get_Date_Difference($row->tgl_lahir, $now);
            if ($row->tgl_masuk != '') {
                $masa       = $this->Main_Model->Get_Date_Difference($row->tgl_masuk, $now);
                $masa_kerja = $masa['year'].' Tahun '.$masa['month']. ' Bulan';
            } else {
                $masa_kerja = '';
            }

            ($row->contact_emergency != '') ? $emergency = $row->telp_emergency.'('.$row->contact_emergency.')' : $emergency = $row->contact_emergency;

            $tbl_active .= '<tr>
					<td>'.$row->nip.'</td>
					<td>'.$row->pin.'</td>
					<td>'.$row->nama.'</td>
					<td>'.$row->departemen.'</td>
					<td>'.$row->cabang.'</td>
					<td>'.$row->divisi.'</td>
					<td>'.$row->sal_pos.'</td>
					<td>'.$row->jab.'</td>
					<td>'.$row->kary_stat.'</td>
					<td>'.$row->tgl_masuk.'</td>
					<td>'.$masa_kerja.'</td>
					<td>'.$gend.'</td>
					<td>'.(string)$row->ktp.'</td>
					<td>'.$row->agama.'</td>
					<td>'.$row->golongan_darah.'</td>
					<td>'.$ttl.'</td>
					<td>'.$usia['year'].' Tahun '.$usia['month'].' Bulan</td>
					<td>'.$row->telp.'</td>
					<td>'.$row->alamat . '</td>
					<td>'.$row->mail . '</td>
					<td>'.$row->mar_stat . '</td>
					<td>'.$row->norek . '</td>
					<td>' . $row->bank . '</td>
					<td>' . $row->norek_an . '</td>
					<td>' . $row->tfinfo . '</td>
					<td>' . $id_pend . '</td>
					<td>' . $row->nama_sklh . '</td>
					<td>' . $row->jurusan . '</td>
					<td>' . $row->th_lulus . '</td>
					<td>'.$row->no_ijazah.'</td>
					<td>'.$row->surat_komitmen.'</td>
					<td>' . $row->npwp . '</td>
					<td>' . $emergency . '</td>
					<td>' . $ikut_asuransi . '</td>
					<td>'.$this->jumlah_anak($row->nip).'</td>
				</tr>';
        }
        $tbl_active .= '</tbody></table>';
        
        $tbl_resign = '<h3>Data Karyawan Resign</h3><table border="1"><thead><tr>
							<th>NIP</th>
							<th>PIN</th>
							<th>NAMA</th>
							<th>DEPARTEMEN</th>
							<th>CABANG</th>
							<th>DIVISI</th>
							<th>LEVEL</th>
							<th>JABATAN</th>
							<th>STATUS</th>
							<th>TGL MASUK</th>
							<th>TGL RESIGN</th>
							<th>MASA KERJA</th>
							<th>REHIRE/DO NOT REHIRE</th>
							<th>ALASAN RESIGN</th>
							<th>JK</th>
							<th>NO KTP</th>
							<th>AGAMA</th>
							<th>GOLONGAN DARAH</th>
							<th>TTL</th>
							<th>USIA</th>
							<th>NO TELP</th>
							<th>ALAMAT</th>
							<th>EMAIL</th>
							<th>STATUS PERNIKAHAN</th>
							<th>NO REKENING</th>
							<th>BANK</th>
							<th>NAMA REKENING</th>
							<th>STATUS REKENING</th>
							<th>PENDIDIKAN</th>
							<th>NAMA SEKOLAH/UNIVERSITAS</th>
							<th>JURUSAN</th>
							<th>TH LULUS</th>
							<th>IJAZAH</th>
							<th>SURAT KOMITMEN</th>
							<th>NPWP</th>
							<th>EMERGENCY CALL</th>
							<th>IKUT BPJS</th>
							<th>JUMLAH ANAK</th>
						</tr></thead><tbody>';

        foreach ($resign as $row) {
            ($row->gend=="L") ? $gend='Laki-laki' : $gend='Perempuan';
            $id_pend = $row->pendidikan;
            ($row->ikut_asuransi=="1") ? $ikut_asuransi="Ya" : $ikut_asuransi="Tidak";
            ($row->t_lahir) ? $ttl = $row->t_lahir . ', '.$this->Main_Model->tanggal($row->tgl_lahir) : $ttl = $this->Main_Model->tanggal($row->tgl_lahir);
            
            $now = date("Y-m-d");
            $usia = $this->Main_Model->Get_Date_Difference($row->tgl_lahir, $now);
            if ($row->tgl_masuk != '' && $row->tgl_resign != '') {
                $masa       = $this->Main_Model->Get_Date_Difference($row->tgl_masuk, $row->tgl_resign);
                $masa_kerja = $masa['year'].' Tahun '.$masa['month']. ' Bulan';
            } else {
                $masa_kerja = '';
            }

            ($row->contact_emergency != '') ? $emergency = $row->telp_emergency.'('.$row->contact_emergency.')' : $emergency = $row->contact_emergency;

            $tbl_resign .= '<tr>
					<td>' . $row->nip . '</td>
					<td>' . $row->pin . '</td>
					<td>' . $row->nama . '</td>
					<td>' . $row->departemen . '</td>
					<td>' . $row->cabang . '</td>
					<td>' . $row->divisi . '</td>
					<td>' . $row->sal_pos . '</td>
					<td>' . $row->jab . '</td>
					<td>' . $row->kary_stat . '</td>
					<td>'.$row->tgl_masuk.'</td>
					<td>'.$row->tgl_resign.'</td>
					<td>' . $masa_kerja . '</td>
					<td>'.$row->rehire.'</td>
					<td>'.$row->alasan_resign.'</td>
					<td>' . $gend . '</td>
					<td>' .(string)$row->ktp . '</td>
					<td>'.$row->agama.'</td>
					<td>'.$row->golongan_darah.'</td>
					<td>' . $ttl . '</td>
					<td>' . $usia['year'].' Tahun '.$usia['month']. ' Bulan</td>
					<td>' . $row->telp . '</td>
					<td>' . $row->alamat . '</td>
					<td>' . $row->mail . '</td>
					<td>' . $row->mar_stat . '</td>
					<td>' . $row->norek . '</td>
					<td>' . $row->bank . '</td>
					<td>' . $row->norek_an . '</td>
					<td>' . $row->tfinfo . '</td>
					<td>' . $id_pend . '</td>
					<td>' . $row->nama_sklh . '</td>
					<td>' . $row->jurusan . '</td>
					<td>' . $row->th_lulus . '</td>
					<td>'.$row->no_ijazah.'</td>
					<td>'.$row->surat_komitmen.'</td>
					<td>' . $row->npwp . '</td>
					<td>' . $emergency . '</td>
					<td>' . $ikut_asuransi . '</td>
					<td>'.$this->jumlah_anak($row->nip).'</td>
				</tr>';
        }
        $tbl_resign .= '</tbody></table>';

        $tbl_mutasi = '<h3>Data Karyawan Mutasi</h3><table border="1"><thead><tr>
							<th>NIP</th>
							<th>PIN</th>
							<th>NAMA</th>
							<th>DEPARTEMEN</th>
							<th>CABANG</th>
							<th>DIVISI</th>
							<th>LEVEL</th>
							<th>JABATAN</th>
							<th>STATUS</th>
							<th>TGL MASUK</th>
							<th>MASA KERJA</th>
							<th>JK</th>
							<th>NO KTP</th>
							<th>AGAMA</th>
							<th>GOLONGAN DARAH</th>
							<th>TTL</th>
							<th>USIA</th>
							<th>NO TELP</th>
							<th>ALAMAT</th>
							<th>EMAIL</th>
							<th>STATUS PERNIKAHAN</th>
							<th>NO REKENING</th>
							<th>BANK</th>
							<th>NAMA REKENING</th>
							<th>STATUS REKENING</th>
							<th>PENDIDIKAN</th>
							<th>NAMA SEKOLAH/UNIVERSITAS</th>
							<th>JURUSAN</th>
							<th>TH LULUS</th>
							<th>IJAZAH</th>
							<th>SURAT KOMITMEN</th>
							<th>NPWP</th>
							<th>EMERGENCY CALL</th>
							<th>IKUT BPJS</th>
							<th>JUMLAH ANAK</th>
						</tr></thead><tbody>';

        foreach ($mutasi as $row) {
            ($row->gend=="L") ? $gend='Laki-laki' : $gend='Perempuan';
            $id_pend = $row->pendidikan;
            ($row->ikut_asuransi=="1") ? $ikut_asuransi="Ya" : $ikut_asuransi="Tidak";
            ($row->t_lahir) ? $ttl = $row->t_lahir . ', '.$this->Main_Model->tanggal($row->tgl_lahir) : $ttl = $this->Main_Model->tanggal($row->tgl_lahir);
        
            $now = date("Y-m-d");
            $usia = $this->Main_Model->Get_Date_Difference($row->tgl_lahir, $now);
            if ($row->tgl_masuk != '') {
                $masa       = $this->Main_Model->Get_Date_Difference($row->tgl_masuk, $now);
                $masa_kerja = $masa['year'].' Tahun '.$masa['month']. ' Bulan';
            } else {
                $masa_kerja = '';
            }

            ($row->contact_emergency != '') ? $emergency = $row->telp_emergency.'('.$row->contact_emergency.')' : $emergency = $row->contact_emergency;

            $tbl_mutasi .= '<tr>
					<td>' . $row->nip . '</td>
					<td>' . $row->pin . '</td>
					<td>' . $row->nama . '</td>
					<td>' . $row->departemen . '</td>
					<td>' . $row->cabang . '</td>
					<td>' . $row->divisi . '</td>
					<td>' . $row->sal_pos . '</td>
					<td>' . $row->jab . '</td>
					<td>' . $row->kary_stat . '</td>
					<td>'.$row->tgl_masuk.'</td>
					<td>' . $masa_kerja . '</td>
					<td>' . $gend . '</td>
					<td>' .(string)$row->ktp . '</td>
					<td>'.$row->agama.'</td>
					<td>'.$row->golongan_darah.'</td>
					<td>' . $ttl . '</td>
					<td>' . $usia['year'].' Tahun '.$usia['month']. ' Bulan</td>
					<td>' . $row->telp . '</td>
					<td>' . $row->alamat . '</td>
					<td>' . $row->mail . '</td>
					<td>' . $row->mar_stat . '</td>
					<td>' . $row->norek . '</td>
					<td>' . $row->bank . '</td>
					<td>' . $row->norek_an . '</td>
					<td>' . $row->tfinfo . '</td>
					<td>' . $id_pend . '</td>
					<td>' . $row->nama_sklh . '</td>
					<td>' . $row->jurusan . '</td>
					<td>' . $row->th_lulus . '</td>
					<td>'.$row->no_ijazah.'</td>
					<td>'.$row->surat_komitmen.'</td>
					<td>' . $row->npwp . '</td>
					<td>' . $emergency . '</td>
					<td>' . $ikut_asuransi . '</td>
					<td>'.$this->jumlah_anak($row->nip).'</td>
				</tr>';
        }
        $tbl_mutasi .= '</tbody></table>';

        if ($status==1) {
            $data = array('active'  => $tbl_active,'title'  => $title.' (Karyawan Aktif)');
        } elseif ($status==0) {
            $data = array('resign'  => $tbl_resign,'title'  => $title.' (Karyawan Resign)');
        } else {
            $data = array('mutasi'  => $tbl_mutasi,'title'  => $title.' (Karyawan Mutasi)');
        }
        

        $this->load->view('karyawan/export_karyawan', $data);
    }
    
    function tabel_karyawan($active = '', $resign = '', $mutasi = '')
    {
        $this->Main_Model->get_login();
        $now = date("Y-m-d");
        $tbl_active = '
        <ul class="nav nav-tabs">
            <li class="active"> <a href="#tab_1_1" data-toggle="tab"> Karyawan Aktif </a> </li>
            <li> <a href="#tab_1_2" data-toggle="tab"> Karyawan Resign </a> </li>
            <!-- <li> <a href="#tab_1_3" data-toggle="tab"> Karyawan Mutasi </a> </li> -->
        </ul>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="tab_1_1">                                  
        		<h3>Data Karyawan Aktif</h3>
        			<table class="table table-striped table-bordered table-hover table-checkable order-column" id="active">
						<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Posisi</th>
								<th>Cabang</th>
								<th>Tanggal Masuk</th>
								<th>Masa Kerja</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>';
        $i = 1;
        foreach ($active as $row) {
            $nip_quote = "'".$row->nip."'";
            $posisi = $this->Karyawan_Model->posisi($row->nip);
            $jabatan = isset($posisi->jab) ? $posisi->jab : '';
            $cabang = isset($posisi->cabang) ? $posisi->cabang : '';
            $tgl_masuk  = $this->Main_Model->format_tgl($row->tgl_masuk);
            if ($row->tgl_masuk != '') {
                $masa = $this->Main_Model->Get_Date_Difference($row->tgl_masuk, $now);
                $masa_kerja = $masa['year'].' Tahun '.$masa['month']. ' Bulan';
            } else {
                $masa_kerja = '';
            }

            $btn_user = '';
            $check_user = $this->Main_Model->check_user($row->nip);
            if ($check_user == true) {
                $btn_user = '<li> <a href="'.base_url('karyawan/form_user').'/'.$row->nip.'">Register User </a></li>';
            }

            $pemberkasan = $this->Karyawan_Model->check_pemberkasan($row->nip);
            ($pemberkasan) ? $status = 'CLEAR' : $status = 'ON PROGRESS';
            $tbl_active .= '<tr>
					<td>'.$i++.'</td>
					<td>'.$row->nip.'</td>
					<td>'.$row->nama.'</td>
					<td>'.$jabatan.'</td>
					<td>'.$cabang.'</td>
					<td>'.$tgl_masuk.'</td>
					<td>'.$masa_kerja.'</td>
					<!--<td>'.$status.'</td>-->
					<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu" style="max-height:200px;overflow:auto;">
				                	'.$btn_user.'
				                    <li> <a href="' . base_url('karyawan/bio_kary') . '/' . $row->nip . '">Biodata </a> </li>
				                    <li> <a href="javascript:;" onclick="reset_password(' . $nip_quote . ')">Reset Password </a> </li>
				                    <li> <a href="' . base_url('pemberkasan/data_pemberkasan') . '/' . $row->nip . '">Pemberkasan </a> </li>
				                    <li> <a href="' . base_url('absensi/presensi') . '/' . $row->nip . '">Presensi </a> </li>
				                    <li> <a href="' . base_url('absensi/absensi_nip') . '/' . $row->nip . '">Absensi </a> </li>
				                    <li> <a href="' . base_url('absensi/cuti') . '/' . $row->nip . '">Cuti </a> </li>
				                    <li> <a href="' . base_url('gaji/gaji_nip') . '/' . $row->nip . '">Gaji </a> </li>
				                    <li> <a href="' . base_url('informasi/kontrak_nip') . '/' . $row->nip . '">Status Kepegawaian </a> </li>
				                    <li> <a href="' . base_url('surat_peringatan/sp_nip') . '/' . $row->nip . '">Surat Peringatan </a> </li>
				                    <li> <a href="' . base_url('karyawan/sk') . '/' . $row->nip . '">Surat Keputusan </a> </li>
				                    <li> <a href="' . base_url('karyawan/bpjs') . '/' . $row->nip . '">BPJS </a> </li>
				                    <li> <a href="' . base_url('karyawan/form_kk') . '/' . $row->nip . '">Form Kartu Keluarga </a> </li>
				                    <li> <a href="javascript:;" onclick="resign(' . $nip_quote . ')">Resign </a> </li>
				                    <li> <a href="' . base_url('karyawan/biodata') . '/' . $row->nip . '">Update </a> </li>
				                    <!--<li> <a href="javascript:;" onclick="delete_data(' . $row->nip . ')">Delete </a> </li>-->
				                </ul>
				        </div>
					</td>
				</tr>';
        }
        $tbl_active .= '</tbody></table></div><div class="tab-pane fade" id="tab_1_2">';
        $tbl_resign = '
        	<h3>Data Karyawan Resign</h3>
        		<table class="table table-striped table-bordered table-hover table-checkable order-column" id="resign">
					<thead>
						<tr>
							<th>No</th>
							<th>NIP</th>
							<th>Nama</th>
							<th>Posisi</th>
							<th>Cabang</th>
							<th>Tanggal Masuk</th>
							<th>Tanggal Resign</th>
							<th>Masa Kerja</th>
							<th width="15%">Action</th>
						</tr>
					</thead>
					<tbody>';
        $j          = 1;
        foreach ($resign as $row) {
            $posisi = $this->Main_Model->sk_terakhir($row->nip);
            $jabatan = isset($posisi->jab) ? $posisi->jab : '';
            $cabang = isset($posisi->cabang) ? $posisi->cabang : '';

            if ($row->tgl_masuk != '') {
                $masa = $this->Main_Model->Get_Date_Difference($row->tgl_masuk, $row->tgl_resign);
                $masa_kerja = $masa['year'].' Tahun '.$masa['month']. ' Bulan';
            } else {
                $masa_kerja = '';
            }

            $tbl_resign .= '<tr>
					<td>'.$j++.'</td>
					<td>'.$row->nip.'</td>
					<td>'.$row->nama.'</td>
					<td>'.$jabatan.'</td>
					<td>'.$cabang.'</td>
					<td>'.$this->Main_Model->format_tgl($row->tgl_masuk).'</td>
					<td>'.$this->Main_Model->format_tgl($row->tgl_resign).'</td>
					<td>'.$masa_kerja.'</td>
					<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu" style="max-height:200px;overflow:auto;">
				                   	<li> <a href="' . base_url('karyawan/bio_kary') . '/' . $row->nip . '">Biodata </a> </li>
				                    <li> <a href="' . base_url('absensi/presensi') . '/' . $row->nip . '">Presensi </a> </li>
				                    <li> <a href="' . base_url('absensi/absensi_nip') . '/' . $row->nip . '">Absensi </a> </li>
				                    <li> <a href="' . base_url('absensi/cuti') . '/' . $row->nip . '">Cuti </a> </li>
				                    <li> <a href="' . base_url('gaji/gaji_nip') . '/' . $row->nip . '">Gaji </a> </li>
				                    <li> <a href="' . base_url('informasi/kontrak_nip') . '/' . $row->nip . '">Status Kepegawaian </a> </li>
				                    <li> <a href="' . base_url('surat_peringatan/sp_nip') . '/' . $row->nip . '">Surat Peringatan </a> </li>
				                    <li> <a href="' . base_url('karyawan/sk') . '/' . $row->nip . '">Surat Keputusan </a> </li>
 									<li> <a href="' . base_url('karyawan/bpjs') . '/' . $row->nip . '">BPJS </a> </li>
				                    <li> <a href="' . base_url('karyawan/form_kk') . '/' . $row->nip . '">Form Kartu Keluarga </a> </li>
				                    <li> <a href="' . base_url('karyawan/biodata') . '/' . $row->nip . '">Update </a> </li>
				                    <!--<li> <a href="javascript:;" onclick="delete_data(' . $row->nip . ')">Delete </a> </li>-->
				                </ul>
				        </div>
					</td>
				</tr>';
        }
        $tbl_resign .= '</tbody></table></div>';

        $tbl_mutasi = '<div class="tab-pane fade" id="tab_1_3"><h3>Data Karyawan Mutasi</h3>
        			<table class="table table-striped table-bordered table-hover table-checkable order-column" id="mutasi">
						<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Posisi</th>
								<th>Cabang</th>
								<th>Tanggal Masuk</th>
								<th>Masa Kerja</th>
								<th width="15%">Action</th>
							</tr>
						</thead>
						<tbody>';
        $k          = 1;
        foreach ($mutasi as $row) {
            $nip_quote = "'".$row->nip."'";
            $tgl_masuk = $this->Main_Model->format_tgl($row->tgl_masuk);
            $posisi = $this->Karyawan_Model->posisi($row->nip);
            $jabatan = isset($posisi->jab) ? $posisi->jab : '';
            $cabang = isset($posisi->cabang) ? $posisi->cabang : '';
            if ($row->tgl_masuk != '') {
                $masa = $this->Main_Model->Get_Date_Difference($row->tgl_masuk, $now);
                $masa_kerja = $masa['year'].' Tahun '.$masa['month']. ' Bulan';
            } else {
                $masa_kerja = '';
            }

            $pemberkasan = $this->Karyawan_Model->check_pemberkasan($row->nip);
            ($pemberkasan) ? $status = 'CLEAR' : $status = 'ON PROGRESS';
            $tbl_mutasi .= '<tr>
					<td>'.$k++.'</td>
					<td>'.$row->nip.'</td>
					<td>'.$row->nama.'</td>
					<td>'.$jabatan.'</td>
					<td>'.$cabang.'</td>
					<td>'.$tgl_masuk.'</td>
					<td>'.$masa_kerja.'</td>
					<td>
						<div class="btn-group">
				            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
				                <i class="fa fa-angle-down"></i>
				            </button>
				                <ul class="dropdown-menu" role="menu" style="max-height:200px;overflow:auto;">
				                    <li> <a href="' . base_url('karyawan/bio_kary') . '/' . $row->nip . '">Biodata </a> </li>
				                    <li> <a href="javascript:;" onclick="reset_password(' . $nip_quote . ')">Reset Password </a> </li>
				                    <li> <a href="' . base_url('pemberkasan/data_pemberkasan') . '/' . $row->nip . '">Pemberkasan </a> </li>
				                    <li> <a href="' . base_url('absensi/presensi') . '/' . $row->nip . '">Presensi </a> </li>
				                    <li> <a href="' . base_url('absensi/absensi_nip') . '/' . $row->nip . '">Absensi </a> </li>
				                    <li> <a href="' . base_url('absensi/cuti') . '/' . $row->nip . '">Cuti </a> </li>
				                    <li> <a href="' . base_url('gaji/gaji_nip') . '/' . $row->nip . '">Gaji </a> </li>
				                    <li> <a href="' . base_url('informasi/kontrak_nip') . '/' . $row->nip . '">Status Kepegawaian </a> </li>
				                    <li> <a href="' . base_url('surat_peringatan/sp_nip') . '/' . $row->nip . '">Surat Peringatan </a> </li>
				                    <li> <a href="' . base_url('karyawan/sk') . '/' . $row->nip . '">Surat Keputusan </a> </li>
				                    <li> <a href="' . base_url('karyawan/bpjs') . '/' . $row->nip . '">BPJS </a> </li>
				                    <li> <a href="' . base_url('karyawan/form_kk') . '/' . $row->nip . '">Form Kartu Keluarga </a> </li>
				                    <li> <a href="javascript:;" onclick="resign(' . $nip_quote . ')">Resign </a> </li>
				                    <li> <a href="' . base_url('karyawan/biodata') . '/' . $row->nip . '">Update </a> </li>
				                    <!--<li> <a href="javascript:;" onclick="delete_data(' . $row->nip . ')">Delete </a> </li>-->
				                </ul>
				        </div>
					</td>
				</tr>';
        }
        $tbl_mutasi .= '</tbody></table></div></div><div class="clearfix margin-bottom-20"> </div>';
        
        echo $tbl_active;
        echo $tbl_resign;
        echo $tbl_mutasi;
    }
    
    function karyawanbaru()
    {
        $this->Main_Model->get_login();
        $list_sk = $this->Karyawan_Model->old_sk();
        $javascript = '
			<script>

				var FormWizard = function() {
				    return {
				        init: function() {
				            
				            if (jQuery().bootstrapWizard) {
				                
				                var r = $("#submit_form"),
				                    t = $(".alert-danger", r),
				                    i = $(".alert-success", r);
				                r.validate({
				                    doNotHideMessage: !0,
				                    errorElement: "span",
				                    errorClass: "help-block help-block-error",
				                    focusInvalid: !1,
				                    rules: {
				                        nama: {
				                            required: !0
				                        },
				                        ktp: {
				                            required: !0
				                        },
				                        t_lahir: {
				                            required: !0
				                        },
				                        tgl_lahir: {
				                            required: !0
				                        },
				                        gend : {
				                        	required: !0
				                        },
				                        mar_stat : {
				                        	required: !0
				                        },
				                        email: {
				                            email: !0
				                        },
				                        alamat: {
				                            required: !0
				                        },
				                        pend: {
				                            required: !0
				                        },
				                        nama_sklh: {
				                            required: !0
				                        },
				                        jurusan: {
				                            required: !0
				                        },
				                        th_lulus: {
				                            required: !0,
				                            maxlength: 4
				                        },
				                        ikut_asuransi: {
				                            required: !0
				                        },
				                        tgl_kerja : {
				                        	required: !0
				                        },
				                        //tgl_sk : {
				                        //	required: !0
				                        //},
				                        //tgl_awal_kontrak : {
				                        //	required: !0
				                        //},
				                        //tgl_akhir_kontrak : {
				                        //	required: !0
				                        //},
				                        idp : {
				                        	required: !0
				                        },
				                        selectarea : {
				                        	required: !0
				                        },
				                        posisi : {
				                        	required: !0
				                        },
				                        gd : {
				                        	required: !0
				                        },
				                        nip : {
				                        	required: !0
				                        },
				                        telp : {
				                        	required: !0
				                        },
				                        telp_emergency : {
				                        	required: !0
				                        },
				                        agama : {
				                        	required: !0
				                        },
				                        jab : {
				                        	required: !0
				                        },
				                        pin : {
				                        	required: !0
				                        }
				                    },
				                   
				                    errorPlacement: function(e, r) {
				                        "gender" == r.attr("name") ? e.insertAfter("#form_gender_error") : "payment[]" == r.attr("name") ? e.insertAfter("#form_payment_error") : e.insertAfter(r)
				                    },
				                    invalidHandler: function(e, r) {
				                        i.hide(), t.show(), App.scrollTo(t, -200)
				                    },
				                    highlight: function(e) {
				                        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
				                    },
				                    unhighlight: function(e) {
				                        $(e).closest(".form-group").removeClass("has-error")
				                    },
				                    success: function(e) {
				                        "gender" == e.attr("for") || "payment[]" == e.attr("for") ? (e.closest(".form-group").removeClass("has-error").addClass("has-success"), e.remove()) : e.addClass("valid").closest(".form-group").removeClass("has-error").addClass("has-success")
				                    },
				                    submitHandler: function(e) {
				                        i.show(), t.hide()
				                    }
				                });
				                var a = function() {
				                        $("#tab7 .form-control-static", r).each(function() {
				                            var e = $("[name=" + $(this).attr("data-display") + "]", r);
				                            if (e.is(":radio") && (e = $("[name=" + $(this).attr("data-display") + "]:checked", r)), e.is(":text") || e.is("textarea")) $(this).html(e.val());
				                            else if (e.is("select")) $(this).html(e.find("option:selected").text());
				                            else if (e.is(":radio") && e.is(":checked")) $(this).html(e.attr("data-title"));
				                            else if ("payment[]" == $(this).attr("data-display")) {
				                                var t = [];
				                                $("[name=payment[]]:checked", r).each(function() {
				                                    t.push($(this).attr("data-title"))
				                                }), $(this).html(t.join("<br>"))
				                            }
				                        })
				                    },
				                    o = function(e, r, t) {
				                        var i = r.find("li").length,
				                            o = t + 1;
				                        $(".step-title", $("#form_wizard_1")).text("Step " + (t + 1) + " of " + i), jQuery("li", $("#form_wizard_1")).removeClass("done");
				                        for (var n = r.find("li"), s = 0; t > s; s++) jQuery(n[s]).addClass("done");
				                        1 == o ? $("#form_wizard_1").find(".button-previous").hide() : $("#form_wizard_1").find(".button-previous").show(), o >= i ? ($("#form_wizard_1").find(".button-next").hide(), $("#form_wizard_1").find(".button-submit").show(), a()) : ($("#form_wizard_1").find(".button-next").show(), $("#form_wizard_1").find(".button-submit").hide()), App.scrollTo($(".page-title"))
				                    };
				                $("#form_wizard_1").bootstrapWizard({
				                    nextSelector: ".button-next",
				                    previousSelector: ".button-previous",
				                    onTabClick: function(e, r, t, i) {
				                        return !1
				                    },
				                    onNext: function(e, a, n) {
				                        return i.hide(), t.hide(), 0 == r.valid() ? !1 : void o(e, a, n)
				                    },
				                    onPrevious: function(e, r, a) {
				                        i.hide(), t.hide(), o(e, r, a)
				                    },
				                    onTabShow: function(e, r, t) {
				                        var i = r.find("li").length,
				                            a = t + 1,
				                            o = a / i * 100;
				                        $("#form_wizard_1").find(".progress-bar").css({
				                            width: o + "%"
				                        })
				                    }
				                }), $("#form_wizard_1").find(".button-previous").hide(), $("#form_wizard_1 .button-submit").click(function() {
				                    
				                }).hide()
				            }
				        }
				    }
				}();

				jQuery(document).ready(function() {
				    FormWizard.init()
				});

				$(".date-picker").datepicker({
                	rtl: App.isRTL(),
                	orientation: "left",
                	autoclose: !0
            	})

				function ceknip() {
					var nip = $("#nip").val();
					$.ajax({
						url : "'.base_url('karyawan/cek_nip').'/"+nip,
						dataType: "JSON",
						success : function(data){
							if (data.status=="true") {
								bootbox.alert(data.message) 
							}
						}
					});
				}

				function save() {
					$.ajax({
						url : "'.base_url('karyawan/add_karyawan').'",
						type : "POST",
						data : new FormData($("#submit_form")[0]),
						processData: false,
      					contentType: false,
      					dataType : "json",
						success : function(data){
							bootbox.dialog({
		                        message : "Sukses Menyimpan Data!",
		                        buttons : {
		                            main : {
		                                label : "OK",
		                                className : "blue",
		                                callback : function(){
		                                    location.href="'.base_url('karyawan/form_user').'/"+data.nip;
		                                    return true;
		                                }
		                            }
		                        }
		                    })
						},
						error 	: function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal Menyimpan data!");
						}
					});	
				}

				$(".mask_number").inputmask({
	                mask: "9",
	                repeat: 25,
	                greedy: !1
	            })

				function change_tipe(id) {
					var tipe = $("#tipe_sk").val();
					if(id == null) {
						if(tipe == 5) {
							form = "<label class=\"control-label col-md-3\">Awal Masuk Kerja <span class=\"required\"> * </span></label><div class=\"col-md-4\"><input type=\"text\" class=\"form-control date-picker\" data-date-format=\"dd/mm/yyyy\" name=\"tgl_kerja\" id=\"tgl_kerja\"></div>";				
						} else {
							form = "<label class=\"control-label col-md-3\">SK Sebelumnya</label><div class=\"col-md-4\"><select name=\"old_sk\" id=\"old_sk\" class=\"form-control\">';
                                // if($list_sk) {
                                //  foreach ($list_sk as $row) {
                                //      $javascript .='<option value=\"'.$row->id_sk.'\">'.$row->id_stob_pos.' - '.$row->posisi.'</option>';
                                //  }
                                // }
                                $javascript .=  '</select></div>";
						}
						$("#tipe_value").html(form);
					} else {
						if(tipe == 5) {
							form = "<label class=\"control-label col-md-3\">Awal Masuk Kerja <span class=\"required\"> * </span></label><div class=\"col-md-4\"><input type=\"text\" class=\"form-control date-picker\" data-date-format=\"dd/mm/yyyy\" name=\"tgl_kerja\" id=\"tgl_kerja\"></div>";
							$("#tipe_value").html(form);
						} else {
							$.ajax({
								url : "'.base_url('karyawan/cari_sk').'/"+id,
								dataType : "JSON",
								success : function(data){
									form = "<label class=\"control-label col-md-3\">SK Sebelumnya</label><div class=\"col-md-4\"><select name=\"old_sk\" id=\"old_sk\" class=\"form-control\"><option value="+data.id_sk+">"+data.id_stob_pos+"  "+data.posisi+"</option></select></div>";
									
									$("#tipe_value").html(form);
								},
								error : function(jqXHR,textStatus,errorThrown){
									'.$this->Main_Model->notif500().'
								}
							});
						}
					}

					'.$this->Main_Model->default_datepicker().'
				}

				$(document).ready(function(){
					change_tipe();
					cari_cabang();
				});

				'.$this->Main_Model->default_select2().'

				function cari_cabang(id_departemen) {
					$.ajax({
						url 	: "'.base_url('karyawan/cari_cabang').'/"+id_departemen,
						success : function(data){
							$("#cabang").val("").trigger("change");
							$("#cabang").html(data);
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							notif("Gagal Mengambil data!");
						}
					});
				}

				function cari_divisi(id_cabang) {
					id_departemen = $("#departemen").val();
					$.ajax({
						url 	: "'.base_url('karyawan/cari_divisi').'/"+id_cabang+"/"+id_departemen,
						success : function(data){
							$("#divisi").val("").trigger("change");
							$("#divisi").html(data);
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							notif("Gagal Mengambil data!");
						}
					});
				}

				function cari_jabatan(id_divisi) {
					id_departemen = $("#departemen").val();
					id_cabang 	  = $("#cabang").val();
					$.ajax({
						url 	: "'.base_url('karyawan/cari_jabatan').'/"+id_divisi+"/"+id_cabang+"/"+id_departemen,
						success : function(data){
							$("#jab").val("").trigger("change");
							$("#jab").html(data);
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							notif("Gagal Mengambil data!");
						}
					});
				}

				$("#jab").change(function(){
					var selected = $("#jab option:selected").text();
					var n = selected.search(/sales force/i);
					if(n > 0) {
						form = "<div class=\"form-group\"><label class=\"control-label col-md-3\">BPKB</label><div class=\"col-md-4\"><input type=\"text\" class=\"form-control\" name=\"bpkb\" id=\"bpkb\"></div></div><div class=\"form-group\"><label class=\"control-label col-md-3\">SIM A/C</label><div class=\"col-md-4\"><input type=\"text\" name=\"sim\" id=\"sim\" class=\"form-control\"></div></div>";
						$("#kolom_sales").html(form);
					} else {
						$("#kolom_sales").html("");
					}
				});

				// $(".bpjs").blur(function(){
				// 	if($("#no_bpjsks").val() != "") {
				// 		$("#alasan_bpjsks").attr("readonly", true);
				// 		$("#alasan_bpjsks").val("");
				// 	} else {
				// 		$("#alasan_bpjsks").attr("readonly", false);
				// 		//$("#alasan_bpjsks").val("");
				// 	}
                // });

                var generate_nip = function() {
                    var region = $("#region").val();
                    $.ajax({
                        url : "'.base_url().'karyawan/generate_nip/"+region,
                        dataType : "json",
                        success : function(data) {
                            $("#nip").val(data.nip);
                            $("#pin").val(data.pin);
                        },
                        error : function() {
                            toastr.danger("", "Terjadi Kesalahaan saat memuat data");
                        }
                    });
                }

                generate_nip();
                
                $("#region").change(function(){
                    generate_nip();
                });
    		</script>';
        $footer = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );

        $ms_gender = $this->Main_Model->view_by_id('ms_gender', [], 'result');
        $gender = [];
        if ($ms_gender) {
            foreach ($ms_gender as $row) {
                $gender[$row->kode] = $row->label;
            }
        }

        // $gender = array(
        //     'L' => 'Laki Laki',
        //     'P' => 'Perempuan'
        // );

        $level = array(
            'Entry' => 'Entry',
            'Pro' => 'Pro'
        );

        $ms_tfinfo = $this->Main_Model->view_by_id('ms_info_rekening', [], 'result');
        $tfinfo = [];
        if ($ms_tfinfo) {
            foreach ($ms_tfinfo as $row) {
                $tfinfo[$row->kode] = $row->label;
            }
        }

        // $tfinfo = array(
        //     '' => 'Status Rekening',
        //     'OWN' => 'Own',
        //     'TITIP' => 'Titip'
        // );

        $ms_marriage = $this->Main_Model->view_by_id('ms_marriage', [], 'result');
        $mar_stat = [];
        if ($ms_marriage) {
            foreach ($ms_marriage as $row) {
                $mar_stat[$row->kode] = $row->label;
            }
        }

        // $mar_stat = array(
        //     'LAJANG' => 'Lajang',
        //     'MENIKAH' => 'Menikah',
        //     'DUDA' => 'Duda',
        //     'JANDA' => 'Janda'
        // );

        $ikut_asuransi  = array(
            '0' => 'Tidak',
            '1' => 'Ya'
        );

        $ms_status = $this->Main_Model->view_by_id('ms_status_karyawan', [], 'result');
        $kary_stat = [];
        if ($ms_status) {
            foreach ($ms_status as $row) {
                $kary_stat[$row->kode] = $row->label;
            }
        }

        // $kary_stat = array(
        //     'KONTRAK' => 'Kontrak',
        //     'TETAP' => 'Tetap',
        //     'TRAINING' => 'Training'
        // );

        $ms_agama = $this->Main_Model->view_by_id('ms_agama', [], 'result');
        $agama = [];
        if ($ms_agama) {
            foreach ($ms_agama as $row) {
                $agama[$row->kode] = $row->label;
            }
        }

        // $agama = array(
        //     'ISLAM' => 'ISLAM',
        //     'KRISTEN' => 'KRISTEN',
        //     'KATHOLIK' => 'KATHOLIK',
        //     'HINDU' => 'HINDU',
        //     'BUDHA' => 'BUDHA'
        // );

        $idp = $this->session->userdata('idp');
        $dept = $this->Karyawan_Model->view_departemen($idp);
        $d[""] = "Pilih Departemen";

        if ($dept) {
            foreach ($dept as $row) {
                $d[$row->id_dept] = $row->departemen;
            }
        }

        $prefix = [];
        $ms_cabang = $this->Main_Model->view_by_id('ms_cabang', ['status' => 1], 'result');
        if ($ms_cabang) {
            foreach ($ms_cabang as $row) {
                $prefix[$row->prefix] = $row->cabang;
            }
        }

        $data = array(
            'gend' => $gender,
            'pend' => $this->Karyawan_Model->pendidikan(),
            'level' => $level,
            'tfinfo' => $tfinfo,
            'mar_stat' => $mar_stat,
            'lokasi_kerja' => $this->Karyawan_Model->lokasi_kerja(),
            'ikut_asuransi' => $ikut_asuransi,
            'kary_stat' => $kary_stat,
            'idp' => $this->Main_Model->opt_idp(),
            'tipe_sk' => $this->Karyawan_Model->tipe_sk_opt(5),
            'agama' => $agama,
            'departemen' => $d,
            'cabang' => $prefix,
            // 'nip_auto' => '03.'.$nip_auto.'.'.date('Y'),
            // 'pin' => $nip_auto
            'nip_auto' => '',
            'pin' => ''
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('karyawan/form_karyawan', $data);
        $this->load->view('template/footer', $footer);
    }

    function generate_nip($prefix = '')
    {
        $maks = $this->Karyawan_Model->generate_nip($prefix);
        if ($maks > 0) {
            if (strlen($maks) == 1) {
                $nip_auto = '00'.$maks;
            } elseif (strlen($maks) == 2) {
                $nip_auto = '0'.$maks;
            } else {
                $nip_auto = $maks;
            }
        } else {
            $nip_auto = '001';
        }

        echo json_encode(array(
            'nip' => $prefix.'.'.$nip_auto.'.'.date('Y'),
            'pin' => (int)$nip_auto
        ));
    }
    
    function cek_nip($nip = '')
    {
        $cek = $this->Main_Model->kary_nip($nip);
        if ($cek) {
            $json = array(
                        'status' => 'true',
                        'message' => 'NIP Sudah terdaftar !'
                    );
        } else {
            $json = array(
                        'status' => 'false',
                        'message' => ''
                    );
        }
        echo json_encode($json);
    }

    function idp_list()
    {
        $this->Main_Model->get_login();
        $idp = $this->input->get('idp');

        $opt = $this->Karyawan_Model->lku_idp($idp);
        foreach ($opt as $row) {
            $result[$row->id_lku] = $row->lku;
        }

        echo form_dropdown('lokasi_kerja', $result, '', 'id="lokasi_kerja" class="form-control"');
    }
    
    function add_karyawan()
    {
        $this->Main_Model->get_login();

        $nip = $this->input->post('nip');
        $nama = $this->input->post('nama');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $tgl_sk = $this->input->post('tgl_sk');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $tgl_kerja = $this->input->post('tgl_kerja');
        $tgl_awal_kontrak = $this->input->post('tgl_awal_kontrak');
        $tgl_akhir_kontrak = $this->input->post('tgl_akhir_kontrak');
        $no_bpjsks = $this->input->post('no_bpjsks');
        $salary = $this->input->post('salary');
        $status_pegawai = $this->input->post('status_pegawai');

        $tgl_lahir = $this->Main_Model->convert_tgl($tgl_lahir);
        // $tgl_sk = $this->Main_Model->convert_tgl($tgl_sk);
        $tgl_kerja = $this->Main_Model->convert_tgl($tgl_kerja);
        $tgl_awal = $this->Main_Model->convert_tgl($tgl_awal_kontrak);
        $tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir_kontrak);

        $file_name_sk = 'SK '.$nip.' '.date("d-m-Y");
        $config_sk = $this->Main_Model->set_upload_options("./assets/sk/", "*", "5048000", $file_name_sk);
        $this->load->library('upload', $config_sk, 'sk');

        // $this->upload->initialize($config_sk);
        if ($this->sk->do_upload('upload_sk')) {
            $upload_sk  = $this->sk->data();
        }

        // ($no_bpjsks != '') ? $ikut_asuransi = 1 : $ikut_asuransi = 0;
        $ikut_asuransi = $this->input->post('ikut_asuransi');
        if ($salary == '') {
            $salary = 0;
        }
        
        $data = array(
            'nip' => $this->input->post('nip'),
            'pin' => $this->input->post('pin'),
            'nama' => $this->input->post('nama'),
            'gend' => $this->input->post('gend'),
            'id_pend' => $this->input->post('pend'),
            'ktp' => $this->input->post('ktp'),
            'bank' => $this->input->post('nama_bank'),
            'norek' => $this->input->post('no_rek'),
            'norek_an' => $this->input->post('nama_akun'),
            'tfinfo' => $this->input->post('tfinfo'),
            'tgl_lahir' => $tgl_lahir,
            'telp' => $this->input->post('telp'),
            'telp_emergency' => $this->input->post('telp_emergency'),
            'contact_emergency' =>$this->input->post('contact_emergency'),
            'agama' => $this->input->post('agama'),
            'surat_komitmen' => $this->input->post('surat_komitmen'),
            'alamat' => $this->input->post('alamat'),
            'mail' => $this->input->post('email'),
            'mar_stat' => $this->input->post('mar_stat'),
            'ikut_asuransi' => $ikut_asuransi,
            't_lahir' => $this->input->post('t_lahir'),
            'nama_sklh' => $this->input->post('nama_sklh'),
            'jurusan' => $this->input->post('jurusan'),
            'th_lulus' => $this->input->post('th_lulus'),
            'npwp' => $this->input->post('npwp'),
            'idp' => $this->session->userdata('idp'),
            'th1' => $this->input->post('th_hist_penyakit_1'),
            'penyakit1' => $this->input->post('desc_penyakit_1'),
            'th2' => $this->input->post('th_hist_penyakit_2'),
            'penyakit2' => $this->input->post('desc_penyakit_2'),
            'th3' => $this->input->post('th_hist_penyakit_3'),
            'penyakit3' => $this->input->post('desc_penyakit_3'),
            'tgl_masuk' => $tgl_kerja,
            'no_ijazah' => $this->input->post('no_ijazah'),
            'status_ijazah' => $this->input->post('status_ijazah'),
            // 'file_surat_komitmen'=>$upload_surat_kom['file_name'],
            // 'file_bpjs_tk'   => $upload_file_bpjs_tk['file_name'],
            // 'file_bpjs_ks'   => $upload_file_bpjs_ks['file_name'],
            // 'file_sertifikat1'=> $upload_file_sertifikat1['file_name'],
            // 'file_sertifikat2'=> $upload_file_sertifikat2['file_name'],
            // 'file_sertifikat3'=> $upload_file_sertifikat3['file_name'],
            'no_bpjsks' => $this->input->post('no_bpjsks'),
            'no_bpjstk' => $this->input->post('no_bpjstk'),
            'golongan_darah' => $this->input->post('golongan_darah'),
            'alasan_bpjsks' => $this->input->post('alasan_bpjsks'),
            'salary' => $salary
        );

        $file_surat_kom = 'Surat Komitmen '.strtoupper($nama).' '.date("d-m-Y");
        $config_surat = $this->Main_Model->set_upload_options("./assets/surat_komitmen/", "*", "5048000", $file_surat_kom);
        $this->load->library('upload', $config_surat, 'surat_komitmen');

        // $this->upload->initialize($config_surat);
        if ($this->surat_komitmen->do_upload('file_surat_komitmen')) {
            $upload_surat_kom= $this->surat_komitmen->data();
            $data['file_surat_komitmen'] = $upload_surat_kom['file_name'];
        }

        $file_bpjs_tk = 'BPJS TK '.strtoupper($nama).' '.date("d-m-Y");
        $config_bpjs_tk = $this->Main_Model->set_upload_options("./assets/bpjs/", "*", "5048000", $file_bpjs_tk);
        $this->load->library('upload', $config_bpjs_tk, 'bpjs_tk');

        // $this->upload->initialize($config_bpjs_tk);
        if ($this->bpjs_tk->do_upload('file_bpjs_tk')) {
            $upload_file_bpjs_tk = $this->bpjs_tk->data();
            $data['file_bpjs_tk'] = $upload_file_bpjs_tk['file_name'];
        }

        $file_bpjs_ks = 'BPJS KS '.strtoupper($nama).' '.date("d-m-Y");
        $config_bpjs_ks = $this->Main_Model->set_upload_options("./assets/bpjs/", "*", "5048000", $file_bpjs_ks);
        $this->load->library('upload', $config_bpjs_ks, 'bpjs_ks');

        // $this->upload->initialize($config_bpjs_ks);
        if ($this->bpjs_ks->do_upload('file_bpjs_ks')) {
            $upload_file_bpjs_ks = $this->bpjs_ks->data();
            $data['file_bpjs_ks'] = $upload_file_bpjs_ks['file_name'];
        }

        // $file_ser1       = 'Sertifikat I '.$this->input->post('nip').' '.date("d-m-Y");
        // $config_ser1     = $this->Main_Model->set_upload_options("./assets/sertifikat/","*","5048000",$file_ser1);
        // $this->load->library('upload', $config_ser1, 'ser1');

        // $this->upload->initialize($config_ser1);
        // if($this->ser1->do_upload('file_sertifikat1'))
        // {
        //  $upload_file_sertifikat1 = $this->ser1->data();
        //  $data['file_sertifikat1'] = $upload_file_sertifikat1['file_name'];
        // }

        $file_ser2 = 'Sertifikat II '.strtoupper($nama).' '.date("d-m-Y");
        $config_ser2 = $this->Main_Model->set_upload_options("./assets/sertifikat/", "*", "5048000", $file_ser2);
        $this->load->library('upload', $config_ser2, 'ser2');

        // $this->upload->initialize($config_ser2);
        if ($this->ser2->do_upload('file_sertifikat2')) {
            $upload_file_sertifikat2 = $this->ser2->data();
            $data['file_sertifikat2'] = $upload_file_sertifikat2['file_name'];
        }

        $file_ser3 = 'Sertifikat III '.strtoupper($nama).' '.date("d-m-Y");
        $config_ser3 = $this->Main_Model->set_upload_options("./assets/sertifikat/", "*", "5048000", $file_ser3);
        $this->load->library('upload', $config_ser3, 'ser3');

        // $this->upload->initialize($config_ser3);
        if ($this->ser3->do_upload('file_sertifikat3')) {
            $upload_file_sertifikat3 = $this->ser3->data();
            $data['file_sertifikat3'] = $upload_file_sertifikat3['file_name'];
        }

        // updated at 15 05 2018
        $file_ijazah = 'Ijazah '.strtoupper($nama).' '.date('d-m-Y');
        $config_file_ijazah = $this->Main_Model->set_upload_options('./assets/sertifikat/', '*', '5048000', $file_ijazah);
        $this->load->library('upload', $config_file_ijazah, 'ijazah');

        if ($this->ijazah->do_upload('file_sertifikat1')) {
            $upload_file_ijazah = $this->ijazah->data();
            $data['file_ijazah'] = $upload_file_ijazah['file_name'];
        }

        $data_sk = array(
            'nip' => $this->input->post('nip'),
            'no_sk' => $this->input->post('no_sk'),
            'id_band' => $this->input->post('band'),
            'aktif' => 1,
            'gd' => $this->input->post('gd'),
            'tprof' => $this->input->post('tprof'),
            'stat_jab' => 'FULL',
            'id_pos_sto' => $this->input->post('jab'),
            'id_tipe_sk' => $this->input->post('tipe_sk'),
            'tgl_sk' => $tgl_kerja,
            'tgl_kerja' => $tgl_kerja,
            'file_sk' => isset($upload_sk['file_name']) ? $upload_sk['file_name'] : '',
            'bpkb' => $this->input->post('bpkb'),
            'sim' => $this->input->post('sim')
        );

        $last = $this->db->query("SELECT * FROM kont ORDER BY id_kontrak DESC")->row();
        $last_no_kontrak = isset($last->no_kontrak) ? $last->no_kontrak : 0;
        $maks = explode("-", $last_no_kontrak);
        $maks_new = isset($maks[0]) ? $maks[0] : 0;
        $maks_new = $maks_new + 1;
        $no_kontrak = $maks_new.'-'.$nip;

        $data_kont  = array(
            'no_kontrak' => $no_kontrak,
            'nip' => $nip,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'tipe' => $status_pegawai,
            'aktif' => 1
        );

        $cek = $this->Main_Model->kary_nip($this->input->post('nip'));
        if (empty($cek) && $tgl_awal != '' && $tgl_akhir != '' && $status_pegawai != '') {
            $this->Informasi_Model->add_kont($data_kont);
            $data['kary_stat'] = $status_pegawai;
        }

        $this->Karyawan_Model->add_sk($data_sk);
        $this->Karyawan_Model->add_karyawan($data);
        $this->Sto_Model->update_sto(array('id_stob_pos' => $this->input->post('pin')), $this->input->post('jab'));

        echo json_encode(array('nip' => $nip));
        // print_r($data_sk);

        // $this->session->set_flashdata('message_upload', '<script>bootbox.alert("Success !");</script>');
        // redirect('karyawan/karyawanbaru');
    }
    
    function dataposisi()
    {
        $this->Main_Model->get_login();
        $idp = $this->session->userdata('idp');
        $url_del = base_url('karyawan/delete_posisi');
        $javascript = '
			<script type="text/javascript">
				'.$this->Main_Model->default_select2()

                 .$this->Main_Model->notif().'

				function reset() {
					$(".select2").val("").trigger("change");
					$(".blank").val("");
				}

				function load_table() {
					var idp = {"idp" : ' . $idp . '}
					$.ajax({
						url : "' . base_url('karyawan/view_dataposisi') . '",
						type : "GET",
						data : idp,
						success : function(data){
							$("#myTable").html(data);
							$("#dataTables-example").DataTable({
								responsive : true
							});
						},
						error : function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal mengambil data!");
						}
					});
				}

				'.$this->Main_Model->post_data('add_posisi', 'save()', '$("#form_posisi").serialize()', '
					if(data.status=="true") {
						load_table();
						reset();
						$("#myModal").modal("toggle");
					}
					notif(data.message);
					').'
				
				function get_id(id){
			    	id_pos = {"id_pos" : id}
			    	$.ajax({
			    		url 	: "' . base_url('karyawan/view_posisi_id') . '",
			    		type 	: "GET",
			    		dataType: "JSON",
			    		data 	: id_pos,
			    		success : function(obj){
							$("#myModal").modal();
    						$("#jab").val(obj.jab).trigger("change");
							$("#id_pos").val(obj.id_pos);
							$("#mySelect").val(obj.sal_pos).trigger("change");
							$("#departemen").val(obj.id_departemen).trigger("change");
							$("#divisi").val(obj.id_divisi).trigger("change");
							$("#cabang").val(obj.id_cabang).trigger("change");
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			notif("Gagal mengambil data!");
			    		}
			    	});
			    }
			    '.$this->Main_Model->default_delete_data($url_del).'
				load_table();
			</script>
			';

        $footer     = array(
            'javascript' => minifyjs($javascript),
            'js' => $this->footer()
        );

        $departemen = $this->Karyawan_Model->view_departemen($idp);
        foreach ($departemen as $row) {
            $dept[$row->id_dept] = $row->departemen;
        }

        $cabang     = $this->Karyawan_Model->view_cabang($idp);
        foreach ($cabang as $row) {
            $cab[$row->id_cab] = $row->cabang;
        }

        $divisi     = $this->Karyawan_Model->view_divisi($idp);
        foreach ($divisi as $row) {
            $div[$row->id_divisi] = $row->divisi;
        }

        $condition = array('status' => 1);
        $job = $this->Main_Model->view_by_id('ms_jobdesk', $condition, 'result');
        $job_opt = array();
        if (!empty($job)) {
            foreach ($job as $row) {
                $job_opt[$row->jab] = $row->jab;
            }
        }

        $sal_pos = array();
        $sal = $this->Main_Model->view_by_id('sal_staff_class', array('status' => 1), 'result');
        if (! empty($sal)) {
            foreach ($sal as $row) {
                $sal_pos[$row->sal_pos] = $row->sal_pos;
            }
        }

        $data = array(
            'departemen' => $dept,
            'cabang' => $cab,
            'divisi' => $div,
            'jab' => $job_opt,
            'sal_pos' => $sal_pos
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '3')
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/dataposisi', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_dataposisi()
    {
        $this->Main_Model->get_login();
        $id = $this->session->userdata('idp');
        // $data = $this->Karyawan_Model->dataposisi($id);
        $data = $this->Sto_Model->view_posisi_jabatan($id);
        $template = $this->Main_Model->tbl_temp();

        // $this->table->set_heading('No','Jabatan','Posisi','Cabang','Divisi','Departemen','Action');
        $this->table->set_heading('No', 'NIP', 'Nama', 'Departemen', 'Cabang', 'Divisi', 'Jabatan', 'Posisi', 'Action');
        $no = 1;
        
        foreach ($data as $row) {
            $edit = '<li> <a href="javascript:;" onclick="get_id('.$row->id_pos.');"> <i class="icon-edit"></i> Update </a> </li>';
            $delete = ($row->nip == '-') ? '<li> <a href="javascript:;" onclick="delete_data('.$row->id_pos.');"> <i class="icon-delete"></i> Delete </a> </li>' : '';
            $this->table->add_row(
                $no++,
                $row->nip,
                $row->nama,
                $row->departemen,
                $row->cabang,
                $row->divisi,
                $row->sal_pos,
                $row->jab,
                '<div class="btn-group dropup">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        '.$edit.$delete.'
                    </ul>
            </div>'
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function view_salpos()
    {
        $this->Main_Model->get_login();
        $id  = $this->input->get('id');
        $idp = $this->input->get('idp');
        $val = $this->input->get('select');
        
        $option = $this->Karyawan_Model->view_salpos($id, $idp);
        
        $select = '<select class="form-control" id="sal_pos" name="sal_pos">';
        foreach ($option->result() as $row) {
            $select .= '<option ';
            if ($row->sal_pos == $val) {
                $select .= 'selected ';
            }
            $select .= 'value="' . $row->sal_pos . '">' . $row->sal_pos . '</option>';
        }
        $select .= '</select>';
        
        echo $select;
    }
    
    function add_posisi()
    {
        $this->Main_Model->get_login();
        $id_pos = $this->input->post('id_pos');
        $jab = $this->input->post('jab');
        $sal_pos = $this->input->post('mySelect');
        $departemen = $this->input->post('departemen');
        $cabang  = $this->input->post('cabang');
        $divisi = $this->input->post('divisi');
        $idp = $this->session->userdata('idp');

        if ($jab == "" || $sal_pos == "" || $departemen == "" || $cabang == "" || $divisi == "") {
            $result = array(
                'status' => 'false',
                'message' => 'Form masih ada yang kosong!'
            );
        } else {
            $data = array(
                'jab' => $jab,
                'idp' => $idp,
                'sns' => 'STAFF',
                'sal_pos' => $sal_pos,
                'id_departemen' => $departemen,
                'id_divisi' => $divisi,
                'id_cabang' => $cabang,
                'stat_pos' => ''
            );

            if ($id_pos) {
                $this->Karyawan_Model->update_posisi($data, $id_pos);
            } else {
                $this->Karyawan_Model->add_posisi($data);
            }

            $result = array(
                'status' => 'true',
                'message' => 'Success!'
            );
        }
        echo json_encode($result);
    }
    
    function view_posisi_id()
    {
        $this->Main_Model->get_login();
        $id     = $this->input->get('id_pos');
        $result = $this->Karyawan_Model->view_posisi_id($id);
        
        echo json_encode($result);
    }
    
    function delete_posisi()
    {
        $this->Main_Model->get_login();
        $id_pos = $this->input->post('id');
        $data = array('status' => 0);
        $this->Karyawan_Model->update_posisi($data, $id_pos);
    }

    function bio_kary($nip = '')
    {
        $this->Main_Model->get_login();
        $footer = array(
            'javascript' => '',
            'js' => $this->footer()
        );
        $data = array(
            'kary' => $this->Karyawan_Model->biodata($nip)
            );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('karyawan/biodata', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function biodata($nip = '')
    {
        $this->Main_Model->get_login();
        $nip_quote = "'".$nip."'";
        $javascript    = '
			<script>
				var FormWizard = function() {
				    return {
				        init: function() {
				            function e(e) {
				                return e.id ? "<img class=flag src=../../assets/global/img/flags/" + e.id.toLowerCase() + ".png/>&nbsp;&nbsp;" + e.text : e.text
				            }
				            if (jQuery().bootstrapWizard) {
				                $("#country_list").select2({
				                    placeholder: "Select",
				                    allowClear: !0,
				                    formatResult: e,
				                    width: "auto",
				                    formatSelection: e,
				                    escapeMarkup: function(e) {
				                        return e
				                    }
				                });
				                var r = $("#submit_form"),
				                    t = $(".alert-danger", r),
				                    i = $(".alert-success", r);
				                r.validate({
				                    doNotHideMessage: !0,
				                    errorElement: "span",
				                    errorClass: "help-block help-block-error",
				                    focusInvalid: !1,
				                    rules: {
				                        nama: {
				                            required: !0
				                        },
				                        nip: {
				                            required: !0
				                        },
				                        ktp: {
				                            required: !0
				                        },
				                        t_lahir: {
				                            required: !0
				                        },
				                        tgl_lahir: {
				                            required: !0
				                        },
				                        gend : {
				                        	required: !0
				                        },
				                        alamat :{
				                        	required : !0
				                        },
				                        mar_stat : {
				                        	required: !0
				                        },
				                        email: {
				                            email: !0
				                        },
				                        pend: {
				                            required: !0
				                        },
				                        nama_sklh: {
				                            required: !0
				                        },
				                        jurusan: {
				                            required: !0
				                        },
				                        th_lulus: {
				                            required: !0,
				                            maxlength: 4
				                        },
				                        ikut_asuransi: {
				                            required: !0
				                        },
				                        telp_emergency : {
				                        	required: !0
				                        },
				                        agama : {
				                        	required: !0
				                        },
				                        pin : {
				                        	required: !0
				                        }
				                    },
				                   
				                    errorPlacement: function(e, r) {
				                        "gender" == r.attr("name") ? e.insertAfter("#form_gender_error") : "payment[]" == r.attr("name") ? e.insertAfter("#form_payment_error") : e.insertAfter(r)
				                    },
				                    invalidHandler: function(e, r) {
				                        i.hide(), t.show(), App.scrollTo(t, -200)
				                    },
				                    highlight: function(e) {
				                        $(e).closest(".form-group").removeClass("has-success").addClass("has-error")
				                    },
				                    unhighlight: function(e) {
				                        $(e).closest(".form-group").removeClass("has-error")
				                    },
				                    success: function(e) {
				                        "gender" == e.attr("for") || "payment[]" == e.attr("for") ? (e.closest(".form-group").removeClass("has-error").addClass("has-success"), e.remove()) : e.addClass("valid").closest(".form-group").removeClass("has-error").addClass("has-success")
				                    },
				                    submitHandler: function(e) {
				                        i.show(), t.hide()
				                    }
				                });
				                var a = function() {
				                        $("#tab6 .form-control-static", r).each(function() {
				                            var e = $("[name=" + $(this).attr("data-display") + "]", r);
				                            if (e.is(":radio") && (e = $("[name=" + $(this).attr("data-display") + "]:checked", r)), e.is(":text") || e.is("textarea")) $(this).html(e.val());
				                            else if (e.is("select")) $(this).html(e.find("option:selected").text());
				                            else if (e.is(":radio") && e.is(":checked")) $(this).html(e.attr("data-title"));
				                            else if ("payment[]" == $(this).attr("data-display")) {
				                                var t = [];
				                                $("[name=payment[]]:checked", r).each(function() {
				                                    t.push($(this).attr("data-title"))
				                                }), $(this).html(t.join("<br>"))
				                            }
				                        })
				                    },
				                    o = function(e, r, t) {
				                        var i = r.find("li").length,
				                            o = t + 1;
				                        $(".step-title", $("#form_wizard_1")).text("Step " + (t + 1) + " of " + i), jQuery("li", $("#form_wizard_1")).removeClass("done");
				                        for (var n = r.find("li"), s = 0; t > s; s++) jQuery(n[s]).addClass("done");
				                        1 == o ? $("#form_wizard_1").find(".button-previous").hide() : $("#form_wizard_1").find(".button-previous").show(), o >= i ? ($("#form_wizard_1").find(".button-next").hide(), $("#form_wizard_1").find(".button-submit").show(), a()) : ($("#form_wizard_1").find(".button-next").show(), $("#form_wizard_1").find(".button-submit").hide()), App.scrollTo($(".page-title"))
				                    };
				                $("#form_wizard_1").bootstrapWizard({
				                    nextSelector: ".button-next",
				                    previousSelector: ".button-previous",
				                    onTabClick: function(e, r, t, i) {
				                        return !1
				                    },
				                    onNext: function(e, a, n) {
				                        return i.hide(), t.hide(), 0 == r.valid() ? !1 : void o(e, a, n)
				                    },
				                    onPrevious: function(e, r, a) {
				                        i.hide(), t.hide(), o(e, r, a)
				                    },
				                    onTabShow: function(e, r, t) {
				                        var i = r.find("li").length,
				                            a = t + 1,
				                            o = a / i * 100;
				                        $("#form_wizard_1").find(".progress-bar").css({
				                            width: o + "%"
				                        })
				                    }
				                }), $("#form_wizard_1").find(".button-previous").hide(), $("#form_wizard_1 .button-submit").click(function() {
				                    
				                }).hide(), $("#country_list", r).change(function() {
				                    r.validate().element($(this))
				                })
				            }
				        }
				    }
				}();
				jQuery(document).ready(function() {
				    FormWizard.init()
				});

				$(".date-picker").datepicker({
                	rtl: App.isRTL(),
                	orientation: "left",
                	autoclose: !0
            	})

				function get_id() {
					var id = {
						"nip" : '.$nip_quote.'
					}

					$.ajax({
						url : "'.base_url('karyawan/get_id').'",
						data : id,
						type : "GET",
						dataType : "JSON",
						success : function(obj){
							$("#pin").val(obj.pin);
							$("#nip_val").val(obj.nip);
							$("#nama").val(obj.nama);
							$("#gend").val(obj.gend);
							$("#pend").val(obj.id_pend);
							$("#ktp").val(obj.ktp);
							$("#nama_bank").val(obj.bank);
							$("#no_rek").val(obj.norek);
							$("#nama_akun").val(obj.norek_an);
							$("#tfinfo").val(obj.tfinfo);
							$("#tgl_lahir").val(obj.tgl_lahir);
							$("#telp").val(obj.telp);
							$("#alamat").val(obj.alamat);
							$("#email").val(obj.mail);
							$("#mar_stat").val(obj.mar_stat);
							$("#ikut_asuransi").val(obj.ikut_asuransi);
							$("#idp").val(obj.idp);
							$("#t_lahir").val(obj.t_lahir);
							$("#nama_sklh").val(obj.nama_sklh);
							$("#jurusan").val(obj.jurusan);
							$("#th_lulus").val(obj.th_lulus);
							$("#npwp").val(obj.npwp);
							$("#th_hist_penyakit_1").val(obj.th1);
							$("#desc_penyakit_1").val(obj.penyakit1);
							$("#th_hist_penyakit_2").val(obj.th2);
							$("#desc_penyakit_2").val(obj.penyakit2);
							$("#th_hist_penyakit_3").val(obj.th3);
							$("#desc_penyakit_3").val(obj.penyakit3);
							$("#no_ijazah").val(obj.no_ijazah);
							$("#status_ijazah").val(obj.status_ijazah);
							$("#telp_emergency").val(obj.telp_emergency);
							$("#contact_emergency").val(obj.contact_emergency);
							$("#surat_komitmen").val(obj.surat_komitmen);
							$("#golongan_darah").val(obj.golongan_darah);
							$("#alasan_bpjsks").val(obj.alasan_bpjsks);
							$("#salary").val(obj.salary);
						},
						error : function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal mengambil data!");
						}
					});
				}

				function save() {
					$.ajax({
						url : "'.base_url('karyawan/update_karyawan').'",
						type : "POST",
						data : new FormData($("#submit_form")[0]),
						processData: false,
      					contentType: false,
						success : function(data){
							bootbox.dialog({
								message : "Sukses Menyimpan Data!", 
								buttons : {
									main : {
										label : "OK", 
										className 	: "blue", 
										callback 	: function(){
											location.href="'.base_url('karyawan/datakaryawan').'"; 
											return true;
										}
									}
								}
							});
						},
						error 	: function(jqXHR, textStatus, errorThrown){
							bootbox.alert("Gagal Menyimpan data!");
						}
					});	
				}
				get_id();

				// $(".bpjs").blur(function(){
				// 	if($("#no_bpjsks").val() != "") {
				// 		$("#alasan_bpjsks").attr("readonly", true);
				// 		$("#alasan_bpjsks").val("");
				// 	} else {
				// 		$("#alasan_bpjsks").attr("readonly", false);
				// 		//$("#alasan_bpjsks").val("");
				// 	}
				// });
			</script>
			';
        $footer = array(
            'javascript'=> $javascript,
            'js' => $this->footer()
        );

        $ms_gender = $this->Main_Model->view_by_id('ms_gender', [], 'result');
        $gender = [];
        if ($ms_gender) {
            foreach ($ms_gender as $row) {
                $gender[$row->kode] = $row->label;
            }
        }

        // $gender = array(
        //     'L' => 'Laki Laki',
        //     'P' => 'Perempuan'
        // );

        $level = array(
            'Entry' => 'Entry',
            'Pro' => 'Pro'
        );

        $ms_tfinfo = $this->Main_Model->view_by_id('ms_info_rekening', [], 'result');
        $tfinfo = [];
        if ($ms_tfinfo) {
            foreach ($ms_tfinfo as $row) {
                $tfinfo[$row->kode] = $row->label;
            }
        }

        // $tfinfo = array(
        //     '' => 'Status Rekening',
        //     'OWN' => 'Own',
        //     'TITIP' => 'Titip'
        // );

        $ms_marriage = $this->Main_Model->view_by_id('ms_marriage', [], 'result');
        $mar_stat = [];
        if ($ms_marriage) {
            foreach ($ms_marriage as $row) {
                $mar_stat[$row->kode] = $row->label;
            }
        }

        // $mar_stat = array(
        //     'LAJANG' => 'Lajang',
        //     'MENIKAH' => 'Menikah',
        //     'DUDA' => 'Duda',
        //     'JANDA' => 'Janda'
        // );

        $ikut_asuransi  = array(
            '0' => 'Tidak',
            '1' => 'Ya'
        );

        $ms_status = $this->Main_Model->view_by_id('ms_status_karyawan', [], 'result');
        $kary_stat = [];
        if ($ms_status) {
            foreach ($ms_status as $row) {
                $kary_stat[$row->kode] = $row->label;
            }
        }

        // $kary_stat = array(
        //     'KONTRAK' => 'Kontrak',
        //     'TETAP' => 'Tetap',
        //     'TRAINING' => 'Training'
        // );

        $ms_agama = $this->Main_Model->view_by_id('ms_agama', [], 'result');
        $agama = [];
        if ($ms_agama) {
            foreach ($ms_agama as $row) {
                $agama[$row->kode] = $row->label;
            }
        }

        // $agama = array(
        //     'ISLAM' => 'ISLAM',
        //     'KRISTEN' => 'KRISTEN',
        //     'KATHOLIK'  => 'KATHOLIK',
        //     'HINDU' => 'HINDU',
        //     'BUDHA' => 'BUDHA'
        // );

        $data = array(
            'gend' => $gender,
            'pend' => $this->Karyawan_Model->pendidikan(),
            'level' => $level,
            'tfinfo' => $tfinfo,
            'mar_stat' => $mar_stat,
            'lokasi_kerja' => $this->Karyawan_Model->lokasi_kerja(),
            'ikut_asuransi' => $ikut_asuransi,
            'kary_stat' => $kary_stat,
            'idp' => $this->Main_Model->opt_idp(),
            'agama' => $agama
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/form_update_karyawan', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function get_id()
    {
        $nip = $this->input->get('nip');
        $result = $this->Karyawan_Model->get_id_karyawan($nip);
        
        echo json_encode($result);
    }
    
    function update_karyawan()
    {
        $this->Main_Model->get_login();
        $this->load->library('upload');
        $oldnip = $this->input->post("nip");
        $new_nip = $this->input->post("nip_val");
        // $tgl_lahir_ex = explode("/", $this->input->post('tgl_lahir'));
        // $tgl_lahir = $tgl_lahir_ex[2] . "-" . $tgl_lahir_ex[1] . "-" . $tgl_lahir_ex[0];

        $tgl_lahir = $this->input->post('tgl_lahir');
        $tgl_lahir = $this->Main_Model->convert_tgl($tgl_lahir);

        $nama = $this->input->post('nama');
        $salary = $this->input->post('salary');

        // ($this->input->post('no_bpjsks') != '') ? $ikut_asuransi = 1 : $ikut_asuransi = 0;
        $ikut_asuransi = $this->input->post('ikut_asuransi');

        $data = array(
            'nip' => $new_nip,
            'pin' => $this->input->post('pin'),
            'nama' => $this->input->post('nama'),
            'gend' => $this->input->post('gend'),
            'id_pend' => $this->input->post('pend'),
            'ktp' => $this->input->post('ktp'),
            'bank' => $this->input->post('nama_bank'),
            'norek' => $this->input->post('no_rek'),
            'norek_an' => $this->input->post('nama_akun'),
            'tfinfo' => $this->input->post('tfinfo'),
            'tgl_lahir' => $tgl_lahir,
            'telp' => $this->input->post('telp'),
            'telp_emergency' => $this->input->post('telp_emergency'),
            'contact_emergency' =>$this->input->post('contact_emergency'),
            'agama' => $this->input->post('agama'),
            'surat_komitmen' => $this->input->post('surat_komitmen'),
            'alamat' => $this->input->post('alamat'),
            'mail' => $this->input->post('email'),
            'mar_stat' => $this->input->post('mar_stat'),
            'ikut_asuransi' => $ikut_asuransi,
            't_lahir' => $this->input->post('t_lahir'),
            'nama_sklh' => $this->input->post('nama_sklh'),
            'jurusan' => $this->input->post('jurusan'),
            'th_lulus' => $this->input->post('th_lulus'),
            'npwp' => $this->input->post('npwp'),
            'idp' => $this->session->userdata('idp'),
            'th1' => $this->input->post('th_hist_penyakit_1'),
            'penyakit1' => $this->input->post('desc_penyakit_1'),
            'th2' => $this->input->post('th_hist_penyakit_2'),
            'penyakit2' => $this->input->post('desc_penyakit_2'),
            'th3' => $this->input->post('th_hist_penyakit_3'),
            'penyakit3' => $this->input->post('desc_penyakit_3'),
            'no_ijazah' => $this->input->post('no_ijazah'),
            'status_ijazah' => $this->input->post('status_ijazah'),
            'agama' => $this->input->post('agama'),
            'no_bpjsks' => $this->input->post('no_bpjsks'),
            'no_bpjstk' => $this->input->post('no_bpjstk'),
            'golongan_darah' => $this->input->post('golongan_darah'),
            'alasan_bpjsks' => $this->input->post('alasan_bpjsks'),
            'salary' => $salary
        );

        $file_surat_kom = 'Surat Komitmen '.strtoupper($nama).' '.date("d-m-Y");
        $config_surat = $this->Main_Model->set_upload_options("./assets/surat_komitmen/", "*", "5048000", $file_surat_kom);
        $this->load->library('upload', $config_surat, 'surat_komitmen');

        // $this->upload->initialize($config_surat);
        if ($this->surat_komitmen->do_upload('file_surat_komitmen')) {
            $upload_surat_kom= $this->surat_komitmen->data();
            $data['file_surat_komitmen'] = $upload_surat_kom['file_name'];
        }

        $file_bpjs_tk   = 'BPJS TK '.strtoupper($nama).' '.date("d-m-Y");
        $config_bpjs_tk = $this->Main_Model->set_upload_options("./assets/bpjs/", "*", "5048000", $file_bpjs_tk);
        $this->load->library('upload', $config_bpjs_tk, 'bpjs_tk');

        // $this->upload->initialize($config_bpjs_tk);
        if ($this->bpjs_tk->do_upload('file_bpjs_tk')) {
            $upload_file_bpjs_tk = $this->bpjs_tk->data();
            $data['file_bpjs_tk'] = $upload_file_bpjs_tk['file_name'];
        }

        $file_bpjs_ks   = 'BPJS KS '.strtoupper($nama).' '.date("d-m-Y");
        $config_bpjs_ks = $this->Main_Model->set_upload_options("./assets/bpjs/", "*", "5048000", $file_bpjs_ks);
        $this->load->library('upload', $config_bpjs_ks, 'bpjs_ks');

        // $this->upload->initialize($config_bpjs_ks);
        if ($this->bpjs_ks->do_upload('file_bpjs_ks')) {
            $upload_file_bpjs_ks = $this->bpjs_ks->data();
            $data['file_bpjs_ks'] = $upload_file_bpjs_ks['file_name'];
        }

        // $file_ser1       = 'Sertifikat I '.$this->input->post('nip').' '.date("d-m-Y");
        // $config_ser1     = $this->Main_Model->set_upload_options("./assets/sertifikat/","*","5048000",$file_ser1);
        // $this->load->library('upload', $config_ser1, 'ser1');

        // // $this->upload->initialize($config_ser1);
        // if($this->ser1->do_upload('file_sertifikat1'))
        // {
        //  $upload_file_sertifikat1 = $this->ser1->data();
        //  $data['file_sertifikat1'] = $upload_file_sertifikat1['file_name'];
        // }

        $file_ser2 = 'Sertifikat II '.strtoupper($nama).' '.date("d-m-Y");
        $config_ser2 = $this->Main_Model->set_upload_options("./assets/sertifikat/", "*", "5048000", $file_ser2);
        $this->load->library('upload', $config_ser2, 'ser2');

        // $this->upload->initialize($config_ser2);
        if ($this->ser2->do_upload('file_sertifikat2')) {
            $upload_file_sertifikat2 = $this->ser2->data();
            $data['file_sertifikat2'] = $upload_file_sertifikat2['file_name'];
        }

        $file_ser3 = 'Sertifikat III '.strtoupper($nama).' '.date("d-m-Y");
        $config_ser3 = $this->Main_Model->set_upload_options("./assets/sertifikat/", "*", "5048000", $file_ser3);
        $this->load->library('upload', $config_ser3, 'ser3');

        // $this->upload->initialize($config_ser3);
        if ($this->ser3->do_upload('file_sertifikat3')) {
            $upload_file_sertifikat3 = $this->ser3->data();
            $data['file_sertifikat3'] = $upload_file_sertifikat3['file_name'];
        }

        // updated at 15 05 2018
        $file_ijazah = 'Ijazah '.strtoupper($nama).' '.date('d-m-Y');
        $config_file_ijazah = $this->Main_Model->set_upload_options('./assets/sertifikat/', '*', '5048000', $file_ijazah);
        $this->load->library('upload', $config_file_ijazah, 'ijazah');

        if ($this->ijazah->do_upload('file_sertifikat1')) {
            $upload_file_ijazah = $this->ijazah->data();
            $data['file_ijazah'] = $upload_file_ijazah['file_name'];
        }
        
        $this->Karyawan_Model->update_karyawan($data, $oldnip);
        // print_r($data);
    }
    
    function sk($nip = '')
    {
        $url_load   = base_url('karyawan/view_sk').'/'.$nip;
        $this->Main_Model->get_login();
        $list_sk    = $this->Karyawan_Model->old_sk($nip);

        $id_sk = isset($list_sk->id_sk)?$list_sk->id_sk:'';
        $id_stob_pos = isset($list_sk->id_stob_pos)?$list_sk->id_stob_pos:'';
        $jab = isset($list_sk->jab)?$list_sk->jab:'';

        $javascript = '
			<script>
				'.$this->Main_Model->default_loadtable($url_load)

                 .$this->Main_Model->default_datepicker()

                 .$this->Main_Model->default_select2()

                 .$this->Main_Model->notif().'

				$("#jab").change(function(){
					var selected = $("#jab option:selected").text();
					var n = selected.search(/sales force/i);
					if(n > 0)
					{
						form = "<div class=\"form-group\"><label>BPKB</label><input type=\"text\" class=\"form-control\" name=\"bpkb\" id=\"bpkb\"></div><div class=\"form-group\"><label>SIM A/C</label><input type=\"text\" name=\"sim\" id=\"sim\" class=\"form-control\"></div>";
						$("#kolom_sales").html(form);
					}
					else
					{
						$("#kolom_sales").html("");
					}
				});

				function change_tipe(id)
				{
					var tipe = $("#tipe_sk").val();
					if(id==null)
					{
						if(tipe==5)
						{
							form = "<label>Tanggal Masuk Kerja</label><input type=\"text\" class=\"form-control date-picker\" data-date-format=\"dd/mm/yyyy\" name=\"tgl_kerja\" id=\"tgl_kerja\" required>";
							$("#tipe_value").html(form);
							$("#status_upah").html("");
							$("#file_upah").html("");
						}
						else
						{
							form 		= "<label>SK Sebelumnya</label><select name=\"old_sk\" id=\"old_sk\" class=\"form-control\" required>"';

                        $javascript .='
							form += "<option value=\"'.$id_sk.'\">'.$id_stob_pos.'  '.$jab.'</option>";';

                            $javascript .=  'form += "</select>";
							st_upah 	= "<label>Status Kesepakatan Upah</label><select name=\"status_kesepakatan\" id=\"status_kesepakatan\" class=\"form-control\" required><option value=\"Belum\">Belum</option><option value=\"Sudah\">Sudah</option></select>";
							fl_upah 	= "<label>File Kesepakatan Upah</label><input type=\"file\" name=\"file_upah\" id=\"file_upah\" class=\"form-control\">";

							$("#tipe_value").html(form);
							$("#status_upah").html(st_upah);
							$("#file_upah").html(fl_upah);
						}
					}
					else
					{
						if(tipe==5)
						{
							form = "<label>Tanggal Masuk Kerja</label><input type=\"text\" class=\"form-control date-picker\" data-date-format=\"dd/mm/yyyy\" name=\"tgl_kerja\" id=\"tgl_kerja\" required>";
							$("#tipe_value").html(form);
							$("#status_upah").html("");
							$("#file_upah").html("");
						}
						else
						{
							$.ajax({
								url 	: "'.base_url('karyawan/cari_sk').'/"+id,
								dataType: "JSON",
								success : function(data){
									form 		= "<label>SK Sebelumnya</label><select name=\"old_sk\" id=\"old_sk\" class=\"form-control\" required><option value="+data.id_sk+">"+data.id_stob_pos+" - "+data.posisi+"</option></select>";

									st_upah 	= "<label>Status Kesepakatan Upah</label><select name=\"status_kesepakatan\" id=\"status_kesepakatan\" class=\"form-control\" required><option value=\"Belum\">Belum</option><option value=\"Sudah\">Sudah</option></select>";

									fl_upah 	= "<label>File Kesepakatan Upah</label><input type=\"file\" name=\"file_upah\" id=\"file_upah\" class=\"form-control\">";
									
									$("#tipe_value").html(form);
									$("#status_upah").html(st_upah);
									$("#file_upah").html(fl_upah);
								},
								error 	: function(jqXHR,textStatus,errorThrown){
									'.$this->Main_Model->notif500().'
								}
							});
						}
					}

					
					'.$this->Main_Model->default_datepicker().'
				}

				$(document).ready(function(){
					change_tipe();
					cari_cabang();
				});

				var edit = false;

				function onedit(id_departemen, id_cabang, id_divisi, id_jabatan, id_sk)
				{
					edit = true;
					$.ajax({
						url 	: "'.base_url('karyawan/cari_cabang').'/"+id_departemen,
						success : function(data){
							$("#cabang").html(data);
							$("#cabang").val(id_cabang).trigger("change");
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							notif("Gagal Mengambil data!");
						}
					}).done(function(){
						$.ajax({
								url 	: "'.base_url('karyawan/cari_divisi').'/"+id_cabang+"/"+id_departemen,
								success : function(data){
									$("#divisi").html(data);
									$("#divisi").val(id_divisi).trigger("change");
								},
								error 	: function(jqXHR,textStatus,errorThrown){
									notif("Gagal Mengambil data!");
								}
							}).done(function(){
								$.ajax({
										url 	: "'.base_url('karyawan/cari_jabatan_edit').'/"+id_divisi+"/"+id_cabang+"/"+id_departemen+"/"+id_sk,
										success : function(data){
											$("#jab").html(data);
											$("#jab").val(id_jabatan).trigger("change");
											edit = false;
										},
										error 	: function(jqXHR,textStatus,errorThrown){
											notif("Gagal Mengambil data!");
										}
									});
							});
					});
				}

				function cari_cabang(id_departemen)
				{
					if(edit == false) {
						$.ajax({
							url 	: "'.base_url('karyawan/cari_cabang').'/"+id_departemen,
							success : function(data){
								$("#cabang").val("").trigger("change");
								$("#cabang").html(data);
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								notif("Gagal Mengambil data!");
							}
						});
					}
					
				}

				function cari_divisi(id_cabang)
				{
					if(edit == false) {
						id_departemen = $("#departemen").val();
						$.ajax({
							url 	: "'.base_url('karyawan/cari_divisi').'/"+id_cabang+"/"+id_departemen,
							success : function(data){
								$("#divisi").val("").trigger("change");
								$("#divisi").html(data);
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								notif("Gagal Mengambil data!");
							}
						});
					}
				}

				function cari_jabatan(id_divisi)
				{
					if(edit == false) {
						tipe = $("#tipe_sk").val();
						(tipe == 7) ? rotasi = "y" : rotasi = "";
						id_departemen = $("#departemen").val();
						id_cabang 	  = $("#cabang").val();
						$.ajax({
							url 	: "'.base_url('karyawan/cari_jabatan').'/"+id_divisi+"/"+id_cabang+"/"+id_departemen+"/"+rotasi,
							success : function(data){
								$("#jab").val("").trigger("change");
								$("#jab").html(data);
							},
							error 	: function(jqXHR,textStatus,errorThrown){
								notif("Gagal Mengambil data!");
							}
						});
					}
				}

				function get_id(id)
				{
					$.ajax({
						url 	: "' . base_url('karyawan/cari_sk') . '/"+id,
						dataType: "JSON",
						success : function(obj){
							$("#upload_sk").prop("required",false);
							$("#id_sk").val(obj.id_sk);
							$("#no_sk").val(obj.no_sk);
							$("#tipe_sk").val(obj.id_tipe_sk).trigger("change");
							$("#old_sk").val(obj.sk_lama);
							$("#tgl_sk").val(obj.tglsk);
							$("#isaktif").val(obj.aktif);
							$("#departemen").val(obj.id_departemen).trigger("change");
							
							change_tipe(obj.sk_lama);
							$("#tgl_kerja").val(obj.tglkerja);

							onedit(obj.id_departemen, obj.id_cabang, obj.id_divisi, obj.id_pos_sto, obj.id_sk);
						},
						error 	: function(jqXHR,textStatus,errorThrown){
							'.$this->Main_Model->notif500().'
						}
					});
				}

				
				function cetak(id_sk)
				{
					window.open("'.base_url('karyawan/cetak_sk').'/"+id_sk);
				}

				function delete_data(id)
				{
					bootbox.dialog({
			    		message : "Yakin ingin menghapus SK?",
			    		title : "Hapus SK",
			    		buttons :{
			    			danger : {
			    				label : "Delete",
			    				className : "red",
			    				callback : function(){
			    					$.ajax({
							    		url : "' . base_url('karyawan/hapus_sk') . '/"+id,
							    		type : "POST",
							    		success : function(data){
							    			bootbox.alert({
												message: "SK Telah dihapus!",
												size: "small"
											});
							    			load_table();
							    		},
							    		error : function(jqXHR, textStatus, errorThrown){
							    			alert("Internal Server Error");
							    		}
							    	});
			    				}
			    			},
			    			main : {
			    				label : "Cancel",
			    				className : "blue",
			    				callback : function(){
			    					return true;
			    				}
			    			}
			    		}
			    	})
				}

			</script>
		';
        $footer     = array(
            'javascript'=> $javascript,
            'js'        => $this->footer()
        );

        ($list_sk) ? $ids = '' : $ids = '5';

        $idp        = $this->session->userdata('idp');
        $dept       = $this->Karyawan_Model->view_departemen($idp);
        $d[""]      = "Pilih Departemen";
        foreach ($dept as $row) {
            $d[$row->id_dept] = $row->departemen;
        }

        $data       = array(
            'tipe_sk'   => $this->Karyawan_Model->tipe_sk_opt($ids),
            'departemen'=> $d
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );

        $this->Karyawan_Model->update_karyawan_posisi();

        $this->load->view('template/header', $header);
        $this->load->view('karyawan/sk', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function cari_sk($id_sk)
    {
        $this->Main_Model->get_login();
        $sk     = $this->Karyawan_Model->sk_id($id_sk);
        
        echo json_encode($sk);
    }

    function cari_cabang($id_departemen = '')
    {
        $this->Main_Model->get_login();
        $data   = $this->Karyawan_Model->cari_cabang($id_departemen);
        $opt    = '<select class="select2" style="width:100%" name="cabang" id="cabang" required>';
        $opt   .= '<option value="">Pilih Cabang</option>';
        foreach ($data as $row) {
            $opt .= '<option value="'.$row->id_cab.'">'.$row->cabang.'</option>';
        }
        $opt   .= '</select>';
        echo $opt;
    }

    function cari_divisi($id_cabang = '', $id_departemen = '')
    {
        $this->Main_Model->get_login();
        $data   = $this->Karyawan_Model->cari_divisi($id_cabang, $id_departemen);
        $opt    = '<select class="select2" style="width:100%" name="divisi" id="divisi" required>';
        $opt   .= '<option value="">Pilih Divisi</option>';
        foreach ($data as $row) {
            $opt .= '<option value="'.$row->id_divisi.'">'.$row->divisi.'</option>';
        }
        $opt   .= '</select>';
        echo $opt;
    }

    function cari_jabatan($id_divisi = '', $id_cabang = '', $id_departemen = '', $rotasi = '')
    {
        $this->Main_Model->get_login();
        $data   = $this->Karyawan_Model->cari_jabatan($id_divisi, $id_cabang, $id_departemen, $rotasi);
        $opt    = '<select class="select2" style="width:100%" name="jab" id="jab" required>';
        $opt   .= '<option value="">Pilih Jabatan</option>';
        foreach ($data as $row) {
            $id_sto = isset($row->id_sto) ? $row->id_sto : '';
            $q = $this->db->query("SELECT b.`nama` FROM sk a LEFT JOIN kary b ON a.`nip` = b.`nip` WHERE a.`id_pos_sto` = '$id_sto' AND a.`aktif` = '1'")->row();
            $nama = isset($q->nama) ? $q->nama : '';
            ($rotasi != '' && $nama != '') ? $label = ' - '.$nama : $label = '';
            $opt .= '<option value="'.$row->id_sto.'">'.$row->id_stob_pos.'  '.$row->jab.' - '.$row->sal_pos.' '.$label.'</option>';
        }
        $opt   .= '</select>';
        echo $opt;
    }

    function cari_jabatan_edit($id_divisi = '', $id_cabang = '', $id_departemen = '', $id_sk = '')
    {
        $this->Main_Model->get_login();
        $data   = $this->Karyawan_Model->cari_jabatan_edit($id_divisi, $id_cabang, $id_departemen, $id_sk);
        $opt    = '<select class="select2" style="width:100%" name="jab" id="jab" required>';
        $opt   .= '<option value="">Pilih Jabatan</option>';
        foreach ($data as $row) {
            $opt .= '<option value="'.$row->id_sto.'">'.$row->id_stob_pos.'  '.$row->jab.'</option>';
        }
        $opt   .= '</select>';
        echo $opt;
    }

    function view_sk($nip = '')
    {
        $this->Main_Model->get_login();
        $data = $this->Karyawan_Model->view_sk($nip);
        
        $tbl = '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="dataTables-example">
					<thead>
						<tr>
							<th>Posisi</th>
							<th>Cabang</th>
							<th>No SK</th>
							<th>Aktif</th>
							<th>Tgl SK</th>
							<th>Tipe SK</th>
							<th width="10%">Action</th>
						</tr>
					</thead>
					<tbody>';
        foreach ($data as $row) {
            ($row->aktif == 1) ? $aktif = "Ya" : $aktif = "Tidak";
            $a    = $this->db->query("select band from band where id_band='$row->id_band'")->row();
            $band = isset($a->band) ? $a->band : '';
            
            $b   = $this->Karyawan_Model->hitung_thp($row->id_sk);
            $thp = isset($b->thp) ? $b->thp : '0';

            ($row->bpkb == 0 || $row->bpkb == "") ? $bpkb = '-' : $bpkb = $row->bpkb;
            ($row->sim == 0 || $row->sim == "") ? $sim = '-' : $sim = $row->sim;
            
            ($row->file_kesepakatan_upah) ? $link = '<li> <a href="'.base_url('assets/kesepakatan_upah').'/'.$row->file_kesepakatan_upah.'"> Download Kesepakatan Upah </a> </li>' : $link = '';

            $tbl .= '<tr>
						<td>' . $row->jab . '</td>
						<td>' . $row->cabang . '</td>
						<td>' . $row->no_sk . '</td>
						<td>' . $aktif . '</td>
						<td>' . $this->Main_Model->format_tgl($row->tgl_sk) . '</td>
						<td>' . $row->tipe . '</td>
						<td>
							<div class="btn-group">
					            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
					                <i class="fa fa-angle-down"></i>
					            </button>
					                <ul class="dropdown-menu" role="menu">
					                	'.$link.'
					                	<li>
					                    	<a href="'.base_url('assets/sk').'/'.$row->file_sk.'"> Download SK </a>
					                    </li>
            							<!--<li>
            								<a href="javascript:;" onclick="cetak(' . $row->id_sk . ')">
            									Cetak </a>
            							</li>-->
            							<li>
					                        <a href="javascript:;" onclick="get_id(' . $row->id_sk . ');">
					                            Update </a>
					                    </li>
										<li>
					                        <a href="javascript:;" onclick="delete_data(' . $row->id_sk . ');">
					                            Delete </a>
					                    </li>
					                    
					                </ul>
					        </div>
						</td>
					</tr>';
        }
        $tbl .= '</tbody></table>';
        
        echo $tbl;
    }

    function add_sk()
    {
        $nip            = $this->input->post('nip');
        $id_sk          = $this->input->post('id_sk');
        $isaktif        = $this->input->post('isaktif');
        $id_tipe_sk     = $this->input->post('tipe_sk');
        $cek_sk         = $this->db->query("select * from sk where nip = '$nip' and aktif='1'")->row();
        $dat_up         = array('aktif' => 0);
        $id_pos_sto     = $this->input->post('jab');
        $id_sk          = isset($cek_sk->id_sk)?$cek_sk->id_sk:'';
        $sk_lama        = $this->input->post('old_sk');
        $this->Karyawan_Model->update_sk($dat_up, $id_sk);

        $check_active_rotation = $this->Main_Model->view_by_id('sk', array('id_pos_sto' => $id_pos_sto, 'aktif' => 1), 'row');
        // print_r($check_active_rotation);exit;

        $exp_pos= explode("-", $this->input->post('posisi'));

        $file_name  = 'SK '.$nip.' '.date("d-m-Y");
        $config     = array(
            'upload_path'   => "./assets/sk/",
            'allowed_types' => "*",
            'file_name'     => $file_name,
            'max_size'      => "5048000",
            'overwrite'     => true
            );
        $this->load->library('upload', $config);
        $this->upload->do_upload('upload_sk');
        $upload_sk          = $this->upload->data();

        $file_name_ku   = 'File Kesepakatan Upah '.$this->input->post('nip').' '.date("d-m-Y");
        $config_ku      = $this->Main_Model->set_upload_options("./assets/kesepakatan_upah/", "*", "5048000", $file_name_ku);

        $this->upload->initialize($config_ku);
        if ($this->upload->do_upload('file_upah')) {
            $file_upah      = $this->upload->data();
        }

        ($id_tipe_sk == '5') ? $tgl_kerja = $this->Main_Model->convert_tgl($this->input->post('tgl_kerja')) : $tgl_kerja='';

        
        // if($check_active_rotation != '') {
        //  $nip_rotasi = isset($check_active_rotation->nip) ? $check_active_rotation->nip : '';
        //  $c      = $this->db->query("select * from sk where nip = '$nip_rotasi' and aktif='1'")->row();
        //     $id_sk_rotation = isset($c->id_sk) ? $c->id_sk : '';

        //  $o = $this->Main_Model->view_by_id('sk', array('nip' => $nip, 'id_sk' => $sk_lama));
        //  $r_id_band = isset($o->id_band) ? $o->id_band : '0';
        //  $r_gd = isset($o->gd) ? $o->gd : '0';
        //  $r_tprof = isset($o->tprof) ? $o->tprof : '0';
        //  $r_stat_jab = isset($o->stat_jab) ? $o->stat_jab : '';
        //  $r_id_pos_sto = isset($o->id_pos_sto) ? $o->id_pos_sto : '';

        //  $data_rotasi = array(
        //         'nip' => $nip_rotasi,
        //         'no_sk' => '',
        //         'id_band' => $r_id_band,
        //         'gd' => $r_gd,
        //         'tprof' => $r_tprof,
        //         'stat_jab' => 'FULL',
        //         'id_pos_sto' => $r_id_pos_sto,
        //         'aktif' => 1,
        //         // 'tgl'             => date("Y-m-d"),
        //         'id_tipe_sk' => $this->input->post('tipe_sk'),
        //         'tgl_sk' => $this->Main_Model->convert_tgl($this->input->post('tgl_sk')),
        //         'sk_lama' => $id_sk_rotation,
        //         'tgl_kerja' => $tgl_kerja,
        //         'file_sk' => $upload_sk['file_name'],
        //         'bpkb'   => $this->input->post('bpkb'),
        //         'sim'    => $this->input->post('sim'),
        //         'status_kesepakatan' => $this->input->post('status_kesepakatan')
        //     );

        //     print_r($data_rotasi);exit;
        //  $pin = '';
        //     $q   = $this->Main_Model->kary_nip($nip_rotasi);
        //     if($q) $pin = $q->pin;
    
        //     $this->Sto_Model->update_sto(array('id_stob_pos' => $pin), $id_pos_sto);
        //     $this->Karyawan_Model->update_sk($dat_up, $id_sk_rotation);

        //     // $this->Karyawan_Model->add_sk($data_rotasi);
        //     $this->Main_Model->process_data('sk', $data_rotasi, null);
        // }
        

        if ($id_sk=="") {
            $data       = array(
                'nip'           => $nip,
                'no_sk'         => $this->input->post('no_sk'),
                'id_band'       => $this->input->post('band'),
                'gd'            => $this->input->post('gd'),
                'tprof'         => $this->input->post('tprof'),
                'stat_jab'      => 'FULL',
                'id_pos_sto'    => $id_pos_sto,
                'aktif'         => 1,
                // 'tgl'            => date("Y-m-d"),
                'id_tipe_sk'    => $this->input->post('tipe_sk'),
                'tgl_sk'        => $this->Main_Model->convert_tgl($this->input->post('tgl_sk')),
                'sk_lama'       => $this->input->post('old_sk'),
                'tgl_kerja'     => $tgl_kerja,
                'file_sk'       => $upload_sk['file_name'],
                'bpkb'          => $this->input->post('bpkb'),
                'sim'           => $this->input->post('sim'),
                'status_kesepakatan' => 'Sudah'
            );

            // print_r($data); exit;
            if ($this->upload->do_upload('file_upah')) {
                $data['file_kesepakatan_upah'] = $file_upah['file_name'];
            }

            $this->Karyawan_Model->add_sk($data);
            $q  = $this->Main_Model->kary_nip($nip);
            $this->Sto_Model->update_sto(array('id_stob_pos'=>$q->pin), $id_pos_sto);
        } else {
            $old_file   = $this->db->query("SELECT file_sk FROM sk WHERE id_sk='$id_sk'")->row();
            ($_FILES['upload_sk']['name'] == "") ? $file_sk = $old_file->file_sk : $file_sk = $upload_sk['file_name'];
            $data       = array(
                'nip'           => $this->input->post('nip'),
                'no_sk'         => $this->input->post('no_sk'),
                'id_band'       => $this->input->post('band'),
                'gd'            => $this->input->post('gd'),
                'tprof'         => $this->input->post('tprof'),
                'stat_jab'      => 'FULL',
                'id_pos_sto'    => $id_pos_sto,
                'aktif'         => $isaktif,
                // 'tgl'            => date("Y-m-d"),
                'id_tipe_sk'    => $this->input->post('tipe_sk'),
                'tgl_sk'        => $this->Main_Model->convert_tgl($this->input->post('tgl_sk')),
                'sk_lama'       => $this->input->post('old_sk'),
                'tgl_kerja'     => $tgl_kerja,
                'file_sk'       => $file_sk,
                'bpkb'          => $this->input->post('bpkb'),
                'sim'           => $this->input->post('sim'),
                'status_kesepakatan' => 'Sudah'
            );

             // print_r($data); exit;
            if ($this->upload->do_upload('file_upah')) {
                $data['file_kesepakatan_upah'] = $file_upah['file_name'];
            }
            $this->Karyawan_Model->update_sk($data, $id_sk);
            $q  = $this->Main_Model->kary_nip($nip);
            $this->Sto_Model->update_sto(array('id_stob_pos'=>$q->pin), $id_pos_sto);
        }



        if ($this->input->post('tgl_kerja') != '') {
            $d = array('tgl_masuk'=>$this->Main_Model->convert_tgl($this->input->post('tgl_kerja')));
            $this->Karyawan_Model->up_tgl_masuk($d, $nip);
        }

            $this->session->set_flashdata('message_upload', '<script>bootbox.alert("Success !")</script>');
            $this->load->library('user_agent');
            redirect($this->agent->referrer());
    }
    
    function cabut_sk($id_sk)
    {
        $data = array(
            'aktif' => 0
        );
        $this->Karyawan_Model->cabut_sk($id_sk, $data);
    }
    
    function hapus_sk($id_sk)
    {
        $this->Karyawan_Model->hapus_sk($id_sk);
    }
    
    function resign()
    {
        $this->load->library('upload');

        $file_name  = 'File Resign '.$this->input->post('nip').' '.date("Y-m-d");
        $config     = $this->Main_Model->set_upload_options("./assets/file_resign/", "*", "5048000", $file_name);
        $this->upload->initialize($config);

        $date_create = explode("/", $this->input->post('tgl'));
        $date        = $date_create[2] . "-" . $date_create[1] . "-" . $date_create[0];
        $data        = array(
            'tgl_resign'    => $date,
            'alasan_resign' => $this->input->post('alasan'),
            'rehire'        => $this->input->post('rehire'),
            'tipe_resign'   => $this->input->post('tipe_resign')
        );
        if ($this->upload->do_upload('file_upload')) {
            $file_resign = $this->upload->data();
            $data['file_resign'] = $file_resign['file_name'];
        }
        $nip         = $this->input->post('nip');
        $active      = array('aktif'=>0);

        $this->Karyawan_Model->resign($data, $nip);
        $this->Karyawan_Model->set_nonactive($nip, $active);
    }
    
    function delete_karyawan()
    {
        $this->Main_Model->get_login();
        $nip = $this->input->post("nip");
        $this->Karyawan_Model->delete_karyawan($nip);
    }
    
    function calon_kary()
    {
        $this->Main_Model->get_login();
        $url    = base_url('karyawan/view_calon_kary');
        $url_del= base_url('karyawan/delete_cal_kary');
        $javascript = '
			<script>
				var save_method;
				'.$this->Main_Model->default_datepicker()
                 .$this->Main_Model->default_select2().'

				function reset(){
					save_method="save";
					$("#warning").addClass("hidden");
					$("#success").addClass("hidden");
					$("#nama").val("");
					$("#tgl_lahir").val("");
					$("#jenkel").val("");
					$("#alamat").val("");
					$("#hp").val("");
					$("#email").val("");
					$("#pend").val("");
					$("#institusi").val("");
					$("#pengalaman").val("");
					$("#status").val("");
					$(".select2").val("").trigger("change");
				}
				function save(){
					var nama = $("#nama").val();
					var tgl_lahir = $("#tgl_lahir").val();
					var jenkel = $("#jenkel").val();
					var alamat = $("#alamat").val();
					var hp = $("#hp").val();
					var email = $("#email").val();
					var pend = $("#pend").val();
					var institusi = $("#institusi").val();
					var pengalaman = $("#pengalaman").val();
					var id = $("#id").val();
					var loker = $("#loker").val();
					var cabang = $("#cabang").val();
					var status = $("#status").val();

					if(nama==""||tgl_lahir==""||jenkel==""||alamat==""||hp==""||pend==""||institusi==""||status==""){
						'.$this->Main_Model->notif400().'
					}else{
						var dat = {
							"id" : id,
							"nama" : nama,
							"tgl_lahir" : tgl_lahir,
							"jen_kel" : jenkel,
							"alamat" : alamat,
							"hp" : hp,
							"email" : email,
							"id_pend" : pend,
							"institusi" : institusi,
							"pengalaman" : pengalaman,
							"loker" : loker,
							"cabang" : cabang,
							"status" : status
						}
						if(save_method=="save"){
							var url = "' . base_url('karyawan/add_cal_kary') . '";
						}else{
							var url = "' . base_url('karyawan/update_cal_kary') . '";
						}
							$.ajax({
								url : url,
								type : "POST",
								data : dat,
								success : function(data){
									'.$this->Main_Model->notif200().'
									$("#nama").val("");
									$("#tgl_lahir").val("");
									$("#jenkel").val("");
									$("#alamat").val("");
									$("#hp").val("");
									$("#email").val("");
									$("#pend").val("");
									$("#institusi").val("");
									$("#pengalaman").val("");
									$(".select2").val("").trigger("change");
									$("#status").val("");
				    				load_table();
								},
								error : function(jqXHR,textStatus,errorThrown){
									bootbox.alert("Internal Server Error");
								}
							});
						
					}
				}
				function get_id(id){
					save_method = "update";
			    	id = {"id" : id}
			    	$.ajax({
			    		url : "' . base_url('karyawan/cal_kary_id') . '",
			    		type : "GET",
			    		data : id,
			    		success : function(data){
			    			$("#myModal").modal();
			    			var dat = jQuery.parseJSON(data);
							$("#nama").val(dat.nama);
							$("#tgl_lahir").val(dat.tgl_lahir);
							$("#jenkel").val(dat.jen_kel);
							$("#alamat").val(dat.alamat);
							$("#hp").val(dat.hp);
							$("#email").val(dat.email);
							$("#pend").val(dat.id_pend);
							$("#institusi").val(dat.institusi);
							$("#pengalaman").val(dat.pengalaman);
							$("#loker").val(dat.id_loker).trigger("change");
							$("#cabang").val(dat.id_cabang).trigger("change");
							$("#status").val(dat.status);
							$("#id").val(dat.id);
			    		},
			    		error : function(jqXHR, textStatus, errorThrown){
			    			alert("Internal Server Error");
			    		}
			    	});
			    }
			    '.$this->Main_Model->default_loadtable($url)

                 .$this->Main_Model->default_delete_data($url_del)

                 .$this->Main_Model->get_data(base_url('karyawan/det_calon_kary'), 'detail(id)', '
			     	$("#ModalDetail").modal();
			     	if(data.status==1){
			     		status = "dipertimbangkan";
			     	}else if(data.status==2){
			     		status = "disetujui";
			     	}else{
			     		status = "ditolak";
			     	}
			     	$("#textNama").html(data.nama);
					$("#textJenisKelamin").html(data.jen_kel);
					$("#textTanggalLahir").html(data.tanggalLahir);
					$("#textHP").html(data.hp);
					$("#textEmail").html(data.email);
					$("#textAlamat").html(data.alamat);
					$("#textPendidikan").html(data.pend);
					$("#textInstitusi").html(data.institusi);
					$("#textPengalaman").html(data.pengalaman);
					$("#textLoker").html(data.posisi);
					$("#textCabang").html(data.cabang);
					$("#textStatus").html(status);').'
			</script>
		';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer().'<script src="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js').'"></script>'
        );

        $loker  = $this->Rekrutmen_Model->view_datloker();
        $a = array();
        foreach ($loker as $row) {
            $a[$row->id_loker] = $row->posisi;
        }

        $cabang = $this->Karyawan_Model->view_cabang($this->session->userdata('idp'));
        $b = array();
        foreach ($cabang as $row) {
            $b[$row->id_cab] = $row->cabang;
        }

        $data       = array(
            'jenkel'=> array(
                'L' => 'Laki-laki',
                'P' => 'Perempuan'
            ),
            'pend'  => $this->Karyawan_Model->pendidikan(),
            'loker' => $a,
            'cabang'=> $b,
            'status'=> array(
                '1' => 'dipertimbangkan',
                '2' => 'disetujui',
                '3' => 'ditolak'
            )
        );
        $menu = array(
            'menu' => $this->Main_Model->menu_admin('0', '0', '5'),
            'style' => '
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
				<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">
				<link href="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css').'" rel="stylesheet" type="text/css" />'
        );
        $this->load->view('template/header', $menu);
        $this->load->view('karyawan/dat_calon_kary', $data);
        $this->load->view('template/footer', $footer);
    }
    
    function view_calon_kary()
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_calon_kary();
        $template   = $this->Main_Model->tbl_temp();
        $this->table->set_heading('No', 'Nama', 'JenKel', 'Alamat', 'No HP', 'Pendidikan', 'Status', 'Posisi yang dilamar', 'Cabang', 'Action');
        $no = 1;
        foreach ($data as $row) {
            ($row->jen_kel == "L") ? $jenkel = "Laki-laki" : $jenkel = "Perempuan";

            if ($row->status == "1") {
                $status = "dipertimbangkan";
            } elseif ($row->status == "2") {
                $status = "disetujui";
            } else {
                $status = "ditolak";
            }
            
            $this->table->add_row(
                $no++,
                $row->nama,
                $jenkel,
                $row->alamat,
                $row->hp,
                $row->institusi.' ('.$row->pend.')',
                $status,
                $row->posisi,
                $row->cabang,
                '<div class="btn-group">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                    	<li>
                            <a href="javascript:;" onclick="detail(' . $row->id . ');">
                                <i class="icon-delete"></i> Detail </a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="get_id(' . $row->id . ');">
                                <i class="icon-edit"></i> Update </a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="delete_data(' . $row->id . ');">
                                <i class="icon-delete"></i> Delete </a>
                        </li>
                    </ul>
            </div>'
            );
        }
        
        $this->table->set_template($template);
        echo $this->table->generate();
    }
    
    function add_cal_kary()
    {
        $this->Main_Model->get_login();
        $tgl_lahir = $this->Main_Model->convert_tgl($this->input->post('tgl_lahir'));
        $data      = array(
            'nama'      => $this->input->post('nama'),
            'tgl_lahir' => $tgl_lahir,
            'jen_kel'   => $this->input->post('jen_kel'),
            'alamat'    => $this->input->post('alamat'),
            'hp'        => $this->input->post('hp'),
            'email'     => $this->input->post('email'),
            'id_pend'   => $this->input->post('id_pend'),
            'institusi' => $this->input->post('institusi'),
            'pengalaman'=> $this->input->post('pengalaman'),
            'id_loker'  => $this->input->post('loker'),
            'id_cabang' => $this->input->post('cabang'),
            'status'    => $this->input->post('status')
        );
        $this->Karyawan_Model->add_cal_kary($data);
    }

    function cal_kary_id()
    {
        $this->Main_Model->get_login();
        $id   = $this->input->get('id');
        $data = $this->Karyawan_Model->cal_kary_id($id);
        
        echo json_encode($data);
    }
    
    function update_cal_kary()
    {
        $this->Main_Model->get_login();
        $id        = $this->input->post('id');
        $tgl_lahir = $this->Main_Model->convert_tgl($this->input->post('tgl_lahir'));
        $data      = array(
            'nama'      => $this->input->post('nama'),
            'tgl_lahir' => $tgl_lahir,
            'jen_kel'   => $this->input->post('jen_kel'),
            'alamat'    => $this->input->post('alamat'),
            'hp'        => $this->input->post('hp'),
            'email'     => $this->input->post('email'),
            'id_pend'   => $this->input->post('id_pend'),
            'institusi' => $this->input->post('institusi'),
            'pengalaman'=> $this->input->post('pengalaman'),
            'id_loker'  => $this->input->post('loker'),
            'id_cabang' => $this->input->post('cabang'),
            'status'    => $this->input->post('status')
        );
        $this->Karyawan_Model->update_cal_kary($data, $id);
    }
    
    function delete_cal_kary()
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Karyawan_Model->delete_cal_kary($id);
    }

    function cetak_sk($id_sk)
    {
        $this->Main_Model->get_login();
        $data = array(
            'sk' => $this->Karyawan_Model->sk_id($id_sk),
            'thp' => $this->Karyawan_Model->hitung_thp($id_sk)
            );
        $this->load->view('karyawan/cetak_sk', $data);
    }

    function bpjs($nip = '')
    {
        $this->Main_Model->get_login();
        $javascript =
        '<script>'.
            $this->Main_Model->default_datatable('bpjs_tk').
            $this->Main_Model->default_datatable('bpjs_ks')
        .'</script>';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );
        $kary           = $this->Main_Model->kary_nip($nip);
        $lokasi_kerja   = $this->Main_Model->lokasi_kerja($nip);
        $th_fk          = $this->Main_Model->th_fk();
        $fk             = $this->Main_Model->fk_lku(100, $th_fk->th);
        $umk            = isset($fk->umk)?$fk->umk:'';

        $head_tk        = $this->Main_Model->tbl_temp('bpjs_tk');
        $head_ks        = $this->Main_Model->tbl_temp('bpjs_ks');

        $tbl_tk         = $this->Penggajian_Model->view_bpjs();
        $tbl_ks         = $this->Karyawan_Model->bpjs_ks();

        $kelas          = $this->Main_Model->kelas_bpjs($nip);
        $kls_bpjs       = isset($kelas->kelas)?$kelas->kelas:'';


        $data       = array(
            'bpjs_tk'   => $this->tbl_bpjs_tk($tbl_tk, $head_tk, $umk),
            'bpjs_ks'   => $this->tbl_bpjs_ks($tbl_ks, $head_ks, $umk, $kls_bpjs),
            'kary'      => $this->Main_Model->kary_nip($nip)
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('karyawan/bpjs_nip', $data);
        $this->load->view('template/footer', $footer);
    }

    function tbl_bpjs_tk($data, $template, $umk)
    {
        $this->table->set_heading('No', 'Tahun', 'JKK', 'JKM', 'JHTTK', 'JHTP', 'JPTK', 'JPP', 'Total');
        $no     =1;
        $jkk    =$data->jkk*$umk/100;
        $jkm    =$data->jkm*$umk/100;
        $jhttk  =$data->jhttk*$umk/100;
        $jhtp   =$data->jhtp*$umk/100;
        $jptk   =$data->jptk*$umk/100;
        $jpp    =$data->jpp*$umk/100;
        $total  =$jkk+$jkm+$jhttk+$jhtp+$jptk+$jpp;

        $this->table->add_row(
            $no++,
            $data->th,
            $jkk,
            $jkm,
            $jhttk,
            $jhtp,
            $jptk,
            $jpp,
            $this->Main_Model->uang($total)
        );

        $this->table->set_template($template);
        return $this->table->generate();
    }
    
    function tbl_bpjs_ks($data, $template, $umk, $kelas)
    {
        if ($kelas) {
            if ($kelas == '1') {
                $umk = $data->kelas;
            }

            $this->table->set_heading('No', 'Tahun', 'TKS', 'PKS', 'Kelas', 'Total');
            $no     =1;
            $tks    =$data->tks*$umk/100;
            $pks    =$data->pks*$umk/100;
            $total  =$tks+$pks;

            $this->table->add_row(
                $no++,
                $data->th,
                $tks,
                $pks,
                $pks,
                $this->Main_Model->uang($total)
            );

            $this->table->set_template($template);
            return $this->table->generate();
        } else {
            return "BPJS KS Belum Diinput <a href='".base_url('penggajian/bpjs_ks')."'>input disini</a>";
        }
    }

    function form_kk($nip = '')
    {
        $this->Main_Model->get_login();
        $no_kk = $this->Karyawan_Model->get_kk($nip);
        $url = base_url('karyawan/view_kk')."/".$nip;
        $url_del = base_url('karyawan/del_kk');
        
        $javascript =
        '<script>
        	var save_method;
        	'.$this->Main_Model->default_loadtable($url).
              $this->Main_Model->default_datepicker().'

        	function reset(id) {
        		if(id==1) {
        			$(".form-control").val("");
        			save_method = "save";
        		} else {
        			$(".form-control").val("");
        			$(".alert").addClass("hidden");
        			save_method = "save";
        		}
        	}

        	function process_kk() {
	    		if(save_method=="save") {
	    			var url = "'.base_url('karyawan/save_no_kk').'";
	    		} else {
	    			var url = "'.base_url('karyawan/update_no_kk').'";
	    		}

	    		$.ajax({
	    			url : url,
	    			data : $("#formKK").serialize(),
	    			type : "POST",
	    			dataType : "JSON",
	    			success : function(data){
	    				if(data.status=="true") {
	    					'.$this->Main_Model->notif200().'
	    					load_table();
							load_kk();
							reset(1);
							location.reload();
	    				} else {
	    					'.$this->Main_Model->notif400().'
	    				}
	    			},
	    			error : function(jqXHR,textStatus,errorThrown){
	    				bootbox.alert("Oops Something Went Wrong!");
	    			}
	    		});
		    }

		    function edit_kk() {
		    	save_method = "update";
		    	$(".status").remove();
		    	$("#myModal").modal();
		    	$.ajax({
		    		url : "'.base_url('karyawan/get_kk').'/'.$nip.'",
		    		dataType : "JSON",
		    		success : function(data){
		    			$("#no_kk").val(data.no_kk);
		    		},
		    		error : function(jqXHR, textStatus, errorThrown){
		    			bootbox.alert("Oops Something Went Wrong!");
		    		}
		    	});
		    }

		    function load_kk() {
		    	$.ajax({
		    		url : "'.base_url('karyawan/get_kk').'/'.$nip.'",
		    		dataType: "JSON",
		    		success : function(data){
		    			$("#kk").html(data.no_kk);
		    		}
		    	});
		    }
		    load_kk();

		    function save() {
		    	$.ajax({
	    			url : "'.base_url('karyawan/process_add_kk').'",
	    			data : $("#add_kk").serialize(),
	    			type : "POST",
	    			dataType : "JSON",
	    			success : function(data){
	    				if(data.status == "true") {
	    					'.$this->Main_Model->notif200().'
	    					load_table();
							load_kk();
							reset(1);
	    				} else {
	    					'.$this->Main_Model->notif400().'
	    				}
	    			},
	    			error : function(jqXHR,textStatus,errorThrown){
	    				bootbox.alert("Oops Something Went Wrong!");
	    			}
	    		});
		    }

		    function get_id(id) {
		    	reset();
		    	$("#ModalTambah").modal();
		    	$.ajax({
		    		url : "'.base_url('karyawan/id_kk').'/"+id,
		    		dataType : "JSON",
		    		success : function(data){
		    			$("#id").val(data.id_kk);
		    			$("#nik").val(data.nik);
						$("#nama").val(data.nama);
						$("#jk").val(data.jk);
						$("#status2").val(data.status);
						$("#t_lahir").val(data.t_lahir);
						$("#tgl").val(data.tgl);
						$("#alamat").val(data.alamat);
						$("#isbpjs").val(data.flag);
		    		},
		    		error 	: function(jqXHR,textStatus,errorThrown){
		    			bootbox.alert("Oops Something Went Wrong!");
		    		} 
		    	});
		    }

		    '.$this->Main_Model->default_delete_data($url_del).'
        </script>';
        $footer = array(
            'javascript' => minifyjs($javascript),
            'js' => $this->footer()
        );

        $data = array(
            'kary' => $this->Main_Model->kary_nip($nip),
            'jk' => array(
                'L' => 'Laki-laki',
                'P' => 'Perempuan'
            ),
            'status' => array(
                'Kepala Keluarga' => 'Kepala Keluarga',
                'Istri' => 'Istri',
                'Anak' => 'Anak'
            )
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('karyawan/form_kk', $data);
        $this->load->view('template/footer', $footer);
    }

    function view_kk($nip = '')
    {
        $this->Main_Model->get_login();
        $data       = $this->Karyawan_Model->view_kk($nip);
        $template   = $this->Main_Model->tbl_temp();

        $this->table->set_heading('No', 'NIK', 'Nama', 'Status', 'JK', 'TTL', 'Alamat', 'Ikut BPJS', 'Action');
        $no=1;

        foreach ($data as $row) {
            if ($row->jk=="L") {
                $jk = "Laki-laki";
            } else {
                $jk = "Perempuan";
            }
            if ($row->t_lahir) {
                $ttl = $row->t_lahir."/".$this->Main_Model->format_tgl($row->tgl_lahir);
            } else {
                $ttl = $this->Main_Model->format_tgl($row->tgl_lahir);
            }
            if ($row->flag=='1') {
                $flag = "Ya";
            } else {
                $flag = "Tidak";
            }
            $this->table->add_row(
                $no++,
                $row->nik,
                $row->nama,
                $row->status,
                $jk,
                $ttl,
                $row->alamat,
                $flag,
                $this->Main_Model->default_action($row->id_kk)
            );
        }

        $this->table->set_template($template);
        echo $this->table->generate();
    }

    function save_no_kk()
    {
        $this->Main_Model->get_login();
        $nip    = $this->input->post("nip");
        $no_kk  = $this->input->post("no_kk");
        $status = $this->input->post("status");


        if (empty($nip) || empty($no_kk) || empty($status)) {
            $result = array('status'=>'false');
        } else {
            $q = $this->db->query("select * from kary where nip='$nip'")->row();

            $data = array(
                'no_kk'     => $no_kk,
                'nik'       => $q->ktp,
                'nama'      => $q->nama,
                'jk'        => $q->gend,
                'status'    => $status,
                't_lahir'   => $q->t_lahir,
                'tgl_lahir' => $q->tgl_lahir,
                'alamat'    => $q->alamat,
                'nip'       => $nip,
                'flag'      => '1'
                );

            $this->Karyawan_Model->save_no_kk($data);
            $result = array('status'=>'true');
        }

        echo json_encode($result);
    }

    function get_kk($nip)
    {
        $this->Main_Model->get_login();
        $data = $this->Karyawan_Model->get_kk($nip);

        echo json_encode($data);
    }

    function update_no_kk()
    {
        $this->Main_Model->get_login();
        $nip    = $this->input->post('nip');
        $no_kk  = $this->input->post('no_kk');

        if (empty($nip) || empty($no_kk)) {
            $result = array('status'=>'false');
        } else {
            $data   = array(
                'no_kk' => $no_kk,
            );
            $this->Karyawan_Model->update_no_kk($data, $nip);

            $result = array('status'=>'true');
        }

        echo json_encode($result);
    }

    function process_add_kk()
    {
        $this->Main_Model->get_login();
        $id         = $this->input->post('id');
        $nip        = $this->input->post('nip2');
        $nik        = $this->input->post('nik');
        $nama       = $this->input->post('nama');
        $jk         = $this->input->post('jk');
        $status     = $this->input->post('status2');
        $t_lahir    = $this->input->post('t_lahir');
        $tgl_lahir  = $this->input->post('tgl');
        $alamat     = $this->input->post('alamat');
        $isbpjs     = $this->input->post("isbpjs");

        if (empty($nip) || empty($nik) || empty($nama) || empty($jk) || empty($status) || empty($tgl_lahir) || empty($alamat) || $isbpjs=="") {
            $result = array('status'=>'false');
        } else {
            $q = $this->Karyawan_Model->get_kk($nip);

            $data = array(
                'no_kk'     => $q->no_kk,
                'nik'       => $nik,
                'nama'      => $nama,
                'jk'        => $jk,
                'status'    => $status,
                't_lahir'   => $t_lahir,
                'tgl_lahir' => $this->Main_Model->convert_tgl($tgl_lahir),
                'alamat'    => $alamat,
                'nip'       => $nip,
                'flag'      => $isbpjs
                );

            $this->Karyawan_Model->process_add_kk($data, $id);

            $result = array('status'=>'true');
        }

        echo json_encode($result);
    }

    function id_kk($id)
    {
        $this->Main_Model->get_login();
        $data = $this->Karyawan_Model->id_kk($id);

        echo json_encode($data);
    }

    function del_kk($id)
    {
        $this->Main_Model->get_login();
        $id = $this->input->post('id');
        $this->Karyawan_Model->del_kk($id);
    }

    function det_calon_kary($id)
    {
        $this->Main_Model->get_login();
        $data = $this->Karyawan_Model->det_calon_kary($id);
        echo json_encode($data);
    }

    function history_sk()
    {
        $this->Main_Model->get_login();
        $javascript = '
    		<script>
    			'.$this->Main_Model->default_datepicker()

                 .$this->Main_Model->notif().'

    			$("#download").click(function(){
				 	var date_start = $("#date_start").val();
				 	var date_end = $("#date_end").val();

				 	var a = date_start.split("/");
				 	var b = date_end.split("/");

				 	if (date_start == "" || date_end == "")
				 	{
				 		notif("Form masih kosong !");
				 	} 
				 	else if (new Date(a[1]+"/"+a[0]+"/"+a[2]) > new Date(b[1]+"/"+b[0]+"/"+b[2]))
				 	{
				 		notif("Format Tanggal Salah !")
				 	}
				 	else
				 	{	
				 		window.location.href="' . base_url('download_excel/history_sk') . '/"+a[2]+"-"+a[1]+"-"+a[0]+"/"+b[2]+"-"+b[1]+"-"+b[0];
				 	}	
				});
    		</script>
    	';
        $footer     = array(
            'javascript' => $javascript,
            'js' => $this->footer()
        );
        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
            );
        $this->load->view('template/header', $header);
        $this->load->view('karyawan/history_sk');
        $this->load->view('template/footer', $footer);
    }

    function form_user($nip = '')
    {
        $this->Main_Model->get_login();
        $javascript = '
			<script>
				$("#user").submit(function(event){
			        event.preventDefault();
			        formData = new FormData($(this)[0]);
			        $.ajax({
			            url : "'.base_url('karyawan/process_user').'",
			            type : "post",
			            data : formData,
			            async : false,
			            cache : false,
			            dataType : "json",
			            contentType : false,
			            processData : false,
			            success : function(data) {
			                if(data.status == 1) {
			                 	bootbox.dialog({
			                        message : data.message,
			                        buttons : {
			                            main : {
			                                label : "OK",
			                                className : "blue",
			                                callback : function(){
			                                    location.href="'.base_url('karyawan/datakaryawan').'";
			                                    return true;
			                                }
			                            }
			                        }
			                    })   
			                } else {
			                	bootbox.alert(data.message);
			                }
			            }
			        });
			        return false;
			    });
			</script>';
        $footer = array(
            'javascript' => minifyjs($javascript),
            'js' => $this->footer()
        );

        $karyawan = $this->Main_Model->view_by_id('kary', array('nip' => $nip), 'row');
        $nama = isset($karyawan->nama) ? $karyawan->nama : '';


        $data = array(
            'nip' => $nip,
            'nama' => $nama
        );

        $header = array(
            'style' => $this->header(),
            'menu' => $this->Main_Model->menu_admin('0', '0', '69')
        );
        $this->load->view('template/header', $header);
        $this->load->view('karyawan/form_user', $data);
        $this->load->view('template/footer', $footer);
    }

    function process_user()
    {
        $this->Main_Model->get_login();
        $nip = $this->input->post('nip');
        $username = $this->input->post('username');
        $acak = rand(000, 999);
        $password = 'gmedia'.$acak;

        $karyawan = $this->Main_Model->view_by_id('kary', array('nip' => $nip), 'row');
        $email = isset($karyawan->mail) ? $karyawan->mail : '';
        $nama = isset($karyawan->nama) ? $karyawan->nama : '';

        if ($username == '') {
            $status = 0;
            $message = 'Masukkan username';
        } else {
            $data = $this->Karyawan_Model->check_user($username);

            if (! empty($data)) {
                $status = 0;
                $message = 'Username sudah ada';
            } else {
                $simpan = $this->Karyawan_Model->simpan_user($username, $nip, $password, $nama);
                if ($simpan > 0) {
                     $pesan = "<table width='500'>
                      <tr rowspan='3'><td colspan='2'><center><h3>Login Sistem Absensi</h3></center></td></tr
                      <tr><td colspan='2'>Sekarang anda telah memiliki akun untuk login sistem absensi dengan : </td></tr>
                      <tr><td>Username</td><td>: ".$username."</td></tr>
                      <tr><td>Password</td><td>: ".$password."</td></tr> 
                      <tr><td colspan='2'>&nbsp;</td></tr> 
                      <tr rowspan='3' border='1'><td border='1' colspan='2' align='center'><a href='https://play.google.com/store/apps/details?id=gmedia.net.id.absenigmedia'>Silahkan klik untuk login sistem absensi.</a></td></tr>
                     <table>";

                    $from = 'hrd.smg@gmedia.co.id';
                    $subject = 'Login Sistem Absensi';

                    // $this->Main_Model->send_mail($to, $pesan, $subject, $from);

                    $this->load->library('email');
                    $config = array(
                        'protocol' => 'smtp',
                        'smtp_host' => '111.68.27.2',
                        'smtp_port' => '25',
                        'smtp_user' => $from, // change it to yours
                        'smtp_timeout' => '7',
                        'mailtype' => 'html',
                        'charset' => 'iso-8859-1',
                        'wordwrap' => true
                    );

                    $this->email->initialize($config);
                    $this->email->set_newline("\r\n");
                    $this->email->from($from, '[Aplikasi HRD] Gmedia'); // change it to yours
                    $this->email->to($email);// change it to yours
                    $this->email->subject($subject);
                    $this->email->message($pesan);
                    $this->email->send();

                    $status = 1;
                    $message = 'User berhasil dibuat';
                } else {
                    $status = 0;
                    $message = 'Username gagal disimpan, ulangi beberapa saat lagi';
                }
            }
        }

        $result = array(
            'status' => $status,
            'message' => $message
        );

        echo json_encode($result);
    }

    function reset_password($nip = '')
    {
        $this->Main_Model->get_login();
        $acak = rand(000, 999);
        $password = 'gmedia'.$acak;

        $karyawan = $this->Main_Model->view_by_id('kary', array('nip' => $nip), 'row');
        $email = isset($karyawan->mail) ? $karyawan->mail : '';
        $nama = isset($karyawan->nama) ? $karyawan->nama : '';

        $data_user = $this->Karyawan_Model->view_user($nip);
        $username = isset($data_user->username) ? $data_user->username : '';

        $from = 'hrd.smg@gmedia.co.id';
        $subject = 'Perubahan Login Sistem Absensi';
        $pesan = "<table width='500'>
                      <tr rowspan='3'><td colspan='2'><center><h3>Perubahan Login Sistem Absensi</h3></center></td></tr
                      <tr><td colspan='2'>Sekarang anda telah memiliki akun untuk login sistem absensi dengan : </td></tr>
                      <tr><td>Username</td><td>: ".$username."</td></tr>
                      <tr><td>Password</td><td>: ".$password."</td></tr> 
                      <tr><td colspan='2'>&nbsp;</td></tr> 
                      <tr rowspan='3' border='1'><td border='1' colspan='2' align='center'><a href='https://play.google.com/store/apps/details?id=gmedia.net.id.absenigmedia'>Silahkan klik untuk login sistem absensi.</a></td></tr>
                     <table>";

        $this->load->library('email');
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => '111.68.27.2',
            'smtp_port' => '25',
            'smtp_user' => $from, // change it to yours
            'smtp_timeout' => '7',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => true
        );

        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->from($from, '[Aplikasi HRD] Gmedia'); // change it to yours
        $this->email->to($email);// change it to yours
        $this->email->subject($subject);
        $this->email->message($pesan);
        // $this->email->bcc('victor.fendi@gmedia.co.id');
        $this->email->send();

        // show_error($this->email->print_debugger());

        $simpan = $this->Karyawan_Model->update_user($nip, $password);
    }

    function dt_karyawan_aktif()
	{
		$is_ajax = $this->input->is_ajax_request();
		if ($is_ajax) {
			$draw = $this->input->get('draw');
			$start = $this->input->get('start');
			$length = $this->input->get('length');
			$search = $this->input->get('search');
			$order = $this->input->get('order');

			$json = $this->Karyawan_Model->dt_karyawan_aktif($draw, $start, $length, $search['value'], $order[0]['column'], $order[0]['dir']);

			echo json_encode($json);
		} else {
			show_404();
		}
	}

	function select2_karyawan()
	{
		$this->Main_Model->get_login();

		$cabang = $this->input->get('cabang');

		$data = $this->Main_Model->view_by_id('karyawan', ['id_cabang' => $cabang, 'tgl_resign' => null], 'result');

		$response = [];

		if ($data) {
			foreach ($data as $row) {
				$response[] = [
					'nik' => $row->nip,
					'nama' => $row->nama
				];
			}
		}

		echo json_encode(
			$response
		);
	}
}
