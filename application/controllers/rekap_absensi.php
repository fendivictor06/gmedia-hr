<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rekap_Absensi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Main_Model', '', TRUE);
		$this->load->model('Rekap_Absensi_Model', '', TRUE);
	}

	function index()
	{
		$this->Main_Model->all_login();
		$sess_cabang = $this->Main_Model->session_cabang();

		$js = $this->Main_Model->js_datatable()
		      .$this->Main_Model->js_modal()
		      .$this->Main_Model->js_bootbox()
		      .$this->Main_Model->js_select2()
		      .$this->Main_Model->js_timepicker()
		      .$this->Main_Model->js_datepicker()
		      .$this->Main_Model->js_bs_select()
		      .$this->Main_Model->js_button_excel();

		$menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');

		$header = array(
			'menu' => $menu,
			'style' => $this->Main_Model->style_datatable()
				      .$this->Main_Model->style_modal()
				      .$this->Main_Model->style_select2()
				      .$this->Main_Model->style_timepicker()
				      .$this->Main_Model->style_datepicker()
				      .$this->Main_Model->style_bs_select()
			);

		$nip_opt = array();
		$nip = (empty($sess_cabang)) ? $this->Main_Model->kary_active() : $this->Main_Model->kary_cabang($sess_cabang, 'q');
		if($nip) {
			foreach($nip as $row) {
				$nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
			} 
		}

		$cabang = array();

		if (empty($sess_cabang)) $cabang[] = 'Semua Cabang';
		if(!empty($sess_cabang)) {
			foreach($sess_cabang as $r) {
				$d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
				$cabang[$d->id_cab] = $d->cabang;
			}
		} else {
			$d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
			foreach($d as $row) {
				$cabang[$row->id_cab] = $row->cabang;
			}
		}

		$data = array(
			'footer' => $this->Main_Model->footer($js),
			'penutup' => $this->Main_Model->close_page(),
			'nip' => $nip_opt,
			'cabang' => $cabang
		);

		$this->load->view('template/header', $header);
		$this->load->view('absensi/rekap_absensi', $data);
	}

      function laporan_rekap_absensi()
      {
            $this->Main_Model->all_login();
            $sess_cabang = $this->Main_Model->session_cabang();

            $js = $this->Main_Model->js_datatable()
                  .$this->Main_Model->js_modal()
                  .$this->Main_Model->js_bootbox()
                  .$this->Main_Model->js_select2()
                  .$this->Main_Model->js_timepicker()
                  .$this->Main_Model->js_datepicker()
                  .$this->Main_Model->js_bs_select()
                  .$this->Main_Model->js_button_excel();

            $menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');

            $header = array(
                  'menu' => $menu,
                  'style' => $this->Main_Model->style_datatable()
                              .$this->Main_Model->style_modal()
                              .$this->Main_Model->style_select2()
                              .$this->Main_Model->style_timepicker()
                              .$this->Main_Model->style_datepicker()
                              .$this->Main_Model->style_bs_select()
                  );

            $nip_opt = array();
            $nip = (empty($sess_cabang)) ? $this->Main_Model->kary_active() : $this->Main_Model->kary_cabang($sess_cabang, 'q');
            if($nip) {
                  foreach($nip as $row) {
                        $nip_opt[$row->nip] = $row->nip.' - '.$row->nama;
                  } 
            }

            $cabang = array();

            if (empty($sess_cabang)) $cabang[] = 'Semua Cabang';
            if(!empty($sess_cabang)) {
                  foreach($sess_cabang as $r) {
                        $d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
                        $cabang[$d->id_cab] = $d->cabang;
                  }
            } else {
                  $d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
                  foreach($d as $row) {
                        $cabang[$row->id_cab] = $row->cabang;
                  }
            }

            $data = array(
                  'footer' => $this->Main_Model->footer($js),
                  'penutup' => $this->Main_Model->close_page(),
                  'nip' => $nip_opt,
                  'cabang' => $cabang
            );

            $this->load->view('template/header', $header);
            $this->load->view('absensi/laporan_rekap_absensi', $data);
      }

      function view_rekap_absensi($date_start='', $date_end='')
      {
            // $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $data = $this->Rekap_Absensi_Model->laporan_terlambat($date_start, $date_end);
            $table = '<center><h1 style="color:#265180"> Laporan Absensi </h1></center><br>';
            $table .= '<span style="font-size:14px;">Tanggal <strong>'.hari_tgl($date_start).'</strong> sampai dengan tanggal <strong>'.hari_tgl($date_end).'</strong></span><br><br>';
            $table .= '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
            $table .= '<thead><tr>
                              <th style="text-align: center; vertical-align: middle;">RANK</th>
                              <th style="text-align: center; vertical-align: middle;">NIK</th>
                              <th style="text-align: center; vertical-align: middle;">NAMA</th>
                              <th style="text-align: center; vertical-align: middle;">TERLAMBAT <br> [JUMLAH HARI]</th>
                              <th style="text-align: center; vertical-align: middle;">TERLAMBAT <br> [JUMLAH MENIT]</th>
                              <th style="text-align: center; vertical-align: middle;">PULANG AWAL <br> [JUMLAH HARI]</th>
                              <th style="text-align: center; vertical-align: middle;">PULANG AWAL <br> [JUMLAH MENIT]</th>
                              <th style="text-align: center; vertical-align: middle;">TIDAK ABSEN MASUK</th>
                              <th style="text-align: center; vertical-align: middle;">TIDAK ABSEN PULANG</th>
                         </tr> </thead>';

            if (! empty($data)) {
                  $no = 1;
                  foreach ($data as $row) {
                        $table .= '<tr>';
                        $table .= '<td>'.$no++.'</td>';
                        $table .= '<td>'.$row->nip.'</td>';
                        $table .= '<td>'.$row->nama.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_telat.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_menit_telat.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_plg_awal.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_menit_plg_awal.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_tdk_absen_masuk.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_tdk_absen_pulang.'</td>';
                        $table .= '</tr>';
                  }
            }

            echo $table;
      }

      function download_rekap_absensi()
      {
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $data = $this->Rekap_Absensi_Model->laporan_terlambat($date_start, $date_end);
            $table = '<center><h1 style="color:#265180"> Laporan Absensi </h1></center><br>';
            $table .= '<span style="font-size:14px;">Tanggal <strong>'.hari_tgl($date_start).'</strong> sampai dengan tanggal <strong>'.hari_tgl($date_end).'</strong></span><br><br>';
            $table .= '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
            $table .= '<thead><tr>
                              <th style="text-align: center; vertical-align: middle;">RANK</th>
                              <th style="text-align: center; vertical-align: middle;">NIK</th>
                              <th style="text-align: center; vertical-align: middle;">NAMA</th>
                              <th style="text-align: center; vertical-align: middle;">TERLAMBAT <br> [JUMLAH HARI]</th>
                              <th style="text-align: center; vertical-align: middle;">TERLAMBAT <br> [JUMLAH MENIT]</th>
                              <th style="text-align: center; vertical-align: middle;">PULANG AWAL <br> [JUMLAH HARI]</th>
                              <th style="text-align: center; vertical-align: middle;">PULANG AWAL <br> [JUMLAH MENIT]</th>
                              <th style="text-align: center; vertical-align: middle;">TIDAK ABSEN MASUK</th>
                              <th style="text-align: center; vertical-align: middle;">TIDAK ABSEN PULANG</th>
                         </tr> </thead>';

            if (! empty($data)) {
                  $no = 1;
                  foreach ($data as $row) {
                        $table .= '<tr>';
                        $table .= '<td>'.$no++.'</td>';
                        $table .= '<td>'.$row->nip.'</td>';
                        $table .= '<td>'.$row->nama.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_telat.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_menit_telat.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_plg_awal.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_menit_plg_awal.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_tdk_absen_masuk.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_tdk_absen_pulang.'</td>';
                        $table .= '</tr>';
                  }
            }

            $data = array(
                  'html' => $table,
                  'title' => 'Laporan Absensi'
            );

            $this->load->view('file_download/terlambat', $data);
            $filename = 'Laporan Absensi '.hari_tgl($date_start).' sd '.hari_tgl($date_end);
            $html = $this->output->get_output();
            $this->load->library('pdfgenerator');
            $output = $this->pdfgenerator->generate($html, $filename, true, 'legal', 'landscape');
      }

	function view_rekap()
	{
		$this->Main_Model->all_login();
		$cabang = $this->input->get('cabang');
		$date_start = $this->input->get('date_start');
		$date_end = $this->input->get('date_end');

		$date_start = $this->Main_Model->convert_tgl($date_start);
		$date_end = $this->Main_Model->convert_tgl($date_end);

		$table = $this->Rekap_Absensi_Model->view_absensi($cabang, $date_start, $date_end);

		echo $table;
	}

      function rekap_excel()
      {
            $this->Main_Model->all_login();

            // header("Content-type: application/vnd-ms-excel");
            // header("Content-Disposition: attachment; filename=Rekapitulasi Absensi ".hari_tgl($date_start)." sd ".hari_tgl($date_end).".xls"); 

            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $tanggal_awal = hari_tgl($date_start);
            $tanggal_akhir = hari_tgl($date_end);

            $table = '<center><h1 style="color:#265180; font-size:18px;"> Laporan Rekapitulasi </h1></center><br>';
            $table .= '<span style="font-size:14px;">Tanggal <strong>'.$tanggal_awal.'</strong> sampai dengan tanggal <strong>'.$tanggal_akhir.'</strong></span><br><br>';
            $table .= $this->Rekap_Absensi_Model->view_absensi($cabang, $date_start, $date_end);

            $data = array(
                  'html' => $table,
                  'title' => 'Rekapitulasi Absensi '.hari_tgl($date_start).' sd '.hari_tgl($date_end)
            );

            $this->load->view('file_download/rekapitulasi_absensi', $data);

            $filename = 'Rekapitulasi Absensi '.tgl_api($date_start).' sd '.tgl_api($date_end).'.xls';
            header("Content-type: application/vnd-ms-excel");
            header("Content-Disposition: attachment; filename=$filename");
      }

      function rekap_excel_old()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $periode = tgl_indo($date_start).' s/d '.tgl_indo($date_end);
            $title = 'Rekap Absensi '.$periode;
            $arr = explode(" ", $title);
            $title = implode("_", $arr);

              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);

              define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

              require_once(APPPATH.'libraries/PHPExcel.php');
              require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Absensi");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Absensi");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Absensi");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Absensi'); //sheet title
            $objset->setCellValue('A1','Rekap Absensi '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            // range tgl
            $date_range = date_range($date_start, $date_end);
            $jumlah_range = count($date_range);

            // Header Tabel
            $head_val = array('NO', 'NIP', 'NAMA', 'CABANG', 'DIVISI');
            $jumlah_head_val = count($head_val);
            for ($i = 0; $i < $jumlah_head_val; $i++) {
                  $a = $i + 1;
                  $objset->setCellValue(alphabet($a).'3', $head_val[$i]);
            }
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG MAKAN');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG LEMBUR');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG DENDA');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'BPJS KETENAGAKERJAAN');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'CICILAN PINJAMAN');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'KETERANGAN');

            // Header Tanggal
            $range_head = count($head_val);
            for ($b = 1; $b <= 3; $b++) {
                  for ($i = 0; $i < $jumlah_range; $i++) {
                        $a = $i + 1;
                        $tanggal = explode(' s/d ', $date_range[$i]);
                        $awal = tgl_indo($tanggal[0]);
                        $akhir = tgl_indo(end($tanggal));
                        $awal_akhir[$i] = $awal.' s/d '.$akhir;

                        $jarak = $range_head + $a;
                        $objset->setCellValue(alphabet($jarak).'4', $awal_akhir[$i]);

                        $objPHPExcel->getActiveSheet()->getStyle(alphabet($jarak).'4:'.alphabet($jarak).'4')->getAlignment()->setWrapText(true);
                  }

                  $range_head = $range_head + $jumlah_range + 1;
            }

            $baris = 5;
            $data = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end, 0);
            $no = 1;

            $total_data = count((array)$data) + 1;
            $total_uang_makan = 0;
            $total_uang_lembur = 0;
            $total_uang_denda = 0;
            $total_bpjs = 0;
            foreach ($data as $r) {
                  $objset->setCellValue("A".$baris, $no++);
                  $objset->setCellValue("B".$baris, $r->nip);
                  $objset->setCellValue("C".$baris, $r->nama);
                  $objset->setCellValue("D".$baris, $r->cabang);
                  $objset->setCellValue("E".$baris, $r->divisi);

                  $range_head = count($head_val);
                  for ($b = 1; $b <= 3; $b++) {
                        $total = 0;
                        for ($i = 0; $i < $jumlah_range; $i++) {
                              $a = $i + 1;
                              $tanggal = explode(' s/d ', $date_range[$i]);
                              $awal = $tanggal[0];
                              $akhir = end($tanggal);

                              if ($b == 1) {
                                    $um = $this->Rekap_Absensi_Model->uang_makan($awal, $akhir, $r->nip);
                                    $uang[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                                    $total_uang_makan = $total_uang_makan + $uang[$i];
                              } elseif ($b == 2) {
                                    $um = $this->Rekap_Absensi_Model->uang_lembur($awal, $akhir, $r->nip);
                                    $uang[$i] = isset($um->uang_lembur) ? $um->uang_lembur : 0;
                                    $total_uang_lembur = $total_uang_lembur + $uang[$i];
                              } else {
                                    $um = $this->Rekap_Absensi_Model->uang_denda($awal, $akhir, $r->nip);
                                    $uang[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                                    $total_uang_denda = $total_uang_denda + $uang[$i];
                              }

                              $total = $total + $uang[$i];

                              $jarak = $range_head + $a;
                              $objset->setCellValue(alphabet($jarak).$baris, $uang[$i]);
                        }
                        $objset->setCellValue(alphabet($jarak + 1).$baris, $total);
                        $range_head = $range_head + $jumlah_range + 1;
                  }

                  // cicilan
                  $c = $this->Rekap_Absensi_Model->cicilan($date_start, $date_end, $r->nip);
                  $cicilan = isset($c->angsuran) ? $c->angsuran : 0;
                  $keterangan = isset($c->keterangan) ? $c->keterangan : '';

                  // bpjs ketenagakerjaan
                  $bpjs_tk = $this->Rekap_Absensi_Model->bpjs_ketenagakerjaan($r->nip);
                  $nominal_bpjs = isset($bpjs_tk->total_k) ? $bpjs_tk->total_k : 0;
                  $total_bpjs = $total_bpjs + $nominal_bpjs;

                  $jarak_bpjs = (count($head_val) + ($jumlah_range * 3)) + 4;
                  $objset->setCellValue(alphabet($jarak_bpjs).$baris, round($nominal_bpjs));
                  $jarak_bpjs = $jarak_bpjs + 1;
                  $objset->setCellValue(alphabet($jarak_bpjs).$baris, $cicilan);
                  $jarak_bpjs = $jarak_bpjs + 1;
                  $objset->setCellValue(alphabet($jarak_bpjs).$baris, $keterangan);

                  if ($total_data == $no) {
                        $baris = $baris + 1;
                        $jarak_total = count($head_val) + $jumlah_range;
                        $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                        $jarak_total = $jarak_total + 1;
                        $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_makan);
                        $jarak_total = $jarak_total + $jumlah_range;
                        $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                        $jarak_total = $jarak_total + 1;
                        $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_lembur);
                        $jarak_total = $jarak_total + $jumlah_range;
                        $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                        $jarak_total = $jarak_total + 1;
                        $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_denda);
                        $jarak_total = $jarak_total + 1;
                        $objset->setCellValue(alphabet($jarak_total).$baris, $total_bpjs);
                  }
           
                  $baris++;
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Absensi');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
      }

	function rekap_excel_by_divisi()
	{
		$this->Main_Model->all_login();
		$cabang = $this->input->get('cabang');
		$date_start = $this->input->get('date_start');
		$date_end = $this->input->get('date_end');

		$date_start = $this->Main_Model->convert_tgl($date_start);
		$date_end = $this->Main_Model->convert_tgl($date_end);

		$periode = tgl_indo($date_start).' s/d '.tgl_indo($date_end);
            $title = 'Rekap Absensi '.$periode;
            $arr = explode(" ", $title);
            $title = implode("_", $arr);

              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);

              define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

              require_once(APPPATH.'libraries/PHPExcel.php');
              require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Absensi");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Absensi");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Absensi");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Absensi'); //sheet title
            $objset->setCellValue('A1','Rekap Absensi '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            // range tgl
            $date_range = date_range($date_start, $date_end);
            $jumlah_range = count($date_range);

            // Header Tabel
            $head_val = array('NO', 'NIP', 'NAMA', 'CABANG', 'DIVISI');
            $jumlah_head_val = count($head_val);
            for ($i = 0; $i < $jumlah_head_val; $i++) {
                  $a = $i + 1;
                  $objset->setCellValue(alphabet($a).'3', $head_val[$i]);
            }
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG MAKAN');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG LEMBUR');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG DENDA');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'BPJS KETENAGAKERJAAN');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'CICILAN PINJAMAN');
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'KETERANGAN');

            // Header Tanggal
            $range_head = count($head_val);
            for ($b = 1; $b <= 3; $b++) {
                  for ($i = 0; $i < $jumlah_range; $i++) {
                        $a = $i + 1;
                        $tanggal = explode(' s/d ', $date_range[$i]);
                        $awal = tgl_indo($tanggal[0]);
                        $akhir = tgl_indo(end($tanggal));
                        $awal_akhir[$i] = $awal.' s/d '.$akhir;

                        $jarak = $range_head + $a;
                        $objset->setCellValue(alphabet($jarak).'4', $awal_akhir[$i]);

                        $objPHPExcel->getActiveSheet()->getStyle(alphabet($jarak).'4:'.alphabet($jarak).'4')->getAlignment()->setWrapText(true);
                  }

                  $range_head = $range_head + $jumlah_range + 1;
            }

            $baris = 5;
            //divisi 
            $d = $this->Rekap_Absensi_Model->divisi();
            if (!empty($d)) {
            	foreach ($d as $row) {
                        //karyawan
            		$data = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end, $row->id_divisi);
                        $objset->setCellValue("A".$baris, $row->divisi);
            		$no = 1;
            		$i = 1;
                        $baris = $baris + $i;
                        $total_data = count((array)$data) + 1;
                        $total_uang_makan = 0;
                        $total_uang_lembur = 0;
                        $total_uang_denda = 0;
            		foreach ($data as $r) {
	            		$objset->setCellValue("A".$baris, $no++);
                              $objset->setCellValue("B".$baris, $r->nip);
                              $objset->setCellValue("C".$baris, $r->nama);
                              $objset->setCellValue("D".$baris, $r->cabang);
                              $objset->setCellValue("E".$baris, $r->divisi);

                              $range_head = count($head_val);
                              for ($b = 1; $b <= 3; $b++) {
                                    $total = 0;
                                    for ($i = 0; $i < $jumlah_range; $i++) {
                                          $a = $i + 1;
                                          $tanggal = explode(' s/d ', $date_range[$i]);
                                          $awal = $tanggal[0];
                                          $akhir = end($tanggal);

                                          if ($b == 1) {
                                                $um = $this->Rekap_Absensi_Model->uang_makan($awal, $akhir, $r->nip);
                                                $uang[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                                                $total_uang_makan = $total_uang_makan + $uang[$i];
                                          } elseif ($b == 2) {
                                                $um = $this->Rekap_Absensi_Model->uang_lembur($awal, $akhir, $r->nip);
                                                $uang[$i] = isset($um->uang_lembur) ? $um->uang_lembur : 0;
                                                $total_uang_lembur = $total_uang_lembur + $uang[$i];
                                          } else {
                                                $um = $this->Rekap_Absensi_Model->uang_denda($awal, $akhir, $r->nip);
                                                $uang[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                                                $total_uang_denda = $total_uang_denda + $uang[$i];
                                          }

                                          $total = $total + $uang[$i];

                                          $jarak = $range_head + $a;
                                          $objset->setCellValue(alphabet($jarak).$baris, $uang[$i]);
                                    }
                                    $objset->setCellValue(alphabet($jarak + 1).$baris, $total);
                                    $range_head = $range_head + $jumlah_range + 1;
                              }

                              // cicilan
                              $c = $this->Rekap_Absensi_Model->cicilan($date_start, $date_end, $r->nip);
                              $cicilan = isset($c->angsuran) ? $c->angsuran : 0;
                              $keterangan = isset($c->keterangan) ? $c->keterangan : '';

                              // bpjs ketenagakerjaan
                              $bpjs_tk = $this->Rekap_Absensi_Model->bpjs_ketenagakerjaan($r->nip);
                              $nominal_bpjs = isset($bpjs_tk->total_k) ? $bpjs_tk->total_k : 0;

                              $jarak_bpjs = (count($head_val) + ($jumlah_range * 3)) + 4;
                              $objset->setCellValue(alphabet($jarak_bpjs).$baris, $nominal_bpjs);
                              $jarak_bpjs = $jarak_bpjs + 1;
                              $objset->setCellValue(alphabet($jarak_bpjs).$baris, $cicilan);
                              $jarak_bpjs = $jarak_bpjs + 1;
                              $objset->setCellValue(alphabet($jarak_bpjs).$baris, $keterangan);

                              if ($total_data == $no) {
                                    $baris = $baris + 1;
                                    $jarak_total = count($head_val) + $jumlah_range;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                                    $jarak_total = $jarak_total + 1;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_makan);
                                    $jarak_total = $jarak_total + $jumlah_range;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                                    $jarak_total = $jarak_total + 1;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_lembur);
                                    $jarak_total = $jarak_total + $jumlah_range;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                                    $jarak_total = $jarak_total + 1;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_denda);
                              }
	                 
                              $baris++;
	            	}
                        $i++; 
            	}
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Absensi');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
	}

      function uang_makan()
      {
            $this->Main_Model->all_login();
            $sess_cabang = $this->Main_Model->session_cabang();

            $js = $this->Main_Model->js_datatable()
                  .$this->Main_Model->js_modal()
                  .$this->Main_Model->js_bootbox()
                  .$this->Main_Model->js_select2()
                  .$this->Main_Model->js_timepicker()
                  .$this->Main_Model->js_datepicker()
                  .$this->Main_Model->js_bs_select()
                  .$this->Main_Model->js_button_excel();

            $menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');

            $header = array(
                  'menu' => $menu,
                  'style' => $this->Main_Model->style_datatable()
                              .$this->Main_Model->style_modal()
                              .$this->Main_Model->style_select2()
                              .$this->Main_Model->style_timepicker()
                              .$this->Main_Model->style_datepicker()
                              .$this->Main_Model->style_bs_select()
                  );

            $cabang = array();
            if (empty($sess_cabang)) $cabang[] = 'Semua Cabang';
            if(!empty($sess_cabang)) {
                  foreach($sess_cabang as $r) {
                        $d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
                        $cabang[$d->id_cab] = $d->cabang;
                  }
            } else {
                  $d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
                  foreach($d as $row) {
                        $cabang[$row->id_cab] = $row->cabang;
                  }
            }

            $data = array(
                  'footer' => $this->Main_Model->footer($js),
                  'penutup' => $this->Main_Model->close_page(),
                  'cabang' => $cabang
            );

            $this->load->view('template/header', $header);
            $this->load->view('absensi/laporan_uang_makan', $data);
      }

      function view_uang_makan()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $table = $this->Rekap_Absensi_Model->view_uang_makan($cabang, $date_start, $date_end);

            echo $table;
      }

      function rekap_uang_makan()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $periode = tgl_indo($date_start).' s/d '.tgl_indo($date_end);
            $title = 'Rekap Uang Makan '.$periode;
            $arr = explode(" ", $title);
            $title = implode("_", $arr);

              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);

              define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

              require_once(APPPATH.'libraries/PHPExcel.php');
              require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Uang Makan");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Uang Makan");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Uang Makan");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Uang Makan'); //sheet title
            $objset->setCellValue('A1','Rekap Uang Makan '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            // range tgl
            $date_range = range_to_date($date_start, $date_end);
            $jumlah_range = count($date_range);

            // Header Tabel
            $head_val = array('NO', 'NIP', 'NAMA', 'CABANG', 'DIVISI');
            $jumlah_head_val = count($head_val);
            for ($i = 0; $i < $jumlah_head_val; $i++) {
                  $a = $i + 1;
                  $objset->setCellValue(alphabet($a).'3', $head_val[$i]);
            }
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG MAKAN');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');

            // Header Tanggal
            $range_head = count($head_val);
            for ($i = 0; $i < $jumlah_range; $i++) {
                  $a = $i + 1;
                  $tanggal[$i] = tgl_indo($date_range[$i]);

                  $jarak = $range_head + $a;
                  $objset->setCellValue(alphabet($jarak).'4', $tanggal[$i]);

                  $objPHPExcel->getActiveSheet()->getStyle(alphabet($jarak).'4:'.alphabet($jarak).'4')->getAlignment()->setWrapText(true);
            }

            $baris = 5;
            //divisi 
            $d = $this->Rekap_Absensi_Model->divisi();
            if (!empty($d)) {
                  foreach ($d as $row) {
                        //karyawan
                        $data = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end, $row->id_divisi);
                        $objset->setCellValue("A".$baris, $row->divisi);
                        $no = 1;
                        $i = 1;
                        $baris = $baris + $i;
                        $total_data = count((array)$data) + 1;
                        $total_uang_makan = 0;

                        foreach ($data as $r) {
                              $objset->setCellValue("A".$baris, $no++);
                              $objset->setCellValue("B".$baris, $r->nip);
                              $objset->setCellValue("C".$baris, $r->nama);
                              $objset->setCellValue("D".$baris, $r->cabang);
                              $objset->setCellValue("E".$baris, $r->divisi);

                              $range_head = count($head_val);
                              $total = 0;
                              for ($i = 0; $i < $jumlah_range; $i++) {
                                    $a = $i + 1;

                                    $um = $this->Rekap_Absensi_Model->uang_makan($date_range[$i], $date_range[$i], $r->nip);
                                    $uang[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                                    $total_uang_makan = $total_uang_makan + $uang[$i];

                                    $total = $total + $uang[$i];

                                    $jarak = $range_head + $a;
                                    $laporan = $this->Rekap_Absensi_Model->laporan($r->nip, $date_range[$i]);
                                    $objset->setCellValue(alphabet($jarak).$baris, $laporan);
                              }
                              $objset->setCellValue(alphabet($jarak + 1).$baris, $total);

                              if ($total_data == $no) {
                                    $baris = $baris + 1;
                                    $jarak_total = count($head_val) + $jumlah_range;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                                    $jarak_total = $jarak_total + 1;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_makan);
                              }
                       
                              $baris++;
                        }
                        $i++; 
                  }
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Uang Makan');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
      }

      function uang_lembur()
      {
            $this->Main_Model->all_login();
            $sess_cabang = $this->Main_Model->session_cabang();

            $js = $this->Main_Model->js_datatable()
                  .$this->Main_Model->js_modal()
                  .$this->Main_Model->js_bootbox()
                  .$this->Main_Model->js_select2()
                  .$this->Main_Model->js_timepicker()
                  .$this->Main_Model->js_datepicker()
                  .$this->Main_Model->js_bs_select()
                  .$this->Main_Model->js_button_excel();

            $menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');

            $header = array(
                  'menu' => $menu,
                  'style' => $this->Main_Model->style_datatable()
                              .$this->Main_Model->style_modal()
                              .$this->Main_Model->style_select2()
                              .$this->Main_Model->style_timepicker()
                              .$this->Main_Model->style_datepicker()
                              .$this->Main_Model->style_bs_select()
                  );

            $cabang = array();
            if (empty($sess_cabang)) $cabang[] = 'Semua Cabang';
            if(!empty($sess_cabang)) {
                  foreach($sess_cabang as $r) {
                        $d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
                        $cabang[$d->id_cab] = $d->cabang;
                  }
            } else {
                  $d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
                  foreach($d as $row) {
                        $cabang[$row->id_cab] = $row->cabang;
                  }
            }

            $data = array(
                  'footer' => $this->Main_Model->footer($js),
                  'penutup' => $this->Main_Model->close_page(),
                  'cabang' => $cabang
            );

            $this->load->view('template/header', $header);
            $this->load->view('absensi/laporan_uang_lembur', $data);
      }

      function view_uang_lembur()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $table = $this->Rekap_Absensi_Model->view_uang_lembur($cabang, $date_start, $date_end);

            echo $table;
      }

      function rekap_uang_lembur()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $periode = tgl_indo($date_start).' s/d '.tgl_indo($date_end);
            $title = 'Rekap Uang Lembur '.$periode;
            $arr = explode(" ", $title);
            $title = implode("_", $arr);

              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);

              define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

              require_once(APPPATH.'libraries/PHPExcel.php');
              require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Uang Lembur");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Uang Lembur");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Uang Lembur");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Uang Lembur'); //sheet title
            $objset->setCellValue('A1','Rekap Uang Lembur '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            // range tgl
            $head = array('TANGGAL', 'SHIFT', 'ABSENSI', 'JAM', 'PER JAM', 'TOTAL', 'KETERANGAN');
            $date_range = range_to_date($date_start, $date_end);
            $jumlah_range = count($head);

            // Header Tabel
            $head_val = array('NO', 'NIP', 'NAMA', 'CABANG', 'DIVISI');
            $jumlah_head_val = count($head_val);
            for ($i = 0; $i < $jumlah_head_val; $i++) {
                  $a = $i + 1;
                  $objset->setCellValue(alphabet($a).'3', $head_val[$i]);
            }
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG LEMBUR');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;

            // Header Tanggal
            $range_head = count($head_val);
            for ($i = 0; $i < $jumlah_range; $i++) {
                  $a = $i + 1;
                  $jarak = $range_head + $a;
                  $objset->setCellValue(alphabet($jarak).'4', $head[$i]);
            }

            $baris = 5;
            $data = $this->Rekap_Absensi_Model->kary_lembur($cabang, $date_start, $date_end);
            $no = 1;
            $arr = array();
            foreach ($data as $row) {
                  if (in_array($row->nip, $arr)) {
                        $no--;
                        $v_no = '';
                        $nip = '';
                        $nama = '';
                        $cabang = '';
                        $divisi = '';
                  } else {
                        $v_no = $no;
                        $nip = $row->nip;
                        $nama = $row->nama;
                        $cabang = $row->cabang;
                        $divisi = $row->divisi;
                  }
                  $arr[] = $row->nip;

                  $hari = hari(date('w', strtotime($row->tgl)), 2);
                  $hari = $hari.', '.tanggal($row->tgl);

                  $ovr_nominal = isset($row->ovr_nominal) ? $row->ovr_nominal : 0;
                  $id_overtime = isset($row->id_overtime) ? $row->id_overtime : 0;

                  if ($id_overtime == 0) {
                        $ovr_nominal = 0;
                  }

                  $objset->setCellValue("A".$baris, $v_no);
                  $objset->setCellValue("B".$baris, $nip);
                  $objset->setCellValue("C".$baris, $nama);
                  $objset->setCellValue("D".$baris, $cabang);
                  $objset->setCellValue("E".$baris, $divisi);
                  $objset->setCellValue("F".$baris, $hari);
                  $objset->setCellValue("G".$baris, $row->shift);
                  $objset->setCellValue("H".$baris, $row->absensi);
                  $objset->setCellValue("I".$baris, $row->jam);
                  $objset->setCellValue("J".$baris, $ovr_nominal);
                  $objset->setCellValue("K".$baris, $row->lembur);
                  $objset->setCellValue("L".$baris, $row->keterangan);

                  $baris++;
                  $no++;
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Uang Lembur');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
      }

      function uang_denda()
      {
            $this->Main_Model->all_login();
            $sess_cabang = $this->Main_Model->session_cabang();

            $js = $this->Main_Model->js_datatable()
                  .$this->Main_Model->js_modal()
                  .$this->Main_Model->js_bootbox()
                  .$this->Main_Model->js_select2()
                  .$this->Main_Model->js_timepicker()
                  .$this->Main_Model->js_datepicker()
                  .$this->Main_Model->js_bs_select()
                  .$this->Main_Model->js_button_excel();

            $menu = (empty($sess_cabang)) ? $this->Main_Model->menu_admin('0','0','4') : $this->Main_Model->menu_user('0','0','72');

            $header = array(
                  'menu' => $menu,
                  'style' => $this->Main_Model->style_datatable()
                              .$this->Main_Model->style_modal()
                              .$this->Main_Model->style_select2()
                              .$this->Main_Model->style_timepicker()
                              .$this->Main_Model->style_datepicker()
                              .$this->Main_Model->style_bs_select()
                  );

            $cabang = array();
            if (empty($sess_cabang)) $cabang[] = 'Semua Cabang';
            if(!empty($sess_cabang)) {
                  foreach($sess_cabang as $r) {
                        $d = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $r), 'row');
                        $cabang[$d->id_cab] = $d->cabang;
                  }
            } else {
                  $d = $this->Main_Model->view_by_id('ms_cabang', array('status' => 1), 'result');
                  foreach($d as $row) {
                        $cabang[$row->id_cab] = $row->cabang;
                  }
            }

            $data = array(
                  'footer' => $this->Main_Model->footer($js),
                  'penutup' => $this->Main_Model->close_page(),
                  'cabang' => $cabang
            );

            $this->load->view('template/header', $header);
            $this->load->view('absensi/laporan_uang_denda', $data);
      }

      function view_uang_denda()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $table = $this->Rekap_Absensi_Model->view_uang_denda($cabang, $date_start, $date_end);

            echo $table;
      }

      function rekap_uang_denda()
      {
            $this->Main_Model->all_login();
            $cabang = $this->input->get('cabang');
            $date_start = $this->input->get('date_start');
            $date_end = $this->input->get('date_end');

            $date_start = $this->Main_Model->convert_tgl($date_start);
            $date_end = $this->Main_Model->convert_tgl($date_end);

            $periode = tgl_indo($date_start).' s/d '.tgl_indo($date_end);
            $title = 'Rekap Uang Denda '.$periode;
            $arr = explode(" ", $title);
            $title = implode("_", $arr);

              ini_set('display_errors', TRUE);
              ini_set('display_startup_errors', TRUE);

              define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');

              require_once(APPPATH.'libraries/PHPExcel.php');
              require_once(APPPATH.'libraries/PHPExcel/IOFactory.php');

            $objPHPExcel = new PHPExcel();
            // Set properties
            $objPHPExcel->getProperties()->setCreator("Aplikasi HRD");
            $objPHPExcel->getProperties()->setLastModifiedBy("Aplikasi HRD");
            $objPHPExcel->getProperties()->setTitle("Download Rekap Uang Denda");
            $objPHPExcel->getProperties()->setSubject("Download Rekap Uang Denda");
            $objPHPExcel->getProperties()->setDescription("Download Rekap Uang Denda");
 
            $objset = $objPHPExcel->setActiveSheetIndex(0); //inisiasi set object
            $objget = $objPHPExcel->getActiveSheet();  //inisiasi get object
 
            $objget->setTitle('Rekap Uang Denda'); //sheet title
            $objset->setCellValue('A1','Rekap Uang Denda '.$periode);

            $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                      )
                  )
            );

            // range tgl
            $date_range = range_to_date($date_start, $date_end);
            $jumlah_range = count($date_range);

            // Header Tabel
            $head_val = array('NO', 'NIP', 'NAMA', 'CABANG', 'DIVISI');
            $jumlah_head_val = count($head_val);
            for ($i = 0; $i < $jumlah_head_val; $i++) {
                  $a = $i + 1;
                  $objset->setCellValue(alphabet($a).'3', $head_val[$i]);
            }
            $jumlah_head_val = $jumlah_head_val + 1;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'UANG DENDA');
            $jumlah_head_val = $jumlah_head_val + $jumlah_range;
            $objset->setCellValue(alphabet($jumlah_head_val).'3', 'TOTAL');

            // Header Tanggal
            $range_head = count($head_val);
            for ($i = 0; $i < $jumlah_range; $i++) {
                  $a = $i + 1;
                  $tanggal[$i] = tgl_indo($date_range[$i]);

                  $jarak = $range_head + $a;
                  $objset->setCellValue(alphabet($jarak).'4', $tanggal[$i]);

                  $objPHPExcel->getActiveSheet()->getStyle(alphabet($jarak).'4:'.alphabet($jarak).'4')->getAlignment()->setWrapText(true);
            }

            $baris = 5;
            //divisi 
            $d = $this->Rekap_Absensi_Model->divisi();
            if (!empty($d)) {
                  foreach ($d as $row) {
                        //karyawan
                        $data = $this->Rekap_Absensi_Model->view_karyawan($cabang, $date_start, $date_end, $row->id_divisi);
                        $objset->setCellValue("A".$baris, $row->divisi);
                        $no = 1;
                        $i = 1;
                        $baris = $baris + $i;
                        $total_data = count((array)$data) + 1;
                        $total_uang_denda = 0;

                        foreach ($data as $r) {
                              $objset->setCellValue("A".$baris, $no++);
                              $objset->setCellValue("B".$baris, $r->nip);
                              $objset->setCellValue("C".$baris, $r->nama);
                              $objset->setCellValue("D".$baris, $r->cabang);
                              $objset->setCellValue("E".$baris, $r->divisi);

                              $range_head = count($head_val);
                              $total = 0;
                              for ($i = 0; $i < $jumlah_range; $i++) {
                                    $a = $i + 1;

                                    $um = $this->Rekap_Absensi_Model->uang_denda($date_range[$i], $date_range[$i], $r->nip);
                                    $uang[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                                    $total_uang_denda = $total_uang_denda + $uang[$i];

                                    $total = $total + $uang[$i];

                                    $jarak = $range_head + $a;
                                    $objset->setCellValue(alphabet($jarak).$baris, $uang[$i]);
                              }
                              $objset->setCellValue(alphabet($jarak + 1).$baris, $total);

                              if ($total_data == $no) {
                                    $baris = $baris + 1;
                                    $jarak_total = count($head_val) + $jumlah_range;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, "TOTAL");
                                    $jarak_total = $jarak_total + 1;
                                    $objset->setCellValue(alphabet($jarak_total).$baris, $total_uang_denda);
                              }
                       
                              $baris++;
                        }
                        $i++; 
                  }
            }
            
            $objPHPExcel->getActiveSheet()->setTitle('Rekap Uang Denda');


            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename='.$title.'.xls');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
      }

      function laporan_terlambat()
      {
            $query = $this->Rekap_Absensi_Model->periode_before();
            $date_start = isset($query->tgl_awal) ? $query->tgl_awal : '';
            $date_end = isset($query->tgl_akhir) ? $query->tgl_akhir : '';
            // $date_start = '2018-04-21';
            // $date_end = '2018-05-20';

            $data = $this->Rekap_Absensi_Model->laporan_terlambat($date_start, $date_end);
            $table = '<center><h1 style="color:#265180"> Laporan Absensi </h1></center><br>';
            $table .= '<span style="font-size:14px;">Tanggal <strong>'.hari_tgl($date_start).'</strong> sampai dengan tanggal <strong>'.hari_tgl($date_end).'</strong></span><br><br>';
            $table .= '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
            $table .= '<thead><tr>
                              <th style="text-align: center; vertical-align: middle;">RANK</th>
                              <th style="text-align: center; vertical-align: middle;">NIK</th>
                              <th style="text-align: center; vertical-align: middle;">NAMA</th>
                              <th style="text-align: center; vertical-align: middle;">TERLAMBAT <br> [JUMLAH HARI]</th>
                              <th style="text-align: center; vertical-align: middle;">TERLAMBAT <br> [JUMLAH MENIT]</th>
                              <th style="text-align: center; vertical-align: middle;">PULANG AWAL <br> [JUMLAH HARI]</th>
                              <th style="text-align: center; vertical-align: middle;">PULANG AWAL <br> [JUMLAH MENIT]</th>
                              <th style="text-align: center; vertical-align: middle;">TIDAK ABSEN MASUK</th>
                              <th style="text-align: center; vertical-align: middle;">TIDAK ABSEN PULANG</th>
                         </tr> </thead>';

            if (! empty($data)) {
                  $no = 1;
                  foreach ($data as $row) {
                        $table .= '<tr>';
                        $table .= '<td>'.$no++.'</td>';
                        $table .= '<td>'.$row->nip.'</td>';
                        $table .= '<td>'.$row->nama.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_telat.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_menit_telat.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_plg_awal.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_menit_plg_awal.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_tdk_absen_masuk.'</td>';
                        $table .= '<td style="text-align: center; vertical-align: middle;">'.$row->jumlah_tdk_absen_pulang.'</td>';
                        $table .= '</tr>';
                  }
            }

            $data = array(
                  'html' => $table,
                  'title' => 'Laporan Absensi'
            );

            $this->load->view('file_download/terlambat', $data);
            $filename = 'Laporan Absensi '.hari_tgl($date_start).' sd '.hari_tgl($date_end);
            $html = $this->output->get_output();
            $this->load->library('pdfgenerator');
            $output = $this->pdfgenerator->generate($html, $filename, false, 'legal', 'landscape');

            $this->load->helper('file');
            write_file('assets/uploads/'.$filename.'.pdf', $output);

            $arr = array('adhi@gmedia.co.id', 'edy.kristanto@gmedia.co.id', 'anne.margaretha@gmedia.co.id', 'sianne.ho@gmedia.co.id');

            $file = 'assets/uploads/'.$filename.'.pdf';
            $isi = 'Dear Bpk/Ibu Dept Head,<br><br>
                  Berikut terlampir file laporan absensi tanggal '.hari_tgl($date_start).' sd '.hari_tgl($date_end).'<br><br>
                  Regards,<br>';
            $from = 'hrd.smg@gmedia.co.id';
            $title = 'Rekap Absensi '.hari_tgl($date_start).' sd '.hari_tgl($date_end);
            $subject = 'Rekap Absensi '.hari_tgl($date_start).' sd '.hari_tgl($date_end);

            $to = $arr;
            $this->Main_Model->send_mail($to, $isi, $subject, $from, $file);
      }
}

/* End of file rekap_absensi.php */
/* Location: ./application/controllers/rekap_absensi.php */ ?>