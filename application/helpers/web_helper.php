<?php 
if (! function_exists('convert_bulan'))
{
	function convert_bulan($param = '')
	{
		switch ($param) {
			case '1':
				$bln = 'Januari';
				break;
			case '2':
				$bln = 'Februari';
				break;
			case '3':
				$bln = 'Maret';
				break;
			case '4':
				$bln = 'April';
				break;
			case '5':
				$bln = 'Mei';
				break;
			case '6':
				$bln = 'Juni';
				break;
			case '7':
				$bln = 'Juli';
				break;
			case '8':
				$bln = 'Agustus';
				break;
			case '9':
				$bln = 'September';
				break;
			case '10':
				$bln = 'Oktober';
				break;
			case '11':
				$bln = 'November';
				break;
			case '12':
				$bln = 'Desember';
				break;
			default:
				$bln = '';
				break;
		}

		return $bln;
	}
} 
 
if (! function_exists('list_bulan'))
{
	function list_bulan()
	{
		$bln = array(
			'1' => 'Januari',
			'2' => 'Februari',
			'3' => 'Maret',
			'4' => 'April',
			'5' => 'Mei',
			'6' => 'Juni',
			'7' => 'Juli',
			'8' => 'Agustus',
			'9' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
			);

		return $bln;
	}
}

if (! function_exists('list_tahun'))
{
	function list_tahun()
	{
		$start = '2016';
		$end = '2020';

		$array = array();
		for($start; $start <= $end; $start++) {
			$array[$start] = $start;
		}

		return $array;
	}
}

if (! function_exists('now'))
{
	function now()
	{
		date_default_timezone_set('Asia/Jakarta');
		return date('Y-m-d H:i:s');
	}
}

if (! function_exists('today'))
{
	function today()
	{
		date_default_timezone_set('Asia/Jakarta');
		return date('Y-m-d');
	}
}

 
if (! function_exists('is_value'))
{
	function is_value($param = '')
	{
		$res = isset($param) ? $param : '';
		return $res;
	}
}

if (! function_exists('form_periode'))
{
	function form_periode($param='', $btn='')
	{
		$CI =& get_instance();
		$bulan = $CI->Main_Model->arr_bulan_periode();
		$tahun = $CI->Main_Model->arr_tahun_periode();

		if ($param == '1') {
			$result = '
					<div class="form-group">
						<label>Bulan</label>
							'.form_dropdown('bulan', $bulan, '', 'id="bulan" class="form-control select2" style="width:100%"').'
					</div>
					<div class="form-group">
						<label>Tahun</label>
							'.form_dropdown('tahun', $tahun, '', 'id="tahun" class="form-control select2" style="width:100%"').'
					</div>';
		} elseif ($param == '2') {
			$result = '
					<form class="form-inline" role="form" id="form_periode">
		            <div class="form-group col-md-2" style="padding-left:0px;">
		                '.form_dropdown('bulan', $bulan, '', 'id="bulan" class="form-control select2" style="width:100%"').'
		            </div>
		            <div class="form-group col-md-2" style="padding-left:0px;">
		                '.form_dropdown('tahun', $tahun, '', 'id="tahun" class="form-control select2" style="width:100%"').'
		           	</div>
		            <button type="button" id="tampil" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
		            <button type="button" id="unduh" class="btn btn-default"><i class="fa fa-download"></i> Download</button>
		            <img id="imgload" src="'.base_url('assets/img/loading.gif').'" class="hidden">
		            '.$btn.'
		            </form>
		            ';
		} elseif ($param == '3') {
			$result = '<form class="form-inline" role="form" id="form_periode">
		            <div class="form-group col-md-2" style="padding-left:0px;">
		                '.form_dropdown('bulan', $bulan, '', 'id="bulan" class="form-control select2" style="width:100%"').'
		            </div>
		            <div class="form-group col-md-2" style="padding-left:0px;">
		                '.form_dropdown('tahun', $tahun, '', 'id="tahun" class="form-control select2" style="width:100%"').'
		           	</div>
		            <img id="imgload" src="'.base_url('assets/img/loading.gif').'" class="hidden">
		            '.$btn.'
		            </form>';
		} else {
			$result = '
					<form class="form-inline" role="form" id="form_periode">
		            <div class="form-group col-md-2" style="padding-left:0px;">
		                '.form_dropdown('bulan', $bulan, '', 'id="bulan" class="form-control select2" style="width:100%"').'
		            </div>
		            <div class="form-group col-md-2" style="padding-left:0px;">
		                '.form_dropdown('tahun', $tahun, '', 'id="tahun" class="form-control select2" style="width:100%"').'
		           	</div>
		            <button type="button" id="tampil" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
		            <img id="imgload" src="'.base_url('assets/img/loading.gif').'" class="hidden">
		            '.$btn.'
		            </form>
		            ';
		}

        return $result;
	}	
}

if (! function_exists('periode'))
{
	function periode($thn = '', $bln = '')
	{
		$CI =& get_instance();
		return $CI->Main_Model->periode($bln, $thn);
	}
}

if (! function_exists('company'))
{
	function company()
	{
		$CI =& get_instance();
		return $CI->Main_Model->company();
	}
}

if (! function_exists('list_company'))
{
	function list_company()
	{
		$CI =& get_instance();
		return $CI->Main_Model->view_company();
	}
}

if (! function_exists('username'))
{
	function username()
	{
		$CI =& get_instance();
		$q = $CI->Main_Model->get_username();

		$nama = isset($q->nama) ? $q->nama : '';
		return $nama;
	}
}

if (! function_exists('sisa_cuti'))
{
	function sisa_cuti($nip = '')
	{
		$CI =& get_instance();
		$q = $CI->Cuti_Model->sisa_cuti($nip);

		return $q;
	}
}

if (! function_exists('uang'))
{
	function uang($param = '')
	{
		$CI =& get_instance();
		$q = $CI->Main_Model->uang($param);

		return $q;
	}
}

if (! function_exists('date_range'))
{
	function date_range($date_start='', $date_end='')
	{
		$mulaix = round(strtotime($date_start) / 60 / 60 / 24);
        $sampaix = round(strtotime($date_end) / 60 / 60 / 24);
        $selisih = $sampaix - $mulaix + 1;
       
        $w2 = $w = 0;
        $arr = array();

        for($q = $mulaix; $q <= $sampaix; $q++){
        	$w2++;
            $hr = date('w', ($q * 60 * 60 * 24));
            $pls = $q + (7 - ($hr));
            if($w2 == 1 or $hr == 1) {
				$t = strftime("%Y-%m-%d",($q * 60 * 60 * 24));
				$tt1 = strftime("%Y-%m-%d",($q * 60 * 60 * 24));
				$r = date('w',strtotime($tt1));
				$t3 = strftime("%Y-%m-%d",($pls * 60 * 60 * 24));
				$t4 = round(strtotime($t3) / 60 / 60 / 24);
				
				if($r == 0) {
					$t2 = strftime("%Y-%m-%d",($q * 60 * 60 * 24));
					$tt2 = strftime("%Y-%m-%d",($q * 60 * 60 * 24));
				} elseif($t4 > $sampaix) {
					$t2 = strftime("%Y-%m-%d",($sampaix * 60 * 60 * 24));
					$tt2 = strftime("%Y-%m-%d",($sampaix * 60 * 60 * 24));
				} else {
					$t2 = strftime("%Y-%m-%d",($pls * 60 * 60 * 24));
					$tt2 = strftime("%Y-%m-%d",($pls * 60 * 60 * 24));
				}
				$arr[] = $t.' s/d '.$t2;         	
            } 
            if($hr == 1) {
            	$w++;
            }
        }
        
        $panjang= count($arr);
        if($panjang == 0) {
            $panjang = 1;
        }

        $week 	= array();
        for($a = 0; $a < $panjang; $a++){
            array_push($week, $arr[$a]);
        } 

        return $week;
	}
}


if (! function_exists('range_to_date'))
{
	function range_to_date($date_start= '', $date_end='')
	{
		$tanggal = array();
		while (strtotime($date_start) <= strtotime($date_end)) {
				$tanggal[] = $date_start;
				$date_start = date("Y-m-d", strtotime("+1 day", strtotime($date_start)));
			}
		return $tanggal;
	}
}

if (! function_exists('tgl_indo'))
{
	function tgl_indo($param = '')
	{
		$CI =& get_instance();
		$q = $CI->Main_Model->tgl_indo($param);

		return $q;
	}
}

if (! function_exists('tanggal'))
{
	function tanggal($param = '')
	{
		$CI =& get_instance();
		$q = $CI->Main_Model->tanggal($param);

		return $q;
	}
}

if (! function_exists('bad_request'))
{
	function bad_request()
	{
		return array(
                'response' => '',
                'metadata' => array(
                    'status' => 0,
                    'message' => 'Bad request.'
                ));
	}
}

if (! function_exists('check_auth'))
{
	function check_auth()
	{
		$CI =& get_instance();
		$q = $CI->Rest_Model->check_auth();

		return $q;
	}
}

if (! function_exists('auth_login'))
{
	function auth_login()
	{
		$CI =& get_instance();
		$q = $CI->Rest_Model->auth_login();

		return $q;
	}
}

if (! function_exists('consumer_id'))
{
	function consumer_id()
	{
		$CI =& get_instance();
		$q = $CI->input->get_request_header('Consumer-id', TRUE);

		return $q;
	}
}

if (! function_exists('nip_api'))
{
	function nip_api()
	{
		$CI =& get_instance();
		$q = $CI->input->get_request_header('Nip', TRUE);

		return $q;
	}
}

if (! function_exists('periode_bulan'))
{
	function periode_bulan()
	{
		$CI =& get_instance();
		$q = $CI->Main_Model->period_this_year();
		$bulan = array();
		if (!empty($q)) {
			foreach ($q as $row) {
				$bulan[] = $row->bulan;
			}
		}

		return json_encode($bulan);
	}
}

if (! function_exists('api_tgl'))
{
	function api_tgl($param='')
	{
		$tgl = '';
		if ($param != '') {
			$x = date_create($param);
			$tgl = date_format($x, 'Y-m-d');
		}

		return $tgl;
	}
}

if (! function_exists('tgl_api'))
{
	function tgl_api($param='')
	{
		$tgl = '';
		if ($param != '') {
			$x = date_create($param);
			$tgl = date_format($x, 'd-m-Y');
		}

		return $tgl;
	}
}

if (! function_exists('format_dashboard'))
{
	function format_dashboard($param='')
	{
		$jumlah = strlen($param); 
		if ($jumlah >= 7) {
			$param = round($param / 1000000, 0);
			$param = $param.' Juta';
		} else if($jumlah >= 5 && $jumlah < 7) {
			$param = round($param / 1000, 0);
			$param = $param.' Ribu';
		} else {
			$param = $param;
		}

		return $param;
	}
}

if (! function_exists('periode_sekarang'))
{
	function periode_sekarang()
	{
		$CI =& get_instance();
		$q = $CI->Main_Model->per_skr();

		return array(
			'tgl_awal' => isset($q->tgl_awal) ? $q->tgl_awal : '',
			'tgl_akhir' => isset($q->tgl_akhir) ? $q->tgl_akhir : ''
		);
	}
}

if (! function_exists('hari_tgl')) 
{
	function hari_tgl($tanggal='', $style='1') {
		$hari = hari(date_format(date_create($tanggal), 'w'), 2);
		$tanggal = tgl_indo($tanggal);

		if ($style == 1) {
			return $hari.', '.$tanggal;
		} else {
			return strtoupper($hari).'<br>'.$tanggal;
		}
	}	
}

if (! function_exists('jam'))
{
	function jam($param = '')
	{
		return date('H:i', strtotime($param));
	}
}
 
if (! function_exists('eksekusi_absen'))
{
	function eksekusi_absen($date_start='', $date_end='', $nip='')
	{
		//step1
		$cSession = curl_init(); 
		//step2
		// curl_setopt($cSession,CURLOPT_URL,"http://appsmg.gmedia.net.id:2180/hrd/api/absensiproses/".$date_start."/".$date_end."/".$nip);
		// curl_setopt($cSession,CURLOPT_URL,"http://localhost/gmedia.hr/api/absensiproses/".$date_start."/".$date_end."/".$nip);
		curl_setopt($cSession,CURLOPT_URL,"http://erpsmg.gmedia.id/hrd/api/absensiproses/".$date_start."/".$date_end."/".$nip);
		curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($cSession,CURLOPT_HEADER, false); 
		//step3
		$result = curl_exec($cSession);
		//step4
		curl_close($cSession);
	}
}

if (! function_exists('hitung_jam'))
{
	function hitung_jam($start = '', $end = '')
	{
		$CI =& get_instance();
		$result = 0;
        if ($start != '' && $end != '') {
            $query = $CI->db->query("
                    SELECT 
                    TIMESTAMPDIFF(HOUR, '$start', '$end') AS jumlah")->row();

            $result = isset($query->jumlah) ? $query->jumlah : 0;
        } 

        return $result;
	}
}
 
if (! function_exists('fcm_key'))
{
	function fcm_key()
	{
		$key = 'AAAAWF5so7E:APA91bEMKXCHFgGhQvgcAJjWUPZspSgCBn5YHQQidRJdwqXi2e3KukmH6ecl36Qm_dMG57tEKhQKFiU9y22u6_y_-a66zAIZ6f0wjg4SVHuWGCAs_4hBF1CaN0ulKDjjs-vJc8sZaU0R';

		return $key;
	}
}

if (! function_exists('fcm_url'))
{
	function fcm_url()
	{
		$url = 'https://fcm.googleapis.com/fcm/send';

		return $url;
	}
}

if (! function_exists('default_key'))
{
	function default_key()
	{
		$ci =& get_instance();
	    $secret = 'die';
	    $query = $ci->db->query("
	        SELECT AES_DECRYPT(a.`key`, '$secret') AS `key` 
	        FROM tb_default_key a ")->row();

	    $key = isset($query->key) ? $query->key : '';
	    return $key;
	}
}

if (! function_exists('date_interval'))
{
	function date_interval($date = '', $count = 0, $type = 'MONTH', $flag = '+')
	{
		$ci =& get_instance();
		$date = ($date == '') ? date('Y-m-d') : $ci->Main_Model->convert_tgl($date);

		$query = $ci->db->query("
			SELECT DATE_ADD('$date', INTERVAL $flag $count $type) AS hasil")->row();

		$hasil = isset($query->hasil) ? $query->hasil : '';

		return $hasil;
	}
}

if (! function_exists('hari'))
{
	function hari($param = '', $style='1')
	{
		if ($style == 1) {
			switch ($param) {
				case '0':
					$result = 'Min';
					break;
				case '1':
					$result = 'Sen';
					break;
				case '2':
					$result = 'Sel';
					break;
				case '3':
					$result = 'Rab';
					break;
				case '4':
					$result = 'Kam';
					break;
				case '5':
					$result = 'Jum';
					break;
				case '6':
					$result = 'Sab';
					break;
				default:
					$result = '';
					break;
			}
		} else {
			switch ($param) {
				case '0':
					$result = 'Minggu';
					break;
				case '1':
					$result = 'Senin';
					break;
				case '2':
					$result = 'Selasa';
					break;
				case '3':
					$result = 'Rabu';
					break;
				case '4':
					$result = 'Kamis';
					break;
				case '5':
					$result = 'Jumat';
					break;
				case '6':
					$result = 'Sabtu';
					break;
				default:
					$result = '';
					break;
			}
		}
		return $result;
	}
}

if (! function_exists('alphabet'))
{
	function alphabet($jumlah='')
    {
        switch ($jumlah) {
            case '1':
                $result = 'A';
                break;
            case '2':
                $result = 'B';
                break;
            case '3':
                $result = 'C';
                break;
            case '4':
                $result = 'D';
                break;
            case '5':
                $result = 'E';
                break;
            case '6':
                $result = 'F';
                break;
            case '7':
                $result = 'G';
                break;
            case '8':
                $result = 'H';
                break;
            case '9':
                $result = 'I';
                break;
            case '10':
                $result = 'J';
                break;
            case '11':
                $result = 'K';
                break;
            case '12':
                $result = 'L';
                break;
            case '13':
                $result = 'M';
                break;
            case '14':
                $result = 'N';
                break;
            case '15':
                $result = 'O';
                break;
            case '16':
                $result = 'P';
                break;
            case '17':
                $result = 'Q';
                break;
            case '18':
                $result = 'R';
                break;
            case '19':
                $result = 'S';
                break;
            case '20':
                $result = 'T';
                break;
            case '21':
                $result = 'U';
                break;
            case '22':
                $result = 'V';
                break;
            case '23':
                $result = 'W';
                break;
            case '24':
                $result = 'X';
                break;
            case '25':
                $result = 'Y';
                break;
            case '26':
                $result = 'Z';
                break;
            case '27':
            	$result = 'AA';
            	break;
            case '28':
            	$result = 'AB';
            	break;
            case '29':
            	$result = 'AC';
            	break;
            case '30':
            	$result = 'AD';
            	break;
            case '31':
            	$result = 'AE';
            	break;
            case '32':
            	$result = 'AF';
            	break;
            case '33':
            	$result = 'AG';
            	break;
            case '34':
            	$result = 'AH';
            	break;
            case '35':
            	$result = 'AI';
            	break;
            case '36':
            	$result = 'AJ';
            	break;
            case '37':
            	$result = 'AK';
            	break;
            case '38':
            	$result = 'AL';
            	break;
            case '39':
            	$result = 'AM';
            	break;
            case '40':
            	$result = 'AN';
            	break;
            default:
                $result = '';
                break;
        }
        return $result;
    }
}

function search_datatable($kolom = [], $search = '')
{
    $condition = '';
    if ($search != '') {
        $condition .= ' AND (';
        for ($i = 0; $i < count($kolom); $i++) {
            $condition .= $kolom[$i]." LIKE '%$search%' ";
            if ($kolom[$i] != end($kolom)) {
                $condition .= ' OR ';
            }
        }
        $condition .= ')';
    }

    return $condition;
}

function order_datatable($kolom_order = [], $column = '', $dir = '')
{
    $order = '';
    if ($column != '' && $dir != '') {
        $col = isset($kolom_order[$column]) ? $kolom_order[$column] : '';

        if ($col != '') {
            $order .= " ORDER BY $col $dir ";
        }
    }

    return $order;
}

function response_datatable($draw = '', $total_filtered = 0, $data = [])
{
    $arr = array(
        'draw' => $draw,
        'recordsFiltered' => $total_filtered,
        'recordsTotal' => $total_filtered,
        'data' => $data
    );

    return $arr;
}
?>