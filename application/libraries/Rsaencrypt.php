<?php 
class Rsaencrypt
{	
	function __construct()
	{
		# code...
	}

	function generate()
	{
		//untuk membuat kunci yang lebih panjang coba gmp_random
        //$rand1 = gmp_random(1); // mengeluarkan random number dari 0 sampai 1 x limb
        //$rand2 = gmp_random(1); // mengeluarkan random number dari 0 sampai 1 x limb
        //mencari bilangan random
        $rand1 = rand(1000, 2000);
        $rand2 = rand(1000, 2000);
        // mencari bilangan prima selanjutnya dari $rand1 &rand2
        $p = gmp_nextprime($rand1); 
        $q = gmp_nextprime($rand2);

        //menghitung&menampilkan n=p*q
        $n = gmp_mul($p, $q);

        //menghitung&menampilkan totient/phi=(p-1)(q-1)
        $totient = gmp_mul(gmp_sub($p, 1), gmp_sub($q, 1));
       
        //mencari e, dimana e merupakan coprime dari totient
        //e dikatakan coprime dari totient jika gcd/fpb dari e dan totient/phi = 1
        for($e = 2; $e < 100; $e++){  //mencoba perulangan max 100 kali, 
            $gcd = gmp_gcd($e, $totient);
            if(gmp_strval($gcd) == '1') {
                break;
            }
        }

        //cari d
        // d.e mod totient =1
        // d.e = totient*x + 1
        // d.e = totient*1 + 1
        // d = (totient *1 + 1)/e
        //menghitung&menampilkan d
        $i = 1;
        do {
            $res = gmp_div_qr(gmp_add(gmp_mul($totient,$i), 1), $e);
            $i++;
            if ($i == 10000) { //maksimal percobaan 10000
                break;
            }
        } while(gmp_strval($res[1]) != '0');
        $d = $res[0];

        $n = gmp_strval($n);
        $e = gmp_strval($e);
        $d = gmp_strval($d);

        $result = array(
        	'n' => $n,
        	'e' => $e,
        	'd' => $d
        );

        return $result;
	}

	function encrypt($e = '', $n = '', $string = '')
	{
        $hasil = '';
        for($i = 0; $i < strlen($string); ++$i){
            //rumus enkripsi <enkripsi>=<pesan>^<e>mod<n>
            $hasil .= gmp_strval(gmp_mod(gmp_pow(ord($string[$i]),$e),$n));
            //antar tiap karakter dipisahkan dengan "."
            if($i != strlen($string) - 1){
                $hasil .= ".";
            }
        }

        return $hasil;
	}

	function decrypt($d = '', $n = '', $string = '')
	{
        $hasil = '';
        $string = explode(".", $string);
        foreach($string as $nilai){
            $hasil .= chr(gmp_strval(gmp_mod(gmp_pow($nilai,$d),$n)));
        }

        return $hasil;
	}
} 
?>