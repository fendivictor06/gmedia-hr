<?php

class Absensi_Model extends CI_Model
{
    
    function __construct()
    {
        parent::__construct();
    }

    function view_ijinjam($qd_id = '')
    {
        $idp = $this->session->userdata('idp');
        $date = $this->db->query("SELECT * FROM q_det WHERE qd_id='$qd_id'")->row();
        $tgl_akhir = isset($date->tgl_akhir) ? $date->tgl_akhir : '';
        $tgl_awal = isset($date->tgl_awal) ? $date->tgl_awal : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $is_approval = $this->Main_Model->is_approval();
        $pengajuan = $this->Main_Model->session_pengajuan();
        $kelas_pengajuan = '';
        if ($is_approval == true) {
            if (!empty($pengajuan)) {
                $kelas_pengajuan .= ' AND (';
                for ($i = 0; $i < count($pengajuan); $i++) {
                    $kelas_pengajuan .= " g.`privilege` = '$pengajuan[$i]'";

                    if (end($pengajuan) != $pengajuan[$i]) {
                        $kelas_pengajuan .= ' OR';
                    } else {
                        $kelas_pengajuan .= ')';
                    }
                }
            }
        }

        // if(!empty($id_cabang) || !empty($sal_pos) || !empty($divisi)) {
            return $this->db->query("
                SELECT a.`id`, a.`nip`, b.`nama`, f.`cabang`, 
                    DATE_FORMAT(a.`tgl`,'%d %M %Y') AS tgl, a.`jmljam`, a.`keterangan`, 
                    g.`keterangan` as app_status, g.`kode`, g.`flag`, e.`jab`, f.`cabang`, 
                    b.`kary_stat` , a.dari, a.sampai, 
                    GROUP_CONCAT(DATE_FORMAT(scan.scandate, '%d %M %Y %H:%i%:%s') SEPARATOR '<br>') AS scanlog
                FROM ijinjam a 
                INNER JOIN kary b ON a.`nip` = b.`nip`
                INNER JOIN sk c ON c.`nip` = b.`nip`
                INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
                INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
                INNER JOIN ms_status_approval g ON g.`kode` = a.`status`
                LEFT JOIN (
                    SELECT DISTINCT(scan_date) AS scandate, pin FROM tb_scanlog
                    WHERE DATE(scan_date) BETWEEN '$tgl_awal' AND '$tgl_akhir'
                    
                ) AS scan ON scan.pin = b.`pin` AND DATE(scandate) = a.`tgl`
                WHERE c.`aktif` = '1' AND ISNULL(b.`tgl_resign`)
                AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND b.`idp` = '$idp'
                $cabang $kelas $div 
                GROUP BY a.id
                ORDER BY a.`tgl` DESC")->result();
            // $kelas_pengajuan
        // } else {
        //     return $this->db->query("
        //         SELECT a.`id`, a.`nip`, b.`nama`, f.`cabang`, DATE_FORMAT(a.`tgl`,'%d %M %Y') AS tgl, a.`jmljam`, a.`keterangan`, g.`keterangan` as app_status, g.`kode`, g.`flag`, e.`jab`, f.`cabang`, b.`kary_stat` FROM ijinjam a
        //         INNER JOIN kary b ON a.`nip` = b.`nip`
        //         INNER JOIN sk c ON c.`nip` = b.`nip`
        //         INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
        //         INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
        //         INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
        //         INNER JOIN ms_status_approval g ON g.`kode` = a.`status`
        //         WHERE c.`aktif` = '1' AND ISNULL(b.`tgl_resign`)
        //         AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND b.`idp` = '$idp'
        //         ORDER BY a.`tgl` DESC")->result();
        // }
    }
    function option_nip()
    {
        $opt = $this->db->from('kary')->order_by('nama', 'asc')->get()->result();
        foreach ($opt as $row) {
            $result[$row->nip] = $row->nama;
        }
        return $result;
    }
    function lwok_opt()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->from('lwok')->where('idp', $idp)->order_by('id_lwok', 'asc')->get()->result();
        foreach ($opt as $row) {
            $result[$row->id_lwok] = $row->wok;
        }
        return $result;
    }
    function add_ijinperjam($data)
    {
        $this->db->insert('ijinjam', $data);
    }

    function view_presensi_nip($nip = '', $periode = '')
    {
        if ($periode) {
            $q = $this->Main_Model->id_periode($periode);
            $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
            $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';
            return $this->db->query("
                SELECT a.*,b.`nama`, absensi.shift, absensi.scan_masuk, absensi.scan_pulang, absensi.flag
                FROM presensi a 
                JOIN kary b ON a.`nip`=b.`nip`
                LEFT JOIN (
                    SELECT * 
                    FROM tb_absensi
                    WHERE tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                ) AS absensi ON absensi.nip = a.nip AND absensi.tgl = a.tgl
                WHERE b.`nip`='$nip' 
                AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'")->result();
        } else {
            return $this->db->query("
                SELECT a.*,b.`nama` 
                FROM presensi a 
                JOIN kary b ON a.`nip`=b.`nip` 
                WHERE b.`nip`='$nip'")->result();
        }
    }

    function view_presensi_cab($tgl_awal = '', $tgl_akhir = '')
    {
        // $per = $this->Main_Model->id_periode($periode);
        // $tgl_awal = $per->tgl_awal;
        // $tgl_akhir = $per->tgl_akhir;
        $tgl_awal = $this->Main_Model->convert_tgl($tgl_awal);
        $tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            select e.`nama`,a.*,f.`cabang` 
            from presensi a 
            join sk b on a.`nip`=b.`nip` 
            join pos_sto c on b.`id_pos_sto`=c.`id_sto` 
            join pos d on d.`id_pos`=c.`id_pos` 
            join kary e on b.`nip`=e.`nip` 
            join ms_cabang f on f.`id_cab` = d.`id_cabang`
            where b.`aktif`='1'
            $cabang $kelas $div
            and a.`tgl` between '$tgl_awal' and '$tgl_akhir'")->result();
    }

    // function view_absensi_nip2($nip='')
    // {
    //     // return $this->db->query("SELECT DATE_FORMAT(b.`tgl_akhir`,'%M %Y') AS periode,a.`j_sakit`,a.`j_ijin`,a.`j_mangkir`,a.`j_lembur`,b.`tgl_akhir`,b.`tgl_awal`,c.`tgl_masuk`,c.`tgl_resign`,a.`j_hr_kerja` FROM ab a JOIN q_det b ON a.`id_per`=b.`qd_id` JOIN kary c ON a.`nip`=c.`nip` WHERE a.`nip` = '$nip'")->result();
    //     return $this->db->query("SELECT (SELECT COUNT(DISTINCT(tb_absensi.`tgl`)) FROM tb_absensi WHERE tb_absensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir`) jml_hari, DATE_FORMAT(a.`tgl_akhir`,'%d %M %Y') periode, (SELECT COUNT(cuti_sub_det.`tgl`) FROM cuti_det JOIN cuti_sub_det ON cuti_det.`id_cuti_det`=cuti_sub_det.`id_cuti_det` WHERE cuti_sub_det.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` AND cuti_det.`nip` = '$nip') cuti, (SELECT COUNT(presensi.`sakit`) FROM presensi WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` AND presensi.`nip` = '$nip' AND presensi.`sakit` = '1') sakit, (SELECT COUNT(presensi.`alpha`) FROM presensi WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` AND presensi.`nip` = '$nip' AND presensi.`alpha` = '1') alpha, (SELECT COUNT(presensi.`mangkir`) FROM presensi WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` AND presensi.`nip` = '$nip' AND presensi.`mangkir` = '1') mangkir, (SELECT COUNT(presensi.`ijin`) FROM presensi WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` AND presensi.`nip` = '$nip' AND presensi.`ijin` = '1') ijin, (SELECT COUNT(lembur.`tgl`) FROM lembur WHERE lembur.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` AND lembur.`nip`='$nip') lembur FROM q_det a WHERE YEAR(a.`tgl_akhir`) = YEAR(NOW())")->result();
    // }

    function view_absensi_nip($tgl_awal = '', $tgl_akhir = '')
    {
        $tgl_awal = $this->Main_Model->convert_tgl($tgl_awal);
        $tgl_akhir = $this->Main_Model->convert_tgl($tgl_akhir);

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= ' AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " c.`id_cabang` = '$id_cabang[$i]'";

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " c.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT
              *
            FROM
              (SELECT
                a.nip,
                DATE_FORMAT('$tgl_awal', '%d %M %Y') AS tgl_awal,
                DATE_FORMAT('$tgl_akhir', '%d %M %Y') AS tgl_akhir,
                d.nama,
                e.cabang,
                udf_jml_hr_kerja ('$tgl_awal', '$tgl_akhir') AS jml_kerja,
                DATEDIFF('$tgl_akhir', '$tgl_awal') + 1 AS hari,
                IFNULL(k.jml_masuk, 0) AS jml_masuk,
                IFNULL(ct.jml_cuti, 0) AS jml_cuti,
                IFNULL(pr.sakit, 0) AS jml_sakit,
                IFNULL(pr.ijin, 0) AS jml_ijin,
                IFNULL(pr.mangkir, 0) AS jml_mangkir,
                IFNULL(lembur.jml, 0) AS jml_lembur,
                IFNULL(dayoff.jml, 0) AS jml_dayoff,
                IFNULL(masuk.jml, 0) AS jml_potong,
                IFNULL(resign.jml, 0) AS jml_resign,
                IFNULL(lembur.jmljam, 0) AS jmljam_lembur,
                IFNULL(uang.uang_makan, 0) AS uang_makan,
                IFNULL(uang.uang_denda, 0) AS uang_denda,
                IFNULL(terlambat.jumlah_telat, 0) AS jumlah_telat,
                IFNULL(pulang_awal.jumlah, 0) AS jumlah_pulang,
                IFNULL(ijin.jumlah, 0) AS jumlah_ijin
              FROM
                sk a
                JOIN pos_sto b
                  ON a.id_pos_sto = b.id_sto
                JOIN pos c
                  ON b.id_pos = c.id_pos
                JOIN kary d
                  ON a.nip = d.nip
                JOIN ms_cabang e
                  ON e.id_cab = c.id_cabang
                LEFT JOIN
                  (SELECT
                    COUNT(tb_absensi.tgl) AS jml_masuk,
                    nip
                  FROM
                    tb_absensi
                  WHERE tb_absensi.tgl BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                    AND `status` = ''
                  GROUP BY nip) k
                  ON k.nip = a.nip
                LEFT JOIN
                  (SELECT
                    COUNT(cuti_sub_det.tgl) AS jml_cuti,
                    nip
                  FROM
                    cuti_det
                    JOIN cuti_sub_det
                      ON cuti_det.id_cuti_det = cuti_sub_det.id_cuti_det
                  WHERE cuti_sub_det.tgl BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                  GROUP BY nip) ct
                  ON ct.nip = a.nip
                LEFT JOIN
                  (SELECT
                    SUM(sakit) AS sakit,
                    SUM(ijin) AS ijin,
                    SUM(mangkir) AS mangkir,
                    nip
                  FROM
                    presensi
                  WHERE presensi.tgl BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                  GROUP BY nip) pr
                  ON pr.nip = a.nip
                LEFT JOIN
                  (SELECT
                    COALESCE(SUM(lembur.lembur), 0) AS jml,
                    nip,
                    COALESCE(SUM(lembur.jml_jam), 0) AS jmljam
                  FROM
                    lembur
                  WHERE lembur.tgl BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                    AND STATUS = '1'
                  GROUP BY nip) lembur
                  ON lembur.`nip` = a.nip
                LEFT JOIN
                  (SELECT
                    COUNT(tgl) AS jml,
                    nip
                  FROM
                    tr_shift
                    JOIN ms_shift
                      ON tr_shift.id_shift = ms_shift.id_shift
                  WHERE ms_shift.nama LIKE '%day off%'
                    AND tgl BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                  GROUP BY nip) dayoff
                  ON dayoff.nip = a.nip
                LEFT JOIN
                  (SELECT
                    (SELECT
                      udf_jml_hr_kerja ('$tgl_awal', tgl_masuk)) jml,
                    tgl_masuk,
                    nip
                  FROM
                    kary
                  WHERE tgl_masuk >= '$tgl_awal'
                    AND tgl_masuk <= '$tgl_akhir'
                  GROUP BY nip) masuk
                  ON masuk.nip = a.nip
                LEFT JOIN
                  (SELECT
                    (SELECT
                      udf_jml_hr_kerja (tgl_resign, '$tgl_akhir')) jml,
                    tgl_resign,
                    nip
                  FROM
                    kary
                  WHERE tgl_resign BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                  GROUP BY nip) resign
                  ON resign.nip = a.nip
                LEFT JOIN
                  (SELECT
                    MAX(id_sk),
                    ms_cabang.`cabang`,
                    nip
                  FROM
                    sk
                    JOIN pos_sto
                      ON sk.`id_pos_sto` = pos_sto.`id_sto`
                    JOIN pos
                      ON pos.`id_pos` = pos_sto.`id_pos`
                    JOIN ms_cabang
                      ON ms_cabang.`id_cab` = pos.`id_cabang`
                  GROUP BY sk.`nip`) cabang
                  ON cabang.nip = a.nip
                LEFT JOIN
                  (SELECT
                    SUM(uang_makan) AS uang_makan,
                    nip,
                    SUM(uang_denda) AS uang_denda
                  FROM
                    tb_absensi
                  WHERE tgl BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                  GROUP BY nip) uang
                  ON uang.nip = a.nip
                LEFT JOIN
                  (SELECT
                    COUNT(a.`id`) AS jumlah_telat,
                    a.`nip`
                  FROM
                    terlambat_temp a
                  WHERE a.`tgl` BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                    AND NOT EXISTS
                    (SELECT
                      1
                    FROM
                      tb_klarifikasi_terlambat
                    WHERE tgl = a.`tgl`
                      AND uang_makan = 0)
                  GROUP BY a.`nip`) terlambat
                  ON terlambat.`nip` = a.nip
                LEFT JOIN
                  (SELECT
                    a.`nip`,
                    COUNT(a.`id_pulang_awal`) AS jumlah
                  FROM
                    tb_pulang_awal a
                  WHERE a.`status` = '12'
                    AND a.`tgl` BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                  GROUP BY a.`nip`) pulang_awal
                  ON pulang_awal.nip = a.nip
                LEFT JOIN
                  (SELECT
                    a.`nip`,
                    COUNT(a.`id`) AS jumlah
                  FROM
                    ijinjam a
                  WHERE a.`tgl` BETWEEN '$tgl_awal'
                    AND '$tgl_akhir'
                    AND a.`status` = '12'
                  GROUP BY a.`nip`) ijin
                  ON ijin.nip = a.nip
              WHERE (
                  ISNULL(d.tgl_resign)
                  OR (
                    d.tgl_resign >= '$tgl_awal'
                    AND d.tgl_resign <= '$tgl_akhir'
                  )
                )
                AND a.`aktif` = 1
                $cabang
                $kelas
                $div
            ) AS presensi GROUP BY nip ORDER BY nama")->result();
    }
    
    function add_presensi($data)
    {
        $this->db->insert('presensi', $data);
    }

    function presensi_id($id)
    {
        return $this->db->query("
            SELECT a.*,b.*,c.`nama`,DATE_FORMAT(b.`scan_masuk`,'%d/%m/%Y %H:%i') jam_masuk, 
            DATE_FORMAT(b.`scan_pulang`,'%d/%m/%Y %H:%i') jam_pulang,
            DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tanggal 
            FROM presensi a 
            JOIN tb_absensi b ON a.`nip` = b.`nip` 
            left JOIN kary c ON a.`nip`=c.`nip` 
            WHERE a.`tgl` = b.`tgl` 
            AND a.`id` = '$id'")->row();
    }

    function absensi_id($id = '')
    {
        return $this->db->query("SELECT a.*,b.`nama`,DATE_FORMAT(a.`scan_masuk`,'%H:%i') jam_masuk, 
            DATE_FORMAT(a.`scan_pulang`,'%H:%i') jam_pulang,
            DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tanggal,
            (SELECT presensi.`sakit` FROM presensi WHERE presensi.`tgl` = a.`tgl` AND presensi.`nip` = a.`nip`) sakit,
            (SELECT presensi.`ijin` FROM presensi WHERE presensi.`tgl` = a.`tgl` AND presensi.`nip` = a.`nip`) ijin,
            (SELECT presensi.`mangkir` FROM presensi WHERE presensi.`tgl` = a.`tgl` AND presensi.`nip` = a.`nip`) mangkir
            FROM tb_absensi a 
            JOIN kary b ON a.`nip` = b.`nip` 
            WHERE a.`id_ab` = '$id'")->row();
    }

    function update_presensi($data, $id)
    {
        $this->db->where('id', $id)->update('presensi', $data);
    }
    function delete_presensi($id)
    {
        $this->db->where('id', $id)->delete('presensi');
    }
    function view_absensi($qd_id)
    {
        $idp = $this->session->userdata('idp');
        // return $this->db->query("SELECT DATE_FORMAT(c.`tgl_akhir`,'%M %Y') AS bln_th, a.`nip`,b.`nama`,a.`j_sakit`,a.`j_ijin`,a.`j_mangkir`,a.`j_hr_kerja` FROM ab a LEFT JOIN kary b ON a.`nip` = b.`nip` LEFT JOIN q_det c ON a.`id_per` = c.`qd_id`  WHERE a.`id_per` = '$qd_id' AND b.`idp`='$idp'")->result();
        $periode    = $this->db->where('qd_id', $qd_id)->get('q_det')->row();

        return $this->db->query("select b.*,c.nama,c.nip from tb_absensi b join kary c on b.`pin`=c.`pin` where b.`tgl` between '$periode->tgl_awal' and '$periode->tgl_akhir' group by b.`pin`")->result();
    }
    function cari_lokasi($nip)
    {
        // return $this->db->query("SELECT lku FROM kary,sk,pos_sto,lku WHERE kary.`nip`=sk.`nip` AND sk.`id_pos_sto`=pos_sto.`id_sto` AND pos_sto.`id_lku`=lku.`id_lku` AND kary.`nip`='$nip'")->row();
        return $this->db->query("SELECT d.`cabang` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_cabang d ON c.`id_cabang`=d.`id_cab` WHERE a.`aktif` = '1' AND a.`nip` = '$nip'")->row();
    }
    function view_absensi_sps($qd_id)
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("SELECT a.*,DATE_FORMAT(f.`tgl_akhir`,'%M %Y') AS bln_th,b.`nama`,g.`lku` FROM ab_m a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`nip`=c.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN pos e ON d.`id_pos`=e.`id_pos` JOIN q_det f ON a.`id_per`=f.`qd_id` LEFT JOIN lku g ON d.`id_lku`=g.`id_lku` WHERE a.`id_per` = '$qd_id' AND e.`sal_pos` = 'SPS' AND b.`idp` = '$idp'")->result();
    }
    function view_periode($qd_id)
    {
        return $this->db->query("select *,DATE_FORMAT(tgl_akhir,'%M %Y') per from q_det where qd_id='$qd_id'")->row();
    }
    function view_kary_presensi($tgl_awal = '', $tgl_akhir = '')
    {
        $idp = $this->session->userdata('idp');
        // return $this->db->query("SELECT a.`nip`,a.`nama`,a.`tgl_masuk`,a.`tgl_resign` FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON c.`id_sto`=b.`id_pos_sto` JOIN pos d ON c.`id_pos`=d.`id_pos` WHERE  a.`idp`='$idp' AND (a.`tgl_masuk`<='$tgl_akhir' AND ISNULL(a.`tgl_resign`) OR (a.`tgl_resign` >= '$tgl_awal' AND a.`tgl_resign` <= '$tgl_akhir')) AND d.`sal_pos`!='SPS' AND b.`aktif`='1'")->result();
        // return $this->db->query("SELECT a.`nip`,a.`nama`,a.`tgl_masuk`,a.`tgl_resign` FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON c.`id_sto`=b.`id_pos_sto` JOIN pos d ON c.`id_pos`=d.`id_pos` WHERE  a.`idp`='$idp' AND (a.`tgl_masuk`<='$tgl_akhir' AND ISNULL(a.`tgl_resign`) OR (a.`tgl_resign` >= '$tgl_awal' AND a.`tgl_resign` <= '$tgl_akhir')) AND b.`aktif`='1'")->result();
        // return $this->db->query("
        //     SELECT COALESCE(SUM(a.`mangkir`),0) mangkir, COALESCE(SUM(a.`ijin`),0) ijin, COALESCE(SUM(a.`sakit`),0) sakit, COALESCE(SUM(a.`alpha`),0) alpha, b.`nip`,b.`nama`
        //     FROM presensi a
        //     join kary b on a.`nip` = b.`nip`
        //     where a.`tgl` between '$tgl_awal' and '$tgl_akhir'
        //     group by a.`nip`
        //     ORDER BY b.`nama` ASC")->result();

        return $this->db->query("
            SELECT a.`nip`, a.`pin`, a.`nama`, e.`cabang`, COALESCE(pr.jm_skt, 0) AS sakit, 
            COALESCE(pr.jm_ijin, 0) AS ijin, COALESCE(pr.jm_mangkir, 0) AS mangkir 
            FROM kary a
            INNER JOIN sk b ON a.`nip` = b.`nip`
            INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            INNER JOIN pos d ON d.`id_pos` = c.`id_pos`
            INNER JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            LEFT JOIN (
                SELECT SUM(sakit) AS jm_skt, SUM(ijin) AS jm_ijin, SUM(mangkir) AS jm_mangkir, nip 
                FROM presensi
                WHERE tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                GROUP BY nip
            ) AS pr ON pr.nip = a.`nip`
            WHERE (ISNULL(a.`tgl_resign`) OR a.`tgl_resign` BETWEEN '$tgl_awal' AND '$tgl_akhir') 
            AND a.`tgl_masuk` <= '$tgl_akhir'
            GROUP BY a.`nip`")->result();
    }
    function count_presensi($nip, $tgl_awal, $tgl_akhir)
    {
        return $this->db->query("SELECT COALESCE(SUM(sakit),0) AS sakit, COALESCE(SUM(ijin),0) AS ijin, COALESCE(SUM(mangkir),0) AS mangkir FROM presensi WHERE nip='$nip' AND tgl >= '$tgl_awal' AND tgl <= '$tgl_akhir'")->row();
    }
    function proses_absensi($data)
    {
        $this->db->insert('ab', $data);
    }
    function clear_ab($qd_id)
    {
        $this->db->where('id_per', $qd_id)->delete('ab');
    }
    function update_absensi($data, $condition)
    {
        $this->db->where($condition)->update('ab', $data);
    }
    function jml_hr_kerja($qd_id)
    {
        $date = $this->db->query("SELECT * FROM q_det WHERE qd_id='$qd_id'")->row();
        $tgl1 = $date->tgl_awal;
        $tgl2 = $date->tgl_akhir;
        return $this->db->query("SELECT `udf_jml_hr_kerja`('$tgl1','$tgl2') AS jml")->row();
    }

    function udf_jml_hr_kerja($tgl_awal, $tgl_akhir)
    {
        return $this->db->query("SELECT `udf_jml_hr_kerja`('$tgl_awal','$tgl_akhir') AS jml")->row();
    }

    function view_cuti_nip($nip = '')
    {
        return $this->db->query("
            SELECT a.*,(
                SELECT b.`th` 
                FROM cuti_dep b 
                WHERE b.`id_cuti`=a.`cuti_dep`
                ) th_cuti, b.`tipe`, c.keterangan
            FROM cuti_det a 
            join ms_tipe_cuti b on a.`id_tipe` = b.`id`
            JOIN ms_status_approval c ON c.kode = a.approval
            WHERE a.`nip`='$nip' ORDER BY a.`id_cuti_det` DESC")->result();
    }
    function view_perdin()
    {
        $idp = $this->session->userdata("idp");
        return $this->db->query("SELECT a.`id_perdin`,DATE_FORMAT(a.`tgl_brgkt`,'%d %M %Y') AS tgl_brgkt, DATE_FORMAT(a.`tgl_pulang`,'%d %M %Y') AS tgl_pulang,a.`tujuan`,a.`keperluan` FROM perdin a WHERE a.`idp` = '$idp' ORDER BY a.`id_perdin` DESC")->result();
    }
    function add_perdin($data)
    {
        $this->db->insert('perdin', $data);
    }
    function add_perdin_det($data)
    {
        $this->db->insert('perdin_det', $data);
    }
    function perdin_id($id)
    {
        // return $this->db->query("SELECT a.`id`,a.`nip`,b.`nama`,DATE_FORMAT(a.`tgl_brgkt`,'%d/%m/%Y - %h:%i') AS tgl_brgkt, DATE_FORMAT(a.`tgl_pulang`,'%d/%m/%Y - %h:%i') AS tgl_pulang,a.`tujuan`,a.`keperluan` FROM perdin a LEFT JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id`='$id'")->row();
        return $this->db->query("SELECT *,DATE_FORMAT(a.`tgl_brgkt`,'%d/%m/%Y') tgl_a, DATE_FORMAT(a.`tgl_pulang`,'%d/%m/%Y') tgl_b FROM perdin a JOIN perdin_det b ON a.`id_perdin`=b.`id_perdin` WHERE a.`id_perdin`='$id'")->result();
    }
    function update_perdin($data, $id)
    {
        $this->db->where('id_perdin', $id)->update('perdin', $data);
    }
    function delete_perdin($id)
    {
        $this->db->where('id_perdin', $id)->delete('perdin');
        $this->db->where('id_perdin', $id)->delete('perdin_det');
    }
    function download_sps($id_per, $id_lwok)
    {
        $idp       = $this->session->userdata('idp');
        $q         = $this->db->query("select * from q_det where qd_id = '$id_per'")->row();
        $tgl_awal  = $q->tgl_awal;
        $tgl_akhir = $q->tgl_akhir.' 23:59:00';
        
        return $this->db->query("select * from sk a join pos_sto b on a.`id_pos_sto`=b.`id_sto` join pos c on b.`id_pos`=c.`id_pos` join kary d on a.`nip`=d.`nip` where a.`aktif`='1' and c.`jab` LIKE '%Telkomsel Sales Agent%' and c.`id_cabang`='$id_lwok' and d.`tgl_masuk` <= '$tgl_akhir' and (isnull(d.`tgl_resign`) or d.`tgl_resign` <= '$tgl_akhir')")->result();
    }

    function absensi_sps_id($id)
    {
        return $this->db->query("SELECT * FROM ab_m a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id_ab`='$id'")->row();
    }

    function update_absensi_sps($data, $id_ab)
    {
        $this->db->where('id_ab', $id_ab)->update('ab_m', $data);
    }

    function delete_absensi_sps($id)
    {
        $this->db->where('id_ab', $id)->delete('ab_m');
    }

    function ijinjam_id($id = '')
    {
        return $this->db->query("
                    SELECT a.`id`,a.`nip`,
                    DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tgl,
                    a.`keterangan`,a.`jmljam`, a.penyetuju, a.pola,
                    b.mail, b.nama, b.fcm_id, a.cc, a.dari, a.sampai,
                    LEFT(a.`dari`, 2) AS starttime_hour, SUBSTR(a.`dari`, 4, 2) AS starttime_minute, 
                    LEFT(a.`sampai`, 2) AS endtime_hour, SUBSTR(a.`sampai`, 4, 2) AS endtime_minute
                    FROM ijinjam a 
                    INNER JOIN kary b ON a.nip = b.nip
                    WHERE a.`id` = '$id'")->row();
    }

    function update_ijinjam($id, $data)
    {
        $this->db->where('id', $id)->update('ijinjam', $data);
    }

    function delete_ijinjam($id)
    {
        $this->db->where('id', $id)->delete('ijinjam');
    }

    function view_terlambat($id_per = '', $id_cabang = '')
    {
        $q = $this->Main_Model->id_periode($id_per);
        $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }
        $condition = '';
        if (!empty($id_cabang) || !empty($sal_pos) || !empty($divisi)) {
            $condition = $cabang . $kelas . $div;
        }

        return $this->db->query("
            SELECT b.`pin`, b.`nip`, b.`nama`, f.`cabang`, DATE_FORMAT('$tgl_akhir', '%M %Y') AS periode,
            IFNULL(total_menit.jml,0) AS total_menit,
            IFNULL(total_hari.jml,0) AS total_hari 
            FROM kary b 
            INNER JOIN sk c ON c.`nip` = b.`nip`
            INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
            INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
            LEFT JOIN (SELECT SUM(terlambat_temp.`total_menit`) AS jml, terlambat_temp.`nip` 
                    FROM terlambat_temp 
                    WHERE terlambat_temp.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
                    GROUP BY terlambat_temp.`nip`) 
                    AS total_menit ON total_menit.nip = b.nip
            LEFT JOIN (SELECT COUNT(terlambat_temp.`tgl`) AS jml, terlambat_temp.`nip` 
                    FROM terlambat_temp 
                    WHERE terlambat_temp.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
                    GROUP BY terlambat_temp.`nip`) 
                    AS total_hari ON total_hari.nip = b.nip
            WHERE (ISNULL(b.`tgl_resign`) OR (b.`tgl_resign` >= '$tgl_awal' AND b.`tgl_resign` <= '$tgl_akhir')) 
            AND c.`aktif` = '1'
            AND total_menit.jml > 0 AND total_hari.jml > 0 $condition ")->result();
    }

    function add_terlambat($data)
    {
        $this->db->insert('terlambat', $data);
    }

    function terlambat_id($id)
    {
        return $this->db->query("SELECT * FROM terlambat a WHERE a.`id_telat`='$id'")->row();
    }

    function update_terlambat($data, $id)
    {
        $this->db->where('id_telat', $id)->update('terlambat', $data);
    }

    function delete_terlambat($id)
    {
        $this->db->where('id_telat', $id)->delete('terlambat');
    }

    function delete_det_perdin($id)
    {
        $this->db->where('id_det_perdin', $id)->delete('perdin_det');
    }

    function update_perdin_det($detail, $id_det_perdin)
    {
        $this->db->where('id_det_perdin', $id_det_perdin)->update('perdin_det', $detail);
    }

    function view_sp($idp, $qd_id = '')
    {
        $periode = $this->Main_Model->id_periode($qd_id);
        $date_start = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $date_end = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if ($id_cabang != '') {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $condition = '';
        if (!empty($id_cabang) && !empty($divisi)) {
            $condition = $cabang . $div;
        }
        $left = 'left';

        // return $this->db->query("
        //     select e.`id_sp`,e.`surat_kesanggupan`,a.`nip`,d.`nama`,e.`awal_sp`,e.`akhir_sp`,e.`sp`,e.`alasan_sp`,e.`aktif`, f.cabang
        //     from sk a
        //     $left join pos_sto b on a.`id_pos_sto`=b.`id_sto`
        //     $left join pos c on b.`id_pos`=c.`id_pos`
        //     $left join kary d on a.`nip`=d.`nip`
        //     $left join tb_sp e on a.`nip`=e.`nip`
        //     left join ms_cabang f on f.id_cab = c.id_cabang
        //     where a.`aktif`='1'
        //     and e.`awal_sp` between '$date_start' and '$date_end'
        //     and c.`sal_pos` != 'Manager' and c.`sal_pos` != 'General Manager'
        //     $condition")->result();

        return $this->db->query("
            select e.`id_sp`,e.`surat_kesanggupan`,a.`nip`,d.`nama`,e.`awal_sp`,e.`akhir_sp`,e.`sp`,e.`alasan_sp`,e.`aktif`, f.cabang, e.sp_terbit 
            from sk a 
            $left join pos_sto b on a.`id_pos_sto`=b.`id_sto` 
            $left join pos c on b.`id_pos`=c.`id_pos` 
            $left join kary d on a.`nip`=d.`nip` 
            $left join tb_sp e on a.`nip`=e.`nip` 
            left join ms_cabang f on f.id_cab = c.id_cabang
            where a.`aktif`='1'
            and e.`awal_sp` between '$date_start' and '$date_end' 
            $condition")->result();
    }

    function view_rekomendasi_sp($idp, $qd_id = '')
    {
        $periode = $this->Main_Model->id_periode($qd_id);
        $date_start = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $date_end = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if ($id_cabang != '') {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $condition = '';
        if (!empty($id_cabang) && !empty($divisi)) {
            $condition = $cabang . $div;
        }
        $left = 'LEFT';

        return $this->db->query("
            SELECT e.`id_sp`, e.`surat_kesanggupan`,
            a.`nip`, d.`nama`, e.`awal_sp`,
            e.`akhir_sp`, e.`sp`, e.`alasan_sp`,
            e.`aktif`, f.cabang, e.sp_terbit 
            FROM sk a 
            $left JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` 
            $left JOIN pos c ON b.`id_pos`=c.`id_pos` 
            $left JOIN kary d ON a.`nip`=d.`nip` 
            $left JOIN temp_sp e ON a.`nip`=e.`nip` 
            LEFT JOIN ms_cabang f ON f.id_cab = c.id_cabang
            WHERE a.`aktif`='1'
            AND e.`awal_sp` BETWEEN '$date_start' AND '$date_end' 
            $condition")->result();
    }

    function view_sp_nip($nip = '')
    {
        return $this->db->query("SELECT a.*,b.`nama` FROM tb_sp a JOIN kary b ON a.`nip` = b.`nip` WHERE a.`nip` = '$nip'")->result();
    }

    function sp_process($data, $id)
    {
        if ($id) {
            $this->db->where('id_sp', $id)->update('tb_sp', $data);
        } else {
            $this->db->insert('tb_sp', $data);
        }
    }

    function sp_id($id)
    {
        return $this->db->where('id_sp', $id)->get('tb_sp')->row();
    }

    function delete_sp($id = '')
    {
        $q = $this->db->where('id_sp', $id)->get('tb_sp')->row();
        if (!empty($q)) {
            $nip = isset($q->nip) ? $q->nip : '';
            $aktif = isset($q->aktif) ? $q->aktif : '';

            if ($aktif == 1) {
                $w = $this->db->where('nip', $nip)->where('id_sp != ', $id)->order_by('awal_sp', 'asc')->get('tb_sp')->result_array();

                $count_w = count($w);
                $last_array = $count_w - 1;

                $e = $w[$last_array];
                $id_sp_last = isset($e['id_sp']) ? $e['id_sp'] : '';

                if ($id_sp_last != '') {
                    $data = array('aktif' => 1);
                    $this->db->where('id_sp', $id_sp_last)->update('tb_sp', $data);
                }
            }
        }
        $this->db->where('id_sp', $id)->delete('tb_sp');
    }

    function laporan_pulang_awal($idp, $qd_id = '')
    {
        $per = $this->Main_Model->id_periode($qd_id);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if ($id_cabang != '') {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if ($sal_pos != '') {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " c.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        if (!empty($cabang) && !empty($kelas) && !empty($div)) {
            return $this->db->query("
                SELECT d.*,e.`nama`,e.`kary_stat` FROM sk a 
                JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` 
                JOIN pos c ON b.`id_pos`=c.`id_pos` 
                JOIN tb_pulang_awal d ON d.`nip`=a.`nip` 
                JOIN kary e ON e.`nip`=a.`nip` 
                WHERE a.`aktif`='1' 
                $cabang $kelas $div
                AND d.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
                AND d.`status` = 1")->result();
        } else {
            return $this->db->query("
                SELECT * FROM tb_pulang_awal a 
                JOIN kary b ON a.`nip`=b.`nip` 
                WHERE a.`idp` = '$idp' 
                AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
                AND a.`status` = 1")->result();
        }
    }

    function view_pulang_awal($idp = '', $qd_id = '')
    {
        $per = $this->Main_Model->id_periode($qd_id);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $is_approval = $this->Main_Model->is_approval();
        $pengajuan = $this->Main_Model->session_pengajuan();
        $kelas_pengajuan = '';
        if ($is_approval == true) {
            if (!empty($pengajuan)) {
                $kelas_pengajuan .= ' AND (';
                for ($i = 0; $i < count($pengajuan); $i++) {
                    $kelas_pengajuan .= " g.`privilege` = '$pengajuan[$i]'";

                    if (end($pengajuan) != $pengajuan[$i]) {
                        $kelas_pengajuan .= ' OR';
                    } else {
                        $kelas_pengajuan .= ')';
                    }
                }
            }
        }

        // if(!empty($id_cabang) || !empty($sal_pos) || !empty($divisi)) {

            return $this->db->query("
                SELECT a.`id_pulang_awal`, a.`nip`, b.`nama`, 
                DATE_FORMAT(a.`tgl`, '%d %M %Y') AS tgl, 
                a.`jam`, g.`keterangan`, 
                a.`alasan`, a.`file_bukti`, g.`flag`, a.`status`, f.`cabang`, 
                GROUP_CONCAT(DATE_FORMAT(scan.scandate, '%d %M %Y %H:%i:%s') SEPARATOR '<br>') AS scanlog
                FROM tb_pulang_awal a 
                INNER JOIN kary b ON a.`nip` = b.`nip`
                INNER JOIN sk c ON c.`nip` = b.`nip`
                INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
                INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
                INNER JOIN ms_status_approval g ON g.`kode` = a.`status`
                LEFT JOIN (
                    SELECT DISTINCT(scan_date) AS scandate, pin FROM tb_scanlog
                    WHERE DATE(scan_date) BETWEEN '$tgl_awal' AND '$tgl_akhir'
                ) AS scan ON scan.pin = b.`pin` AND DATE(scandate) = a.`tgl`
                WHERE c.`aktif` = '1'
                AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                AND ISNULL(b.`tgl_resign`)
                $cabang $kelas $div
                GROUP BY a.`id_pulang_awal` ")->result();
                
                // $cabang $kelas $div $kelas_pengajuan")->result();
        // } else {
        //     return $this->db->query("
        //         SELECT a.`id_pulang_awal`, a.`nip`, b.`nama`, DATE_FORMAT(a.`tgl`, '%d %M %Y') AS tgl, a.`jam`, g.`keterangan`,
        //         a.`alasan`, a.`file_bukti`, g.`flag`, a.`status`, f.`cabang`
        //         FROM tb_pulang_awal a
        //         INNER JOIN kary b ON a.`nip` = b.`nip`
        //         INNER JOIN sk c ON c.`nip` = b.`nip`
        //         INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
        //         INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
        //         INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
        //         INNER JOIN ms_status_approval g ON g.`kode` = a.`status`
        //         WHERE c.`aktif` = '1'
        //         AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
        //         AND ISNULL(b.`tgl_resign`)")->result();
        // }
    }

    function pulang_awal_process($data, $id)
    {
        if ($id) {
            $this->db->where('id_pulang_awal', $id)->update('tb_pulang_awal', $data);
        } else {
            $this->db->insert('tb_pulang_awal', $data);
        }
    }

    function id_pulang_awal($id = '')
    {
        return $this->db->query("
            SELECT a.*, DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tanggal, 
            DATE_FORMAT(a.`jam`,'%H:%i') jam_pulang, b.nama, b.mail, b.fcm_id 
            FROM tb_pulang_awal a 
            INNER JOIN kary b ON a.nip = b.nip
            WHERE a.`id_pulang_awal` = '$id'")->row();
    }

    function delete_pulang_awal($id)
    {
        $this->db->where('id_pulang_awal', $id)->delete('tb_pulang_awal');
    }

    function summary_absensi($qd_id = '')
    {
        $periode = $this->Main_Model->id_periode($qd_id);
        $date_start = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $date_end = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        return $this->db->query("select a.`cabang`, 
            (select count(b.`id_sk`) from sk b 
                join pos_sto c on b.`id_pos_sto`=c.`id_sto` 
                join pos d on c.`id_pos`=d.`id_pos` 
                join kary x on b.`nip`=x.`nip` 
                where d.`id_cabang`=a.`id_cab` AND b.`aktif`='1' AND x.`tgl_masuk` <= '$date_start' AND (isnull(x.`tgl_resign`) OR (x.`tgl_resign` <= '$date_end' AND x.`tgl_resign` >= '$date_start'))) jml_percabang, 
            (select count(distinct(e.`pin`)) from terlambat_temp e 
                join kary x on e.`pin`=x.`pin`
                join sk f on x.`nip`=f.`nip` 
                join pos_sto g on f.`id_pos_sto`=g.`id_sto` 
                join pos h on g.`id_pos`=h.`id_pos` 
                where f.`aktif`='1' and h.`id_cabang`=a.`id_cab` and e.`tgl` between '$date_start' and '$date_end' AND x.`tgl_masuk` <= '$date_start' AND (isnull(x.`tgl_resign`) OR (x.`tgl_resign` <= '$date_end' AND x.`tgl_resign` >= '$date_start'))) jml_telat, 
            (select count(distinct(i.`nip`)) from tb_sp i 
                join kary x on i.`nip`=x.`nip`
                join sk j on x.`nip`=j.`nip` 
                join pos_sto k on j.`id_pos_sto`=k.`id_sto` 
                join pos l on k.`id_pos`=l.`id_pos` 
                where l.`id_cabang`=a.`id_cab` and j.`aktif`='1' and i.`awal_sp` BETWEEN '$date_start' AND '$date_end' AND x.`tgl_masuk` <= '$date_start' AND (isnull(x.`tgl_resign`) OR (x.`tgl_resign` <= '$date_end' AND x.`tgl_resign` >= '$date_start'))) jml_sp, 
            (select count(distinct(m.`pin`)) from presensi m 
                join kary x on m.`pin`=x.`pin`
                join sk n on x.`nip`=n.`nip` 
                join pos_sto k ON n.`id_pos_sto`=k.`id_sto` 
                JOIN pos l ON k.`id_pos`=l.`id_pos` 
                where l.`id_cabang`=a.`id_cab` AND n.`aktif`='1' AND m.`tgl` BETWEEN '$date_start' AND '$date_end' and m.`sakit`='1' AND x.`tgl_masuk` <= '$date_start' AND (isnull(x.`tgl_resign`) OR (x.`tgl_resign` <= '$date_end' AND x.`tgl_resign` >= '$date_start'))) jml_sakit, 
            (SELECT count(distinct(o.`nip`)) FROM ijinjam o 
                join kary x on o.`nip`=x.`nip`
                join sk n on x.`nip`=n.`nip` 
                join pos_sto k ON n.`id_pos_sto`=k.`id_sto` 
                JOIN pos l ON k.`id_pos`=l.`id_pos` 
                where l.`id_cabang`=a.`id_cab` AND n.`aktif`='1' AND o.`tgl` BETWEEN '$date_start' AND '$date_end' AND x.`tgl_masuk` <= '$date_start' AND (isnull(x.`tgl_resign`) OR (x.`tgl_resign` <= '$date_end' AND x.`tgl_resign` >= '$date_start'))) jml_ijinkeluar, 
            (select count(distinct(p.`nip`)) from kary p 
                JOIN sk n ON p.`nip`=n.`nip` 
                JOIN pos_sto k ON n.`id_pos_sto`=k.`id_sto` 
                JOIN pos l ON k.`id_pos`=l.`id_pos` 
                WHERE l.`id_cabang`=a.`id_cab` AND n.`aktif`='1' AND p.`tgl_resign` BETWEEN '$date_start' AND '$date_end' and p.tipe_resign='PHK') jml_phk, 
            (SELECT COUNT(DISTINCT(q.`nip`)) FROM lembur q 
                JOIN sk n ON q.`nip`=n.`nip` 
                JOIN pos_sto k ON n.`id_pos_sto`=k.`id_sto` 
                JOIN pos l ON k.`id_pos`=l.`id_pos` 
                join kary x on n.`nip`=x.`nip` 
                WHERE l.`id_cabang`=a.`id_cab` AND n.`aktif`='1' AND q.`tgl` BETWEEN '$date_start' AND '$date_end' AND x.`tgl_masuk` <= '$date_start' AND (isnull(x.`tgl_resign`) OR (x.`tgl_resign` <= '$date_end' AND x.`tgl_resign` >= '$date_start'))) jml_lembur 
            from ms_cabang a where a.`status`='1'")->result();
    }

    function presensi_by_id($id = '')
    {
        return $this->db->query("SELECT a.*,b.`nama`,DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tanggal FROM presensi a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id` = '$id'")->row();
    }

    // function view_cuti_cab($qd_id='')
    // {
    //     $periode = $this->Main_Model->id_periode($qd_id);
    //     $date_start = $periode->tgl_awal;
    //     $date_end = $periode->tgl_akhir;

    //     $id_cabang = $this->Main_Model->session_cabang();
    //     $cabang = '';
    //     if($id_cabang != '') {
    //         $cabang .= 'AND (';
    //         for($i = 0; $i < count($id_cabang); $i++) {
    //             $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

    //             if(end($id_cabang) != $id_cabang[$i]) {
    //                 $cabang .= ' OR';
    //             } else {
    //                 $cabang .= ')';
    //             }
    //         }
    //     }

    //     $sal_pos = $this->Main_Model->class_approval();
    //     $kelas = '';
    //     if($sal_pos != '') {
    //         $kelas .= ' AND (';
    //         for($i = 0; $i < count($sal_pos); $i++) {
    //             $kelas .= " c.`sal_pos` = '$sal_pos[$i]'";

    //             if(end($sal_pos) != $sal_pos[$i]) {
    //                 $kelas .= ' OR';
    //             } else {
    //                 $kelas .= ')';
    //             }
    //         }
    //     }

    //     $divisi = $this->Main_Model->session_divisi();
    //     $div = '';
    //     if(!empty($divisi)) {
    //         $div .= ' AND (';
    //         for($i = 0; $i < count($divisi); $i++) {
    //             $div .= " c.`id_divisi` = '$divisi[$i]'";

    //             if(end($divisi) != $divisi[$i]) {
    //                 $div .= ' OR';
    //             } else {
    //                 $div .= ')';
    //             }
    //         }
    //     }

    //     return $this->db->query("
    //         select e.`approval`,d.`nip`,d.`nama`,e.`alasan_cuti`,g.`th`,f.`id_cuti_det`,h.`tipe`,i.`cabang` from sk a
    //         join pos_sto b on a.`id_pos_sto`=b.`id_sto`
    //         join pos c on b.`id_pos`=c.`id_pos`
    //         join kary d on a.`nip`=d.`nip`
    //         join cuti_det e on a.`nip`=e.`nip`
    //         join cuti_sub_det f on e.`id_cuti_det`=f.`id_cuti_det`
    //         join cuti_dep g on e.`cuti_dep`=g.`id_cuti`
    //         join ms_tipe_cuti h on h.`id` = e.`id_tipe`
    //         join ms_cabang i on c.`id_cabang` = i.`id_cab`
    //         where a.`aktif` = '1' $cabang and f.`tgl` between '$date_start' and '$date_end'
    //         $kelas $div
    //         group by e.`id_cuti_det`")->result();
    // }

    function view_cuti_khusus_cab()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " c.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }
        return $this->db->query("
            SELECT e.*, f.`tipe`, d.`nama`, g.cabang, h.flag, h.keterangan 
            FROM sk a 
            JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` 
            JOIN pos c ON c.`id_pos`=b.`id_pos` 
            JOIN kary d ON a.`nip`=d.`nip` 
            JOIN cuti_khusus e ON d.`nip`=e.`nip` 
            JOIN `tipe_cuti_khusus` f ON e.`id_tipe_cuti`=f.`id_tipe` 
            JOIN ms_cabang g ON g.id_cab = c.id_cabang
            JOIN ms_status_approval h ON h.kode = e.approval
            WHERE a.`aktif` = '1' 
            $cabang $kelas $div 
            ORDER BY e.`tgl_awal` DESC")->result();
    }

    function detail_terlambat($nip = '', $periode = '')
    {
        $q = $this->Main_Model->id_periode($periode);
        $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';
        $per = isset($q->periode) ? $q->periode : '';

        return $this->db->query("
            SELECT a.*,b.`nama`, '$per' periode, a.`scan_masuk`, c.klarifikasi,
            DATE_FORMAT(d.scan_masuk, '%H:%i') AS scan_absen 
            FROM terlambat_temp a 
            JOIN kary b ON a.`nip`=b.`nip` 
            LEFT JOIN tb_klarifikasi_terlambat c ON c.id_terlambat = a.id
            LEFT JOIN tb_absensi d ON d.nip = a.nip AND d.tgl = a.tgl
            WHERE a.`nip`='$nip' 
            AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            ORDER BY a.`tgl` ASC")->result();
    }

    function view_ms_shift($id = '')
    {
        if ($id) {
            $query = $this->db->query("
                    SELECT *,DATE_FORMAT(a.`jam_masuk`,'%H:%i') masuk, 
                    DATE_FORMAT(a.`jam_pulang`,'%H:%i') pulang 
                    FROM ms_shift a 
                    WHERE a.`id_shift` = '$id'")->row();
        } else {
            $query = $this->db->query("
                    SELECT *,DATE_FORMAT(a.`jam_masuk`,'%H:%i') masuk, 
                    DATE_FORMAT(a.`jam_pulang`,'%H:%i') pulang 
                    FROM ms_shift a 
                    WHERE a.`status` = 1")->result();
        }
        return $query;
    }

    function set_default_shift($id = '')
    {
        $this->db->query("UPDATE ms_shift SET default_shift = 0 ");

        $this->db->query("UPDATE ms_shift SET default_shift = 1 WHERE id_shift = '$id'");

        return true;
    }

    function view_penjadwalan($date = '', $id = '')
    {
        // return $this->db->query("SELECT a.*,b.`nama` shift,c.`nama` FROM tr_shift a JOIN ms_shift b ON a.`id_shift`=b.`id_shift` JOIN kary c ON a.`nip`=c.`nip` WHERE b.`default_shift` != 1 AND b.`status` = 1 AND a.`tgl` BETWEEN '$date_start' AND '$date_end' ORDER BY c.`nama` ASC")->result();
        if ($id) {
            return $this->db->query("
                SELECT a.*, c.`nama`, b.`nama` AS nama_shift, DATE_FORMAT(a.`tgl`, '%d/%m/%Y') AS tanggal FROM tr_shift a 
                JOIN ms_shift b ON a.`id_shift` = b.`id_shift`
                JOIN kary c ON c.`nip` = a.`nip`
                WHERE a.`id` = '$id'")->row();
        } else {
            $id_cabang = $this->Main_Model->session_cabang();
            $cabang = '';
            if (!empty($id_cabang)) {
                $cabang .= 'AND (';
                for ($i = 0; $i < count($id_cabang); $i++) {
                    $cabang .= ' f.`id_cabang` = '.$id_cabang[$i];

                    if (end($id_cabang) != $id_cabang[$i]) {
                        $cabang .= ' OR';
                    } else {
                        $cabang .= ')';
                    }
                }
            }

            $sal_pos = $this->Main_Model->session_pengajuan();
            $kelas = '';
            if (!empty($sal_pos)) {
                $kelas .= ' AND (';
                for ($i = 0; $i < count($sal_pos); $i++) {
                    $kelas .= " f.`sal_pos` = '$sal_pos[$i]'";

                    if (end($sal_pos) != $sal_pos[$i]) {
                        $kelas .= ' OR';
                    } else {
                        $kelas .= ')';
                    }
                }
            }

            $divisi = $this->Main_Model->session_divisi();
            $div = '';
            if (!empty($divisi)) {
                $div .= ' AND (';
                for ($i = 0; $i < count($divisi); $i++) {
                    $div .= " f.`id_divisi` = '$divisi[$i]'";

                    if (end($divisi) != $divisi[$i]) {
                        $div .= ' OR';
                    } else {
                        $div .= ')';
                    }
                }
            }
            return $this->db->query("
                SELECT a.*, c.`nama`, b.`nama` AS nama_shift FROM tr_shift a 
                JOIN ms_shift b ON a.`id_shift` = b.`id_shift`
                JOIN kary c ON c.`nip` = a.`nip`
                JOIN sk d ON d.`nip` = c.`nip`
                JOIN pos_sto e ON e.`id_sto` = d.`id_pos_sto`
                JOIN pos f ON f.`id_pos` = e.`id_pos`
                WHERE a.`tgl` = '$date'
                $cabang $kelas $div")->result();
        }
    }

    function view_klarifikasi_absensi($id_periode = '')
    {
        $periode = $this->Main_Model->id_periode($id_periode);
        $date_start = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $date_end = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }
        $condition = '';
        if (!empty($id_cabang)) {
            $condition = $cabang . $kelas . $div;
        }
        return $this->db->query("
            SELECT a.*,b.`nama`,f.`cabang`, g.keterangan, h.status
            FROM tb_klarifikasi_absensi a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN sk c ON b.`nip`=c.`nip`
            JOIN pos_sto d ON d.`id_sto`=c.`id_pos_sto`
            JOIN pos e ON d.`id_pos`=e.`id_pos`
            JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab`
            LEFT JOIN ms_status_approval g ON g.kode = a.approval
            LEFT JOIN tb_absensi h ON h.id_ab = a.id_ab
            WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
            $condition AND c.`aktif` = '1'")->result();
    }

    function view_klarifikasi_terlambat($id_periode = '')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $periode = $this->Main_Model->id_periode($id_periode);
        $date_start = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $date_end = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';

        $condition = '';
        if (!empty($id_cabang)) {
            $condition = $cabang . $kelas . $div;
        }

        return $this->db->query("
            SELECT a.*,b.`nama`,f.`cabang`,g.`jam_masuk`, h.keterangan, h.flag 
            FROM tb_klarifikasi_terlambat a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN sk c ON b.`nip`=c.`nip`
            JOIN pos_sto d ON d.`id_sto`=c.`id_pos_sto`
            JOIN pos e ON d.`id_pos`=e.`id_pos`
            JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab`
            LEFT JOIN terlambat_temp g ON g.`id` = a.`id_terlambat`
            LEFT JOIN ms_status_approval h ON h.kode = a.approval
            WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
            $condition AND c.`aktif` = '1' AND ISNULL(b.tgl_resign)")->result();
    }

    function reminder_approval_cuti($id_cabang = '', $sal_pos = '', $divisi = '')
    {
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT a.*,b.`nama`,e.`jab`,f.`cabang`,f.`id_cab` 
            FROM cuti_det a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON c.`nip` = b.`nip`
            JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto`
            JOIN pos e ON d.`id_pos` = e.`id_pos`
            JOIN ms_cabang f on f.`id_cab` = e.`id_cabang`
            JOIN ms_status_approval g ON g.kode = a.approval
            WHERE g.`flag` = 1  AND ISNULL(b.tgl_resign)
            AND c.aktif = 1
            $cabang $kelas $div")->result();
    }

    function reminder_cuti_khusus($id_cabang = '', $sal_pos = '', $divisi = '')
    {
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT a.*,b.`nama`,e.`jab`,f.`cabang`,f.`id_cab`, h.tipe
            FROM cuti_khusus a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON c.`nip` = b.`nip`
            JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto`
            JOIN pos e ON d.`id_pos` = e.`id_pos`
            JOIN ms_cabang f on f.`id_cab` = e.`id_cabang`
            JOIN ms_status_approval g ON g.kode = a.approval
            JOIN tipe_cuti_khusus h ON h.id_tipe = a.id_tipe_cuti
            WHERE g.`flag` = 1  AND ISNULL(b.tgl_resign)
            AND c.aktif = 1
            $cabang $kelas $div")->result();
    }

    function reminder_klarifikasi_terlambat($nip = '')
    {
        return $this->db->query("
            SELECT a.*, b.`nama`, d.`penyetuju`
            FROM terlambat_temp a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN ms_rule_detail c ON c.`nip` = b.`nip`
            INNER JOIN ms_rule_approval d ON c.`pola` = d.`pola`
            WHERE a.`tgl` = DATE_FORMAT(NOW() - INTERVAL 1 DAY,'%Y-%m-%d')
            AND d.penyetuju = '$nip'
            GROUP BY a.`nip`, a.`tgl` ")->result();
    }

    function reminder_klarifikasi_absensi($nip = '')
    {
        // $cabang = '';
        // if($id_cabang != '') {
        //     $cabang .= 'AND (';
        //     for($i = 0; $i < count($id_cabang); $i++) {
        //         $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

        //         if(end($id_cabang) != $id_cabang[$i]) {
        //             $cabang .= ' OR';
        //         } else {
        //             $cabang .= ')';
        //         }
        //     }
        // }

        // $kelas = '';
        // if($sal_pos != '') {
        //     $kelas .= ' AND (';
        //     for($i = 0; $i < count($sal_pos); $i++) {
        //         $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

        //         if(end($sal_pos) != $sal_pos[$i]) {
        //             $kelas .= ' OR';
        //         } else {
        //             $kelas .= ')';
        //         }
        //     }
        // }

        // $div = '';
        // if(!empty($divisi)) {
        //     $div .= ' AND (';
        //     for($i = 0; $i < count($divisi); $i++) {
        //         $div .= " e.`id_divisi` = '$divisi[$i]'";

        //         if(end($divisi) != $divisi[$i]) {
        //             $div .= ' OR';
        //         } else {
        //             $div .= ')';
        //         }
        //     }
        // }

        return $this->db->query("
            SELECT a.*,b.`nama`,e.`jab`,f.`cabang`,f.`id_cab`, h.penyetuju 
            FROM presensi a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON c.`nip` = b.`nip`
            JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto`
            JOIN pos e ON d.`id_pos` = e.`id_pos`
            JOIN ms_cabang f on f.`id_cab` = e.`id_cabang`
            JOIN ms_rule_detail g ON g.`nip` = a.`nip`
            JOIN ms_rule_approval h ON h.`pola` = g.`pola`
            WHERE a.`tgl` = DATE_FORMAT(NOW() - INTERVAL 1 DAY,'%Y-%m-%d') 
            AND a.`mangkir` = '1' 
            AND ISNULL(b.tgl_resign) 
            AND c.aktif = 1
            AND h.`penyetuju` = '$nip'
            GROUP BY a.nip, a.tgl")->result();
    }

    function view_setting_reminder($id = '')
    {
        ($id) ? $condition = "where a.`id` = '$id'" : $condition = "";
        return $this->db->query("
            SELECT a.`id`,a.`nip`,a.`user`, b.`nama`, b.`mail`, b.`telp`,
            GROUP_CONCAT(DISTINCT(f.`cabang`) SEPARATOR ', <br>') AS cabang,
            GROUP_CONCAT(DISTINCT(e.`sal_pos`) SEPARATOR ', <br>') AS approval,
            GROUP_CONCAT(DISTINCT(g.`divisi`) SEPARATOR ', <br>') AS divisi 
            FROM ms_reminder a
            JOIN kary b ON a.`nip` = b.`nip`
            LEFT JOIN tb_user_cabang c ON a.`user` = c.`usr_name`
            LEFT JOIN ms_cabang f ON f.`id_cab` = c.`id_cabang`
            LEFT JOIN tb_user_divisi d ON a.`user` = d.`usr_name`
            LEFT JOIN ms_divisi g ON g.`id_divisi` = d.`id_divisi`
            LEFT JOIN tb_level_approval e ON a.`user` = e.`usr_name`
            $condition
            GROUP BY a.`id`");
    }

    function view_perjalanan_dinas($id_cabang = '', $id_periode = '')
    {
        $idp = $this->session->userdata('idp');
        $per = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= ' WHERE (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' a.`cabang_create` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }
        if (empty($id_cabang)) {
            return $this->db->query("
                SELECT a.*, DATE_FORMAT(a.`tgl_brgkt`, '%d %M %Y') berangkat, 
                DATE_FORMAT(a.`tgl_pulang`, '%d %M %Y') pulang, GROUP_CONCAT(c.nama) AS nama 
                FROM perdin a 
                INNER JOIN perdin_det b ON a.nobukti = b.nobukti
                INNER JOIN kary c ON c.nip = b.nip
                WHERE a.`idp` = '$idp' 
                AND CAST(a.`tgl_brgkt` AS DATE) BETWEEN '$tgl_awal' AND '$tgl_akhir'
                GROUP BY a.nobukti
                ORDER BY a.`tgl_brgkt` DESC")->result();
        } else {
            return $this->db->query("
                SELECT a.*, DATE_FORMAT(a.`tgl_brgkt`, '%d %M %Y') berangkat, DATE_FORMAT(a.`tgl_pulang`, '%d %M %Y') pulang 
                FROM perdin a $cabang 
                AND CAST(a.`tgl_brgkt` AS DATE) BETWEEN '$tgl_awal' AND '$tgl_akhir'
                ORDER BY a.`tgl_brgkt` DESC")->result();
        }
    }

    function perdin_detail($nobukti = '')
    {
        return $this->db->query("SELECT a.*, 
            DATE_FORMAT(a.`tgl_brgkt`, '%d/%m/%Y') berangkat, 
            DATE_FORMAT(a.`tgl_pulang`, '%d/%m/%Y') pulang, b.`nip`, c.`nama` 
            FROM perdin a 
            JOIN perdin_det b ON a.`nobukti` = b.`nobukti`
            LEFT JOIN kary c ON b.`nip` = c.`nip`
            WHERE a.`nobukti` = '$nobukti'")->result();
    }

    function minus_month($tgl = '')
    {
        $query = $this->db->query("SELECT '$tgl' - INTERVAL 1 MONTH AS tgl")->row();

        return $query->tgl;
    }

    function hitung_telat_mangkir($qd_id = '')
    {
        $q = $this->Main_Model->id_periode($qd_id);
        $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';

        $tgl_awal = $this->minus_month($tgl_awal);
        $tgl_akhir = $this->minus_month($tgl_akhir);

        return $this->db->query("
            SELECT a.`nip`, DATE_FORMAT('$tgl_akhir', '%M %Y') AS bulan,
            (SELECT COUNT(terlambat_temp.`tgl`) FROM terlambat_temp 
                WHERE terlambat_temp.`nip` = a.`nip` 
                AND terlambat_temp.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir') jumlah_terlambat,
            (SELECT COUNT(presensi.`tgl`) FROM presensi 
                WHERE presensi.`nip` = a.`nip` 
                AND presensi.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND presensi.`mangkir` = '1') jumlah_mangkir
            FROM kary a ")->result();
    }

    function get_id_terlambat($id = '')
    {
        return $this->db->query("
            SELECT a.*, b.`nama`,date_format(a.`tgl`,'%d/%m/%Y') tanggal,
            (
                SELECT DATE_FORMAT(tb_absensi.`scan_masuk`,'%H:%i') 
                FROM tb_absensi 
                WHERE tb_absensi.`tgl` = a.`tgl` 
                AND tb_absensi.`nip` = a.`nip`
            ) masuk,
            (
                SELECT tb_absensi.`id_ab` 
                FROM tb_absensi 
                WHERE tb_absensi.`tgl` = a.`tgl` 
                AND tb_absensi.`nip` = a.`nip`
            ) id_ab
             FROM terlambat_temp a 
             JOIN kary b ON a.`nip` = b.`nip`
             WHERE a.`id` = '$id'")->row();
    }

    function view_detail_pulang_awal($nip = '', $bulan = '', $th = '')
    {
        return $this->db->query("
            SELECT a.*, b.`nama`, c.keterangan 
            FROM tb_pulang_awal a 
            JOIN kary b ON a.`nip` = b.`nip` 
            JOIN ms_status_approval c ON a.status = c.kode
            WHERE a.`nip` = '$nip' 
            AND DATE_FORMAT(a.`tgl`,'%m') = '$bulan'  
            AND DATE_FORMAT(a.`tgl`,'%Y') = '$th'")->result();
    }

    function view_detail_ijinjam($nip = '', $bulan = '', $th = '')
    {
        return $this->db->query("SELECT a.*, b.`nama`, c.keterangan AS ket 
            FROM ijinjam a 
            JOIN kary b ON a.`nip` = b.`nip` 
            JOIN ms_status_approval c ON a.status = c.kode
            WHERE a.`nip` = '$nip' 
            AND DATE_FORMAT(a.`tgl`,'%m') = '$bulan'  
            AND DATE_FORMAT(a.`tgl`,'%Y') = '$th'")->result();
    }

    function hitung_teguran($condition = '', $nip = '')
    {
        return $this->db->query("
            SELECT COUNT(tb_teguran.`nip`) jumlah_teguran 
            FROM tb_teguran 
            WHERE $condition
            AND tb_teguran.`nip` = '$nip' 
            AND tb_teguran.`teguran` = 1")->row();
    }

    function view_all_mangkir_klarifikasi($id_periode = '')
    {
        $q = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';

        return $this->db->query("
            SELECT a.*, b.`nama`,f.`cabang` 
            FROM presensi a 
            JOIN kary b ON a.`nip` = b.`nip` 
            JOIN sk c ON b.`nip` = c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto` 
            JOIN pos e ON d.`id_pos` = e.`id_pos` 
            JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab` 
            WHERE c.`aktif` = '1' 
            AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND a.`mangkir` = '1'
            AND a.nip <> '03.001.2009'")->result();
    }

    function view_all_klarifikasi_terlambat($id_periode = '')
    {
        $q = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = $q->tgl_awal;
        $tgl_akhir = $q->tgl_akhir;

        return $this->db->query("
            SELECT a.*,b.`nama`,f.`cabang` FROM terlambat_temp a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON b.`nip` = c.`nip`
            JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            JOIN pos e ON e.`id_pos` = d.`id_pos`
            JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
            WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND c.`aktif` = '1'
            ")->result();
    }

    function view_scanlog($date = '')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }
    
        return $this->db->query("
            SELECT a.*, b.`nama`, f.`cabang`, DATE_FORMAT(a.`scan_date`, '%d %M %Y %H:%i:%s') tanggal, b.`nip`,
            g.description AS mesin 
            FROM tb_scanlog a 
            LEFT JOIN kary b ON a.`nip` = b.`nip`
            LEFT JOIN sk c ON c.`nip` = b.`nip`
            LEFT JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            LEFT JOIN pos e ON e.`id_pos` = d.`id_pos`
            LEFT JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
            LEFT JOIN ms_mesin g ON g.sn = a.sn
            WHERE CAST(a.`scan_date` AS DATE) = '$date' 
            $cabang 
            GROUP BY a.scan_date, a.nip
            ORDER BY a.`scan_date` ASC")->result();
    }

    function view_scandate($date = '')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cab` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }
        
        return $this->db->query("
            SELECT a.pin, a.nama, e.`cabang`, a.nip,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(scan.scan_date, '%d/%m/%Y %H:%i:%s'), '') SEPARATOR '<br>') AS scan
            FROM kary a
            INNER JOIN sk b ON a.`nip` = b.`nip`
            INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            INNER JOIN pos d ON d.`id_pos` = c.`id_pos`
            INNER JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            LEFT JOIN (
                SELECT DISTINCT(scan_date), nip, pin 
                FROM tb_scanlog
                WHERE DATE(scan_date) = '$date'
                
            ) AS scan ON scan.nip = a.`nip`
            WHERE b.`aktif` = 1 
            AND ((ISNULL(a.tgl_resign) OR a.tgl_resign >= '$date') AND a.tgl_masuk <= '$date')
            $cabang
            GROUP BY a.nip")->result();
    }

    function jumlah_scandate($date = '')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cab` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }
        
        return $this->db->query("
            SELECT a.pin, a.nama, e.`cabang`, a.nip,
            GROUP_CONCAT(IFNULL(DATE_FORMAT(scan.scan_date, '%d/%m/%Y %H:%i:%s'), '') SEPARATOR '<br>') AS scan
            FROM kary a
            INNER JOIN sk b ON a.`nip` = b.`nip`
            INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            INNER JOIN pos d ON d.`id_pos` = c.`id_pos`
            INNER JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            LEFT JOIN (
                SELECT DISTINCT(scan_date), pin FROM tb_scanlog
                WHERE DATE(scan_date) = '$date'
                
            ) AS scan ON scan.pin = a.`pin`
            WHERE b.`aktif` = 1 AND ISNULL(a.tgl_resign)
            AND scan.scan_date IS NOT NULL
            $cabang
            GROUP BY a.nip")->num_rows();
    }

    function laporan_teguran($tahun = '', $bulan = '')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $where = '';
        if (!empty($id_cabang) || !empty($sal_pos) || !empty($divisi)) {
            $where = $cabang . $kelas . $div;
        }

        $condition = ($bulan) ? "AND a.`bulan` = '$bulan' AND a.`tahun` = '$tahun'" : "AND a.`tahun` = '$tahun'";

        $cari_data = $this->db->query("
                SELECT a.*,b.`nama`,f.`cabang` FROM tb_teguran a 
                JOIN kary b ON a.`nip` = b.`nip`
                JOIN sk c ON c.`nip` = b.`nip`
                JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                JOIN pos e ON e.`id_pos` = d.`id_pos`
                JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
                WHERE c.`aktif` = '1' $condition 
                $where
                ")->result();
        
        return $cari_data;
    }

    function view_template($id = '')
    {
        if ($id) {
            return $this->db->query("
                SELECT a.id, a.0 as minggu, a.1 as senin, a.2 as selasa, a.3 as rabu, a.4 as kamis, a.5 as jumat, a.6 as sabtu, 
                a.description 
                FROM tb_template_shift a 
                WHERE a.id = '$id'")->row();
        } else {
            return $this->db->query("
                SELECT a.*, IFNULL(b.nama,'') AS minggu, IFNULL(c.nama,'') AS senin,
                IFNULL(d.nama,'') AS selasa, IFNULL(e.nama,'') AS rabu, IFNULL(f.nama,'') AS kamis,
                IFNULL(g.nama,'') AS jumat, IFNULL(h.nama,'') AS sabtu 
                FROM tb_template_shift a
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS b ON b.id_shift = a.`0`
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS c ON c.id_shift = a.`1`
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS d ON d.id_shift = a.`2`
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS e ON e.id_shift = a.`3`
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS f ON f.id_shift = a.`4`
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS g ON g.id_shift = a.`5`
                LEFT JOIN (
                    SELECT * FROM ms_shift
                ) AS h ON h.id_shift = a.`6`")->result();
        }
    }

    function view_absensi_nip2($nip = '')
    {
        $q = $this->db->query("SELECT tgl_masuk, tgl_resign 
                                FROM kary 
                                WHERE nip = '$nip'")->row();
        $tgl_awal = isset($q->tgl_masuk) ? $q->tgl_masuk : '';
        $tgl_resign = isset($q->tgl_resign) ? $q->tgl_resign : '';

        $today = date('Y-m-d');
        $w = $this->db->query("SELECT tgl_akhir 
                                FROM q_det 
                                WHERE '$today' BETWEEN tgl_awal AND tgl_akhir")->row();
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';

        if ($tgl_resign == '') {
            $w_akhir = "CURDATE()";
        } else {
            $w_akhir = $tgl_resign;
        }

        return $this->db->query("
            SELECT 
                DISTINCT(a.`qd_id`),
                DATE_FORMAT(a.`tgl_akhir`,'%M %Y') periode
            ,(
                SELECT COALESCE(SUM(presensi.`mangkir`),0) 
                FROM presensi 
                WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` 
                AND presensi.`nip`='$nip'
            ) jml_mangkir
            ,(
                SELECT COALESCE(SUM(presensi.`sakit`),0) 
                FROM presensi 
                WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` 
                AND presensi.`nip`='$nip'
            ) jml_sakit
            ,(
                SELECT COALESCE(SUM(presensi.`ijin`),0) 
                FROM presensi 
                WHERE presensi.`tgl` BETWEEN a.`tgl_awal` AND a.`tgl_akhir` 
                AND presensi.`nip`='$nip'
            ) jml_ijin
            ,(
                SELECT COUNT(lembur.`id_lembur`) 
                FROM lembur 
                WHERE lembur.`nip`='$nip' 
                AND lembur.`tgl` BETWEEN a.`tgl_awal` 
                AND a.`tgl_akhir`
            ) jml_lembur
            ,(
                SELECT COUNT(cuti_sub_det.`tgl`) 
                FROM cuti_det 
                JOIN cuti_sub_det ON cuti_det.`id_cuti_det`=cuti_sub_det.`id_cuti_det` 
                WHERE cuti_sub_det.`tgl` BETWEEN a.`tgl_awal` 
                AND a.`tgl_akhir` 
                AND cuti_det.`nip`='$nip'
            ) jml_cuti
            ,udf_jml_hr_kerja(a.`tgl_awal` , a.`tgl_akhir`) AS jml_kerja
            ,DATEDIFF(a.`tgl_akhir` , a.`tgl_awal`) + 1 AS hari
            FROM q_det a 
            WHERE a.`tgl_awal` >= $tgl_awal
            AND a.tgl_akhir <= $w_akhir
            ORDER BY a.tgl_akhir DESC")->result();
    }

    function laporan_absensi($id_cabang = '', $date_start = '', $date_end = '', $nip = '')
    {
        $condition = '';
        if ($nip != '') {
            $condition .= " AND a.nip = '$nip' "; 
        }

        return $this->db->query("
                SELECT a.`nip`, a.`pin`, f.`nama`, e.`cabang`, a.`tgl`, a.`scan_masuk`, a.`scan_pulang`, a.`shift`, a.`jam_masuk`, a.`status`,
                IFNULL(klar_abs.klarifikasi, '') AS klarifikasi_absensi, IFNULL(UPPER(klar_abs.presensi), '') AS klarifikasi_presensi,
                IFNULL(klar_ter.klarifikasi, '') AS klarifikasi_terlambat
                FROM tb_absensi a 
                JOIN sk b ON a.`nip` = b.`nip`
                JOIN pos_sto c ON b.`id_pos_sto` = c.`id_sto`
                JOIN pos d ON c.`id_pos` = d.`id_pos`
                JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
                JOIN kary f ON a.`nip` = f.`nip`
                LEFT JOIN (
                    SELECT * 
                    FROM tb_klarifikasi_absensi t
                    WHERE t.`tgl` BETWEEN '$date_start' AND '$date_end'
                    GROUP BY t.`nip`, t.`tgl`
                ) AS klar_abs ON klar_abs.nip = a.`nip` AND klar_abs.tgl = a.`tgl`
                LEFT JOIN (
                    SELECT * 
                    FROM tb_klarifikasi_terlambat r
                    WHERE r.`tgl` BETWEEN '$date_start' AND '$date_end'
                    GROUP BY r.`nip`, r.`tgl`
                ) AS klar_ter ON klar_ter.nip = a.`nip` AND klar_ter.tgl = a.`tgl`
                WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
                AND b.`aktif` = 1 
                AND e.`id_cab` = '$id_cabang'
                $condition 
                ORDER BY a.nip ASC ")->result();
    }

    function scan_from_pin($pin = '', $tgl = '')
    {
        return $this->db->query("
            SELECT GROUP_CONCAT(DISTINCT(a.`scan_date`) SEPARATOR '<br>') AS scanlog
            FROM tb_scanlog a
            WHERE a.`pin` = '$pin'
            AND DATE(a.`scan_date`) = '$tgl'
            GROUP BY a.`pin`")->row();
    }

    function view_edit_klarifikasi($id = '')
    {
        return $this->db->query("
            SELECT a.*, b.`nama`, c.`scan_masuk`, c.`scan_pulang`, c.`status`,
            DATE_FORMAT(a.tgl, '%d/%m/%Y') AS tanggal
            FROM tb_klarifikasi_absensi a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN tb_absensi c ON c.`id_ab` = a.`id_ab`
            WHERE a.`id` = '$id'")->row();
    }

    function view_rekap_periode($tahun = '', $bulan = '')
    {
        $p = periode($tahun, $bulan);
        $periode = isset($p->qd_id) ? $p->qd_id : '';
        $tgl_awal = isset($p->tgl_awal) ? $p->tgl_awal : '';
        $tgl_akhir = isset($p->tgl_akhir) ? $p->tgl_akhir : '';

        $ab = $this->Gaji_Model->periode_absensi($tgl_awal, $tgl_akhir);
        $tgl_awal_ab = isset($ab->tgl_awal_ab) ? $ab->tgl_awal_ab : '';
        $tgl_akhir_ab = isset($ab->tgl_akhir_ab) ? $ab->tgl_akhir_ab : '';

        return $this->db->query("
            SELECT * FROM 
            (   SELECT a.nip, $periode AS id_periode,
                DATE_FORMAT('$tgl_akhir','%M %Y') AS periode,
                cabang.id_sk, a.tgl_masuk, a.tgl_resign, a.nama, cabang.cabang,
                udf_jml_hr_kerja('$tgl_awal' , '$tgl_akhir') AS jml_kerja,
                DATEDIFF('$tgl_akhir' , '$tgl_awal') + 1 AS hari,
                IFNULL(ct.jml_cuti,0) AS jml_cuti,
                IFNULL(pr.sakit,0) AS jml_sakit,
                IFNULL(pr.ijin,0) AS jml_ijin,
                IFNULL(pr.mangkir,0) AS jml_mangkir,
                IFNULL(dayoff.jml,0) AS jml_dayoff,
                '$tgl_awal' AS tgl_awal_ab,
                '$tgl_akhir' AS tgl_akhir_ab,
                CASE 
                    WHEN (a.tgl_masuk BETWEEN '$tgl_awal' AND '$tgl_akhir')
                    THEN DATEDIFF(a.tgl_masuk, '$tgl_awal') + 1
                    ELSE 0
                END AS jml_potong,
                CASE 
                    WHEN (a.tgl_resign BETWEEN '$tgl_awal' AND '$tgl_akhir')
                    THEN DATEDIFF('$tgl_akhir', a.tgl_resign) + 1
                    ELSE 0
                END AS jml_resign,
                IFNULL(telat.jml,0) AS jml_telat,
                IFNULL(f.qt,0) AS sisa_cuti,
                cabang.jab, a.kary_stat
                FROM kary a 
                LEFT JOIN cuti_dep f ON f.nip = a.nip AND f.th = YEAR('$tgl_akhir') 
                LEFT JOIN 
                (
                    SELECT COUNT(cuti_sub_det.tgl) AS jml_cuti, nip FROM cuti_det 
                    JOIN cuti_sub_det ON cuti_det.id_cuti_det=cuti_sub_det.id_cuti_det 
                    WHERE cuti_sub_det.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                    GROUP BY nip
                ) ct ON ct.nip = a.nip
                LEFT JOIN 
                (
                    SELECT SUM(sakit) AS sakit, SUM(ijin) AS ijin, SUM(mangkir) AS mangkir, nip FROM presensi
                    WHERE presensi.tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                    GROUP BY nip
                ) pr ON pr.nip = a.nip
                LEFT JOIN 
                (
                    SELECT COUNT(tgl) AS jml, nip FROM tr_shift 
                    JOIN ms_shift ON tr_shift.id_shift = ms_shift.id_shift
                    WHERE ms_shift.nama LIKE '%day off%' AND tgl BETWEEN '$tgl_awal' AND '$tgl_akhir'
                    GROUP BY nip
                ) dayoff ON dayoff.nip = a.nip
                LEFT JOIN 
                (
                    SELECT MAX(id_sk) AS id_sk,ms_cabang.`cabang`,nip, pos.*
                    FROM sk 
                    JOIN pos_sto ON sk.`id_pos_sto` = pos_sto.`id_sto`
                    JOIN pos ON pos.`id_pos` = pos_sto.`id_pos`
                    JOIN ms_cabang ON ms_cabang.`id_cab` = pos.`id_cabang`
                    GROUP BY sk.`nip`
                ) cabang ON cabang.nip = a.nip
                LEFT JOIN 
                (
                    SELECT COUNT(tgl) AS jml, nip FROM terlambat_temp 
                    WHERE terlambat_temp.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                    GROUP BY terlambat_temp.`nip`
                ) AS telat ON telat.nip = a.nip
                
                WHERE (
                    (ISNULL(a.tgl_resign) 
                    OR (a.tgl_masuk BETWEEN '$tgl_awal' AND '$tgl_akhir')) 
                    OR (a.tgl_resign BETWEEN '$tgl_awal' AND '$tgl_akhir')
                )
            ) AS presensi GROUP BY nip ORDER BY nama")->result();
    }

    function cek_pulang_awal($nip = '', $tgl = '')
    {
        return $this->db->query("
                    SELECT *
                    FROM tb_pulang_awal a
                    WHERE a.`tgl` = '$tgl'
                    AND a.`nip` = '$nip'
                    AND a.`status` = '12'")->row();
    }

    function verifikasi_pulang_awal()
    {
        $username = $this->session->userdata('username');
        $query = $this->db->where('user', $username)
                    ->get('ms_reminder')
                    ->row();
        $nip_wtf = isset($query->nip) ? $query->nip  : '';

        return $this->db->query("
            SELECT a.`id_pulang_awal`, a.`nip`, b.`nama`, a.`tgl`, a.`jam`,
            c.`keterangan`, a.`alasan`, c.flag
            FROM tb_pulang_awal a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN ms_status_approval c ON c.`kode` = a.`status`
            WHERE a.`penyetuju` = '$nip_wtf'
            AND c.`kode` <> 12
            AND c.`flag` <> 0
            ORDER BY a.tgl DESC")->result();
    }

    function verifikasi_tinggal()
    {
        $username = $this->session->userdata('username');
        $query = $this->db->where('user', $username)
                    ->get('ms_reminder')
                    ->row();
        $nip_wtf = isset($query->nip) ? $query->nip  : '';

        return $this->db->query("
            SELECT a.`id`, a.`nip`, b.`nama`, a.`tgl`, a.`jmljam`,
            a.`keterangan`, c.`keterangan` AS status_approval, c.flag,
            a.dari, a.sampai
            FROM ijinjam a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN ms_status_approval c ON c.`kode` = a.`status`
            WHERE a.`penyetuju` = '$nip_wtf'
            AND c.`kode` <> 12
            AND c.`flag` <> 0
            ORDER BY a.tgl DESC")->result();
    }
}
