<?php

class Gaji_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function grouplokasi($id)
    {
        $idp    = $this->session->userdata('idp');
        if($id==0)
        {
            $opt    = $this->db->query("SELECT * FROM lwok a WHERE a.`idp` = '$idp'")->result();
            foreach ($opt as $row) {
                $result[$row->id_lwok] = $row->wok;
            }
        }
        elseif($id==1)
        {
            $opt    = $this->db->query("SELECT * FROM lwok a JOIN lku b ON a.`id_lwok`=b.`id_lwok` WHERE a.`idp` = '$idp'")->result();
            foreach ($opt as $row) {
                $result[$row->id_lku] = $row->lku;
            }
        }
        else
        {
            $opt    = $this->db->query("SELECT * FROM lwok a JOIN lku b ON a.`id_lwok`=b.`id_lwok` JOIN lka c ON b.`id_lku`=c.`id_lku` WHERE a.`idp` = '$idp'")->result();
            foreach ($opt as $row) {
                $result[$row->id_lka] = $row->lka;
            }
        }
        return $result;
    }

    function gaji_all_kary($tgl_awal, $tgl_akhir, $grouplokasi, $lokasi)
    {
        $idp    = $this->session->userdata('idp');
        //if lwok
        if($grouplokasi==0)
        {
            $result = $this->db->query("SELECT * FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN lku d ON c.`id_lku`=d.`id_lku` JOIN lwok e ON d.`id_lwok`=e.`id_lwok` JOIN pos f ON c.`id_pos`=f.`id_pos` WHERE (ISNULL(a.`tgl_resign`) OR (a.`tgl_resign`< '$tgl_akhir' AND a.`tgl_resign`> '$tgl_awal')) AND b.`aktif`='1' AND e.`idp`='$idp' AND e.`id_lwok`='$lokasi' OR (a.`tgl_masuk`>='$tgl_awal' AND a.`tgl_masuk`<='$tgl_akhir')")->result();
        }
        // if lku
        elseif($grouplokasi==1)
        {
            $result = $this->db->query("SELECT * FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN lku d ON c.`id_lku`=d.`id_lku` JOIN lwok e ON d.`id_lwok`=e.`id_lwok` JOIN pos f ON c.`id_pos`=f.`id_pos` WHERE (ISNULL(a.`tgl_resign`) OR (a.`tgl_resign`< '$tgl_akhir' AND a.`tgl_resign`> '$tgl_awal')) AND b.`aktif`='1' AND e.`idp`='$idp' AND d.`id_lku`='$lokasi' OR (a.`tgl_masuk`>='$tgl_awal' AND a.`tgl_masuk`<='$tgl_akhir')")->result();
        }
        // else lka
        elseif($grouplokasi==2)
        {
            $result = $this->db->query("SELECT * FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN lku d ON c.`id_lku`=d.`id_lku` JOIN lwok e ON d.`id_lwok`=e.`id_lwok` JOIN pos f ON c.`id_pos`=f.`id_pos` JOIN lka g ON d.`id_lku`=g.`id_lku` WHERE (ISNULL(a.`tgl_resign`) OR (a.`tgl_resign`< '$tgl_akhir' AND a.`tgl_resign`> '$tgl_awal')) AND b.`aktif`='1' AND e.`idp`='$idp' AND g.`id_lka`='$lokasi' OR (a.`tgl_masuk`>='$tgl_awal' AND a.`tgl_masuk`<='$tgl_akhir')")->result();
        }
        // else all wok
        else
        {
            $result = $this->db->query("SELECT * FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN lku d ON c.`id_lku`=d.`id_lku` JOIN lwok e ON d.`id_lwok`=e.`id_lwok` JOIN pos f ON c.`id_pos`=f.`id_pos` WHERE (ISNULL(a.`tgl_resign`) OR (a.`tgl_resign`< '$tgl_akhir' AND a.`tgl_resign`> '$tgl_awal')) AND b.`aktif`='1' AND e.`idp`='$idp' OR (a.`tgl_masuk`>='$tgl_awal' AND a.`tgl_masuk`<='$tgl_akhir')")->result();
        }


        return $result;
    }

    function view_gaji_after_submit($periode='', $cabang='', $table='temp_payroll')
    {
        // $condition = ($cabang != '') ? " AND g.id_cab = '$cabang' " : "";

        if ($cabang == 0) {
            $id_cabang = $this->Main_Model->session_cabang();
            $condition = '';
            if(!empty($id_cabang)) {
                $condition .= 'AND (';
                for($i = 0; $i < count($id_cabang); $i++) {
                    $condition .= ' g.`id_cab` = '.$id_cabang[$i];

                    if(end($id_cabang) != $id_cabang[$i]) {
                        $condition .= ' OR';
                    } else {
                        $condition .= ')';
                    }
                }
            }
        } else {
            $condition = " AND g.id_cab = '$cabang' ";
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " f.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT a.`nip`, b.`pin`, b.`nama`, a.id_periode, a.id,
            g.`cabang`, f.`divisi`, e.`jab`, DATE_FORMAT(h.`tgl_akhir`, '%M %Y') AS periode,
            b.`kary_stat`, b.`norek`, a.`hari_upah_pokok`, a.`hari_tunj_jabatan`, a.`hari_pengh_kerja`,
            a.`hari_tidak_tetap`, a.`tunj_kemahalan`, a.`ins_presensi`, a.`telat`, a.`mangkir`, a.`cuti`, a.ijin,
            a.`sakit`, a.`sisa_cuti`, a.`cuti_khusus`, b.tgl_resign, b.tgl_masuk, h.tgl_awal, h.tgl_akhir,
            CONCAT(DATE_FORMAT(DATE_ADD(h.tgl_awal, INTERVAL -1 MONTH), '%Y-%m-'),'26') AS tgl_awal_ab, 
            CONCAT(DATE_FORMAT(h.tgl_akhir, '%Y-%m-'),'25') AS tgl_akhir_ab
            FROM $table a
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON c.`id_sk` = a.`id_sk`
            JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            JOIN pos e ON d.`id_pos` = e.`id_pos`
            JOIN ms_divisi f ON e.`id_divisi` = f.`id_divisi`
            JOIN ms_cabang g ON g.`id_cab` = e.`id_cabang`
            JOIN q_det h ON h.`qd_id` = a.`id_periode`
            WHERE a.id_periode = '$periode' $condition $div")->result();
    }

    function view_gaji($periode,$lokasi,$grouplokasi)
    {
        $idp    = $this->session->userdata('idp');
          //if lwok
        if($grouplokasi==0)
        {
            $result = $this->db->query("SELECT a.*,b.`nama`,d.*,f.*,h.*,DATE_FORMAT(g.`tgl_akhir`,'%M %Y') periode FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`nip`=c.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN lku e ON d.`id_lku`=e.`id_lku` JOIN lwok f ON e.`id_lwok`=f.`id_lwok` JOIN q_det g ON a.`id_per`=g.`qd_id` JOIN pos h ON d.`id_pos`=h.`id_pos` WHERE f.`id_lwok` = '$lokasi' AND c.`aktif`='1'")->result();
        }
        // if lku
        elseif($grouplokasi==1)
        {
            $result = $this->db->query("SELECT a.*,b.`nama`,d.*,f.*,h.*,DATE_FORMAT(g.`tgl_akhir`,'%M %Y') periode FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`nip`=c.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN lku e ON d.`id_lku`=e.`id_lku` JOIN lwok f ON e.`id_lwok`=f.`id_lwok` JOIN q_det g ON a.`id_per`=g.`qd_id` JOIN pos h ON d.`id_pos`=h.`id_pos` WHERE e.`id_lku` = '$lokasi' AND c.`aktif`='1'")->result();
        }
        // else lka
        elseif($grouplokasi==2)
        {
            $result = $this->db->query("SELECT a.*,b.`nama`,d.*,f.*,h.*,DATE_FORMAT(g.`tgl_akhir`,'%M %Y') periode FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`nip`=c.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN lku e ON d.`id_lku`=e.`id_lku` JOIN lwok f ON e.`id_lwok`=f.`id_lwok` JOIN q_det g ON a.`id_per`=g.`qd_id` JOIN pos h ON d.`id_pos`=h.`id_pos` WHERE d.`id_lka` = '$lokasi' AND c.`aktif`='1'")->result();
        }
        // else all wok
        else
        {
            $result = $this->db->query("SELECT a.*,b.`nama`,d.*,f.*,h.*,DATE_FORMAT(g.`tgl_akhir`,'%M %Y') periode FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` JOIN sk c ON a.`nip`=c.`nip` JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` JOIN lku e ON d.`id_lku`=e.`id_lku` JOIN lwok f ON e.`id_lwok`=f.`id_lwok` JOIN q_det g ON a.`id_per`=g.`qd_id` JOIN pos h ON d.`id_pos`=h.`id_pos` WHERE f.`idp`='$idp' AND c.`aktif`='1'")->result();
        }
        return $result;
    }

    function cari_p_temp($nip,$id_per)
    {
        return $this->db->query("select * from p_temp where nip='$nip' and id_per='$id_per'")->result();
    }

    function insert_p_hist($data='')
    {
        $this->db->insert('tb_payroll',$data);
    }

    function add_p_temp($data)
    {
        $this->db->insert('p_temp',$data);
    }

    function delete_p_temp($id='')
    {
        $this->db->where('id', $id)->delete('temp_payroll');
    }

    function view_gaji_nip($nip)
    {
        return $this->db->query("SELECT a.*,DATE_FORMAT(b.`tgl_akhir`,'%M %Y') AS periode FROM p_hist a JOIN q_det b ON a.`id_per`=b.`qd_id` WHERE a.`nip`='$nip'")->result();
    }

    function slip_gaji($nip,$per)
    {
        return $this->db->query("SELECT * FROM p_hist a WHERE a.`nip` = '$nip' AND a.`id_per` = '$per'")->row();
    }

    function tutup_periode($id_per, $data)
    {
        $this->db->where('qd_id',$id_per)->update('q_det',$data);
    }

    function delete_p_hist($nip='', $id_per='')
    {
        $this->db->where('nip',$nip)->where('id_periode',$id_per)->delete('tb_payroll');
    }

    function process_edit_payroll($data='', $id='')
    {
        $this->db->where('id', $id)->update('temp_payroll', $data);
    }

    function periode_absensi($tgl_awal='', $tgl_akhir='')
    {
        $p = $this->db->query("
                SELECT 
                CONCAT(DATE_FORMAT(DATE_ADD('$tgl_awal', INTERVAL -1 MONTH), '%Y-%m-'),'26') AS tgl_awal_ab, 
                CONCAT(DATE_FORMAT('$tgl_akhir', '%Y-%m-'),'25') AS tgl_akhir_ab")->row();

        return $p;
    }

    function generate_gaji($periode='', $cabang='', $fk='')
    {
        $per = $this->Main_Model->id_periode($periode);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $p = $this->periode_absensi($tgl_awal, $tgl_akhir);

        $tgl_awal_ab = isset($p->tgl_awal_ab) ? $p->tgl_awal_ab : '';
        $tgl_akhir_ab = isset($p->tgl_akhir_ab) ? $p->tgl_akhir_ab : '';

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " cabang.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $cab = '';
        if ($cabang == 0) {
            $id_cabang = $this->Main_Model->session_cabang();
            $cab = '';
            if(!empty($id_cabang)) {
                $cab .= 'AND (';
                for($i = 0; $i < count($id_cabang); $i++) {
                    $cab .= ' cabang.id_cabang = '.$id_cabang[$i];

                    if(end($id_cabang) != $id_cabang[$i]) {
                        $cab .= ' OR';
                    } else {
                        $cab .= ')';
                    }
                }
            }
        } else {
            $cab = " AND cabang.id_cabang = '$cabang' ";
        }

        // $cab = '';
        // if ($cabang != '') {
        //     $cab .= " c.id_cabang = '$cabang' ";
        // }

        return $this->db->query("
            SELECT * FROM 
            (   SELECT a.nip, $periode AS id_periode,
                DATE_FORMAT('$tgl_akhir','%M %Y') AS periode,
                cabang.id_sk, a.tgl_masuk, a.tgl_resign, a.nama, cabang.cabang,
                udf_jml_hr_kerja('$tgl_awal_ab' , '$tgl_akhir_ab') AS jml_kerja,
                DATEDIFF('$tgl_akhir' , '$tgl_awal') + 1 AS hari,
                IFNULL(ct.jml_cuti,0) AS jml_cuti,
                IFNULL(pr.sakit,0) AS jml_sakit,
                IFNULL(pr.ijin,0) AS jml_ijin,
                IFNULL(pr.mangkir,0) AS jml_mangkir,
                IFNULL(dayoff.jml,0) AS jml_dayoff,
                CASE 
                    WHEN (a.tgl_masuk BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab')
                    THEN DATEDIFF(a.tgl_masuk, '$tgl_awal_ab') + 1
                    ELSE 0
                END AS jml_potong,
                CASE 
                    WHEN (a.tgl_resign BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab')
                    THEN DATEDIFF('$tgl_akhir_ab', a.tgl_resign) + 1
                    ELSE 0
                END AS jml_resign,
                IFNULL(telat.jml,0) AS jml_telat,
                IFNULL(f.qt,0) AS sisa_cuti,
                cabang.jab, a.kary_stat
                FROM kary a 
                LEFT JOIN cuti_dep f ON f.nip = a.nip AND f.th = YEAR('$tgl_akhir') 
                LEFT JOIN 
                (
                    SELECT COUNT(cuti_sub_det.tgl) AS jml_cuti, nip FROM cuti_det 
                    JOIN cuti_sub_det ON cuti_det.id_cuti_det=cuti_sub_det.id_cuti_det 
                    WHERE cuti_sub_det.tgl BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab'
                    GROUP BY nip
                ) ct ON ct.nip = a.nip
                LEFT JOIN 
                (
                    SELECT SUM(sakit) AS sakit, SUM(ijin) AS ijin, SUM(mangkir) AS mangkir, nip FROM presensi
                    WHERE presensi.tgl BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab'
                    GROUP BY nip
                ) pr ON pr.nip = a.nip
                LEFT JOIN 
                (
                    SELECT COUNT(tgl) AS jml, nip FROM tr_shift 
                    JOIN ms_shift ON tr_shift.id_shift = ms_shift.id_shift
                    WHERE ms_shift.nama LIKE '%day off%' AND tgl BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab'
                    GROUP BY nip
                ) dayoff ON dayoff.nip = a.nip
                LEFT JOIN 
                (
                    SELECT MAX(id_sk) AS id_sk,ms_cabang.`cabang`,nip, pos.*
                    FROM sk 
                    JOIN pos_sto ON sk.`id_pos_sto` = pos_sto.`id_sto`
                    JOIN pos ON pos.`id_pos` = pos_sto.`id_pos`
                    JOIN ms_cabang ON ms_cabang.`id_cab` = pos.`id_cabang`
                    GROUP BY sk.`nip`
                ) cabang ON cabang.nip = a.nip
                LEFT JOIN 
                (
                    SELECT COUNT(tgl) AS jml, nip FROM terlambat_temp 
                    WHERE terlambat_temp.`tgl` BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab'
                    GROUP BY terlambat_temp.`nip`
                ) AS telat ON telat.nip = a.nip
                
                WHERE (
                    (ISNULL(a.tgl_resign) 
                    OR (a.tgl_masuk BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab')) 
                    OR (a.tgl_resign BETWEEN '$tgl_awal_ab' AND '$tgl_akhir_ab')
                )
                $cab
                $div
            ) AS presensi GROUP BY nip ORDER BY nama")->result(); 
    }

    function cuti_khusus($periode='', $nip='')
    {
        $per = $this->Main_Model->id_periode($periode);
        $tgl_awal = $per->tgl_awal;
        $tgl_akhir = $per->tgl_akhir;

        $tgl_awal_ab = $per->tgl_awal_ab;
        $tgl_akhir_ab = $per->tgl_akhir_ab;

        $q = $this->db->query("
            SELECT * FROM cuti_khusus a
            WHERE a.`nip` = '$nip'
            AND (('$tgl_awal_ab' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`) OR 
            ('$tgl_akhir_ab' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`)) ")->row();

        $c_awal = '';
        $c_akhir = '';
        if (!empty($q)) {
            if ($q->tgl_awal <= $tgl_awal_ab) {
                $c_awal = $tgl_awal_ab;
            }

            if ($q->tgl_awal >= $tgl_awal_ab) {
                $c_awal = $q->tgl_awal;
            }

            if ($q->tgl_akhir <= $tgl_akhir_ab) {
                $c_akhir = $q->tgl_akhir;
            }

            if ($q->tgl_akhir >= $tgl_akhir_ab) {
                $c_akhir = $tgl_akhir_ab;
            }
        }

        $jumlah = 0;
        if ($c_awal != '' && $c_akhir != '') {
            $diff = $this->db->query("SELECT DATEDIFF('$c_akhir', '$c_awal') AS diff")->row();
            $jumlah = isset($diff->diff) ? $diff->diff : 0;
        }

        return $jumlah;
    }

    function tunj_penghargaan($tgl_masuk='') 
    {
        $result = FALSE;
        if ($tgl_masuk != '') {
            $th = date('Y');
            $batas = $th.'-01-01';

            $last_year = $th - 1;
            $batas_lalu = $last_year.'-01-31';

            if (strtotime($tgl_masuk) <= strtotime($batas_lalu)) {
                $diff_month = $this->Main_Model->diff_month($tgl_masuk, $batas);

                if ($diff_month >= 12) {
                    $result = TRUE;
                }
            }
        }

        return $result;
    }

    function view_kemahalan()
    {
        return $this->db->query("
            SELECT a.`nip`, b.`nama`, f.`cabang`, g.`divisi`, e.`jab`, a.id
            FROM tb_tunj_kemahalan a
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON a.`nip` = c.`nip`
            JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            JOIN pos e ON d.`id_pos` = e.`id_pos`
            JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
            JOIN ms_divisi g ON g.`id_divisi` = e.`id_divisi`
            WHERE c.`aktif` = 1
            ")->result();
    }   

    function view_detail_gaji($id='', $table='')
    {
        $table = ($table == '') ? 'temp_payroll' : 'tb_payroll';
        $query = $this->db->query("
            SELECT b.`nama`, e.`jab`, f.`cabang`, g.`divisi`, 
            DATE_FORMAT(h.`tgl_akhir`, '%M %Y') AS periode, a.* 
            FROM $table a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN sk c ON c.`id_sk` = a.`id_sk`
            INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            INNER JOIN pos e ON d.`id_pos` = e.`id_pos`
            INNER JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
            INNER JOIN ms_divisi g ON g.`id_divisi` = e.`id_divisi`
            INNER JOIN q_det h ON a.`id_periode` = h.`qd_id`
            WHERE a.`id` = '$id'")->row();
        return $query;
    }

    function log_payroll($id='')
    {
        $this->db->query("INSERT INTO log_edit_payroll (SELECT * FROM temp_payroll WHERE id = '$id')");
    }
}
?>