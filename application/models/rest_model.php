<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rest_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $this->load->model('Absensi_Model');
        $this->load->library('Rsaencrypt');
    }

    function check_auth()
    {
        $consumer_id = $this->input->get_request_header('Consumer-id', true);
        $secret_key = $this->input->get_request_header('Secret-key', true);

        $q = $this->db->where('consumer_id', $consumer_id)
                ->where('secret_key', $secret_key)
                ->get('ms_user_api')
                ->row();

        if (!empty($q)) {
            $status = 1;
            $message = '';
        } else {
            $status = 0;
            $message = 'Unauthorized';
        }

        $response = array(
                'response' => '',
                'metadata' => array(
                    'status' => $status,
                    'message' => $message
                )
            );

        return $response;
    }

    function login($username = '', $password = '')
    {
        $db = $this->Main_Model->connect_to_absensi();
        $q = $db->where('username', $username)
            ->where('password', md5($password))
            ->where('status', 1)
            ->get('ms_user')
            ->row();

        if (empty($q)) {
            $response = array(
                'response' => '',
                'metadata' => array(
                    'status' => 0,
                    'message' => 'Username/Password salah!'
                )
            );
        } else {
            $hashed_password = isset($q->password) ? $q->password : '';
            $nip = isset($q->id_employee) ? $q->id_employee : '';

            // karyawan
            $karyawan = $this->db->where('nip', $nip)
                            ->get('kary')
                            ->row();
            // tgl_resign
            $tgl_resign = isset($karyawan->tgl_resign) ? $karyawan->tgl_resign : '';

            if ($tgl_resign == '') {
                $token = crypt(substr(md5(date("Y m d H i s u")), 0, 7), '');
                $expired_at = date("Y-m-d H:i:s", strtotime('+24 hours'));
                // $expired_at = date("Y-m-d H:i:s", strtotime('+2 minutes'));

                // cek level
                $level = $this->db->where('nip', $nip)
                            ->get('ms_reminder')
                            ->row();
                $approval = (! empty($level)) ? 1 : 0;

                $data = array(
                    'users_id' => $nip,
                    'token' => $token,
                    'expired_at' => $expired_at
                );
                $create = $this->create_token($data);
                if ($create > 0) {
                    $response = array(
                        'response' => array(
                            'nip' => $nip,
                            'token' => $token,
                            'expired_at' => $expired_at,
                            'approval' => $approval
                        ),
                        'metadata' => array(
                            'status' => 1,
                            'message' => 'Success!'
                        )
                    );
                } else {
                    $response = array(
                        'response' => '',
                        'metadata' => array(
                            'status' => 0,
                            'message' => 'Gagal membuat token!'
                        )
                    );
                }
            } else {
                $response = array(
                    'response' => '',
                    'metadata' => array(
                        'status' => 0,
                        'message' => 'Akun sudah dinonaktifkan.'
                    )
                );
            }
        }

        return $response;
    }

    function auth_login()
    {
        $nip = $this->input->get_request_header('Nip', true);
        $token = $this->input->get_request_header('Token', true);

        // $q = $this->db->query("
        //  SELECT *
        //  FROM users_authentication a
        //  WHERE a.`users_id` = '$nip'
        //  AND a.token = '$token'
        //  AND a.`expired_at` >= NOW()")->row();

        // updated at 15 05 2018
        $q = $this->db->query("
			SELECT a.*
			FROM users_authentication a
			INNER JOIN kary b ON a.users_id = b.nip
			WHERE a.`users_id` = '$nip'
			AND a.token = '$token'
			AND b.tgl_resign IS NULL")->row();

        if (!empty($q)) {
            $expired_at = date("Y-m-d H:i:s", strtotime('+24 hours'));
            $update_at = date('Y-m-d H:i:s');
            $data = array(
                'expired_at' => $expired_at,
                'updated_at' => $update_at
            );

            $this->db->where('token', $token)
                ->where('users_id', $nip)
                ->update('users_authentication', $data);

            $response = array(
                'response' => '',
                'metadata' => array(
                    'status' => 1,
                    'message' => 'Success'
                )
            );
        } else {
            $response = array(
                'response' => '',
                'metadata' => array(
                    'status' => 0,
                    'message' => 'Unauthorized'
                )
            );
        }

        return $response;
    }

    function create_log($data = '')
    {
        $data['user'] = $this->input->get_request_header('Consumer-id', true);
        $data['nip'] = $this->input->get_request_header('Nip', true);
        $this->db->insert('log_trx_api', $data);
    }

    function create_token($data = '')
    {
        $create = $this->db->insert('users_authentication', $data);
        return $this->db->affected_rows();
    }

    function check_in($nip = '', $flag = 0)
    {
        $message = ($flag == 0) ? 'Check In' : 'Check Out';
        // $simpan = $this->Main_Model->process_data('tb_scanlog', $scanlog);
        $this->db->query("
					INSERT INTO tb_scanlog (scan_date, nip, flag, keterangan) 
					VALUES (NOW(), '$nip', '$flag', 'Via App')");
        // $simpan = $this->db->affected_rows();
        $simpan = $this->db->insert_id();
        if ($simpan > 0) {
            $response = array(
                'response' => '',
                'metadata' => array(
                    'status' => 1,
                    'message' => 'You have Successfully '.$message
                )
            );
        } else {
            $response = array(
                'response' => '',
                'metadata' => array(
                    'status' => 0,
                    'message' => $message.' Failed, Try again later'
                )
            );
        }

        return $response;
    }

    function view_cuti($flag = 0, $id = '')
    {
        $nip = $this->input->get_request_header('Nip', true);
        $data = $this->db->where('nip', $nip)
                    ->get('kary')
                    ->row();
        $tgl_masuk = isset($data->tgl_masuk) ? $data->tgl_masuk : '';

        $condition = '';
        if ($id != '') {
            $condition .= "AND a.id_cuti_det = '$id'";
        }
        if ($flag == 1) {
            $condition .= "AND awal.tgl >= CURDATE()";
        }

        if ($flag == 1) {
            return $this->db->query("
	            SELECT a.*, b.`tipe`, c.keterangan, awal.tgl AS mulai, akhir.tgl AS selesai
				FROM cuti_det a 
				INNER JOIN ms_tipe_cuti b ON a.`id_tipe` = b.`id`
				INNER JOIN ms_status_approval c ON c.kode = a.approval
				LEFT JOIN (
					SELECT id_cuti_det, MIN(tgl) AS tgl
					FROM cuti_sub_det
					GROUP BY id_cuti_det
				) AS awal ON awal.id_cuti_det = a.`id_cuti_det`
				LEFT JOIN (
					SELECT id_cuti_det, MAX(tgl) AS tgl
					FROM cuti_sub_det
					GROUP BY id_cuti_det
				) AS akhir ON akhir.id_cuti_det = a.`id_cuti_det`
				WHERE a.`nip` = '$nip'
				AND c.flag = '$flag'
				$condition
				GROUP BY a.`id_cuti_det`
				ORDER BY a.`id_cuti_det` DESC")->result();
        } else {
            return $this->db->query("			
		    	SELECT * 
				FROM (
				(SELECT a.`id_cuti_det`, a.`alasan_cuti`, b.`tipe`, c.keterangan, awal.tgl AS mulai, akhir.tgl AS selesai, a.pola
				FROM cuti_det a 
				INNER JOIN ms_tipe_cuti b ON a.`id_tipe` = b.`id`
				INNER JOIN ms_status_approval c ON c.kode = a.approval
				LEFT JOIN (
					SELECT id_cuti_det, MIN(tgl) AS tgl
					FROM cuti_sub_det
					GROUP BY id_cuti_det
				) AS awal ON awal.id_cuti_det = a.`id_cuti_det`
				LEFT JOIN (
					SELECT id_cuti_det, MAX(tgl) AS tgl
					FROM cuti_sub_det
					GROUP BY id_cuti_det
				) AS akhir ON akhir.id_cuti_det = a.`id_cuti_det`
				WHERE a.`nip` = '$nip'
				AND c.flag = '$flag'
				GROUP BY a.`id_cuti_det`
				ORDER BY a.`id_cuti_det` DESC)
				UNION 
				(SELECT '', hol.`hol_desc`, '', 'Potong Cuti', hol.`hol_tgl`, hol.`hol_tgl`, ''
				FROM hol
				INNER JOIN tr_shift ON tr_shift.tgl = hol.hol_tgl
				INNER JOIN ms_shift ON ms_shift.id_shift = tr_shift.id_shift
				WHERE hol.`hol_tgl` BETWEEN '$tgl_masuk' AND CURDATE()
				AND hol.`cuti` = 1
				AND ms_shift.jam_masuk = '00:00:00' 
				AND ms_shift.jam_pulang = '00:00:00'
				AND tr_shift.nip = '$nip')
				) AS cuti ORDER BY mulai DESC")->result();
        }
    }

    function biodata($nip = '')
    {
        return $this->db->query("
            SELECT a.*, b.`pend`, f.`cabang`, h.`divisi`, e.`jab`, e.`sal_pos`,
            g.`departemen`, h.id_divisi
            FROM kary a
            INNER JOIN pend b ON a.`id_pend` = b.`id_pend`
            INNER JOIN sk c ON c.`nip` = a.`nip`
            INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
            INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
            INNER JOIN ms_departemen g ON g.`id_dept` = e.`id_departemen`
            INNER JOIN ms_divisi h ON h.`id_divisi` = e.`id_divisi`
            WHERE a.`nip` = '$nip'")->row();
    }

    function news($id = '')
    {
        if ($id == '') {
            return $this->db->query("
    				SELECT *
					FROM tb_news a
					WHERE CURDATE() BETWEEN DATE(a.`insert_at`)
					AND (DATE_ADD(DATE(a.`insert_at`), INTERVAL 7 DAY))
					ORDER BY a.insert_at DESC")->result();
        } else {
            return $this->db->query("
    				SELECT *
					FROM tb_news a
					WHERE a.id = '$id'")->row();
        }
    }

    function view_log($date = '')
    {
        return $this->db->where('DATE(insert_at)', $date)
                ->order_by('insert_at', 'desc')
                ->get('log_trx_api')
                ->result_array();
    }

    function check_ip($ip = '', $mac = '')
    {
        return $this->db->query("
					SELECT * 
					FROM ms_allowed_ip
					WHERE SUBSTR(macaddress, 1, 9) = SUBSTR('$mac', 1, 9)
					AND ippublic = '$ip'
					AND status = 1")->result();
    }

    function view_terlambat($datestart = '', $dateend = '', $nip = '')
    {
        return $this->db->query("
			SELECT a.`tgl`, a.`jam_masuk`, a.`scan_masuk`, a.`total_menit`
			FROM terlambat_temp a
			WHERE a.`tgl` BETWEEN '$datestart' AND '$dateend'
			AND a.nip = '$nip'")->result();
    }

    function header_terlambat($datestart = '', $dateend = '', $nip = '')
    {
        return $this->db->query("
			SELECT COUNT(a.`tgl`) AS total_hari, SUM(a.`total_menit`) AS total_menit
			FROM terlambat_temp a
			WHERE a.`tgl` BETWEEN '$datestart' AND '$dateend'
			AND a.`nip` = '$nip'")->row();
    }

    function reminder_cuti($data = '', $tgl = '')
    {
        $subject = '[Aplikasi HRD] Pengajuan Cuti';
        $nip = isset($data['nip']) ? $data['nip'] : '';
        $status = isset($data['approval']) ? $data['approval'] : '';
        
        $q = $this->Main_Model->kary_nip($nip);
        $nama = isset($q->nama) ? $q->nama : '';
        $from = isset($q->mail) ? $q->mail : '';

        $penyetuju = isset($data['penyetuju']) ? $data['penyetuju'] : '';
        $cc = isset($data['cc']) ? $this->Main_Model->pecah_cc($data['cc']) : '';
        $w = $this->Main_Model->kary_nip($penyetuju);
        $kepada = isset($w->nama) ? $w->nama : '';
        $to = isset($w->mail) ? $w->mail : '';


        $url = base_url();
        $tahun = date('Y');
        $message = '<table>
	                    <tr><td colspan="3">Kepada Yth, <br /><b>'.$kepada.'</b></td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Dengan ini, saya ingin mengajukan permohonan Cuti dengan detail sbb:</td></tr>
	                    <tr><td><b>Nama</b></td><td> : </td><td>'.$nama.'</td></tr>
	                    <tr><td><b>NIK</b></td><td> : </td><td>'.$nip.'</td></tr>
	                    <tr><td><b>Mulai Cuti</b></td><td> : </td><td>'.tanggal($tgl[0]).'</td></tr>
	                    <tr><td><b>Sampai Dengan</b></td><td> : </td><td>'.tanggal(end($tgl)).'</td></tr>
	                    <tr><td><b>Keperluan</b></td><td> : </td><td>'.$data['alasan_cuti'].'</td></tr>
	                    <tr><td><b>Sisa Cuti</b></td><td> : </td><td>'.sisa_cuti($nip).' Hari</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Demikian yang dapat saya sampaikan, atas perhatiannya saya sampaikan terima kasih.</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Klik Link ini untuk <a href="'.$url.'">Menyetujui atau Menolak</a> Permohonan Cuti</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
	                </table>';

        $kirim = false;
        if ($to != '') {
            $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
        }

        return $kirim;
    }

    function reminder_keluar_kantor($data = '')
    {
        $subject = '[Aplikasi HRD] Ijin Keluar Kantor';
        $nip = isset($data['nip']) ? $data['nip'] : '';
        $status = isset($data['status']) ? $data['status'] : '';
        $tgl = isset($data['tgl']) ? $data['tgl'] : '';
        $jmlmenit = isset($data['jmljam']) ? $data['jmljam'] : '';
        $keterangan = isset($data['keterangan']) ?  $data['keterangan'] : '';
        $dari = isset($data['dari']) ? $data['dari'] : '';
        $sampai = isset($data['sampai']) ? $data['sampai'] : '';

        $q = $this->Main_Model->kary_nip($nip);
        $nama = isset($q->nama) ? $q->nama : '';
        $from = isset($q->mail) ? $q->mail : '';

        $penyetuju = isset($data['penyetuju']) ? $data['penyetuju'] : '';
        $cc = isset($data['cc']) ? $this->Main_Model->pecah_cc($data['cc']) : '';
        $w = $this->Main_Model->kary_nip($penyetuju);
        $kepada = isset($w->nama) ? $w->nama : '';
        $to = isset($w->mail) ? $w->mail : '';

        $url = base_url();
        $tahun = date('Y');
        $message = '<table>
	                    <tr><td colspan="3">Kepada Yth, <br /><b>'.$kepada.'</b></td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Dengan ini, saya ingin mengajukan permohonan Ijin Keluar Kantor dengan detail sbb:</td></tr>
	                    <tr><td><b>Nama</b></td><td> : </td><td>'.$nama.'</td></tr>
	                    <tr><td><b>NIK</b></td><td> : </td><td>'.$nip.'</td></tr>
	                    <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
	                    <tr><td><b>Dari</b></td><td> : </td><td>'.jam($dari).'</td></tr>
	                    <tr><td><b>Sampai</b></td><td> : </td><td>'.jam($sampai).'</td></tr>
	                    <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Demikian yang dapat saya sampaikan, atas perhatiannya saya sampaikan terima kasih.</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Klik Link ini untuk <a href="'.$url.'">Menyetujui atau Menolak</a> Permohonan Keluar Kantor</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
	                </table>';

        $kirim = false;
        if ($to != '') {
            $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
        }

        return $kirim;
    }

    function reminder_pulang_awal($data = '')
    {
        $subject = '[Aplikasi HRD] Pengajuan Pulang Awal';
        $nip = isset($data['nip']) ? $data['nip'] : '';
        $status = isset($data['status']) ? $data['status'] : '';
        $tgl = isset($data['tgl']) ? $data['tgl'] : '';
        $jam = isset($data['jam']) ? $data['jam'] : '';
        $alasan = isset($data['alasan']) ?  $data['alasan'] : '';
        $penyetuju = isset($data['penyetuju']) ? $data['penyetuju'] : '';
        $cc = isset($data['cc']) ? $this->Main_Model->pecah_cc($data['cc']) : '';

        // data karyawan
        $karyawan = $this->Main_Model->kary_nip($nip);
        $nama = isset($karyawan->nama) ? $karyawan->nama : '';
        $from = isset($karyawan->mail) ? $karyawan->mail : '';

        // data penyetuju
        $wtf = $this->Main_Model->kary_nip($penyetuju);
        $kepada = isset($wtf->nama) ? $wtf->nama : '';
        $to = isset($wtf->mail) ? $wtf->mail : '';

        $url = base_url();
        $tahun = date('Y');

        $message = '<table>
	                    <tr><td colspan="3">Kepada Yth, <br /><b>'.$kepada.'</b></td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Dengan ini, saya ingin mengajukan permohonan Pulang Awal dengan detail sbb:</td></tr>
	                    <tr><td><b>Nama</b></td><td> : </td><td>'.$nama.'</td></tr>
	                    <tr><td><b>NIK</b></td><td> : </td><td>'.$nip.'</td></tr>
	                    <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
	                    <tr><td><b>Jam</b></td><td> : </td><td>'.jam($jam).'</td></tr>
	                    <tr><td><b>Keperluan</b></td><td> : </td><td>'.$alasan.'</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Demikian yang dapat saya sampaikan, atas perhatiannya saya sampaikan terima kasih.</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Klik Link ini untuk <a href="'.$url.'">Menyetujui atau Menolak</a> Permohonan Pulang Awal</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
	                </table>';

        $kirim = false;
        if ($to != '') {
            $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
        }

        return $kirim;
    }

    // pulang awal
    function view_pulang_awal($tgl_awal = '', $tgl_akhir = '', $id = '')
    {
        $nip = nip_api();
        $nip = $this->db->escape_str($nip);
        $tgl_awal = $this->db->escape_str($tgl_awal);
        $tgl_akhir = $this->db->escape_str($tgl_akhir);
        $id = $this->db->escape_str($id);

        $condition = '';
        if ($id != '') {
            $condition .= " AND a.id_pulang_awal = '$id' ";
        }

        return $this->db->query("
				SELECT a.`id_pulang_awal`, a.`nip`, b.`nama`, 
				a.`jam`, g.`keterangan`, a.tgl,
                a.`alasan`, a.`file_bukti`, g.`flag`, a.`status`, f.`cabang`
                FROM tb_pulang_awal a 
                INNER JOIN kary b ON a.`nip` = b.`nip`
                INNER JOIN sk c ON c.`nip` = b.`nip`
                INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
                INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
                INNER JOIN ms_status_approval g ON g.`kode` = a.`status`
                WHERE c.`aktif` = '1'
                AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                AND ISNULL(b.`tgl_resign`)
                AND a.nip = '$nip'
                $condition 
                GROUP BY a.`id_pulang_awal`")->result();
    }

    // ijin keluar kantor
    function view_keluar_kantor($tgl_awal = '', $tgl_akhir = '', $id = '')
    {
        $nip = nip_api();
        $nip = $this->db->escape_str($nip);
        $tgl_awal = $this->db->escape_str($tgl_awal);
        $tgl_akhir = $this->db->escape_str($tgl_akhir);
        $id = $this->db->escape_str($id);

        $condition = '';
        if ($id != '') {
            $condition .= " AND a.id = '$id' ";
        }

        return $this->db->query("		
				SELECT a.`id`, a.`nip`, b.`nama`, f.`cabang`, 
               	a.`jmljam`, a.`keterangan`, a.tgl,
                g.`keterangan` AS app_status, g.`kode`, g.`flag`, e.`jab`, f.`cabang`,
                a.dari, a.sampai
                FROM ijinjam a 
                INNER JOIN kary b ON a.`nip` = b.`nip`
                INNER JOIN sk c ON c.`nip` = b.`nip`
                INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
                INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
                INNER JOIN ms_status_approval g ON g.`kode` = a.`status`
                WHERE c.`aktif` = '1' 
                AND ISNULL(b.`tgl_resign`)
                AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
                AND a.nip = '$nip'
                $condition
                GROUP BY a.id
                ORDER BY a.`tgl` DESC")->result();
    }

    // rak sido kanggo
    function rekap_absensi($nip = '', $startdate = '', $enddate = '')
    {
        return $this->db->query("
			SELECT a.`nip`, a.`nama`,
			IFNULL(telat.jumlah, 0) AS jumlah_terlambat,
			IFNULL(telat.total_menit, 0) AS jumlah_menit_terlambat,
			IFNULL(absen.jumlah, 0) AS jumlah_pulang_awal,
			IFNULL(masuk.jumlah, 0) AS tidak_absen_masuk,
			IFNULL(pulang.jumlah, 0) AS tidak_absen_pulang,
			DATE_FORMAT('$startdate', '%d-%m-%Y') AS startdate,
			DATE_FORMAT('$enddate', '%d-%m-%Y') AS enddate
			FROM kary a
			LEFT JOIN (
				SELECT terlambat_temp.`nip`, COUNT(*) AS jumlah,
				SUM(terlambat_temp.`total_menit`) AS total_menit 
				FROM terlambat_temp
				WHERE terlambat_temp.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND terlambat_temp.`nip` = '$nip'
				GROUP BY terlambat_temp.`id`
			) AS telat ON telat.nip = a.`nip`
			LEFT JOIN (
				SELECT a.`nip`, a.`scan_pulang`, 
				CONCAT(a.`tgl`,' ', c.`jam_pulang`) AS jam_pulang, 
				COUNT(*) AS jumlah
				FROM tb_absensi a
				INNER JOIN tr_shift b ON a.`nip` = b.`nip` AND a.`tgl` = b.`tgl`
				INNER JOIN ms_shift c ON b.`id_shift` = c.`id_shift`
				WHERE a.`nip` = '$nip'
				AND a.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND CONCAT(a.`tgl`,' ', c.`jam_pulang`) > a.`scan_pulang`
				AND c.`jam_masuk` <> '' 
				AND c.`jam_pulang` <> ''
				AND a.`scan_pulang` <> ''
			) AS absen ON absen.nip = a.`nip`
			LEFT JOIN (
				SELECT a.`nip`, COUNT(*) AS jumlah
				FROM tb_absensi a
				WHERE a.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND a.`nip` = '$nip'
				AND a.`jam_masuk` <> ''
				AND a.`scan_masuk` = ''
			)AS masuk ON masuk.nip = a.`nip`
			LEFT JOIN (
				SELECT a.`nip`, COUNT(*) AS jumlah
				FROM tb_absensi a
				WHERE a.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND a.`nip` = '$nip'
				AND a.`jam_masuk` <> ''
				AND a.`scan_pulang` = ''
			) AS pulang ON pulang.nip = a.`nip`
			WHERE a.`nip` = '$nip'")->row();
    }

    // reminder batal cuti
    function reminder_batal_cuti($id_cuti = '')
    {
        $subject = '[Aplikasi HRD] Pembatalan Cuti';
        $data = $this->Cuti_Model->view_cuti_id($id_cuti);
        $nama = isset($data->nama) ? $data->nama : '';
        $nip = isset($data->nip) ? $data->nip : '';
        $mulai = isset($data->mulai) ? $data->mulai : '';
        $selesai = isset($data->selesai) ? $data->selesai : '';
        $keterangan = isset($data->alasan_cuti) ? $data->alasan_cuti : '';
        $sisa_cuti = sisa_cuti($nip);
        $from = isset($data->mail) ? $data->mail : '';
        $penyetuju = isset($data->penyetuju) ? $data->penyetuju : '';
        $cc = isset($data->cc) ? $this->Main_Model->pecah_cc($data->cc) : '';
        $tahun = date('Y');

        /// data penyetuju
        $query = $this->Main_Model->kary_nip($penyetuju);
        $kepada = isset($query->nama) ? $query->nama : '';
        $to = isset($query->mail) ? $query->mail : '';

        $url = base_url();
        $tahun = date('Y');
        $message  = '<table>
	                    <tr><td colspan="3">Kepada Yth, <br /><b>'.$kepada.'</b></td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Dengan ini, saya membatalkan permohonan Cuti saya dengan detail sbb:</td></tr>
	                    <tr><td><b>Nama</b></td><td> : </td><td>'.$nama.'</td></tr>
	                    <tr><td><b>NIK</b></td><td> : </td><td>'.$nip.'</td></tr>
	                    <tr><td><b>Mulai Cuti</b></td><td> : </td><td>'.tanggal($mulai).'</td></tr>
	                    <tr><td><b>Sampai Dengan</b></td><td> : </td><td>'.tanggal($selesai).'</td></tr>
	                    <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>
	                    <tr><td><b>Sisa Cuti</b></td><td> : </td><td>'.$sisa_cuti.' Hari</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Regards, '.$nama.'</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
	                </table>';

        $kirim = false;
        if ($to != '') {
            $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);

            // cari fcm id
            $fcm = $this->Main_Model->cari_fcm_id($penyetuju);
            $usr_id = isset($fcm->usr_id) ? $fcm->usr_id : '';
            $fcm_id = isset($fcm->fcm_id) ? $fcm->fcm_id : '';

            // send notification
            $title_notif = 'Pembatalan Cuti';
            $text_notif = $nama.' telah membatalkan cuti.';
            $this->Rest_Model->notif_fcm($title_notif, $text_notif, $fcm_id, $usr_id);
        }

        return $kirim;
    }

    // reminder batal pulang awal
    function reminder_batal_pulang($id = '')
    {
        $subject = '[Aplikasi HRD] Pembatalan Pulang Awal';
        $data = $this->Absensi_Model->id_pulang_awal($id);
        $nama = isset($data->nama) ? $data->nama : '';
        $nip = isset($data->nip) ? $data->nip : '';
        $tgl = isset($data->tgl) ? $data->tgl : '';
        $jam = isset($data->jam_pulang) ? $data->jam_pulang : '';
        $alasan = isset($data->alasan) ? $data->alasan : '';
        $from = isset($data->mail) ? $data->mail : '';
        $penyetuju = isset($data->penyetuju) ? $data->penyetuju : '';
        $cc = isset($data->cc) ? $this->Main_Model->pecah_cc($data->cc) : '';
        $tahun = date('Y');

        /// data penyetuju
        $query = $this->Main_Model->kary_nip($penyetuju);
        $kepada = isset($query->nama) ? $query->nama : '';
        $to = isset($query->mail) ? $query->mail : '';

        $url = base_url();
        $tahun = date('Y');
        $message = '<table>
	                    <tr><td colspan="3">Kepada Yth, <br /><b>'.$kepada.'</b></td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Dengan ini, saya ingin membatalkan permohonan Pulang Awal dengan detail sbb:</td></tr>
	                    <tr><td><b>Nama</b></td><td> : </td><td>'.$nama.'</td></tr>
	                    <tr><td><b>NIK</b></td><td> : </td><td>'.$nip.'</td></tr>
	                    <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
	                    <tr><td><b>Jam</b></td><td> : </td><td>'.$jam.'</td></tr>
	                    <tr><td><b>Keperluan</b></td><td> : </td><td>'.$alasan.'</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3">Demikian yang dapat saya sampaikan, atas perhatiannya saya sampaikan terima kasih.</td></tr>
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    
	                    <tr><td colspan="3">&nbsp;</td></tr>
	                    <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
	                </table>';

        $kirim = false;
        if ($to != '') {
            $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);

            // cari fcm id
            $fcm = $this->Main_Model->cari_fcm_id($penyetuju);
            $usr_id = isset($fcm->usr_id) ? $fcm->usr_id : '';
            $fcm_id = isset($fcm->fcm_id) ? $fcm->fcm_id : '';

            // send notification
            $title_notif = 'Pembatalan Pulang Awal';
            $text_notif = $nama.' telah membatalkan Pulang Awal.';
            $this->Rest_Model->notif_fcm($title_notif, $text_notif, $fcm_id, $usr_id);
        }

        return $kirim;
    }

    // reminder batal keluar kantor
    function reminder_batal_keluar($id = '')
    {
        $subject = '[Aplikasi HRD] Pembatalan Pulang Awal';
        $data = $this->Absensi_Model->ijinjam_id($id);
        $nip = isset($data->nip) ? $data->nip : '';
        $nama = isset($data->nama) ? $data->nama : '';
        $tgl = isset($data->tgl) ? $data->tgl : '';
        $jmlmenit = isset($data->jmljam) ? $data->jmljam : '';
        $keterangan = isset($data->keterangan) ? $data->keterangan : '';
        $from = isset($data->mail) ? $data->mail : '';
        $penyetuju = isset($data->penyetuju) ? $data->penyetuju : '';
        $cc = isset($data->cc) ? $this->Main_Model->pecah_cc($data->cc) : '';

        /// data penyetuju
        $query = $this->Main_Model->kary_nip($penyetuju);
        $kepada = isset($query->nama) ? $query->nama : '';
        $to = isset($query->mail) ? $query->mail : '';

        $url = base_url();
        $tahun = date('Y');
        $message  = '<table>
		                <tr><td colspan="3">Kepada Yth, <br /><b>'.$kepada.'</b></td></tr>
		                <tr><td colspan="3">&nbsp;</td></tr>
		                <tr><td colspan="3">Dengan ini, saya ingin membatalkan permohonan Keluar Kantor dengan detail sbb:</td></tr>
		                <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
		                <tr><td><b>Durasi</b></td><td> : </td><td>'.$jmlmenit.' menit</td></tr>
		                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>
		                
		                <tr><td colspan="3">&nbsp;</td></tr>
		                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
		                <tr><td colspan="3">&nbsp;</td></tr>
		                
		                <tr><td colspan="3">&nbsp;</td></tr>
		                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
		            </table>';

        $kirim = false;
        if ($to != '') {
            $kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);

            // cari fcm id
            $fcm = $this->Main_Model->cari_fcm_id($penyetuju);
            $usr_id = isset($fcm->usr_id) ? $fcm->usr_id : '';
            $fcm_id = isset($fcm->fcm_id) ? $fcm->fcm_id : '';

            // send notification
            $title_notif = 'Pembatalan Keluar Kantor';
            $text_notif = $nama.' telah membatalkan keluar kantor.';
            $this->Rest_Model->notif_fcm($title_notif, $text_notif, $fcm_id, $usr_id);
        }

        return $kirim;
    }

    function check_version()
    {
        return $this->db->get('application_version')
                ->row();
    }

    function check_imei($nip = '', $imei = '')
    {
        $arr_imei = explode(',', $imei);
        $hitung = 0;
        if (! empty($arr_imei)) {
            for ($i = 0; $i < count($arr_imei); $i++) {
                $imei_string = isset($arr_imei[$i]) ? $arr_imei[$i] : '';
                $imei_string = str_replace('[', '', $imei_string);
                $imei_string = str_replace(']', '', $imei_string);
                $imei_string = str_replace(' ', '', $imei_string);
                
                $data = array(
                    'nip' => $nip,
                    'imei' => $imei_string
                );

                $cek = $this->Main_Model->view_by_id('tb_imei', $data, 'row');
                if (! empty($cek)) {
                    $hitung = $hitung + 1;
                }
            }
        }

        if ($hitung > 0) {
            return true;
        } else {
            return false;
        }
    }

    function view_scanlog($date = '')
    {
        $nip = $this->input->get_request_header('Nip', true);
        $date = $this->db->escape_str($date);
        $date = api_tgl($date);

        return $this->db->query("
				SELECT a.`nip`, b.`nama`, DATE(a.`scan_date`) AS tanggal, 
				DATE_FORMAT(a.`scan_date`, '%H:%i') AS jam,
				(CASE 
					WHEN (a.flag = 1) 
						THEN 'Check Out'
					ELSE 'Check In'
				END) AS type    
				FROM tb_scanlog a
				INNER JOIN kary b ON a.`nip` = b.`nip`
				WHERE DATE(a.`scan_date`) = '$date'
				AND a.`nip` = '$nip'")->result();
    }

    function notif_fcm($title = '', $text = '', $to = '', $id_user = '', $click_action = 'ACT_NOTIF', $extends_data = [], $extends_value = [])
    {
        ini_set('memory_limit', '-1');
        ini_set('MAX_EXECUTION_TIME', '-1');
        set_time_limit(0);

        $url = '';

        // set authentication
        if ($id_user != '') {
            $randomstring = $this->Main_Model->generateRandomString(8);
            $url = base_url('rmd').'/'.$randomstring;
            $now = date('Y-m-d');
            $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

            $authenticantion = array(
                'usr_id' => $id_user,
                'token' => $randomstring,
                'expiration' => $expiration
            );
            $this->Main_Model->process_data('tb_authentication', $authenticantion);
        }
        
        // begin send notification
        if ($to != '') {
            $url = fcm_url();
            $key = fcm_key();

            $list_header[] = "Content-Type:application/json";
            $list_header[] = "Authorization:key=".$key;

            $parameter['notification']['title'] = $title;
            $parameter['notification']['text'] = $text;
            $parameter['notification']['click_action'] = $click_action;
            $parameter['notification']['sound'] = 'content://settings/system/notification_sound';
            $parameter['to'] = $to;
            $parameter['priority'] = 'high';

            if (! empty($extends_data)) {
                for ($i = 0; $i < count($extends_data); $i++) {
                    $parameter['data'][$extends_data[$i]] = $extends_value[$i];
                }
            }

            $send_notif = $this->Main_Model->curl_notif($url, $parameter, $list_header);
        }
    }

    // begin query approval
    // approval cuti list
    function view_app_cuti()
    {
        $nip = $this->input->get_request_header('Nip', true);
        return $this->db->query("
			SELECT a.`id_cuti_det` AS id, b.`nip`, b.`nama`, a.`alasan_cuti`, 
			IFNULL(DATE_FORMAT(cuti_awal.tgl, '%d-%m-%Y'), '') AS tgl_awal,
			IFNULL(DATE_FORMAT(cuti_akhir.tgl, '%d-%m-%Y'), '') AS tgl_akhir,
			c.`keterangan`
			FROM cuti_det a
			INNER JOIN kary b ON a.`nip` = b.`nip`
			INNER JOIN ms_status_approval c ON a.`approval` = c.`kode`
			LEFT JOIN (
				SELECT MIN(tgl) AS tgl, id_cuti_det
				FROM cuti_sub_det 
				GROUP BY id_cuti_det
			) AS cuti_awal ON cuti_awal.id_cuti_det = a.`id_cuti_det`
			LEFT JOIN (
				SELECT MAX(tgl) AS tgl, id_cuti_det
				FROM cuti_sub_det
				GROUP BY id_cuti_det
			) AS cuti_akhir ON cuti_akhir.id_cuti_det = a.`id_cuti_det`
			WHERE a.`penyetuju` = '$nip'
			AND c.`flag` = 1")->result();
    }

    // approval pulang awal list
    function view_app_pulang()
    {
        $nip = $this->input->get_request_header('Nip', true);
        return $this->db->query("
			SELECT a.`id_pulang_awal` AS id, c.`nip`, c.`nama`, a.`alasan`,
			DATE_FORMAT(a.`tgl`, '%d-%m-%Y') AS tgl, DATE_FORMAT(a.`jam`, '%H:%i') AS jam, b.`keterangan`
			FROM tb_pulang_awal a
			INNER JOIN ms_status_approval b ON a.`status` = b.`kode`
			INNER JOIN kary c ON c.`nip` = a.`nip`
			WHERE a.`penyetuju` = '$nip'
			AND b.`flag` = 1")->result();
    }

    // approval keluar kantor list
    function view_app_keluar()
    {
        $nip = $this->input->get_request_header('Nip', true);
        return $this->db->query("
			SELECT a.`id`, a.`nip`, b.`nama`, a.`keterangan` AS alasan, DATE_FORMAT(a.`tgl`, '%d-%m-%Y') AS tgl, 
			DATE_FORMAT(a.`dari`, '%H:%i') AS dari, DATE_FORMAT(a.`sampai`, '%H:%i') AS sampai, c.`keterangan`
			FROM ijinjam a
			INNER JOIN kary b ON a.`nip` = b.`nip`
			INNER JOIN ms_status_approval c ON c.`kode` = a.`status`
			WHERE a.`penyetuju` = '$nip'
			AND c.`flag` = 1")->result();
    }

    // changelog
    function view_changelog()
    {
        return $this->db->order_by('tgl', 'desc')
                ->get('tb_changelog')->result();
    }

    function periode($bulan = '', $tahun = '')
    {
        return $this->db->query("
			SELECT *
			FROM q_det a
			WHERE a.`bln` = '$bulan'
			AND YEAR(a.`tgl_akhir`) = '$tahun'")->row_array();
    }

    function detail_gaji($nip = '', $key = '')
    {   
        $default_key = default_key();
        $tb_key = $this->db->query("
            SELECT *
            FROM tb_key a 
            WHERE a.nik = '$nip'")->row();
        $default = isset($tb_key->default) ? $tb_key->default : 0; 

        if ($default == 1) {
            $def = $this->db->query("
                SELECT AES_DECRYPT(a.n2, '$default_key') AS n2,
                AES_DECRYPT(a.d2, '$default_key') AS d2
                FROM tb_key a 
                WHERE a.nik = '$nip' ")->row();

                $n2 = isset($def->n2) ? $def->n2 : '';
                $d2 = isset($def->d2) ? $def->d2 : '';

            $this->db->query("
                UPDATE tb_key 
                SET n2 = AES_ENCRYPT('$n2', '$key'),
                    d2 = AES_ENCRYPT('$d2', '$key'),
                    update_at = NOW(),
                    user_update = '$nip',
                    `default` = 0
                WHERE nik = '$nip' ");
        }

        $query = $this->db->query("
            SELECT AES_DECRYPT(a.n2, '$key') AS n,
            AES_DECRYPT(a.d2, '$key') AS d, a.value,
            a.nik, b.nama
            FROM kary b 
            LEFT JOIN tb_key a ON a.nik = b.nip
            WHERE b.nip = '$nip' ")->row();

        $val = isset($query->value) ? $query->value : '';
        $d = isset($query->d) ? $query->d : '';
        $n = isset($query->n) ? $query->n : '';
        $nama = isset($query->nama) ? $query->nama : '';
        $value = 0;

        if ($val != '' && $val > 0 && $d != '' && $n != '') {
            $value = $this->rsaencrypt->decrypt($d, $n, $val);
        }

        return [
            'value' => $value, 
            'bpjs' => [
                'potongan' => [
                    ['item' => 'Jaminan Hari Tua', 'value' => ($value * 2.00/100)],
                    ['item' => 'Jaminan Pensiun', 'value' => ($value * 1.00/100)],
                    ['item' => 'Total', 'value' => (($value * 2.00/100) + ($value * 1.00/100))]
                ],
                'tunjangan' => [
                    ['item' => 'Jaminan Kecelakaan Kerja', 'value' => ($value * 0.24/100)],
                    ['item' => 'Jaminan Kematian', 'value' => ($value * 0.30/100)],
                    ['item' => 'Jaminan Hari Tua', 'value' => ($value * 3.70/100)],
                    ['item' => 'Jaminan Pensiun', 'value' => ($value * 2.00/100)],
                    ['item' => 'Total', 'value' => (($value * 0.24/100) + ($value * 0.30/100) + ($value * 3.70/100) + ($value * 2.00/100))]
                ]
            ],
            'nama' => $nama
        ];
    }

    function create_pin($nip = '', $pin = '')
    {
        $default_key = default_key();
        $this->db->trans_begin();
        $query = $this->db->query("
            SELECT AES_DECRYPT(a.n2, '$default_key') AS n2,
            AES_DECRYPT(a.d2, '$default_key') AS d2
            FROM tb_key a 
            WHERE a.nik = '$nip' ")->row();

        $n = isset($query->n2) ? $query->n2 : '';
        $d = isset($query->d2) ? $query->d2 : '';


        $this->db->query("
            UPDATE tb_key 
            SET n2 = AES_ENCRYPT('$n', '$pin'),
                d2 = AES_ENCRYPT('$d', '$pin'),
                update_at = NOW(),
                user_update = '$nip',
                `default` = 0
            WHERE nik = '$nip' ");

        $this->db->insert('tb_master_key', [
                                'user' => $nip, 
                                'key' => md5($pin.'&gmediapayroll&secretkey'),
                                'user_insert' => $nip,
                                'flag' => 1
                            ]);


        if ($this->db->trans_status() == true) {
            $this->db->trans_commit();

            return true;
        } else {
            $this->db->trans_rollback();

            return false;
        }   
    }

    function update_pin($pin_lama = '', $pin_baru = '', $nip = '')
    {
        $this->db->trans_begin();
        $query = $this->db->query("
            SELECT AES_DECRYPT(a.n2, '$pin_lama') AS n2,
            AES_DECRYPT(a.d2, '$pin_lama') AS d2
            FROM tb_key a 
            WHERE a.nik = '$nip' ")->row();

        $n = isset($query->n2) ? $query->n2 : '';
        $d = isset($query->d2) ? $query->d2 : '';

        $this->db->query("
            UPDATE tb_key 
            SET n2 = AES_ENCRYPT('$n', '$pin_baru'),
                d2 = AES_ENCRYPT('$d', '$pin_baru'),
                update_at = NOW(),
                user_update = '$nip',
                `default` = 0
            WHERE nik = '$nip' ");

        $this->db->where('user', $nip)
                ->update('tb_master_key', [
                                'key' => md5($pin_baru.'&gmediapayroll&secretkey'),
                                'user_update' => $nip,
                                'update_at' => now(),
                                'flag' => 1
                            ]);


        if ($this->db->trans_status() == true) {
            $this->db->trans_commit();

            return true;
        } else {
            $this->db->trans_rollback();

            return false;
        }   
    }

    function flag_pin($nip = '')
    {
        $pin = $this->db->where(['user' => $nip, 'flag' => 1])
                            ->get('tb_master_key')
                            ->row();
        $pin_gaji = ($pin) ? 1 : 0;

        return $pin_gaji;
    }
}

/* End of file rest_model.php */
/* Location: ./application/models/rest_model.php */
