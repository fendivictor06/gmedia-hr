<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reminder_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Rest_Model', '', TRUE);
	}

	function view_reminder() 
	{
		return $this->db->query("
				SELECT a.*, b.nama, b.fcm_id
				FROM ms_reminder a 
				INNER JOIN kary b ON a.nip = b.nip")->result();
	}

	function id_cabang($username='') 
	{
		$cabang =  $this->db->query("
			SELECT * FROM tb_user_cabang a
			WHERE a.`usr_name` = '$username'")->result();

		$sess_cabang = array();
        if($cabang) {
            foreach($cabang as $row) {
                $sess_cabang[] = $row->id_cabang;
            }
        }

        return $sess_cabang;
	}

	function approval($username='')
	{
		$class = $this->db->where('usr_name', $username)->get('tb_level_approval')->result();
        $sess_class = array();
        if($class) {
            foreach($class as $row) {
                $sess_class[] = $row->sal_pos;
            }
        }

        return $sess_class;
	}

	function divisi($username='')
	{
		$divisi = $this->db->where('usr_name', $username)->get('tb_user_divisi')->result();
        $sess_divisi = array();
        if($divisi) {
            foreach($divisi as $row) {
                $sess_divisi[] = $row->id_divisi;
            }
        }

        return $sess_divisi;
	}

	function reminder_cuti($kode='', $id_cuti='')
	{
		$status = $this->Main_Model->view_by_id('ms_status_approval', array('kode' => $kode), 'row');
		$flag = isset($status->flag) ? $status->flag : '';

		$data = $this->Cuti_Model->view_cuti_id($id_cuti);
		$nama = isset($data->nama) ? $data->nama : '';
		$nip = isset($data->nip) ? $data->nip : '';
		$mulai = isset($data->mulai) ? $data->mulai : '';
		$selesai = isset($data->selesai) ? $data->selesai : '';
		$keterangan = isset($data->alasan_cuti) ? $data->alasan_cuti : '';
		$penyetuju = isset($data->penyetuju) ? $data->penyetuju : '';
		$cc = isset($data->cc) ? $this->Main_Model->pecah_cc($data->cc) : '';
		$sisa_cuti = sisa_cuti($nip);
		$to = isset($data->mail) ? $data->mail : '';
		$fcm_id = isset($data->fcm_id) ? $data->fcm_id : '';
		$tahun = date('Y');

		// data penyetuju 
		$wtf = $this->Main_Model->kary_nip($penyetuju);
		$from = isset($wtf->mail) ? $wtf->mail : '';
 
		if ($kode == 12) {
			$subject = '[Aplikasi HRD] Persetujuan Cuti';
			// $hrd = 'hrd.smg@gmedia.co.id';
			$message  = '<table>
                <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama.'</b><br /><b>'.$nip.'</b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Dengan ini, saya setujui permohonan Cuti anda dengan detail sbb:</td></tr>
                <tr><td><b>Mulai Cuti</b></td><td> : </td><td>'.tanggal($mulai).'</td></tr>
                <tr><td><b>Sampai Dengan</b></td><td> : </td><td>'.tanggal($selesai).'</td></tr>
                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>
                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
            </table>';
           	if ($to != '') {
		    	$kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
		    	$title = 'Pengajuan Cuti';
		    	$text = 'Pengajuan cuti anda telah disetujui.';
		    	$this->Rest_Model->notif_fcm($title, $text, $fcm_id);
		    }
		} else if($kode == 13) {
			$subject = '[Aplikasi HRD] Penolakan Cuti';
			// $hrd = 'hrd.smg@gmedia.co.id';
			$message  = '<table>
                <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama.'</b><br /><b>'.$nip.'</b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Dengan ini, permohonan Cuti tidak dapat kami setujui dengan detail sbb:</td></tr>
                <tr><td><b>Mulai Cuti</b></td><td> : </td><td>'.tanggal($mulai).'</td></tr>
                <tr><td><b>Sampai Dengan</b></td><td> : </td><td>'.tanggal($selesai).'</td></tr>
                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>

                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>

                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
            </table>';
            if ($to != '') {
		    	$kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
		    	$title = 'Pengajuan Cuti';
		    	$text = 'Pengajuan cuti anda telah ditolak.';
		    	$this->Rest_Model->notif_fcm($title, $text, $fcm_id);
		    }
		} else {
			$kirim = FALSE;
		}
	}

	function reminder_pulang_awal($kode='', $id_pulang_awal='')
	{
		$status = $this->Main_Model->view_by_id('ms_status_approval', array('kode' => $kode), 'row');
		$flag = isset($status->flag) ? $status->flag : '';

		$data = $this->Absensi_Model->id_pulang_awal($id_pulang_awal);
		$nama = isset($data->nama) ? $data->nama : '';
		$nip = isset($data->nip) ? $data->nip : '';
		$tgl = isset($data->tgl) ? $data->tgl : '';
		$jam = isset($data->jam_pulang) ? $data->jam_pulang : '';
		$alasan = isset($data->alasan) ? $data->alasan : '';
		$to = isset($data->mail) ? $data->mail : '';
		$penyetuju = isset($data->penyetuju) ? $data->penyetuju : '';
		$cc = isset($data->cc) ? $this->Main_Model->pecah_cc($data->cc) : '';
		$fcm_id = isset($data->fcm_id) ? $data->fcm_id : '';
		$tahun = date('Y');

		// data penyetuju 
		$wtf = $this->Main_Model->kary_nip($penyetuju);
		$from = isset($wtf->mail) ? $wtf->mail : '';

		if ($kode == 12) {
			$subject = '[Aplikasi HRD] Persetujuan Pulang Awal';
			// $hrd = 'hrd.smg@gmedia.co.id';
			$message  = '<table>
                <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama.'</b><br /><b>'.$nip.'</b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Dengan ini, saya setujui permohonan Pulang Awal anda dengan detail sbb:</td></tr>
                <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
                <tr><td><b>Jam</b></td><td> : </td><td>'.jam($jam).'</td></tr>
                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$alasan.'</td></tr>
                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
            </table>';
            if ($to != '') {
		    	$kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
		    	$title = 'Pengajuan Pulang Awal';
		    	$text = 'Pengajuan Pulang Awal anda telah disetujui.';
		    	$this->Rest_Model->notif_fcm($title, $text, $fcm_id);
		    }
		} else if($kode == 13) {
			$subject = '[Aplikasi HRD] Penolakan Pulang Awal';
			// $hrd = 'hrd.smg@gmedia.co.id';
			$message  = '<table>
                <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama.'</b><br /><b>'.$nip.'</b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Dengan ini, saya tolak permohonan Pulang Awal anda dengan detail sbb:</td></tr>
                <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
                <tr><td><b>Jam</b></td><td> : </td><td>'.jam($jam).'</td></tr>
                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$alasan.'</td></tr>

                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>

                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
            </table>';
            if ($to != '') {
		    	$kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
		    	$title = 'Pengajuan Pulang Awal';
		    	$text = 'Pengajuan Pulang Awal anda telah ditolak.';
		    	$this->Rest_Model->notif_fcm($title, $text, $fcm_id);
		    }
		} else {
			$kirim = FALSE;
		}
	}

	function reminder_keluar_kantor($kode='', $id='')
	{
		$status = $this->Main_Model->view_by_id('ms_status_approval', array('kode' => $kode), 'row');
		$flag = isset($status->flag) ? $status->flag : '';

		$data = $this->Absensi_Model->ijinjam_id($id);
		$nip = isset($data->nip) ? $data->nip : '';
		$nama = isset($data->nama) ? $data->nama : '';
		$tgl = isset($data->tgl) ? $data->tgl : '';
		$jmlmenit = isset($data->jmljam) ? $data->jmljam : '';
		$keterangan = isset($data->keterangan) ? $data->keterangan : '';
		$to = isset($data->mail) ? $data->mail : '';
		$penyetuju = isset($data->penyetuju) ? $data->penyetuju : '';
		$cc = isset($data->cc) ? $this->Main_Model->pecah_cc($data->cc) : '';
		$fcm_id = isset($data->fcm_id) ? $data->fcm_id : '';
		$dari = isset($data->dari) ? jam($data->dari) : '';
		$sampai = isset($data->sampai) ? jam($data->sampai) : '';
		$tahun = date('Y');

		// data penyetuju 
		$wtf = $this->Main_Model->kary_nip($penyetuju);
		$from = isset($wtf->mail) ? $wtf->mail : '';

		if ($kode == 12) {
			$subject = '[Aplikasi HRD] Persetujuan Keluar Kantor';
			// $hrd = 'hrd.smg@gmedia.co.id';
			$message  = '<table>
                <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama.'</b><br /><b>'.$nip.'</b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Dengan ini, saya setujui permohonan Keluar Kantor anda dengan detail sbb:</td></tr>
                <tr><td><b>Tanggal</b></td><td> : </td><td>'.$tgl.'</td></tr>
                <tr><td><b>Dari</b></td><td> : </td><td>'.$dari.'</td></tr>
                <tr><td><b>Sampai</b></td><td> : </td><td>'.$sampai.'</td></tr>
                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>
                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
            </table>';
            if ($to != '') {
		    	$kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
		    	$title = 'Pengajuan Keluar Kantor';
		    	$text = 'Pengajuan Keluar Kantor anda telah disetujui.';
		    	$this->Rest_Model->notif_fcm($title, $text, $fcm_id);
		    }
		} else if($kode == 13) {
			$subject = '[Aplikasi HRD] Penolakan Keluar Kantor';
			// $hrd = 'hrd.smg@gmedia.co.id';
			$message  = '<table>
                <tr><td colspan="3">Kepada Yth, <br /><b>'.$nama.'</b><br /><b>'.$nip.'</b></td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Dengan ini, saya tolak permohonan Keluar Kantor anda dengan detail sbb:</td></tr>
                <tr><td><b>Tanggal</b></td><td> : </td><td>'.tanggal($tgl).'</td></tr>
                <tr><td><b>Dari</b></td><td> : </td><td>'.$dari.'</td></tr>
                <tr><td><b>Sampai</b></td><td> : </td><td>'.$sampai.'</td></tr>
                <tr><td><b>Keperluan</b></td><td> : </td><td>'.$keterangan.'</td></tr>

                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">Demikian yang dapat kami sampaikan, atas perhatiannya kami sampaikan terima kasih.</td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>

                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3"><font size="1" face="arial">copyright &copy; '.$tahun.' PT Media Sarana Data all rights reserved, Powered by IT Team</font></td></tr>
            </table>';
            if ($to != '') {
		    	$kirim = $this->Main_Model->kirim_email($to, $cc, $message, $subject, $from);
		    	$title = 'Pengajuan Keluar Kantor';
		    	$text = 'Pengajuan Keluar Kantor anda telah ditolak.';
		    	$this->Rest_Model->notif_fcm($title, $text, $fcm_id);
		    }
		} else {
			$kirim = FALSE;
		}
	}

	function reminder_kontrak_habis()
	{
		return $this->db->query("
			SELECT a.*, DATE_ADD(a.`tgl_akhir`, INTERVAL -3 MONTH) AS tgl_notif,
			b.`nama`, e.`nama` AS penyetuju, e.`mail` AS email_penyetuju, e.`fcm_id` AS fcm_penyetuju,
			e.nip AS nip_penyetuju, d.cc
			FROM kont a
			INNER JOIN kary b ON a.`nip` = b.`nip`
			INNER JOIN ms_rule_detail c ON c.`nip` = a.`nip`
			INNER JOIN ms_rule_approval d ON d.`pola` = c.`pola`
			INNER JOIN kary e ON e.`nip` = d.`penyetuju`
			WHERE a.`tgl_akhir` BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL +3 MONTH)
			AND NOT EXISTS (
				SELECT NULL
				FROM log_reminder_kontrak
				WHERE nip = a.`nip`
				AND id_kontrak = a.`id_kontrak`
			)
			AND a.`tipe` = 'KONTRAK'
			AND a.`aktif` = 1
			AND c.`modul` = 'cuti'
			AND e.`tgl_resign` IS NULL")->result();
	}
}

/* End of file reminder_model.php */
/* Location: ./application/models/reminder_model.php */ ?>