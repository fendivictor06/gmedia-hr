<?php

class Informasi_Model extends CI_Model {

    function __construct(){
        parent::__construct();

    }

    function view_kont()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT a.`nip`,a.`nama`,a.`kary_stat`,b.`no_kontrak`,b.`tgl_akhir` FROM kary a JOIN kont b ON a.`nip` = b.`nip` WHERE ISNULL(a.`tgl_resign`) AND a.`kary_stat`='KONTRAK' AND b.`tgl_akhir`< NOW()  AND b.`aktif`=1 AND a.`idp`='$idp'")->result();
    }

    function view_kont_not_exists()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT * FROM kary a WHERE NOT EXISTS (SELECT c.`nip` FROM kont c WHERE c.`nip`=a.`nip`) AND ISNULL(a.`tgl_resign`)AND a.`kary_stat`='KONTRAK' AND a.`idp` = '$idp'")->result();
    }

    function view_kontrak_nip($nip)
    {
    	return $this->db->query("SELECT * FROM kont a LEFT JOIN kary b ON a.`nip`=b.`nip` WHERE a.`nip`='$nip' ORDER BY a.`no_kontrak` DESC")->result();
    }

    function add_kont($data)
    {
    	$this->db->insert('kont',$data);
    }

    function edit_aktif($edit,$nip)
    {
    	$this->db->where('nip',$nip)->update('kont',$edit);
    }

    function kontrak_id($id)
    {
    	return $this->db->where('id_kontrak',$id)->from('kont')->get()->row();
    }

    function update_kont($data,$id)
    {
    	$this->db->where('id_kontrak',$id)->update('kont',$data);
    }

    function delete_kont($id)
    {
    	$this->db->where('id_kontrak',$id)->delete('kont');
    }

    function view_rekrut()
    {
        return $this->db->query("
            SELECT a.id, a.tgl, a.jenis, b.nama, a.tgl_kontrak, a.tgl_akhir, 
            a.jumlah, a.keterangan 
            FROM rekrut_train a 
            LEFT JOIN kary b ON a.nip = b.nip
            ORDER BY a.`id` DESC ")->result();
    }

    function add_rekrut($data)
    {
        $this->db->insert('rekrut_train',$data);

        return $this->db->insert_id();
    }

    function biaya_rekrut_id($id)
    {
        return $this->db->query("
            SELECT a.`id`, a.`jenis`, a.`jumlah`, DATE_FORMAT(a.`tgl`,'%d/%m/%Y') AS tgl,
            a.nip, DATE_FORMAT(a.tgl_kontrak, '%d/%m/%Y') AS tgl_kontrak, 
            DATE_FORMAT(a.tgl_akhir, '%d/%m/%Y') AS tgl_akhir, a.durasi, a.keterangan
            FROM rekrut_train a 
            WHERE a.`id` = '$id' ")->row();
    }

    function update_rekrut($data, $id)
    {
        $this->db->trans_begin();
        $this->db->where('id',$id)->update('rekrut_train',$data);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();

            return true;
        } else {
            $this->db->trans_rollback();

            return false;
        }
    }

    function delete_rekrut($id)
    {
        $this->db->where('id',$id)->delete('rekrut_train');
    }

    function ubah_status($data,$nip)
    {
        $this->db->where('nip',$nip)->update('kary',$data);
    }

    function view_refreshment($idp)
    {
        return $this->db->where('idp',$idp)->get('tb_refreshment')->result();
    }

    function refreshment_process($data,$id)
    {
        if($id)
        {
            $this->db->where('id',$id)->update('tb_refreshment',$data);
        }
        else
        {
            $this->db->insert('tb_refreshment',$data);
        }
    }

    function delete_refreshment($id)
    {
        $this->db->where('id',$id)->delete('tb_refreshment');
    }

    function refreshment_id($id)
    {
        return $this->db->query("SELECT *, DATE_FORMAT(tanggal, '%d/%m/%Y') tgl FROM (`tb_refreshment`) WHERE `id` = '$id'")->row();
    }
}
?>