 <?php if (! defined('BASEPATH')) {
     exit('No direct script access allowed');
 }

class Shift_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function karyawan($mode = '', $nip_array = array(), $tgl_awal = '', $tgl_selesai = '')
    {
        $condition = '';
        $acctype = $this->session->userdata('acctype');
        if ($mode == 2 || $mode == 3) {
            if (!empty($nip_array)) {
                     $condition = ' AND (';
                for ($i = 0; $i < count($nip_array); $i++) {
                    $condition .= " d.nip = '$nip_array[$i]' ";
                    (end($nip_array) != $nip_array[$i]) ? $condition .= " OR " : $condition .= "";
                }
                     $condition .= ') AND';
            }
        } else {
            $condition = ' AND ';
        }

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang) && $mode != '3') {
             $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                    $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                     $cabang .= ')';
                }
            }
        }

             $sal_pos = $this->Main_Model->session_pengajuan();
             $kelas = '';
        if (!empty($sal_pos) && $mode != '3') {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                       $kelas .= " c.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

             $divisi = $this->Main_Model->session_divisi();
             $div = '';
        if (!empty($divisi) && $mode != '3') {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                  $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                         $div .= ')';
                }
            }
        }

                $query = $this->db->query("
            SELECT d.`nip`, d.`nama`, e.id, f.nama AS nama_shift,
            f.jam_masuk, f.jam_pulang, e.tgl, f.keterangan,
            (CASE 
                WHEN (libur.hol_tgl IS NOT NULL) 
                    THEN '1'
                WHEN (DAYOFWEEK(e.`tgl`) = 1 OR DAYOFWEEK(e.`tgl`) = 7)
                    THEN '1'
                    ELSE '0'
            END) AS flag_libur
            FROM sk a 
            INNER JOIN pos_sto b ON a.`id_pos_sto` = b.`id_sto` 
            INNER JOIN pos c ON b.`id_pos` = c.`id_pos` 
            INNER JOIN kary d ON a.`nip` = d.`nip` 
            INNER JOIN tr_shift e ON e.nip = d.nip
            INNER JOIN ms_shift f ON f.id_shift = e.id_shift
            LEFT JOIN (
                SELECT *
                FROM hol
            ) AS libur ON libur.hol_tgl = e.`tgl`
            WHERE a.`aktif` = '1' 
            $cabang $kelas $div 
            $condition 
			d.tgl_masuk <= '$tgl_selesai' 
			AND (ISNULL(d.tgl_resign) 
			OR d.tgl_resign BETWEEN '$tgl_awal' AND '$tgl_selesai')
            AND e.tgl BETWEEN '$tgl_awal' AND '$tgl_selesai'
            ORDER BY d.`nama` ASC, e.tgl ASC")->result();

        $array = array();
        $index = 0;
        $arr_nip = array();
        if (!empty($query)) {
            foreach ($query as $row) {
                if (in_array($row->nip, $arr_nip)) {
                    $array[$index][$row->tgl] = array(
                    'id' => $row->id,
                    'shift' => $row->nama_shift,
                    'jam_masuk' => $row->jam_masuk,
                    'jam_pulang' => $row->jam_pulang,
                    'keterangan' => $row->keterangan,
                    'flag_libur' => $row->flag_libur
                    );
                } else {
                    $index++;
                    $array[$index] = array(
                    'nip' => $row->nip,
                    'nama' => $row->nama,
                    $row->tgl => array(
                    'id' => $row->id,
                    'shift' => $row->nama_shift,
                    'jam_masuk' => $row->jam_masuk,
                    'jam_pulang' => $row->jam_pulang,
                    'keterangan' => $row->keterangan,
                    'flag_libur' => $row->flag_libur
                    )
                    );
                }
                   $arr_nip[] = $row->nip;
            }
        }

        return $array;
    }

    function jadwal($tgl = '', $nip = '')
    {
          return $this->db->query("
					SELECT a.id, b.nama, b.jam_masuk, b.jam_pulang 
					FROM tr_shift a
					INNER JOIN ms_shift b ON a.id_shift = b.id_shift
					WHERE a.tgl = '$tgl'
					AND a.nip = '$nip'")->row();
    }

    function nip_ts($tgl_awal = '', $tgl_akhir = '', $nip_header = '')
    {
          $id_cabang = $this->Main_Model->session_cabang();

          $cabang = '';
        if ($nip_header == '' && ! empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                  $cabang .= " h.`id_cabang` = '$id_cabang[$i]'";

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        } else {
            $karyawan = $this->db->where('nip', $nip_header)
                    ->get('karyawan')
                    ->row();
            $cb = isset($karyawan->id_cabang) ? $karyawan->id_cabang : '';
            if ($cb != '') {
                  $cabang = " AND h.id_cabang = '$cb'";
            }
        }

          $query =  $this->db->query("
            SELECT f.`nip`, f.`nama`, a.`tgl`, 
            DATE_FORMAT(g.`jam_masuk`, '%H:%i') AS jam_masuk, 
            DATE_FORMAT(g.`jam_pulang`, '%H:%i') AS jam_pulang,  
            g.color, g.`nama` AS nama_shift, g.`keterangan`, 
            h.color AS warna_nip
            FROM tr_shift a
            INNER JOIN kary f ON f.`nip` = a.`nip`
            INNER JOIN ms_shift g ON g.`id_shift` = a.`id_shift`
            INNER JOIN tb_jadwal_color h ON h.nip = a.nip
            WHERE a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
            AND f.tgl_resign IS NULL
            $cabang
            ORDER BY h.group, f.nip ASC, a.tgl ASC")->result();

          $array = array();
          $index = 0;
          $arr_nip = array();
        if (!empty($query)) {
            foreach ($query as $row) {
                if (in_array($row->nip, $arr_nip)) {
                    $array[$index][$row->tgl] = array(
                     'shift' => $row->nama_shift,
                     'jam_masuk' => $row->jam_masuk,
                     'jam_pulang' => $row->jam_pulang,
                     'keterangan' => $row->keterangan,
                     'color' => isset($row->color) ? $row->color : 'silver'
                    );
                } else {
                    $index++;
                    $array[$index] = array(
                    'nip' => $row->nip,
                    'nama' => $row->nama,
                    'warna' => $row->warna_nip,
                    $row->tgl => array(
                    'shift' => $row->nama_shift,
                    'jam_masuk' => $row->jam_masuk,
                    'jam_pulang' => $row->jam_pulang,
                    'keterangan' => $row->keterangan,
                    'color' => isset($row->color) ? $row->color : 'silver'
                       )
                    );
                }
                  $arr_nip[] = $row->nip;
            }
        }

          return $array;
    }

    function master_shift()
    {
          return $this->db->query("
            SELECT a.`nama`, DATE_FORMAT(a.`jam_masuk`, '%H:%i') AS jam_masuk,
            DATE_FORMAT(a.`jam_pulang`, '%H:%i') AS jam_pulang, a.`keterangan`,
            a.color
            FROM ms_shift a
            WHERE a.`status` = 1
            AND a.color <> ''")->result();
    }
}

/* End of file shift_model.php */
/* Location: ./application/models/shift_model.php */
