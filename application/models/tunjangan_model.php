<?php

class Tunjangan_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function periode_tgl($periode)
    {
        return $this->db->query("SELECT * FROM q_det a WHERE a.`qd_id` = '$periode'")->row();
    }

    function view_bbp()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("SELECT * FROM kary_bbmp a JOIN kary b ON a.`nip`=b.`nip` JOIN lku c ON a.`id_lku`=c.`id_lku` WHERE b.`idp`='$idp' ORDER BY a.`bbm_start` DESC")->result();
    }

    function bbp_id($id)
    {
        return $this->db->query("SELECT *,DATE_FORMAT(a.`bbm_start`,'%d/%m/%Y') tgl_start FROM kary_bbmp a WHERE a.`id`='$id'")->row();
    }

    function ms_lembur($holiday='0')
    {
        return $this->db->where('isholiday',$holiday)->get('ms_lembur')->row();
    }

    function delete_bbp($id)
    {
        $this->db->where('id',$id)->delete('kary_bbmp');
    }

    function view_lembur($tgl_awal='', $tgl_akhir='', $nip='', $id_cabang='')
    {
        $idp = $this->session->userdata('idp');
        // $q = $this->periode_tgl($periode);
        // $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
        // $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';
        // if(!empty($id_cabang)) {
            $cabang = '';
            if(!empty($id_cabang)) {
                $cabang .= 'AND (';
                for($i = 0; $i < count($id_cabang); $i++) {
                    $cabang .= ' e.`id_cab` = '.$id_cabang[$i];

                    if(end($id_cabang) != $id_cabang[$i]) {
                        $cabang .= ' OR';
                    } else {
                        $cabang .= ')';
                    }
                }
            }

            $sal_pos = $this->Main_Model->session_pengajuan();
            $kelas = '';
            if(!empty($sal_pos)) {
                $kelas .= ' AND (';
                for($i = 0; $i < count($sal_pos); $i++) {
                    $kelas .= " d.`sal_pos` = '$sal_pos[$i]'";

                    if(end($sal_pos) != $sal_pos[$i]) {
                        $kelas .= ' OR';
                    } else {
                        $kelas .= ')';
                    }
                }
            }

            $divisi = $this->Main_Model->session_divisi();
            $div = '';
            if(!empty($divisi)) {
                $div .= ' AND (';
                for($i = 0; $i < count($divisi); $i++) {
                    $div .= " d.`id_divisi` = '$divisi[$i]'";

                    if(end($divisi) != $divisi[$i]) {
                        $div .= ' OR';
                    } else {
                        $div .= ')';
                    }
                }
            }

            $condition = '';
            if ($nip != '') {
                $condition = " AND a.nip = '$nip' ";
            }

            return $this->db->query("
                    SELECT f.`id_lembur`, a.`nip`, a.`nama`, e.`cabang`, 
                    CONCAT(DATE_FORMAT(f.`starttime`, '%H:%i'),' - ',DATE_FORMAT(f.`endtime`, '%H:%i')) AS jam, f.`lembur`, f.`uangmakan`, f.`status`, f.`tgl`,
                    f.starttime, f.endtime, f.file, f.keterangan, g.name AS jenis
                    FROM kary a 
                    JOIN sk b ON a.`nip` = b.`nip`
                    JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
                    JOIN pos d ON d.`id_pos` = c.`id_pos`
                    JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
                    JOIN lembur f ON f.`nip` = a.`nip`
                    JOIN ms_overtime g ON g.id = f.id_overtime
                    WHERE f.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                    AND b.`aktif` = '1' 
                    $cabang $kelas $div 
                    $condition
                    ORDER BY f.`tgl` DESC")->result();
        // } else {
        //     return $this->db->query("SELECT * FROM lembur a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`tgl` BETWEEN '$q->tgl_awal' AND '$q->tgl_akhir'")->result();
        // }
    }

    function add_lembur($data)
    {
        $this->db->insert('lembur',$data);
    }

    function add_lembur_by_nip($data,$id_per,$nip)
    {
        $this->db->where('nip',$nip)->where('id_per',$id_per)->update('ab',$data);
    }

    function lembur_id($id)
    {
        return $this->db->query("
            SELECT a.`id_lembur`, a.`nip`,DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tgl, a.`jml_jam`, a.`lembur`, a.`uangmakan`, 
                DATE_FORMAT(a.`starttime`,'%H:%i') starttime,DATE_FORMAT(a.`endtime`,'%H:%i') endtime, a.`islibur`, a.id_overtime,
                a.keterangan,  DATE_FORMAT(a.`starttime`,'%H') AS starttime_hour, DATE_FORMAT(a.`starttime`,'%i') AS starttime_minute,
                DATE_FORMAT(a.`endtime`,'%H') AS endtime_hour, DATE_FORMAT(a.`endtime`,'%i') AS endtime_minute
                FROM lembur a 
                WHERE a.`id_lembur`='$id'")->row();
    }

    function update_lembur($data,$id_lembur)
    {
        $this->db->where('id_lembur',$id_lembur)->update('lembur',$data);
    }

    function delete_lembur($id)
    {
        $this->db->where('id_lembur',$id)->delete('lembur');
    }

    function process_bbmp($data,$id)
    {
        if(empty($id))
        {
            $this->db->insert('kary_bbmp',$data);
        }
        else
        {
            $this->db->where('id',$id)->update('kary_bbmp',$data); 
        }
    }

    function view_lainnya($periode)
    {
        $idp    = $this->session->userdata('idp');
        $q      = $this->periode_tgl($periode);

        return $this->db->query("SELECT * FROM tunj_lainnya a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`tgl` BETWEEN '$q->tgl_awal' AND '$q->tgl_akhir'")->result();
    }

    function process_lainnya($data,$id)
    {
        if($id=="")
        {
            $this->db->insert('tunj_lainnya',$data);
        }
        else
        {
            $this->db->where('id_t_lain',$id)->update('tunj_lainnya',$data);
        }
    }

    function delete_lainnya($id)
    {
        $this->db->where('id_t_lain',$id)->delete('tunj_lainnya');
    }

    function lainnya_id($id)
    {
        return $this->db->query("SELECT *,DATE_FORMAT(a.`tgl`,'%d/%m/%Y') tanggal FROM tunj_lainnya a WHERE a.`id_t_lain`='$id'")->row();
    }

    function view_approval_lembur()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        } 

        return $this->db->query("SELECT a.*,e.`nama`
            ,(SELECT MAX(tb_absensi.`scan_pulang`) FROM tb_absensi WHERE DATE_FORMAT(tb_absensi.`tgl`,'%Y-%m-%d') = a.`tgl` AND tb_absensi.`nip` = a.`nip`) scan_pulang
            FROM lembur a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON b.`id_pos_sto` = c.`id_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN kary e ON a.`nip` = e.`nip`
            WHERE a.`status` = '0' AND b.`aktif` = '1' 
            AND a.`tgl` + INTERVAL 1 DAY <= CURDATE()
            $cabang $kelas $div")->result();
    }

    function view_overtime() 
    {
        return $this->db->where('status', 1)
                ->get('ms_overtime')->result();
    }

    function opt_overtime()
    {
        $q = $this->view_overtime();
        $arr = array();
        if (!empty($q)) {
            foreach($q as $row) {
                $arr[$row->id] = $row->name.' => '.$row->keterangan;
            }
        }
        return $arr;
    }

    function id_overtime($id='')
    {
        return $this->db->where('id', $id)
                ->get('ms_overtime')
                ->row();
    }

    function hitung_jam($start='', $end='')
    {
        $result = 0;
        if ($start != '' && $end != '') {
            $query = $this->db->query("
                    SELECT 
                    TIMESTAMPDIFF(HOUR, '$start', '$end') AS jumlah")->row();

            $result = isset($query->jumlah) ? $query->jumlah : 0;
        } 

        if ($result > 5) {
            $result = 5;
        }

        return $result;
    }

    function view_laporan_lembur_divisi($cabang = '', $divisi = '', $date_start = '', $date_end = '')
    {
        $condition_divisi = '';
        if ($divisi != 0) {
            $condition_divisi = " AND a.id_divisi = '$divisi' ";
        }

        $ms_divisi = $this->db->query("
                SELECT *
                FROM ms_divisi a 
                WHERE a.status = 1 
                $condition_divisi ")->result();

        $table = '  <table class="table table-striped table-hover table-bordered" id="tb_rekap">';
        $table .= '     <thead>
                            <tr>
                                <th>#</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Cabang</th>
                                <th>Divisi</th>
                                <th>Tanggal</th>
                                <th>Shift</th>
                                <th>Absensi</th>
                                <th>Jam</th>
                                <th>Per Jam</th>
                                <th>Total</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>';

        if ($ms_divisi) {
            foreach ($ms_divisi as $row) {
                $table .= ' <tr>
                                <td colspan="12"><h5 style="font-weight: bold;">'.$row->divisi.'</h5></td>
                            </tr>';

                $lembur = $this->lemburan_divisi($cabang, $row->id_divisi, $date_start, $date_end);
                if ($lembur) {
                    $i = 1;
                    foreach ($lembur as $val) {
                        $ovr_nominal = isset($val->ovr_nominal) ? $val->ovr_nominal : 0;
                        $id_overtime = isset($val->id_overtime) ? $val->id_overtime : 0;

                        if ($id_overtime == 0) {
                            $ovr_nominal = 0;
                        }

                        $hari = hari(date('w', strtotime($val->tgl)), 2);
                        $table .= ' <tr>
                                        <td>'.$i++.'</td>
                                        <td>'.$val->nip.'</td>
                                        <td>'.$val->nama.'</td>
                                        <td>'.$val->cabang.'</td>
                                        <td>'.$val->divisi.'</td>
                                        <td>'.$hari.', '.tanggal($val->tgl).'</td>
                                        <td>'.$val->shift.'</td>
                                        <td>'.$val->absensi.'</td>
                                        <td>'.$val->jam.'</td>
                                        <td>'.uang($ovr_nominal).'</td>
                                        <td>'.uang($val->lembur).'</td>
                                        <td>'.$val->keterangan.'</td>
                                    </tr>';
                    }
                }
            }
        }

        $table .= '     </tbody>
                    </table>';

        return $table;
    }

    function lemburan_divisi($cabang = '', $divisi = '', $date_start = '', $date_end = '')
    {
        $condition = '';
        if ($cabang != 0) {
            $condition .= " AND f.`id_cab` = '$cabang' ";
        }

        $q = $this->db->query("
                SELECT a.*, b.`nama`, f.`cabang`, g.`divisi`, 
                CONCAT(DATE_FORMAT(a.starttime, '%H:%i'),' ',DATE_FORMAT(a.endtime, '%H:%i')) AS shift,
                CONCAT(absen.scan_masuk,' ',absen.scan_pulang) AS absensi,
                a.jml_jam AS jam, h.nominal AS ovr_nominal, h.id AS id_overtime
                FROM lembur a
                INNER JOIN kary b ON a.`nip` = b.nip
                INNER JOIN sk c ON c.`nip` = b.`nip`
                INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
                INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
                INNER JOIN ms_divisi g ON g.`id_divisi` = e.`id_divisi`
                INNER JOIN ms_overtime h ON h.id = a.id_overtime
                LEFT JOIN (
                    SELECT nip, tgl,
                    DATE_FORMAT(scan_masuk, '%H:%i') AS scan_masuk,
                    DATE_FORMAT(scan_pulang, '%H:%i') AS scan_pulang  
                    FROM tb_absensi 
                    WHERE tgl BETWEEN '$date_start' AND '$date_end'
                    GROUP BY nip, tgl
                ) AS absen ON absen.nip = a.`nip` AND a.tgl = absen.tgl
                WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
                $condition
                AND c.`aktif` = 1
                AND g.id_divisi = '$divisi'
                AND a.status = 1 
                ORDER BY a.nip ASC")->result();

        return $q;
    }
}
?>