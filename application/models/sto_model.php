<?php

class Sto_Model extends CI_Model {
    function __construct(){
        parent::__construct();

    }

    function idp(){
    	$option = $this->db->from('idp')->get()->result();
    	foreach ($option as $row) {
    		$result[$row->idp] = $row->bu;
    	}
    	return $result;
    }

    function view_sto($idp='1'){
    	return $this->db->query("
            SELECT * 
            FROM pos_sto a 
            JOIN pos g ON a.`id_pos`=g.`id_pos` 
            left join ms_departemen h on g.`id_departemen`=h.`id_dept` 
            left join ms_cabang i on g.`id_cabang`=i.`id_cab` 
            left join ms_divisi j on g.`id_divisi`=j.`id_divisi` 
            WHERE g.`idp`='$idp' 
            AND a.`status` = 1 
            ORDER BY g.`jab` ASC")->result();
    }

    function sto_id($id){
        return $this->db->query("SELECT * FROM pos_sto a JOIN pos b ON a.`id_pos`=b.`id_pos` WHERE a.`id_sto`='$id'")->row();
    }

    function lku(){
        $idp = $this->session->userdata('idp');
    	$option = $this->db->query("select * from lku a join lwok b on a.id_lwok = b.id_lwok where b.idp = '$idp'")->result();
    	foreach ($option as $row) {
    		$result[$row->id_lku] = $row->lku;
    	}
    	return $result;
    }

    function lka(){
        $idp = $this->session->userdata('idp');
    	$option = $this->db->query("select * from lka a join lku b on a.id_lku=b.id_lku join lwok c on b.id_lwok=c.id_lwok where c.idp='$idp'")->result();
    	foreach ($option as $row) {
    		$result[$row->id_lka] = $row->lka;
    	}
    	return $result;
    }

    function view_posisi($idp){
        return $this->db->where('idp',$idp)->from('pos')->order_by('posisi','asc')->get()->result();
    }

    function add_sto($data){
        $this->db->insert('pos_sto',$data);
    }

    function update_sto($data,$id_sto){
        $this->db->where('id_sto',$id_sto)->update('pos_sto',$data);
    }

    function delete_sto($id){
        $this->db->where('id_sto',$id)->delete('pos_sto');
    }

    function sto_rel_opt()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->query("SELECT * FROM sto_rel a JOIN pos_sto b ON a.`id_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE c.`idp`='$idp' ORDER BY a.`id_rel` ASC")->result();

        if($opt){

            foreach ($opt as $row) {
                $q = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
                $tgl_resign = isset($q->tgl_resign)?$q->tgl_resign:'';
                if($tgl_resign=='')
                {
                    $nama = isset($q->nama)?$q->nama:'';
                }
                else
                {
                    $nama = '';
                }

                $result[$row->id_sto] = $row->id_stob_pos. ' - ' .$row->jab.' - '.$nama;
            }

            $result[0] = 'No Parent'; 
        }
        else
        {
           $result[0] = 'No Parent'; 
        }

        return $result;
    }

    function sto_rel_tbl()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->query("SELECT * FROM sto_rel a JOIN pos_sto b ON a.`id_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE c.`idp`='$idp' ORDER BY a.`id_rel` ASC")->result();
        $result ='<table class="table table-striped table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Posisi</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>';
        $i = 1;
        // if($opt){

            foreach ($opt as $row) {
                $q = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
                $tgl_resign = isset($q->tgl_resign)?$q->tgl_resign:'';
                if($tgl_resign=='')
                {
                    $nama = isset($q->nama)?$q->nama:'-';
                }
                else
                {
                    $nama = '-';
                }

                // $result .= $row->posisi.' - '.$nama;
                $result .= '<tr>
                                <td>'.$i++.'</td>
                                <td>'.$row->jab.'</td>
                                <td>'.$nama.'</td>
                                <td><a href="javascript:;" onclick="delete_data('.$row->id_sto.')">Delete</a></td>
                            </tr>';
            }
        // }
        // else
        // {
        //    $result .= ''; 
        // }
        $result .= '</tbody></table>';

        return $result;
    }

    function sto_opt()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->query("SELECT c.`posisi`,a.`id_sto`,a.`id_stob_pos`,c.`jab` FROM pos_sto a JOIN pos c ON a.`id_pos`=c.`id_pos` WHERE NOT EXISTS(SELECT b.`id_sto` FROM sto_rel b WHERE a.`id_sto`=b.`id_sto`) AND c.`idp`='$idp' ORDER BY c.`posisi` ASC")->result();

        if($opt)
        {
            foreach ($opt as $row) {

                $q = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
                $tgl_resign = isset($q->tgl_resign)?$q->tgl_resign:'';
                if($tgl_resign=='')
                {
                    $nama = isset($q->nama)?$q->nama:'';
                }
                else
                {
                    $nama = '';
                }

                $result[$row->id_sto] = $row->id_stob_pos. ' - ' .$row->jab.' - '.$nama;
            }

            return $result;
        }
        else
        {
            return array();
        }
    }

    function add_struktur($data){
        $this->db->insert('sto_rel',$data);
    }

    function sto_rel(){
//         return $this->db->query("SELECT a.`id_rel`,c.`posisi` pos_sto,a.`id_sto`,e.`posisi` pos_parent,a.`id_parent` FROM sto_rel a
// JOIN pos_sto b ON a.`id_sto`=b.`id_sto`
// JOIN pos c ON b.`id_pos`=c.`id_pos`
// JOIN pos_sto d ON a.`id_parent`=d.`id_sto`
// JOIN pos e ON d.`id_pos`=e.`id_pos`")->result();
        return $this->db->query("SELECT * FROM sto_rel a")->result();
    }

    function sto_rel_id($id){
        return $this->db->query("SELECT * FROM sto_rel a WHERE a.`id_parent`='$id'")->result();
    }

    function delete_struktur($id){
        $this->db->where('id_sto',$id)->delete('sto_rel');
        $this->db->where('id_parent',$id)->delete('sto_rel');
    }

    // function drawMenu($listOfItems){
    //     $rel_sto = $this->sto_rel_id($listOfItems);
    //     $menu ="<ul>";
    //     foreach ($rel_sto as $row) {
    //         $menu .="<li>".$row->id_parent;
    //         // $q = $this->db->query("select * from sto_rel where id_parent = '$row->id_sto'")->row();
    //         if($row->id_sto){
    //             $this->drawMenu($row->id_sto);
    //         }
    //         $menu .="</li>";
    //     }
    //     $menu .="</ul>";
    //     return $menu;
    // }

    function view_posisi_jabatan($idp='1')
    {
        return $this->db->query("       
            SELECT g.`id_pos`, IFNULL(jab.nip, '-') AS nip, IFNULL(jab.nama, '-') AS nama, 
            h.`departemen`, i.`cabang`,
            j.`divisi`, g.`sal_pos`, g.`jab`
            FROM pos_sto a 
            JOIN pos g ON a.`id_pos` = g.`id_pos` 
            LEFT JOIN ms_departemen h ON g.`id_departemen` = h.`id_dept` 
            LEFT JOIN ms_cabang i ON g.`id_cabang` = i.`id_cab` 
            LEFT JOIN ms_divisi j ON g.`id_divisi` = j.`id_divisi` 
            LEFT JOIN (
                SELECT sk.`id_pos_sto`, kary.`nip`, kary.nama 
                FROM kary 
                INNER JOIN sk ON sk.`nip` = kary.`nip`
                WHERE sk.`aktif` = 1
            ) AS jab ON jab.id_pos_sto = a.`id_sto`
            WHERE g.`idp` = '$idp' 
            AND a.`status` = 1 
            AND g.status = 1
            ORDER BY g.`jab` ASC")->result();
    }
}
?>