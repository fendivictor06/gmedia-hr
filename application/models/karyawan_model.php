<?php

class Karyawan_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function lka()
    {
        $idp = $this->session->userdata('idp');
    	return $this->db->query("SELECT * FROM lka a JOIN lku b ON a.`id_lku`=b.`id_lku` JOIN lwok c ON b.`id_lwok`=c.`id_lwok` WHERE c.`idp`='$idp' ORDER BY a.`id_lka` ASC");
    }

    function lwok()
    {
        $idp = $this->session->userdata('idp');
    	return $this->db->query("SELECT * FROM lwok WHERE idp='$idp' ORDER BY id_lwok ASC");
    }

    function tipe_sk($id='')
    {
        ($id) ? $condition = 'WHERE id_tipe_sk = 5' : $condition = '';

        return $this->db->query("SELECT * FROM ms_tipe_sk $condition")->result();
    }

    function old_sk($nip='')
    {
        return $this->db->query("SELECT c.`jab`,a.`id_sk`,b.`id_stob_pos` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE a.`nip` = '$nip' AND a.`aktif`='1'")->row();
    }

    function tipe_sk_opt($id='')
    {
        $opt    = $this->tipe_sk($id);
        foreach ($opt as $row) {
            $result[$row->id_tipe_sk] = $row->tipe;
        }
        return $result;
    }

    function lku()
    {
        $idp = $this->session->userdata('idp');
    	return $this->db->query("SELECT * FROM lku a JOIN lwok b ON a.`id_lwok`=b.`id_lwok` WHERE b.`idp`='$idp' ORDER BY a.`id_lku` ASC");
    }

    function lokasi($idp='')
    {
        $data = $this->db->query("SELECT * FROM lwok a JOIN lku b ON a.`id_lwok`=b.`id_lwok` JOIN lka c ON b.`id_lku`=c.`id_lku` WHERE a.`idp`='$idp'")->result();
        if($data)
        {
            foreach ($data as $row) {
                $result[$row->id_lka] = $row->lka;
            }

            return $result;
        }
    }

    function search_karyawan($cabang=0)
    {
        $idp = $this->session->userdata('idp');
        $condition = '';
        if ($cabang > 0) {
            $condition = " AND surat.id_cabang = '$cabang' ";
        }

        $active = $this->db->query("
            SELECT a.*, surat.jab, surat.cabang, surat.divisi, surat.departemen,
            b.pend AS pendidikan, surat.sal_pos
            FROM kary a
            INNER JOIN pend b ON a.id_pend = b.id_pend
            LEFT JOIN (
                SELECT MAX(id_sk), ms_cabang.cabang, sk.nip, pos.jab, 
                pos.id_cabang, ms_divisi.divisi, ms_departemen.departemen,
                pos.sal_pos
                FROM sk
                INNER JOIN pos_sto ON sk.id_pos_sto = pos_sto.id_sto
                INNER JOIN pos ON pos.id_pos = pos_sto.id_pos
                INNER JOIN ms_cabang ON ms_cabang.id_cab = pos.id_cabang
                INNER JOIN ms_divisi ON ms_divisi.id_divisi = pos.id_divisi
                INNER JOIN ms_departemen ON ms_departemen.id_dept = pos.id_departemen
                GROUP BY sk.nip
            ) AS surat ON surat.nip = a.nip
            WHERE a.tgl_resign IS NULL 
            AND a.idp = '$idp'
            $condition
            ORDER BY a.nip ASC ")->result();

        $resign = $this->db->query("
            SELECT a.*, surat.jab, surat.cabang, surat.divisi, surat.departemen,
            b.pend AS pendidikan, surat.sal_pos
            FROM kary a
            INNER JOIN pend b ON a.id_pend = b.id_pend
            LEFT JOIN (
                SELECT MAX(id_sk), ms_cabang.cabang, sk.nip, pos.jab, 
                pos.id_cabang, ms_divisi.divisi, ms_departemen.departemen,
                pos.sal_pos
                FROM sk
                INNER JOIN pos_sto ON sk.id_pos_sto = pos_sto.id_sto
                INNER JOIN pos ON pos.id_pos = pos_sto.id_pos
                INNER JOIN ms_cabang ON ms_cabang.id_cab = pos.id_cabang
                INNER JOIN ms_divisi ON ms_divisi.id_divisi = pos.id_divisi
                INNER JOIN ms_departemen ON ms_departemen.id_dept = pos.id_departemen
                GROUP BY sk.nip
            ) AS surat ON surat.nip = a.nip
            WHERE a.tgl_resign IS NOT NULL 
            AND a.idp = '$idp'
            $condition
            ORDER BY a.nip ASC ")->result();

        $mutasi = $this->db->query("
            SELECT a.*, surat.jab, surat.cabang, surat.divisi, surat.departemen,
            b.pend AS pendidikan, surat.sal_pos
            FROM kary a
            INNER JOIN pend b ON a.id_pend = b.id_pend
            LEFT JOIN (
                SELECT MAX(id_sk), ms_cabang.cabang, sk.nip, pos.jab, 
                pos.id_cabang, ms_divisi.divisi, ms_departemen.departemen,
                sk.id_tipe_sk, pos.sal_pos
                FROM sk
                INNER JOIN pos_sto ON sk.id_pos_sto = pos_sto.id_sto
                INNER JOIN pos ON pos.id_pos = pos_sto.id_pos
                INNER JOIN ms_cabang ON ms_cabang.id_cab = pos.id_cabang
                INNER JOIN ms_divisi ON ms_divisi.id_divisi = pos.id_divisi
                INNER JOIN ms_departemen ON ms_departemen.id_dept = pos.id_departemen
                GROUP BY sk.nip
            ) AS surat ON surat.nip = a.nip
            WHERE a.tgl_resign IS NULL 
            AND a.idp = '$idp'
            AND (surat.`id_tipe_sk` = '1' 
                OR surat.`id_tipe_sk` = '4' 
                OR surat.`id_tipe_sk` = '6'
            ) 
            $condition
            ORDER BY a.nip ASC ")->result();

        return $result = array(
            'active' => $active, 
            'resign' => $resign, 
            'mutasi' => $mutasi
        );
    }

    function lku_id($id_lku)
    {
        return $this->db->where('id_lku', $id_lku)->from('lku')->get()->row();
    }

    function posisi($nip)
    {
        // return $this->db->where('sk.nip',$nip)->where('sk.aktif',1)->from('sk')->join('pos_sto','pos_sto.id_sto=sk.id_pos_sto')->join('pos','pos.id_pos=pos_sto.id_pos')->join('lka','pos_sto.id_lka=lka.id_lka')->get()->row();
        return $this->db->query("SELECT c.`jab`,d.`cabang` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_cabang d ON c.`id_cabang`=d.`id_cab` WHERE a.`aktif`='1' AND a.`nip`='$nip'")->row();
    }

    function pendidikan()
    {
        $this->db->from('pend');
        $db = $this->db->get()->result();

        foreach ($db as $row) {
            $result[$row->id_pend] = $row->pend;
        }

        return $result;
    }

    function lokasi_kerja()
    {
        $this->db->from('lku');
        $db = $this->db->get()->result();

        foreach ($db as $row) {
            $result[$row->id_lku] = $row->lku;
        }

        return $result;
    }

    function lku_idp($idp)
    {
        return $this->db->query("SELECT * FROM idp a JOIN lku b ON a.`idp`=b.`idp` WHERE a.`idp` = '$idp'")->result();
    }

    function ceknip($kd)
    {
        return $this->db->query("SELECT MAX(RIGHT(nip,4))AS nip FROM kary WHERE LEFT(nip,2)='$kd'");
    }

    function add_karyawan($data)
    {
        $this->db->insert('kary',$data);
    }

    function dataposisi($id)
    {
        // return $this->db->where('idp',$id)->order_by('posisi','asc')->from('pos')->get()->result();
        return $this->db->query("
            SELECT a.*, 
                   b.`departemen`, 
                   c.`cabang`, 
                   d.`divisi` 
            FROM   pos a 
                   LEFT JOIN ms_departemen b 
                          ON a.`id_departemen` = b.`id_dept` 
                   LEFT JOIN ms_cabang c 
                          ON a.`id_cabang` = c.`id_cab` 
                   LEFT JOIN ms_divisi d 
                          ON a.`id_divisi` = d.`id_divisi` 
            WHERE  a.`idp` = '$id' 
            AND a.status = 1
            AND NOT EXISTS (
                SELECT NULL
                FROM pos_sto
                WHERE id_pos = a.`id_pos`
                AND `status` = 1
            )
            ORDER  BY a.`jab` ASC ")->result();
    }

    function view_salpos($id,$idp)
    {
        if($id=="STAFF")
        {
            $query = $this->db->query("select distinct sal_pos from sal_staff_class where idp='$idp'");
        }
        elseif($id=="NON-STAFF")
        {
            $query = $this->db->query("select distinct sal_pos from sal_nonstaff_class where idp='$idp'");
        }
        return $query;
    }

    function add_posisi($data='')
    {
        $cabang = isset($data['id_cabang']) ? $data['id_cabang'] : '';
        $r = $this->Main_Model->view_by_id('ms_cabang', array('id_cab' => $cabang), 'row');
        $id_lku = isset($r->id_lku) ? $r->id_lku : '';

        if ($cabang != '' && $id_lku != '') {
            $this->db->insert('pos', $data);
            $insert_id = $this->db->insert_id();

            $ins = array(
                'id_pos' => $insert_id,
                'id_stob_pos' => '',
                'id_lku' => $id_lku,
                'id_lka' => 0
            );

            $this->db->insert('pos_sto', $ins);
        }
    }

    function view_posisi_id($id='')
    {
        // return $this->db->where('id_pos',$id)->from('pos')->get()->row();
        return $this->db->query("
            SELECT a.*,b.`departemen`,c.`cabang`,d.`divisi` 
            FROM pos a 
            LEFT JOIN ms_departemen b ON a.`id_departemen` = b.`id_dept` 
            LEFT JOIN ms_cabang c ON a.`id_cabang` = c.`id_cab` 
            LEFT JOIN ms_divisi d ON a.`id_divisi` = d.`id_divisi` 
            WHERE a.`id_pos` = '$id'")->row();
    }

    function update_posisi($data,$id_pos)
    {
        $this->db->where('id_pos',$id_pos)->update('pos',$data);
    }

    function delete_posisi($id_pos='')
    {
        $this->db->where('id_pos',$id_pos)->delete('pos');
        $this->db->where('id_pos',$id_pos)->delete('pos_sto');
    }

    function get_id_karyawan($nip='')
    {
        return $this->db->query("
                SELECT a.`alasan_bpjsks`, a.`golongan_darah`, a.`pin`, a.`nip`, a.`nama`, a.`ktp`, a.`t_lahir`, DATE_FORMAT(a.`tgl_lahir`,'%d/%m/%Y') AS tgl_lahir, a.`gend`, a.`telp`, a.`mar_stat`, a.`mail`, a.`alamat`, a.`bank`, a.`norek`, a.`norek_an`, a.`tfinfo`, a.`nama_sklh`, a.`jurusan`, a.`th_lulus`, a.`ikut_asuransi`, a.`npwp`, a.`id_pend`, a.`idp`, a.`th1`, a.`penyakit1`, a.`th2`, a.`penyakit2`, a.`th3`, a.`penyakit3`, a.`no_ijazah`, a.`status_ijazah`, a.`telp_emergency`, a.`contact_emergency`, a.`surat_komitmen`, a.salary 
                FROM kary a 
                WHERE a.`nip` = '$nip'")->row();
    }

    function update_karyawan($data,$nip)
    {
        $this->db->where('nip',$nip)->update('kary',$data);
    }

    function idp_opt()
    {
        $option=$this->db->from('idp')->get()->result();
        foreach ($option as $row) {
            $result[$row->idp] = $row->bu;
        }
        return $result;
    }

    function cari_jab($idp,$lokasi)
    {
        return $this->db->from("pos_sto")->join('pos','pos.id_pos=pos_sto.id_pos')->where('pos.idp',$idp)->where('pos_sto.id_lka',$lokasi)->get()->result();
    }

    function sal_pos($id_sto)
    {
        return $this->db->query("SELECT * FROM pos_sto,pos WHERE pos.`id_pos`=pos_sto.`id_pos` AND pos.`sns`='STAFF' AND pos_sto.`id_sto`='$id_sto' ")->row();
    }

    function cari_band($sal_pos,$idp)
    {
        return $this->db->where('sal_staff_class.sal_pos',$sal_pos)->where('sal_staff_class.idp',$idp)->from('sal_staff_class')->join('band','band.id_band=sal_staff_class.id_band')->get()->result();
    }

    function add_sk($data)
    {
        $this->db->insert('sk',$data);
    }

    function view_sk($nip)
    {
        return $this->db->query(
            "SELECT * FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_cabang d ON c.`id_cabang`=d.`id_cab` JOIN ms_tipe_sk e ON a.`id_tipe_sk`=e.`id_tipe_sk` WHERE a.`nip`='$nip' ORDER BY a.`id_sk` DESC")->result();
    }

    function hitung_thp($id_sk)
    {
        return $this->db->query("SELECT kary.`nama`,pos.`posisi`,sal_staff_class.`tpj`,sk.`gd`,ROUND(sal_staff_class.`td`*sk.`gd`/100) AS td, sal_staff_class.`tpj`,sal_staff_class.`tk`,sk.`tprof`, ROUND(sk.`gd`+(sal_staff_class.`td`*sk.`gd`/100)+sal_staff_class.`tpj`+sal_staff_class.`tk`+sk.`tprof`,0) AS thp FROM sk,pos_sto,pos,sal_staff_class,kary WHERE sk.`id_pos_sto`=pos_sto.`id_sto` AND sk.`id_sk`='$id_sk' AND pos_sto.`id_pos`=pos.`id_pos` AND sal_staff_class.`sal_pos`=pos.`sal_pos` AND sk.`id_band`=sal_staff_class.`id_band` AND pos.`idp`=sal_staff_class.`idp` AND kary.`nip`=sk.`nip`")->row();
    }

    function sk_id($id_sk)
    {
        return $this->db->query("SELECT a.*,b.*,c.*,DATE_FORMAT(a.`tgl_kerja`,'%d/%m/%Y') tglkerja, DATE_FORMAT(a.`tgl_sk`,'%d/%m/%Y') tglsk FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE a.`id_sk`='$id_sk'")->row();
    }

    function update_sk($data,$id)
    {
        $this->db->where('id_sk',$id)->update('sk',$data);
    }

    function cabut_sk($id, $data)
    {
        $this->db->where('id_sk',$id)->update('sk',$data);
    }

    function hapus_sk($id)
    {
        $this->db->where('id_sk',$id)->delete('sk');
    }

    function resign($data,$nip)
    {
        $this->db->where('nip', $nip)->update('kary',$data);
        $this->db->where('id_employee', $nip)->update('absensi.ms_user', ['status' => 0]);
    }

    function delete_karyawan($nip)
    {
        $this->db->where('nip',$nip)->delete('kary');
        $this->db->where('nip',$nip)->delete('sk');
        $this->db->where('nip',$nip)->delete('kont');
    }

    function view_calon_kary()
    {
        return $this->db->query("SELECT a.`status`,c.`posisi`,d.`cabang`,a.`id`,a.`nama`,a.`jen_kel`,a.`alamat`,DATE_FORMAT(a.`tgl_lahir`,'%d %M %Y') AS tgl_lahir, a.`hp`,b.`pend`,a.`email`,a.`institusi`,a.`pengalaman` FROM cal_kary a JOIN pend b ON a.`id_pend`=b.`id_pend` left JOIN r_loker c ON a.`id_loker`=c.`id_loker` left JOIN ms_cabang d ON a.`id_cabang`=d.`id_cab`")->result();
    }

    function add_cal_kary($data)
    {
        $this->db->insert('cal_kary',$data);
    }

    function cal_kary_id($id)
    {
        return $this->db->query("SELECT a.`status`,a.`id_loker`,a.`id_cabang`,a.`id`,a.`alamat`,DATE_FORMAT(a.`tgl_lahir`,'%d/%m/%Y') AS tgl_lahir,a.`email`,a.`hp`,a.`id_pend`,a.`institusi`,a.`jen_kel`,a.`nama`,a.`pengalaman` FROM cal_kary a WHERE a.`id` = '$id'")->row();
    }

    function update_cal_kary($data,$id)
    {
        $this->db->where('id',$id)->update('cal_kary',$data);
    }

    function delete_cal_kary($id)
    {
        $this->db->where('id',$id)->delete('cal_kary');
    }

    function bpjs_ks()
    {
        return $this->db->query("SELECT * FROM bpjs_ks ORDER BY th DESC")->row();
    }

    function view_kk($nip)
    {
        return $this->db->query("SELECT * FROM kk WHERE nip = '$nip' ")->result();
    }

    function save_no_kk($data)
    {
        $this->db->insert('kk',$data);
    }

    function get_kk($nip)
    {
        return $this->db->query("SELECT no_kk FROM kk WHERE nip = '$nip' LIMIT 1")->row();
    }

    function update_no_kk($data,$nip)
    {
        $this->db->where('nip',$nip)->update('kk',$data);
    }

    function process_add_kk($data,$id='')
    {   
        if(empty($id))
        {
            $this->save_no_kk($data);
        }
        else
        {
            $this->db->where('id_kk',$id)->update('kk',$data);
        }
    }

    function id_kk($id)
    {
        return $this->db->query("SELECT *,DATE_FORMAT(a.`tgl_lahir`, '%d/%m/%Y') tgl FROM kk a WHERE a.`id_kk`='$id'")->row();
    }

    function del_kk($id)
    {
        $this->db->where('id_kk',$id)->delete('kk');
    }

    function kary_nip($nip)
    {
        return $this->db->query("SELECT * FROM kary a JOIN pend b ON a.`id_pend` = b.`id_pend` WHERE a.`nip` = '$nip'")->row();
    }

    function up_tgl_masuk($d, $nip)
    {
        $this->db->where('nip',$nip)->update('kary',$d);
    }

    function view_sk_kosong()
    {
        $per    = $this->Main_Model->per_skr();
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT * FROM kary a WHERE NOT EXISTS (SELECT * FROM sk b WHERE a.`nip`=b.`nip` AND b.`aktif`='1') AND a.`idp`='$idp' AND ISNULL(a.`tgl_resign`) AND a.`tgl_masuk` BETWEEN '$per->tgl_awal' AND '$per->tgl_akhir'")->result();
    }

    function view_baru_or_resign($condition)
    {
        $per    = $this->Main_Model->per_skr();
        $idp    = $this->session->userdata('idp');

        return $this->db->query("SELECT * FROM kary a WHERE $condition BETWEEN '$per->tgl_awal' AND '$per->tgl_akhir' AND a.`idp`='$idp'")->result();
    }

    function view_kary_ijazah()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT * FROM kary a WHERE isnull(a.`no_ijazah`) and isnull(a.`tgl_resign`) AND a.`idp`='$idp'")->result();
        // return $this->db->query("SELECT * FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN pos d ON c.`id_pos`=d.`id_pos` JOIN ms_cabang e ON d.`id_cabang`=e.`id_cab` WHERE isnull(a.`no_ijazah`) and isnull(a.`tgl_resign`) AND a.`idp`='$idp' AND b.`aktif`='1'")->result();
    }

    function view_kary_mutasi()
    {
        $idp    = $this->session->userdata('idp');

        return $this->db->query("select * from sk a join kary b on a.`nip`=b.`nip` join pos_sto c on a.`id_pos_sto`=c.`id_sto` join pos d on c.`id_pos`=d.`id_pos` join ms_cabang e on d.`id_cabang`=e.`id_cab` where ISNULL(b.`tgl_resign`) and a.`aktif`='1' and (a.`id_tipe_sk`='1' or a.`id_tipe_sk`='4' or a.`id_tipe_sk`='6')")->result();
    }

    // departemen

    function view_departemen($idp)
    {
        return $this->db->where('idp',$idp)->where('status',1)->get('ms_departemen')->result();
    }

    function departemen_process($data,$id)
    {
        if($id)
        {
            $this->db->where('id_dept',$id)->update('ms_departemen',$data);
        }
        else
        {
            $this->db->insert('ms_departemen',$data);
        }
    }

    function departemen_id($id)
    {
        return $this->db->where('id_dept',$id)->get('ms_departemen')->row();
    }

    // cabang

    function view_cabang($idp)
    {
        return $this->db->where('idp',$idp)->where('status',1)->get('ms_cabang')->result();
    }

    function cabang_process($data,$id)
    {
        if($id)
        {
            $this->db->where('id_cab',$id)->update('ms_cabang',$data);
        }
        else
        {
            $this->db->insert('ms_cabang',$data);
        }
    }

    function cabang_id($id)
    {
        return $this->db->where('id_cab',$id)->get('ms_cabang')->row();
    }

    // divisi

    function view_divisi($idp)
    {
        return $this->db->where('idp',$idp)->where('status',1)->get('ms_divisi')->result();
    }

    function divisi_process($data,$id)
    {
        if($id)
        {
            $this->db->where('id_divisi',$id)->update('ms_divisi',$data);
        }
        else
        {
            $this->db->insert('ms_divisi',$data);
        }
    }

    function divisi_id($id)
    {
        return $this->db->where('id_divisi',$id)->get('ms_divisi')->row();
    }

    // pencarian sk
    function cari_cabang($id_departemen)
    {
        return $this->db->query("SELECT DISTINCT(b.`cabang`),b.`id_cab` FROM pos a JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` WHERE a.`id_departemen`='$id_departemen' AND a.`status`=1")->result();
    }

    function cari_divisi($id_cabang, $id_departemen)
    {
        return $this->db->query("SELECT DISTINCT(b.`divisi`),b.`id_divisi` FROM pos a JOIN ms_divisi b ON a.`id_divisi`=b.`id_divisi` WHERE a.`id_cabang`='$id_cabang' AND a.`id_departemen`='$id_departemen' AND a.`status`=1")->result();
    }

    function cari_jabatan($id_divisi = '', $id_cabang = '', $id_departemen = '', $rotasi = '')
    {
        if($rotasi == '') {
            return $this->db->query("SELECT a.`jab`,b.`id_sto`,b.`id_stob_pos`, a.sal_pos FROM pos a JOIN pos_sto b ON a.`id_pos`=b.`id_pos` WHERE a.`id_departemen`='$id_departemen' AND a.`id_cabang`='$id_cabang' AND a.`id_divisi`='$id_divisi' AND b.`status`=1 AND NOT EXISTS(SELECT NULL FROM sk c WHERE b.`id_sto`=c.`id_pos_sto` AND c.`aktif` = 1)")->result();
        } else {
            return $this->db->query("SELECT a.`jab`,b.`id_sto`,b.`id_stob_pos`, a.sal_pos FROM pos a JOIN pos_sto b ON a.`id_pos`=b.`id_pos` WHERE a.`id_departemen`='$id_departemen' AND a.`id_cabang`='$id_cabang' AND a.`id_divisi`='$id_divisi' AND b.`status`=1")->result();
        }
    }

    function cari_jabatan_edit($id_divisi, $id_cabang, $id_departemen, $id_sk)
    {
        return $this->db->query("SELECT a.`jab`,b.`id_sto`,b.`id_stob_pos` FROM pos a JOIN pos_sto b ON a.`id_pos`=b.`id_pos` WHERE a.`id_departemen`='$id_departemen' AND a.`id_cabang`='$id_cabang' AND a.`id_divisi`='$id_divisi' AND b.`status`=1 AND NOT EXISTS(SELECT NULL FROM sk c WHERE b.`id_sto`=c.`id_pos_sto` AND c.`aktif` = 1 AND c.`id_sk` != '$id_sk')")->result();
    }

    function cari_dept_for_excel($nip)
    {
        return $this->db->query("SELECT d.`departemen`,e.`cabang`,f.`divisi`,c.`sal_pos`,c.`jab` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN ms_departemen d ON c.`id_departemen`=d.`id_dept` JOIN ms_cabang e ON c.`id_cabang`=e.`id_cab` JOIN ms_divisi f ON c.`id_divisi`=f.`id_divisi` WHERE a.`aktif`='1' AND a.`nip`='$nip'")->row();
    }

    function view_kary_surat_komitmen()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT * FROM kary a WHERE (ISNULL(a.`surat_komitmen`) OR a.`surat_komitmen`='BELUM' OR isnull(a.`file_surat_komitmen`)) AND ISNULL(a.`tgl_resign`) AND a.`idp` = '$idp'")->result();
    }

    function view_kary_rekening()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
                SELECT * 
                FROM kary a 
                WHERE (a.`norek` = 'Belum Ada' OR ISNULL(a.`norek`) OR a.`norek` = '') 
                AND a.`idp` = '$idp'
                AND ISNULL(a.tgl_resign)")->result();
    }

    function det_calon_kary($id)
    {
        return $this->db->query("select a.*,date_format(a.`tgl_lahir`,'%d %M %Y') tanggalLahir, b.`pend`, c.`cabang`, d.`posisi` from cal_kary a join pend b on a.`id_pend`=b.`id_pend` left join ms_cabang c on a.`id_cabang`=c.`id_cab` left join r_loker d on a.`id_loker`=d.`id_loker` where a.`id` = '$id'")->row();
    }

    function set_nonactive($nip='', $active='')
    {
        $this->db->where('nip',$nip)->update('sk',$active);
        $this->db->where('nip',$nip)->update('kont',$active);
    }

    function view_kary_kontrak_habis($qd_id='')
    {
        $periode = $this->Main_Model->id_periode($qd_id);
        $tgl_akhir = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " f.`id_cab` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT * FROM kont a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN sk c ON a.`nip`=c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` 
            JOIN pos e ON d.`id_pos`=e.`id_pos` 
            JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab` 
            WHERE a.`tgl_akhir` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND a.`aktif`='1' 
            AND c.`aktif`='1' $cabang")->result();
    }

    function view_kary_kontrak_baru($qd_id='')
    {
        $periode = $this->Main_Model->id_periode($qd_id);
        $tgl_akhir = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " f.`id_cab` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $id_divisi = $this->Main_Model->session_divisi();
        $divisi = '';
        if (! empty($id_divisi)) {
            $divisi .= ' AND( ';
            for ($i = 0; $i < count($id_divisi); $i++) {
                $divisi .= " g.id_divisi = '$id_divisi[$i]' ";
                if (end($id_divisi) != $id_divisi[$i]) {
                    $divisi .= ' OR ';
                } else {
                    $divisi .= ' ) ';
                }
            }
        }

        return $this->db->query("
            SELECT * FROM kont a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN sk c ON a.`nip`=c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` 
            JOIN pos e ON d.`id_pos`=e.`id_pos` 
            JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab` 
            JOIN ms_divisi g ON g.id_divisi = e.id_divisi
            WHERE a.`tgl_awal` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND a.`aktif`='1' 
            AND c.`aktif`='1' 
            $cabang $divisi")->result();
    }

    function view_kary_resign($qd_id='')
    {
        $periode    = $this->Main_Model->id_periode($qd_id);
        $tgl_akhir  = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';

        return $this->db->query("
            SELECT a.*, IFNULL(s.cabang, '') AS cabang
            FROM kary a 
            LEFT JOIN (
                SELECT MAX(tgl_sk), id_sk, nip, id_pos_sto, cabang
                FROM sk 
                INNER JOIN pos_sto ON pos_sto.`id_sto` = sk.`id_pos_sto`
                INNER JOIN pos ON pos.`id_pos` = pos_sto.`id_pos`
                INNER JOIN ms_cabang ON ms_cabang.`id_cab` = pos.`id_cabang`
                GROUP BY nip
            ) AS s ON s.nip = a.`nip`
            WHERE a.`tgl_resign` != '' 
            AND a.`tgl_resign` 
            BETWEEN '$tgl_awal' AND '$tgl_akhir'")->result();
    }

    function view_kary_baru($qd_id='')
    {
        $periode    = $this->Main_Model->id_periode($qd_id);
        $tgl_akhir  = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';
        $tgl_awal  = isset($periode->tgl_awal) ? $periode->tgl_awal : '';

        return $this->db->query("
            SELECT * FROM kary a 
            join sk b on a.`nip`=b.`nip` 
            join pos_sto c on b.`id_pos_sto`=c.`id_sto` 
            join pos d on c.`id_pos`=d.`id_pos` 
            join ms_cabang e on e.`id_cab`=d.`id_cabang` 
            WHERE isnull(a.`tgl_resign`) 
            AND a.`tgl_masuk` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND b.`aktif`='1'")->result();
    }

    function view_surat_kesanggupan()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("select * from kary a join tb_sp b on a.`nip`=b.`nip` where b.`aktif`='1' and b.`idp`='$idp' and isnull(b.`surat_kesanggupan`) and isnull(a.`tgl_resign`)")->result();
    }

    function cek_pengajuan_karyawan()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " a.`id_cabang` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag`,
                e.keterangan as ket_hr, f.keterangan as ket_progress, g.keterangan as ket_ttd 
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                LEFT JOIN ms_status_rekrutmen e ON e.kode = a.status_hr
                LEFT JOIN ms_status_rekrutmen f on f.kode = a.progress
                LEFT JOIN ms_status_rekrutmen g on g.kode = a.status_ttd
                LEFT JOIN d_pengajuan_kary h ON h.`id` = a.`id`
                WHERE ISNULL(h.`nip`)
                $cabang
                GROUP BY a.`id`
                ")->result();
    }

    function view_pengajuan_karyawan($qd_id='')
    {
        $per = $this->Main_Model->id_periode($qd_id);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " a.`id_cabang` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }


        if($id_cabang) {
            return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag`,
                e.keterangan as ket_hr, f.keterangan as ket_progress, g.keterangan as ket_ttd 
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                LEFT JOIN ms_status_rekrutmen e ON e.kode = a.status_hr
                LEFT JOIN ms_status_rekrutmen f on f.kode = a.progress
                LEFT JOIN ms_status_rekrutmen g on g.kode = a.status_ttd
                WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                $cabang
                ")->result();
        } else {
            return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag`,
                e.keterangan as ket_hr, f.keterangan as ket_progress, g.keterangan as ket_ttd  
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                LEFT JOIN ms_status_rekrutmen e ON e.kode = a.status_hr
                LEFT JOIN ms_status_rekrutmen f on f.kode = a.progress
                LEFT JOIN ms_status_rekrutmen g on g.kode = a.status_ttd
                WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                AND a.`status` = '12'
                ")->result();
        }
    }

    function view_karyawan_finalisasi($tgl_awal='', $tgl_akhir='')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " a.`id_cabang` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag`,
                e.keterangan as ket_hr, f.keterangan as ket_progress, g.keterangan as ket_ttd 
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                LEFT JOIN ms_status_rekrutmen e ON e.kode = a.status_hr
                LEFT JOIN ms_status_rekrutmen f on f.kode = a.progress
                LEFT JOIN ms_status_rekrutmen g on g.kode = a.status_ttd
                WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                AND a.progress = 5
                $cabang")->result();
    }

    function view_approval_pengajuan()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' a.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        // $sal_pos = $this->Main_Model->class_approval();
        // $kelas = '';
        // if(!empty($sal_pos)) {
        //     $kelas .= ' AND (';
        //     for($i = 0; $i < count($sal_pos); $i++) {
        //         $kelas .= " a.`sal_pos` = '$sal_pos[$i]'";

        //         if(end($sal_pos) != $sal_pos[$i]) {
        //             $kelas .= ' OR';
        //         } else {
        //             $kelas .= ')';
        //         }
        //     }
        // }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`privilege` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }
        return $this->db->query("
            SELECT a.*,b.`cabang`,c.`nama`, d.`keterangan` 
            FROM pengajuan_kary a 
            JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
            LEFT JOIN kary c ON a.`nip`=c.`nip`
            JOIN ms_status_approval d ON d.`kode` = a.`status` 
            WHERE d.`flag` = 1 $cabang $kelas
             ")->result();
    }

    function pengajuan_karyawan_process($data, $id='')
    {
        if($id) {
            $this->db->where('id',$id)->update('pengajuan_kary',$data);
        } else {
            $this->db->insert('pengajuan_kary',$data);
        }
    }

    function id_pengajuan_kary($id='')
    {
        return $this->db->query("select *,date_format(tgl_pengajuan,'%d/%m/%Y') tanggal_pengajuan, date_format(tgl_pemenuhan,'%d/%m/%Y') tanggal_pemenuhan from pengajuan_kary where id = '$id'")->row();
    }

    function delete_pengajuan_kary($id)
    {
        $this->db->where('id',$id)->delete('pengajuan_kary');
    }

    function view_management_user()
    {
        // return $this->db->query("SELECT a.`approval`,a.`usr_id`,a.`usr_name`,a.`usr_pass`,a.`usr_AccType`,a.`nama`,b.`id_cab`,b.`cabang`,AES_DECRYPT(a.`usr_pass`,'hr') pwd FROM users a LEFT JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab`")->result();

        return $this->db->query("
            SELECT a.*,
            AES_DECRYPT(a.`usr_pass`,'hr') pwd, 
            GROUP_CONCAT(DISTINCT(b.`sal_pos`) SEPARATOR ',<br>') kelas, 
            GROUP_CONCAT(DISTINCT(d.`cabang`) SEPARATOR ',<br>') cabang, 
            GROUP_CONCAT(DISTINCT(f.`divisi`) SEPARATOR ',<br>') divisi,
            GROUP_CONCAT(DISTINCT(g.`sal_pos`) SEPARATOR ',<br>') pengajuan,
            GROUP_CONCAT(DISTINCT(i.`cabang`) SEPARATOR ',<br>') cabang_maninfo
            FROM users a 
            LEFT JOIN tb_level_approval b ON a.`usr_name` = b.`usr_name`
            LEFT JOIN tb_user_cabang c ON a.`usr_name` = c.`usr_name`
            LEFT JOIN ms_cabang d ON d.`id_cab` = c.`id_cabang`
            LEFT JOIN tb_user_divisi e ON e.`usr_name` = a.`usr_name`
            LEFT JOIN ms_divisi f ON e.`id_divisi` = f.`id_divisi`
            LEFT JOIN tb_level_pengajuan g ON g.`usr_name` = a.`usr_name`
            LEFT JOIN tb_maninfo_cabang h ON h.usr_name = a.usr_name
            LEFT JOIN ms_cabang i ON i.`id_cab` = h.`id_cabang`
            WHERE a.`usr_active` = '1' GROUP BY a.`usr_name`
            ")->result();
    }

    function process_management_user($data, $id='')
    {
        $usr_name = $data['usr_name'];
        $usr_pass = $data['usr_pass'];
        // $id_cabang  = $data['id_cabang'];
        $nama = $data['nama'];
        $level = $data['usr_AccType'];
        $approval = $data['approval'];

        if($id) {
            $this->db->query("
                update users 
                    set usr_name='$usr_name',
                    usr_pass=AES_ENCRYPT('$usr_pass','hr'), 
                    nama='$nama',
                    usr_AccType='$level',
                    approval='$approval' 
                    where usr_id='$id'");
        } else {
            $this->db->query("
                insert into users(
                usr_name,
                usr_pass,
                usr_AccType,
                nama,
                approval) 
                values(
                '$usr_name',
                AES_ENCRYPT('$usr_pass','hr'),
                '$level',
                '$nama',
                '$approval')");
        }
    }

    function user_id($id='')
    {
        return $this->db->query("
            SELECT a.*,
            AES_DECRYPT(a.`usr_pass`,'hr') pwd, 
            GROUP_CONCAT(DISTINCT(b.`sal_pos`)) kelas, 
            GROUP_CONCAT(DISTINCT(c.`id_cabang`)) cabang, 
            GROUP_CONCAT(DISTINCT(d.`id_divisi`)) divisi,
            GROUP_CONCAT(DISTINCT(e.`sal_pos`)) pengajuan,
            GROUP_CONCAT(DISTINCT(f.id_cabang)) maninfo_cabang
            FROM users a 
            LEFT JOIN tb_level_approval b ON a.`usr_name` = b.`usr_name`
            LEFT JOIN tb_user_cabang c ON a.`usr_name` = c.`usr_name`
            LEFT JOIN tb_user_divisi d ON d.`usr_name` = a.`usr_name`
            LEFT JOIN tb_level_pengajuan e ON e.`usr_name` = a.`usr_name`
            LEFT JOIN tb_maninfo_cabang f ON f.usr_name = a.usr_name
            WHERE a.`usr_id` = '$id' GROUP BY a.`usr_name`
            ")->row();
    }

    function delete_user($id)
    {
        // $q = $this->db->where('usr_id', $id)->get('users')->row();
        // if($q) {
        //     $this->db->where('usr_id',$id)->delete('users');
        //     $this->db->where('usr_name', $q->usr_name)->delete('tb_user_cabang');
        //     $this->db->where('usr_name', $q->usr_name)->delete('tb_level_approval');
        // }
        $this->db->where('usr_id', $id)->update('users', array('usr_active' => 0));
    }

    function delete_det_pengajuan($id)
    {
        $this->db->where('id_det',$id)->delete('d_pengajuan_kary');
    }

    function add_detail_pengajuan($data)
    {   
        $jenis = $data['jenis'];

        if ($jenis != 'baru') {
            $id_p = $data['nip']; // ketika jenis mutasi / promosi 'nip' merupakan id.
            $p = $this->db->where('id', $id_p)->get('tb_pengajuan')->row();
            $nip = isset($p->nip) ? $p->nip : '';

            $id = $data['id'];
            $tgl_pemenuhan = $data['tgl_pemenuhan'];
            $user = $data['user'];

            $this->db->where('id', $id_p)->update('tb_pengajuan', array('tgl_pemenuhan' => $tgl_pemenuhan));
            $d = array(
                'id' => $id,
                'nip' => $nip,
                'tgl_pemenuhan' => $tgl_pemenuhan,
                'jenis' => $jenis,
                'user' => $user
            );
            $this->db->insert('d_pengajuan_kary', $d);
        } else {
            $this->db->insert('d_pengajuan_kary', $data);
        }
    }

    function history_sk($date_start='', $date_end='')
    {
        return $this->db->query("select a.*,b.`tipe`,c.`nama`,e.`jab`,f.`cabang`,
        (select pos.`jab` from sk left join pos_sto  on sk.`id_pos_sto`=pos_sto.`id_sto` left join pos on pos_sto.`id_pos`=pos.`id_pos` where sk.`nip`=c.`nip` AND sk.`id_sk` <> a.`id_sk` ORDER BY sk.`id_sk` DESC LIMIT 1) jab_lama, 
        (select ms_cabang.`cabang` from sk left join pos_sto on sk.`id_pos_sto`=pos_sto.`id_sto` left join pos on pos_sto.`id_pos`=pos.`id_pos` LEFt join ms_cabang on ms_cabang.`id_cab`=pos.`id_cabang` where sk.`nip`=c.`nip` AND sk.`id_sk` <> a.`id_sk` ORDER BY sk.`id_sk` DESC LIMIT 1) lok_lama
        from sk a 
        join ms_tipe_sk b on a.`id_tipe_sk`=b.`id_tipe_sk` 
        join kary c on a.`nip`=c.`nip`
        join pos_sto d on a.`id_pos_sto`=d.`id_sto`
        join pos e on d.`id_pos`=e.`id_pos`
        join ms_cabang f on e.`id_cabang`=f.`id_cab`
        where a.`id_tipe_sk` <> 5 and a.`tgl_sk` between '$date_start' and '$date_end 23:59:00'")->result();
    }

    function check_pemberkasan($nip='')
    {
        return $this->db->query("
            SELECT * 
            FROM tb_pemberkasan a 
            WHERE a.`nip`='$nip' 
            AND a.`ijazah`='1' 
            AND a.`kesanggupan`='1' 
            AND a.`lamaran`='1' 
            AND a.`ojt`='1' 
            AND a.`pertanggungjawaban`='1' 
            AND a.`rekening`='1' 
            AND a.`upah`='1'")->row();
    }

    function view_pemberkasan($nip='')
    {
        return $this->db->where('nip', $nip)->get('tb_pemberkasan')->row();
    }

    function process_pemberkasan($data, $nip='')
    {
        if($nip)
        {
            $this->db->where('nip',$nip)->update('tb_pemberkasan',$data);
        }
        else
        {
            $this->db->insert('tb_pemberkasan',$data);
        }
    }

    function view_history_kontrak($id_periode='')
    {
        $q = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = isset($q->tgl_awal) ? $q->tgl_awal : '';
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';
        // return $this->db->query("
        //     select a.`id_kontrak`,a.`nip`,b.`nama`,a.`no_kontrak`,a.`tgl_awal`,a.`tgl_akhir`,a.`tipe`,a.`aktif`,
        //     (select kont.`tipe` from kont where kont.`nip`=a.`nip` and kont.`aktif`='0') tipe_kontrak_lama,
        //     (SELECT kont.`tgl_awal` FROM kont WHERE kont.`nip`=a.`nip` AND kont.`aktif`='0') tgl_awal_lama,
        //     (SELECT kont.`tgl_akhir` FROM kont WHERE kont.`nip`=a.`nip` AND kont.`aktif`='0') tgl_akhir_lama,
        //     b.`tgl_tetap`,b.`kary_stat` status_karyawan
        //      from kont a 
        //     join kary b on a.`nip`=b.`nip`
        //     join sk c on a.`nip`=c.`nip`
        //     join pos_sto d on c.`id_pos_sto`=d.`id_sto`
        //     join pos e on d.`id_pos`=e.`id_pos`
        //     where ((a.`tgl_awal` between '$tgl_awal' and '$tgl_akhir' AND a.`aktif`='1') or b.`tgl_tetap` BETWEEN '$tgl_awal' AND '$tgl_akhir')  and c.`aktif`='1'
        //     and isnull(b.`tgl_resign`)")->result();

        return $this->db->query("
            SELECT a.*, b.`nama`, IFNULL(kont_a.tipe, '') AS tipe_kontrak_lama,
            IFNULL(kont_a.tgl_awal, '') AS tgl_awal_lama, IFNULL(kont_a.tgl_akhir, '') AS tgl_akhir_lama, b.`kary_stat` AS status_karyawan
            FROM kont a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN sk c ON a.`nip`=c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` 
            JOIN pos e ON d.`id_pos`=e.`id_pos` 
            LEFT JOIN (
                SELECT * FROM kont 
                WHERE kont.`aktif` = 0
                GROUP BY kont.`id_kontrak`
                ORDER BY kont.`id_kontrak` DESC
            ) AS kont_a ON kont_a.nip = a.`nip`
            WHERE ((a.`tgl_awal` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`aktif`='1') 
            OR b.`tgl_tetap` BETWEEN '$tgl_awal' AND '$tgl_akhir') AND c.`aktif`='1' AND ISNULL(b.`tgl_resign`)
            GROUP BY a.`id_kontrak`")->result();
    }

    function view_file($table='', $column='')
    {
        if($table != '' && $column != '') {
            if($table == 'kary') {
                $q = $this->db->select($column)->select('nama')->where($column, '!= ""')->get($table)->result();
            } else {
                $q = $this->db->query("select $table.$column, kary.nama from $table join kary on kary.nip=$table.nip where $table.$column != ''")->result();
            }
            return $q;
        }
    }

    function reminder_approval_lembur($id_cabang='', $sal_pos='', $divisi='')
    {
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT a.*, b.`nama`,
                (SELECT MAX(tb_absensi.`scan_pulang`) 
                FROM tb_absensi 
                WHERE DATE_FORMAT(tb_absensi.`tgl`,'%Y-%m-%d') = a.`tgl`) scan_pulang 
            FROM lembur a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON b.`nip` = c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto` 
            JOIN pos e ON d.`id_pos` = e.`id_pos`
            WHERE a.`tgl` + INTERVAL + 2 DAY <= CURDATE() 
            AND a.`status` = '0' AND ISNULL(b.tgl_resign)
            $cabang $kelas $div")->result();
    }

    function view_teguran($id_periode='')
    {
        $q = $this->Main_Model->id_periode($id_periode);
        $tgl_akhir = isset($q->tgl_akhir) ? $q->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " e.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $where = '';
        if(!empty($id_cabang) || !empty($sal_pos) || !empty($divisi)) $where = $cabang . $kelas . $div;

        return $this->db->query("
            SELECT b.`nama`,a.*,f.`cabang` FROM tb_teguran a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON c.`nip` = b.`nip`
            JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            JOIN pos e ON e.`id_pos` = d.`id_pos`
            JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang` 
            WHERE a.`bulan` = DATE_FORMAT('$tgl_akhir','%m') AND a.`tahun` = DATE_FORMAT('$tgl_akhir','%Y') 
            AND c.aktif = 1
            $where ORDER BY a.`insert_at` DESC")->result();
    }

    function th_teguran()
    {
        return $this->db->query("SELECT DISTINCT(a.`tahun`) th FROM tb_teguran a")->result();
    }

    function update_karyawan_posisi()
    {
        $q = $this->db->query(
            "SELECT a.`pin`, a.`nip`, a.`nama`, d.`jab`, d.`sal_pos`, e.`cabang`, f.`divisi`, g.`departemen` 
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            JOIN ms_divisi f ON f.`id_divisi` = d.`id_divisi`
            JOIN ms_departemen g ON g.`id_dept` = d.`id_departemen`
            WHERE b.`aktif` = '1'"
            )->result();

        foreach ($q as $row) {
            $data = array(
                'pin' => $row->pin,
                'nip' => $row->nip,
                'nama' => $row->nama,
                'posisi' => $row->jab,
                'sal_pos' => $row->sal_pos,
                'cabang' => $row->cabang,
                'divisi' => $row->divisi,
                'departemen' => $row->departemen
                );
            $r = $this->db->where($data)->get('tr_posisi_aktif')->row();
            if(empty($r)) {
                $this->db->insert('tr_posisi_aktif', $data);
            } else {
                $this->db->where('nip', $row->nip)->update('tr_posisi_aktif', $data);
            }
        }
    }

    function view_pengajuan($tipe='', $id_periode='')
    {
        $per = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " f.`id_cabang` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT a.*,b.`cabang`,c.`nama`
            FROM tb_pengajuan a 
            LEFT JOIN ms_cabang b ON a.`id_cabang` = b.`id_cab` 
            LEFT JOIN kary c ON a.`nip`=c.`nip`
            LEFT JOIN sk d ON d.nip = c.nip
            LEFT JOIN pos_sto e ON e.id_sto = d.id_pos_sto
            LEFT JOIN pos f ON f.id_pos = e.id_pos 
            WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
            $cabang AND tag = '$tipe' AND d.aktif = 1 
            ")->result();
    }

    function id_pengajuan($id='')
    {
        return $this->db->query("
            SELECT *, DATE_FORMAT(tgl_pengajuan, '%d/%m/%Y') tanggal_pengajuan 
            FROM tb_pengajuan WHERE id = '$id'")->row();
    }

    function data_karyawan($jenis='', $id='')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' pos.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $p = $this->db->where('id', $id)->get('pengajuan_kary')->row();
        $p_idcabang = isset($p->id_cabang) ? $p->id_cabang : '';
        $p_posisi = isset($p->posisi) ? $p->posisi : '';
        $p_sal_pos = isset($p->sal_pos) ? $p->sal_pos : '';

        switch ($jenis) {
            case 'baru':
                $query = "
                    SELECT a.nip, a.nama FROM kary a
                    INNER JOIN sk b ON a.`nip` = b.`nip`
                    INNER JOIN pos_sto c ON b.`id_pos_sto` = c.`id_sto`
                    INNER JOIN pos ON c.`id_pos` = pos.`id_pos`
                    INNER JOIN ms_cabang e ON pos.`id_cabang` = e.`id_cab`
                    WHERE b.`aktif` = 1
                    AND ISNULL(a.`tgl_resign`)
                    $cabang ORDER BY a.nama ASC";
                break;
            case 'promosi':
                $query = "
                    SELECT a.id AS nip, b.nama FROM tb_pengajuan a
                    INNER JOIN kary b ON a.`nip` = b.`nip`
                    INNER JOIN sk c ON c.`nip` = b.`nip`
                    INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                    INNER JOIN pos ON pos.`id_pos` = d.`id_pos`
                    INNER JOIN ms_cabang f ON f.`id_cab` = pos.`id_cabang`
                    WHERE c.`aktif` = 1
                    AND a.`tag` = 'promosi'
                    AND a.id_cabang = '$p_idcabang'
                    AND a.posisi = '$p_posisi'
                    AND a.sal_pos = '$p_sal_pos'
                    AND ISNULL(a.tgl_pemenuhan)
                    ORDER BY b.nama ASC";
                break;
            case 'mutasi':
                $query = "
                    SELECT a.id AS nip, b.nama FROM tb_pengajuan a
                    INNER JOIN kary b ON a.`nip` = b.`nip`
                    INNER JOIN sk c ON c.`nip` = b.`nip`
                    INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
                    INNER JOIN pos ON pos.`id_pos` = d.`id_pos`
                    INNER JOIN ms_cabang f ON f.`id_cab` = pos.`id_cabang`
                    WHERE c.`aktif` = 1
                    AND a.`tag` = 'mutasi'
                    AND a.id_cabang = '$p_idcabang'
                    AND a.posisi = '$p_posisi'
                    AND a.sal_pos = '$p_sal_pos'
                    AND ISNULL(a.tgl_pemenuhan)
                    ORDER BY b.nama ASC";
                break;
            default:
                $query = '';
                break;
        }

        $result = array(
            // 'result' => array(),
            // 'pagination' => array(
            //     'more' => true
            // )
        );

        if ($query != '') {
            $data = $this->db->query($query)->result();
            foreach ($data as $row => $val) {
                $arr[$row] = array(
                    'id' => $val->nip,
                    'text' => $val->nama
                );
                $result = $arr;
            }
        }

        return json_encode($result);
    }

    function jab_setup_gaji()
    {
        return $this->db->query("
            SELECT a.`jab`, IFNULL(gaji.hitung, 0) AS hitung 
            FROM ms_jobdesk a
            LEFT JOIN (
                SELECT COALESCE(COUNT(*), 0) AS hitung, jab 
                FROM tb_setup_gaji
            ) AS gaji ON gaji.jab = a.`jab`
            HAVING hitung < '2' 
            ORDER BY a.jab ASC")->result();
    }

    function view_karyawan_aktif($cabang_id = '') 
    {
        $idp = $this->session->userdata('idp');
        
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if ($cabang_id) {
            $cabang .= ' AND d.`id_cabang` = '.$cabang_id;
        } else {
           if(!empty($id_cabang)) {
                $cabang .= 'AND (';
                for($i = 0; $i < count($id_cabang); $i++) {
                    $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                    if(end($id_cabang) != $id_cabang[$i]) {
                        $cabang .= ' OR';
                    } else {
                        $cabang .= ')';
                    }
                }
            } 
        }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }
        $q = $this->db->query("
            SELECT a.`nip`, a.`pin`, a.`nama`, d.`jab`, e.`cabang`, 
            DATE_FORMAT(a.`tgl_masuk`, '%d %M %Y') AS tgl_masuk, 
            CASE 
                WHEN ISNULL(berkas.nip) THEN 'ON PROGRESS' 
                ELSE 'CLEAR' 
                END AS file_berkas,
            TIMESTAMPDIFF(YEAR, a.tgl_masuk, CURDATE()) AS tahun,
            TIMESTAMPDIFF(MONTH, a.tgl_masuk, CURDATE()) % 12 AS bulan,
            FLOOR(TIMESTAMPDIFF(DAY, a.tgl_masuk, CURDATE()) % 30.4375) AS hari,
            f.departemen, g.divisi, d.sal_pos, a.kary_stat,
            CASE 
                WHEN (a.gend = 'L') THEN 'Laki-laki' 
                WHEN (a.gend = 'P') THEN 'Perempuan'
                ELSE ''
                END AS jk,
            a.ktp, a.agama, a.golongan_darah, a.t_lahir, 
            DATE_FORMAT(a.tgl_lahir, '%d/%m/%Y') AS tgl_lahir,
            TIMESTAMPDIFF(YEAR, a.tgl_lahir, CURDATE()) AS usia_th,
            TIMESTAMPDIFF(MONTH, a.tgl_lahir, CURDATE()) % 12 AS usia_bln,
            a.telp, a.alamat, a.mail, a.mar_stat, a.norek, a.bank, a.norek_an,
            a.tfinfo, h.pend, a.nama_sklh, a.jurusan, a.th_lulus, a.no_ijazah, a.surat_komitmen, a.npwp,
            a.telp_emergency, a.contact_emergency, 
            CASE 
                WHEN (a.ikut_asuransi = 1) THEN 'Ya'
                ELSE 'Tidak'
                END AS ikut_bpjs, DATE_FORMAT(a.tgl_resign, '%d/%m/%Y') AS tgl_resign, a.alasan_resign, a.rehire
            FROM kary a 
            INNER JOIN sk b ON a.`nip` = b.`nip`
            INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            INNER JOIN pos d ON d.`id_pos` = c.`id_pos`
            INNER JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            INNER JOIN ms_departemen f ON d.id_departemen = f.id_dept
            INNER JOIN ms_divisi g ON g.id_divisi = d.id_divisi
            LEFT JOIN pend h ON h.id_pend = a.id_pend
            LEFT JOIN (
                SELECT * FROM tb_pemberkasan 
                WHERE ijazah = 1
                AND kesanggupan = 1
                AND lamaran = 1
                AND ojt = 1
                AND pertanggungjawaban = 1
                AND rekening = 1
                AND upah = 1
            ) AS berkas ON berkas.nip = a.`nip`
            WHERE a.`idp` = '$idp'
            AND b.`aktif` = 1
            AND ISNULL(a.tgl_resign)
            $cabang $kelas $div
            ORDER BY a.`nip` ASC ")->result();
        return $q;
    }

    function view_karyawan_resign($cabang_id = '') 
    {
        $idp = $this->session->userdata('idp');
        
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if ($cabang_id) {
            $cabang .= ' AND b.`id_cab` = '.$cabang_id;
        } else {
           if(!empty($id_cabang)) {
                $cabang .= 'AND (';
                for($i = 0; $i < count($id_cabang); $i++) {
                    $cabang .= ' b.`id_cab` = '.$id_cabang[$i];

                    if(end($id_cabang) != $id_cabang[$i]) {
                        $cabang .= ' OR';
                    } else {
                        $cabang .= ')';
                    }
                }
            } 
        }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " b.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " b.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }
        $q = $this->db->query("
            SELECT a.`nip`, a.`pin`, a.`nama`, b.`jab`, b.`cabang`, 
            DATE_FORMAT(a.`tgl_masuk`, '%d %M %Y') AS tgl_masuk, 
            CASE 
                WHEN ISNULL(berkas.nip) THEN 'ON PROGRESS' 
                ELSE 'CLEAR' 
                END AS file_berkas,
            TIMESTAMPDIFF(YEAR, a.tgl_masuk, a.tgl_resign) AS tahun,
            TIMESTAMPDIFF(MONTH, a.tgl_masuk, a.tgl_resign) % 12 AS bulan,
            FLOOR(TIMESTAMPDIFF(DAY, a.tgl_masuk, a.tgl_resign) % 30.4375) AS hari,
            f.departemen, g.divisi, b.sal_pos, a.kary_stat,
            CASE 
                WHEN (a.gend = 'L') THEN 'Laki-laki' 
                WHEN (a.gend = 'P') THEN 'Perempuan'
                ELSE ''
                END AS jk,
            a.ktp, a.agama, a.golongan_darah, a.t_lahir, 
            DATE_FORMAT(a.tgl_lahir, '%d/%m/%Y') AS tgl_lahir,
            TIMESTAMPDIFF(YEAR, a.tgl_lahir, CURDATE()) AS usia_th,
            TIMESTAMPDIFF(MONTH, a.tgl_lahir, CURDATE()) % 12 AS usia_bln,
            a.telp, a.alamat, a.mail, a.mar_stat, a.norek, a.bank, a.norek_an,
            a.tfinfo, h.pend, a.nama_sklh, a.jurusan, a.th_lulus, a.no_ijazah, a.surat_komitmen, a.npwp,
            a.telp_emergency, a.contact_emergency, 
            CASE 
                WHEN (a.ikut_asuransi = 1) THEN 'Ya'
                ELSE 'Tidak'
                END AS ikut_bpjs, DATE_FORMAT(a.tgl_resign, '%d/%m/%Y') AS tgl_resign, a.alasan_resign, a.rehire
            FROM kary a 
            LEFT JOIN max_sk z ON z.nip = a.nip
            LEFT JOIN posisi_karyawan b ON z.id = b.id_sk
            -- INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            -- INNER JOIN pos d ON d.`id_pos` = c.`id_pos`
            -- INNER JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            LEFT JOIN ms_departemen f ON b.id_dept = f.id_dept
            LEFT JOIN ms_divisi g ON g.id_divisi = b.id_divisi
            LEFT JOIN pend h ON h.id_pend = a.id_pend
            LEFT JOIN (
                SELECT * 
                FROM tb_pemberkasan 
                WHERE ijazah = 1
                AND kesanggupan = 1
                AND lamaran = 1
                AND ojt = 1
                AND pertanggungjawaban = 1
                AND rekening = 1
                AND upah = 1
            ) AS berkas ON berkas.nip = a.`nip`
            WHERE a.`idp` = '$idp'
            AND a.tgl_resign != ''
            $cabang $kelas $div
            ORDER BY a.`nip` ASC ")->result();

        return $q;
    }

    function biodata($nip='')
    {
        return $this->db->query("
            SELECT a.*, b.`pend`, f.`cabang`, h.`divisi`, e.`jab`, e.`sal_pos`,
            g.`departemen`
            FROM kary a
            INNER JOIN pend b ON a.`id_pend` = b.`id_pend`
            INNER JOIN sk c ON c.`nip` = a.`nip`
            INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
            INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
            INNER JOIN ms_departemen g ON g.`id_dept` = e.`id_departemen`
            INNER JOIN ms_divisi h ON h.`id_divisi` = e.`id_divisi`
            WHERE a.`nip` = '$nip'")->row();
    }

    function check_user($username='')
    {
        $db = $this->Main_Model->connect_to_absensi();
        return $db->where('username', $username)
                    ->get('ms_user')
                    ->row();
    }

    function view_user($nip='')
    {
        $db = $this->Main_Model->connect_to_absensi();
        return $db->where('id_employee', $nip)
                    ->get('ms_user')
                    ->row();
    }

    function simpan_user($username='', $nip='', $password='', $nama='')
    {
        $data = array(
            'id_employee' => $nip,
            'username' => $username,
            'password' => md5($password), 
            'status' => 1, 
            'keterangan' => $nama,
            'session_id' => '',
            'session_id_erp' => '',
            'session_id_inventory' => '',
            'session_id_absensi' => '',
            'session_id_mrtg' => '',
            'session_id_crm' => '',
            'session_id_pengajuan' => ''
        );

        $db = $this->Main_Model->connect_to_absensi();
        $db->insert('ms_user', $data);

        return $this->db->affected_rows();
    }

    function update_user($nip='', $password='')
    {
        $data = array(
            'password' => md5($password), 
            'session_id' => '',
            'session_id_erp' => '',
            'session_id_inventory' => '',
            'session_id_absensi' => '',
            'session_id_mrtg' => '',
            'session_id_crm' => '',
            'session_id_pengajuan' => ''
        );

        $db = $this->Main_Model->connect_to_absensi();
        $db->where('id_employee', $nip)->update('ms_user', $data);

        return $this->db->affected_rows();
    }

    function view_imei()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' b.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " b.`jabatan` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " b.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT a.*, b.`nama`
            FROM tb_imei a
            INNER JOIN karyawan b ON a.`nip` = b.`nip`
            WHERE b.`tgl_resign` IS NULL
            $cabang 
            $kelas 
            $div ")->result();
    }

    function generate_nip($prefix='')
    {
        $query = $this->db->query("
            SELECT MAX(SUBSTR(nip, 4, 3)) + 1 AS maks
            FROM kary
            WHERE SUBSTR(nip, 1, 2) = '$prefix'")->row();

        return isset($query->maks) ? $query->maks : 0;
    }

    function dt_karyawan_aktif($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '')
    {
        /* default parameter datatable */
        $start = $this->db->escape_str($start);
        $length = $this->db->escape_str($length);
        $search = $this->db->escape_str($search);
        $column = $this->db->escape_str($column);
        $dir = $this->db->escape_str($dir);

        /* data store to json datatable */
        $data = [];

        $total = $this->q_karyawan_aktif($start, $length, $search, $column, $dir, 'count');
        $request = $this->q_karyawan_aktif($start, $length, $search, $column, $dir, 'view');

        /* special case when view all data */
        if ($length == -1) {
            $length = $this->q_karyawan_aktif($start, $length, $search, $column, $dir, 'count');
        }

        if ($request) {
            $no = $start = 1;
            foreach ($request as $row) {

                $data[] = array(
                    $no++,
                    $row->nip,
                    $row->nama,
                    $row->posisi,
                    $row->cabang,
                    $row->tanggal_masuk,
                    $row->masa_kerja,
                    ''
                );
            }
        }

        return response_datatable($draw, $total, $data);
    }

    function q_karyawan_aktif($draw = 1, $start = 0, $length = 0, $search = '', $column = '', $dir = '', $mode = '')
    {
        /* for searching data */
        $kolom = ['nip', 'nama', 'posisi', 'cabang', 'tanggal_masuk', 'masa_kerja'];
        $condition = search_datatable($kolom, $search);

        /* for ordering data */
        $kolom_order = ['nip', 'nip', 'nama', 'posisi', 'cabang', 'tgl_masuk', 'bulan', 'nip'];
        $order = order_datatable($kolom_order, $column, $dir);

        if ($mode == 'view') {
            $q = $this->db->query("
                    SELECT *
                    FROM karyawan_aktif 
                    WHERE 1 = 1
                    $condition 
                    $order 
                    LIMIT $start, $length ")->result();

            return $q;

        } else {
            $q = $this->db->query("
                    SELECT COUNT(*) AS jumlah
                    FROM karyawan_aktif 
                    WHERE 1 = 1
                    $condition ")->row();

            return isset($q->jumlah) ? $q->jumlah : 0;
        }
    }
}
?>