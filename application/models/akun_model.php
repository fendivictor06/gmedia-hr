<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Akun_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function jumlah_telat()
    {
        $tgl = date('Y-m-d');
        $cabang = $this->session->userdata('cabang');
        return $this->db->query("select count(a.`scan_date`) jumlah from tb_scanlog a join kary b on a.`pin`=b.`pin` join sk c on b.`nip`=c.`nip` join pos_sto d on c.`id_pos_sto`=d.`id_sto` join pos e on d.`id_pos`=e.`id_pos` where e.`id_cabang`='$cabang' and c.`aktif`='1' and date_format(a.`scan_date`,'%Y-%m-%d') = '$tgl'and date_format(a.`scan_date`,'%H:%i:%s') > '08:00:59'")->row();
    }

    function jumlah_absen()
    {
        $tgl = date('Y-m-d');
        $cabang = $this->session->userdata('cabang');
        return $this->db->query("SELECT COUNT(DISTINCT(a.`pin`)) jumlah FROM tb_scanlog a JOIN kary b ON a.`pin` = b.`pin` JOIN sk c ON b.`nip` = c.`nip` JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto` JOIN pos e ON d.`id_pos` = e.`id_pos` WHERE e.`id_cabang` = '$cabang' AND c.`aktif` = '1' AND DATE_FORMAT(a.`scan_date`,'%Y-%m-%d') = '$tgl'")->row();
    }

    function jumlah_mangkir()
    {
        $tgl = date('Y-m-d', strtotime("-1 day", strtotime(date('Y-m-d'))));
        $cabang = $this->session->userdata('cabang');
        return $this->db->query("select count(distinct(a.`pin`)) jumlah from presensi a join kary b on a.`pin` = b.`pin` join sk c on b.`nip` = c.`nip` join pos_sto d on c.`id_pos_sto` = d.`id_sto` join pos e on d.`id_pos` = e.`id_pos` where e.`id_cabang` = '$cabang' and c.`aktif` = '1' and a.`tgl` = '$tgl' AND a.`mangkir` = '1'")->row();
    }

    function view_sudah_absen($id_cabang = '')
    {
        $date = date('Y-m-d');
        $batas = $date.' 12:00:00';
        return $this->db->query("SELECT a.*,b.`nama`,f.`cabang`,date_FORMAT(a.`scan_date`,'%d %M %Y %H:%i') waktu,b.`nip` FROM tb_scanlog a JOIN kary b ON a.`pin` = b.`pin` JOIN sk c ON b.`nip` = c.`nip` JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto` JOIN pos e ON d.`id_pos` = e.`id_pos` join ms_cabang f on e.`id_cabang`=f.`id_cab` WHERE e.`id_cabang` = '$id_cabang'AND c.`aktif` = '1' AND DATE_FORMAT(a.`scan_date`,'%Y-%m-%d') = '$date' and a.`scan_date` < '$batas' group by a.`pin`")->result();
    }

    function view_mangkir($cabang = '')
    {
  //    $id_cabang = $this->Main_Model->session_cabang();
        // $cabang = '';
        // if(!empty($id_cabang)) {
        //  $cabang .= ' (';
        //  for($i = 0; $i < count($id_cabang); $i++) {
        //      $cabang .= " e.`id_cabang` = '$id_cabang[$i]'";

        //      if(end($id_cabang) != $id_cabang[$i]) {
        //          $cabang .= ' OR';
        //      } else {
        //          $cabang .= ')';
        //      }
        //  }
        // }

        // $sal_pos = $this->Main_Model->class_approval();
        // $kelas = '';
        // if(!empty($sal_pos)) {
        //  $kelas .= ' AND (';
        //  for($i = 0; $i < count($sal_pos); $i++) {
        //      $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

        //      if(end($sal_pos) != $sal_pos[$i]) {
        //          $kelas .= ' OR';
        //      } else {
        //          $kelas .= ')';
        //      }
        //  }
        // }

        // $divisi = $this->Main_Model->session_divisi();
        // $div = '';
        // if(!empty($divisi)) {
        //  $div .= ' AND (';
        //  for($i = 0; $i < count($divisi); $i++) {
        //      $div .= " e.`id_divisi` = '$divisi[$i]'";

        //      if(end($divisi) != $divisi[$i]) {
        //          $div .= ' OR';
        //      } else {
        //          $div .= ')';
        //      }
    //      }
        // }

        $date = date('Y-m-d', strtotime("-1 day", strtotime(date('Y-m-d'))));
        $usr_name = $this->session->userdata('username');
        $user = $this->db->where('user', $usr_name)
                    ->get('ms_reminder')
                    ->row();
        $nip = isset($user->nip) ? $user->nip : '';

        return $this->db->query("
    		SELECT a.*,b.`nama`,f.`cabang` 
    		FROM presensi a 
    		JOIN kary b ON a.`nip` = b.`nip` 
    		JOIN sk c ON b.`nip` = c.`nip` 
    		JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto` 
    		JOIN pos e ON d.`id_pos` = e.`id_pos` 
    		JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab` 
    		JOIN ms_rule_detail g ON g.`nip` = a.`nip`
    		JOIN ms_rule_approval h ON h.`pola` = g.`pola`
    		WHERE c.`aktif` = '1' 
    		AND a.`tgl` = '$date' 
    		AND a.`mangkir` = '1'
    		AND a.flag = 0
    		AND h.penyetuju = '$nip'
    		GROUP BY a.`id`")->result();

        // return $this->db->query("
        //  select a.*,b.`nama`,f.`cabang`
        //  from presensi a
        //  join kary b on a.`pin` = b.`pin`
        //  join sk c on b.`nip` = c.`nip`
        //  join pos_sto d on c.`id_pos_sto` = d.`id_sto`
        //  join pos e on d.`id_pos` = e.`id_pos`
        //  join ms_cabang f on e.`id_cabang`=f.`id_cab`
        //  where $cabang
        //  and c.`aktif` = '1'
        //  and a.`tgl` >= '$date'
        //  AND a.`mangkir` = '1'
        //  AND a.flag = 0
        //  $kelas
        //  $div
        //  ")->result();
    }

    function view_perdin()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if ($id_cabang != '') {
            $cabang .= ' (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' a.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }
        return $this->db->query("
    		SELECT a.*, date_format(a.`tgl_brgkt`, '%d %M %Y %H:%i') berangkat, 
    		date_format(a.`tgl_pulang`, '%d %M %Y %H:%i') pulang 
    		FROM perdin a 
    		WHERE $cabang
    		ORDER BY a.`tgl_brgkt` DESC")->result();
    }

    function detail_perdin($nobukti = '')
    {
        return $this->db->query("SELECT a.*,b.`nama` FROM perdin_det a JOIN kary b ON a.`nip` = b.`nip` WHERE a.`nobukti` = '$nobukti'")->result();
    }
}

/* End of file akun_model.php */
/* Location: ./application/models/akun_model.php */
