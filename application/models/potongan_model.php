<?php

class Potongan_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_piutang()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
                SELECT a.`id_pot`, b.`nip`, b.`nama`, a.`tot_pot`, a.`ket`,
                (a.`tot_pot` - IFNULL(detail.total_cicilan, 0)) AS sisa,
                IFNULL(cicilan.jml_cicilan, 0) AS jml_cicilan,
                a.`cicilan`
                    FROM pot a 
                    JOIN kary b ON a.`nip` = b.`nip`
                    LEFT JOIN (
                SELECT SUM(ags) AS total_cicilan, id_pot
                FROM pot_det
                    ) AS detail ON detail.id_pot = a.`id_pot`
                    LEFT JOIN (
                SELECT COUNT(*) AS jml_cicilan, id_pot
                FROM pot_det
                    ) AS cicilan ON cicilan.id_pot = a.`id_pot`
                    WHERE b.`idp` = '$idp' 
                    ORDER BY a.`id_pot` DESC")->result();
    }

    function piutang_id($id = '')
    {
        return $this->db->query("
            SELECT *, DATE_FORMAT(a.`tgl`, '%d/%m/%Y') AS tanggal
            FROM pot a
            WHERE a.`id_pot` = '$id'")->row();
    }

    function delete_piutang($id)
    {
        $this->db->where('id_pot', $id)->delete('pot');
    }

    function add_cicil($data)
    {
        $this->db->insert('pot_det', $data);
    }

    function view_jabatan()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("SELECT b.`nip`,b.`nama`,a.`stat_jab` FROM sk  a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`stat_jab`!='FULL' AND a.`stat_jab`!='' AND a.`aktif`='1' AND ISNULL(b.`tgl_resign`) AND b.`idp` = '$idp' ORDER BY b.`nama` ASC")->result();
    }

    function view_lainnya($periode)
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("SELECT a.`id`,c.`nip`,c.`nama`,DATE_FORMAT(b.`tgl_akhir`,'%M %Y') AS periode,a.`jumlah`,a.`keterangan` FROM pot_lain a LEFT JOIN q_det b ON a.`id_per`=b.`qd_id` LEFT JOIN kary c ON a.`nip`=c.`nip`  WHERE b.`qd_id`='$periode'  AND c.`idp`='$idp' ORDER BY a.`id` DESC")->result();
    }

    function lainnya_id($id)
    {
        return $this->db->where('id', $id)->from('pot_lain')->get()->row();
    }

    function delete_lainnya($id)
    {
        $this->db->where('id', $id)->delete('pot_lain');
    }

    function view_cicilan($id = '')
    {
        return $this->db->query("
            SELECT a.`id_pot_det`,a.`id_pot`,DATE_FORMAT(a.`tgl`,'%Y-%m-%d') tgl, a.`ags`,a.`inc_payroll`,a.`ket`
            FROM pot_det a 
            WHERE a.`id_pot`='$id' 
            ORDER BY a.`id_pot_det` ASC")->result();
    }

    function delete_cicilan($id)
    {
        $this->db->where('id_pot_det', $id)->delete('pot_det');
    }

    function process_piutang($data, $id = '')
    {
        if ($id=="") {
            $this->db->insert('pot', $data);
        } else {
            $this->db->where('id_pot', $id)->update('pot', $data);
        }
    }

    function process_lainnya($data, $id = '')
    {
        if ($id=="") {
            $this->db->insert('pot_lain', $data);
        } else {
            $this->db->where('id', $id)->update('pot_lain', $data);
        }
    }

    function view_asuransi()
    {
        return $this->db->query("
            SELECT a.*, b.nama
            FROM tb_asuransi a
            INNER JOIN kary b ON a.nip = b.nip")->result();
    }
}
