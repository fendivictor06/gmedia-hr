<?php

class Achievement_Model extends CI_Model {

    function __construct(){
        parent::__construct();

    }

   function view_achievement($tgl_akhir, $tgl_awal){
        $idp = $this->session->userdata('idp');
        // return $this->db->query("SELECT a.`id_ach`,DATE_FORMAT(c.`tgl_akhir`,'%M %Y') AS bln,a.`nip`,b.`nama`,a.`ach`,a.`perf` FROM ach a LEFT JOIN kary b ON a.`nip`=b.`nip` LEFT JOIN q_det c ON a.`id_per`=c.`qd_id` WHERE a.`id_per`='$periode' ORDER BY a.`id_ach` DESC")->result();
        return $this->db->query("SELECT * FROM sk a LEFT JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` LEFT JOIN kary c ON a.`nip`=c.`nip` LEFT JOIN pos d ON b.`id_pos`=d.`id_pos` WHERE a.`aktif`='1' AND d.`sns`='NON-STAFF' AND ((c.`tgl_resign`>'$tgl_awal' AND c.`tgl_resign`<'$tgl_akhir') OR ISNULL(c.`tgl_resign`)) AND c.`tgl_masuk`<'$tgl_akhir'  AND c.`idp` = '$idp' ORDER BY c.`nama` ASC")->result();
   }

   function cari_ach($nip){
   		$sal_pos=$this->db->query("SELECT d.`nip`,d.`nama`,a.`posisi`,a.`jab`,a.`sal_pos` FROM pos a JOIN pos_sto b ON a.`id_pos`=b.`id_pos` JOIN sk c ON b.`id_sto`=c.`id_pos_sto` LEFT JOIN kary d ON c.`nip`=d.`nip` WHERE a.`sns`='NON-STAFF' AND ISNULL(d.`tgl_resign`) AND c.`aktif`='1' AND d.`nip`='$nip'")->row();
   		return $this->db->where('sal_pos',$sal_pos->sal_pos)->from('sal_nonstaff_class')->get()->result();
   }

   function add_achievement($data){
      $this->db->insert('ach_temp',$data);
      $this->call_ach();
   }

   function achievement_id($nip,$id_per){
      // return $this->db->where('id_ach',$id_ach)->from('ach')->get()->row();
      return $this->db->query("SELECT DATE_FORMAT(c.`tgl_akhir`,'%M %Y') AS bln,a.`nip`,a.`nama`,b.`id_per`,b.`ach` FROM kary a LEFT JOIN ach b ON a.`nip`=b.`nip` LEFT JOIN q_det c ON b.`id_per`=c.`qd_id` WHERE b.`id_per`='$id_per' AND a.`nip`='$nip'")->row();
   }

   function delete_achievement($nip,$id_per){
      $this->db->where('nip',$nip)->where('id_per',$id_per)->delete('ach');
   }

   function call_ach(){
      $this->db->query("CALL insert_ach()");
   }

   function download_list($id_per, $id_lwok)
   {
      $idp       = $this->session->userdata('idp');
      $q         = $this->db->query("select * from q_det where qd_id = '$id_per'")->row();
      $tgl_awal  = isset($q->tgl_awal)?$q->tgl_awal:'';
      $tgl_akhir = isset($q->tgl_akhir)?$q->tgl_akhir:'';

      return $this->db->query("SELECT * FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto` JOIN pos d ON c.`id_pos`=d.`id_pos` LEFT JOIN lku e ON c.`id_lku`=e.`id_lku` LEFT JOIN lwok f ON e.`id_lwok`=f.`id_lwok` WHERE b.`aktif`='1' AND ((a.`tgl_resign` > '$tgl_awal' AND a.`tgl_resign` < '$tgl_akhir') OR ISNULL(a.`tgl_resign`)) AND a.`idp`='$idp' AND f.`id_lwok`='$id_lwok'AND d.`sns` = 'NON-STAFF'")->result();
   }
}
?>