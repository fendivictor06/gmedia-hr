<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Terlambat_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function view_terlambat($cabang = '')
	{
		// $id_cabang = $this->Main_Model->session_cabang();
		// $cabang = '';
		// if(!empty($id_cabang)) {
		// 	$cabang .= 'AND (';
		// 	for($i = 0; $i < count($id_cabang); $i++) {
		// 		$cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

		// 		if(end($id_cabang) != $id_cabang[$i]) {
		// 			$cabang .= ' OR';
		// 		} else {
		// 			$cabang .= ')';
		// 		}
		// 	}
		// }

		// $sal_pos = $this->Main_Model->class_approval();
		// $kelas = '';
		// if(!empty($sal_pos)) {
		// 	$kelas .= ' AND (';
		// 	for($i = 0; $i < count($sal_pos); $i++) {
		// 		$kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

		// 		if(end($sal_pos) != $sal_pos[$i]) {
		// 			$kelas .= ' OR';
		// 		} else {
		// 			$kelas .= ')';
		// 		}
		// 	}
		// }

		// $divisi = $this->Main_Model->session_divisi();
  //       $div = '';
  //       if(!empty($divisi)) {
  //           $div .= ' AND (';
  //           for($i = 0; $i < count($divisi); $i++) {
  //               $div .= " e.`id_divisi` = '$divisi[$i]'";

  //               if(end($divisi) != $divisi[$i]) {
  //                   $div .= ' OR';
  //               } else {
  //                   $div .= ')';
  //               }
  //           }
  //       } 

		$date = date('Y-m-d', strtotime("-1 day", strtotime(date('Y-m-d'))));
		$usr_name = $this->session->userdata('username');
    	$user = $this->db->where('user', $usr_name)
    				->get('ms_reminder')
    				->row();
    	$nip = isset($user->nip) ? $user->nip : '';

    	return $this->db->query("			
			SELECT a.*,b.`nama`,f.`cabang` 
			FROM terlambat_temp a 
			JOIN kary b ON a.`nip` = b.`nip`
			JOIN sk c ON b.`nip` = c.`nip`
			JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
			JOIN pos e ON e.`id_pos` = d.`id_pos`
			JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
			JOIN ms_rule_detail g ON g.`nip` = a.`nip`
			JOIN ms_rule_approval h ON h.`pola` = g.`pola`
			WHERE a.`tgl` = '$date' 
			AND c.`aktif` = '1' 
			AND h.`penyetuju` = '$nip'
			AND NOT EXISTS (
				SELECT NULL 
				FROM tb_klarifikasi_terlambat
				WHERE id_terlambat = a.id
			)
			GROUP BY a.`id`")->result();
		// return $this->db->query("
		// 	SELECT a.*,b.`nama`,f.`cabang` 
		// 	FROM terlambat_temp a 
		// 	JOIN kary b ON a.`nip` = b.`nip`
		// 	JOIN sk c ON b.`nip` = c.`nip`
		// 	JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
		// 	JOIN pos e ON e.`id_pos` = d.`id_pos`
		// 	JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
		// 	WHERE a.`tgl` >= '$date' $cabang 
		// 	AND c.`aktif` = '1' 
		// 	AND NOT EXISTS (
		// 		SELECT NULL 
		// 		FROM tb_klarifikasi_terlambat
		// 		WHERE id_terlambat = a.id
		// 	)
		// 	$kelas $div  ")->result();
	}

	function view_terlambat_klarifikasi($id_periode='')
	{
		$periode = $this->Main_Model->id_periode($id_periode);
        $date_start = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $date_end = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';

		return $this->db->query("
			SELECT a.*,b.`nama`,f.`cabang` 
			FROM terlambat_temp a 
			JOIN kary b ON a.`nip` = b.`nip`
			JOIN sk c ON b.`nip` = c.`nip`
			JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
			JOIN pos e ON e.`id_pos` = d.`id_pos`
			JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
			WHERE a.`tgl` BETWEEN '$date_start' 
			AND '$date_end' 
			AND c.`aktif` = '1'
			AND NOT EXISTS (
				SELECT NULL 
				FROM tb_klarifikasi_terlambat
				WHERE id_terlambat = a.id
			)
			ORDER BY a.`tgl` DESC ")->result();
	}

}

/* End of file terlambat_model.php */
/* Location: ./application/models/terlambat_model.php */ ?>