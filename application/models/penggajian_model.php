<?php

class Penggajian_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', TRUE);
    }
    
    function option_band()
    {
        $option = $this->db->from('band')->get()->result();
        foreach ($option as $row) {
            $result[$row->id_band] = $row->band;
        }
        return $result;
    }
    
    function perusahaan_id($id)
    {
        return $this->db->where('idp', $id)->from('idp')->get();
    }
    
    function staff_rule($id)
    {
        // return $this->db->from('sal_staff_class')->where('sal_staff_class.idp', $id)->join('idp', 'sal_staff_class.idp=idp.idp')->join('band', 'sal_staff_class.id_band=band.id_band')->get()->result();
        return $this->db->where('idp',$id)->where('status',1)->order_by('sal_pos','asc')->get('sal_staff_class')->result();
    }

    function staff_rule_process($data,$id_ssc='')
    {
        if($id_ssc=='')
        {
            $this->db->insert('sal_staff_class', $data);
        }
        else
        {
            $this->db->where('id_ssc', $id_ssc)->update('sal_staff_class', $data);
        }
    }
    
    function staff_rule_id($id)
    {
        return $this->db->where('id_ssc', $id)->from('sal_staff_class')->join('idp', 'sal_staff_class.idp=idp.idp')->get()->row();
    }
    
    function delete_rule_staff($id_ssc)
    {
        $this->db->where('id_ssc', $id_ssc)->delete('sal_staff_class');
    }
    
    function non_staff_rule($id)
    {
        return $this->db->where('idp', $id)->from('sal_nonstaff_class')->get()->result();
    }
    
    function nonstaff_rule_id($id_nsc)
    {
        return $this->db->where('id_nsc', $id_nsc)->from('sal_nonstaff_class')->get()->row();
    }

    function nonstaff_rule_process($data,$id_nsc)
    {
        if($id_nsc=="")
        {
            $this->db->insert('sal_nonstaff_class', $data);
        }
        else
        {
            $this->db->where('id_nsc', $id_nsc)->update('sal_nonstaff_class', $data);
        }
    }
    
    function delete_rule_nonstaff($id_nsc)
    {
        $this->db->where('id_nsc', $id_nsc)->delete('sal_nonstaff_class');
    }
    
    function view_hari_libur()
    {
        return $this->db->from('hol')->order_by('hol_tgl','desc')->get()->result();
    }

    function view_periode_gaji()
    {
        return $this->db->from('q_det')->order_by('tgl_awal', 'desc')->get()->result();
    }
    
    function add_hari_libur($data)
    {
        $this->db->insert('hol', $data);
    }
    
    function hari_libur_id($id)
    {
        return $this->db->where('id', $id)->from('hol')->get()->row();
    }
    
    function update_hari_libur($data, $id)
    {
        $this->db->where('id', $id)->update('hol', $data);
    }
    
    function delete_hari_libur($id)
    {
        $this->db->where('id', $id)->delete('hol');
    }
    
    function view_faktor_kemahalan()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
            SELECT * FROM fk a 
            JOIN lku b ON a.`id_lku`=b.`id_lku` 
            ORDER BY a.`id_fk`")->result();
    }

    function view_bpjs()
    {
        return $this->db->from('bpjs')->order_by('th','desc')->get()->row();
    }

    function view_bpjs_umk($th='')
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
            SELECT * FROM fk a 
            JOIN lku b ON a.`id_lku`=b.`id_lku` 
            WHERE a.`th`='$th'")->result();
    }

    function bpjsks_id($id)
    {
        return $this->db->where('id_bpjsks',$id)->from('bpjs_ks')->get()->row();
    }

    function add_bpjs($data)
    {
        $this->db->insert('bpjs',$data);
    }

    function bpjs_id($id)
    {
        return $this->db->where('id_bpjs',$id)->from('bpjs')->get()->row();
    }

    function update_bpjs($data,$id)
    {
        $this->db->where('id_bpjs',$id)->update('bpjs',$data);
    }

    function quartal()
    {
        $opt = $this->db->from('q')->order_by('q_id','asc')->get()->result();
        foreach ($opt as $row) {
            $result[$row->q_id] = $row->q_name;
        }
            $result[0]  = '-';
        return $result;
    }

    function process_q($data,$id='')
    {
        if($id=="")
        {
            $this->db->insert('q_det',$data);
        }
        else
        {
            $this->db->where('qd_id',$id)->update('q_det',$data);
        }
    }

    function q_det_id($id)
    {
        return $this->db->query("SELECT a.`qd_id`,a.`bln`,DATE_FORMAT(a.`tgl_awal`,'%d/%m/%Y') tgl_awal,DATE_FORMAT(a.`tgl_akhir`,'%d/%m/%Y') tgl_akhir, a.`q_id`,a.`q_id2` FROM q_det a WHERE a.`qd_id` = '$id'")->row();
    }

    function delete_qdet($id)
    {
        $this->db->where('qd_id',$id)->delete('q_det');
    }

    function view_lku()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
            SELECT * 
            FROM lku a")->result();
    }

    function add_fk($data)
    {
        $this->db->insert('fk',$data);
    }

    function fk_id($id)
    {
        return $this->db->query("SELECT * FROM fk a JOIN lku b ON a.`id_lku`=b.`id_lku` WHERE a.`id_fk`='$id'")->row();
    }

    function update_fk($data,$id_fk)
    {
        $this->db->where('id_fk',$id_fk)->update('fk',$data);
    }

    function delete_fk($id)
    {
        $this->db->where('id_fk',$id)->delete('fk');
    }

    function view_quartal()
    {
        return $this->db->from('q')->order_by('q_id','ASC')->get()->result();
    }

    function add_quartal($data)
    {
        $this->db->insert('q',$data);
    }

    function q_id($id)
    {
        return $this->db->where('q_id',$id)->from('q')->get()->row();
    }

    function update_quartal($data,$id)
    {
        $this->db->where('q_id',$id)->update('q',$data);
    }

    function delete_q($id)
    {
        $this->db->where('q_id',$id)->delete('q');
    }

    function view_bpjs_ks()
    {
        $idp = $this->session->userdata('idp');
        $per = $this->Main_Model->per_skr();
        // return $this->db->query("SELECT * FROM kary a WHERE a.`ikut_asuransi`='1' AND a.`idp`='$idp'")->result();
        return $this->db->query("SELECT * FROM kary a WHERE a.`ikut_asuransi`='1' AND a.`idp`='1' AND (ISNULL(a.`tgl_resign`) OR (a.`tgl_resign` BETWEEN '$per->tgl_awal' AND '$per->tgl_akhir'))")->result();
    }

    function nip_id($id)
    {
        return $this->db->query("SELECT * FROM kary a WHERE a.`nip` ='$id'")->row();
    }

    function savetoks($data)
    {
        $this->db->insert('bpjs_ks_det',$data);
    }

    function bpjs_det_id($id)
    {
        return $this->db->query("SELECT *,DATE_FORMAT(a.`tgl_lahir`,'%d/%m/%Y') tgl FROM bpjs_ks_det a WHERE a.`id_bpjs_det`='$id'")->row();
    }

    function update_ks_det($data,$id)
    {
        $this->db->where('id_bpjs_det',$id)->update('bpjs_ks_det',$data);
    }

    function delete_det_ks($id)
    {
        $this->db->where('id_bpjs_det',$id)->delete('bpjs_ks_det');
    }

    function bpjs_ks_nip($nip)
    {
        return $this->db->query("SELECT * FROM bpjs_ks_det a WHERE a.`nip` = '$nip' LIMIT 1")->row();
    }

    function updatekk($data,$nip)
    {
        $this->db->where('nip',$nip)->update('bpjs_ks_det',$data);
    }

    function view_ms_bbm()
    {
        return $this->db->query("SELECT * FROM ms_bbmp a WHERE a.`tipe`='bbm'")->result();
    } 

    function view_ms_bbp()
    {
        return $this->db->query("SELECT * FROM ms_bbmp a WHERE a.`tipe`='bbp'")->result();
    }  

    function bbmp_id($id)
    {
        return $this->db->query("SELECT * FROM ms_bbmp a WHERE a.`id_bbmp`='$id'")->row();
    }

    function edit_bbmp($data,$id)
    {
        $this->db->where('id_bbmp',$id)->update('ms_bbmp',$data);
    }

    function process_bpjs_ks($data,$id)
    {
        if(empty($id))
        {
            $this->db->insert('bpjs_ks_det',$data);
        }
        else
        {
            $this->db->where('id_bpjs_det',$id)->update('bpjs_ks_det',$data);
        }
    }

    function view_aturan_bpjs_ks()
    {
        $th  = $this->Main_Model->th_bpjs_ks();
        return $this->db->query("SELECT * FROM bpjs_ks WHERE th = '$th->th' LIMIT 1")->row();
    }

    function bpjsks_process($data,$id)
    {
        $this->db->where('id_bpjsks',$id)->update('bpjs_ks',$data);
    }

    function opt_bbmp()
    {
        $idp    = $this->session->userdata('idp');
        $data   = $this->db->query("SELECT DISTINCT(b.`sal_pos`) sal_pos FROM pos b WHERE b.`idp`='$idp' ORDER BY b.`sal_pos`")->result();
        foreach ($data as $row) {
            $result[$row->sal_pos] = $row->sal_pos;
        }
        return $result;
    }

    function process_msbbmp($data,$id)
    {
        if($id=="")
        {
            $this->db->insert('ms_bbmp',$data);
        }
        else
        {
            $this->db->where('id_bbmp',$id)->update('ms_bbmp',$data);
        }
    }

    function delete_msbbmp($id)
    {
        $this->db->where('id_bbmp',$id)->delete('ms_bbmp');
    }

    function download_bpjsks($qd_id='')
    {
        $per = $this->Main_Model->id_periode($qd_id);
        return $this->db->query("SELECT * FROM kary a JOIN bpjs_ks_det b ON a.`nip`=b.`nip` WHERE (ISNULL(a.`tgl_resign`) OR a.`tgl_resign` BETWEEN '$per->tgl_awal' AND '$per->tgl_akhir') AND a.`tgl_masuk` <= '$per->tgl_awal' AND a.`ikut_asuransi` = '1'")->result();
    }

    function download_bpjstk($qd_id='')
    {
        $per = $this->Main_Model->id_periode($qd_id);
        return $this->db->query("SELECT * FROM kary a WHERE (ISNULL(a.`tgl_resign`) OR a.`tgl_resign` BETWEEN '$per->tgl_awal' AND '$per->tgl_akhir') AND a.`tgl_masuk` <= '$per->tgl_awal'")->result();
    }

        function view_ms_perdin($id='')
    {
        if($id==1)
        {
            $tipe = 'uangsaku';
        }
        else
        {
            $tipe = 'akomodasi';
        }

        return $this->db->query("SELECT * FROM ms_perdin a WHERE a.`tipe`='$tipe' AND a.`status`='1'")->result();
    }
    function process_msperdin($data,$id)
    {
        if($id=="")
        {
            $this->db->insert('ms_perdin',$data);
        }
        else
        {
            $this->db->where('id_ms_perdin',$id)->update('ms_perdin',$data);
        }
    }

    function msperdin_id($id)
    {
        return $this->db->query("SELECT * FROM ms_perdin a WHERE a.`id_ms_perdin`='$id'")->row();
    }
    function delete_msperdin($id,$data)
    {
        $this->db->where('id_ms_perdin',$id)->update('ms_perdin',$data);
    }

    function bpjs_ketenagakerjaan($id_cabang=0)
    {
        $condition = '';
        if ($id_cabang > 0) {
            $condition = " AND surat.id_cabang = '$id_cabang' ";
        }

        return $this->db->query("
            SELECT a.`nama`, a.`salary` AS nominal, 
            (a.`salary` * (0.24/100)) AS jkkp,
            0 AS jkkk, 
            (a.`salary` * (0.30/100)) AS jkmp, 
            0 AS jkmk,
            (a.`salary` * (3.70/100)) AS jhtp, 
            (a.`salary` * (2.00/100)) AS jhtk,
            (a.`salary` * (2.00/100)) AS jpp, 
            (a.`salary` * (1.00/100)) AS jpk,
            ((a.`salary` * (0.24/100)) + 
                (a.`salary` * (0.30/100)) + 
                (a.`salary` * (3.70/100)) + 
                (a.`salary` * (2.00/100))) AS total_p,
            ((a.`salary` * (2.00/100)) + 
                (a.`salary` * (1.00/100))) AS total_k
            FROM kary a
            LEFT JOIN (
                SELECT MAX(id_sk), ms_cabang.cabang, sk.nip, pos.jab, 
                pos.id_cabang, ms_divisi.divisi, ms_departemen.departemen,
                pos.sal_pos
                FROM sk
                INNER JOIN pos_sto ON sk.id_pos_sto = pos_sto.id_sto
                INNER JOIN pos ON pos.id_pos = pos_sto.id_pos
                INNER JOIN ms_cabang ON ms_cabang.id_cab = pos.id_cabang
                INNER JOIN ms_divisi ON ms_divisi.id_divisi = pos.id_divisi
                INNER JOIN ms_departemen ON ms_departemen.id_dept = pos.id_departemen
                GROUP BY sk.nip
            ) AS surat ON surat.nip = a.nip
            WHERE a.`tgl_resign` IS NULL
            $condition ")->result();
    }
}
?>