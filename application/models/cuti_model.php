<?php

class Cuti_Model extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

    function view_cutibiasa($id_periode = '')
    {
        $idp = $this->session->userdata('idp');
        $periode = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $tgl_akhir = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';

        return $this->db->query("SELECT f.`id_cuti_det`, a.`nip`, a.`nama`, e.`cabang`, f.`alasan_cuti`, f.`alamat_cuti`, f.`no_hp`, h.`tipe`
            ,GROUP_CONCAT(DATE_FORMAT(g.`tgl`, '%d %M %Y') SEPARATOR ' <br> ') tgl, i.`keterangan`, i.`flag`,
            f.`approval`
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            JOIN cuti_det f ON f.`nip` = a.`nip`
            JOIN cuti_sub_det g ON g.`id_cuti_det` = f.`id_cuti_det`
            JOIN ms_tipe_cuti h ON h.`id` = f.`id_tipe`
            JOIN ms_status_approval i ON i.`kode` = f.`approval`
            WHERE ISNULL(a.`tgl_resign`)
            AND b.`aktif` = '1'
            AND g.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
            AND a.`idp` = '$idp'
            GROUP BY f.`id_cuti_det`
            ORDER BY g.`tgl` DESC")->result();
    }

    function view_cutimelahirkan()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
            SELECT a.*, b.nama, g.tipe, h.flag, h.keterangan 
            FROM cuti_khusus a 
            JOIN kary b ON a.`nip` = b.`nip` 
            JOIN tipe_cuti_khusus g ON a.`id_tipe_cuti` = g.`id_tipe` 
            JOIN ms_status_approval h ON h.kode = a.approval
            WHERE b.`idp` = '1' 
            ORDER BY a.`tgl_awal` DESC")->result();
    }

    function get_id_cutimelahirkan($id='')
    {
        return $this->db->query("
            SELECT *,DATE_FORMAT(a.`tgl_awal`,'%d/%m/%Y') tglawal,
            DATE_FORMAT(a.`tgl_akhir`,'%d/%m/%Y') tglakhir 
            FROM cuti_khusus a 
            WHERE a.`id`='$id'")->row();
    }

    function process_cuti_khusus($data='', $id='')
    {
        if($id=="") {
            $this->db->insert('cuti_khusus',$data);
        } else {
            $this->db->where('id',$id)->update('cuti_khusus',$data);
        }
    }

    function delete_cutimelahirkan($id)
    {
		$this->db->where('id',$id)->delete('cuti_khusus');    	
    }

    function th_cutibiasa()
    {
        // $m      = date("m");
        // if($m<=4)
        // {
        //     $opt    = $this->db->query("SELECT distinct(th) AS th FROM cuti_dep ORDER BY th DESC LIMIT 2")->result();
        // }
        // else
        // {
            $opt = $this->db->query("SELECT distinct(th) AS th FROM cuti_dep ORDER BY th DESC LIMIT 1")->result();
        // }
        foreach ($opt as $row) {
            $result[$row->th] = $row->th;
        }
        return $result;
    }

    function cek_pengajuan_cuti($nip='')
    {
        return $this->db->query("
            SELECT *
            FROM cuti_det a
            JOIN cuti_sub_det b ON a.id_cuti_det = b.id_cuti_det
            JOIN ms_status_approval c ON c.kode = a.approval 
            WHERE c.flag = 1 
            ")->num_rows();
    }

    function cek_kuotacuti($nip='', $th='')
    {
        return $this->db->query("
            SELECT MAX(tgl_awal) AS tgl_awal, qt
            FROM cuti_dep
            WHERE nip = '$nip'
            AND qt > 0")->row();
    }

    function add_cutibiasa($data)
    {
        $this->db->insert('cuti_det',$data);
        // $this->db->insert('cuti_sub_det',$data_tgl);
    }

    function add_subdet_cuti($data_tgl='')
    {
        $this->db->insert('cuti_sub_det',$data_tgl);
    }

    function delete_cutibiasa($id='')
    {
        $this->db->where('id_cuti_det', $id)->delete('cuti_det');
        $this->db->where('id_cuti_det', $id)->delete('cuti_sub_det');
    }

    function get_id_cutibiasa($id='')
    {
        return $this->db->query("
            SELECT a.`id_cuti_det`, a.`nip`, a.`no_hp`, a.`alasan_cuti`, a.`alamat_cuti`, a.`cuti_dep`, 
            DATE_FORMAT(b.`tgl`,'%d/%m/%Y') tgl,b.`id`, YEAR(b.`tgl`) AS th, a.`id_tipe`, a.pola 
            FROM cuti_det a 
            JOIN cuti_sub_det b ON a.`id_cuti_det`=b.`id_cuti_det` 
            LEFT JOIN cuti_dep c ON a.`cuti_dep`=c.`id_cuti` 
            WHERE a.`id_cuti_det`='$id' 
            ORDER BY b.`tgl` ASC")->result();
    }

    function update_cutibiasa($data,$id)
    {
        $this->db->where('id_cuti_det',$id)->update('cuti_det',$data);
        // $this->db->where('id_cuti_det',$id)->update('cuti_sub_det',$data_tgl);
    }

    function tipe_cuti()
    {
        $opt = $this->db->query("select * from tipe_cuti_khusus")->result();
        foreach ($opt as $row) {
            $result[$row->id_tipe] = $row->tipe;
        }

        return $result;
    }

    function update_qty($id_cuti_dep)
    {
        $this->db->query("UPDATE cuti_dep SET qt=qt-1 WHERE id_cuti = '$id_cuti_dep'");
    }

    function update_subdet_cuti($data_tgl,$idtgl)
    {
        $this->db->where('id',$idtgl)->update('cuti_sub_det',$data_tgl);
    }

    function del_detail($id)
    {
        $q = $this->db->query("
            SELECT * FROM cuti_sub_det a 
            JOIN cuti_det b ON a.`id_cuti_det`=b.`id_cuti_det` 
            JOIN cuti_dep c ON c.`id_cuti`=b.`cuti_dep` 
            WHERE a.`id`= '$id'")->row();
        $this->db->query("update cuti_dep set qt=qt+1 where id_cuti='$q->id_cuti'");
        $this->db->where('id',$id)->delete('cuti_sub_det');
    }

    function cek_qt($nip='')
    {
        $date = date('Y-m-d');
        return $this->db->query("
            SELECT a.`qt` FROM cuti_dep a 
            WHERE a.`nip`='$nip' 
            AND '$date' BETWEEN a.tgl_awal AND a.tgl_akhir")->row();
    }

    function view_verifikasi_cuti() 
    {
        $username = $this->session->userdata('username');
        $query = $this->db->where('user', $username)
                    ->get('ms_reminder')
                    ->row();
        $nip_wtf = isset($query->nip) ? $query->nip  : '';

        return $this->db->query("
            SELECT f.`id_cuti_det`, a.`nip`, a.`nama`, e.`cabang`, f.`alasan_cuti`, f.`alamat_cuti`, f.`no_hp`, h.`tipe`
            ,GROUP_CONCAT(DATE_FORMAT(g.`tgl`, '%d %M %Y') SEPARATOR ' <br> ') tgl, i.`keterangan`, i.`flag`
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            JOIN cuti_det f ON f.`nip` = a.`nip`
            JOIN cuti_sub_det g ON g.`id_cuti_det` = f.`id_cuti_det`
            JOIN ms_tipe_cuti h ON h.`id` = f.`id_tipe`
            JOIN ms_status_approval i ON i.`kode` = f.`approval`
            WHERE ISNULL(a.`tgl_resign`)
            AND b.`aktif` = '1'
            AND i.`flag` <> 0
            AND i.`kode` <> 12
            AND f.penyetuju = '$nip_wtf'
            GROUP BY f.`id_cuti_det`
            ORDER BY g.`tgl` DESC")->result();
    }

    function view_verifikasi_cuti_khusus()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' f.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " f.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " f.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $pengajuan = $this->Main_Model->session_pengajuan();
        $is_approval = $this->Main_Model->is_approval();
        $kelas_pengajuan = '';
        if($is_approval == TRUE) {
            if(!empty($pengajuan)) {
                $kelas_pengajuan .= ' AND (';
                for($i = 0; $i < count($pengajuan); $i++) {
                    $kelas_pengajuan .= " g.`privilege` = '$pengajuan[$i]'";

                    if(end($pengajuan) != $pengajuan[$i]) {
                        $kelas_pengajuan .= ' OR';
                    } else {
                        $kelas_pengajuan .= ')';
                    }
                }
            } 
        }

        return $this->db->query("
            SELECT a.`nip`, c.`nama`, b.`tipe`, a.*, g.`flag`, g.`keterangan`
            FROM cuti_khusus a
            INNER JOIN tipe_cuti_khusus b ON a.`id_tipe_cuti` = b.`id_tipe`
            INNER JOIN kary c ON c.`nip` = a.`nip`
            INNER JOIN sk d ON d.`nip` = c.`nip`
            INNER JOIN pos_sto e ON e.`id_sto` = d.`id_pos_sto`
            INNER JOIN pos f ON f.`id_pos` = e.`id_pos`
            INNER JOIN ms_status_approval g ON g.`kode` = a.`approval`
            WHERE d.`aktif` = 1
            AND g.`flag` <> 0
            AND g.`kode` <> 6
            $cabang $kelas $div
            $kelas_pengajuan
            ORDER BY a.tgl_awal")->result();
    }

    function view_tgl_cuti($id_cuti_det='')
    {
        return $this->db->query("SELECT * FROM cuti_sub_det a WHERE a.`id_cuti_det` = '$id_cuti_det'")->result();
    }

    function generate_nomor_cuti()
    {
        return $this->db->query("SELECT MAX(a.`id_cuti_det`) + 1 AS maks FROM cuti_det a")->row();
    }

    function cuti_bulan_ini($tanggal='', $id_cuti_dep='')
    {
        return $this->db->query("
            SELECT COUNT(b.`tgl`) jml 
            FROM cuti_det a 
            JOIN cuti_sub_det b ON a.`id_cuti_det` = b.`id_cuti_det` 
            WHERE a.`cuti_dep` = '$id_cuti_dep' 
            AND DATE_FORMAT(b.`tgl`,'%m %Y') = DATE_FORMAT('$tanggal','%m %Y')")->row();
    }

    function view_cuti_akun($id_periode='')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        } 

        $idp = $this->session->userdata('idp');
        $periode = $this->Main_Model->id_periode($id_periode);
        $tgl_awal = isset($periode->tgl_awal) ? $periode->tgl_awal : '';
        $tgl_akhir = isset($periode->tgl_akhir) ? $periode->tgl_akhir : '';

        return $this->db->query("
            SELECT f.`id_cuti_det`, a.`nip`, a.`nama`, e.`cabang`, f.`alasan_cuti`, f.`alamat_cuti`, f.`no_hp`, h.`tipe`
            ,GROUP_CONCAT(DATE_FORMAT(g.`tgl`, '%d %M %Y') SEPARATOR ' <br> ') tgl, i.`keterangan`, i.`flag`,
            f.`approval`, f.`user_insert`
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            JOIN cuti_det f ON f.`nip` = a.`nip`
            JOIN cuti_sub_det g ON g.`id_cuti_det` = f.`id_cuti_det`
            JOIN ms_tipe_cuti h ON h.`id` = f.`id_tipe`
            JOIN ms_status_approval i ON i.`kode` = f.`approval`
            WHERE ISNULL(a.`tgl_resign`)
            AND b.`aktif` = '1'
            AND g.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'
            AND a.`idp` = '$idp'
            $cabang $kelas $div
            GROUP BY f.`id_cuti_det`
            ORDER BY g.`tgl` DESC")->result();
    }

    function sisa_cuti($nip='')
    {
        $today = date('Y-m-d');
        $q = $this->db->query("
            SELECT * 
            FROM cuti_dep 
            WHERE nip = '$nip' 
            AND '$today' BETWEEN tgl_awal AND tgl_akhir")->row();

        $sisa = isset($q->qt) ? $q->qt : 0;

        if ($sisa < 0) {
            $sisa = 0;
        }
        
        return $sisa;
    }

    function hitung_cuti_lama($id='')
    {
        return $this->db->query("
                SELECT COUNT(*) AS jml
                FROM cuti_sub_det a
                WHERE a.`id_cuti_det` = '$id'")->row();
    }

    function view_cuti_id($id_cuti='')
    {
        return $this->db->query("
            SELECT a.*, b.`tipe`, c.keterangan, awal.tgl AS mulai, 
            akhir.tgl AS selesai, d.nama, d.mail, d.fcm_id
            FROM cuti_det a 
            INNER JOIN ms_tipe_cuti b ON a.`id_tipe` = b.`id`
            INNER JOIN ms_status_approval c ON c.kode = a.approval
            INNER JOIN kary d ON d.nip = a.nip 
            LEFT JOIN (
                SELECT id_cuti_det, MIN(tgl) AS tgl
                FROM cuti_sub_det
                GROUP BY id_cuti_det
            ) AS awal ON awal.id_cuti_det = a.`id_cuti_det`
            LEFT JOIN (
                SELECT id_cuti_det, MAX(tgl) AS tgl
                FROM cuti_sub_det
                GROUP BY id_cuti_det
            ) AS akhir ON akhir.id_cuti_det = a.`id_cuti_det`
            WHERE a.id_cuti_det = '$id_cuti'
            GROUP BY a.`id_cuti_det`
            ORDER BY a.`id_cuti_det` DESC")->row();
    }
}
?>