<?php

class Rekrutmen_Model extends CI_Model {
    function __construct(){
        parent::__construct();

    }

    function view_datloker()
    {
        return $this->db->query("
            SELECT a.`id_loker`, a.`posisi`, a.`deskripsi`,
            GROUP_CONCAT(c.`kota` SEPARATOR '<br>') AS kota
            FROM r_loker a
            LEFT JOIN r_loker_detail b ON b.`id_loker` = a.`id_loker`
            LEFT JOIN ms_kota c ON c.`id` = b.`id_kota`
            WHERE a.`status` = 1 
            GROUP BY a.`id_loker`")->result();
    }

    function add_loker($data)
    {
        $this->db->insert('r_loker',$data);
    }

    function loker_id($id='')
    {
        return $this->db->query("
            SELECT a.*, GROUP_CONCAT(b.`id_kota`) AS id_kota
            FROM r_loker a 
            LEFT JOIN r_loker_detail b ON a.`id_loker` = b.`id_loker`
            LEFT JOIN ms_kota c ON c.`id` = b.`id_kota`
            WHERE a.`id_loker` = '$id'")->row();
    }

    function update_loker($data,$id)
    {
        $this->db->where('id_loker',$id)->update('r_loker',$data);
    }

    function delete_loker($id)
    {
        $this->db->where('id_loker',$id)->delete('r_loker');
    }
    
    function view_pengajuan_karyawan($qd_id='')
    {
        $per = $this->Main_Model->id_periode($qd_id);
        $tgl_awal = isset($per->tgl_awal) ? $per->tgl_awal : '';
        $tgl_akhir = isset($per->tgl_akhir) ? $per->tgl_akhir : '';

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if($id_cabang != '') {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " a.`id_cabang` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }


        if($id_cabang) {
            return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag`, 
                e.keterangan as ket_hr, f.keterangan as ket_progress, g.keterangan as ket_ttd
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                LEFT JOIN ms_status_rekrutmen e ON e.kode = a.status_hr
                LEFT JOIN ms_status_rekrutmen f on f.kode = a.progress
                LEFT JOIN ms_status_rekrutmen g on g.kode = a.status_ttd
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                $cabang AND a.`status` = '12'
                ")->result();
        } else {
            return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag` 
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                AND a.`status` = '12'
                ")->result();
        }
    }

    function cari_nip($id='')
    {
        return $this->db->query("
            SELECT a.`nip`, b.`nama` FROM d_pengajuan_kary a
            JOIN kary b ON a.`nip` = b.`nip`
            WHERE a.`id` = '$id'")->result();
    }

    function data_ojt($id='', $nip='')
    {
        return $this->db->query("
            SELECT a.`posisi`, b.*, c.nama, 
            CASE WHEN c.gend = 'P' THEN 'Perempuan' ELSE 'Laki-laki' END AS gend, 
            c.tgl_lahir, c.alamat, d.`description`, e.`keterangan` AS cabang
            FROM pengajuan_kary a
            JOIN d_pengajuan_kary b ON a.`id`= b.`id`
            JOIN kary c ON c.nip = b.nip
            LEFT JOIN ms_jobdesk d ON d.`jab` = a.`posisi`
            LEFT JOIN ms_cabang e ON e.`id_cab` = a.`id_cabang`
            WHERE a.`id` = '$id' AND b.`nip` = '$nip'")->row();
    }

    function pwkt_kontrak($nip='')
    {
        return $this->db->query("
            SELECT a.nama, 
            CASE WHEN a.gend = 'P' THEN 'Perempuan' ELSE 'Laki-laki' END AS gend, 
            a.tgl_lahir, a.alamat, e.`description`, f.`keterangan` AS cabang, 
            g.tgl_awal, g.tgl_akhir
            FROM kary a 
            INNER JOIN sk b ON a.`nip` = b.`nip`
            INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            INNER JOIN pos d ON c.`id_pos` = d.`id_pos`
            LEFT JOIN ms_jobdesk e ON e.`jab` = d.`jab`
            LEFT JOIN ms_cabang f ON d.`id_cabang` = f.`id_cab`
            INNER JOIN kont g ON g.nip = a.nip
            WHERE a.`nip` = '$nip' AND g.aktif = 1")->row();
    }
}   
?>