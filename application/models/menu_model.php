<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menu_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function menu($parent='', $user_name='')
	{
		return $this->db->query("
                SELECT a.*, COALESCE(Deriv1.`Count`,0) hitung,
                CASE WHEN (c.id_menu <> '') THEN TRUE ELSE FALSE END AS checked 
                FROM `menu` a  
                LEFT OUTER JOIN (
                    SELECT parent, COUNT(*) AS COUNT 
                    FROM `menu` 
                    GROUP BY parent
                ) Deriv1 ON a.`id_menu` = Deriv1.`parent` 
                LEFT JOIN (
				    SELECT id_menu FROM tb_user_menu
				    WHERE tb_user_menu.`usr_name` = '$user_name'
		        ) AS c ON c.id_menu = a.`id_menu`
                WHERE a.`user` = 2 
                AND a.`status` = 1 
                AND a.`parent` = '$parent' 
                ORDER BY a.`id_menu` ASC")->result();
	}

	function json_menu($user_name='', $parent='0', $level='0')
	{
		$result = $this->menu($parent, $user_name);
		$arr = array();
		if (!empty($result)) {
			foreach ($result as $row => $val) {
				$arr[$row] = array(
					'text' => $val->title,
					'id' => $val->id_menu,
					'icon' => $val->icon
				);

				if($val->checked == 1 && $val->hitung == 0) {
					$arr[$row]['state'] = array(
						'selected' => true,
						'opened' => true
					);
				}

				if ($val->hitung > 0) {
					$arr[$row]['children'] = $this->json_menu($user_name, $val->id_menu, $level + 1);
				}
			}

			$select = $this->db->query("
				SELECT * FROM tb_user_menu 
				WHERE usr_name = '$user_name'")->result();

		}

		return $arr;
	}
}

/* End of file menu_model.php */
/* Location: ./application/models/menu_model.php */ ?>