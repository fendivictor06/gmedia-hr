<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Rekap_Absensi_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function view_karyawan($id_cabang = '', $date_start = '', $date_end = '', $id_divisi = '', $nip = '', $flag = 0)
    {
        $condition = '';
        if ($id_cabang != 0) {
            $condition .= " AND cabang.`id_cabang` = '$id_cabang' ";
        }
        if ($id_divisi != 0 || $id_divisi != '') {
            $condition .= " AND cabang.id_divisi = '$id_divisi' ";
        }
        if ($nip != '') {
            $condition .= " AND a.nip = '$nip'";
        }
        if ($flag == 0) {
            $condition .= " AND a.nip <> '03.001.2009'";
        }
        // $q = $this->db->query("
        //      SELECT a.`nip`, a.`pin`, a.`nama`, cabang.cabang, cabang.divisi
        //      FROM kary a
        //      LEFT JOIN (
  //                SELECT MAX(id_sk), ms_cabang.`cabang`, ms_divisi.divisi, nip, ms_cabang.id_cab, ms_divisi.id_divisi
  //                FROM sk
  //                JOIN pos_sto ON sk.`id_pos_sto` = pos_sto.`id_sto`
  //                JOIN pos ON pos.`id_pos` = pos_sto.`id_pos`
  //                LEFT JOIN ms_cabang ON ms_cabang.`id_cab` = pos.`id_cabang`
  //                LEFT JOIN ms_divisi ON ms_divisi.id_divisi = pos.id_divisi
  //                GROUP BY sk.`nip`
  //               ) cabang ON cabang.nip = a.nip
        //      WHERE (ISNULL(a.tgl_resign)
        //      OR (a.tgl_resign BETWEEN '$date_start' AND '$date_end'))
        //      AND a.tgl_masuk <= '$date_end'
        //      $condition
        //      ORDER BY a.nip ASC")->result();

        $q = $this->db->query("
				SELECT a.`nip`, a.`pin`, a.`nama`, cabang.cabang, cabang.divisi 
				FROM kary a
				LEFT JOIN (
                	SELECT *
                	FROM karyawan 
                ) cabang ON cabang.nip = a.nip
				WHERE (ISNULL(a.tgl_resign) 
				OR (a.tgl_resign BETWEEN '$date_start' AND '$date_end'))
				AND a.tgl_masuk <= '$date_end' 
				$condition 
				ORDER BY a.nip ASC")->result();
        return $q;
    }

    function kary_lembur($id_cabang = '', $date_start = '', $date_end = '')
    {
        $condition = '';
        if ($id_cabang != 0) {
            $condition .= " AND f.`id_cab` = '$id_cabang' ";
        }
        $q = $this->db->query("
				SELECT a.*, b.`nama`, f.`cabang`, g.`divisi`, 
				CONCAT(DATE_FORMAT(a.starttime, '%H:%i'),' ',DATE_FORMAT(a.endtime, '%H:%i')) AS shift,
				CONCAT(absen.scan_masuk,' ',absen.scan_pulang) AS absensi,
				a.jml_jam AS jam, h.nominal AS ovr_nominal, h.id AS id_overtime
				FROM lembur a
				INNER JOIN kary b ON a.`nip` = b.nip
				INNER JOIN sk c ON c.`nip` = b.`nip`
				INNER JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
				INNER JOIN pos e ON e.`id_pos` = d.`id_pos`
				INNER JOIN ms_cabang f ON f.`id_cab` = e.`id_cabang`
				INNER JOIN ms_divisi g ON g.`id_divisi` = e.`id_divisi`
				INNER JOIN ms_overtime h ON h.id = a.id_overtime
				LEFT JOIN (
					SELECT nip, tgl,
					DATE_FORMAT(scan_masuk, '%H:%i') AS scan_masuk,
					DATE_FORMAT(scan_pulang, '%H:%i') AS scan_pulang  
					FROM tb_absensi 
					WHERE tgl BETWEEN '$date_start' AND '$date_end'
					GROUP BY nip, tgl
				) AS absen ON absen.nip = a.`nip` AND a.tgl = absen.tgl
				WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
				$condition
				AND c.`aktif` = 1
				AND a.status = 1 
				ORDER BY a.nip ASC")->result();
        return $q;
    }

    function data_absensi($pin = '', $date = '')
    {
        $q = $this->db->query("
			SELECT a.`scan_masuk`, a.`scan_pulang`, a.`shift`, a.`jam_masuk`, a.`status`,
			IFNULL(telat.total_menit, '') AS total_menit,
			IFNULL(kla_absen.klarifikasi, '') AS klarifikasi_absen,
			IFNULL(UPPER(kla_absen.presensi), '') AS klarifikasi_presensi,
			IFNULL(kla_telat.klarifikasi, '') AS klarifikasi_telat,
			IFNULL(GROUP_CONCAT(scanlog.scan_date SEPARATOR ', '),'') AS scandate
			FROM tb_absensi a
			LEFT JOIN (
				SELECT pin, tgl, total_menit 
				FROM terlambat_temp 
				WHERE tgl = '$date'
			) AS telat ON telat.pin = a.`pin`
			LEFT JOIN (
				SELECT nip, klarifikasi, presensi FROM tb_klarifikasi_absensi
				WHERE tgl = '$date'
			) AS kla_absen ON kla_absen.nip = a.`nip`
			LEFT JOIN (
				SELECT nip, klarifikasi FROM tb_klarifikasi_terlambat
				WHERE tgl = '$date'
			) AS kla_telat ON kla_telat.nip = a.`nip`
			LEFT JOIN (
				SELECT * FROM tb_scanlog
				WHERE DATE(tb_scanlog.`scan_date`) = '$date'
				AND tb_scanlog.`pin` = '$pin'
				GROUP BY tb_scanlog.`scan_date`
			) AS scanlog ON scanlog.pin = a.`pin`
			WHERE a.`pin` = '$pin' 
			AND a.`tgl` = '$date'")->row();
        return $q;
    }

    function rekap_absensi($nip = '', $date = '')
    {
        $q = $this->db->query("
			SELECT DATE_FORMAT(a.`scan_masuk`, '%H:%i') AS scan_masuk, 
			DATE_FORMAT(a.`scan_masuk`, '%H:%i') AS masuk,
			DATE_FORMAT(a.`scan_pulang`, '%H:%i') AS pulang,
			DATE_FORMAT(a.`scan_pulang`, '%H:%i') AS scan_pulang, a.`shift`, a.`jam_masuk`, a.`status`,
			a.uang_makan, a.uang_denda, 
			IFNULL(telat.total_menit, 0) AS total_menit,
			(CASE 
                WHEN (libur.hol_tgl IS NOT NULL) 
                    THEN '1'
                WHEN (DAYOFWEEK(a.`tgl`) = 1 OR DAYOFWEEK(a.`tgl`) = 7)
                    THEN '1'
                    ELSE '0'
            END) AS flag_libur
			FROM tb_absensi a
			LEFT JOIN (
                SELECT *
                FROM hol
            ) AS libur ON libur.hol_tgl = a.`tgl`
			LEFT JOIN (
				SELECT nip, tgl, total_menit 
				FROM terlambat_temp 
				WHERE tgl = '$date'
			) AS telat ON telat.nip = a.`nip`
			WHERE a.`nip` = '$nip' 
			AND a.`tgl` = '$date'")->row();
        return $q;
    }

    function hari_libur($date = '')
    {
        $q = $this->db->where('hol_tgl', $date)
                ->get('hol')->row();

        return $q;
    }

    function shift($date = '', $nip = '')
    {
        $q = $this->db->query("
				SELECT b.* 
				FROM tb_absensi a 
				INNER JOIN ms_shift b ON b.nama = a.shift
				WHERE a.tgl = '$date'
				AND a.nip = '$nip'")->row();

        return $q;
    }

    function cuti($date = '', $nip = '')
    {
        $q = $this->db->query("
				SELECT b.*
				FROM cuti_sub_det a
				INNER JOIN cuti_det b ON a.id_cuti_det = b.id_cuti_det
				WHERE a.tgl = '$date'
				AND b.nip = '$nip'
				AND b.approval = 12")->row();

        return $q;
    }

    function cuti_khusus($date = '', $nip = '')
    {
        $q = $this->db->query("
			SELECT a.*, b.`tipe` 
			FROM cuti_khusus a
			INNER JOIN tipe_cuti_khusus b ON a.`id_tipe_cuti` = b.`id_tipe`
			WHERE a.`nip` = '$nip'
			AND '$date' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`
			AND a.`approval` = 12")->row();

        return $q;
    }

    function laporan($nip = '', $date = '')
    {
        // cari shift
        $s = $this->shift($date, $nip);
        $jam_masuk = isset($s->jam_masuk) ? $s->jam_masuk : '00:00';
        $jam_pulang = isset($s->jam_pulang) ? $s->jam_pulang : '00:00';
        $shift = isset($s->keterangan) ? $s->keterangan : '';
        $kode = isset($s->nama) ? $s->nama : '';

        $r = $this->rekap_absensi($nip, $date);
        // $klarifikasi = isset($r->keterangan) ? $r->keterangan : '';
        $total_menit = isset($r->total_menit) ? $r->total_menit : '';
        $scan_masuk = isset($r->scan_masuk) ? $r->scan_masuk : '';
        $scan_pulang = isset($r->scan_pulang) ? $r->scan_pulang : '';
        $status = isset($r->status) ? $r->status : '';
        $keterangan = '';

        $k_abs = $this->db->where('nip', $nip)
                    ->where('tgl', $date)
                    ->get('tb_klarifikasi_absensi')
                    ->row();
        $klarifikasi_absensi = isset($k_abs->klarifikasi) ? $k_abs->klarifikasi : '';
        $scan_a = isset($k_abs->scan_masuk) ? $k_abs->scan_masuk : '';
        $scan_b = isset($k_abs->scan_pulang) ? $k_abs->scan_pulang : '';
  
        $k_ter = $this->db->where('nip', $nip)
                    ->where('tgl', $date)
                    ->get('tb_klarifikasi_terlambat')
                    ->row();
        $klarifikasi_telat = isset($k_ter->klarifikasi) ? $k_ter->klarifikasi : '';
        $scan_t = isset($k_ter->scan_masuk) ? $k_ter->scan_masuk : '';


        if ($status == '') {
            if ($total_menit > 0) {
                if ($klarifikasi_telat != '') {
                    $keterangan = $scan_masuk.' | '.$scan_pulang.' <br> '.'<span style="color:orange">'.$klarifikasi_telat.'</span>';
                } else {
                    $ishead = $this->db->query("
						SELECT *
						FROM ms_reminder a
						INNER JOIN ms_rule_approval b ON a.`nip` = b.`penyetuju`
						WHERE a.`nip` = '$nip'")->row();
                    
                    if (! empty($ishead)) {
                        $keterangan = $scan_masuk.' | '.$scan_pulang.' <br> '.'<span style="color:orange">Tugas Kantor</span>';
                    } else {
                        $keterangan = $scan_masuk.' | '.$scan_pulang.' <br> '.'<span style="color:red">Terlambat '.$total_menit.' Menit</span>';
                    }
                }
            } else {
                $keterangan = $scan_masuk.' | '.$scan_pulang;
            }

            if ($scan_masuk == '' && $scan_pulang == '') {
                $keterangan = '';
            }
        } else {
            $scan = '';
            if ($scan_masuk != '00:00' || $scan_pulang != '00:00') {
                $scan = $scan_masuk.' | '.$scan_pulang.'<br>';
            }
            
            $keterangan = $scan.$status;
            
            if ($klarifikasi_absensi != '') {
                $keterangan = $scan.'<span style="color:blue">'.$klarifikasi_absensi.'</span>';
            }
        }

        $week = date_format(date_create($date), 'w');
        $holiday = $this->db->where('hol_tgl', $date)
                        ->get('hol')
                        ->row();

        if (($week == 0 || $week == 6 || ! empty($holiday)) && ($jam_masuk != '00:00:00' || $jam_pulang != '00:00:00') && $kode != 'R' && $kode != 'RMD' && $kode != 'R-60') {
            $keterangan .= '<br><span style="color:brown">'.$shift.'</span>';
        }

        return $keterangan;
    }

    function uang_makan($date_start = '', $date_end = '', $nip = '', $id_cabang = '')
    {
        $query = '';
        if ($nip != '') {
            $query .= " AND tb_absensi.nip = '$nip' ";
        }

        if ($id_cabang != '') {
            $query .= " AND cabang.id_cabang = '$id_cabang' ";
        }

        return $this->db->query("
			SELECT SUM(uang_makan) AS uang_makan 
			FROM tb_absensi 
			LEFT JOIN (
				SELECT *
				FROM karyawan
			) AS cabang ON tb_absensi.nip = cabang.nip
			WHERE tgl BETWEEN '$date_start' AND '$date_end' 
			$query ")->row();
    }

    function uang_lembur($date_start = '', $date_end = '', $nip = '', $id_cabang = '')
    {
        $query = '';
        if ($nip != '') {
            $query .= " AND lembur.nip = '$nip' ";
        }

        if ($id_cabang != '') {
            $query .= " AND cabang.id_cabang = '$id_cabang' ";
        }

        return $this->db->query("
			SELECT SUM(lembur) AS uang_lembur, keterangan 
			FROM lembur 
			LEFT JOIN (
				SELECT *
				FROM karyawan
			) AS cabang ON lembur.nip = cabang.nip
			WHERE tgl BETWEEN '$date_start' AND '$date_end' 
			$query
			AND status = 1")->row();
    }

    function uang_denda($date_start = '', $date_end = '', $nip = '', $id_cabang = '')
    {
        $query = '';
        if ($nip != '') {
            $query .= " AND tb_absensi.nip = '$nip' ";
        } else {
            $query .= " AND tb_absensi.nip <> '03.001.2009' ";
        }

        if ($id_cabang != '') {
            $query .= " AND cabang.id_cabang = '$id_cabang' ";
        }
        return $this->db->query("
			SELECT SUM(uang_denda) AS uang_denda 
			FROM tb_absensi 
			LEFT JOIN (
				SELECT *
				FROM karyawan
			) AS cabang ON tb_absensi.nip = cabang.nip
			WHERE tgl BETWEEN '$date_start' AND '$date_end' 
			$query ")->row();
    }

    function terlambat($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
			SELECT COUNT(tgl) AS jml, SUM(total_menit) AS jml_menit
			FROM terlambat_temp 
			WHERE nip = '$nip'
			AND tgl BETWEEN '$date_start' AND '$date_end'")->row();
    }

    function cicilan($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
			SELECT SUM(a.`ags`) AS angsuran, 
			GROUP_CONCAT(a.`ket` SEPARATOR '<br>') AS keterangan 
			FROM pot_det a
			INNER JOIN pot b ON a.`id_pot` = b.`id_pot`
			WHERE a.`tgl` BETWEEN '$date_start' AND '$date_end'
			AND b.`nip` = '$nip'
			AND a.inc_payroll = 1")->row();
    }

    function asuransi($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
    		SELECT SUM(a.`nominal`) AS nominal,
			GROUP_CONCAT(a.`keterangan` SEPARATOR '<br>') AS keterangan 
			FROM tb_asuransi a
			WHERE a.`nip` = '$nip'
			AND CONCAT(a.`akhir_tahun`,'-',a.`akhir_bulan`,'-20') BETWEEN '$date_start' AND '$date_end'
            AND CONCAT(a.`awal_tahun`,'-',a.`awal_bulan`,'-20') BETWEEN '$date_start' AND '$date_end'")->row();
    }

    function pulang_awal($startdate = '', $enddate = '', $nip = '')
    {
        return $this->db->query("
				SELECT a.`nip`, a.`scan_pulang`,
				COUNT(*) AS jumlah
				FROM tb_absensi a
				INNER JOIN tr_shift b ON a.`nip` = b.`nip` AND a.`tgl` = b.`tgl`
				INNER JOIN ms_shift c ON b.`id_shift` = c.`id_shift`
				WHERE a.`nip` = '$nip'
				AND a.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND a.status = 'Pulang Awal'
				AND c.`jam_masuk` <> '' 
				AND c.`jam_pulang` <> ''
				AND a.`scan_pulang` <> ''")->row();
    }

    function scan_masuk($startdate = '', $enddate = '', $nip = '')
    {
        return $this->db->query("
				SELECT a.`nip`, COUNT(*) AS jumlah
				FROM tb_absensi a
				WHERE a.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND a.`nip` = '$nip'
				AND a.`jam_masuk` <> ''
				AND a.`scan_masuk` = '' 
				AND a.scan_pulang <> ''")->row();
    }

    function scan_pulang($startdate = '', $enddate = '', $nip = '')
    {
        return $this->db->query("
				SELECT a.`nip`, COUNT(*) AS jumlah
				FROM tb_absensi a
				WHERE a.`tgl` BETWEEN '$startdate' AND '$enddate'
				AND a.`nip` = '$nip'
				AND a.`jam_masuk` <> ''
				AND a.`scan_pulang` = ''
				AND a.scan_masuk <> ''")->row();
    }

    function bpjs_ketenagakerjaan($nip = '')
    {
        return $this->db->query("
            SELECT a.`nama`, a.`salary` AS nominal, 
            (a.`salary` * (0.24/100)) AS jkkp,
            0 AS jkkk, 
            (a.`salary` * (0.30/100)) AS jkmp, 
            0 AS jkmk,
            (a.`salary` * (3.70/100)) AS jhtp, 
            (a.`salary` * (2.00/100)) AS jhtk,
            (a.`salary` * (2.00/100)) AS jpp, 
            (a.`salary` * (1.00/100)) AS jpk,
            ((a.`salary` * (0.24/100)) + 
           		(a.`salary` * (0.30/100)) + 
           		(a.`salary` * (3.70/100)) + 
           		(a.`salary` * (2.00/100))) AS total_p,
            ((a.`salary` * (2.00/100)) + 
            	(a.`salary` * (1.00/100))) AS total_k
            FROM kary a
            WHERE a.nip = '$nip'")->row();
    }

    function view_absensi($id_cabang = '', $date_start = '', $date_end = '')
    {
        $karyawan = $this->view_karyawan($id_cabang, $date_start, $date_end, '', '', 1);
        $tanggal = date_range($date_start, $date_end);

        $arr_tgl = array();
        if (!empty($tanggal)) {
            for ($a = 0; $a < count($tanggal); $a++) {
                $tgl = explode(' s/d ', $tanggal[$a]);
                $format_tgl = tgl_indo($tgl[0]).'<br> s/d <br>'.tgl_indo(end($tgl));
                $arr_tgl[] = $format_tgl;
            }
        }

        $colspan = count($arr_tgl);
        $table = '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
        $table .= '<thead><tr>
						<th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">#</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">NIP</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Nama</th>
			            <th colspan="'.$colspan.'" style="text-align: center; vertical-align: middle; font-size:11px;">Uang Makan</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Total</th>
			            <th colspan="'.$colspan.'" style="text-align: center; vertical-align: middle; font-size:11px;">Uang Lembur</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Total</th>
			            <th colspan="'.$colspan.'" style="text-align: center; vertical-align: middle; font-size:11px;">Uang Denda</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Total</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">BPJS Ketenagakerjaan</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Cicilan Pinjaman</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Keterangan</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Asuransi</th>
			            <th rowspan="2" style="text-align: center; vertical-align: middle; font-size:11px;">Keterangan Asuransi</th>
			       </tr>';

        $table .= '<tr>';
        if (!empty($arr_tgl)) {
            $baris = 3;
            for ($a = 0; $a < $baris; $a++) {
                $p = 1;
                for ($i = 0; $i < count($arr_tgl); $i++) {
                    $table .= '<th class="text-center" style="text-align: center; vertical-align: middle; font-size:11px;">Minggu '.$p.'<br>'.$arr_tgl[$i].'</th>';
                    $p++;
                }
            }
        }
        $table .= '</tr></thead><tbody>';


        if (!empty($karyawan)) {
            $no = 1;
            $grand_total_um = 0;
            $grand_total_ul = 0;
            $grand_total_ud = 0;
            $grand_total_bpjs = 0;
            $grand_total_cicilan = 0;
            $grand_total_asuransi = 0;
            foreach ($karyawan as $row) {
                $table .= '<tr>
								<td style="font-size:10px;">'.$no++.'</td>
								<td style="font-size:10px;">'.$row->nip.'</td>
								<td style="font-size:10px;">'.$row->nama.'</td>
								';
                    // uang makan
                if (!empty($tanggal)) {
                    $total_uang_makan = 0;
                    for ($i = 0; $i < count($tanggal); $i++) {
                        $tgl = explode(' s/d ', $tanggal[$i]);
                        $um = $this->uang_makan($tgl[0], end($tgl), $row->nip);

                        $uang_makan[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                        $total_uang_makan = $total_uang_makan + $uang_makan[$i];
                        $table .= '<td class="text-right" style="font-size:10px;">'.$uang_makan[$i].'</td>';
                    }
                }
                    $table .= '<td class="text-right" style="font-size:10px;">'.$total_uang_makan.'</td>';
                    $grand_total_um = $grand_total_um + $total_uang_makan;

                    // uang lembur
                if (!empty($tanggal)) {
                    $total_uang_lembur = 0;
                    for ($i = 0; $i < count($tanggal); $i++) {
                        $tgl = explode(' s/d ', $tanggal[$i]);
                        $um = $this->uang_lembur($tgl[0], end($tgl), $row->nip);

                        $uang_lembur[$i] = isset($um->uang_lembur) ? $um->uang_lembur : 0;
                        $total_uang_lembur = $total_uang_lembur + $uang_lembur[$i];
                        $table .= '<td class="text-right" style="font-size:10px;">'.$uang_lembur[$i].'</td>';
                    }
                }
                    $table .= '<td class="text-right" style="font-size:10px;">'.$total_uang_lembur.'</td>';
                    $grand_total_ul = $grand_total_ul + $total_uang_lembur;

                    // uang denda
                if (!empty($tanggal)) {
                    $total_uang_denda = 0;
                    for ($i = 0; $i < count($tanggal); $i++) {
                        $tgl = explode(' s/d ', $tanggal[$i]);
                        $um = $this->uang_denda($tgl[0], end($tgl), $row->nip);

                        $uang_denda[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                        $total_uang_denda = $total_uang_denda + $uang_denda[$i];

                        $denda = $uang_denda[$i];
                        if ($row->nip == '03.001.2009') {
                            $denda = 0;
                        }

                        $table .= '<td class="text-right" style="font-size:10px;">'.$denda.'</td>';
                    }
                }

                if ($row->nip == '03.001.2009') {
                    $total_uang_denda = 0;
                }

                $table .= '<td class="text-right" style="font-size:10px;">'.$total_uang_denda.'</td>';
                $grand_total_ud = $grand_total_ud + $total_uang_denda;

                // bpjs
                $bpjs_tk = $this->bpjs_ketenagakerjaan($row->nip);
                $nominal_bpjs = isset($bpjs_tk->total_k) ? $bpjs_tk->total_k : 0;
                $table .= '<td class="text-right" style="font-size:10px;">'.round($nominal_bpjs, 0).'</td>';
                $grand_total_bpjs = $grand_total_bpjs + $nominal_bpjs;

                // cicilan
                $c = $this->cicilan($date_start, $date_end, $row->nip);
                $cicilan = isset($c->angsuran) ? $c->angsuran : 0;
                $keterangan = isset($c->keterangan) ? $c->keterangan : '';
                $grand_total_cicilan = $grand_total_cicilan + $cicilan;

                $table .= '<td class="text-right" style="font-size:10px;">'.$cicilan.'</td>';
                $table .= '<td style="font-size:10px;">'.$keterangan.'</td>';

                # asuransi
                $ass = $this->asuransi($date_start, $date_end, $row->nip);
                $asuransi = isset($ass->nominal) ? $ass->nominal : 0;
                $keterangan_asuransi = isset($ass->keterangan) ? $ass->keterangan : '';
                $grand_total_asuransi = $grand_total_asuransi + $asuransi;

                $table .= '<td class="text-right" style="font-size:10px;">'.$asuransi.'</td>';
                $table .= '<td style="font-size:10px;">'.$keterangan_asuransi.'</td>';

                $table  .=  '</tr>';
            }
            $table .= '</tbody><tfoot>';
            $table .= '<tr>
						<td></td>
						<td></td>
						<td class="text-right" style="font-size:10px;">Total</td>';
            if (! empty($tanggal)) {
                for ($i = 0; $i < count($tanggal); $i++) {
                    $tgl = explode(' s/d ', $tanggal[$i]);
                    $um = $this->uang_makan($tgl[0], end($tgl), '', $id_cabang);

                    $sub_um[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                    $table .= '<td class="text-right" style="font-size:10px;">'.$sub_um[$i].'</td>';
                }
            }
            $table .= '<td class="text-right" style="font-size:10px;">'.$grand_total_um.'</td>';
            if (!empty($tanggal)) {
                for ($i = 0; $i < count($tanggal); $i++) {
                    $tgl = explode(' s/d ', $tanggal[$i]);
                    $um = $this->uang_lembur($tgl[0], end($tgl), '', $id_cabang);
                    $sub_l[$i] = isset($um->uang_lembur) ? $um->uang_lembur : 0;
                    $table .= '<td class="text-right" style="font-size:10px;">'.$sub_l[$i].'</td>';
                }
            }
            $table .= '<td class="text-right" style="font-size:10px;">'.$grand_total_ul.'</td>';
            if (!empty($tanggal)) {
                for ($i = 0; $i < count($tanggal); $i++) {
                    $tgl = explode(' s/d ', $tanggal[$i]);
                    $um = $this->uang_denda($tgl[0], end($tgl), '', $id_cabang);
                    $sub_d[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                    $table .= '<td class="text-right" style="font-size:10px;">'.$sub_d[$i].'</td>';
                }
            }
            $table .= '<td class="text-right" style="font-size:10px;">'.$grand_total_ud.'</td>';
            $table .= '<td class="text-right" style="font-size:10px;">'.round($grand_total_bpjs, 0).'</td>';
            $table .= '<td class="text-right" style="font-size:10px;">'.$grand_total_cicilan.'</td>';
            $table .= '<td class="text-right" style="font-size:10px;"></td>';
            $table .= '<td class="text-right" style="font-size:10px;">'.$grand_total_asuransi.'</td>';
            $table .= '<td class="text-right" style="font-size:10px;"></td>';
            $table .= '</tr>';
            $table .= '</tfoot>';
        }

        $table .= '</table>';
        return $table;
    }

    function view_uang_makan($id_cabang = '', $date_start = '', $date_end = '')
    {
        $karyawan = $this->view_karyawan($id_cabang, $date_start, $date_end);
        $tanggal = range_to_date($date_start, $date_end);

        $arr_tgl = array();
        if (!empty($tanggal)) {
            for ($a = 0; $a < count($tanggal); $a++) {
                $hari = hari(date('w', strtotime($tanggal[$a])));
                $format_tgl = $hari.', '.tanggal($tanggal[$a]);
                $arr_tgl[] = $format_tgl;
            }
        }

        $colspan = count($arr_tgl);
        $table = '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
        $table .= '<thead><tr>
						<th rowspan="2">#</th>
			            <th rowspan="2">NIP</th>
			            <th rowspan="2">Nama</th>
			            <th rowspan="2">Cabang</th>
			            <th rowspan="2">Divisi</th>
			            <th colspan="'.$colspan.'">Uang Makan</th>
			            <th rowspan="2">Total</th>
			       </tr>';

        $table .= '<tr>';
        if (!empty($arr_tgl)) {
            $baris = 1;
            for ($a = 0; $a < $baris; $a++) {
                for ($i = 0; $i < count($arr_tgl); $i++) {
                    $table .= '<th class="text-center">'.$arr_tgl[$i].'</th>';
                }
            }
        }
        $table .= '</tr></thead><tbody>';


        if (!empty($karyawan)) {
            $no = 1;
            $total_uang_makan = 0;
            foreach ($karyawan as $row) {
                $table .= '<tr>
								<td>'.$no++.'</td>
								<td>'.$row->nip.'</td>
								<td>'.$row->nama.'</td>
								<td>'.$row->cabang.'</td>
								<td>'.$row->divisi.'</td>';
                    // uang makan
                if (!empty($tanggal)) {
                    $total_uang_makan = 0;
                    for ($i = 0; $i < count($tanggal); $i++) {
                        $um = $this->uang_makan($tanggal[$i], $tanggal[$i], $row->nip);

                        $uang_makan[$i] = isset($um->uang_makan) ? $um->uang_makan : 0;
                        $total_uang_makan = $total_uang_makan + $uang_makan[$i];

                        $k = $this->laporan($row->nip, $tanggal[$i]);
                        $laporan = $k;

                        $table .= '<td class="text-center">'.$laporan.'</td>';
                    }
                }
                    $table .= '<td class="text-right">'.uang($total_uang_makan).'</td>';

                $table  .=  '</tr>';
            }
        }

        $table .= '</tbody></table>';
        return $table;
    }

    function view_uang_lembur($id_cabang = '', $date_start = '', $date_end = '')
    {
        $karyawan = $this->kary_lembur($id_cabang, $date_start, $date_end);
        $tanggal = range_to_date($date_start, $date_end);

        $head = array('Tanggal', 'Shift', 'Absensi', 'Jam', 'Per Jam', 'Total', 'Keterangan');
        $colspan = count($head);
        $table = '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
        $table .= '<thead><tr>
						<th rowspan="2">#</th>
			            <th rowspan="2">NIP</th>
			            <th rowspan="2">Nama</th>
			            <th rowspan="2">Cabang</th>
			            <th rowspan="2">Divisi</th>
			            <th colspan="'.$colspan.'">Uang Lembur</th>
			       </tr>';

        $table .= '<tr>';
        if (!empty($head)) {
            $baris = 1;
            for ($a = 0; $a < $baris; $a++) {
                for ($i = 0; $i < count($head); $i++) {
                    $table .= '<th class="text-center">'.$head[$i].'</th>';
                }
            }
        }
        $table .= '</tr></thead><tbody>';


        if (!empty($karyawan)) {
            $no = 1;
            $arr = array();
            foreach ($karyawan as $row) {
                $ovr_nominal = isset($row->ovr_nominal) ? $row->ovr_nominal : 0;
                $id_overtime = isset($row->id_overtime) ? $row->id_overtime : 0;

                if ($id_overtime == 0) {
                    $ovr_nominal = 0;
                }

                $span_start = '';
                $span_end = '';
                if (in_array($row->nip, $arr)) {
                    $span_start = '<span style="display:none;">';
                    $span_end = '</span>';
                    $no--;
                }
                $arr[] = $row->nip;
                $hari = hari(date('w', strtotime($row->tgl)), 2);
                $table .= '<tr>
								<td>'.$span_start.$no++.$span_end.'</td>
								<td>'.$span_start.$row->nip.$span_end.'</td>
								<td>'.$span_start.$row->nama.$span_end.'</td>
								<td>'.$span_start.$row->cabang.$span_end.'</td>
								<td>'.$span_start.$row->divisi.$span_end.'</td>
								<td>'.$hari.', '.tanggal($row->tgl).'</td>
								<td>'.$row->shift.'</td>
								<td>'.$row->absensi.'</td>
								<td>'.$row->jam.'</td>
								<td class="text-right">'.uang($ovr_nominal).'</td>
								<td class="text-right">'.uang($row->lembur).'</td>
								<td>'.$row->keterangan.'</td>';

                $table  .=  '</tr>';
            }
        }

        $table .= '</tbody></table>';
        return $table;
    }

    function view_uang_denda($id_cabang = '', $date_start = '', $date_end = '')
    {
        $karyawan = $this->view_karyawan($id_cabang, $date_start, $date_end);
        $tanggal = range_to_date($date_start, $date_end);

        $arr_tgl = array();
        if (!empty($tanggal)) {
            for ($a = 0; $a < count($tanggal); $a++) {
                $hari = hari(date('w', strtotime($tanggal[$a])));
                $format_tgl = $hari.', '.tanggal($tanggal[$a]);
                $arr_tgl[] = $format_tgl;
            }
        }

        $colspan = count($arr_tgl);
        $table = '<table class="table table-striped table-hover table-bordered" id="tb_rekap">';
        $table .= '<thead><tr>
						<th rowspan="2">#</th>
			            <th rowspan="2">NIP</th>
			            <th rowspan="2">Nama</th>
			            <th rowspan="2">Cabang</th>
			            <th rowspan="2">Divisi</th>
			            <th colspan="'.$colspan.'">Uang Denda</th>
			            <th rowspan="2">Total</th>
			       </tr>';

        $table .= '<tr>';
        if (!empty($arr_tgl)) {
            $baris = 1;
            for ($a = 0; $a < $baris; $a++) {
                for ($i = 0; $i < count($arr_tgl); $i++) {
                    $table .= '<th class="text-center">'.$arr_tgl[$i].'</th>';
                }
            }
        }
        $table .= '</tr></thead><tbody>';


        if (!empty($karyawan)) {
            $no = 1;
            foreach ($karyawan as $row) {
                $table .= '<tr>
								<td>'.$no++.'</td>
								<td>'.$row->nip.'</td>
								<td>'.$row->nama.'</td>
								<td>'.$row->cabang.'</td>
								<td>'.$row->divisi.'</td>';
                    // uang denda
                if (!empty($tanggal)) {
                    $total_uang_denda = 0;
                    for ($i = 0; $i < count($tanggal); $i++) {
                        $um = $this->uang_denda($tanggal[$i], $tanggal[$i], $row->nip);

                        $uang_denda[$i] = isset($um->uang_denda) ? $um->uang_denda : 0;
                        $total_uang_denda = $total_uang_denda + $uang_denda[$i];

                        $k = $this->laporan($row->nip, $tanggal[$i]);
                        $laporan = $k;

                        $table .= '<td class="text-center">'.$uang_denda[$i].'</td>';
                    }
                }
                    $table .= '<td class="text-right">'.uang($total_uang_denda).'</td>';

                $table  .=  '</tr>';
            }
        }

        $table .= '</tbody></table>';
        return $table;
    }

    function divisi($id_divisi = '')
    {
        $condition = ($id_divisi) ? "AND b.id_divisi = '$id_divisi'" : '';
        return $this->db->query("
			SELECT DISTINCT(b.`id_divisi`) AS id_divisi, b.divisi 
			FROM pos a
			INNER JOIN ms_divisi b ON a.`id_divisi` = b.`id_divisi`
			WHERE b.`id_divisi` <> 0
			$condition")->result();
    }

    function rekap_terlambat($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
			SELECT *
			FROM terlambat_temp
			WHERE terlambat_temp.`tgl` BETWEEN '$date_start' AND '$date_end'
			AND terlambat_temp.`nip` = '$nip'");
    }

    function rekap_pulang_awal($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
				SELECT a.*, 
				DATE_FORMAT(a.scan_pulang, '%d/%m/%Y %H:%i') AS pulang,
				CASE WHEN (c.jam_pulang < c.jam_masuk) 
				THEN CONCAT(a.tgl + INTERVAL 1 DAY, ' ', c.jam_pulang)
				ELSE 
				CONCAT(a.tgl, ' ', c.jam_pulang)
				END AS jam_pulang
				FROM tb_absensi a
				INNER JOIN tr_shift b ON a.`nip` = b.`nip` AND a.`tgl` = b.`tgl`
				INNER JOIN ms_shift c ON b.`id_shift` = c.`id_shift`
				WHERE a.`nip` = '$nip'
				AND a.`tgl` BETWEEN '$date_start' AND '$date_end'
				AND c.`jam_masuk` <> '' 
				AND c.`jam_pulang` <> ''
				AND a.`scan_pulang` <> ''
				HAVING jam_pulang > a.scan_pulang");
    }

    function rekap_scan_masuk($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
				SELECT a.*,
				DATE_FORMAT(a.scan_masuk, '%d/%m/%Y %H:%i') AS masuk
				FROM tb_absensi a
				WHERE a.`nip` = '$nip'
				AND a.`tgl` BETWEEN '$date_start' AND '$date_end'
				AND a.scan_masuk = ''
				AND a.scan_pulang <> ''
				AND a.jam_masuk <> ''");
    }

    function rekap_scan_pulang($date_start = '', $date_end = '', $nip = '')
    {
        return $this->db->query("
				SELECT a.*, 
				IFNULL(shift.jam_pulang, (
					SELECT jam_pulang 
					FROM ms_shift 
					WHERE default_shift = 1)
				) AS jam_pulang, 
				DATE_FORMAT(a.scan_pulang, '%d/%m/%Y %H:%i') AS pulang
				FROM tb_absensi a
				LEFT JOIN (
					SELECT tr_shift.`nip`, tr_shift.`tgl`, 
					tr_shift.`id_shift`, ms_shift.`jam_masuk`,
					ms_shift.`jam_pulang`
					FROM tr_shift 
					INNER JOIN ms_shift ON tr_shift.`id_shift` = ms_shift.`id_shift`
					WHERE tr_shift.`tgl` BETWEEN '$date_start' AND '$date_end'
				) shift ON shift.tgl = a.`tgl` AND shift.nip = a.`nip`
				WHERE a.`nip` = '$nip'
				AND a.`tgl` BETWEEN '$date_start' AND '$date_end'
				AND a.scan_pulang = ''
				AND a.scan_masuk <> ''
				HAVING jam_pulang <> ''");
    }

    function periode_before()
    {
        return $this->db->query("
				SELECT a.`tgl_awal`, a.`tgl_akhir`
				FROM q_det a
				WHERE a.`tgl_akhir` < DATE(NOW())
				ORDER BY a.`tgl_akhir` DESC
				LIMIT 1")->row();
    }

    function laporan_terlambat($date_start = '', $date_end = '')
    {
        return $this->db->query("
				SELECT a.`nip`, a.`nama`, 
				IFNULL(hari_telat.jumlah, 0) AS jumlah_telat,
				IFNULL(jumlah_telat.jumlah, 0) AS jumlah_menit_telat,
				IFNULL(pl_awal.jumlah, 0) AS jumlah_plg_awal,
				IFNULL(pl_awal_x.menit, 0) AS jumlah_menit_plg_awal,
				IFNULL(absen_masuk.jumlah, 0) AS jumlah_tdk_absen_masuk,
				IFNULL(absen_pulang.jumlah, 0) AS jumlah_tdk_absen_pulang
				FROM kary a
				INNER JOIN sk b ON a.`nip` = b.`nip`
				INNER JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
				INNER JOIN pos d ON d.`id_pos` = c.`id_pos`
				LEFT JOIN (
					SELECT nip, COUNT(*) AS jumlah
				 	FROM terlambat_temp 
					WHERE tgl BETWEEN '$date_start' AND '$date_end'
					AND NOT EXISTS(
						SELECT NULL
						FROM tb_klarifikasi_terlambat 
						WHERE tb_klarifikasi_terlambat.`tgl` = terlambat_temp.`tgl`
						AND tb_klarifikasi_terlambat.`nip` = terlambat_temp.`nip`
						
					)
					GROUP BY nip 
				) AS hari_telat ON hari_telat.nip = a.`nip`
				LEFT JOIN (
					SELECT nip, SUM(terlambat_temp.`total_menit`) AS jumlah
					FROM terlambat_temp
					WHERE tgl BETWEEN '$date_start' AND '$date_end'
					AND NOT EXISTS(
						SELECT NULL
						FROM tb_klarifikasi_terlambat 
						WHERE tb_klarifikasi_terlambat.`tgl` = terlambat_temp.`tgl`
						AND tb_klarifikasi_terlambat.`nip` = terlambat_temp.`nip`
						
					)
					GROUP BY nip 
				) AS jumlah_telat ON jumlah_telat.nip = a.`nip`
				LEFT JOIN (
					SELECT tb_absensi.nip, ms_shift.`jam_masuk`, ms_shift.`jam_pulang`, 
					tb_absensi.`scan_pulang`, 
					COUNT(*) AS jumlah,
					SUM(TIMESTAMPDIFF(MINUTE, TIME(tb_absensi.`scan_pulang`), ms_shift.`jam_pulang`)) AS menit
					FROM tb_absensi 
					INNER JOIN tr_shift ON tr_shift.`nip` = tb_absensi.`nip` AND tr_shift.`tgl` = tb_absensi.`tgl`
					INNER JOIN ms_shift ON ms_shift.`id_shift` = tr_shift.`id_shift`
					WHERE tb_absensi.`status` LIKE '%Pulang Awal%' 
					AND tb_absensi.tgl BETWEEN '$date_start' AND '$date_end'
					GROUP BY tb_absensi.`nip`
					HAVING menit > 0
				) AS pl_awal ON pl_awal.nip = a.`nip`
				LEFT JOIN (
					SELECT tb_absensi.nip, ms_shift.`jam_masuk`, ms_shift.`jam_pulang`, 
					tb_absensi.`scan_pulang`, 
					SUM(TIMESTAMPDIFF(MINUTE, TIME(tb_absensi.`scan_pulang`), ms_shift.`jam_pulang`)) AS menit
					FROM tb_absensi 
					INNER JOIN tr_shift ON tr_shift.`nip` = tb_absensi.`nip` AND tr_shift.`tgl` = tb_absensi.`tgl`
					INNER JOIN ms_shift ON ms_shift.`id_shift` = tr_shift.`id_shift`
					WHERE tb_absensi.`status` LIKE '%Pulang Awal%' 
					AND tb_absensi.tgl BETWEEN '$date_start' AND '$date_end'
					GROUP BY tb_absensi.`nip`
					HAVING menit > 0
				) AS pl_awal_x ON pl_awal_x.nip = a.`nip`
				LEFT JOIN (
					SELECT  COUNT(*) AS jumlah, nip, tgl
					FROM tb_absensi
					WHERE `status` LIKE '%Tidak Absen Masuk%' 
					AND tgl BETWEEN '$date_start' AND '$date_end'
					GROUP BY nip
				) AS absen_masuk ON absen_masuk.nip = a.`nip`
				LEFT JOIN (
					SELECT  COUNT(*) AS jumlah, nip, tgl
					FROM tb_absensi
					WHERE `status` LIKE '%Tidak Absen Pulang%' 
					AND tgl BETWEEN '$date_start' AND '$date_end'
					GROUP BY nip
				) AS absen_pulang ON absen_pulang.nip = a.`nip`
				WHERE b.`aktif` = 1
				AND a.`tgl_resign` IS NULL
				AND d.`sal_pos` = 'Staff'
				AND d.id_cabang = '1'
				ORDER BY hari_telat.jumlah DESC, jumlah_telat.jumlah DESC, pl_awal.jumlah DESC, pl_awal_x.menit DESC,
				absen_masuk.jumlah DESC, absen_pulang.jumlah DESC")->result();
    }

    function detail_rekapitulasi($nip = '', $tgl_awal = '', $tgl_akhir = '')
    {
        return $this->db->query("
			SELECT a.`tgl`, a.`uang_makan`, a.`uang_denda`, a.`status`
			, a.`keterangan` AS keterangan_absen, a.`klarifikasi_terlambat` AS keterangan_terlambat
			FROM tb_absensi	a
			WHERE a.`nip` = '$nip'
			AND a.`tgl` BETWEEN '$tgl_awal' AND '$tgl_akhir'")->result();
    }
}

/* End of file rekap_absensi_model.php */
/* Location: ./application/models/rekap_absensi_model.php */
