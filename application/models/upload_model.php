<?php

class Upload_Model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Main_Model', '', TRUE);
        $this->load->model('Karyawan_Model', '', TRUE);
        $this->load->model('Absensi_Model', '', TRUE);
        $this->load->model('Cuti_Model', '', TRUE);
        $this->load->model('Api_Model', '', TRUE);
        $this->load->model('Tunjangan_Model', '', TRUE);
    }

    function upload_data($filename)
    {
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploads/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=2; $i < ($numRows+1) ; $i++) { 
            // $tgl_asli = str_replace('/', '-', $worksheet[$i]['B']);
            // $exp_tgl_asli = explode('-', $tgl_asli);
            // $exp_tahun = explode(' ', $exp_tgl_asli[2]);
            // $tgl_sql = $exp_tahun[0].'-'.$exp_tgl_asli[0].'-'.$exp_tgl_asli[1].' '.$exp_tahun[1];
            if($worksheet[$i]["A"]==""){

            }else{
                $ins = array(
                        "nip"           => $worksheet[$i]["A"],
                        "nama"          => $worksheet[$i]["B"],
                        "gend"          => $worksheet[$i]["C"], 
                        "ktp"           => $worksheet[$i]["D"],
                        "tgl_lahir"     => $worksheet[$i]["E"],
                        "t_lahir"       => $worksheet[$i]["F"],
                        "telp"          => $worksheet[$i]["G"],
                        "alamat"        => $worksheet[$i]["H"],
                        "mail"          => $worksheet[$i]["I"],
                        "mar_stat"      => $worksheet[$i]["J"],
                        "bank"          => $worksheet[$i]["K"],
                        "norek"         => $worksheet[$i]["L"],
                        "norek_an"      => $worksheet[$i]["M"],
                        "tfinfo"        => $worksheet[$i]["N"],
                        "id_pend"       => $worksheet[$i]["O"],
                        "nama_sklh"     => $worksheet[$i]["P"],
                        "jurusan"       => $worksheet[$i]["Q"],
                        "th_lulus"      => $worksheet[$i]["R"],
                        "ikut_asuransi" => $worksheet[$i]["S"],
                        "npwp"          => $worksheet[$i]["T"],
                        "agama"         => $worksheet[$i]["U"],
                        "no_ijazah"     => $worksheet[$i]["V"],
                        "status_ijazah" => $worksheet[$i]["W"],
                        "surat_komitmen"=> $worksheet[$i]["X"],
                        "telp_emergency"=> $worksheet[$i]["Y"],
                        "contact_emergency"=> $worksheet[$i]["Z"],
                        "kary_stat"     => $worksheet[$i]["AA"],
                        "tgl_tetap"     => $worksheet[$i]["AB"],
                        "pin"           => $worksheet[$i]["AC"],
                        "idp"           => $this->session->userdata('idp')
                       );
                $nip= $worksheet[$i]["A"];
                $q  = $this->db->query("SELECT * FROM kary WHERE nip = '$nip'")->row();
                if(empty($q)) {
                    $this->db->insert('kary', $ins);
                }
            }
        }
    }

    function upload_sps($filename,$id_per)
    {
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploads/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=8; $i < ($numRows+1) ; $i++) { 
            // $tgl_asli = str_replace('/', '-', $worksheet[$i]['B']);
            // $exp_tgl_asli = explode('-', $tgl_asli);
            // $exp_tahun = explode(' ', $exp_tgl_asli[2]);
            // $tgl_sql = $exp_tahun[0].'-'.$exp_tgl_asli[0].'-'.$exp_tgl_asli[1].' '.$exp_tahun[1];
            if($worksheet[$i]["B"]==""){

            }else{
                $ins = array(
                        "nip"           => $worksheet[$i]["B"],
                        "id_per"        => $id_per,
                        "j_m_biasa"     => $worksheet[$i]["G"], 
                        "j_m_libur"     => $worksheet[$i]["H"],
                        "Keterangan"    => $worksheet[$i]["I"]
                       );

                $this->db->insert('ab_m', $ins);
            }
        }
    }

    function upload_ach($filename,$id_per)
    {
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploads/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=5; $i < ($numRows+1) ; $i++) { 
            // $tgl_asli = str_replace('/', '-', $worksheet[$i]['B']);
            // $exp_tgl_asli = explode('-', $tgl_asli);
            // $exp_tahun = explode(' ', $exp_tgl_asli[2]);
            // $tgl_sql = $exp_tahun[0].'-'.$exp_tgl_asli[0].'-'.$exp_tgl_asli[1].' '.$exp_tahun[1];
            if($worksheet[$i]["B"]==""){

            }else{
                $ins = array(
                        "nip"       => $worksheet[$i]["B"],
                        "id_per"    => $id_per,
                        "ach"       => $worksheet[$i]["F"]
                       );

                $this->db->insert('ach_temp', $ins);
                $this->call_ach();
            }
        }
    }

    function upload_telat($filename,$id_per)
    {
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploads/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);
        for ($i=2; $i < ($numRows+1) ; $i++) { 
            if($worksheet[$i]["A"]==""){

            }else{
                $ins = array(
                        "nip"           => $worksheet[$i]["A"],
                        "id_per"        => $id_per,
                        "total_menit"   => $worksheet[$i]["B"],
                        "total_hari"    => $worksheet[$i]["C"]
                       );

                $this->db->insert('terlambat', $ins);
            }
        }
    }

    function call_ach()
    {
        $this->db->query("CALL insert_ach()");
    }

    function upload_absensi($filename)
    {
        ini_set('memory_limit', '-1');
        ini_set('MAX_EXECUTION_TIME', '-1');
        set_time_limit(0);
        $inputFileName = './assets/uploads/'.$filename;
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
            die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);
        for ($i=3; $i < ($numRows+1) ; $i++) { 
            if($worksheet[$i]["G"] == "" && $worksheet[$i]["A"] == "") {

            } else {
                $nip        = $worksheet[$i]["B"];
                $pin        = $worksheet[$i]["A"];
                if(strpos($worksheet[$i]["G"], '-') == false) 
                {
                    $tgl        = $this->Main_Model->convert_tgl($worksheet[$i]["G"]);
                } 
                else 
                {
                    $tgl        = $this->Main_Model->convert_tgl_excel($worksheet[$i]["G"]);
                }
                
                // $scan_masuk = $worksheet[$i]["K"];
                // $scan_pulang= $worksheet[$i]["Q"];
                $scan = array();
                if($worksheet[$i]["H"])  $scan[] = $tgl.' '.$worksheet[$i]["H"];
                if($worksheet[$i]["I"])  $scan[] = $tgl.' '.$worksheet[$i]["I"];
                if($worksheet[$i]["J"])  $scan[] = $tgl.' '.$worksheet[$i]["J"];
                if($worksheet[$i]["K"])  $scan[] = $tgl.' '.$worksheet[$i]["K"];
                if($worksheet[$i]["L"])  $scan[] = $tgl.' '.$worksheet[$i]["L"];

                $scan_masuk = strtotime($scan[0]);
                $scan_pulang = strtotime($scan[0]);
       
                for($a=0; $a < count($scan); $a++) {
                    if($scan_masuk > strtotime($scan[$a])) $scan_masuk = strtotime($scan[$a]);
                    if($scan_pulang < strtotime($scan[$a])) $scan_pulang = strtotime($scan[$a]);
                }

                // print_r($scan_pulang);exit;
                $shift = $this->Api_Model->cari_shift($tgl, $pin);
                $default_shift = $this->Api_Model->default_shift();
                if(!empty($shift) || !empty($default_shift)) {
                    // jam masuk
                    ($shift) ? $masuk = strtotime($shift->jam_masuk) : $masuk = strtotime($default_shift->jam_masuk);
                    ($shift) ? $jam_masuk = $shift->jam_masuk : $jam_masuk = $default_shift->jam_masuk;
                    // jam pulang
                    ($shift) ? $pulang = strtotime($shift->jam_pulang) : $pulang = strtotime($default_shift->jam_pulang);
                    // nama shift
                    ($shift) ? $nama_shift = $shift->nama : $nama_shift = $default_shift->nama;
                } else {
                    $masuk      = strtotime('08:00:59');
                    $jam_masuk  = '08:00:00';
                    $nama_shift = 'Shift General';
                }

                // tambah 2 jam 
                $plus = date('H:i:s', strtotime('+2 hour', strtotime($tgl.' '.$masuk)));
                // kurang 2 jam 
                $minus = date('H:i:s', strtotime('-2 hour', strtotime($tgl.' '.$masuk)));

                // cek data di tabel absensi
                $check  = $this->db->query("SELECT * FROM tb_absensi WHERE tgl = '$tgl' AND pin = '$pin'")->row();
                $flag   = 0;
                if($check) {
                    // jika ada maka hapus dulu
                    $this->db->where('pin',$pin)->where('tgl',$tgl)->delete('tb_absensi');
                }
                    // jika tidak absen masuk dan pulang
                    if($scan_masuk == '' && $scan_pulang == '') {
                        // jika hari libur nasional
                        $check_holiday  = $this->db->where('hol_tgl',$tgl)->get('hol')->row();
                        if($check_holiday && empty($shift)) {
                            $status = $check_holiday->hol_desc;
                        } else {
                            // jika hari minggu
                            if(date_format(date_create($tgl), 'w') == 0 && empty($shift)) {
                                $status = 'Hari Minggu';
                            } else {
                                $status = 'Tidak Masuk';
                                $flag   = 1;
                            }
                        }

                        $check_cuti_biasa = $this->db->query("select * from cuti_det a join cuti_sub_det b on a.id_cuti_det = b.id_cuti_det where b.tgl = '$tgl' and a.nip = '$nip'")->row();
                        if($check_cuti_biasa) {
                            $status = 'Cuti';
                            $flag = 0;
                        }

                        $check_cuti_khusus = $this->db->query("SELECT * FROM cuti_khusus a JOIN tipe_cuti_khusus b ON a.`id_tipe_cuti`=b.`id_tipe` WHERE a.`nip` = '$nip' AND '$tgl' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`")->row();
                        if($check_cuti_khusus) {
                            $status = $check_cuti_khusus->tipe;
                            $flag = 0;
                        }
                    } else {
                        $status = '';
                        $flag   = 0;
                        if($scan_masuk == $scan_pulang) {
                            if($scan_masuk < strtotime($tgl.' '.$plus)) {
                                $scan_masuk = $scan_masuk;
                                $scan_pulang = 0;
                                $status = 'Tidak Absen Pulang';
                                $flag   = 1;
                            } else {
                                $scan_masuk = 0;
                                $scan_pulang = $scan_pulang;
                                $status = 'Tidak Absen Masuk';
                                $flag   = 1;
                            }
                        } else {
                            if($scan_masuk < strtotime($tgl.' '.$minus) && $scan_masuk > strtotime($tgl.' '.$plus)) {
                                $scan_masuk = $scan_masuk;
                                $scan_pulang = 0;
                                $status = 'Tidak Absen Pulang';
                                $flag = 1;
                            } else {
                                $scan_masuk = 0;
                                $scan_pulang = $scan_pulang;
                                $status = 'Tidak Absen Masuk';
                                $flag   = 1;
                            }
                        }
                    }
                    ($scan_masuk == 0) ? $scan_masuk = '00:00:00' : $scan_masuk = date('H:i:s',$scan_masuk);
                    ($scan_pulang == 0) ? $scan_pulang = '00:00:00' : $scan_pulang = date('H:i:s',$scan_pulang);

                    // jika flag = 1 maka isi presensi 
                    if($flag == 1) {
                        // cek data presensi jika ada yang sama hapus dulu
                        $check_presensi = $this->db->where('pin',$pin)->where('tgl',$tgl)->get('presensi')->row();
                        if($check_presensi) {
                            $this->db->where('pin',$pin)->where('tgl',$tgl)->delete('presensi');
                        }

                        $ins_presensi   = array(
                            'nip'       => $nip,
                            'tgl'       => $tgl,
                            'mangkir'   => 1,
                            'ket'       => $status,
                            'pin'       => $pin,
                            'user'      => 'Manual Excel'
                            );
                        $this->db->insert('presensi',$ins_presensi);
                    } else {
                        $check_presensi = $this->db->where('pin',$pin)->where('tgl',$tgl)->get('presensi')->row();
                        if($check_presensi)
                        {
                            $this->db->where('pin',$pin)->where('tgl',$tgl)->delete('presensi');
                        }
                    }

                    $ins    = array(
                            "nip"           => $nip,
                            "pin"           => $pin,
                            "tgl"           => $tgl,
                            "scan_masuk"    => $scan_masuk,
                            "scan_pulang"   => $scan_pulang,
                            "status"        => $status,
                            'user'      => 'Manual Excel'
                           );

                    $this->db->where('pin',$pin)->where('tgl',$tgl)->delete('terlambat_temp');
                    if($scan_masuk != '00:00:00' && (strtotime($scan_masuk)>$masuk))
                    {
                        $menit  = round((strtotime($scan_masuk) - $masuk)/60);
                        ($menit == 0) ? $menit = 1 : $menit = $menit;
                        $ins_telat  = array(
                            "nip"           => $nip,
                            "tgl"           => $tgl,
                            "total_menit"   => $menit,
                            "pin"           => $pin,
                            "scan_masuk"    => $scan_masuk,
                            "shift"     => $nama_shift,
                            "jam_masuk" => $jam_masuk
                            );
                        
                        $this->db->insert('terlambat_temp', $ins_telat);
                    }

                $check_absensi = $this->db->where(array('nip' => $nip, 'tgl' => $tgl, 'flag' => 1))->get('tb_absensi')->row();
                if(empty($check_absensi)){
                    $this->db->insert('tb_absensi', $ins);
                }
            }
        }
    }

    function upload_rekrutmen($filename)
    {
        ini_set('memory_limit', '-1');
        $inputFileName = './assets/uploads/'.$filename;
        try {
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch(Exception $e) {
        die('Error loading file :' . $e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);

        for ($i=2; $i < ($numRows+1) ; $i++) { 
            if($worksheet[$i]["A"] == ""){

            }else{
                ($worksheet[$i]["B"]) ? $tgl_lahir = $this->Main_Model->convert_tgl_excel($worksheet[$i]["B"]) : $tgl_lahir = '';
                $ins = array(
                        "nama" => $worksheet[$i]["A"],
                        "tgl_lahir" => $tgl_lahir,
                        "jen_kel" => $worksheet[$i]["C"],
                        "alamat" => $worksheet[$i]["D"],
                        "hp"  => $worksheet[$i]["E"],
                        "email" => $worksheet[$i]["F"],
                        "id_pend" => $worksheet[$i]["G"],
                        "institusi" => $worksheet[$i]["H"],
                        "pengalaman" => $worksheet[$i]["I"],
                        "status" => $worksheet[$i]["J"]
                       );

                $this->db->insert('cal_kary', $ins);
            }
        }
    }

    function upload_jadwal($filename='')
    {
        ini_set('memory_limit', '-1');
        ini_set('MAX_EXECUTION_TIME', '-1');
        set_time_limit(0);
        $inputFileName = './assets/uploads/'.$filename;
        try {
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        } catch (Exception $e) {
            die('Error loading file :'.$e->getMessage());
        }

        $worksheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $numRows = count($worksheet);
        $bulan = strtotime($worksheet[3]['E']);
        $bulan = date('Y-m', $bulan);
        $username = $this->session->userdata('username');

        $arr_day = ['E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI'];

        $arr_tgl = [];
        for ($i = 0; $i < count($arr_day); $i++) {
            $str_tgl = $worksheet[5][$arr_day[$i]];
            
            if (strlen($str_tgl) == 1) {
                $str_tgl = '0'.$str_tgl;
            }

            if ($str_tgl != '') {
                $arr_tgl[$arr_day[$i]] = $bulan.'-'.$str_tgl;
            }
        }

        $jadwal = [];
        for ($i = 6; $i <= $numRows; $i++) {
            for ($a = 0; $a < count($arr_tgl); $a++) {
                $nip = trim($worksheet[$i]['C']);
                $shift = $worksheet[$i][$arr_day[$a]];
                $tgl = $arr_tgl[$arr_day[$a]];

                $cari_shift = $this->db->where('nama', $shift)
                                ->get('ms_shift')
                                ->row();
                $id_shift = isset($cari_shift->id_shift) ? $cari_shift->id_shift : '';
                $jam_masuk = isset($cari_shift->jam_masuk) ? $cari_shift->jam_masuk : '';
                $jam_pulang = isset($cari_shift->jam_pulang) ? $cari_shift->jam_pulang : '';
                $keterangan = isset($cari_shift->keterangan) ? $cari_shift->keterangan : '';

                if ($shift != '' && $tgl != '' && $nip != '') {
                    $jadwal[] = array(
                        'nip' => $nip,
                        'tgl' => $tgl,
                        'id_shift' => $id_shift,
                        'keterangan' => $keterangan,
                        'user_insert' => $username,
                        'template' => 0,
                        'ts' => 1
                    );

                    $condition = array(
                        'nip' => $nip,
                        'tgl' => $tgl
                    );

                    $this->Main_Model->generate_lembur($nip, $tgl, $id_shift);

                    // $holiday = $this->db->where('hol_tgl', $tgl)
                    //                     ->get('hol')
                    //                     ->row();

                    // $week = date('w', strtotime($tgl));
                    // if ($week == '0' || $week == '6' && ! empty($holiday)) {
                    //     if ($shift != 'L' && $shift != 'R') {
                    //         // is libur nasional or libur hari besar
                    //         $is_libur = isset($holiday->libur) ? $holiday->libur : '';

                    //         if ($is_libur == 1) {
                    //             $hari = ' Hari Libur Besar';
                    //         } elseif ($is_libur == 0) {
                    //             $hari = ' Hari Libur Nasional';
                    //         } else {
                    //             $hari = ($week == 0) ? ' Hari Minggu' : ' Hari Sabtu'; 
                    //         }

                    //         // data lembur
                    //         $keterangan_lembur = $keterangan.$hari;

                    //         $mulai = $tgl.' '.$jam_masuk;
                    //         if (strtotime($jam_masuk) > strtotime($jam_pulang)) {
                    //             $date = date('Y-m-d', strtotime("$tgl + 1 days"));
                    //             $date = $date.' '.$jam_pulang;

                    //             $selesai = $date;
                    //         } else {
                    //             $date = $tgl.' '.$jam_pulang;

                    //             $selesai = $date;
                    //         }

                    //         $jml_jam = $this->db->query("SELECT TIMESTAMPDIFF(HOUR, '$mulai', '$selesai') AS jumlah")->row();
                    //         $jumlah_jam = isset($jml_jam->jumlah) ? $jml_jam->jumlah : 0;

                    //         if ($is_libur == 1) {
                    //             $id_overtime = 20;
                    //         } else {
                    //             if ($jumlah_jam == 0) {
                    //                 $id_overtime = 0;
                    //                 $keterangan_lembur = $shift;
                    //             } else if ($jumlah_jam <= 5) {
                    //                 $id_overtime = 22; 
                    //             } else if ($jumlah_jam <= 8) {
                    //                 $id_overtime = 19;
                    //             } else {
                    //                 $id_overtime = 18;
                    //             }
                    //         }

                    //         $over = $this->Tunjangan_Model->id_overtime($id_overtime);
                    //         $flag = isset($over->flag) ? $over->flag : 0;
                    //         $nominal = isset($over->nominal) ? $over->nominal : 0;
                    //         if ($flag == 1) {
                    //             if ($jumlah_jam >= 5) {
                    //                 $jumlah_jam = 5;
                    //             }

                    //             $total = $nominal * $jumlah_jam;
                    //         } else {
                    //             $total = $nominal;
                    //         }

                    //         if ($id_overtime == 0) {
                    //             $total = 0;
                    //         }

                    //         $data_lembur = array(
                    //             'id_overtime' => $id_overtime,
                    //             'nip' => $nip,
                    //             'tgl' => $tgl,
                    //             'jml_jam' => $jumlah_jam,
                    //             'lembur' => $total,
                    //             'starttime' => $jam_masuk,
                    //             'endtime' => $jam_pulang,
                    //             'keterangan' => $keterangan_lembur,
                    //             'status' => 1,
                    //             'insert_at' => now(),
                    //             'user_insert' => username()
                    //         );

                    //         $this->Main_Model->delete_data('lembur', $condition);
                    //         $this->db->insert('lembur', $data_lembur);
                    //     }
                    // }
                    $this->Main_Model->delete_data('tr_shift', $condition);
                }
            }
        }
        
        $this->db->insert_batch('tr_shift', $jadwal);
    }
}
?>