<?php

class Main_Model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Api_Model', '', true);
    }
    
    function uang($angka = '')
    {
        return number_format($angka, 0, ".", ".");
    }
    
    function tanggal($tanggal = '')
    {
        $create = date_create($tanggal);
        return date_format($create, "d/m/Y");
    }
    
    function convert_tgl($tanggal = '')
    {
        if ($tanggal) {
            $tgl_ex = explode("/", $tanggal);

            $tahun = isset($tgl_ex[2]) ? $tgl_ex[2] : '';
            $bulan = isset($tgl_ex[1]) ? $tgl_ex[1] : '';
            $day = isset($tgl_ex[0]) ? $tgl_ex[0] : '';

            if ($tahun != '' && $bulan != '' && $day != '') {
                return $tahun.'-'.$bulan.'-'.$day;
            } else {
                return '';
            }

            // return $tgl_ex[2] . "-" . $tgl_ex[1] . "-" . $tgl_ex[0];
        } else {
            return null;
        }
    }

    function format_tgl($tanggal = '')
    {
        $create = date_create($tanggal);
        if ($tanggal != '') {
            return date_format($create, "d F Y");
        } else {
            return '';
        }
    }

    function tgl_indo($tanggal = '')
    {
        $m_create = date_create($tanggal);
        $m_format = date_format($m_create, 'n');

        $d_format = date_format($m_create, 'd');
        $y_format = date_format($m_create, 'Y');

        switch ($m_format) {
            case '1':
                $bulan = 'Januari';
                break;
            case '2':
                $bulan = 'Februari';
                break;
            case '3':
                $bulan = 'Maret';
                break;
            case '4':
                $bulan = 'April';
                break;
            case '5':
                $bulan = 'Mei';
                break;
            case '6':
                $bulan = 'Juni';
                break;
            case '7':
                $bulan = 'Juli';
                break;
            case '8':
                $bulan = 'Agustus';
                break;
            case '9':
                $bulan = 'September';
                break;
            case '10':
                $bulan = 'Oktober';
                break;
            case '11':
                $bulan = 'Nopember';
                break;
            case '12':
                $bulan = 'Desember';
                break;
            
            default:
                $bulan = '';
                break;
        }

        return $d_format.' '.$bulan.' '.$y_format;
    }

    function default_datepicker()
    {
        $script = '$(".date-picker").datepicker({rtl: App.isRTL(), orientation: "left", autoclose: !0 });';
        return $script;
    }

    function tbl_temp($id = 'dataTables-example')
    {
        $template = array(
                'table_open'            => '<table class="table table-striped table-bordered table-hover table-checkable order-column" id="'.$id.'">',

                'thead_open'            => '<thead>',
                'thead_close'           => '</thead>',

                'heading_row_start'     => '<tr>',
                'heading_row_end'       => '</tr>',
                'heading_cell_start'    => '<th>',
                'heading_cell_end'      => '</th>',

                'tbody_open'            => '<tbody>',
                'tbody_close'           => '</tbody>',

                'row_start'             => '<tr>',
                'row_end'               => '</tr>',
                'cell_start'            => '<td>',
                'cell_end'              => '</td>',

                'row_alt_start'         => '<tr>',
                'row_alt_end'           => '</tr>',
                'cell_alt_start'        => '<td>',
                'cell_alt_end'          => '</td>',

                'table_close'           => '</table>'
        );
        
        return $template;
    }

    function default_loadtable($url, $method = 'load_table()', $div = 'myTable', $id = 'dataTables-example', $onsuccess = '')
    {
        $script = 'function '.$method.'{
                        $.ajax({
                            url     : "'.$url.'",
                            success : function(data){
                                $("#'.$div.'").html(data);
                                '.$this->default_datatable($id).$onsuccess.'
                            },
                            error   : function(jqXHR, textStatus, errorThrown){
                                bootbox.alert("Oops Something went wrong!");
                            }
                        });
                    }
                    '.$method.';';
        return $script;
    }

    function ajax($url = '', $method = '', $type = '', $data = '', $onsuccess = '', $dataType = '', $beforeSend = '', $onComplete = '', $param = '')
    {
        $script = 'function '.$method.'
                    {
                        $.ajax({
                            url     : "'.$url.'"'.$param.',
                            '.$type
                             .$data
                             .$dataType
                             .$beforeSend
                             .$onComplete.'
                            success : function(data){
                                '.$onsuccess.'
                            },
                            error   : function(jqXHR, textStatus, errorThrown){
                                '.$this->Main_Model->notif500().'
                            }
                        });
                    }';

        return $script;
    }

    function image_load($id_img = 'imgload', $class_img = '')
    {
        $img = '<img id="'.$id_img.'" src="'.base_url('assets/img/loading.gif').'" class="hidden '.$class_img.'" />';

        return $img;
    }

    function post_data($url, $method = 'save()', $data = '', $onsuccess = '')
    {
        $script = 'function '.$method.'
                        {
                            $.ajax({
                                url     : "'.$url.'",
                                type    : "POST",
                                data    : '.$data.',
                                dataType: "JSON",
                                success : function(data){
                                    '.$onsuccess.'
                                },
                                error   : function(jqXHR, textStatus, errorThrown){
                                    '.$this->Main_Model->notif500().'
                                }
                            });
                        }';

        return $script;
    }

    function get_data($url, $method = 'get_id(id)', $data = '')
    {
        $script = 'function '.$method.'
                        {
                            $.ajax({
                                url     : "' .$url. '/"+id,
                                dataType: "JSON",
                                success : function(data){
                                    '.$data.'
                                },
                                error : function(jqXHR, textStatus, errorThrown){
                                    '.$this->notif500().'
                                }
                            });
                        }';

        return $script;
    }

    function notif400()
    {
        $script = ' bootbox.alert({
                        message: "Form masih ada yg kosong!",
                        size: "small"
                    });';

        return $script;
    }

    function notif200()
    {
        $script = ' bootbox.alert({
                        message: "Success!",
                        size: "small"
                    });';

        return $script;
    }

    function notif()
    {
        $script = ' 
                    function notif(p){bootbox.alert({message: p, size: "small"}); }
                    ';

        return $script;
    }

    function notif500()
    {
        $script = ' bootbox.alert({
                        message: "Terjadi Kesalahan!",
                        size: "small"
                    });';

        return $script;
    }

    function default_delete_data($url, $load = 'load_table();')
    {
        $script = 'function delete_data(id)
                {
                    id = {"id":id}

                    bootbox.dialog({
                        message : "Yakin ingin menghapus data?",
                        title : "Hapus Data",
                        buttons :{
                            danger : {
                                label : "Delete",
                                className : "red",
                                callback : function(){
                                    $.ajax({
                                        url : "' . $url . '",
                                        type : "POST",
                                        data : id,
                                        success : function(data){
                                            // bootbox.alert({
                                            //     message: "Delete Success",
                                            //     size: "small"
                                            // });
                                            toastr.success("", "Data telah dihapus");

                                            ' . $load . '
                                        },
                                        error : function(jqXHR, textStatus, errorThrown){
                                            bootbox.alert("Terjadi Kesalahan");
                                        }
                                    });
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    })

                }

        ';

        return $script;
    }

    function default_action($id = '')
    {
        $script = '<div class="btn-group dropup">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="javascript:;" onclick="get_id(' . $id . ');">
                                <i class="icon-edit"></i> Update </a>
                        </li>
                        <li>
                            <a href="javascript:;" onclick="delete_data(' . $id . ');">
                                <i class="icon-delete"></i> Delete </a>
                        </li>
                    </ul>
            </div>';
        return $script;
    }

    function action($data = '')
    {
        $script = '<div class="btn-group dropup" style="position:relative;">
                <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                    <i class="fa fa-angle-down"></i>
                </button>
                    <ul class="dropdown-menu" role="menu" style="position:relative;">';
        if (!empty($data)) {
            for ($i=0; $i<count($data); $i++) {
                $event = isset($data[$i]['event']) ? $data[$i]['event'] : '';
                $label = isset($data[$i]['label']) ? $data[$i]['label'] : '';
                $url = isset($data[$i]['url']) ? $data[$i]['url'] : 'javascript:;';

                $script .= '<li> <a href="'.$url.'" onclick="'.$event.';"> <i class="icon-edit"></i> '.$label.' </a> </li>';
            }
        }

        $script .= '</ul>
            </div>';

        return $script;
    }

    function default_datatable($id = 'dataTables-example')
    {
        $js =   '$("#'.$id.'").DataTable({
                    responsive: true
                });';
        return $js;
    }

    function default_select2()
    {
        $js = '$(document).ready(function() {
                    $(".select2").select2();
                });';

        return $js;
    }
    
    function login($username = '', $password = '')
    {
        $query =  $this->db->query("
            SELECT * FROM users 
            WHERE usr_pass = AES_ENCRYPT('$password','hr') 
            AND usr_name = '$username'
            AND usr_active = '1'")->row();

        if (! empty($query)) {
            return $query;
        } else {
            $db = $this->connect_to_absensi();
            $q = $db->where('username', $username)
                ->where('password', md5($password))
                ->where('status', 1)
                ->get('ms_user')
                ->row();

            $nip = isset($q->id_employee) ? $q->id_employee : '';
            if ($nip != '') {
                $reminder = $this->db->where('nip', $nip)
                            ->get('ms_reminder')
                            ->row();

                $usr = isset($reminder->user) ? $reminder->user : '';
                return $this->db->query("
                    SELECT * FROM users 
                    WHERE usr_name = '$usr'
                    AND usr_active = '1' ")->row();
            } else {
                return [];
            }
        }
    }
    
    function get_login()
    {
        if ($this->session->userdata('username') == '' || $this->session->userdata('loggedin') != 1 || $this->session->userdata('idp') == '' || $this->session->userdata('acctype') == '' || $this->session->userdata('id_user') == '') {
            redirect('main/logout');
        } else {
            $this->get_username($this->session->userdata('id_user'));
        }
        
        if ($this->session->userdata('acctype') == 'Operator') {
            redirect('akun/dashboard');
        }
    }
    
    function guest_login()
    {
        if ($this->session->userdata('username') == '' || $this->session->userdata('loggedin') != 1 || $this->session->userdata('idp') == '' || $this->session->userdata('acctype') == '' || $this->session->userdata('id_user') == '') {
            redirect('main/logout');
        } else {
            $this->get_username($this->session->userdata('id_user'));
        }
    }


    function all_login()
    {
        $acctype = $this->session->userdata('acctype');
        ($acctype == 'Administrator') ? $this->get_login() : $this->guest_login();
    }
    
    
    function get_username()
    {
        $usr_id = $this->session->userdata('id_user');
        $data = $this->db->query("
                    SELECT * 
                    FROM users a 
                    WHERE a.`usr_id` = '$usr_id'")->row();

        return $data;
    }
    
    function periode_opt()
    {
        $opt = $this->db->query("SELECT qd_id,DATE_FORMAT(tgl_akhir,'%M %Y') AS periode 
            FROM q_det WHERE DATE_FORMAT(tgl_akhir, '%Y') = YEAR(NOW()) AND aktif = 1")->result();
        foreach ($opt as $row) {
            $result[$row->qd_id] = $row->periode;
        }
        return $result;
    }

    function periode_opt_this_year()
    {
        $opt = $this->db->query("SELECT qd_id,DATE_FORMAT(tgl_akhir,'%M %Y') AS periode FROM q_det WHERE DATE_FORMAT(tgl_akhir, '%Y') = YEAR(NOW())")->result();
        foreach ($opt as $row) {
            $result[$row->qd_id] = $row->periode;
        }
        return $result;
    }

    function all_periode_opt()
    {
        $opt = $this->db->query("SELECT qd_id,DATE_FORMAT(tgl_akhir,'%M %Y') AS periode FROM q_det WHERE aktif = '1'")->result();
        foreach ($opt as $row) {
            $result[$row->qd_id] = $row->periode;
        }
        return $result;
    }
    
    function all_kary()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->query("SELECT * FROM kary a WHERE a.`idp` = '$idp' ORDER BY a.`nama` ASC")->result();
        if ($opt) {
            foreach ($opt as $row) {
                $result[$row->nip] = $row->nip . " - " . $row->nama;
            }
        } else {
            $result[''] = '';
        }
        return $result;
    }

    function kary_active()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->query("
            SELECT * 
            FROM kary a 
            WHERE ISNULL(a.`tgl_resign`) 
            AND a.`idp` = '$idp' 
            ORDER BY a.`nama` ASC")->result();

        return $opt;
    }
    
    function all_kary_active()
    {
        $idp = $this->session->userdata('idp');
        $opt = $this->db->query("SELECT * FROM kary a WHERE ISNULL(a.`tgl_resign`) AND a.`idp` = '$idp' ORDER BY a.`nama` ASC")->result();
        $result = array();
        if ($opt) {
            foreach ($opt as $row) {
                $result[$row->nip] = $row->nip . " - " . $row->nama;
            }
        }
        return $result;
    }
    
    function opt_idp()
    {
        $opt = $this->db->query("select * from idp order by idp asc")->result();
        foreach ($opt as $row) {
            $result[$row->idp] = $row->bu;
        }
        return $result;
    }
    
    function kary_nip($nip = '')
    {
        return $this->db->query("select * from kary where nip='$nip'")->row();
    }
    
    function all_kary_nonstaff()
    {
        $opt = $this->db->query("SELECT d.`nip`,d.`nama`,a.`posisi`,a.`jab`,a.`sal_pos` FROM pos a JOIN pos_sto b ON a.`id_pos`=b.`id_pos` JOIN sk c ON b.`id_sto`=c.`id_pos_sto` LEFT JOIN kary d ON c.`nip`=d.`nip` WHERE a.`sns`='NON-STAFF' AND ISNULL(d.`tgl_resign`) AND c.`aktif`='1'")->result();
        foreach ($opt as $row) {
            $result[$row->nip] = $row->nip . " - " . $row->nama . " - " . $row->sal_pos;
        }
        return $result;
    }
    
    function register($data)
    {
        $this->db->query($data);
    }
    
    function update_usr($data)
    {
        $this->db->query($data);
    }
    
    function view_user()
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("SELECT a.`usr_id`,b.`nip`,b.`nama`,a.`usr_id`,a.`usr_name`,AES_DECRYPT(a.`usr_pass`,'hr') AS usr_pass FROM users a JOIN kary b ON a.`nip`=b.`nip` LEFT JOIN lku c ON b.`lokasi_kerja` = c.`id_lku` WHERE c.`idp` = '$idp' ORDER BY b.`nama`")->result();
    }
    
    function user_id($id)
    {
        return $this->db->query("
            SELECT a.`usr_id`,b.`nip`,b.`nama`,a.`usr_id`,a.`usr_name`,AES_DECRYPT(a.`usr_pass`,'hr') AS usr_pass FROM users a 
            JOIN kary b ON a.`nip`=b.`nip` 
            WHERE a.`usr_id`='$id' ORDER BY b.`nama`
            ")->row();
    }
    
    function delete_usr($id)
    {
        $this->db->where('usr_id', $id)->delete('users');
    }
    
    function all_bu()
    {
        $opt = $this->db->from('idp')->get()->result();
        foreach ($opt as $row) {
            $result[$row->idp] = $row->bu;
        }
        return $result;
    }
    
    function fk_y()
    {
        $opt = $this->db->query("select distinct(th) as th from fk")->result();
        foreach ($opt as $row) {
            $result[$row->th] = $row->th;
        }
        return $result;
    }
    
    function cuti_y()
    {
        $opt = $this->db->query("select distinct(th) as th from cuti_dep")->result();
        foreach ($opt as $row) {
            $result[$row->th] = $row->th;
        }
        return $result;
    }
    
    function insert_q()
    {
        $bln = date("n");
        $thn = date("Y");
        
        $cek = $this->db->query("select * from q where th='$thn'")->num_rows();
        
        if ($cek < 0) {
            for ($i = 1; $i <= 4; $i++) {
                $data = array(
                    'q_name' => 'Q' . $i . ' ' . $thn,
                    'th' => $thn
                );
                $this->db->insert('q', $data);
            }
        }
    }
    
    function cuti_dep($year)
    {
        $this->db->query("CALL insert_cuti_dep('$year')");
    }
    
    function jml_kontrak_habis($tgl_awal = '', $tgl_akhir = '')
    {
        $idp = $this->session->userdata('idp');

        $id_cabang = $this->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= ' AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " f.`id_cab` = '$id_cabang[$i]'";

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        return $this->db->query("
            SELECT count(a.nip) as jml FROM kont a 
            INNER JOIN kary b ON a.`nip`=b.`nip` 
            INNER JOIN sk c ON a.`nip`=c.`nip` 
            INNER JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` 
            INNER JOIN pos e ON d.`id_pos`=e.`id_pos` 
            INNER JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab` 
            WHERE a.`tgl_akhir` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND a.`aktif`='1' 
            AND c.`aktif`='1'
            $cabang ")->row();
    }

    function jml_kontrak_baru($tgl_awal = '', $tgl_akhir = '')
    {
        $idp = $this->session->userdata('idp');

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= ' AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " f.`id_cab` = '$id_cabang[$i]'";

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $id_divisi = $this->Main_Model->session_divisi();
        $divisi = '';
        if (! empty($id_divisi)) {
            $divisi .= ' AND( ';
            for ($i = 0; $i < count($id_divisi); $i++) {
                $divisi .= " g.id_divisi = '$id_divisi[$i]' ";
                if (end($id_divisi) != $id_divisi[$i]) {
                    $divisi .= ' OR ';
                } else {
                    $divisi .= ' ) ';
                }
            }
        }

        return $this->db->query("
            SELECT COUNT(*) AS jml 
            FROM kont a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN sk c ON a.`nip`=c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto` 
            JOIN pos e ON d.`id_pos`=e.`id_pos` 
            JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab` 
            JOIN ms_divisi g ON g.id_divisi = e.id_divisi
            WHERE a.`tgl_awal` BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND a.`aktif`='1' 
            AND c.`aktif`='1' 
            $cabang $divisi")->row();

        // return $this->db->query("
        //     SELECT count(a.nip) as jml FROM kont a
        //     INNER JOIN kary b ON a.`nip`=b.`nip`
        //     INNER JOIN sk c ON a.`nip`=c.`nip`
        //     INNER JOIN pos_sto d ON c.`id_pos_sto`=d.`id_sto`
        //     INNER JOIN pos e ON d.`id_pos`=e.`id_pos`
        //     INNER JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab`
        //     WHERE a.`tgl_awal` BETWEEN '$tgl_awal' AND '$tgl_akhir'
        //     AND a.`aktif`='1'
        //     AND c.`aktif`='1'
        //     $cabang ")->row();
    }

    function jml_tidak_ada_kontrak($tgl_awal = '', $tgl_akhir = '')
    {
        $idp    = $this->session->userdata('idp');
        // return $this->db->query("SELECT COUNT(a.`nip`) jml FROM kary a WHERE NOT EXISTS (SELECT c.`nip` FROM kont c WHERE c.`nip`=a.`nip`) AND ISNULL(a.`tgl_resign`)AND a.`kary_stat`='KONTRAK' AND a.`idp` = '$idp'")->row();
        return $this->db->query("SELECT COUNT(a.`nip`) jml FROM kary a WHERE NOT EXISTS (SELECT c.`nip` FROM kont c WHERE c.`nip`=a.`nip`) AND ISNULL(a.`tgl_resign`)AND a.`kary_stat`='KONTRAK' AND a.`tgl_masuk` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`idp` = '$idp'")->row();
    }
    
    function per_skr()
    {
        $year = date("Y");
        $month = date("m");
        // $tgl_awal  = date( 'Y-m-d' ,strtotime('-1 month',strtotime($year . '-' . $month . '-26')));
        // $tgl_akhir = $year . '-' . $month . '-25';
        // $tgl_awal  = date( 'Y-m' ,strtotime('-1 month',strtotime($year . '-' . $month)));
        $ym = $year . '-' . $month;
        
        return $this->db->query("
            SELECT *,DATE_FORMAT(a.`tgl_akhir`,'%M %Y') periode 
            FROM q_det a 
            WHERE DATE_FORMAT(a.`tgl_akhir`,'%Y-%m') = '$ym'")->row();
    }
    
    function kary_baru($tgl_awal = '', $tgl_akhir = '')
    {
        // return $this->db->query("SELECT COUNT(nip) jml FROM kary a WHERE a.`tgl_masuk`>='$tgl_awal' AND a.`tgl_masuk`<='$tgl_akhir'")->row();
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT COUNT(a.`nip`) jml FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE a.`tgl_kerja` BETWEEN '$tgl_awal' AND '$tgl_akhir' AND a.`id_tipe_sk`='5' AND c.`idp`='$idp'")->row();
    }
    
    function resign_baru($tgl_awal = '', $tgl_akhir = '')
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
                SELECT COUNT(nip) jml 
                FROM kary a 
                WHERE a.`tgl_resign`>='$tgl_awal' 
                AND a.`tgl_resign`<='$tgl_akhir' 
                AND a.`idp`='$idp'")->row();
    }
    function jml_a($tgl_awal = '', $tgl_akhir = '')
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT count(a.`nip`) jmla FROM kary a WHERE ISNULL(a.`tgl_resign`) AND a.`idp`='$idp' AND a.`tgl_masuk` BETWEEN '$tgl_awal' AND '$tgl_akhir'")->row();
    }
    function jml_b($tgl_awal = '', $tgl_akhir = '')
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT count(a.`nip`) jmlb FROM sk a JOIN kary b ON a.`nip` = b.`nip` WHERE a.`aktif`='1' AND ISNULL(b.`tgl_resign`) AND b.`idp`='$idp' AND b.`tgl_masuk` BETWEEN '$tgl_awal' AND '$tgl_akhir'")->row();
    }

    function jumlah_karyawan_baru()
    {
        return $this->db->query("
            SELECT DATE_FORMAT(a.`tgl_akhir`, '%M %Y') AS periode,
            IFNULL(COUNT(k.nip), 0) AS jumlah 
            FROM q_det a
            LEFT JOIN (
                SELECT tgl_masuk, nip  
                FROM kary 
                WHERE ISNULL(tgl_resign)
            ) AS k ON k.tgl_masuk BETWEEN a.`tgl_awal` AND a.`tgl_akhir`
            WHERE YEAR(a.`tgl_akhir`) = YEAR(CURDATE()) 
            GROUP BY a.`tgl_akhir`")->result();
    }

    function jumlah_karyawan_resign()
    {
        return $this->db->query("
            SELECT DATE_FORMAT(a.`tgl_akhir`, '%M %Y') AS periode,
            IFNULL(COUNT(k.nip), 0) AS jumlah 
            FROM q_det a
            LEFT JOIN (
                SELECT tgl_resign, nip  
                FROM kary 
                WHERE tgl_resign <> ''
            ) AS k ON k.tgl_resign BETWEEN a.`tgl_awal` AND a.`tgl_akhir`
            WHERE YEAR(a.`tgl_akhir`) = YEAR(CURDATE()) 
            GROUP BY a.`tgl_akhir`")->result();
    }

    function per()
    {
        return $this->db->query("SELECT a.`qd_id`,DATE_FORMAT(a.`tgl_akhir`,'%b') AS bln,a.`tgl_awal`,a.`tgl_akhir` FROM q_det a WHERE DATE_FORMAT(a.`tgl_akhir`,'%Y') = YEAR(NOW())")->result();
    }
    
    function sum_thp($id_per)
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT SUM(a.`thp`) AS thp FROM p_hist a JOIN kary b ON a.`nip`=b.`nip` WHERE a.`id_per` = '$id_per' AND b.`idp`='$idp'")->row();
    }
    
    function jml_kary_baru($tgl_awal = '', $tgl_akhir = '')
    {
        $idp = $this->session->userdata('idp');
        return $this->db->query("
            SELECT COUNT(kary.nip) AS jml 
            FROM kary 
            JOIN sk ON kary.nip = sk.nip 
            WHERE kary.tgl_masuk BETWEEN '$tgl_awal' AND '$tgl_akhir' 
            AND kary.idp='$idp' 
            AND sk.aktif='1'")->row();
    }

    function jumlah_karyawan($tgl_awal = '', $tgl_akhir = '')
    {
        return $this->db->query("
                SELECT COUNT(*) AS jml
                FROM kary a
                WHERE (ISNULL(a.`tgl_resign`)
                OR (a.`tgl_resign` BETWEEN '$tgl_awal' AND '$tgl_akhir'))
                AND a.`tgl_masuk` <= '$tgl_akhir'")->row();
    }

    function jml_blm_ijazah()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT COUNT(a.`nip`) jml FROM kary a WHERE isnull(a.`no_ijazah`) and isnull(a.`tgl_resign`) AND a.`idp`='$idp'")->row();
    }

    function jml_surat_komitmen()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT COUNT(a.`nip`) jml FROM kary a WHERE (ISNULL(a.`surat_komitmen`) OR a.`surat_komitmen`='BELUM' OR isnull(a.`file_surat_komitmen`)) AND ISNULL(a.`tgl_resign`) AND a.`idp` = '$idp'")->row();
    }

    function jml_mutasi()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT COUNT(a.`nip`) jml FROM sk a JOIN kary b ON a.`nip`=b.`nip` WHERE ISNULL(b.`tgl_resign`) AND a.`aktif` = '1' AND (a.`id_tipe_sk`='1' OR a.`id_tipe_sk`='4' OR a.`id_tipe_sk`='6')")->row();
    }

    function jml_surat_kesanggupan()
    {
        $idp    = $this->session->userdata('idp');
        return $this->db->query("SELECT COUNT(a.`nip`) jml FROM kary a JOIN tb_sp b ON a.`nip`=b.`nip` WHERE b.`aktif`='1' AND b.`idp`='$idp' AND ISNULL(b.`surat_kesanggupan`)")->row();
    }
    
    function posisi($nip = '')
    {
        return $this->db->query("
            SELECT f.nip, f.nama, f.mail, d.id_cab, c.sal_pos, 
            e.id_divisi, c.jab, d.cabang 
            FROM sk a 
            LEFT JOIN pos_sto b ON a.`id_pos_sto` = b.`id_sto` 
            LEFT JOIN pos c ON b.`id_pos` = c.`id_pos` 
            LEFT JOIN ms_cabang d ON c.`id_cabang` = d.`id_cab` 
            LEFT JOIN ms_divisi e ON c.`id_divisi` = e.`id_divisi` 
            LEFT JOIN kary f ON f.nip = a.nip
            WHERE a.`aktif` = '1' 
            AND a.`nip` = '$nip'")->row();
    }
    
    function ceknip($nip)
    {
        $q = $this->db->query("select * from kary where nip = '$nip' and isnull(tgl_resign)")->num_rows();
        
        if ($q > 0) {
            $w = $this->db->query("select * from users where nip = '$nip'")->num_rows();
            if ($w > 0) {
                $e      = $this->db->query("select * from users where nip = '$nip'")->row();
                $result = "false|<div class='alert alert-danger' id='warning'>NIP " . $nip . " sudah terdaftar ! <br> Username : " . $e->usr_name . '</div>';
            } else {
                $result = "true|";
            }
        } else {
            $result = "false|<div class='alert alert-danger' id='warning'>NIP " . $nip . " tidak terdaftar atau sudah resign.</div>";
        }
        
        return $result;
    }
    
    function ceknip_tgl($nip, $tgl)
    {
        $q = $this->db->query("select * from kary where nip = '$nip' and tgl_lahir = '$tgl'")->num_rows();
        if ($q > 0) {
            $result = "true|";
        } elseif ($q == 0) {
            $result = "false|<div class='alert alert-danger' id='warning'>Tanggal Lahir tidak sesuai dengan NIP anda</div>";
        }
        return $result;
    }

    function lku()
    {
        // $idp = $this->session->userdata("idp");
        $q = $this->db->query("SELECT * FROM lku a JOIN lwok b ON a.`id_lwok`=b.`id_lwok`")->result();
        foreach ($q as $row) {
            $result[$row->id_lku] = $row->lku;
        }
        return $result;
    }

    function lku_idp()
    {
        $idp = $this->session->userdata("idp");
        $q = $this->db->query("SELECT * FROM lku a JOIN lwok b ON a.`id_lwok`=b.`id_lwok` WHERE b.`idp`='$idp'")->result();
        foreach ($q as $row) {
            $result[$row->id_lku] = $row->lku;
        }
        return $result;
    }

    function kary_bbp()
    {
        $idp = $this->session->userdata("idp");
        $q = $this->db->query("SELECT a.`nip`,d.`nama`,c.`jab` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` JOIN kary d ON a.`nip`=d.`nip` WHERE a.`aktif`='1' AND c.`idp`='$idp' AND ISNULL(d.`tgl_resign`) AND (c.`jab`='Branch Manager' OR c.`jab`='General Manager' OR c.`jab`='Manager' OR c.`jab`='Officer 1' OR c.`jab`='Officer 2' OR c.`jab`='Supervisor' OR c.`jab`='Vice President') order by d.`nama` asc")->result();
        if ($q) {
            foreach ($q as $row) {
                $result[$row->nip] = $row->nip.' - '.$row->nama;
            }
        } else {
            $result[''] = '';
        }
        return $result;
    }

    function kary_staff()
    {
        $idp = $this->session->userdata("idp");
        $q = $this->db->query("SELECT a.`nip`,d.`nama`,c.`sns`,c.`jab` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON c.`id_pos`=b.`id_pos` JOIN kary d ON a.`nip`=d.`nip` WHERE a.`aktif`='1' AND c.`idp`='$idp' AND c.`sns`='STAFF' AND isnull(d.`tgl_resign`) ORDER BY d.`nama` ASC")->result();
        
        return $q;
    }

    function kary_staff_opt()
    {
        $idp = $this->session->userdata("idp");
        $q = $this->db->query("SELECT a.`nip`,d.`nama`,c.`sns`,c.`jab` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON c.`id_pos`=b.`id_pos` JOIN kary d ON a.`nip`=d.`nip` WHERE a.`aktif`='1' AND c.`idp`='$idp' AND c.`sns`='STAFF' AND isnull(d.`tgl_resign`) ORDER BY d.`nama` ASC")->result();
        if ($q) {
            foreach ($q as $row) {
                $result[$row->nip] = $row->nip.' - '.$row->nama;
            }
        } else {
            $result[''] = '';
        }
        return $result;
    }

    function sisa_cuti($nip = '', $th = '')
    {
        return $this->db->query("
            SELECT a.`nip`,a.`nama`,IFNULL(b.`qt`,0) AS qt,IFNULL(COUNT(d.`tgl`+1),0) AS terpakai, (IFNULL(b.`qt`,0) - IFNULL(COUNT(d.`tgl`+1),0)) AS sisa 
            FROM kary a 
            LEFT OUTER JOIN cuti_dep b ON b.`nip` = a.`nip` AND b.`th` = '$th' 
            LEFT OUTER JOIN cuti_det c ON c.`cuti_dep` = b.`id_cuti` 
            LEFT OUTER JOIN cuti_sub_det d ON d.`id_cuti_det` = c.`id_cuti_det` 
            WHERE a.`tgl_resign` IS NULL 
            AND a.`nip`='$nip' 
            GROUP BY a.`nip`")->row();
    }

    function generate_cuti($th = '', $nip = '')
    {
        if ($th == '') {
            $th = date('Y');
        }
        $today = today();
        $condition = ($nip != '') ? " AND nip = '$nip' " : "";
        $karyawan = $this->db->query("
                SELECT nip, tgl_masuk, 
                DATE_FORMAT(tgl_masuk, '%m-%d') AS bulan,
                DATE_FORMAT(tgl_masuk + INTERVAL 12 MONTH, '%Y-%m-%d') AS tgl_hak,
                DATE_FORMAT(tgl_masuk + INTERVAL 6 MONTH, '%Y-%m-%d') AS tgl_hutang  
                FROM kary 
                WHERE ISNULL(tgl_resign)
                AND YEAR(tgl_masuk) <= '$th'
                $condition ")->result();

        $result = [];
        foreach ($karyawan as $row) {
            $a = $th.'-'.$row->bulan;
            $b = $this->db->query("SELECT DATE_FORMAT('$a' + INTERVAL 12 MONTH, '%Y-%m-%d') AS akhir")->row();

            $awal = $a;
            $akhir = isset($b->akhir) ? $b->akhir : '';

            $today = date('Y-m-d');

            // jumlah cuti dalam satu tahun
            $c_thn = $this->db->query("
                SELECT * 
                FROM cuti_sub_det a
                JOIN cuti_det b ON a.`id_cuti_det` = b.`id_cuti_det`
                JOIN ms_status_approval c ON c.kode = b.approval
                WHERE a.`tgl` BETWEEN '$awal' AND '$akhir'
                AND b.`nip` = '$row->nip' 
                AND b.`approval` = '12'")->num_rows();
            $result[$row->nip]['awal'] = $awal;
            $result[$row->nip]['akhir'] = $akhir;
            $result[$row->nip]['c_thn'] = $c_thn; 

            // cari hari libur
            $libur = $this->db->query("
                SELECT * 
                FROM cuti_sub_det a
                JOIN cuti_det b ON a.`id_cuti_det` = b.`id_cuti_det`
                JOIN ms_status_approval c ON c.kode = b.approval
                LEFT JOIN hol d ON d.hol_tgl = a.tgl
                WHERE a.`tgl` BETWEEN '$awal' AND '$akhir'
                AND b.`nip` = '$row->nip' 
                AND b.`approval` = '12'
                AND (d.hol_tgl IS NOT NULL
                OR DAYOFWEEK(a.tgl) = 1 
                OR DAYOFWEEK(a.tgl) = 7)")->num_rows();
            $result[$row->nip]['libur'] = $libur;

            // potong cuti bersama
            $c_bersama = $this->db->query("
                SELECT * 
                FROM hol a
                LEFT JOIN tr_shift b ON a.hol_tgl = b.tgl
                LEFT JOIN ms_shift c ON c.id_shift = b.id_shift
                WHERE a.cuti = 1
                AND c.jam_masuk = '00:00:00'
                AND c.jam_pulang = '00:00:00'
                AND a.`hol_tgl` BETWEEN '$awal' AND '$akhir'
                AND a.`hol_tgl` <= CURDATE()
                AND b.nip = '$row->nip'")->num_rows();
            $result[$row->nip]['c_bersama'] = $c_bersama;

            if ($row->tgl_hak <= $awal) {
                $qt = 12 - (($c_thn - $libur) + $c_bersama);
            } else {
                // $diff = $this->diff_month($row->tgl_masuk, $today);
                // if ($diff >= 6) {
                //     $qt = 6 - ($c_thn + $c_bersama);
                // } else {
                //     $qt = 0;
                // }

                $qt = 0;
            }
            $result[$row->nip]['qt'] = $qt;

            $exists = $this->db->query("
                    SELECT * 
                    FROM cuti_dep a 
                    WHERE a.`nip` = '$row->nip' 
                    AND a.`tgl_awal` = '$awal'
                    AND a.tgl_akhir = '$akhir'")->row();

            if (!empty($exists)) {
                $this->db->where('nip', $row->nip)
                ->where('tgl_awal', $awal)
                ->where('tgl_akhir', $akhir)
                ->update('cuti_dep', array('qt' => $qt));
            } else {
                if ($awal <= $today) {
                    $this->db->query("
                        INSERT INTO cuti_dep (nip, th, qt, tgl_hak_cuti, tgl_awal, tgl_akhir)
                        VALUES('$row->nip', '$th', '$qt', '$row->tgl_hak', '$awal', '$akhir')");
                }
            }

            $this->db->where('tgl_awal >', $today)
                ->delete('cuti_dep');
        }

        return json_encode($result);
    }

    function th_cuti()
    {
        return $this->db->query("SELECT MAX(th) th FROM cuti_dep")->row();
    }

    function th_bpjs()
    {
        return $this->db->query("SELECT MAX(th) th FROM bpjs")->row();
    }

    function th_bpjs_ks()
    {
        return $this->db->query("SELECT MAX(th) th FROM bpjs_ks")->row();
    }

    function th_fk()
    {
        return $this->db->query("SELECT MAX(th) th FROM fk")->row();
    }

    function lokasi_kerja($nip)
    {
        return $this->db->query("SELECT c.`id_lku` FROM kary a JOIN sk b ON a.`nip`=b.`nip` JOIN pos_sto c ON b.`id_pos_sto`=c.`id_sto`WHERE a.`nip` = '$nip' AND b.`aktif`= '1'")->row();
    }

    function fk_lku($lku, $th)
    {
        return $this->db->query("SELECT * FROM fk WHERE id_lku = '$lku' AND th = '$th' ")->row();
    }

    function kelas_bpjs($nip)
    {
        return $this->db->query("SELECT a.`kelas` FROM bpjs_ks_det a WHERE a.`nip` = '$nip'")->row();
    }

    function bpjs($th, $tipe)
    {
        if ($tipe=="ks") {
            $result = $this->db->query("SELECT * FROM bpjs_ks WHERE th = '$th'")->row();
        } else {
            $result = $this->db->query("SELECT * FROM bpjs WHERE th = '$th'")->row();
        }

        return $result;
    }

    function fk_th($th = '')
    {
        if ($th=='') {
            $q  = $this->th_fk();
            $th = isset($q->th)?$q->th:'';
        }
        $idp = $this->session->userdata('idp');
        return $this->db->query("
            SELECT * 
            FROM fk a 
            JOIN lku b ON a.`id_lku`=b.`id_lku` 
            WHERE a.`th`='$th'")->result();
    }

    // style & js

    function js_counter()
    {
        $js     = '
                <script type="text/javascript" src="'.base_url('assets/plugins/counterup/jquery.waypoints.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/plugins/counterup/jquery.counterup.min.js').'"></script>';
        return $js;
    }

    function js_flot()
    {
        $js     = '
                <script src="' . base_url('assets/plugins/flot/jquery.flot.min.js') . '" type="text/javascript"></script>
                <script src="' . base_url('assets/plugins/flot/jquery.flot.resize.min.js') . '" type="text/javascript"></script>
                <script src="' . base_url('assets/plugins/flot/jquery.flot.categories.min.js') . '" type="text/javascript"></script>';
        return $js;
    }

    function style_datatable()
    {
        $style  = '<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/datatables.min.css') . '">
            <link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') . '">';

        return $style;
    }

    function js_datatable()
    {
        $js     = '<script src="' . base_url('assets/plugins/datatables/datatables.min.js') . '"></script>
            <script src="' . base_url('assets/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') . '"></script>';

        return $js;
    }

    function style_modal()
    {
        $style  = '<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') . '">
            <link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-modal/css/bootstrap-modal.css') . '">';

        return $style;
    }

    function js_modal()
    {
        $js     = '<script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') . '"></script>
            <script src="' . base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') . '"></script>';

        return $js;
    }

    function js_bootbox()
    {
        $js     = '<script src="' . base_url('assets/plugins/bootbox/bootbox.min.js') . '"></script>';
        return $js;
    }

    function style_datepicker()
    {
        $style  = '<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') . '">';

        return $style;
    }

    function js_datepicker()
    {
        $js     = '<script src="' . base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') . '"></script>';
        return $js;
    }

    function style_select2()
    {
        $style  = '<link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2.min.css') . '">
                <link rel="stylesheet" type="text/css" href="' . base_url('assets/plugins/select2/css/select2-bootstrap.min.css') . '">';

        return $style;
    }

    function js_button_excel()
    {
        $js = '<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/dataTables.buttons.min.js"></script>
                <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/pdfmake.min.js"></script>
                <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.24/build/vfs_fonts.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.4/js/buttons.html5.min.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>';
        return $js;
    }

    function js_select2()
    {
        $js     = '<script src="' . base_url('assets/plugins/select2/js/select2.full.min.js') . '"></script>';
        return $js;
    }

    function js_ckeditor()
    {
        $js = '<script src="'.base_url('assets/plugins/ckeditor/ckeditor.js').'"></script>';
        return $js;
    }

    function style_fileinput()
    {
        $style = '<link href="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css').'" rel="stylesheet" type="text/css" />';
        return $style;
    }

    function js_fileinput()
    {
        $js    = '<script src="'.base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js').'"></script>';
        return $js;
    }

    function style_timepicker()
    {
        $style = '<link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'"> <link rel="stylesheet" type="text/css" href="'.base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'">';
        return $style;
    }

    function js_timepicker()
    {
        $js    = '<script src="'.base_url('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'"></script> <script src="'.base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'"></script>';
        return $js;
    }

    function js_validate()
    {
        $js     = '<script src="' . base_url('assets/plugins/jquery-validation/js/jquery.validate.min.js') . '" type="text/javascript"></script> <script src="' . base_url('assets/plugins/jquery-validation/js/additional-methods.min.js') . '" type="text/javascript"></script>';
        return $js;
    }

    function style_file_upload()
    {
        $style  = '<link href="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.css') . '" rel="stylesheet" type="text/css" />';
        return $style;
    }

    function js_file_upload()
    {
        $js     = '<script src="' . base_url('assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js') . '"></script>';
        return $js;
    }

    function js_fullcalendar()
    {
        $js = '<script src="' . base_url('assets/plugins/fullcalendar/fullcalendar.min.js') . '" type="text/javascript"></script>';
        return $js;
    }

    function style_fullcalendar()
    {
        $style = '<link href="' . base_url('assets/plugins/fullcalendar/fullcalendar.min.css') . '" rel="stylesheet" type="text/css" />';
        return $style;
    }

    function js_moment()
    {
        $js = '<script src="' . base_url('assets/plugins/moment.min.js') . '" type="text/javascript"></script>';
        return $js;
    }

    function style_bs_select()
    {
        $style = '<link href="' . base_url('assets/plugins/bootstrap-select/css/bootstrap-select.min.css') . '" rel="stylesheet" type="text/css" />';
        return $style;
    }

    function js_bs_select()
    {
        $js = '<script src="' . base_url('assets/plugins/bootstrap-select/js/bootstrap-select.min.js') . '" type="text/javascript"></script>';
        return $js;
    }

    function style_datetimepicker()
    {
        $style = '<link href="' . base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') . '" rel="stylesheet" type="text/css" />';
        return $style;
    }

    function js_datetimepicker()
    {
        $js = '<script src="' . base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') . '" type="text/javascript"></script>';
        return $js;
    }

    function js_summernote()
    {
        $js = '<script src="'.base_url('assets/plugins/bootstrap-summernote/summernote.min.js').'" type="text/javascript"></script>';
        return $js;
    }

    function style_summernote()
    {
        $css = '<link href="'.base_url('assets/plugins/bootstrap-summernote/summernote.css').'" rel="stylesheet" type="text/css" />';
        return $css;
    }

    function style_tree()
    {
        $css = '<link href="'.base_url('assets/plugins/jstree/dist/themes/default/style.min.css').'" rel="stylesheet" type="text/css" />';
        return $css;
    }

    function js_tree()
    {
        $js = '<script src="'.base_url('assets/plugins/jstree/dist/jstree.min.js').'" type="text/javascript"></script>';
        return $js;
    }

    function Get_Date_Difference($start_date = '', $end_date = '')
    {
        $diff   = abs(strtotime($end_date) - strtotime($start_date));
        $years  = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $days   = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
        
        $data   = array('year' => $years, 'month'=> $months);

        return $data;
    }

    function diff_month($start_date = '', $end_date = '')
    {
        $diff = abs(strtotime($end_date) - strtotime($start_date));
        $months = floor(($diff) / (30 * 60 * 60 * 24));

        return $months;
    }

    function kekata($x = '')
    {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x < 12) {
            $temp = " ". $angka[$x];
        } elseif ($x < 20) {
            $temp = $this->kekata($x - 10). " belas";
        } elseif ($x < 100) {
            $temp = $this->kekata($x / 10)." puluh". $this->kekata($x % 10);
        } elseif ($x < 200) {
            $temp = " seratus" . $this->kekata($x - 100);
        } elseif ($x < 1000) {
            $temp = $this->kekata($x / 100) . " ratus" . $this->kekata($x % 100);
        } elseif ($x < 2000) {
            $temp = " seribu" . $this->kekata($x - 1000);
        } elseif ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " ribu" . $this->kekata($x % 1000);
        } elseif ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " juta" . $this->kekata($x % 1000000);
        } elseif ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " milyar" . $this->kekata(fmod($x, 1000000000));
        } elseif ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
            return $temp;
    }
     
     
    function terbilang($x = '', $style = 4)
    {
        if ($x < 0) {
            $hasil = "minus ". trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    function last_day($month = '', $year = '')
    {
        if (empty($month)) {
            $month = date('m');
        }
        
        if (empty($year)) {
            $year = date('Y');
        }
        
        $result = strtotime("{$year}-{$month}-01");
        $result = strtotime('-1 second', strtotime('+1 month', $result));
     
        return date('Y-m-d', $result);
    }

    function cari_idp($idp = '')
    {
        return $this->db->query("select * from idp where idp = '$idp'")->row();
    }

    function convert_tgl_excel($date = '')
    {
        if ($date) {
            $a = date_create($date);
            $b = date_format($a, 'Y-m-d');
        } else {
            $b = '0000-00-00';
        }

        return $b;
    }

    function set_upload_options($upload_path = '', $file_type = '', $max_size = '', $file_name = '')
    {
        //upload an image options
        $config = array();
        ($upload_path != '') ? $config['upload_path'] = $upload_path : $config['upload_path'] = '';
        ($file_type != '') ? $config['allowed_types'] = $file_type : $config['allowed_types'] = '';
        ($max_size != '') ? $config['max_size'] = $max_size : $config['max_size'] = '';
        ($file_name != '') ? $config['file_name'] = $file_name : $config['file_name'] = '';
        $config['overwrite']     = false;
        
        return $config;
    }

    function kary_lembur()
    {
        return $this->db->query("
            SELECT a.`nip`,b.`nama`,d.`jab`,d.`sal_pos` 
            FROM sk a 
            JOIN kary b ON a.`nip`=b.`nip` 
            JOIN pos_sto c ON a.`id_pos_sto`=c.`id_sto` 
            JOIN pos d ON d.`id_pos`=c.`id_pos` 
            WHERE a.`aktif` = '1' 
            ORDER BY b.`nama` ASC")->result();
    }

    function id_periode($qd_id = '')
    {
        return $this->db->query("
            SELECT *,DATE_FORMAT(tgl_akhir,'%M %Y') periode, 
            DATE_FORMAT(tgl_akhir,'%m') bulan, 
            DATE_FORMAT(tgl_akhir,'%Y') tahun  
            FROM q_det 
            WHERE qd_id = '$qd_id'")->row();
    }

    function js_amchart()
    {
        return '<script src="'.base_url('assets/plugins/amcharts/amcharts/amcharts.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/serial.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/pie.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/radar.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/themes/light.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/themes/patterns.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/themes/chalk.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/ammap/ammap.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/ammap/maps/js/worldLow.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amstockcharts/amstock.js').'" type="text/javascript"></script>
        <script src="'.base_url('assets/plugins/amcharts/amcharts/plugins/dataloader/dataloader.min.js').'" type="text/javascript"></script>';
    }

    function highchart()
    {
        // return '<script src="'.base_url('assets/plugins/highcharts/highcharts.js').'" type="text/javascript"></script>
        // <script src="'.base_url('assets/plugins/highcharts/modules/exporting.js').'" type="text/javascript"></script>';
        return '<script src="https://code.highcharts.com/highcharts.js"></script> <script src="https://code.highcharts.com/modules/exporting.js"></script>';
    }

    function pie_chart()
    {
        $per = $this->per_skr();
        $tgl_awal = $per->tgl_awal;
        $tgl_akhir = $per->tgl_akhir;
        $a = $this->db->query("
            SELECT a.`cabang`, (SELECT COUNT(sk.`nip`) FROM sk 
            JOIN pos_sto ON sk.`id_pos_sto`=pos_sto.`id_sto` 
            JOIN pos ON pos.`id_pos`=pos_sto.`id_pos` 
            JOIN kary ON sk.`nip`=kary.`nip` 
            WHERE sk.`aktif`='1' AND pos.`id_cabang`=a.`id_cab` 
            AND kary.`tgl_masuk` BETWEEN '$tgl_awal' AND '$tgl_akhir') jml 
            FROM ms_cabang a 
            WHERE a.`status`='1'")->result();

        return $a;
    }

    function kary_cabang($cabang = '', $q = '', $jab = '')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' c.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if (!empty($sal_pos)) {
            $kelas .= ' AND (';
            for ($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " c.`sal_pos` = '$sal_pos[$i]'";

                if (end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " c.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $query = $this->db->query("
            SELECT d.`nip`,d.`nama`,c.`jab`, d.`pin` FROM sk a 
            JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` 
            JOIN pos c ON b.`id_pos`=c.`id_pos` 
            JOIN kary d ON a.`nip`=d.`nip` 
            WHERE a.`aktif`='1' 
            $cabang $kelas $div 
            AND ISNULL(d.`tgl_resign`)
            ORDER BY d.`nama` ASC")->result();

        $result = array();
        foreach ($query as $row) {
            $label = ($jab == '') ? ' - '.$row->jab : '';
            $result[$row->nip] = $row->nip.' - '.$row->nama.$label;
        }

        if ($q == '') {
            return $result;
        } else {
            return $query;
        }
    }

    function date_difference($date_start = '', $date_end = '')
    {
        return $this->db->query("SELECT DATEDIFF('$date_end','$date_start') days")->row();
    }

    function notif_action($message = '', $label = 'OK', $classname = 'blue', $callback = 'location.reload();')
    {
        return 'bootbox.dialog({
                    message : '.$message.',
                    buttons : {
                        main : {
                            label : "'.$label.'",
                            className : "'.$classname.'",
                            callback : function(){
                                '.$callback.'
                                return true;
                            }
                        }
                    }
                });';
    }

    function lastpos($nip = '')
    {
        return $this->db->query("SELECT c.`jab`,d.`id_lku` FROM sk a JOIN pos_sto b ON a.`id_pos_sto`=b.`id_sto` JOIN pos c ON b.`id_pos`=c.`id_pos` join ms_cabang d on c.`id_cabang`=d.`id_cab` WHERE a.`nip` = '$nip' ORDER BY a.`id_sk` DESC")->row();
    }

    function timepicker()
    {
        $result = 'jQuery().timepicker && ($(".timepicker-default").timepicker({autoclose   : !0, showSeconds : !0, minuteStep  : 1 }), $(".timepicker-no-seconds").timepicker({autoclose   : !0, minuteStep  : 5 }), $(".timepicker-24").timepicker({autoclose   : !0, minuteStep  : 5, showSeconds : !1, showMeridian: !1 }), $(".timepicker").parent(".input-group").on("click", ".input-group-btn", function(t) {t.preventDefault(), $(this).parent(".input-group").find(".timepicker").timepicker("showWidget") }), $(document).scroll(function() {$("#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24").timepicker("place") }));';
        return $result;
    }

    function view_by_id($table = '', $condition = '', $row = 'row')
    {
        if ($row == 'row') {
            if ($condition) {
                return $this->db->where($condition)->get($table)->row();
            } else {
                return $this->db->get($table)->row();
            }
        } else {
            if ($condition) {
                return $this->db->where($condition)->get($table)->result();
            } else {
                return $this->db->get($table)->result();
            }
        }
    }

    function process_data($table = '', $data = '', $condition = '')
    {
        if ($condition) {
            $this->db->where($condition)->update($table, $data);
        } else {
            $this->db->insert($table, $data);
        }
        return $this->db->affected_rows();
    }

    function delete_data($table = '', $condition = '')
    {
        $this->db->where($condition)->delete($table);
        return $this->db->affected_rows();
    }

    function data_menu_admin($parent = '', $user)
    {
        $acctype = $this->session->userdata('acctype');
        $username = $this->session->userdata('username');
        if ($acctype == 'Administrator') {
            return $this->db->query("
                SELECT a.*, COALESCE(Deriv1.`Count`,0) hitung 
                FROM `menu` a  
                LEFT OUTER JOIN (
                    SELECT parent, COUNT(*) AS COUNT 
                    FROM `menu` 
                    GROUP BY parent
                ) Deriv1 ON a.`id_menu` = Deriv1.`parent` 
                WHERE a.`user`= '$user' 
                AND a.`status`= 1 
                AND a.`parent`='$parent' 
                ORDER BY a.`id_menu` ASC")->result();
        } else {
            return $this->db->query("
                SELECT b.*, COALESCE(d.jml, 0) hitung 
                FROM tb_user_menu a
                JOIN menu b ON a.`id_menu` = b.id_menu
                LEFT OUTER JOIN (
                    SELECT parent, COUNT(*) AS jml
                    FROM menu
                    GROUP BY parent
                ) AS d ON a.`id_menu` = d.parent
                WHERE a.usr_name = '$username' 
                AND a.`parent`='$parent' 
                ORDER BY a.`id_menu` ASC ")->result();
        }
    }

    function menu_admin($parent = '', $level = '', $active)
    {
        $result     = $this->data_menu_admin($parent, 1);

        if ($result) {
            if ($level == 0) {
                $return     = '<ul class="nav navbar-nav">';
            } elseif ($level == 1) {
                $return     = '<ul class="dropdown-menu dropdown-menu-fw ">';
            } else {
                $return     = '<ul class="dropdown-menu">';
            }

            $menu_active    = $this->session->userdata('url');
            foreach ($result as $row) {
                if ($row->link) {
                    $link   = base_url().$row->link;
                } else {
                    $link   = 'javascript:;';
                }


                if ($row->hitung>0) {
                    if ($level==0) {
                        if ($row->id_menu==$active) {
                            $return .= '<li class="dropdown dropdown-fw active open selected">';
                        } else {
                            $return .= '<li class="dropdown dropdown-fw">';
                        }
                    } elseif ($level>0 && $row->hitung>0) {
                        $return .= '<li class="dropdown more-dropdown-sub">';
                    } else {
                        $return .= '<li>';
                    }
                    
                    $return .= '<a href="'.$link.'" class="text-uppercase"><i class="'.$row->icon.'"></i> '.$row->title.' </a>';
                       

                    $return .= $this->menu_admin($row->id_menu, $level + 1, $active);
                    $return .= '</li>';
                } elseif ($row->hitung==0) {
                    if ($level==0) {
                        if ($row->id_menu==$active) {
                            $return .= '<li class="dropdown dropdown-fw active open selected">';
                        } else {
                            $return .= '<li class="dropdown dropdown-fw">';
                        }
                    } else {
                        $return .= '<li>';
                    }
                    $return .= '<a href="'.$link.'" class="text-uppercase"><i class="'.$row->icon.'"></i> '.$row->title.' </a></li>';
                } else {
                }
            }
            $return     .= '</ul>';
        } else {
            $return = '';
        }

        return $return;
    }


    function menu_user($parent = '', $level = '', $active)
    {
        $result     = $this->data_menu_admin($parent, 2);

        if ($result) {
            if ($level == 0) {
                $return     = '<ul class="nav navbar-nav">';
            } elseif ($level == 1) {
                $return     = '<ul class="dropdown-menu dropdown-menu-fw ">';
            } else {
                $return     = '<ul class="dropdown-menu">';
            }

            $menu_active    = $this->session->userdata('url');
            foreach ($result as $row) {
                if ($row->link) {
                    $link   = base_url().$row->link;
                } else {
                    $link   = 'javascript:;';
                }


                if ($row->hitung>0) {
                    if ($level==0) {
                        if ($row->id_menu==$active) {
                            $return .= '<li class="dropdown dropdown-fw active open selected">';
                        } else {
                            $return .= '<li class="dropdown dropdown-fw">';
                        }
                    } elseif ($level>0 && $row->hitung>0) {
                        $return .= '<li class="dropdown more-dropdown-sub">';
                    } else {
                        $return .= '<li>';
                    }
                    
                    $return .= '<a href="'.$link.'" class="text-uppercase"><i class="'.$row->icon.'"></i> '.$row->title.' </a>';
                       

                    $return .= $this->menu_user($row->id_menu, $level + 1, $active);
                    $return .= '</li>';
                } elseif ($row->hitung==0) {
                    if ($level==0) {
                        if ($row->id_menu==$active) {
                            $return .= '<li class="dropdown dropdown-fw active open selected">';
                        } else {
                            $return .= '<li class="dropdown dropdown-fw">';
                        }
                    } else {
                        $return .= '<li>';
                    }
                    $return .= '<a href="'.$link.'" class="text-uppercase"><i class="'.$row->icon.'"></i> '.$row->title.' </a></li>';
                } else {
                }
            }
            $return     .= '</ul>';
        } else {
            $return = '';
        }

        return $return;
    }

    function jumlah_terlambat()
    {
        // $default_shift = $this->Api_Model->default_shift();
        // $time = isset($default_shift->jam_masuk) ? $default_shift->jam_masuk : '08:00:59';
        // $timestamp = strtotime($time) + 120*60;
        // $pulang = date('H:i:s', $timestamp);

        return $this->db->query("
            SELECT ms_cabang.`cabang` AS country, 
            IFNULL(t.visits,0) AS visits, '#B0DE09' color 
            FROM ms_cabang 
            LEFT JOIN (
            SELECT e.`cabang` AS country, IFNULL(COUNT(a.`nip`),0) AS visits, e.`id_cab`
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            LEFT JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            LEFT JOIN (
                SELECT tb_scanlog.`scan_date`, nip 
                FROM tb_scanlog 
                WHERE CAST(tb_scanlog.`scan_date` AS DATE) = CURDATE()
                GROUP BY tb_scanlog.`nip`
                ORDER BY tb_scanlog.`scan_date` ASC 
                ) AS scan ON scan.nip = a.`nip`
            LEFT JOIN (
                SELECT ms_shift.`jam_masuk`, tr_shift.`nip`, ms_shift.`jam_pulang` FROM tr_shift 
                JOIN ms_shift ON ms_shift.`id_shift` = tr_shift.`id_shift`
                WHERE tr_shift.`tgl` = CURDATE()
                ) AS jadwal ON jadwal.nip = a.`nip`
            WHERE ISNULL(a.`tgl_resign`) AND b.`aktif` = '1'
            AND CAST(scan.scan_date AS TIME) > IFNULL(jadwal.jam_masuk, (SELECT jam_masuk FROM ms_shift WHERE ms_shift.`default_shift` = '1'))
            GROUP BY e.`id_cab`
            ) AS t ON t.id_cab = ms_cabang.`id_cab`
            WHERE ms_cabang.`status` = 1")->result();
    }

    function jumlah_absen()
    {
        return $this->db->query("
            SELECT e.`cabang`, IFNULL(COUNT(scanlog.pin), '') AS jumlah FROM kary a
            INNER JOIN sk b ON a.`nip` = b.`nip`
            INNER JOIN pos_sto c ON b.`id_pos_sto` = c.`id_sto`
            INNER JOIN pos d ON c.`id_pos` = d.`id_pos`
            INNER JOIN ms_cabang e ON d.`id_cabang` = e.`id_cab`
            LEFT JOIN (
                SELECT DISTINCT(pin) AS pin FROM tb_scanlog
                WHERE DATE(scan_date) = CURDATE()
            ) AS scanlog ON scanlog.pin = a.`pin`
            WHERE b.`aktif` = 1
            GROUP BY e.`id_cab`")->result();
    }

    function footer($js = '')
    {
        return '<!-- BEGIN FOOTER -->
                        <p class="copyright">'.date("Y").'© '.company().' App</p>
                        <a href="#index" class="go2top">
                            <i class="icon-arrow-up"></i>
                        </a>
                    <!-- END FOOTER -->
                    </div>
                </div>

                <script type="text/javascript" src="'.base_url('assets/js/jquery.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/plugins/bootstrap/js/bootstrap.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/js/app.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/plugins/layout5/scripts/layout.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/js/jquery.blockui.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/plugins/bootstrap-toastr/toastr.min.js').'"></script>
                <script type="text/javascript" src="'.base_url('assets/js/ui-toastr.js').'"></script>

                <script type="text/javascript">
                    var base_url = "'.base_url().'";
                </script>

                '.$js;
    }

    function close_page()
    {
        return '<script type="text/javascript">
                    function ChangePT(id){
                        $.ajax({
                            url : "'.base_url('main/changept').'/"+id,
                            success : function(data){
                                location.reload();
                            },
                            error : function(jqXHR, textStatus, errorThrown){
                                alert("Oops Something went wrong !");
                            } 
                        })
                    }
                </script>

                </body>
                </html>';
    }

    function is_approval()
    {
        $user = $this->session->userdata('username');
        $q = $this->view_by_id('users', array('usr_name' => $user, 'approval' => 1), 'row');
        ($q) ? $result = true : $result = false;
        return $result;
    }

    // function connect_to_ga()
    // {
    //     $config['hostname'] = '119.2.53.122';
    //     $config['username'] = 'gmedia_psp';
    //     $config['password'] = 'FvmeASScpKQ*';
    //     $config['database'] = 'gmedia_gapsp';
    //     $config['dbdriver'] = "mysqli";
    //     $config['dbprefix'] = "";
    //     $config['pconnect'] = FALSE;
    //     $config['db_debug'] = TRUE;

    //     $db_new = $this->load->database($config, TRUE);
    //     return $db_new;
    // }

    // function connect_to_mkios()
    // {
    //     $config['hostname'] = '119.2.53.122';
    //     $config['username'] = 'gmedia_psp';
    //     $config['password'] = 'FvmeASScpKQ*';
    //     $config['database'] = 'gmkios_psp';
    //     $config['dbdriver'] = "mysqli";
    //     $config['dbprefix'] = "";
    //     $config['pconnect'] = FALSE;
    //     $config['db_debug'] = TRUE;

    //     $db_new = $this->load->database($config, TRUE);
    //     return $db_new;
    // }

    function connect_to_absensi()
    {
        $config['hostname'] = '103.255.240.2';
        $config['username'] = 'erp';
        $config['password'] = 'tz6yhyrune';
        $config['database'] = 'absensi';
        $config['dbdriver'] = 'mysqli';
        $config['dbprefix'] = '';
        $config['pconnect'] = false;
        $config['db_debug'] = true;

        $db_new = $this->load->database($config, true);
        return $db_new;
    }

    function check_user($nip = '')
    {
        $db = $this->connect_to_absensi();
        $query = $db->where('id_employee', $nip)
                    ->get('ms_user')
                    ->row();

        $result = (!empty($query)) ? false : true;

        return $result;
    }

    function max_kode($id_cabang = '')
    {
        $q = $this->view_by_id('ms_cabang', array('id_cab' => $id_cabang), 'row');
        $profitcode = isset($q->profitcode) ? $q->profitcode : '';
        $prefix = isset($q->prefix) ? $q->prefix : '';

        $m = date('m');
        $y = date('y');

        $maks = '';
        if ($prefix) {
            $maks .= $prefix.'/';
        } else {
            $maks .= 'DL/';
        }

        if (!empty($m) && !empty($y)) {
            $maks .= $m.$y.'/';
        }

        $jumlah = strlen($maks);

        $db = $this->connect_to_ga();
        $result = $db->query("SELECT COALESCE(RIGHT(MAX(a.`nobukti`),4),0) + 1 maks FROM jurnalumum a WHERE LEFT(a.`nobukti`, $jumlah) = '$maks'")->row();

        if (!empty($result)) {
            $n = isset($result->maks) ? $result->maks : '0';
            if (strlen($n) == 4) {
                $no = $n;
            } elseif (strlen($n) == 3) {
                $no = '0'.$n;
            } elseif (strlen($n) == 2) {
                $no = '00'.$n;
            } else {
                $no = '000'.$n;
            }
        } else {
            $no = '0001';
        }
        $res_number = $maks.$no;

        return $res_number;
    }

    function bukti_perdin($id_cabang = '')
    {
        $q = $this->view_by_id('ms_cabang', array('id_cab' => $id_cabang), 'row');
        $profitcode = isset($q->profitcode) ? $q->profitcode : '';
        $prefix = isset($q->prefix) ? $q->prefix : '';

        $m = date('m');
        $y = date('y');

        $maks = '';
        if ($prefix) {
            $maks .= $prefix.'/';
        } else {
            $maks .= 'DL/';
        }

        if (!empty($m) && !empty($y)) {
            $maks .= $m.$y.'/';
        }

        $jumlah = strlen($maks);
        $result = $this->db->query("SELECT COALESCE(RIGHT(MAX(a.`nobukti`),4),0) + 1 maks FROM perdin a WHERE LEFT(a.`nobukti`, $jumlah) = '$maks'")->row();

        if (!empty($result)) {
            $n = isset($result->maks) ? $result->maks : '0';
            if (strlen($n) == 4) {
                $no = $n;
            } elseif (strlen($n) == 3) {
                $no = '0'.$n;
            } elseif (strlen($n) == 2) {
                $no = '00'.$n;
            } else {
                $no = '000'.$n;
            }
        } else {
            $no = '0001';
        }
        $res_number = $maks.$no;

        return $res_number;
    }

    function verif_perdin($id_perdin = '')
    {
        $dperdin = $this->view_by_id('perdin', array('id_perdin' => $id_perdin), 'row');
        $nobukti = isset($dperdin->nobukti) ? $dperdin->nobukti : '';
        $tgl_berangkat = isset($dperdin->tgl_brgkt) ? $dperdin->tgl_brgkt : '';
        $explode_tgl = explode(' ', $tgl_berangkat);
        $tgl = $explode_tgl[0];
        $tujuan = isset($dperdin->tujuan) ? $dperdin->tujuan : '';
        $keperluan = isset($dperdin->keperluan) ? $dperdin->keperluan : '';
        $keterangan = 'Perjalanan Dinas. Tujuan : '.$tujuan.' Keperluan : '.$keperluan;
        $kodeakun = '711';
        $total = isset($dperdin->total) ? $dperdin->total : '0';
        $user = isset($dperdin->user_insert) ? $dperdin->user_insert : '';
        $tgl_insert = date('Y-m-d H:i:s');
        $status = 1;
        $id_cabang = isset($dperdin->id_cabang) ? $dperdin->id_cabang : '';

        $cabang = $this->view_by_id('ms_cabang', array('id_cab' => $id_cabang), 'row');
        $kode_lokasi = isset($cabang->profitcode) ? $cabang->profitcode : '';

        $data = array(
            array(
                'nobukti' => $nobukti,
                'tgl' => $tgl,
                'keterangan' => $keterangan,
                'kodeakun' => $kodeakun,
                'debit' => $total,
                'kredit' => 0,
                'userid' => $user,
                'usertgl' => $tgl_insert,
                'kode_lokasi' => $kode_lokasi,
                'status' => $status
            ),
            array(
                'nobukti' => $nobukti,
                'tgl' => $tgl,
                'keterangan' => $keterangan,
                'kodeakun' => $kodeakun,
                'debit' => 0,
                'kredit' => $total,
                'userid' => $user,
                'usertgl' => $tgl_insert,
                'kode_lokasi' => $kode_lokasi,
                'status' => $status
            )
        );
        $db = $this->connect_to_ga();
        for ($i=0; $i<count($data); $i++) {
            $db->insert('jurnalumum', $data[$i]);
        }
    }

    function send_mail($to = '', $message = '', $subject = '', $from = '', $attach = '', $flag = 0)
    {
        $this->load->library('email');
        $email = $to;
        $bcc_mail = array('victor.fendi@gmedia.co.id', 'rinda.resi@gmedia.co.id');
        // $from_mail = 'no-reply@hrd.putmasaripratama.co.id';

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => '111.68.27.2',
            'smtp_port' => '25',
            'smtp_user' => $from, // change it to yours
            // 'smtp_pass' => 'k1k2k3k4', // change it to yours
            'smtp_timeout' => '7',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => true
        );
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($from, '[Aplikasi HRD] Gmedia'); // change it to yours
            $this->email->bcc($bcc_mail);
            $this->email->to($email);// change it to yours
            $this->email->subject($subject);
            $this->email->message($message);
        if ($attach != '') {
            if ($flag > 0) {
                foreach ($attach as $row) {
                    $this->email->attach($row);
                }
            } else {
                $this->email->attach($attach);
            }
        }

        if ($this->email->send()) {
            return true;
        } else {
            show_error($this->email->print_debugger());
            return false;
        }
            
            // return $this->email->print_debugger();
    }

    function kirim_email($to = '', $cc = '', $message = '', $subject = '', $from = '', $attach = '', $flag = 0)
    {
        $this->load->library('email');
        $bcc_mail = array('victor.fendi@gmedia.co.id', 'rinda.resi@gmedia.co.id');

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => '111.68.27.2',
            'smtp_port' => '25',
            'smtp_user' => $from, // change it to yours
            // 'smtp_pass' => 'k1k2k3k4', // change it to yours
            'smtp_timeout' => '7',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => true
        );
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($from, '[Aplikasi HRD] Gmedia'); // change it to yours
            $this->email->bcc($bcc_mail);
        if (! empty($cc)) {
            $this->email->cc($cc);
        }
            $this->email->to($to);// change it to yours
            $this->email->subject($subject);
            $this->email->message($message);
        if ($attach != '') {
            if ($flag > 0) {
                foreach ($attach as $row) {
                    $this->email->attach($row);
                }
            } else {
                $this->email->attach($attach);
            }
        }

        if ($this->email->send()) {
            return true;
        } else {
            // show_error($this->email->print_debugger());
            return false;
        }
            
            // return $this->email->print_debugger();
    }

    function reminder_pengajuan_karyawan($data = '', $kode = '')
    {
        $id_cabang = isset($data['id_cabang']) ? $data['id_cabang'] : '';
        $posisi = isset($data['posisi']) ? $data['posisi'] : '';
        $tgl_pengajuan = isset($data['tgl_pengajuan']) ? $data['tgl_pengajuan'] : '';
        $jumlah = isset($data['jumlah']) ? $data['jumlah'] : '';
        $spesifikasi = isset($data['spesifikasi']) ? $data['spesifikasi'] : '';
        $sal_pos = isset($data['sal_pos']) ? $data['sal_pos'] : '';

        $p = $this->view_by_id('ms_status_approval', array('kode' => $kode));
        $privilege = isset($p->privilege) ? $p->privilege : '';

        $usr = $this->view_by_id('tb_user_cabang', array('id_cabang' => $id_cabang), 'result');
        foreach ($usr as $row) {
            // contact
            $contact = $this->reminder_contact($row->usr_name, $sal_pos);
            $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
            $hp = isset($contact['hp']) ? $contact['hp'] : '08156647203';
            $user = isset($contact['user']) ? $contact['user'] : '';

            // email
            $subject = '[Aplikasi HRD] Reminder Pengajuan Karyawan';
            $no = 1;
            $message = 'Berikut adalah Daftar Pengajuan Karyawan yang butuh diverifikasi';
            $table = '<table border = "1">
                            <tr><th>No</th><th>Posisi</th><th>Tanggal Pengajuan</th><th>Jumlah</th><th>Spesifikasi</th></tr>';
            $table .= '<tr><td>'.$no++.'</td><td>'.$posisi.'</td><td>'.$tgl_pengajuan.'</td><td>'.$jumlah.'</td><td>'.$spesifikasi.'</td></tr>';
            $table .= '</table>';
            $message .= '<br>'.$table;
            $link_sms = base_url();

            if ($user != '') {
                $randomstring = $this->generateRandomString(8);
                // $md5 = md5($randomstring);
                $url = base_url('rmd').'/'.$randomstring;
                $link_sms = $url;
                $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

                $message .= '<br>'.$link;

                $usr_id = $this->view_by_id('users', array('usr_name' => $user), 'row');
                $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                $now = date('Y-m-d');
                $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                $authenticantion = array(
                    'usr_id' => $id_user,
                    'token' => $randomstring,
                    'expiration' => $expiration
                    );

                $this->process_data('tb_authentication', $authenticantion);
            }
            // sms
            $message_sms = '(HRD) Ada Pengajuan Karyawan butuh diverifikasi, '.$link_sms;

            if ($email != '') {
                    $this->Main_Model->send_mail($email, $message, $subject);
            }
            if ($hp != '') {
                // $this->Main_Model->send_sms($message_sms, $hp);
            }
        }
    }

    function reminder_meninggalkan_kerja($data = '')
    {
        $nip = isset($data['nip']) ? $data['nip'] : '';
        $tgl = isset($data['tgl']) ? $this->format_tgl($data['tgl']) : '';
        $jmlmenit = isset($data['jmljam']) ? $data['jmljam'] : '';
        $keterangan = isset($data['keterangan']) ? $data['keterangan'] : '';
        $q = $this->kary_nip($nip);
        $nama = isset($q->nama) ? $q->nama : '';
        $link_sms = base_url();
        $w = $this->posisi($nip);
        $id_cabang = isset($w->id_cab) ? $w->id_cab : '0';
        $sal_pos = isset($w->sal_pos) ? $w->sal_pos : '';
        $id_divisi = isset($w->id_divisi) ? $w->id_divisi : '';

        $usr = $this->view_by_id('tb_user_cabang', array('id_cabang' => $id_cabang), 'result');
        foreach ($usr as $row) {
            // contact
            $contact = $this->reminder_contact($row->usr_name, $sal_pos, $id_divisi);
            $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
            $hp = isset($contact['hp']) ? $contact['hp'] : '08156647203';
            $user = isset($contact['user']) ? $contact['user'] : '';

            // email
            $subject = '[Aplikasi HRD] Reminder Ijin Meninggalkan Kerja';
            $no = 1;
            $message = 'Berikut adalah Daftar Ijin Meninggalkan Kerja yang butuh diverifikasi';
            $table = '<table border = "1">
                            <tr><th>No</th><th>Nama</th><th>Tanggal</th><th>Jumlah Menit</th><th>Status</th><th>Alasan</th></tr>';
            $table .= '<tr><td>'.$no++.'</td><td>'.$nama.'</td><td>'.$tgl.'</td><td>'.$jmlmenit.'</td><td>Proses</td><td>'.$keterangan.'</td></tr>';
            $table .= '</table>';
            $message .= '<br>'.$table;

            if ($user != '') {
                $randomstring = $this->generateRandomString(8);
                // $md5 = md5($randomstring);
                $url = base_url('rmd').'/'.$randomstring;
                $link_sms = $url;
                $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

                $message .= '<br>'.$link;

                $usr_id = $this->view_by_id('users', array('usr_name' => $user), 'row');
                $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                $now = date('Y-m-d');
                $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                $authenticantion = array(
                    'usr_id' => $id_user,
                    'token' => $randomstring,
                    'expiration' => $expiration
                    );

                $this->process_data('tb_authentication', $authenticantion);
            }
            // sms
            $message_sms = '(HRD) Ada Ijin Meninggalkan Kerja butuh diverifikasi, '.$link_sms;

            if ($email != '') {
                $this->Main_Model->send_mail($email, $message, $subject);
            }
            if ($hp != '') {
                // $this->Main_Model->send_sms($message_sms, $hp);
            }
        }
    }

    function approval_perdin($nobukti = '')
    {
        $data = $this->db->query("SELECT a.*, DATE_FORMAT(a.`tgl_brgkt`, '%d %M %Y %H:%i:%s') berangkat, DATE_FORMAT(a.`tgl_pulang`, '%d %M %Y %H:%i:%s') pulang FROM perdin a WHERE a.`nobukti` = '$nobukti'")->row();
        $today = date('d F Y', strtotime(date('Y-m-d').'-1 days'));
        $subject = '[Aplikasi HRD] Verifikasi Perjalanan Dinas '.$today;
        if ($data) :
            $header = 'Berikut adalah Daftar Perdin yang butuh diverifikasi';
            $table = '<table border = "1">
                        <tr><th>Nobukti</th><th>Berangkat</th><th>Pulang</th><th>Tujuan</th><th>Keperluan</th><th>Nama</th></tr>';
            $no = 1;
            $detail = $this->db->query("SELECT b.`nama` FROM perdin_det a JOIN kary b ON a.`nip` = b.`nip` WHERE a.`nobukti` = '$data->nobukti'")->result();
            $nama = '';
            $nama_sms = '';
            $i = 1;
            foreach ($detail as $row) {
                $nama .= $row->nama.'<br>';
                ($i == count($detail)) ? $nama_sms .= $row->nama : $nama_sms .= $row->nama.', ';
                $i++;
            }
            $table .= '<tr><td>'.$data->nobukti.'</td><td>'.$data->berangkat.'</td><td>'.$data->pulang.'</td><td>'.$data->tujuan.'</td><td>'.$data->keperluan.'</td><td>'.$nama.'</td></tr></table>';

            $message = $header.'<br>'.$table;

            $link_sms = base_url();

            $usr = $this->view_by_id('tb_user_cabang', array('id_cabang' => $data->id_cabang), 'result');
            foreach ($usr as $row) {
                $contact = $this->reminder_contact($row->usr_name);
                $email = isset($contact['email']) ? $contact['email'] : 'victor.fendi@gmedia.co.id';
                $hp = isset($contact['hp']) ? $contact['hp'] : '08156647203';
                $user = isset($contact['user']) ? $contact['user'] : '';

                if ($user != '') {
                    $randomstring = $this->generateRandomString(8);
                    // $md5 = md5($randomstring);
                    $url = base_url('rmd').'/'.$randomstring;
                    $link_sms = $url;
                    $link = 'Silahkan <a href="'.$url.'">klik disini</a>';

                    $pesan = $message.'<br>'.$link;

                    $usr_id = $this->view_by_id('users', array('usr_name' => $user), 'row');
                    $id_user = isset($usr_id->usr_id) ? $usr_id->usr_id : '';
                    $now = date('Y-m-d');
                    $expiration = date('Y-m-d', strtotime("+1 day", strtotime($now)));

                    $authenticantion = array(
                        'usr_id' => $id_user,
                        'token' => $randomstring,
                        'expiration' => $expiration
                        );

                    $this->process_data('tb_authentication', $authenticantion);
                }

                $message_sms = '(HRD) Ada Perdin yg butuh diverifikasi '.$link_sms;

                if ($email != '') {
                    // $this->Main_Model->send_mail($email, $pesan, $subject);
                }
                if ($hp != '') {
                    // $this->Main_Model->send_sms($message_sms, $hp);
                }
            }
        endif;
    }

    function reminder_contact($username = '', $sal_pos = '', $divisi = '')
    {
        $condition = " WHERE a.user = '$username' ";
        if ($sal_pos != '') {
            $condition .= " AND b.sal_pos = '$sal_pos' AND e.sal_pos = '$sal_pos' ";
        }
        if ($divisi != '') {
            $condition .= " AND d.id_divisi = '$divisi' ";
        }
        // if ($sal_pos == '' && $divisi == '') {
        //     $contact = $this->db->query("
        //         SELECT b.`mail`, b.`telp`, a.user FROM ms_reminder a
        //         JOIN kary b ON a.`nip` = b.`nip` WHERE a.`user` = '$username'")->row();
        // } else {
            $contact = $this->db->query("
                SELECT c.`mail`, c.`telp`, a.`user`, c.nama 
                FROM ms_reminder a
                INNER JOIN tb_level_approval b ON a.`user` = b.`usr_name`
                INNER JOIN kary c ON a.`nip` = c.`nip`
                INNER JOIN tb_user_divisi d ON d.`usr_name` = a.`user`
                INNER JOIN tb_level_pengajuan e ON e.`usr_name` = a.`user`
                INNER JOIN users f ON f.usr_name = a.user
                $condition
                AND f.approval = 1
                GROUP BY a.`user`")->row();
        // }

        if (!empty($contact)) {
            if ($contact->mail != '') {
                $email = $contact->mail;
            } else {
                $email = '';
            }

            if ($contact->telp != '') {
                $hp = $contact->telp;
            } else {
                $hp = '';
            }

            if ($contact->user != '') {
                $user = $contact->user;
            } else {
                $user = '';
            }

            $nama = isset($contact->nama) ? $contact->nama : '';
        } else {
            $email = '';
            $hp = '';
            $user = '';
            $nama = '';
        }

        return array(
            'email' => $email,
            'hp' => $hp,
            'user' => $user,
            'nama' => $nama
        );
    }

    function send_sms($message = '', $receiver = '')
    {
        $db = $this->connect_to_mkios();
        $db->query("
            INSERT INTO kirim_sms (status, kode_terminal, tgl, pesan, penerima, tipe, flag, nama_sales)
            VALUES ('PENDING', '253', NOW(), '$message', '$receiver', 'SMS', '1', 'HRD')");
    }

    function generateRandomString($length = 8)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function session_cabang()
    {
        $usr_name = $this->session->userdata('username');
        $cabang = $this->db->where('usr_name', $usr_name)->get('tb_user_cabang')->result();
        $sess_cabang = array();
        if ($cabang) {
            foreach ($cabang as $row) {
                $sess_cabang[] = $row->id_cabang;
            }
        }

        return $sess_cabang;
    }

    function class_approval()
    {
        $usr_name = $this->session->userdata('username');
        $class = $this->db->where('usr_name', $usr_name)->get('tb_level_approval')->result();
        $sess_class = array();
        if ($class) {
            foreach ($class as $row) {
                $sess_class[] = $row->sal_pos;
            }
        }

        return $sess_class;
    }

    function session_divisi()
    {
        $usr_name = $this->session->userdata('username');
        $divisi = $this->db->where('usr_name', $usr_name)->get('tb_user_divisi')->result();
        $sess_divisi = array();
        if ($divisi) {
            foreach ($divisi as $row) {
                $sess_divisi[] = $row->id_divisi;
            }
        }

        return $sess_divisi;
    }

    function session_pengajuan()
    {
        $usr_name = $this->session->userdata('username');
        $class = $this->db->where('usr_name', $usr_name)->get('tb_level_pengajuan')->result();
        $sess_class = array();
        if ($class) {
            foreach ($class as $row) {
                $sess_class[] = $row->sal_pos;
            }
        }

        return $sess_class;
    }

    function kode_approval($nip = '', $modul = '')
    {
        return $this->db->query("
            SELECT MIN(e.`id`), e.*, a.`nama`, d.`sal_pos` 
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON c.`id_pos` = d.`id_pos`
            JOIN ms_level_approval e ON e.`sal_pos` = d.`sal_pos`
            WHERE ISNULL(a.`tgl_resign`) 
            AND b.`aktif` = '1' 
            AND a.`nip` = '$nip' 
            AND e.`modul` = '$modul'")->row();
    }

    function kode_level($sal_pos = '', $modul = '')
    {
        return $this->db->query("
                SELECT *, MIN(a.`id`) 
                FROM ms_level_approval a 
                WHERE a.`sal_pos` = '$sal_pos' 
                AND a.`modul` = '$modul'")->row();
    }

    function rule_approval($kode = '', $pola = '', $approval = '')
    {
        $q = $this->db->query("
            SELECT * 
            FROM ms_rule_approval 
            WHERE kode = '$kode'
            AND pola = '$pola'")->row();
        
        return isset($q->$approval) ? $q->$approval : '12';
    }

    function kota_option()
    {
        $q = $this->view_by_id('ms_kota', null, 'result');
        $result = array();
        foreach ($q as $row) {
            $result[$row->id] = $row->kota;
        }

        return $result;
    }

    function json_karyawan($id_cabang = '')
    {
        return $this->db->query("
            SELECT a.`nip`, a.`nama`, e.`cabang` 
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN ms_cabang e ON d.`id_cabang` = e.`id_cab`
            WHERE ISNULL(a.`tgl_resign`) AND b.`aktif` = '1'
            AND e.`id_cab` = '$id_cabang' 
            ORDER BY a.`nama`")->result();
    }

    function kop_surat()
    {
        $query = $this->db->query("SELECT * FROM ms_kop")->row();
        $image = isset($query->image) ? $query->image : '';

        return $image;
    }

    function tahun_periode()
    {
        return $this->db->query("
            SELECT DISTINCT(YEAR(a.`tgl_akhir`)) AS tahun
            FROM q_det a 
            WHERE a.`aktif` = 1 
            ORDER BY YEAR(a.`tgl_akhir`) DESC")->result();
    }

    function arr_tahun_periode()
    {
        $data = $this->tahun_periode();
        $arr = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $arr[$row->tahun] = $row->tahun;
            }
        }
        return $arr;
    }

    function bulan_periode()
    {
        return $this->db->query("
            SELECT DISTINCT(DATE_FORMAT(a.`tgl_akhir`, '%M')) AS bulan,
            MONTH(a.`tgl_akhir`) AS idbln
            FROM q_det a 
            WHERE a.`aktif` = 1
            GROUP BY DATE_FORMAT(a.`tgl_akhir`, '%M') 
            ORDER BY a.`bln` ASC ")->result();
    }

    function arr_bulan_periode()
    {
        $data = $this->bulan_periode();
        $arr = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $arr[$row->idbln] = $row->bulan;
            }
        }

        return $arr;
    }

    function periode($bln = '', $thn = '')
    {
        return $this->db->query("
            SELECT *, DATE_FORMAT(tgl_akhir, '%M %Y') AS periode 
            FROM q_det a
            WHERE a.`bln` = '$bln' 
            AND YEAR(a.`tgl_akhir`) = '$thn'")->row();
    }

    function sk_terakhir($nip = '')
    {
        return $this->db->query("
            SELECT MAX(a.`id_sk`) AS id_sk, a.`id_pos_sto`, p.*
            FROM sk a
            LEFT JOIN (
                SELECT pos.*, ms_cabang.`cabang`, ms_divisi.`divisi`, pos_sto.`id_sto`, ms_departemen.departemen 
                FROM pos_sto
                INNER JOIN pos ON pos.`id_pos` = pos_sto.`id_pos`
                INNER JOIN ms_cabang ON ms_cabang.`id_cab` = pos.`id_cabang`
                INNER JOIN ms_divisi ON ms_divisi.`id_divisi` = pos.`id_divisi`
                INNER JOIN ms_departemen ON ms_departemen.id_dept = pos.id_departemen
            ) AS p ON p.id_sto = a.`id_pos_sto`
            WHERE a.`nip` = '$nip'")->row();
    }

    function company()
    {
        $idp = $this->session->userdata('idp');
        if ($idp == '') {
            $idp = 1;
        }
        $q = $this->db->where('idp', $idp)->order_by('idp', 'asc')->limit(1)->get('idp')->row();

        $company = isset($q->bu) ? $q->bu : '';
        return $company;
    }

    function view_company()
    {
        $q = $this->db->query("SELECT * FROM idp")->result();
        $result = '';
        if (!empty($q)) {
            foreach ($q as $row) {
                $result .= '<li>';
                $result .= '<a href="javascript:;" onclick="ChangePT('.$row->idp.')">';
                $result .= '<i class="icon-home"></i> '.$row->bu.'</a> </li>';
            }
        }
        return $result;
    }

    function period_this_year()
    {
        $q = $this->db->query("
            SELECT a.`qd_id`, a.`tgl_awal`, a.`tgl_akhir`, 
            DATE_FORMAT(a.`tgl_akhir`, '%M') AS bulan
            FROM q_det a
            WHERE a.`tgl_awal` <= CURDATE()
            AND YEAR(a.`tgl_akhir`) = YEAR(CURDATE())")->result();

        return $q;
    }

    function generate_lembur($nip = '', $tgl = '', $id_shift = '')
    {
        $holiday = $this->db->where('hol_tgl', $tgl)
                            ->get('hol')
                            ->row();
        $shift = $this->db->where('id_shift', $id_shift)
                        ->get('ms_shift')
                        ->row();

        $condition = array(
                        'nip' => $nip,
                        'tgl' => $tgl
                    );

        $this->Main_Model->delete_data('lembur', $condition);

        $week = date('w', strtotime($tgl));
        if ($week == '0' || $week == '6' || ! empty($holiday)) {
            if ($shift != 'L' && $shift != 'R') {
                // is libur nasional or libur hari besar
                $is_libur = isset($holiday->libur) ? $holiday->libur : '';

                if ($is_libur != '') {
                    if ($is_libur == 1) {
                        $hari = ' Hari Libur Besar';
                        $besar = 1;
                    } else {
                        $hari = ' Hari Libur Nasional';
                        $besar = 0;
                    }
                } else {
                    $hari = ($week == 0) ? ' Hari Minggu' : ' Hari Sabtu';
                    $besar = 0;
                }

                $id_overtime = isset($shift->id_overtime) ? $shift->id_overtime : '';
                $lembur = isset($shift->lembur) ? $shift->lembur : 0;
                if ($lembur == 1) {
                    $total = 0;
                    if ($besar == 1) {
                        $total = isset($shift->nominal_raya) ? $shift->nominal_raya : 0;
                    } else {
                        $total = isset($shift->nominal_nasional) ? $shift->nominal_nasional : 0;
                    }

                    $jam_masuk = isset($shift->jam_masuk) ? $shift->jam_masuk : '';
                    $jam_pulang = isset($shift->jam_pulang) ? $shift->jam_pulang : '';
                    $keterangan = isset($shift->keterangan) ? $shift->keterangan : '';

                    $keterangan_lembur = $keterangan.$hari;
                    if ($jam_masuk == '00:00:00' && $jam_pulang == '00:00:00') {
                        $keterangan_lembur = isset($shift->keterangan) ? $shift->keterangan : '';
                    }

                    $mulai = $tgl.' '.$jam_masuk;
                    if (strtotime($jam_masuk) > strtotime($jam_pulang)) {
                        $date = date('Y-m-d', strtotime("$tgl + 1 days"));
                        $date = $date.' '.$jam_pulang;

                        $selesai = $date;
                    } else {
                        $date = $tgl.' '.$jam_pulang;

                        $selesai = $date;
                    }

                    $jml_jam = $this->db->query("SELECT TIMESTAMPDIFF(HOUR, '$mulai', '$selesai') AS jumlah")->row();
                    $jumlah_jam = isset($jml_jam->jumlah) ? $jml_jam->jumlah : 0;

                    $data_lembur = array(
                        'id_overtime' => $id_overtime,
                        'nip' => $nip,
                        'tgl' => $tgl,
                        'jml_jam' => $jumlah_jam,
                        'lembur' => $total,
                        'starttime' => $jam_masuk,
                        'endtime' => $jam_pulang,
                        'keterangan' => $keterangan_lembur,
                        'status' => 1,
                        'insert_at' => now(),
                        'user_insert' => username()
                    );

                    $this->db->insert('lembur', $data_lembur);
                }
            }
        }
    }

    function curl_notif($url = '', $parameter = '', $header = '')
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($parameter),
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_FRESH_CONNECT => true
            ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        curl_close($curl);
        if ($err) {
            $response = ($err);
        } else {
            $response;
        }

        return $response;
    }

    function master_penyetuju()
    {
        return $this->db->query("
                SELECT a.*, b.`nama`
                FROM ms_rule_approval a
                INNER JOIN kary b ON a.`penyetuju` = b.`nip`
                ORDER BY a.penyetuju ASC")->result();
    }

    function kode_penyetuju($pola = '')
    {
        return $this->db->query("
                SELECT a.*, b.`nama`
                FROM ms_rule_approval a
                INNER JOIN kary b ON a.`penyetuju` = b.`nip`
                WHERE a.`pola` = '$pola'
                ORDER BY a.`urutan` ASC
                LIMIT 1")->row();
    }

    function pecah_cc($cc = '')
    {
        $email = [];
        if ($cc != '') {
            $explode = explode(', ', $cc);
            if (!empty($explode)) {
                for ($i = 0; $i < count($explode); $i++) {
                    $data = $this->db->where('nip', $explode[$i])
                                ->get('kary')
                                ->row();
                    $mail = isset($data->mail) ? $data->mail : '';
                    $email[] = $mail;
                }
            }
        }

        return $email;
    }

    function master_app_penyetuju()
    {
        $nip = nip_api();

        return $this->db->query("
                SELECT b.pola, c.nama
                FROM ms_rule_detail a
                INNER JOIN ms_rule_approval b ON a.`pola` = b.`pola`
                INNER JOIN kary c ON c.nip = b.penyetuju
                WHERE a.`nip` = '$nip'
                GROUP BY a.`nip` 
                ORDER BY b.`penyetuju` ASC")->result();
    }

    function cari_fcm_id($nip = '')
    {
        return $this->db->query("
                SELECT b.`usr_id`, c.`fcm_id`
                FROM ms_reminder a
                INNER JOIN users b ON a.`user` = b.`usr_name`
                INNER JOIN kary c ON c.`nip` = a.`nip`
                WHERE a.`nip` = '$nip'")->row();
    }
}
