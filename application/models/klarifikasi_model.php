<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Klarifikasi_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		
	}

	function insert_presensi_from_log($id='') 
	{
		$user = $this->session->userdata('username');

		$this->db->query("
			INSERT INTO presensi (nip, tgl, sakit, ijin, mangkir, alpha, ket, pin, klarifikasi, file, update_at, user) 
			(	SELECT nip, tgl, sakit, ijin, mangkir, alpha, ket, pin, klarifikasi, file, NOW(), '$user' 
				FROM log_presensi 
				WHERE id_presensi = '$id'
				ORDER BY id DESC 
				LIMIT 1
			)");
	}

	function delete_cuti($nip='', $tgl='')
	{
		$query = $this->db->query("
			SELECT *
			FROM cuti_sub_det a
			INNER JOIN cuti_det b ON a.`id_cuti_det` = b.`id_cuti_det`
			WHERE a.`tgl` = '$tgl'
			AND b.`nip` = '$nip' ")->row();

		$id_cuti_det = isset($query->id_cuti_det) ? $query->id_cuti_det : '';

		// hapus cuti
		$this->db->where('id_cuti_det', $id_cuti_det)
			->delete('cuti_det');

		// hapus cuti detail
		$this->db->where('id_cuti_det', $id_cuti_det)
			->delete('cuti_sub_det');
	}

	function delete_cuti_khusus($nip='', $tgl='')
	{
		$query = $this->db->query("
			SELECT *
			FROM cuti_khusus a
			WHERE a.`nip` = '$nip'
			AND '$tgl' BETWEEN a.`tgl_awal` AND a.`tgl_akhir`")->row();

		$id = isset($query->id) ? $query->id : '';

		// hapus cuti khusus
		$this->db->where('id', $id)
			->delete('cuti_khusus');
	}
}

/* End of file klarifikasi_model.php */
/* Location: ./application/models/klarifikasi_model.php */ ?>