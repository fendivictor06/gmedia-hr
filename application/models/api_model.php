<?php if (! defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Api_Model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function check_scan($scan_date = '', $pin = '')
    {
        return $this->db->where('scan_date', $scan_date)->where('pin', $pin)->get('tb_scanlog')->row();
    }

    function add_scan($data)
    {
        $this->db->insert('tb_scanlog', $data);
    }

    function check_user($pin = '')
    {
        return $this->db->where('pin', $pin)->get('tb_user_template')->row();
    }

    function add_user($data)
    {
        $this->db->insert('tb_user_template', $data);
    }

    function view_scanlog($date_start = '', $nip = '')
    {
        return $this->db->query("
			SELECT a.*,DATE_FORMAT(a.`scan_date`,'%Y-%m-%d') tgl,DATE_FORMAT(a.`scan_date`,'%H:%i:%s') jam 
			FROM tb_scanlog a 
			WHERE DATE_FORMAT(a.`scan_date`,'%Y-%m-%d') = '$date_start' 
			AND a.`nip` = '$nip'")->row();
    }

    function terlambat($date = '')
    {
        if ($date == '') {
            $date = date('Y-m-d');
        }

        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if (!empty($id_cabang)) {
            $cabang .= 'AND (';
            for ($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                if (end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if (!empty($divisi)) {
            $div .= ' AND (';
            for ($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if (end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $result = $this->db->query("
			SELECT a.`nip`, a.`pin`, a.`nama`, e.`cabang`, 
			IFNULL(CAST(scan.scan_date AS TIME),'') AS scan, 
			IFNULL(jadwal.jam_masuk,
				( SELECT jam_masuk 
				  FROM ms_shift 
				  WHERE ms_shift.`default_shift` = '1')
				) AS jam_masuk 
			FROM kary a 
			JOIN sk b ON a.`nip` = b.`nip`
			JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
			JOIN pos d ON d.`id_pos` = c.`id_pos`
			LEFT JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
			LEFT JOIN (
				SELECT tb_scanlog.`scan_date`, nip 
				FROM tb_scanlog 
				WHERE CAST(tb_scanlog.`scan_date` AS DATE) = '$date' 
				GROUP BY tb_scanlog.`nip`
				ORDER BY tb_scanlog.`scan_date` ASC 
			) AS scan ON scan.nip = a.`nip`
			LEFT JOIN (
				SELECT ms_shift.`jam_masuk`, tr_shift.`nip` 
				FROM tr_shift 
				JOIN ms_shift ON ms_shift.`id_shift` = tr_shift.`id_shift`
				WHERE tr_shift.`tgl` = '$date'
			) AS jadwal ON jadwal.nip = a.`nip`
			WHERE ISNULL(a.`tgl_resign`) 
			AND b.`aktif` = '1'
			AND CAST(scan.scan_date AS TIME) > IFNULL(jadwal.jam_masuk, 
				(	SELECT jam_masuk 
					FROM ms_shift 
					WHERE ms_shift.`default_shift` = '1'
				))
			$cabang
			$div 
			ORDER BY a.`nama` ASC ")->result();

        return $result;
    }

    function cari_shift($tgl = '', $nip = '')
    {
        return $this->db->query("
			SELECT b.`nama`, b.`jam_masuk`, b.`jam_pulang`, b.keterangan,
			b.lembur 
			FROM tr_shift a 
			JOIN ms_shift b ON a.`id_shift` = b.`id_shift` 
			WHERE a.`nip` = '$nip' 
			AND a.`tgl` = '$tgl'")->row();
    }

    function default_shift()
    {
        return $this->db->query("
			SELECT a.`nama`, a.`jam_masuk`, a.`jam_pulang`, a.keterangan,
			a.lembur 
			FROM ms_shift a 
			WHERE a.`default_shift` = 1 
			AND a.`status` = 1")->row();
    }

    function generate_terlambat($date = '', $w_nip = '')
    {
        // $q = $this->db->query("
        //  SELECT *, CAST(a.scan_masuk AS TIME) AS absen
        //  FROM tb_absensi a
        //  INNER JOIN (
        //      SELECT nip, tgl
        //      FROM tr_shift
        //      WHERE tgl = '$date'
        //  ) AS jadwal ON jadwal.nip = a.`nip` AND jadwal.tgl = a.`tgl`
        //  WHERE a.`tgl` = '$date'
        //  AND CAST(a.`scan_masuk` AS TIME) > a.`jam_masuk`
        //  AND a.jam_masuk != '00:00:00'
        //  $w_nip ")->result();

        $q = $this->db->query("
			SELECT a.`nip`, a.`tgl`, TIME(a.`scan_masuk`) AS absen,
			a.`jam_masuk`, a.`pin`, a.`shift`
			FROM tb_absensi a
			WHERE a.`tgl` = '$date'
			AND CAST(a.`scan_masuk` AS TIME) > a.`jam_masuk`
			AND a.jam_masuk != '00:00:00'
			$w_nip ")->result();

        if (! empty($q)) {
            foreach ($q as $row) {
                $nip = $row->nip;
                $tgl = $row->tgl;
                $scan_masuk = $row->absen;
                $masuk = $row->jam_masuk;
                $pin = $row->pin;
                $nama_shift = $row->shift;

                // jika flag absensi 1 maka jam masuk tidak bisa diedit
                // if($masuk == '') {
                //  $shift = $this->cari_shift($date, $nip);
                //  if($shift) {
                //      $masuk = $shift->jam_masuk;
                //  } else {
                //      $shift = $this->default_shift();
                //      $masuk = $shift->jam_masuk;
                //  }
                // }

                $check_terlambat = $this->db->where('nip', $nip)->where('tgl', $tgl)->get('terlambat_temp')->row();
                if ($check_terlambat) {
                    $this->db->where('nip', $nip)->where('tgl', $tgl)->delete('terlambat_temp');
                }

                $um = $this->Api_Model->uang_makan($nip);
                $uang_makan = isset($um->nominal) ? $um->nominal : 0;
                $uang_denda = $uang_makan;

                $klarifikasi = $this->db->where('nip', $nip)
                                ->where('tgl', $tgl)
                                ->get('tb_klarifikasi_terlambat')
                                ->row();

                $ishead = $this->db->where('penyetuju', $nip)
                            ->get('ms_rule_approval')
                            ->row();

                if (! empty($klarifikasi)) {
                    $kla_um = isset($klarifikasi->uang_makan) ? $klarifikasi->uang_makan : 0;
                    if ($kla_um == 1) {
                        $uang_denda = 0;
                    } else {
                        $uang_makan = 0;
                    }
                } elseif (! empty($ishead)) {
                    $uang_denda = 0;
                } else {
                    $uang_makan = 0;
                }

                $menit  = round((strtotime($scan_masuk) - strtotime($masuk))/60);
                ($menit == 0) ? $menit = 1 : $menit = $menit;
                $ins_telat  = array(
                    "nip" => $nip,
                    "tgl" => $tgl,
                    "total_menit" => $menit,
                    "pin" => $pin,
                    "scan_masuk" => $scan_masuk,
                    "shift" => $nama_shift,
                    "jam_masuk" => $masuk
                );
                $this->db->insert('terlambat_temp', $ins_telat);
                $this->db->where('nip', $nip)->where('tgl', $tgl)->update('tb_absensi', array('uang_makan' => $uang_makan, 'uang_denda' => $uang_denda));
            }
        }
    }

    function view_log_mesin($date = '')
    {
        return $this->db->query("
			SELECT a.*, b.`description` FROM log_mesin a
			JOIN ms_mesin b ON a.`sn` = b.`sn`
			WHERE CAST(a.`datetime` AS DATE) = '$date'
			ORDER BY a.datetime DESC
			")->result();
    }

    function uang_makan($nip = '')
    {
        return $this->db->query("
			SELECT uang_makan AS nominal
			FROM karyawan a
			INNER JOIN ms_cabang b ON a.id_cabang = b.id_cab 
			WHERE a.nip = '$nip'")->row();
    }
}

/* End of file api_model.php */
/* Location: ./application/models/api_model.php */
