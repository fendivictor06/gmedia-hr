<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

	// jumlah terlambat hari ini
	function jml_telat_hari_ini() 
	{
		$date = date('Y-m-d');

		$id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        } 

        $result = $this->db->query("
            SELECT a.`nip`, a.`pin`, a.`nama`, e.`cabang`, IFNULL(CAST(scan.scan_date AS TIME),'') AS scan, 
            IFNULL(jadwal.jam_masuk,
                (SELECT jam_masuk FROM ms_shift WHERE ms_shift.`default_shift` = '1')
                ) AS jam_masuk 
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            LEFT JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            LEFT JOIN (
                SELECT tb_scanlog.`scan_date`,pin FROM tb_scanlog 
                WHERE CAST(tb_scanlog.`scan_date` AS DATE) = '$date' 
                GROUP BY tb_scanlog.`pin`
                ORDER BY tb_scanlog.`scan_date` ASC 
            ) AS scan ON scan.pin = a.`pin`
            LEFT JOIN (
                SELECT ms_shift.`jam_masuk`, tr_shift.`nip` FROM tr_shift 
                JOIN ms_shift ON ms_shift.`id_shift` = tr_shift.`id_shift`
                WHERE tr_shift.`tgl` = '$date'
            ) AS jadwal ON jadwal.nip = a.`nip`
            WHERE ISNULL(a.`tgl_resign`) AND b.`aktif` = '1'
            AND CAST(scan.scan_date AS TIME) > IFNULL(jadwal.jam_masuk, (SELECT jam_masuk FROM ms_shift WHERE ms_shift.`default_shift` = '1'))
            $cabang
            $div 
            ORDER BY a.`nama` ASC
            ")->num_rows();

        return $result;
	}

	// jumlah mangkir di dashboard akun
	function jumlah_mangkir()
	{
		// $id_cabang = $this->Main_Model->session_cabang();
		// $cabang = '';
		// if(!empty($id_cabang)) {
		// 	$cabang .= ' AND (';
		// 	for($i = 0; $i < count($id_cabang); $i++) {
		// 		$cabang .= " e.`id_cabang` = '$id_cabang[$i]'";

		// 		if(end($id_cabang) != $id_cabang[$i]) {
		// 			$cabang .= ' OR';
		// 		} else {
		// 			$cabang .= ')';
		// 		}
		// 	}
		// }

		// $sal_pos = $this->Main_Model->class_approval();
		// $kelas = '';
		// if(!empty($sal_pos)) {
		// 	$kelas .= ' AND (';
		// 	for($i = 0; $i < count($sal_pos); $i++) {
		// 		$kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

		// 		if(end($sal_pos) != $sal_pos[$i]) {
		// 			$kelas .= ' OR';
		// 		} else {
		// 			$kelas .= ')';
		// 		}
		// 	}
		// }

		// $divisi = $this->Main_Model->session_divisi();
		// $div = '';
		// if(!empty($divisi)) {
		// 	$div .= ' AND (';
		// 	for($i = 0; $i < count($divisi); $i++) {
		// 		$div .= " e.`id_divisi` = '$divisi[$i]'";

		// 		if(end($divisi) != $divisi[$i]) {
		// 			$div .= ' OR';
		// 		} else {
		// 			$div .= ')';
		// 		}
 	// 		}
		// } 

    	// $date = date('Y-m-d', strtotime("-2 day", strtotime(date('Y-m-d'))));
    	// return $this->db->query("
    	// 	select a.*,b.`nama`,f.`cabang` from presensi a 
    	// 	join kary b on a.`pin` = b.`pin` 
    	// 	join sk c on b.`nip` = c.`nip` 
    	// 	join pos_sto d on c.`id_pos_sto` = d.`id_sto` 
    	// 	join pos e on d.`id_pos` = e.`id_pos` 
    	// 	join ms_cabang f on e.`id_cabang`=f.`id_cab` 
    	// 	where 
    	// 	c.`aktif` = '1' 
    	// 	and a.`tgl` >= '$date' 
    	// 	AND a.`mangkir` = '1'
     //        AND a.flag = 0
     //        $cabang
    	// 	$kelas
    	// 	$div 
    	// 	")->num_rows();

        $date = date('Y-m-d', strtotime("-1 day", strtotime(date('Y-m-d'))));
        $usr_name = $this->session->userdata('username');
        $user = $this->db->where('user', $usr_name)
                    ->get('ms_reminder')
                    ->row();
        $nip = isset($user->nip) ? $user->nip : '';

        return $this->db->query("
            SELECT a.*,b.`nama`,f.`cabang` 
            FROM presensi a 
            JOIN kary b ON a.`pin` = b.`pin` 
            JOIN sk c ON b.`nip` = c.`nip` 
            JOIN pos_sto d ON c.`id_pos_sto` = d.`id_sto` 
            JOIN pos e ON d.`id_pos` = e.`id_pos` 
            JOIN ms_cabang f ON e.`id_cabang`=f.`id_cab` 
            JOIN ms_rule_detail g ON g.`nip` = a.`nip`
            JOIN ms_rule_approval h ON h.`pola` = g.`pola`
            WHERE c.`aktif` = '1' 
            AND a.`tgl` = '$date' 
            AND a.`mangkir` = '1'
            AND a.flag = 0
            AND h.penyetuju = '$nip'
            GROUP BY a.`id`")->num_rows();
	}

	// jumlah terlambat
	function jumlah_terlambat()
	{
		// $id_cabang = $this->Main_Model->session_cabang();
  //       $cabang = '';
  //       if(!empty($id_cabang)) {
  //           $cabang .= 'AND (';
  //           for($i = 0; $i < count($id_cabang); $i++) {
  //               $cabang .= ' e.`id_cabang` = '.$id_cabang[$i];

  //               if(end($id_cabang) != $id_cabang[$i]) {
  //                   $cabang .= ' OR';
  //               } else {
  //                   $cabang .= ')';
  //               }
  //           }
  //       }

  //       $sal_pos = $this->Main_Model->class_approval();
  //       $kelas = '';
  //       if(!empty($sal_pos)) {
  //           $kelas .= ' AND (';
  //           for($i = 0; $i < count($sal_pos); $i++) {
  //               $kelas .= " e.`sal_pos` = '$sal_pos[$i]'";

  //               if(end($sal_pos) != $sal_pos[$i]) {
  //                   $kelas .= ' OR';
  //               } else {
  //                   $kelas .= ')';
  //               }
  //           }
  //       }

  //       $divisi = $this->Main_Model->session_divisi();
  //       $div = '';
  //       if(!empty($divisi)) {
  //           $div .= ' AND (';
  //           for($i = 0; $i < count($divisi); $i++) {
  //               $div .= " e.`id_divisi` = '$divisi[$i]'";

  //               if(end($divisi) != $divisi[$i]) {
  //                   $div .= ' OR';
  //               } else {
  //                   $div .= ')';
  //               }
  //           }
  //       } 

  //       $date = date('Y-m-d', strtotime("-2 day", strtotime(date('Y-m-d'))));
  //       return $this->db->query("
  //           SELECT a.*,b.`nama`,f.`cabang` FROM terlambat_temp a 
  //           JOIN kary b ON a.`nip` = b.`nip`
  //           JOIN sk c ON b.`nip` = c.`nip`
  //           JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
  //           JOIN pos e ON e.`id_pos` = d.`id_pos`
  //           JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
  //           WHERE a.`tgl` >= '$date' $cabang AND c.`aktif` = '1' $kelas $div
  //           ")->num_rows();

        $date = date('Y-m-d', strtotime("-1 day", strtotime(date('Y-m-d'))));
        $usr_name = $this->session->userdata('username');
        $user = $this->db->where('user', $usr_name)
                    ->get('ms_reminder')
                    ->row();
        $nip = isset($user->nip) ? $user->nip : '';

        return $this->db->query("           
            SELECT a.*,b.`nama`,f.`cabang` 
            FROM terlambat_temp a 
            JOIN kary b ON a.`nip` = b.`nip`
            JOIN sk c ON b.`nip` = c.`nip`
            JOIN pos_sto d ON d.`id_sto` = c.`id_pos_sto`
            JOIN pos e ON e.`id_pos` = d.`id_pos`
            JOIN ms_cabang f ON e.`id_cabang` = f.`id_cab`
            JOIN ms_rule_detail g ON g.`nip` = a.`nip`
            JOIN ms_rule_approval h ON h.`pola` = g.`pola`
            WHERE a.`tgl` = '$date' 
            AND c.`aktif` = '1' 
            AND h.`penyetuju` = '$nip'
            AND NOT EXISTS (
                SELECT NULL 
                FROM tb_klarifikasi_terlambat
                WHERE id_terlambat = a.id
            )
            GROUP BY a.`id`")->num_rows();
	}

	// jumlah lembur
	function jumlah_lembur()
	{
		$id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' d.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " d.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        } 

        return $this->db->query("SELECT a.*,e.`nama`
            ,(SELECT MAX(tb_absensi.`scan_pulang`) FROM tb_absensi WHERE DATE_FORMAT(tb_absensi.`tgl`,'%Y-%m-%d') = a.`tgl` AND tb_absensi.`nip` = a.`nip`) scan_pulang
            FROM lembur a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON b.`id_pos_sto` = c.`id_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN kary e ON a.`nip` = e.`nip`
            WHERE a.`status` = '0' AND b.`aktif` = '1' 
            AND a.`tgl` + INTERVAL 1 DAY <= CURDATE()
            $cabang $kelas $div")->num_rows();
	}

	// jumlah_cuti
	function jumlah_cuti()
	{
        $username = $this->session->userdata('username');
        $query = $this->db->where('user', $username)
                    ->get('ms_reminder')
                    ->row();
        $nip_wtf = isset($query->nip) ? $query->nip  : '';

        return $this->db->query("
            SELECT f.`id_cuti_det`, a.`nip`, a.`nama`, e.`cabang`, f.`alasan_cuti`, f.`alamat_cuti`, f.`no_hp`, h.`tipe`
            ,GROUP_CONCAT(DATE_FORMAT(g.`tgl`, '%d %M %Y') SEPARATOR ' <br> ') tgl, i.`keterangan`, i.`flag`
            FROM kary a 
            JOIN sk b ON a.`nip` = b.`nip`
            JOIN pos_sto c ON c.`id_sto` = b.`id_pos_sto`
            JOIN pos d ON d.`id_pos` = c.`id_pos`
            JOIN ms_cabang e ON e.`id_cab` = d.`id_cabang`
            JOIN cuti_det f ON f.`nip` = a.`nip`
            JOIN cuti_sub_det g ON g.`id_cuti_det` = f.`id_cuti_det`
            JOIN ms_tipe_cuti h ON h.`id` = f.`id_tipe`
            JOIN ms_status_approval i ON i.`kode` = f.`approval`
            WHERE ISNULL(a.`tgl_resign`)
            AND b.`aktif` = '1'
            AND i.`flag` <> 0
            AND i.`kode` <> 12
            AND f.penyetuju = '$nip_wtf'
            GROUP BY f.`id_cuti_det`
            ORDER BY g.`tgl` DESC")->num_rows();
	}

    // jumlah pulang_awal
    function jumlah_pulang_awal()
    {
        $username = $this->session->userdata('username');
        $query = $this->db->where('user', $username)
                    ->get('ms_reminder')
                    ->row();
        $nip_wtf = isset($query->nip) ? $query->nip  : '';

        return $this->db->query("
            SELECT a.`id_pulang_awal`, a.`nip`, b.`nama`, a.`tgl`, a.`jam`,
            c.`keterangan`, a.`alasan`, c.flag
            FROM tb_pulang_awal a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN ms_status_approval c ON c.`kode` = a.`status`
            WHERE a.`penyetuju` = '$nip_wtf'
            AND c.`kode` <> 12
            AND c.`flag` <> 0
            ORDER BY a.tgl DESC")->num_rows();
    }

    // jumlah meninggalkan kerja
    function jumlah_tinggal_kerja()
    {
        $username = $this->session->userdata('username');
        $query = $this->db->where('user', $username)
                    ->get('ms_reminder')
                    ->row();
        $nip_wtf = isset($query->nip) ? $query->nip  : '';

        return $this->db->query("
            SELECT a.`id`, a.`nip`, b.`nama`, a.`tgl`, a.`jmljam`,
            a.`keterangan`, c.`keterangan` AS status_approval
            FROM ijinjam a
            INNER JOIN kary b ON a.`nip` = b.`nip`
            INNER JOIN ms_status_approval c ON c.`kode` = a.`status`
            WHERE a.`penyetuju` = '$nip_wtf'
            AND c.`kode` <> 12
            AND c.`flag` <> 0
            ORDER BY a.tgl DESC")->num_rows();
    }

    // jumlah cuti_khusus
    function jumlah_cuti_khusus()
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= 'AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' f.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        $sal_pos = $this->Main_Model->class_approval();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " f.`sal_pos` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }

        $divisi = $this->Main_Model->session_divisi();
        $div = '';
        if(!empty($divisi)) {
            $div .= ' AND (';
            for($i = 0; $i < count($divisi); $i++) {
                $div .= " f.`id_divisi` = '$divisi[$i]'";

                if(end($divisi) != $divisi[$i]) {
                    $div .= ' OR';
                } else {
                    $div .= ')';
                }
            }
        }

        $pengajuan = $this->Main_Model->session_pengajuan();
        $is_approval = $this->Main_Model->is_approval();
        $kelas_pengajuan = '';
        if($is_approval == TRUE) {
            if(!empty($pengajuan)) {
                $kelas_pengajuan .= ' AND (';
                for($i = 0; $i < count($pengajuan); $i++) {
                    $kelas_pengajuan .= " g.`privilege` = '$pengajuan[$i]'";

                    if(end($pengajuan) != $pengajuan[$i]) {
                        $kelas_pengajuan .= ' OR';
                    } else {
                        $kelas_pengajuan .= ')';
                    }
                }
            } 
        }

        return $this->db->query("
            SELECT a.`nip`, c.`nama`, b.`tipe`, a.*, g.`flag`, g.`keterangan`
            FROM cuti_khusus a
            INNER JOIN tipe_cuti_khusus b ON a.`id_tipe_cuti` = b.`id_tipe`
            INNER JOIN kary c ON c.`nip` = a.`nip`
            INNER JOIN sk d ON d.`nip` = c.`nip`
            INNER JOIN pos_sto e ON e.`id_sto` = d.`id_pos_sto`
            INNER JOIN pos f ON f.`id_pos` = e.`id_pos`
            INNER JOIN ms_status_approval g ON g.`kode` = a.`approval`
            WHERE d.`aktif` = 1
            AND g.`flag` <> 0
            AND g.`kode` <> 6
            $cabang $kelas $div
            $kelas_pengajuan
            ORDER BY a.tgl_awal")->num_rows();
    }


	// jumlah perdin
	function jumlah_perdin()
	{
		$id_cabang = $this->Main_Model->session_cabang();
		$cabang = '';
		if(!empty($id_cabang)) {
			$cabang .= ' AND (';
			for($i = 0; $i < count($id_cabang); $i++) {
				$cabang .= ' a.`id_cabang` = '.$id_cabang[$i];

				if(end($id_cabang) != $id_cabang[$i]) {
					$cabang .= ' OR';
				} else {
					$cabang .= ')';
				}
			}
		}
    	return $this->db->query("
    		SELECT a.*, date_format(a.`tgl_brgkt`, '%d %M %Y %H:%i') berangkat, 
    		date_format(a.`tgl_pulang`, '%d %M %Y %H:%i') pulang 
    		FROM perdin a 
    		WHERE a.`verifikasi` = 0 $cabang
    		ORDER BY a.`tgl_brgkt` DESC")->num_rows();
	}

	// jumlah pengajuan karyawan
	function jml_pengajuan_karyawan()
	{
		$id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= ' a.`id_cabang` = '.$id_cabang[$i];

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        // $sal_pos = $this->Main_Model->class_approval();
        // $kelas = '';
        // if(!empty($sal_pos)) {
        //     $kelas .= ' AND (';
        //     for($i = 0; $i < count($sal_pos); $i++) {
        //         $kelas .= " a.`sal_pos` = '$sal_pos[$i]'";

        //         if(end($sal_pos) != $sal_pos[$i]) {
        //             $kelas .= ' OR';
        //         } else {
        //             $kelas .= ')';
        //         }
        //     }
        // }

        $sal_pos = $this->Main_Model->session_pengajuan();
        $kelas = '';
        if(!empty($sal_pos)) {
            $kelas .= ' AND (';
            for($i = 0; $i < count($sal_pos); $i++) {
                $kelas .= " d.`privilege` = '$sal_pos[$i]'";

                if(end($sal_pos) != $sal_pos[$i]) {
                    $kelas .= ' OR';
                } else {
                    $kelas .= ')';
                }
            }
        }
        return $this->db->query("
            SELECT a.*,b.`cabang`,c.`nama`, d.`keterangan` 
            FROM pengajuan_kary a 
            JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
            LEFT JOIN kary c ON a.`nip`=c.`nip`
            JOIN ms_status_approval d ON d.`kode` = a.`status` 
            WHERE d.`flag` = 1 $cabang $kelas
            ")->num_rows();
	}

    function jml_karyawan_finalisasi($tgl_awal='', $tgl_akhir='')
    {
        $id_cabang = $this->Main_Model->session_cabang();
        $cabang = '';
        if(!empty($id_cabang)) {
            $cabang .= ' AND (';
            for($i = 0; $i < count($id_cabang); $i++) {
                $cabang .= " a.`id_cabang` = '$id_cabang[$i]'";

                if(end($id_cabang) != $id_cabang[$i]) {
                    $cabang .= ' OR';
                } else {
                    $cabang .= ')';
                }
            }
        }

        return $this->db->query("
                SELECT a.*,b.`cabang`,c.`nama`, d.`kode`, d.`keterangan`, d.`flag`,
                e.keterangan as ket_hr, f.keterangan as ket_progress, g.keterangan as ket_ttd 
                FROM pengajuan_kary a 
                JOIN ms_cabang b ON a.`id_cabang`=b.`id_cab` 
                LEFT JOIN kary c ON a.`nip`=c.`nip` 
                JOIN ms_status_approval d ON d.`kode` = a.`status`
                LEFT JOIN ms_status_rekrutmen e ON e.kode = a.status_hr
                LEFT JOIN ms_status_rekrutmen f on f.kode = a.progress
                LEFT JOIN ms_status_rekrutmen g on g.kode = a.status_ttd
                WHERE a.`tgl_pengajuan` BETWEEN '$tgl_awal' AND '$tgl_akhir'
                AND a.progress = 5
                $cabang")->num_rows();
    }
}

/* End of file dashboard_model.php */
/* Location: ./application/models/dashboard_model.php */ ?>