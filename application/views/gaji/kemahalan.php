<div class="page-content">
    <div class="breadcrumbs">
        <h1>Daftar Penerima Tunjangan Kemahalan</h1>
    </div>
	<div class="row">
        <div class="portlet light">
            <div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="btn-group">
                                <button type="button" class="btn btn-primary" id="add_new" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Data
                                </button>
                            </div>
						</div>
					</div>
				</div>
            </div>
                <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form Tunjangan Kemahalan</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form id="fkemahalan">
					<input type="hidden" name="id" id="id" class="kosong">
					<div class="form-group">
						<label>Nama Karyawan</label>
							<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2 kosong"'); ?>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" id="save" class="btn btn-primary">Simpan</button>
	</div>
</div>