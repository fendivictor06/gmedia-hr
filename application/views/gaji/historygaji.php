<div class="page-content">
    <div class="breadcrumbs">
        <h1>Lihat Payroll</h1>
    </div>
    <div class="row">
    	<div class="panel panel-default">
			<div class="panel-body">
				<?php echo form_periode(1); ?>
				<div class="form-group">
					<label>Cabang</label>
						<?php echo form_dropdown('cabang',$cabang,'','id="cabang" class="select2" style="width:100%"'); ?>
				</div>
				<div class="form-group">
					<button type="button" class="btn btn-primary" id="tampil" onclick="load_table()"><i class="fa fa-search"></i> Tampilkan</button> 
					<button type="button" class="btn btn-default" id="download"><i class="fa fa-download"></i> Download</button>
					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
				</div>
			</div>
    	</div>
    	<div id="myTable"></div>
    </div>
</div>

<div id="payroll" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Payroll Detail</h4>
    </div>
    <div class="modal-body">
        <div id="target_modal"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>