<!DOCTYPE html>
<html>
<head>	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Slip Gaji</title>
</head>
	<style type="text/css">
		body{
			font-family: 'Arial';
			font-size: 10px;
		}
	</style>
<body>
	<table border='0' width="100%">
		<tr>
			<td colspan='2'><h2>Slip Gaji</h2></td>
			<td align="right">
				<table border='0'>
					<tr>
						<td>Tanggal</td>
						<td>:</td>
						<?php $date = date_create($slip->tgl_exec); ?>
						<td><?php echo date_format($date,'d/m/Y'); ?></td>
					</tr>
					<tr>
						<td>NIP</td>
						<td>:</td>
						<td><?php echo $kary->nip; ?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='2'>
				<table border='0'>
					<tr>
						<td>Nama</td>
						<td>:</td>
						<td><?php echo $kary->nama; ?></td>
					</tr>
					<tr>
						<td>Posisi</td>
						<td>:</td>
						<td><?php echo $posisi->posisi; ?></td>
					</tr>
				</table>
			</td>
			<td align="right">
				<table border='0'>
					<tr>
						<td>Alamat</td>
						<td>:</td>
						<td><?php echo $kary->alamat; ?></td>
					</tr>
					<tr>
						<td>Telp</td>
						<td>:</td>
						<td><?php echo $kary->telp; ?></td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
	<hr>
	<table border='0' width="100%">
		<tr>
			<td><strong>NO</strong></td>
			<td><strong>KETERANGAN</strong></td>
			<td><strong>JUMLAH</strong></td>
			<td><strong>KETERANGAN</strong></td>
			<td><strong>JUMLAH</strong></td>
		</tr>
		<tr>
			<td>1</td>
			<td>Gaji Dasar</td>
			<td><?php echo $this->Main_Model->uang($slip->gd); ?></td>
			<td>Potongan Pinjaman</td>
			<td><?php echo $this->Main_Model->uang($slip->pot_ags); ?></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Tunjangan Dasar</td>
			<td><?php echo $this->Main_Model->uang($slip->td); ?></td>
			<td>Potongan Asuransi</td>
			<td><?php echo $this->Main_Model->uang($slip->pot_ass); ?></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Tunjangan Jabatan</td>
			<td><?php echo $this->Main_Model->uang($slip->tpj); ?></td>
			<td>Potongan Jabatan</td>
			<td><?php echo $this->Main_Model->uang($slip->pjab); ?></td>
		</tr>
		<tr>
			<td>4</td>
			<td>Tunjangan Komunikasi</td>
			<td><?php echo $this->Main_Model->uang($slip->tk); ?></td>
			<td>Potongan Lainnya</td>
			<td><?php echo $this->Main_Model->uang($slip->pl); ?></td>
		</tr>
		<tr>
			<td>5</td>
			<td>Tunjangan Professional</td>
			<td><?php echo $this->Main_Model->uang($slip->tprof); ?></td>
		</tr>
		<tr>
			<td>6</td>
			<td>Tunjangan Mutasi</td>
			<td><?php echo $this->Main_Model->uang($slip->bbm); ?></td>
		</tr>
		<tr>
			<td>7</td>
			<td>Tunjangan Perumahan</td>
			<td><?php echo $this->Main_Model->uang($slip->bbp); ?></td>
		</tr>
		<tr>
			<td>8</td>
			<td>Tunjangan Lembur</td>
			<td><?php echo $this->Main_Model->uang($slip->tlembur); ?></td>
		</tr>
		<tr>
			<td>9</td>
			<td>Tunjangan Asuransi</td>
			<td><?php echo $this->Main_Model->uang($slip->tass); ?></td>
		</tr>
		<tr>
			<td>10</td>
			<td>Bonus</td>
			<td><?php echo $this->Main_Model->uang($slip->bb + $slip->btw); ?></td>
		</tr>
		<tr>
			<td></td>
			<td><strong>Total Gaji</strong></td>
			<td><?php echo $this->Main_Model->uang($slip->gd+$slip->td+$slip->tpj+$slip->tk+$slip->tprof+$slip->bbm+$slip->bbp+$slip->tlembur+$slip->tass+$slip->bb+$slip->btw); ?></td>
			<td><strong>Total Potongan</strong></td>
			<td><?php echo $this->Main_Model->uang($slip->pot_ags+$slip->pot_ass+$slip->pjab+$slip->pl); ?></td>
		</tr>
		<!-- <tr>
			<td>1</td>
			<td>Potongan Pinjaman</td>
			<td><?php echo $this->Main_Model->uang($slip->pot_ags); ?></td>
		</tr>
		<tr>
			<td>2</td>
			<td>Potongan Asuransi</td>
			<td><?php echo $this->Main_Model->uang($slip->pot_ass); ?></td>
		</tr>
		<tr>
			<td>3</td>
			<td>Potongan Jabatan</td>
			<td><?php echo $this->Main_Model->uang($slip->pjab); ?></td>
		</tr>
		<tr>
			<td>4</td>
			<td>Potongan Lainnya</td>
			<td><?php echo $this->Main_Model->uang($slip->pl); ?></td>
		</tr>
		<tr>
			<td></td>
			<td><strong>Total Potongan</strong></td>
			<td><?php echo $this->Main_Model->uang($slip->pot_ags+$slip->pot_ass+$slip->pjab+$slip->pl); ?></td>
		</tr> -->
		<tr>
			<td></td>
			<td><strong>Total Diterima</strong></td>
			<td><?php echo $this->Main_Model->uang($slip->thp); ?></td>
		</tr>
	</table>
	<hr>
	<table border='0' width="100%">
		<?php if($posisi->idp=='1'){
			$bu = 'Perkasa Telkomselindo';
		}elseif ($posisi->idp=='2') {
			$bu = 'Surya Perkasa Telkomselindo';
		} ?>
		<tr>
			<td align="center">Penerima,</td>
			<td align="center"><?php echo date("d F Y"); ?></td>
		</tr>
		<tr>
			<td align="center"><br><br><br><br><br><?php echo $kary->nama; ?></td>
			<td align="center"><br><br><br><br><br><?php echo $bu; ?></td>
		</tr>
	</table>
	
</body>
</html>