<div class="page-content">
    <div class="breadcrumbs"><h1>Setup Gaji</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <a class="btn btn-primary" href="javascript:;" id="add_new"><i class="fa fa-plus"></i> Tambah Data</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Setup Gaji</h4>
    </div>
    <form id="form_setup">
        <div class="modal-body">
           <div class="form-group">
                <label>Jabatan</label>
                    <input type="hidden" name="id" id="id">
                    <?php echo form_dropdown('jab', $jab, '', 'id="jab" class="select2"'); ?>
           </div>
           <div class="form-group">
                <label>Status</label>
                    <?php echo form_dropdown('status', $status, '', 'id="status" class="select2"'); ?>
            </div>
            <div class="form-group">
                <label>Upah Pokok</label>
                    <input type="number" name="upah_pokok" id="upah_pokok" class="form-control">
            </div>
            <div class="form-group">
                <label>Tunjangan Jabatan</label>
                    <input type="number" name="tunj_jabatan" id="tunj_jabatan" class="form-control">
            </div>
            <div class="form-group">
                <label>Pengh Masa Kerja</label>
                    <input type="number" name="pmasa_kerja" id="pmasa_kerja" class="form-control">
            </div>
            <div class="form-group">
                <label>Tunj Tidak Tetap</label>
                    <input type="number" name="tunj_tdktetap" id="tunj_tdktetap" class="form-control">
            </div>
            <div class="form-group">
                <label>Insentif Presensi</label>
                    <input type="number" name="ins_presensi" id="ins_presensi" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".select2").select2();
        load_table();
    });

    function clear_form() {
        document.getElementById("form_setup").reset();
        $("#id").val('');
        $(".select2").val('').trigger('change');
    }

    $("#add_new").click(function(){
        clear_form(), $("#myModal").modal();
    });

    function load_table() {
        $.ajax({
            url : "<?php echo base_url('setup_gaji/view_setup_gaji'); ?>",
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_setup_gaji").DataTable({
                    responsive : true
                });
            }
        });
    }

    $("#form_setup").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('setup_gaji/process_setup_gaji') ?>",
            type : "post",
            dataType : "json",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success : function(data) {
                if(data.status == true) {
                    clear_form(), load_table();
                }
                bootbox.alert(data.message);
            }
        });
        return false;
    });

    function get_id(id) {
        $.ajax({
            url : "<?php echo base_url('setup_gaji/id_setup_gaji') ?>/"+id,
            dataType : "json",
            success : function(data) {
                $("#id").val(data.id);
                $("#jab").val(data.jab).trigger('change');
                $("#status").val(data.category).trigger('change');
                $("#upah_pokok").val(data.upah_pokok);
                $("#tunj_jabatan").val(data.tunj_jabatan);
                $("#pmasa_kerja").val(data.pmasa_kerja);
                $("#tunj_tdktetap").val(data.tunj_tdktetap);
                $("#ins_presensi").val(data.ins_presensi);
                $("#myModal").modal();
            }
        });
    }

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "<?php echo base_url('setup_gaji/delete_setup_gaji') ?>/"+id,
                            success : function(data){
                                bootbox.alert({
                                    message: "Delete Success",
                                    size: "small"
                                });
                                load_table();
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        })
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>