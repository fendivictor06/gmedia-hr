<div class="page-content">
    <div class="breadcrumbs">
        <h1>Validasi Payroll</h1>
    </div>
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-body">
				<?php echo form_periode(1); ?>
				<div class="form-group">
					<label>Cabang</label>
						<?php echo form_dropdown('cabang',$cabang,'','id="cabang" class="select2" style="width:100%"'); ?>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary" id="tampil" onclick="view_gaji_after_submit($('#periode').val(), $('#cabang').val());"><i class="fa fa-search"></i>Cari</button> 
					<button type="submit" class="btn btn-danger" id="tutup" onclick="tutup_periode();"><i class="fa fa-lock"></i>Tutup Periode</button> 
					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
				</div>
			</div>
		</div>
		<div id="myTable"></div>
	</div>
</div>


<div id="payroll" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Payroll Detail</h4>
    </div>
    <div class="modal-body">
        <div id="target_modal"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<div id="edit_payroll" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Payroll Edit</h4>
    </div>
    <div class="modal-body">
        <div id="target_modal_edit"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>