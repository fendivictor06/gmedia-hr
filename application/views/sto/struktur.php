<div class="page-content">
	<div class="row">

		<div class="col-md-12">
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#tab_1" data-toggle="tab" >Struktur STO</a>
				</li>
				<li>
					<a href="#tab_2" data-toggle="tab" >Delete STO</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="tab_1">
					<div class="col-md-12">
						<div class="btn-group">
		                    <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
		                    <i class="fa fa-plus"></i> Tambah Struktur STO
		                    </button>
		                </div>
		            </div>
	                <?php 
	                	$idp = $this->session->userdata('idp');
						$q = $this->db->query("select * from sto_rel a join pos_sto b on a.id_sto=b.id_sto join pos c on b.id_pos=c.id_pos where a.id_parent='0' and c.idp='$idp'")->result();
						for($o=1;$o<=count($q);$o++)
						{
	                 ?>

							<div class="chart" id="basic-example<?php echo $o; ?>" ></div>

						<?php } ?>

				</div>
				<div class="tab-pane fade" id="tab_2">
					<!-- <div class="form-group">
						<label>STO</label>
							<?php 
								echo form_dropdown('del_sto',$sto_rel_opt,'','id="del_sto" class="select2" style="width:100%"');
							?>
					</div>
					<div class="form-group">
						<button type="button" class="btn btn-danger" onclick="delete_data();">Delete</button>
					</div> -->
					<?php echo $sto_rel_tbl; ?>
				</div>
			</div>
		</div>


	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Struktur STO</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="form_sto">
					<div class="form-group">
						<label>STO</label>
							<?php 
								echo form_dropdown('sto',$sto,'','id="sto" class="select2" style="width:100%"');
							?>
					</div>
					<div class="form-group">
						<label>Parent</label>
							<?php 
								echo form_dropdown('parent',$sto_rel_opt,'','id="parent" class="select2" style="width:100%"');
							?>
					</div>
				</form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
<!-- 	</main>

    </div>
	<footer id="footer">Copyright © 2016 <a href="#" title="Perkasa App">Perkasa App</a></footer>
</body>
</html> -->

	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © Perkasa App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</diV>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

	<?php echo isset($js)?$js:''; ?>

	<?php echo isset($javascript)?$javascript:''; ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$(".select2").select2();
		});
		$("#myTable").DataTable({
        	responsive: true
    	});
		<?php 
			

			if($q){

			$i=1;
				foreach ($q as $row) {
				
		 ?>
		
		var chart_config<?php echo $i; ?> = {
	        chart: {
	            container: "#basic-example<?php echo $i; ?>",
	            
	            connectors: {
	                type: 'step'
	            },
	            node: {
	                HTMLclass: 'nodeExample1'
	            }
	        },

	        <?php 
	        	
	        	
	        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
	        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
	        		$posisi = isset($sto->jab)?$sto->jab:'';
	        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

		            if($tgl_resign=='')
		            {
		                $nama = isset($nip->nama)?$nip->nama:'';
		            }
		            else
		            {
		                $nama = '';
		            }
	        		echo 'nodeStructure : {
	        					text: {
	        						name :"'.$nama.'",
	        						title : "'.$posisi.'",
	        					},
	        			';
	        	
		        	// child 1
		        	$w = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
		        	echo 'children : [';
		        	foreach ($w as $row) {
		        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
		        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
		        		$posisi = isset($sto->jab)?$sto->jab:'';
		        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

			            if($tgl_resign=='')
			            {
			                $nama = isset($nip->nama)?$nip->nama:'';
			            }
			            else
			            {
			                $nama = '';
			            }
		        		echo '{
		        				text: {
		        						name :"'.$nama.'",
		        						title : "'.$posisi.'",
		        					},
		        			';
		        		//child 2
		        		$e = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
		        		if($e){
		        			echo 'children : [';
				        	foreach ($e as $row) {
				        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
				        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
				        		$posisi = isset($sto->jab)?$sto->jab:'';
				        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

					            if($tgl_resign=='')
					            {
					                $nama = isset($nip->nama)?$nip->nama:'';
					            }
					            else
					            {
					                $nama = '';
					            }
				        		echo '{
				        				text: {
				        						name :"'.$nama.'",
				        						title : "'.$posisi.'",
				        					},stackChildren: true,
				        			';
				        		// child 3
					        	$r = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
					        	if($r){
					        		echo 'children : [';
						        	foreach ($r as $row) {
						        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
						        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
						        		$posisi = isset($sto->jab)?$sto->jab:'';
						        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

							            if($tgl_resign=='')
							            {
							                $nama = isset($nip->nama)?$nip->nama:'';
							            }
							            else
							            {
							                $nama = '';
							            }
						        		echo '{
						        				text: {
						        						name :"'.$nama.'",
						        						title : "'.$posisi.'",
						        					},stackChildren: true,
						        			';
						        	// child 4
						        	$t = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
						        	if($t){
						        		echo 'children : [';
							        	foreach ($t as $row) {
							        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
							        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
							        		$posisi = isset($sto->jab)?$sto->jab:'';
							        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

								            if($tgl_resign=='')
								            {
								                $nama = isset($nip->nama)?$nip->nama:'';
								            }
								            else
								            {
								                $nama = '';
								            }
							        		echo '{
							        				text: {
							        						name :"'.$nama.'",
							        						title : "'.$posisi.'",
							        					},stackChildren: true,
							        			';
							        	// child 5
							        	$y = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
							        	if($y){
							        		echo 'children : [';
								        	foreach ($y as $row) {
								        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
								        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
								        		$posisi = isset($sto->jab)?$sto->jab:'';
								        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

									            if($tgl_resign=='')
									            {
									                $nama = isset($nip->nama)?$nip->nama:'';
									            }
									            else
									            {
									                $nama = '';
									            }
								        		echo '{
								        				text: {
								        						name :"'.$nama.'",
								        						title : "'.$posisi.'",
								        					},stackChildren: true,
								        			';
								        	// child 6
								        	$u = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
								        	if($u){
								        		echo 'children : [';
									        	foreach ($u as $row) {
									        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
									        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
									        		$posisi = isset($sto->jab)?$sto->jab:'';
									        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

										            if($tgl_resign=='')
										            {
										                $nama = isset($nip->nama)?$nip->nama:'';
										            }
										            else
										            {
										                $nama = '';
										            }
									        		echo '{
									        				text: {
									        						name :"'.$nama.'",
									        						title : "'.$posisi.'",
									        					},stackChildren: true,
									        			';
									        	// child 7
									        	$i = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
									        	if($i){
									        		echo 'children : [';
										        	foreach ($i as $row) {
										        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
										        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
										        		$posisi = isset($sto->jab)?$sto->jab:'';
										        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

											            if($tgl_resign=='')
											            {
											                $nama = isset($nip->nama)?$nip->nama:'';
											            }
											            else
											            {
											                $nama = '';
											            }
										        		echo '{
										        				text: {
										        						name :"'.$nama.'",
										        						title : "'.$posisi.'",
										        					},stackChildren: true,
										        			';
										        	// child 8
										        	$o = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
										        	if($o){
										        		echo 'children : [';
											        	foreach ($o as $row) {
											        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
											        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
											        		$posisi = isset($sto->jab)?$sto->jab:'';
											        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

												            if($tgl_resign=='')
												            {
												                $nama = isset($nip->nama)?$nip->nama:'';
												            }
												            else
												            {
												                $nama = '';
												            }
											        		echo '{
											        				text: {
											        						name :"'.$nama.'",
											        						title : "'.$posisi.'",
											        					},stackChildren: true,
											        			';
											        	// child 9
											        	$p = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
											        	if($p){
											        		echo 'children : [';
												        	foreach ($p as $row) {
												        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
												        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
												        		$posisi = isset($sto->jab)?$sto->jab:'';
												        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

													            if($tgl_resign=='')
													            {
													                $nama = isset($nip->nama)?$nip->nama:'';
													            }
													            else
													            {
													                $nama = '';
													            }
												        		echo '{
												        				text: {
												        						name :"'.$nama.'",
												        						title : "'.$posisi.'",
												        					},stackChildren: true,
												        			';
												        	// child 10
												        	$a = $this->db->query("select * from sto_rel where id_parent='$row->id_sto'")->result();
												        	if($a){
												        		echo 'children : [';
													        	foreach ($a as $row) {
													        		$sto = $this->db->query("SELECT c.`jab`,b.`id_sto` FROM pos_sto b JOIN pos c ON b.`id_pos`=c.`id_pos` WHERE b.`id_sto`='$row->id_sto'")->row();
													        		$nip = $this->db->query("SELECT b.`nama`,b.`tgl_resign` FROM sk a JOIN kary b ON a.`nip`=b.`nip`WHERE a.`id_pos_sto`='$row->id_sto' AND a.`aktif`='1'")->row();
													        		$posisi = isset($sto->jab)?$sto->jab:'';
													        		$tgl_resign = isset($nip->tgl_resign)?$nip->tgl_resign:'';

														            if($tgl_resign=='')
														            {
														                $nama = isset($nip->nama)?$nip->nama:'';
														            }
														            else
														            {
														                $nama = '';
														            }
													        		echo '{
													        				text: {
													        						name :"'.$nama.'",
													        						title : "'.$posisi.'",
													        					},stackChildren: true,
													        			},';
													        	}
													        	echo ']},';
												        	}
												        	else
												        	{
												        		echo '},';
												        	}
												        	// end child 10
												        	}
												        	echo ']},';
											        	}
											        	else
											        	{
											        		echo '},';
											        	}
											        	// end child 9
											        	}
											        	echo ']},';
										        	}
										        	else
										        	{
										        		echo '},';
										        	}
										        	// end child 8
										        	}
										        	echo ']},';
									        	}
									        	else
									        	{
									        		echo '},';
									        	}
									        	// end child 7
									        	}
									        	echo ']},';
								        	}
								        	else
								        	{
								        		echo '},';
								        	}
								        	// end child 6
								        	}
								        	echo ']},';
							        	}
							        	else
							        	{
							        		echo '},';
							        	}
							        	// end child 5
							        	}
							        	echo ']},';
						        	}
						        	else
						        	{
						        		echo '},';
						        	}
						        	// end child 4
						        	}
						        	echo ']},';
					        	}
					        	else
					        	{
					        		echo '},';
					        	}
					        	// end child 3
				        	}
				        	echo ']},';
				        	

		        		}else{
		        			echo '},';
		        		}
		        	}
		        
	        	
	        	echo ']';

	        	echo '}';
	        ?>
	    };

	    new Treant( chart_config<?php echo $i++; ?> );

	    <?php
	    }// end foreach 
	    	
				}//end if 

				?>
	</script>


	<script type="text/javascript">
		function profile(){
			$("#myProfile").modal();
		}
		function ChangePT(id){
			$.ajax({
				url 	: "<?php echo base_url('main/changept'); ?>/"+id,
				type 	: "POST",
				success : function(data){
					location.reload();
				},
				error 	: function(jqXHR, textStatus, errorThrown){
					alert("Oops Something went wrong !");
				} 
			})
		}
	</script>

</body>
</html>