<div class="page-content">
	<div class="breadcrumbs">
		<h1>Edit STO</h1>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-search"></i> Cari
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
					<table style="width:100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
							<tr>
								<th>No</th>
								<th>ID Posisi STO</th>
								<th>Lokasi Utama</th>
								<th>Lokasi Akhir</th>
								<th>Posisi</th>
								<th>Edit</th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
			</div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Cari STO</h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger hidden" id="warning">
            Please Complete this form!
        </div>
        <div class="alert alert-success hidden" id="success">
            Success!
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Lokasi</label>
                    <?php echo form_dropdown('lka',$lka,'','id="lka" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>Perusahaan</label>
                    <?php echo form_dropdown('idp',$idp,'','id="idp" class="form-control"'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="cari();" class="btn btn-primary">Cari</button>
    </div>
</div>