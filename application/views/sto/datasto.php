<div class="page-content">
	<div class="breadcrumbs">
		<h1>STO</h1>
	</div>
	<div class="row">
        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        	<button type="button" class="btn btn-primary pull-left" onclick="reset();" data-toggle="modal" data-target="#myModal">Posisi Baru Tambah ke STO</button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        	<br>
        	<div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form STO</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_sto">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label>Posisi</label>
                            <?php echo form_dropdown('pos', $sto, '', 'id="pos" class="select2"'); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>