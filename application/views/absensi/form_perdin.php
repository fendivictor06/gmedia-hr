<div class="page-content">
    <div class="breadcrumbs"><h1>Perjalanan Dinas</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php  
                            $btn = '<button type="button" class="btn btn-primary pull-right"  id="add_new"> <i class="fa fa-plus"></i> Tambah Data</button>';
                            echo form_periode(2, $btn);
                        ?>
                    </div>
                </div> <br>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Perjalanan Dinas</h4>
    </div>
    <form id="form_perdin">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id" >
                    <input type="hidden" name="nobukti" id="nobukti" >
                    <div class="form-group">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang_create', $cabang_create, '', 'id="cabang_create" class="select2" onchange="carikaryawan()"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip[]', $nip, '', 'id="nip" class="bs-select form-control" data-live-search="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Waktu Keberangkatan</label>
                            <input type="text" size="16" data-date-format="dd/mm/yyyy" class="form-control hitung date-picker" id="waktu_berangkat" name="waktu_berangkat" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Waktu Kepulangan</label>
                            <input type="text" size="16" data-date-format="dd/mm/yyyy" class="form-control hitung date-picker" id="waktu_pulang" name="waktu_pulang" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Nominal</label>
                            <input type="number" name="nominal" id="nominal" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jenis Tujuan</label>
                            <select class="form-control" name="dinas" id="dinas">
                                <option value="0">Dinas Cabang</option>
                                <option value="1">Dinas Luar Cabang</option>
                            </select>
                    </div>
                    <div class="form-group hidden" id="dl">
                        <label>Tujuan</label>
                            <textarea class="form-control" name="tujuan" id="tujuan" rows="5"></textarea>
                    </div>
                    <div class="form-group hidden" id="dd">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Keperluan</label>
                            <textarea class="form-control" name="keperluan" id="keperluan" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<div id="modalDetail" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Detail Perjalanan Dinas</h4>
    </div>
    <div class="modal-body">
        <div id="view_detail"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" id="close_x" class="btn btn-outline dark">Close</button>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    function select_dinas(val='') {
        (val == '') ? value = $("#dinas").val() : value = val;

        if(value == "0") {
            $("#dl").addClass("hidden"), $("#dd").removeClass("hidden");
            $("#tujuan").val("");
        } else {
            $("#dl").removeClass("hidden"), $("#dd").addClass("hidden");
        }
    }

    $("#dinas").change(function(){
        select_dinas();
    });

    function clearform() {
        document.getElementById('form_perdin').reset();
        $("#id").val(""), $("#nobukti").val("");
        $('.bs-select').selectpicker('refresh');
        select_dinas(0);
    }

    $("#add_new").click(function(){
        clearform(), $("#myModal").modal();
    });

    function hitung_nominal(){
        waktu_berangkat = $("#waktu_berangkat").val();
        waktu_pulang = $("#waktu_pulang").val();
        $.ajax({
            url : "perdin/hitung_nominal",
            data : {
                start : waktu_berangkat,
                end : waktu_pulang
            },
            type : "post",
            dataType : "json",
            success : function(data) {
                $("#nominal").val(data.nominal);
            }
        });
    }

    $(".hitung").change(function(){
        hitung_nominal();
    });

    $("#form_perdin").submit(function(event){
        event.preventDefault();
        formData = new FormData($(this)[0]);
        $.ajax({
            url : "perdin/perdin_process",
            type : "post",
            data : formData,
            async : false,
            cache : false,
            dataType : "json",
            contentType : false,
            processData : false,
            beforeSend : function() {
                $("#save").attr('disabled', true);
            },
            complete : function() {
                $("#save").attr('disabled', false);
            },
            success : function(data) {
                if(data.status == true) {
                    load_table(), clearform();
                    $("#myModal").modal('toggle');
                }
                bootbox.alert(data.message);
            }
        });
        return false;
    });

    $(document).ready(function(){
        <?php  
            $month = date('m');
            $month = str_replace('0', '', $month);

            $year = date('Y');
        ?>

        var year = "<?php echo $year; ?>";
        var month = "<?php echo $month; ?>";

        $("#tahun").val(year).trigger("change");
        $("#bulan").val(month).trigger("change");
        
        select_dinas();

        $(".select2").select2();

        $(".bs-select").selectpicker({
            iconBase: "fa",
            tickIcon: "fa-check"
        });

        $(".form_datetime").datetimepicker({
            autoclose : !0,
            format : "dd/mm/yyyy hh:ii",
            pickerPosition : "bottom-left"
        });

        load_table(), carikaryawan();
    });

    function load_table() {
        var tahun = $("#tahun").val();
        var bulan = $("#bulan").val();
        $.ajax({
            url : "perdin/view_perdin",
            data : {
                "tahun" : tahun,
                "bulan" : bulan
            },
            beforeSend : function() {
                $("#tampil").addClass("hidden");
                $("#imgload").removeClass("hidden");
                $("#unduh").addClass("hidden");
            },
            complete : function() {
                $("#tampil").removeClass("hidden");
                $("#imgload").addClass("hidden");
                $("#unduh").removeClass("hidden");
            },
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_perdin").DataTable({
                    responsive : true
                });
            }
        });
    }

    $("#tampil").click(function(){
        load_table();
    });

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "perdin/delete_perdin/"+id,
                            dataType : "json",
                            success : function(data){
                                if(data.status == true) {
                                    load_table();
                                }
                                bootbox.alert(data.message);
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        })
    }

    function get_id(id) {
        $.ajax({
            url : "perdin/perdin_id?nobukti="+id,
            dataType : "json",
            success : function(data){
                (data[0].id_cabang == 0) ? dinas = 1 : dinas = 0;
                $("#waktu_berangkat").val(data[0].berangkat); 
                $("#waktu_pulang").val(data[0].pulang);
                $("#nominal").val(data[0].nominal);
                $("#cabang_create").val(data[0].cabang_create).trigger("change");
                $("#tujuan").val(data[0].tujuan);
                $("#cabang").val(data[0].id_cabang).trigger("change");
                $("#keperluan").val(data[0].keperluan);
                select_dinas(dinas);
                $("#dinas").val(dinas);

                $("#id").val(data[0].id_perdin);
                $("#nobukti").val(data[0].nobukti);

                nip_val = [];
                for(i=0; i<data.length; i++) {
                    nip_val.push(data[i].nip);
                }
                setTimeout(function(){ $("#nip").selectpicker('val', nip_val); }, 1500);

                $("#myModal").modal();
            }
        });
    }

    function detail(id) {
        $.ajax({
            url : "perdin/view_detail_perdin?nobukti="+id,
            success : function(data) {
                $("#view_detail").html(data);
                $("#tb_perdin_detail").DataTable({
                    responsive : true
                });
                $("#modalDetail").modal();
            }
        });
    }

    function verif(id) {
        bootbox.dialog({
            message : "Yakin ingin memverifikasi data?",
            title : "Verifikasi Data",
            buttons :{
                danger : {
                    label : "Verifikasi",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "perdin/verifikasi/"+id,
                            dataType : "json",
                            success : function(data) {
                                if(data.status == true) {
                                    load_table();
                                }
                                bootbox.alert(data.message);
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        })
    }

    function carikaryawan() {
        cabang_create = $("#cabang_create").val();
        $("#nip option").remove();
        $.getJSON("<?php echo base_url('perdin/carikaryawan'); ?>/"+cabang_create, function(jsonResult){
            $("#nip").attr("enabled","true");
            if (jsonResult != null) {
                $.each(jsonResult, function(){
                    $("#nip").append(
                        $("<option></option>").text(this.nip+' - '+this.nama).val(this.nip)
                    );
                });
            }
            $('#nip').selectpicker('refresh');
        });
    }

    $("#unduh").click(function(){
        var tahun = $("#tahun").val();
        var bulan = $("#bulan").val();
        window.location.href="<?php echo base_url('download_excel/laporan_perdin'); ?>/"+tahun+"/"+bulan;
    });

    <?php echo $this->Main_Model->default_datepicker(); ?>
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>