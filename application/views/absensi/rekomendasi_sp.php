<div class="page-content">
    <div class="breadcrumbs">
        <h1>Rekomendasi SP Karyawan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <?php  
                    $btn = '<button type="button" class="btn btn-danger" id="proses"><i class="fa fa-refresh"></i> Proses</button>';
                    echo form_periode(null, $btn);
                ?> <br>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>