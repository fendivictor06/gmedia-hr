<div class="page-content">
    <div class="breadcrumbs"><h1>Template Shift</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <?php if($acctype == 'Administrator') echo '<div class="form-group"> <button type="button" class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Tambah Data</button> </div>';  ?>
                <div id="myTable"></div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Penjadwalan</h4>
    </div>
    <form id="form_jadwal">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>Senin</label>
                            <?php echo form_dropdown('senin', $shift, '', 'id="senin" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Selasa</label>
                            <?php echo form_dropdown('selasa', $shift, '', 'id="selasa" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Rabu</label>
                            <?php echo form_dropdown('rabu', $shift, '', 'id="rabu" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Kamis</label>
                            <?php echo form_dropdown('kamis', $shift, '', 'id="kamis" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Jumat</label>
                            <?php echo form_dropdown('jumat', $shift, '', 'id="jumat" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Sabtu</label>
                            <?php echo form_dropdown('sabtu', $shift, '', 'id="sabtu" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Minggu</label>
                            <?php echo form_dropdown('minggu', $shift_minggu, '', 'id="minggu" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
            <img id="loading" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
        </div>
    </form>
</div>

<div id="copyModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Copy Penjadwalan</h4>
    </div>
    <form id="form_copy">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id_copy" id="id_copy" >
                    <div class="form-group">
                        <label>Copy to</label>
                            <select class="form-control" name="copy_to" id="copy_to">
                                <option value="1">All</option>
                                <option value="2">By 1</option>
                            </select>
                    </div>
                    <div class="form-group hidden" id="target">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip_copy[]', $nip, '', 'id="nip_copy" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tgl Awal</label>
                            <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group">
                        <label>Tgl Akhir</label>
                            <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <!-- <div class="form-group">
                        <label>Keterangan</label>
                            <textarea name="keterangan_copy" id="keterangan_copy" class="form-control" rows="5"></textarea>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark" id="close_copy">Close</button>
            <button type="submit" class="btn btn-primary" id="save_copy">Simpan</button>
            <img id="loading_copy" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
    
    <script type="text/javascript">
        function load_table() {
            $.ajax({
                url : "<?php echo base_url('shift/view_template'); ?>",
                success : function(data) {
                    $("#myTable").html(data);
                    $("#dataTables-example").DataTable({
                        responsive : true
                    });
                }
            });
        }

        function salin(id){
            $("#id_copy").val(id);
            $("#copyModal").modal();
        }

        $(document).ready(function(){
            $(".select2").select2(), load_table();

            $(".date-picker").datepicker({
                orientation: "left", 
                autoclose: !0
            });

            $(".bs-select").selectpicker({
                iconBase: "fa",
                tickIcon: "fa-check"
            });
        });

        function add_new() {
            document.getElementById('form_jadwal').reset();
            $(".select2").val("").trigger("change");
        }

        $("#copy_to").change(function(){
            ($(this).val() == 2) ? $("#target").removeClass("hidden") : $("#target").addClass("hidden");
        });

        $("#add").click(function(){
            add_new(), $("#myModal").modal();
            $('.bs-select').selectpicker('refresh');
        });

        $("#form_jadwal").submit(function(event){
            event.preventDefault();
            formData = new FormData($(this)[0]);
            $.ajax({
                url : "process_template",
                type : "post",
                data : formData,
                cache : false,
                dataType : "json",
                contentType : false,
                processData : false,
                success : function(data) {
                    if(data.status == true) {
                        load_table(), add_new();
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });

        function get_id(id) {
            $.ajax({
                url : "id_template/"+id,
                dataType : "json",
                success : function(data) {
                    $("#id").val(data.id);
                    $("#senin").val(data.senin).trigger("change");
                    $("#selasa").val(data.selasa).trigger("change");
                    $("#rabu").val(data.rabu).trigger("change");
                    $("#kamis").val(data.kamis).trigger("change");
                    $("#jumat").val(data.jumat).trigger("change");
                    $("#sabtu").val(data.sabtu).trigger("change");
                    $("#minggu").val(data.minggu).trigger("change");
                    $("#keterangan").val(data.description);
                    $("#myModal").modal();
                }
            });
        }

        function delete_data(id) {
            bootbox.dialog({
                message : "Yakin ingin menghapus data?",
                title : "Hapus Data",
                buttons :{
                    danger : {
                        label : "Delete",
                        className : "red",
                        callback : function(){
                            $.ajax({
                                url : "delete_template/"+id,
                                dataType : "json",
                                success : function(data){
                                    if(data.status == true) {
                                        load_table();
                                    }
                                    bootbox.alert(data.message);
                                }
                            });
                        }
                    },
                    main : {
                        label : "Cancel",
                        className : "blue",
                        callback : function(){
                            return true;
                        }
                    }
                }
            })
        }

        $("#form_copy").submit(function(event){
            event.preventDefault();
            formData = new FormData($(this)[0]);
            $.ajax({
                url : "copy_jadwal",
                type : "post",
                data : formData,
                // async : false,
                cache : false,
                dataType : "json",
                contentType : false,
                processData : false,
                beforeSend : function() {
                    $("#close_copy").addClass("hidden"), $("#save_copy").addClass("hidden"), $("#loading_copy").removeClass("hidden");
                },
                complete : function() {
                    $("#close_copy").removeClass("hidden"), $("#save_copy").removeClass("hidden"), $("#loading_copy").addClass("hidden");
                },
                success : function(data) {
                    if(data.status == true) {
                        load_table(), add_new(), $("#keterangan_copy").val("");
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });

    </script>

<?php echo isset($penutup) ? $penutup : ''; ?>