<div class="page-content">
	<div class="breadcrumbs">
		<h1 class="page-header">Absensi</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-inline" role="form" id="form_periode">
						    <div class="form-group col-md-2" style="padding-left:0px;">
						        <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy">
						    </div>
						    <div class="form-group col-md-2" style="padding-left:0px;">
						        <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
						    </div>
						    <button type="button" id="tampil" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
						    <button type="button" id="unduh" class="btn btn-default"><i class="fa fa-download"></i> Download</button>
						    <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
						</form>
                    </div>
                </div>
			</div>
			<br>
			<div id="absensi"></div>
		</div>
	</div>
</div>



	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © <?php echo company(); ?> App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</diV>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

	<?php echo isset($js)?$js:''; ?>

	<?php echo isset($javascript)?$javascript:''; ?>

	<?php echo $this->session->flashdata('message'); ?>


	<script type="text/javascript">
		function ChangePT(id) {$.ajax({url : "<?php echo base_url('main/changept'); ?>/"+id, type : "POST", success : function(data){location.reload(); }, error : function(jqXHR, textStatus, errorThrown){alert("Oops Something went wrong !"); } }); }
	</script>

</body>
</html>