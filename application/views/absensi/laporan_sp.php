<div class="page-content">
    <div class="breadcrumbs">
        <h1>Laporan Surat Peringatan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="form-group">
                    <label>Bulanan/Tahunan</label>
                        <select name="mode" id="mode" class="form-control">
                            <option value="1">Bulanan</option>
                            <option value="2">Tahunan</option>
                        </select>
                </div>
                <div class="form-group" id="bl">
                    <label>Bulan</label>
                        <select name="bulan" id="bulan" class="select2" style="width:100%">
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">July</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                </div>
                <div class="form-group" id="th">
                    <label>Tahun</label>
                        <?php echo form_dropdown('tahun', $th, '', 'id="tahun" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <button id="download" class="btn btn-primary"><i class="fa fa-download"></i>Download</button>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>

<script>
    $(document).ready(function(){
        $(".select2").select2();
    });

    $("#mode").change(function(){
        if($(this).val() == 2) {
            $("#bl").addClass("hidden");
        } else {
            $("#bl").removeClass("hidden");
        }
    })

    $("#download").click(function(){
        bulan = $("#bulan").val(), tahun = $("#tahun").val(), mode = $("#mode").val();
        (mode == 1) ? link = tahun+"/"+bulan : link = tahun;
        window.location.href = "<?php echo base_url('download_excel/laporan_sp'); ?>/"+link;
    })
</script>

<?php echo isset($penutup) ? $penutup : ''; ?>