<div class="page-content">
    <div class="breadcrumbs">
        <h1 class="page-header">Detail Meninggalkan Kerja</h1>
    </div>
    <div class="row">
		<div class="portlet light">
			<div class="portlet-body"> 
				<form id="ijinjam">
					<div class="form-group">
						<label>Nama</label>
						<?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2" style="width:100%"'); ?>
					</div>
					<div class="form-group">
						<label>Bulan</label>
						<?php echo form_dropdown('bulan', $bulan, '', 'id="bulan" class="select2" style="width:100%"'); ?>
					</div>
					<div class="form-group">
						<label>Tahun</label>
						<?php echo form_dropdown('th', $th, '', 'id="th" class="select2" style="width:100%"'); ?>
					</div>
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Tampilkan</button>
						<button class="btn btn-default" type="button" id="download">Download</button>
					</div>
				</form>
			</div>
			<div id="myTable"> </div>
		</div>
	</div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
	$("#ijinjam").submit(function(event){
	    event.preventDefault();
	    formData = new FormData($(this)[0]);
	    $.ajax({
	        url : "view_detail_ijinjam",
	        type : "post",
	        data : formData,
	        async : false,
	        cache : false,
	        contentType : false,
	        processData : false,
	        success : function(data) {
	            $("#myTable").html(data);
	            $("#tb_ijinjam").DataTable({
	            	responsive : true
	            });
	        }
	    });
	    return false;
	});

	$("#download").click(function(){
		nip = $("#nip").val();
		bulan = $("#bulan").val();
		th = $("#th").val();
		window.location.href="<?php echo base_url('download_excel/detail_ijinjam'); ?>/"+nip+"/"+bulan+"/"+th;
	});

	$(document).ready(function(){
		$(".select2").select2();
	});
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>