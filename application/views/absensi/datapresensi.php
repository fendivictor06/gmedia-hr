<div class="page-content">
	<div class="breadcrumbs">
		<h1>Data Presensi</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="row">
					<div class="col-md-12">
						<form class="form-inline" role="form" id="form_periode">
						    <div class="form-group col-md-2" style="padding-left:0px;">
						        <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy">
						    </div>
						    <div class="form-group col-md-2" style="padding-left:0px;">
						        <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
						    </div>
						    <button type="button" id="tampil" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
						    <button type="button" id="unduh" class="btn btn-default"><i class="fa fa-download"></i> Download</button>
						    <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
						</form>
					</div>
				</div>
			</div>
			<br>
			<div id="presensi"></div>
		</div>
	</div>
</div>


