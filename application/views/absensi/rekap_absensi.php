<div class="page-content">
    <div class="breadcrumbs">
        <h1>Rekapitulasi Absensi</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <form id="laporan">
                                <div class="form-group">
                                    <label>Cabang <span class="required">*</span></label>
                                        <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="select2" style="width:100%"'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Mulai <span class="required">*</span></label>
                                        <input type="text" class="form-control date-picker" name="date_start" id="date_start" data-date-format="dd/mm/yyyy" />
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Selesai <span class="required">*</span></label>
                                        <input type="text" class="form-control date-picker" name="date_end" id="date_end" data-date-format="dd/mm/yyyy" />
                                </div>
                            </form>
                            <div class="form-group">
                                <button type="button" class="btn btn-default" id="download">
                                <i class="fa fa-download"></i> Download
                                </button>
                                <button type="button" class="btn btn-primary" id="tampil">
                                <i class="fa fa-eye"></i> Tampilkan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
            </div>
            <div id="myTable"></div>
            
        </div>
    </div>
</div>
<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
	<?php echo 	$this->Main_Model->default_datepicker().
				$this->Main_Model->default_select2(); ?>
	$("#tampil").click(function(){
		$("#myTable").html("");
		$("#imgload").removeClass("hidden");
		$("#tampil").prop("disabled", true);
		$.ajax({
			url : "<?php echo base_url('rekap_absensi/view_rekap'); ?>",
			data : $("#laporan").serialize(),
			// berforeSend : function(){
			// 	$("#myTable").html("");
			// 	$("#imgload").removeClass("hidden");
			// },
			// complete : function(){
			// 	$("#imgload").addClass("hidden");
			// },
			success : function(data) {
				$("#tampil").prop("disabled", false);
				$("#imgload").addClass("hidden");
				$("#myTable").html(data);
				$("#tb_rekap").DataTable();
                // $(".buttons-html5").addClass("btn btn-success").css("margin-right","2px");
			}
		})
	});

    $("#download").click(function(){
        var cabang = $("#cabang").val();
        var date_start = $("#date_start").val();
        var date_end = $("#date_end").val();
        window.location.href="<?php echo base_url('rekap_absensi/rekap_excel?cabang='); ?>"+cabang+"&date_start="+date_start+"&date_end="+date_end;
    })
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>