<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data SP Karyawan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <?php  
                    $btn = '<button type="button" class="btn btn-primary pull-right" id="tambah" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data</button>';
                    echo form_periode(null, $btn);
                ?> <br>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <form id="form_sp" enctype="multipart/form-data" method="post" action="<?php echo base_url('surat_peringatan/sp_process'); ?>">
        <div class="modal-header">
            <h4 class="modal-title">Form Surat Peringatan</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label class="control-label">Nama Karyawan <span class="required"> * </span></label>
                            <?php echo form_dropdown('nip', $karyawan, '', 'id="nip" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Status SP : <span id="jml_sp"></span></label> <br>
                        <label>Akhir SP : <span id="akhir_sp"></span></label>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Periode Terbit SP <span class="required"> * </span></label>
                            <?php echo form_dropdown('periode', $periode, '', 'id="periode" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tipe SP <span class="required"> * </span></label>
                            <?php echo form_dropdown('tipe', $tipe, '', 'id="tipe" class="form-control blank"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">SP <span class="required">*</span></label>
                            <select name="sp" id="sp" class="form-control blank">
                                <option value="1">SP 1</option>
                                <option value="2">SP 2</option>
                                <option value="3">SP 3</option>
                                <option value="4">SP 4 / PHK</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <label class="control-label">Surat Kesanggupan</label>
                            <div class="input-group">
                                <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename"> </span>
                                </div>
                                <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> Select file </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="userfile" id="userfile"> </span>
                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Alasan SP <span class="required"> * </span></label>
                            <textarea class="form-control blank" name="keterangan" id="keterangan" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>
