<?php 
	// Fungsi header dengan mengirimkan raw data excel
	header("Content-type: application/vnd-ms-excel");
 
	// // Mendefinisikan nama file ekspor "hasil-export.xls"
	$filename = $title;
	// header("Content-Disposition: attachment; filename=\"" . $filename . "\".xls");
	header("Content-Disposition: attachment; filename=$filename.xls"); 
	header("Pragma: no-cache"); 
	header("Expires: 0");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

	echo $tabel;
?>