<div class="page-content">
	<div class="breadcrumbs">
		<h1>Presensi Karyawan</h1>
	</div>
	<div class="row">
        <div class="portlet light">
            <div class="portlet-body">
            	<?php $kary = $this->Main_Model->kary_nip($this->uri->segment(3)); ?>
                <table>
                    <tr> <td>NIP</td> <td width="20px"> : </td> <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td> </tr> 
                    <tr> <td>Nama</td> <td> : </td> <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td> </tr>
                </table>
                <br>
                <div class="table-toolbar">
                    <div class="row">
                    	<div class="col-md-12">
                    		<?php echo form_periode(); ?>
                    	</div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Klarifkasi Absensi</h4>
    </div>
    <form id="form_presensi">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_ab" id="id_ab">
                    <input type="hidden" name="nip" id="nip">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control" readonly="readonly" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                    </div>
                    <div class="form-group">
                        <label>Presensi</label>
                            <?php echo form_dropdown('absen', $absen, '', 'class="form-control" id="absen"'); ?>
                    </div>
                    <div id="kuota_cuti"></div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Jenis Cuti Khusus</label>
                            <?php echo form_dropdown('cuti_khusus', $tipe, '', 'id="cuti_khusus" class="form-control"'); ?>
                    </div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Awal Cuti Khusus</label>
                            <input type="text" name="awal_cuti_khusus" id="awal_cuti_khusus" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Akhir Cuti Khusus</label>
                            <input type="text" name="akhir_cuti_khusus" id="akhir_cuti_khusus" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group masuk hidden">
                        <label>Scan Masuk</label>
                            <!-- <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24"> -->
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" class="form-control" id="scan_masuk" name="scan_masuk" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                    </div>
                    <div class="form-group masuk hidden">
                        <label>Scan Pulang</label>
                            <!-- <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24"> -->
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" class="form-control" id="scan_pulang" name="scan_pulang" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="ket" id="ket" rows="5" readonly="readonly"></textarea>
                    </div>
                    <div class="form-group">
                        <label>File Upload</label>
                            <input type="file" name="file_upload" id="file_upload" class="form-control">
                    </div>
                    <div class="form-group klarifikasi">
                        <label>Klarifikasi</label>
                            <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save_button" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div> 
