<div class="page-content">
	<div class="breadcrumbs">
		<h1>Absensi Karyawan</h1>
	</div>
	<div class="row">

            <div class="portlet light">
                <div class="portlet-body">
                    <?php $kary = $this->Main_Model->kary_nip($this->uri->segment(3)); ?>
                   
                    <table>
                        <tr>
                            <td>NIP</td>
                            <td width="20px"> : </td>
                            <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td> : </td>
                            <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td>
                        </tr>
                        
                    </table>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="btn-group">
                                    <!-- <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i> Tambah Presensi
                                    </button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <img src="<?php echo base_url('assets/img/loading.gif') ?>" class="hidden" id="imgload">
                <div id="myTable"></div>
            </div>

    </div>
</div>