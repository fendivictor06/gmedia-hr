<div class="page-content">
	<div class="breadcrumbs"><h1>Setting Reminder</h1></div>
		<div class="row">
			<div class="portlet light">
				<div class="portlet-body">
					<div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" id="add_new">
                                    <i class="fa fa-plus"></i> Tambah Data
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
				<div id="myTable"></div>
			</div>
		</div>
</div>
	
<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Setting Reminder</h4>
    </div>
    <form id="form_setting">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>User</label>
                            <?php echo form_dropdown('user', $user, '', 'id="user" class="select2"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    function clear_form() {
        document.getElementById('form_setting').reset();
        $("#id").val('');
        $(".bs-select").selectpicker("refresh");
    }

    $("#add_new").click(function(){
        clear_form();
        $("#myModal").modal();
    });

	function load_table() {
		$.ajax({
			url : "view_setting",
			success : function(data) {
				$("#myTable").html(data);
				$("#tb_setting").DataTable({
					responsive : true
				});
			}
		});
	}

	function get_id(id) {
		$.ajax({
			url : "id_setting/"+id,
			dataType : "json",
			success : function(data){
				$("#nip").val(data.nip).trigger("change");
                $("#user").val(data.user).trigger("change");
				$("#id").val(id);
				$("#myModal").modal();
			}
		})
	}

	$("#form_setting").submit(function(event){
            event.preventDefault();
            formData = new FormData($(this)[0]);
            $.ajax({
                url : "setting_process",
                type : "post",
                data : formData,
                async : false,
                cache : false,
                dataType : "json",
                contentType : false,
                processData : false,
                success : function(data) {
                    if(data.status == true) {
                        load_table(), $("myModal").modal("toggle");
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });

	$(document).ready(function(){
		load_table(), $(".select2").select2();
        $(".bs-select").selectpicker({
            iconBase: "fa",
            tickIcon: "fa-check"
        });
	});

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "<?php echo base_url('reminder/delete_reminder'); ?>/"+id,
                            dataType : "json",
                            success : function(data){
                                if(data.status == true) {
                                    load_table();
                                }
                                bootbox.alert(data.message);
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        })
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>