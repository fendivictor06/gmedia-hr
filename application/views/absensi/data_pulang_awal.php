<div class="page-content">
    <div class="breadcrumbs">
        <h1 class="page-header">Ijin Pulang Awal</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php  
                            $btn = '<button type="button" class="btn btn-primary pull-right"  data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>';
                            echo form_periode(2, $btn);
                        ?>
                    </div>
                </div>
            </div>
            <br>
            <div id="myTable"></div>
        </div>
    </div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <form id="form_pulang_awal" method="post" action="<?php echo base_url('pulang_awal/pulang_awal_process'); ?>" enctype="multipart/form-data">
    <div class="modal-header">
        <h4 class="modal-title">Form Pulang Awal</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label class="control-label">Nama <span class="required"> * </span></label>
                            <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Penyetuju <span class="required"> * </span></label>
                            <?php echo form_dropdown('penyetuju', $penyetuju, '', 'id="penyetuju" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tanggal <span class="required"> * </span></label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl" id="tgl" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jam Pulang <span class="required"> * </span></label>
                            <input type="text" class="form-control blank timepicker timepicker-24" name="jam_pulang" id="jam_pulang"  />
                    </div>
                    <div class="form-group">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <label class="control-label">File Upload </label>
                            <div class="input-group">
                                <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                    <span class="fileinput-filename"> </span>
                                </div>
                                <span class="input-group-addon btn default btn-file">
                                    <span class="fileinput-new"> Select file </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="userfile" id="userfile"> </span>
                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Alasan <span class="required"> * </span></label>
                            <textarea class="form-control blank" name="alasan" id="alasan" rows="5"></textarea>
                    </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    </form>
</div>
