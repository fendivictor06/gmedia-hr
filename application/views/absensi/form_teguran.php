<div class="page-content">
    <div class="breadcrumbs"><h1>Teguran</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php  
                                $btn = '<button type="button" class="btn btn-primary pull-right"  data-target="#myModal" data-toggle="modal" id="add_new"><i class="fa fa-plus"></i> Tambah Data</button>';
                                echo form_periode(null, $btn);
                            ?>
                        </div>
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <form id="search">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </form>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary pull-right" style="margin-top:25px;" data-target="#myModal" data-toggle="modal" id="add_new"><i class="fa fa-plus"></i> Tambah Data</button>
                        </div> -->
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Teguran</h4>
    </div>
    <form id="form_teguran">
        <div class="modal-body">
           <div class="form-group">
                <label>Nama</label>
                    <input type="hidden" name="id" id="id">
                    <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2"'); ?>
           </div>
           <div class="form-group">
                <label>Bulan</label>
                    <?php echo form_dropdown('bln', $bln, '', 'id="bln" class="select2"'); ?>
           </div>
           <div class="form-group">
                <label>Tahun</label>
                    <?php echo form_dropdown('thn', $thn, '', 'id="thn" class="select2"'); ?>
           </div>
           <div class="form-group">
               <label>Pencapaian (%)</label>
                    <input type="number" name="persen_pencapaian" id="persen_pencapaian" class="form-control">
           </div>
           <div class="form-group">
               <label>Pencapaian (Rp)</label>
                    <input type="number" name="rp_pencapaian" id="rp_pencapaian" class="form-control">
           </div>
           <div class="form-group">
               <label>Target</label>
                    <input type="number" name="target" id="target" class="form-control">
           </div>
           <div class="form-group">
                <label>Teguran</label>
                    <?php echo form_dropdown('teguran', $teguran, '', 'id="teguran" class="form-control"'); ?>
           </div>
           <div class="form-group">
               <label>Rekomendasi SP</label>
                <input type="number" name="rec_sp" id="rec_sp" class="form-control">
           </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".select2").select2(), load_table();
    });

    function clearform() {
        document.getElementById("form_teguran").reset();
        $("#id").val("");
        $(".select2").val("").trigger("change");
    }

    $("#add_new").click(function(){
        clearform();
    });

    function load_table() {
        // periode = $("#qd_id").val();
        var tahun = $("#tahun").val();
        var bulan = $("#bulan").val();
        $.ajax({
            url : "teguran/view_teguran",
            data : {
                "tahun" : tahun,
                "bulan" : bulan
            },
            beforeSend : function() {
                $("#tampil").addClass("hidden"), $("#imgload").removeClass("hidden");
            },
            complete : function() {
                $("#tampil").removeClass("hidden"), $("#imgload").addClass("hidden");
            },
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_teguran").DataTable({
                    responsive : true
                });
            }
        });
    }

    $("#tampil").click(function(){
        load_table();
    });

    $("#form_teguran").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url : "teguran/process_teguran",
            type : "post",
            dataType : "json",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success : function(data) {
                if(data.status == true) {
                    clearform(), load_table();
                }
                bootbox.alert(data.message);
            }
        });
        return false;
    });

    function get_id(id) {
        $.ajax({
            url : "teguran/id_teguran/"+id,
            dataType : "json",
            success : function(data) {
                $("#id").val(data.id);
                $("#nip").val(data.nip).trigger("change");
                $("#bln").val(data.bulan).trigger("change");
                $("#thn").val(data.tahun).trigger("change");
                $("#persen_pencapaian").val(data.persen_pencapaian);
                $("#rp_pencapaian").val(data.rp_pencapaian);
                $("#target").val(data.target);
                $("#teguran").val(data.teguran);
                $("#rec_sp").val(data.sp);
                $("#myModal").modal();
            }
        });
    }

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "teguran/delete_teguran/"+id,
                            type : "POST",
                            success : function(data){
                                bootbox.alert({
                                    message: "Delete Success",
                                    size: "small"
                                });
                                load_table();
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        })
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>