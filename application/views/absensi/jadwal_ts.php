<div class="page-content">
    <div class="breadcrumbs">
        <h1>Jadwal TS</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_periode(2); ?>
                        </div>
                    </div>
                </div>
            </div>
                <div id="myTable"></div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    <?php  
        $month = date('m');
        $month = (str_replace('0', '', $month));
        $year = date('Y');
    ?>

    var month = "<?php echo $month; ?>";
    var year = "<?php echo $year; ?>";
    $(document).ready(function(){
        $(".select2").select2();
        $("#bulan").val(month).trigger("change");
        $("#tahun").val(year).trigger("change");
        load_table();
    });

    function load_table() {
        var bulan = $("#bulan").val();
        var tahun = $("#tahun").val();
        $.ajax({
            url : "<?php echo base_url('shift/view_jadwal_ts'); ?>",
            data : {
                "tahun" : tahun,
                "bulan" : bulan
            },
            beforeSend : function() {
                $("#tampil").addClass("hidden");
                $("#unduh").addClass("hidden");
                $("#imgload").removeClass("hidden");
            },
            complete : function() {
                $("#tampil").removeClass("hidden");
                $("#unduh").removeClass("hidden");
                $("#imgload").addClass("hidden");
            },
            success : function(data) {
                $("#myTable").html(data);
            }
        });
    }

    $("#tampil").click(function(){
        load_table();
    });

    $("#unduh").click(function(){
        var bulan = $("#bulan").val();
        var tahun = $("#tahun").val();
        window.location.href="<?php echo base_url('download_file/view_jadwal_ts'); ?>?bulan="+bulan+"&tahun="+tahun;
    });
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>