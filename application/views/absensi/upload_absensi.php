<div class="page-content">
    <div class="breadcrumbs">
        <h1>Upload Absensi</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <?php echo $this->session->flashdata('message'); ?>
                    <form id="uploaddata" action="<?php echo base_url('upload/upload_absensi'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <label>File Import</label>
                                <div class="input-group">
                                    <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                        <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                        <span class="fileinput-filename"> </span>
                                    </div>
                                    <span class="input-group-addon btn default btn-file">
                                        <span class="fileinput-new"> Select file </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="userfile" id="userfile" required> </span>
                                    <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

