<div class="page-content">
	<div class="breadcrumbs">
		<h1>Cuti Karyawan</h1>
	</div>
	<div class="row">     
        <div class="portlet light">
            <div class="portlet-body">
                <?php 
                    $nip = $this->uri->segment(3);
                    $kary = $this->Main_Model->kary_nip($nip); 
                    $w = $this->db->query("SELECT MAX(th) th FROM cuti_dep")->row();
                    $th = isset($w->th)?$w->th:'';
                    $q = $this->db->query("select * from cuti_dep where nip ='$nip' and th ='$th'")->row();
                    $month = date("m");
                    $th_old = $th-1;
                    $w = $this->db->query("select * from cuti_dep where nip ='$nip' and th ='$th_old'")->row();
                    $tgl_hak_cuti = isset($q->tgl_hak_cuti)?$q->tgl_hak_cuti:'';
                ?>
               
                <table>
                    <tr>
                        <td>NIP</td>
                        <td width="20px"> : </td>
                        <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td> : </td>
                        <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td>
                    </tr>
                    <tr>
                        <td>Sisa Cuti</td>
                        <td> : </td>
                        <td><?php echo sisa_cuti($nip); ?></td>
                    </tr>
                    
                </table>
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>