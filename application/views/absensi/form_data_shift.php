<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data Shift</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="form-group"> 
                    <button type="button" class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Tambah Data</button> 
                </div>
                <div id="myTable"></div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Data Shift</h4>
    </div>
    <form id="form_shift">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>Shift</label>
                            <input type="text" name="shift" id="shift" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Jam Masuk</label>
                            <input type="text" name="jam_masuk" id="jam_masuk" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Jam Pulang</label>
                            <input type="text" name="jam_pulang" id="jam_pulang" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
    
    <script type="text/javascript">
        <?php 
            echo $this->Main_Model->default_loadtable(base_url('shift/view_data_shift'))
                //.$this->Main_Model->timepicker()
                .$this->Main_Model->default_delete_data(base_url('shift/delete_shift'))
                .$this->Main_Model->get_data(base_url('shift/id_shift'), 'get_id(id)', 
                    '$("#id").val(data.id_shift), $("#shift").val(data.nama), $("#jam_masuk").val(data.jam_masuk), $("#jam_pulang").val(data.jam_pulang), $("#keterangan").val(data.keterangan), $("#myModal").modal();') 
        ?>

        jQuery().timepicker && ($(".timepicker-default").timepicker({
            autoclose: !0,
            showSeconds: !0,
            minuteStep: 1
        }), $(".timepicker-no-seconds").timepicker({
            autoclose: !0,
            minuteStep: 5
        }), $(".timepicker-24").timepicker({
            autoclose: !0,
            minuteStep: 5,
            showSeconds: !0,
            showMeridian: !1
        }), $(".timepicker").parent(".input-group").on("click", ".input-group-btn", function(t) {
            t.preventDefault(), $(this).parent(".input-group").find(".timepicker").timepicker("showWidget")
        }), $(document).scroll(function() {
            $("#form_modal4 .timepicker-default, #form_modal4 .timepicker-no-seconds, #form_modal4 .timepicker-24").timepicker("place")
        }));

        function add_new() {
            document.getElementById('form_shift').reset();
        }

        $("#add").click(function(){
            add_new(), $("#myModal").modal();
        });

        $("#form_shift").submit(function(event){
            event.preventDefault();
            formData = new FormData($(this)[0]);
            $.ajax({
                url : "shift_process",
                type : "post",
                data : formData,
                async : false,
                cache : false,
                dataType : "json",
                contentType : false,
                processData : false,
                success : function(data) {
                    if(data.status == true) {
                        load_table(), add_new();
                        $("#myModal").modal('toggle');
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });

        function set_default(id) {
            $.ajax({
                url : "default_shift/"+id,
                dataType : "json",
                success : function(data) {
                    if(data.status == true) {
                        load_table();
                    } 
                    bootbox.alert(data.message);
                }
            })
        }
    </script>

<?php echo isset($penutup) ? $penutup : ''; ?>