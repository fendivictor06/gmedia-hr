<div class="page-content">
    <div class="breadcrumbs">
        <h1 class="page-header">Pencatatan Perjalanan Dinas</h1>
    </div>
    <div class="row">

            <div class="portlet light">
                <div class="portlet-body">
                    <div class="btn-group">
                        <form class="form-inline">
                            <div class="form-group">
                                <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                            </div>
                                <button type="button" onclick="load_table();" class="btn btn-primary" id="tampil">Tampilkan</button>
                                <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                        </form>
                    </div>
                    <button class="btn btn-primary" type="button" onclick="reset();" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data </button>
                </div>
                <br>
                <div id="myTable"></div>
            </div>

    </div>
</div>

<div id="myModal" class="modal fade container" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Perjalanan Dinas</h4>
    </div>
    <form id="form_perdin">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6">
                            <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label>Tanggal Berangkat</label>
                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl_berangkat" id="tgl_berangkat">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pulang</label>
                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl_pulang" id="tgl_pulang">
                    </div>
                    <div class="form-group">
                        <label>Tujuan</label>
                            <input type="text" class="form-control" name="tujuan" id="tujuan">
                    </div>
                    
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Keperluan</label>
                            <input type="text" class="form-control" name="keperluan" id="keperluan">
                    </div>
                    <div class="form-group">
                        <label>Undangan Mitra?</label>
                            <?php echo form_dropdown('ismitra',$option,'','id="ismitra" class="form-control"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <h4 class="modal-title">Detail Perdin</h4>
                        <a href="#" id="add"> Add + </a>
                        <a href="#" id="remove"> Remove - </a> <br><br>
                </div>
                
                    <div id="input">
                            <div class="col-md-4"><input type="hidden" name="id_det_perdin[]" value="baru"><div class="form-group"><label>Nama</label><select name="nip[]" id="nip[]" class="select2 nip" style="width:100%;"><?php foreach($nip as $row){echo '<option value="'.$row->nip.'">'.$row->nip.' - '.$row->nama.'</option>';} ?></select></div></div><div class="col-md-4"><div class="form-group"><label>Uang Transportasi</label><input type="text" name="trans[]" id="trans[]" class="form-control"></div></div><div class="col-md-4"><div class="form-group"><label>Uang Akomodasi</label><input type="text" name="akomo[]" id="akomo[]" class="form-control"></div></div>
                    </div>

                    <div class="inputs"></div>
                    
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>


<!--    </main>

    </div>
    <footer id="footer">Copyright © 2016 <a href="#" title="Perkasa App">Perkasa App</a></footer>
</body>
</html> -->

    <!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © Perkasa App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
    </div>
</diV>
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
    <![endif]-->

    <!-- Javascript Core -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

    <?php echo isset($js)?$js:''; ?>

            <script>
                var save_method;
                $(".date-picker").datepicker({
                    rtl: App.isRTL(),
                    orientation: "left",
                    autoclose: !0
                })
                $(document).ready(function() {
                    $(".select2").select2();
                });
                <?php echo $this->Main_Model->ajax(base_url('absensi/view_perdin').'/','load_table()','','',
                           '$("#myTable").html(data);
                            $("#dataTables-example").DataTable({
                                responsive: true
                            });','',
                        'beforeSend     : function(){
                            $("button").addClass("hidden");
                            $("#imgload").removeClass("hidden");
                        },',
                        'complete   : function(){
                            $("button").removeClass("hidden");
                            $("#imgload").addClass("hidden");
                        },','+$("#qd_id").val()'); ?>
                
                function reset()
                {
                    // var i = $(".field").size()+1;
                    $(".field").remove();
                    $(".field-edit").remove();
                    $("#tgl_berangkat").val("");
                    $("#tgl_pulang").val("");
                    $("#tujuan").val("");
                    $("#keperluan").val("");
                    $("#ismitra").val("");
                    $(".form-control").val("");
                    $(".select2").val("").trigger("change");
                    save_method = "save";
                    $("#warning").addClass("hidden");
                    $("#success").addClass("hidden");
                    // while(i > 2) {
                    //     $('.field:last').remove();
                    //     i--;
                    // }
                }
                function get_id(id)
                {
                    save_method = "update";
                    $("#warning").addClass("hidden");
                    $("#success").addClass("hidden");
                    id = {"id" : id}
                    $.ajax({
                        url         : "<?php echo base_url('absensi/perdin_id'); ?>",
                        type        : "GET",
                        data        : id,
                        dataType    : "JSON",
                        success     : function(data){
                            $(".field").remove();
                            $(".field-edit").remove();
                            $("#myModal").modal();
                            $("#tgl_berangkat").val(data[0].tgl_a);
                            $("#tgl_pulang").val(data[0].tgl_b);
                            $("#tujuan").val(data[0].tujuan);
                            $("#keperluan").val(data[0].keperluan);
                            $("#id").val(data[0].id_perdin);
                            $("#ismitra").val(data[0].ismitra);
                            $("#input").html("");
                            // console.log(data);
                            var i = data.length;
                            // console.log(i);

                            for(q=0;q<i;q++){
                                $('<div class="field-edit" id="field-edit-'+data[q].id_det_perdin+'"><div class="col-md-4"><input type="hidden" name="id_det_perdin[]" value="'+data[q].id_det_perdin+'"><div class="form-group"><label>Nama</label><select name="nip[]" id="nip'+q+'" class="select2 nip" style="width:100%;"><?php foreach($nip as $row){echo '<option value="'.$row->nip.'">'.$row->nip.' - '.str_replace("'", "", $row->nama).'</option>';} ?></select></div></div><div class="col-md-4"><div class="form-group"><label>Uang Transportasi</label><input type="text" name="trans[]" id="trans[]" class="form-control" value="'+data[q].transportasi+'"></div></div><div class="col-md-4"><div class="form-group"><label>Uang Akomodasi</label><div class="input-icon right"><i class="fa fa-times" data-original-title="Delete" style="cursor:pointer;" data-container="body" onclick="del_detail('+data[q].id_det_perdin+');"></i><input type="text" name="akomo[]" id="akomo[]" class="form-control" value="'+data[q].akomodasi+'"></div></div></div></div>').fadeIn('slow').appendTo('.inputs');

                                $("#nip"+q).val(data[q].nip).trigger("change");
                                $(document).ready(function() {
                                    $(".select2").select2();
                                });
                                
                            }
                        },
                        error       : function(jqXHR, textStatus, errorThrown){
                            alert("Internal Server Error");
                        }
                    });
                }
                function save()
                {
                    if(save_method=="save")
                    {
                        var url = "<?php echo base_url('absensi/add_perdin'); ?>";
                    }
                    else
                    {
                        var url = "<?php echo base_url('absensi/update_perdin'); ?>";
                    }
                        $.ajax({
                            url         : url,
                            type        : "POST",
                            data        : $("#form_perdin").serialize(),
                            success     : function(data){
                                
                                load_table();
                                console.log(data);
                                if(data=="false")
                                {
                                    $("#warning").removeClass("hidden");
                                    $("#success").addClass("hidden");
                                    $("#warning").html("Please Complete this form!");
                                }
                                else if(data=="true")
                                {
                                    $("#warning").addClass("hidden");
                                    $("#success").removeClass("hidden");
                                    $(".field").remove();
                                    $(".field-edit").remove();
                                    $(".form-control").val("");
                                    $(".select2").val("").trigger("change");
                                }
                                else if(data=="invalid")
                                {
                                    $("#warning").removeClass("hidden");
                                    $("#success").addClass("hidden");
                                    $("#warning").html("Invalid date format!");
                                }
                                
                            },
                            error       : function(jqXHR,textStatus,errorThrown){
                                bootbox.alert("Internal Server Error");
                            }
                        });     
                }
                function delete_data(id)
                {
                    id = {"id":id}

                    bootbox.dialog({
                        message : "Yakin ingin menghapus data?",
                        title : "Hapus Data",
                        buttons :{
                            danger : {
                                label : "Delete",
                                className : "red",
                                callback : function(){
                                    $.ajax({
                                        url : "<?php echo base_url('absensi/delete_perdin'); ?>",
                                        type : "POST",
                                        data : id,
                                        success : function(data){
                                            bootbox.alert({
                                                message: "Delete Success",
                                                size: "small"
                                            });
                                            load_table();
                                        },
                                        error : function(jqXHR, textStatus, errorThrown){
                                            alert("Internal Server Error");
                                        }
                                    });
                                }
                            },
                            main : {
                                label : "Cancel",
                                className : "blue",
                                callback : function(){
                                    return true;
                                }
                            }
                        }
                    })
                }
                function del_detail(id)
                {
                    $.ajax({
                        url : "<?php echo base_url('absensi/delete_det_perdin'); ?>/"+id,
                        type : "GET",
                        success : function(data){
                            $('#field-edit-'+id).remove();
                            load_table();
                        }
                    });
                }
                $(document).ready(function(){
                    
                    $("#add").click(function(){
                        var i = $(".field").size()+1;
                        $('<div class="field"><div class="col-md-4"><input type="hidden" name="id_det_perdin[]" value="baru"><div class="form-group"><label>Nama</label><select name="nip[]" id="nip[]" class="select2 nip" style="width:100%;"><?php foreach($nip as $row){echo '<option value="'.$row->nip.'">'.$row->nip.' - '.str_replace("'", "", $row->nama).'</option>';} ?></select></div></div><div class="col-md-4"><div class="form-group"><label>Uang Transportasi</label><input type="text" name="trans[]" id="trans[]" class="form-control"></div></div><div class="col-md-4"><div class="form-group"><label>Uang Akomodasi</label><input type="text" name="akomo[]" id="akomo[]" class="form-control"></div></div></div>').fadeIn('slow').appendTo('.inputs');
                        $(document).ready(function() {
                            $(".select2").select2();
                        });
                        i++;

                    });

                    $('#remove').click(function() {
                        var i = $(".field").size()+1;
                        if(i > 1) {
                            $('.field:last').remove();
                            i--; 
                        }
                    });     
                });

                load_table();
            </script>

    <div id="myProfile" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
        <div class="modal-header">
            <h4 class="modal-title"><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->nama; ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table>
                        <tr>
                            <td width="80px" height="50px">NIP</td>
                            <td><?php echo $this->session->userdata('nip'); ?></td>
                        </tr>
                        <tr>
                            <td width="80px" height="50px">Alamat</td>
                            <td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->alamat; ?></td>
                        </tr>
                        <tr>
                            <td width="80px" height="50px">Telepon</td>
                            <td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->telp; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        </div>
    </div>


    <script type="text/javascript">
        function profile(){
            $("#myProfile").modal();
        }
        function ChangePT(id){
            $.ajax({
                url : "<?php echo base_url('main/changept'); ?>/"+id,
                type : "POST",
                success : function(data){
                    location.reload();
                },
                error : function(jqXHR, textStatus, errorThrown){
                    alert("Oops Something went wrong !");
                } 
            })
        }
    </script>

</body>
</html>