<div class="page-content">
    <div class="breadcrumbs">
        <h1>Pencatatan Terlambat</h1>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php  
                echo form_periode();
            ?>
            <br>
            <div id="myTable"></div>
        </div>
        <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <label>Periode</label>
                        <div class="form-group input-group">
                            <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                            <span class="input-group-btn">
                                <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                            </span> 
                        </div>
                </div>
            </div>
            <br><br>
            <div id="myTable"></div>
        </div> -->
    </div>          
</div>




<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Keterlambatan</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="id" id="id">
                <div class="form-group">
                    <label>NIP</label>
                    <?php echo form_dropdown('nip',$nip,'','id="nip" class="select2"'); ?>
                </div>
                <div class="form-group">
                    <label>Periode</label>
                    <?php echo form_dropdown('periode',$periode,'','id="periode" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>Total Menit</label>
                    <input type="number" class="form-control" name="total_menit" id="total_menit">
                </div>
                <div class="form_group">
                    <label>Total Hari</label>
                    <input type="number" class="form-control" name="total_hari" id="total_hari">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

<div id="modaldetail" class="modal fade container" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title" id="detailtitle"></h4>
    </div>
    <div class="modal-body">
        <div id="detailresult"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

    <!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © PSP App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
    </div>
</diV>
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
    <![endif]-->

    <!-- Javascript Core -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

    <?php echo isset($js)?$js:''; ?>

    <?php echo isset($javascript)?$javascript:''; ?>

    <?php echo $this->session->flashdata('message'); ?>


    <script type="text/javascript">
        function profile(){$("#myProfile").modal(); } function ChangePT(id){$.ajax({url : "<?php echo base_url('main/changept'); ?>/"+id, type : "POST", success : function(data){location.reload(); }, error : function(jqXHR, textStatus, errorThrown){alert("Oops Something went wrong !"); } }); }
    </script>

</body>
</html>