<div class="page-content">
    <div class="breadcrumbs">
        <h1>Rekap Absensi Periode</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_periode(2); ?>
                        </div>
                    </div>
                </div>
            </div>
                <div id="myTable"></div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.select2').select2();   
        load_table();     
    });

    function load_table() {
        var tahun = $('#tahun').val();
        var bulan = $('#bulan').val();
        $.ajax({
            url : "<?php echo base_url('absensi/view_rekap_periode'); ?>?tahun="+tahun+"&bulan="+bulan,
            beforeSend : function() {
                $('#tampil').addClass('hidden');
                $('#unduh').addClass('hidden');
                $('#imgload').removeClass('hidden');
            },
            complete : function() {
                $('#tampil').removeClass('hidden');
                $('#unduh').removeClass('hidden');
                $('#imgload').addClass('hidden');
            },
            success : function(data) {
                $('#myTable').html(data);
                $('#tb_rekap').DataTable({
                    responsive : true
                });
            },
            error : function() {
                bootbox.alert('Gagal mengambil data!');
            }
        });
    }

    $('#unduh').click(function(){
        var tahun = $('#tahun').val();
        var bulan = $('#bulan').val();
        window.location.href="<?php echo base_url('download_excel/rekap_periode'); ?>?tahun="+tahun+"&bulan="+bulan;
    });

    $('#tampil').click(function(){
        load_table();
    });
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>