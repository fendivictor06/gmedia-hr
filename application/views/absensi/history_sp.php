<div class="page-content">
	<div class="breadcrumbs">
		<h1>History SP</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="form-group">
					<label>Nama </label>
						<div class="form-group input-group">
							<?php echo form_dropdown('nip', $karyawan, '', 'id="nip" class="select2" style="width:100%"'); ?>
							<span class="input-group-btn">
	        					<button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
	        					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
	        				</span>	
						</div>
				</div>
			</div>
			<div id="myTable"></div>
		</div>
	</div>
</div>