<div class="page-content">
    <div class="breadcrumbs"><h1>Tarik Scanlog Manual</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <form id="search">  
                            <div class="form-group">
                                <label>Mesin</label>
                                <?php echo form_dropdown('mesin', $mesin, '', 'id="mesin" class="form-control"'); ?>
                            </div>
                            <div class="form-group">
                                <label>Mode</label>
                                <select name="mode" id="mode" class="form-control">
                                    <option value="new">New</option>
                                    <option value="all">All</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="button" id="scan">Scan</button>
                                <img src="../assets/img/loading.gif" class="hidden" id="imgload">
                            </div>
                    </form>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $("#scan").click(function(){
        $.ajax({
            url : "<?php echo base_url('api/json_mesin'); ?>",
            type : "post",
            dataType : "json",
            data : $("#search").serialize(),
            success : function(data) {
               link = data.url;
               $.ajax({
                    url : link,
                    dataType : "json",
                    beforeSend : function() {
                        $("#scan").addClass("hidden"), $("#imgload").removeClass("hidden");
                    },
                    complete : function() {
                        $("#scan").removeClass("hidden"), $("#imgload").addClass("hidden");
                    },
                    success : function(data){
                        bootbox.alert(data.message);
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                        bootbox.alert("Terjadi kesalahan pada SDK !");
                    }
               });
            }
        });
    });
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>