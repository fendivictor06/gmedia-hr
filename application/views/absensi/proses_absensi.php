<div class="page-content">
    <div class="breadcrumbs"><h1>Proses Absensi</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                            <input type="text" name="tgl_mulai" id="tgl_mulai" class="form-control date-picker" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tanggal Akhir</label>
                            <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nama</label>
                            <?php  
                                echo form_dropdown('nip', $nip, '', 'id="nip" class="select2" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <button type="button" id="proses" class="btn btn-primary"><i class="fa fa-refresh"></i> Proses</button>
                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".date-picker").datepicker({
            "setDate": new Date(),
            "autoclose": true
        });
        $(".select2").select2();
    });

    $("#proses").click(function(){
        load_table();
    })

    function load_table() {
        tgl_awal = $("#tgl_mulai").val();
        tgl_akhir = $("#tgl_akhir").val();
        nip = $("#nip").val();

        var query = tgl_awal+"/"+tgl_akhir;
        if (nip != '') {
            query += "/"+nip;
        }

        $.ajax({
            url : "<?php echo base_url('api/absensiproses'); ?>/"+query,
            dataType : "json",
            beforeSend : function() {
                $("#proses").addClass("hidden"), $("#imgload").removeClass("hidden");
            },
            complete : function() {
                $("#proses").removeClass("hidden"), $("#imgload").addClass("hidden");
            },
            success : function(data) {
                bootbox.alert(data.message);
            },
            error : function() {
                bootbox.alert("Gagal memproses absensi");
            }
        });
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>