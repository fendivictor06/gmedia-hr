<div class="page-content">
    <div class="breadcrumbs"><h1>Log Mesin</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-3">
                            <form id="search">
                                <label>Date</label>
                                    <div class="form-group input-group">
                                        <input type="text" name="tgl" id="tgl" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </form>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".date-picker").datepicker({
            "setDate": new Date(),
            "autoclose": true
        });
        load_table();
    });

    function load_table() {
        date = $("#tgl").val();
        $.ajax({
            url : "<?php echo base_url('scanlog/view_log_mesin?date='); ?>"+date,
            beforeSend : function() {
                $("#tampil").addClass("hidden"), $("#imgload").removeClass("hidden");
            },
            complete : function() {
                $("#tampil").removeClass("hidden"), $("#imgload").addClass("hidden");
            },
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_scanlog").DataTable({
                    responsive : true
                });
            }
        });
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>