<div class="page-content">
    <div class="breadcrumbs"><h1>Penjadwalan</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <form id="search">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Mulai</label>
                                        <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Selesai</label>
                                        <input type="text" name="tgl_selesai" id="tgl_selesai" class="form-control date-picker" data-date-format="dd/mm/yyyy" autocomplete="off">
                                </div> 
                            </div> 
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>All / By 1</label>
                                        <select class="form-control" name="copy_to" id="copy_to">
                                            <option value="1">All</option>
                                            <option value="2">By 1</option>
                                        </select>
                                </div>
                                <div class="form-group hidden" id="target">
                                    <label>Nama</label>
                                        <?php echo form_dropdown('nip_copy', $nip, '', 'id="nip_copy" class="select2" multiple="multiple"'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button id="tampil" class="btn btn-primary" type="button"><i class="fa fa-search"></i> Cari</button>
                                <button type="button" class="btn btn-default" id="add"><i class="fa fa-plus"></i> Tambah Data</button>
                                <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                            </div>
                        </form> 
                    </div>
                </div>  
                <div id="myTable"></div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Penjadwalan</h4>
    </div>
    <form id="form_jadwal">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Shift</label>
                            <?php echo form_dropdown('shift', $shift, '', 'id="shift" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" name="tanggal" id="tanggal" data-date-format="dd/mm/yyyy" class="form-control date-picker" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea name="keterangan" id="keterangan" class="form-control" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
            <img id="loading" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
        </div>
    </form>
</div>

<div id="tukarModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Tukar Penjadwalan</h4>
    </div>
    <form id="form_jadwal_tukar">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id_tukar" id="id_tukar" >
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip_tukar', $nip, '', 'id="nip_tukar" class="select2"'); ?>
                    </div>
                    <!-- <div class="form-group">
                        <label>Shift</label>
                            <?php //echo form_dropdown('shift_tukar', $shift, '', 'id="shift_tukar" class="select2"'); ?>
                    </div> -->
                    <div class="form-group">
                        <label>Tanggal Awal</label>
                            <input type="text" name="tanggal_tukar_awal" id="tanggal_tukar_awal" data-date-format="dd/mm/yyyy" class="form-control" readonly="readonly">
                    </div>
                    <!-- <div class="form-group">
                        <label>Tanggal Akhir</label>
                            <input type="text" name="tanggal_tukar_akhir" id="tanggal_tukar_akhir" data-date-format="dd/mm/yyyy" class="form-control date-picker" autocomplete="off">
                    </div> -->

                    <div class="form-group">
                        <label><strong>Tukar Dengan</strong></label>
                            <?php echo form_dropdown('nip_ditukar', $nip, '', 'id="nip_ditukar" class="select2"'); ?>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Awal</label>
                            <input type="text" name="tanggal_ditukar_awal" id="tanggal_ditukar_awal" data-date-format="dd/mm/yyyy" class="form-control date-picker" autocomplete="off">
                    </div>

                    <!-- <div class="form-group">
                        <label>Tanggal Akhir</label>
                            <input type="text" name="tanggal_ditukar_akhir" id="tanggal_ditukar_akhir" data-date-format="dd/mm/yyyy" class="form-control date-picker" autocomplete="off">
                    </div> -->

                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea name="keterangan_tukar" id="keterangan_tukar" class="form-control" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close_tukar" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save_tukar" class="btn btn-primary">Simpan</button>
            <img id="loading_tukar" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
    
    <script type="text/javascript">
        function salin(id){
            $("#id_copy").val(id);
            $("#copyModal").modal();
        }

        // $("#form_copy").submit(function(event){
        //     event.preventDefault();
        //     formData = new FormData($(this)[0]);
        //     $.ajax({
        //         url : "copy_jadwal",
        //         type : "post",
        //         data : formData,
        //         // async : false,
        //         cache : false,
        //         dataType : "json",
        //         contentType : false,
        //         processData : false,
        //         beforeSend : function() {
        //             $("#close_copy").addClass("hidden"), $("#save_copy").addClass("hidden"), $("#loading_copy").removeClass("hidden");
        //         },
        //         complete : function() {
        //             $("#close_copy").removeClass("hidden"), $("#save_copy").removeClass("hidden"), $("#loading_copy").addClass("hidden");
        //         },
        //         success : function(data) {
        //             if(data.status == true) {
        //                 load_table(), add_new(), $("#keterangan_copy").val("");
        //             }
        //             bootbox.alert(data.message);
        //         }
        //     });
        //     return false;
        // });

        $(document).ready(function(){
            $(".select2").select2(), 
            $(".date-picker").datepicker({
                orientation: "left", 
                autoclose: !0
            });

            $(".select2-container").css('width', 'auto');
        });

        $("#copy_to").change(function(){
            ($(this).val() == 2) ? $("#target").removeClass("hidden") : $("#target").addClass("hidden");
        });

        function add_new() {
            document.getElementById('form_jadwal').reset();
            $("#nip").val("").trigger("change");
            $("#shift").val("").trigger("change");
        }

        $("#add").click(function(){
            add_new(), $("#myModal").modal();
        });

        $("#tampil").click(function(){
            load_table();
        });

        function load_table() {
            tgl_awal = $("#tgl_awal").val();
            tgl_selesai = $("#tgl_selesai").val();
            copy_to = $("#copy_to").val();
            nip_copy = $("#nip_copy").val();

            $.ajax({
                url : "view_penjadwalan",
                type : "post",
                data : {
                    "tgl_awal" : tgl_awal,
                    "tgl_selesai" : tgl_selesai,
                    "copy_to" : copy_to,
                    "nip_copy" : nip_copy
                },
                beforeSend : function() {
                    $("#imgload").removeClass("hidden"), $("#tampil").addClass("hidden"), $("#add").addClass("hidden")
                },
                complete : function() {
                    $("#imgload").addClass("hidden"), $("#tampil").removeClass("hidden"), $("#add").removeClass("hidden");
                },
                success : function(data) {
                    $("#myTable").html(data);
                    $("#dataTables-example").DataTable({
                        responsive : true
                    });
                }
            })
        }

        $("#form_jadwal").submit(function(event){
            event.preventDefault();
            formData = new FormData($(this)[0]);
            $.ajax({
                url : "jadwal_process",
                type : "post",
                data : formData,
                // async : false,
                cache : false,
                dataType : "json",
                contentType : false,
                processData : false,
                // beforeSend : function() {
                //     $("#close").addClass("hidden"), $("#save").addClass("hidden"), $("#loading").removeClass("hidden");
                // },
                // complete : function() {
                //     $("#close").removeClass("hidden"), $("#save").removeClass("hidden"), $("#loading").addClass("hidden");
                // },
                success : function(data) {
                    if(data.status == true) {
                        load_table(), add_new();
                        $("#myModal").modal('toggle');
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });

        function get_id(id) {
            $.ajax({
                url : "id_jadwal/"+id,
                dataType : "json",
                success : function(data) {
                    $("#id").val(data.id), $("#nip").val(data.nip).trigger("change"), $("#shift").val(data.id_shift).trigger("change"), $("#tanggal").val(data.tanggal), $("#keterangan").val(data.keterangan);
                    $("#myModal").modal();
                }
            });
        }

        function delete_data(id) {
            bootbox.dialog({
                message : "Yakin ingin menghapus data?",
                title : "Hapus Data",
                buttons :{
                    danger : {
                        label : "Delete",
                        className : "red",
                        callback : function(){
                            $.ajax({
                                url : "delete_jadwal/"+id,
                                dataType : "json",
                                success : function(data){
                                    if(data.status == true) {
                                        load_table();
                                    }
                                    bootbox.alert(data.message);
                                }
                            });
                        }
                    },
                    main : {
                        label : "Cancel",
                        className : "blue",
                        callback : function(){
                            return true;
                        }
                    }
                }
            })
        }

        function tukar(id) {
            $.ajax({
                url : "id_jadwal/"+id,
                dataType : "json",
                success : function(data) {
                    $("#id_tukar").val(data.id);
                    $("#nip_tukar").val(data.nip).trigger("change");
                    $("#nip_tukar").attr('disabled', true);
                    $("#shift_tukar").val(data.id_shift).trigger("change");
                    $("#shift_tukar").attr('disabled', true);
                    $("#tanggal_tukar_awal").val(data.tanggal);
                    $("#tanggal_ditukar_awal").val(data.tanggal);
                    $("#nip_ditukar").val('').trigger('change');
                    $("#tanggal_tukar_akhir").val('');
                    $("#tanggal_ditukar_akhir").val('');
                    $("#tukarModal").modal();
                }
            });
        }

        $("#form_jadwal_tukar").submit(function(event){
            event.preventDefault();
            formData = new FormData($(this)[0]);
            $.ajax({
                url : "tukar_jadwal",
                type : "post",
                data : formData,
                cache : false,
                dataType : "json",
                contentType : false,
                processData : false,
                beforeSend : function() {
                    $("#loading_tukar").removeClass("hidden"), 
                    $("#save_tukar").addClass("hidden"), 
                    $("#close_tukar").addClass("hidden");
                },
                complete : function() {
                    $("#loading_tukar").addClass("hidden"), 
                    $("#save_tukar").removeClass("hidden"), 
                    $("#close_tukar").removeClass("hidden");
                },
                success : function(data) {
                    if(data.status == true) {
                        load_table();
                        $("#tukarModal").modal('toggle');
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });
    </script>

<?php echo isset($penutup) ? $penutup : ''; ?>
