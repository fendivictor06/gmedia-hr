<div class="page-content">
	<div class="breadcrumbs">
		<h1>Data BBM & BBP</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Data
                                </button>
                            </div>
						</div>
					</div>
				</div>
			</div>
			<div id="myTable"></div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form BBM&BBP</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form id="bbmp">
					<input type="hidden" name="id" id="id" class="kosong">
					<div class="form-group">
						<label>Nama*</label>
							<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2"'); ?>
					</div>
					<div class="form-group">
						<label>Tanggal Mulai*</label>
							<input type="text" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" name="bbmstart" id="bbmstart">
					</div>
					<div class="form-group">
						<label>Lokasi*</label>
							<?php echo form_dropdown('lokasi',$lokasi,'','id="lokasi" class="select2"'); ?>
					</div>
					<div class="form-group">
						<label>Menerima BBP ?*</label>
							<?php echo form_dropdown('isbbp',$option,'','id="isbbp" class="form-control kosong" onchange="is_bbp()"'); ?>
					</div>
					<div class="form-group hidden" id="bbp">
						<label>Bersama Keluarga ?*</label>
							<?php echo form_dropdown('family',$option,'','id="family" class="form-control kosong"'); ?>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
	</div>
</div>

