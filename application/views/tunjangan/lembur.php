<div class="page-content">
    <div class="breadcrumbs">
        <h1>Tunjangan Lembur</h1>
    </div>

    <div class="row">
    	<div class="portlet light">
    		<div class="portlet-body">
    			<div class="table-toolbar">
    				<div class="row">
    					<div class="col-md-12">
    						<form class="form-inline" role="form" id="form_periode">
    							<?php  
    								$cabang = isset($cabang) ? $cabang : [];
    								if (! empty($cabang)) {
    									echo '<div class="form-group col-md-2" style="padding-left:0px;">';
    									echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="select2" style="width:100%"');
    									echo '</div>';
    								}
    							?>
					            <div class="form-group col-md-2" style="padding-left:0px;">
					                <?php echo form_dropdown('bulan', $bulan, '', 'id="bulan" class="form-control select2" style="width:100%"'); ?>
					            </div>
					            <div class="form-group col-md-2" style="padding-left:0px;">
					                <?php echo form_dropdown('tahun', $tahun, '', 'id="tahun" class="form-control select2" style="width:100%"'); ?>
					           	</div>
					            <button type="button" id="tampil" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
					            <button type="button" id="unduh" class="btn btn-default"><i class="fa fa-download"></i> Download</button>
					            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
					            <button type="button" class="btn btn-primary pull-right" onclick="clearform();"> <i class="fa fa-plus"></i> Tambah Data</button>
				            </form>
						</div>
    				</div>
    			</div>
    		</div>
    		<div id="myTable"></div>
    	</div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<form id="flembur" action="#" enctype="multipart/form-data">
		<div class="modal-header">
			<h4 class="modal-title">Form Tunjangan Lembur</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<input type="hidden" name="id" id="id" class="kosong">
					<div class="form-group">
						<label>NIK</label>
							<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2 kosong"'); ?>
					</div>
					<!-- <div class="form-group">
						<label>Hari Libur?</label>
							<?php echo form_dropdown('islibur',$option,'','id="islibur" class="form-control kosong"'); ?>
					</div> -->
					<div class="form-group">
						<label>Jenis Overtime</label>
							<?php echo form_dropdown('overtime', $overtime, '', 'id="overtime" class="select2"'); ?>
					</div>
					<div class="form-group">
						<label>Tanggal</label>
							<input type="text" name="tgl" id="tgl" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" autocomplete="off">
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<label>Waktu Mulai</label>
					</div>
					<div class="form-group">
						<div class="col-md-6" style="padding-left: 0px;">
							<input type="text" name="starttime_hour" class="kosong form-control" id="starttime_hour" maxlength="2">
						</div>
						<div class="col-md-6" style="padding-right: 0px;">
							<input type="text" name="starttime_minute" id="starttime_minute" class="kosong form-control" maxlength="2">
						</div>
					</div>
					<div class="form-group" style="margin-bottom: 0px;">
						<label>Waktu Selesai</label>
							<!-- <input type="text" name="timeend" id="timeend" class="kosong form-control timepicker timepicker-24"> -->
					</div>
					<div class="form-group">
						<div class="col-md-6" style="padding-left: 0px;">
							<input type="text" name="endtime_hour" class="kosong form-control" id="endtime_hour" maxlength="2">
						</div>
						<div class="col-md-6" style="padding-right: 0px;">
							<input type="text" name="endtime_minute" id="endtime_minute" class="kosong form-control" maxlength="2">
						</div>
					</div>
					<!-- <div class="form-group">
						<label>File Upload</label>
							<input type="file" name="upload" id="upload" class="form-control">
					</div> -->
					<br> <br>
					<div class="form-group">
						<label>Keterangan</label>
							<textarea class="form-control kosong" name="keterangan" id="keterangan" rows="5"></textarea>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
	        <button type="submit" class="btn btn-primary">Simpan</button>
		</div>
	</form>
</div>


<div id="downloadModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<form id="fdownload" action="#" enctype="multipart/form-data">
		<div class="modal-header">
			<h4 class="modal-title">Download Laporan Lembur</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label>NIK</label>
							<?php echo form_dropdown('nik', $nip, '', 'id="nik" class="select2"'); ?>
					</div>
					<div class="form-group">
						<label>Tanggal Mulai</label>
							<input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy" autocomplete="off">
					</div>
					<div class="form-group">
						<label>Tanggal Selesai</label>
							<input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy" autocomplete="off">
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
	        <button type="submit" id="download_laporan" class="btn btn-primary">Download</button>
		</div>
	</form>
</div>