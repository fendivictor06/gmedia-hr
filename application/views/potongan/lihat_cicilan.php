<div class="page-content">
	<div class="breadcrumbs">
		<h1>Cicilan</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<table>
                    <tr>
                        <td>NIP</td>
                        <td width="20px"> : </td>
                        <td><?php echo $kary->nip; ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td> : </td>
                        <td><?php echo $kary->nama; ?></td>
                    </tr>
                    <tr>
                        <td>Keterangan Piutang</td>
                        <td> : </td>
                        <td><?php echo $pot->ket; ?></td>
                    </tr>
                </table>
				<div class="table-toolbar">
					
				</div>
			</div>
			<div id="myTable"></div>
		</div>
	</div>
</div>