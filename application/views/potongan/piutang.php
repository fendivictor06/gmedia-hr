<div class="page-content">
	<div class="breadcrumbs">
		<h1>Hutang Karyawan</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Data
                                </button>
                            </div>
						</div>
					</div>
				</div>
			</div>
			<div id="myTable"></div>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form Hutang</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form id="fpiutang">
					<input type="hidden" name="id" id="id" class="kosong">
					<div class="form-group">
						<label>Nama Karyawan</label>
							<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2 kosong"'); ?>
					</div>
					<div class="form-group">
						<label>Tanggal</label>
							<input type="text" name="tgl_hutang" id="tgl_hutang" class="form-control kosong date-picker" data-date-format="dd/mm/yyyy">
					</div>
					<div class="form-group">
						<label>Jumlah Cicilan (x)</label>
							<input type="number" class="form-control kosong" name="jml_cicilan" id="jml_cicilan">
					</div>
					<div class="form-group">
						<label>Total Hutang (Rp)</label>
							<input type="number" class="form-control kosong" name="tot_pot" id="tot_pot">
					</div>
					<div class="form-group">
						<label>Keterangan</label>
							<textarea class="form-control kosong" name="ket" id="ket" rows="5"></textarea>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
	</div>
</div>

<div id="cicil" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form Cicilan</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form id="fcicilan">
					<input type="hidden" name="id_pot" id="id_pot">
					<div class="form-group">
						<label>Angsuran (Rp)</label>
							<input type="number" class="form-control kosong" name="ags" id="ags">
					</div>
					<div class="form-group">
						<label>Tanggal</label>
							<input type="text" name="tgl" id="tgl" class="form-control kosong date-picker" data-date-format="dd/mm/yyyy">
					</div>
					<div class="form-group">
						<label>Include Payroll?</label>
							<?php echo form_dropdown('inc',$option,'','id="inc" class="form-control kosong"'); ?>
					</div>
					<div class="form-group">
						<label>Keterangan</label>
							<textarea class="form-control kosong" name="keterangan" id="keterangan" rows="5"></textarea>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save_cicil();" class="btn btn-primary">Simpan</button>
	</div>
</div>
