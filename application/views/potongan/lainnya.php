<div class="page-content">
    <div class="breadcrumbs">
        <h1>Potongan Lainnya</h1>
    </div>
    <div class="row">
    	<div class="portlet light">
    		<div class="portlet-body">
    			<div class="table-toolbar">
    				<div class="row">
    					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<label>Periode</label>
								<div class="form-group input-group">
									<?php echo form_dropdown('periode',$periode,'','id="periode" class="form-control"'); ?>
									<span class="input-group-btn">
	                					<button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
	                					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
	                				</span>	
								</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<button type="button" class="btn btn-primary pull-right" style="margin-top:25px;" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>
						</div>
    				</div>
    			</div>
    		</div>
    		<div id="myTable"></div>
    	</div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form Potongan Lainnya</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<form id="flainnya">
					<input type="hidden" name="id" id="id" class="kosong">
					<div class="form-group">
						<label>Periode</label>
							<?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="select2 kosong"') ?>
					</div>
					<div class="form-group">
						<label>Nama</label>
							<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2 kosong"'); ?>
					</div>
					<div class="form-group">
						<label>Jumlah Potongan</label>
							<input type="text" class="form-control kosong" id="jumlah" name="jumlah">
					</div>
					<div class="form-group">
						<label>Keterangan</label>
							<textarea id="keterangan" name="keterangan" class="form-control kosong"></textarea>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
	</div>
</div>

