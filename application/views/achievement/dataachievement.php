<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data Achivement</h1>
    </div>
    <div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="portlet light bordered">

					<div class="btn-group">
						<form class="form-inline">
							<div class="form-group">
								<?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
							</div>
								<button type="button" onclick="load_table();" class="btn btn-primary" id="tampil">Tampilkan</button>

								<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
						</form>
					</div>
					<!-- <button class="btn btn-primary pull-right" type="button" id="tambah" onclick="reset()">Tambah Achievement</button> -->
					<br><br>
					<div id="myTable"></div>
			</div>
		</div>
	</div>

</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form Hari Libur</h4>
	</div>
	<div class="modal-body">
		<div class="alert alert-danger hidden" id="warning">
			Please Complete this form!
		</div>
		<div class="alert alert-success hidden" id="success">
			Success!
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="form-group">
					<label>NIP</label>
						<input type="hidden" name="nip" id="nip">
						<?php echo form_dropdown('nama',$kary_list,'','id="nama" class="form-control" disabled'); ?>
				</div>
				<div class="form-group">
					<label>Periode</label>
						<input type="hidden" name="id_per" id="id_per">
						<?php echo form_dropdown('per',$periode,'','id="per" class="form-control" disabled'); ?>
				</div>
				<div class="form-group">
					<label>Achievement</label>
						<input type="text" name="ach" id="ach" class="form-control">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
	</div>
</div>



<!-- 	</main>

    </div>
	<footer id="footer">Copyright © 2016 <a href="#" title="Perkasa App">Perkasa App</a></footer>
</body>
</html> -->

	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © Perkasa App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</diV>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

	<?php echo isset($js)?$js:''; ?>


	<script type="text/javascript">
		function load_table(){
			var dat = {
				"periode" : $("#qd_id").val()
			}
			$.ajax({
				url : "<?php echo base_url('achievement/view_achievement') ?>",
				type : "GET",
				data : dat,
				beforeSend : function(){
					$("#tampil").addClass("hidden");
					$("#imgload").removeClass("hidden");
				},
				complete : function(){
					$("#tampil").removeClass("hidden");
					$("#imgload").addClass("hidden");
				},
				success : function(data){
					$("#myTable").html(data);
					$("#dataTables-example").DataTable({
						responsive: true
					});
				},
				error : function(jqXHR, textStatus, errorThrown){
					bootbox.alert("Internal Server Error");
				}
			});
		}
		function reset(){
			$("#myModal").modal();
			$("#nip").val("").trigger("change");
			$("#periode").val("");
			save_method="save";
			$("#warning").addClass("hidden");
	    	$("#success").addClass("hidden");
		}
		function get_id(nip,id_per){
	    	$("#warning").addClass("hidden");
	    	$("#success").addClass("hidden");
	    	id = {"nip" : nip, "id_per" : id_per}
	    	$.ajax({
	    		url : "<?php echo base_url('achievement/achievement_id') ?>",
	    		type : "GET",
	    		data : id,
	    		success : function(data){
	    			$("#myModal").modal();
	    			var dat = jQuery.parseJSON(data);
					$("#nama").val(nip);
					$("#nip").val(nip);
					$("#id_per").val(id_per);
					$("#per").val(id_per);
					$("#ach").val(dat.ach);
	    		},
	    		error : function(jqXHR, textStatus, errorThrown){
	    			alert("Internal Server Error");
	    		}
	    	});
	    }
	    function delete_data(nip,id_per){

	    	bootbox.dialog({
	    		message : "Yakin ingin menghapus data?",
	    		title : "Hapus Data",
	    		buttons :{
	    			danger : {
	    				label : "Delete",
	    				className : "red",
	    				callback : function(){
	    					$.ajax({
					    		url : "<?php echo  base_url('achievement/delete_achievement') ?>/"+nip+"/"+id_per,
					    		type : "POST",
					    		success : function(data){
					    			bootbox.alert({
										message: "Delete Success",
										size: "small"
									});
					    			load_table();
					    		},
					    		error : function(jqXHR, textStatus, errorThrown){
					    			alert("Internal Server Error");
					    		}
					    	});
	    				}
	    			},
	    			main : {
	    				label : "Cancel",
	    				className : "blue",
	    				callback : function(){
	    					return true;
	    				}
	    			}
	    		}
	    	})

	    }
		function save(){
			var nip = $("#nip").val();
			var periode = $("#id_per").val();
			var ach = $("#ach").val();

			if(nip=="" || ach=="" || periode==""){
				$("#warning").removeClass("hidden");
				$("#success").addClass("hidden");
			}else{
				var dat = {
					"nip" : nip,
					"id_per" : periode,
					"ach" : ach
				}
				
				var url = "<?php echo base_url('achievement/add_achievement') ?>";
				
				$.ajax({
					url : url,
					data : dat,
					type : "POST",
					success : function(data){
						$("#warning").addClass("hidden");
						$("#success").removeClass("hidden");
						load_table();
					},	
					error : function(jqXHR,textStatus,errorThrown){
						bootbox.alert("Internal Server Error");
					}
				});
			}
		}
		function ach(){
			var nip = {"nip":$("#nip").val()}
			$.ajax({
				url 	: "<?php echo base_url('achievement/cari_ach') ?>",
				data 	: nip,
				type 	: "GET",
				success : function(data){
					$("#ach").html(data);
				},
				error 	: function(jqXHR,textStatus,errorThrown){
					bootbox.alert("Internal Server Error");
				}
			});
		}
		$(document).ready(function() {
				$(".select2").select2();
		});

		load_table();
	</script>

	<div id="myProfile" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
		<div class="modal-header">
			<h4 class="modal-title"><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->nama; ?></h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<table>
						<tr>
							<td width="80px" height="50px">NIP</td>
							<td><?php echo $this->session->userdata('nip'); ?></td>
						</tr>
						<tr>
							<td width="80px" height="50px">Alamat</td>
							<td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->alamat; ?></td>
						</tr>
						<tr>
							<td width="80px" height="50px">Telepon</td>
							<td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->telp; ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		</div>
	</div>


	<script type="text/javascript">
		function profile(){
			$("#myProfile").modal();
		}
		function ChangePT(id){
			$.ajax({
				url : "<?php echo base_url('main/changept'); ?>/"+id,
				type : "POST",
				success : function(data){
					location.reload();
				},
				error : function(jqXHR, textStatus, errorThrown){
					alert("Oops Something went wrong !");
				} 
			})
		}
	</script>

</body>
</html>