<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data Achivement</h1>
    </div>
	<div class="row">
	        <!-- BEGIN EXAMPLE TABLE PORTLET-->
	        <div class="portlet light">
	            
	            <div class="portlet-body">
	                <div class="table-toolbar">
	                    <div class="row">
	                        <div class="col-md-6">
	                        	<div class="btn-group">
									<form class="form-inline">
										<div class="form-group">
											<?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
										</div>
											<button type="button" onclick="load_table();" class="btn btn-primary" id="tampil">Tampilkan</button>

											<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
									</form>
								</div>
	                            
	                        </div>
	                   
	                    </div>
	                </div>
	                <div id="myTable"></div>
	            </div>
	        </div>
	        <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<!-- 	</main>

    </div>
	<footer id="footer">Copyright © 2016 <a href="#" title="Perkasa App">Perkasa App</a></footer>
</body>
</html> -->

	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © Perkasa App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</diV>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

	<?php echo isset($js)?$js:''; ?>

	<script type="text/javascript">
		function load_table(){
			dat = {
				'periode' : $("#qd_id").val()
			}
			$.ajax({
				url 	: "<?php echo base_url('achievement/tb_ach'); ?>",
				type 	: "GET",
				data 	: dat,
				beforeSend : function(){
					$("#tampil").addClass("hidden");
					$("#imgload").removeClass("hidden");
				},
				complete : function(){
					$("#tampil").removeClass("hidden");
					$("#imgload").addClass("hidden");
				},
				success : function(data){
					$("#myTable").html(data);
					jQuery(document).ready(function() {
					    TableDatatablesEditable.init()
					});
				},
				error 	: function(jqXHR,textStatus,errorThrown){
					bootbox.alert("Oops Something went wrong !");
				}
			});
		}

		load_table();
		function simpan(){
			var dat = {
				"nip" 		: $("#nip").val(),
				"id_per"	: $("#peri").val(),
				"ach"		: $("#ach").val()
			}
			$.ajax({
				url 	: "<?php echo base_url('achievement/add_achievement'); ?>",
				data 	: dat,
				type 	: "POST",
				success : function(data){
					load_table();
				},
				error 	: function(jqXHR,textStatus,errorThrown){
					bootbox.alert("Oops Something went wrong!");
				}
			});
		}
		function delete_data(nip,id_per){

	    	bootbox.dialog({
	    		message : "Yakin ingin menghapus data?",
	    		title : "Hapus Data",
	    		buttons :{
	    			danger : {
	    				label : "Delete",
	    				className : "red",
	    				callback : function(){
	    					$.ajax({
					    		url : "<?php echo  base_url('achievement/delete_achievement') ?>/"+nip+"/"+id_per,
					    		type : "POST",
					    		success : function(data){
					    			bootbox.alert({
										message: "Delete Success",
										size: "small"
									});
					    			load_table();
					    		},
					    		error : function(jqXHR, textStatus, errorThrown){
					    			alert("Internal Server Error");
					    		}
					    	});
	    				}
	    			},
	    			main : {
	    				label : "Cancel",
	    				className : "blue",
	    				callback : function(){
	    					return true;
	    				}
	    			}
	    		}
	    	})

	    }
		var TableDatatablesEditable = function() {
		    var e = function() {
		        function e(e, t) {
		            for (var n = e.fnGetData(t), a = $(">td", t), l = 0, r = a.length; r > l; l++) e.fnUpdate(n[l], t, l, !1);
		            e.fnDraw()
		        }

		        function t(e, t) {
		            var n = e.fnGetData(t),
		                a = $(">td", t);
		                // alert(n[5]);
		            a[5].innerHTML = '<input type="text" class="form-control input-small" id="ach" value="' + n[5] + '"><input type="hidden" class="form-control input-small" id="nip" value="' + n[2] + '">', 
		            // a[5].innerHTML = '<input type="hidden" class="form-control input-small" id="nip" value="' + n[2] + '">', 
		            // a[2].innerHTML = '<input type="text" class="form-control input-small" value="' + n[2] + '">', 
		            // a[3].innerHTML = '<input type="text" class="form-control input-small" value="' + n[3] + '">', 
		            // a[4].innerHTML = '<a class="edit" href="">Save</a>', 
		            a[7].innerHTML = '<a class="edit" href="javascript:;" onclick="simpan();">Save</a> <a class="cancel" href="javascript:;">Cancel</a>'
		        }

		        function n(e, t) {
		            var n = $("input", t);
		            // e.fnUpdate(n[0].value, t, 0, !1), e.fnUpdate(n[1].value, t, 1, !1), e.fnUpdate(n[2].value, t, 2, !1), e.fnUpdate(n[3].value, t, 3, !1), e.fnUpdate('<a class="edit" href="">Edit</a>', t, 4, !1), e.fnUpdate('<a class="delete" href="">Delete</a>', t, 5, !1), e.fnDraw()
		        }
		        var a = $("#sample_editable_1"),
		            l = a.dataTable({
		                lengthMenu: [
		                    [5, 10, 20, -1],
		                    [5, 10, 20, "All"]
		                ],
		                pageLength: 10,
		                language: {
		                    lengthMenu: " _MENU_ records"
		                },
		                columnDefs: [{
		                    orderable: !0,
		                    targets: [0]
		                }, {
		                    searchable: !0,
		                    targets: [0]
		                }],
		                order: [
		                    [0, "asc"]
		                ]
		            }),
		            r = ($("#sample_editable_1_wrapper"), null),
		            o = !1;
		        $("#sample_editable_1_new").click(function(e) {
		            if (e.preventDefault(), o && r) {
		                if (!confirm("Previose row not saved. Do you want to save it ?")) return l.fnDeleteRow(r), r = null, void(o = !1);
		                n(l, r), $(r).find("td:first").html("Untitled"), r = null, o = !1
		            }
		            var a = l.fnAddData(["", "", "", "", "", ""]),
		                i = l.fnGetNodes(a[0]);
		            t(l, i), r = i, o = !0
		        }), a.on("click", ".delete", function(e) {
		            if (e.preventDefault(), 0 != confirm("Are you sure to delete this row ?")) {
		                var t = $(this).parents("tr")[0];
		                l.fnDeleteRow(t), alert("Deleted! Do not forget to do some ajax to sync with backend :)")
		            }
		        }), a.on("click", ".cancel", function(t) {
		            t.preventDefault(), o ? (l.fnDeleteRow(r), r = null, o = !1) : (e(l, r), r = null)
		        }), 
		        a.on("click", ".edit", function(a) {
		            a.preventDefault(), o = !1;
		            var i = $(this).parents("tr")[0];
		            null !== r && r != i ? (e(l, r), t(l, i), r = i) : r == i && "Save" == this.innerHTML ? (n(l, r), r = null) : (t(l, i), r = i)
		        })
		    };
		    return {
		        init: function() {
		            e()
		        }
		    }
		}();
		
	</script>

	<div id="myProfile" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
		<div class="modal-header">
			<h4 class="modal-title"><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->nama; ?></h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<table>
						<tr>
							<td width="80px" height="50px">NIP</td>
							<td><?php echo $this->session->userdata('nip'); ?></td>
						</tr>
						<tr>
							<td width="80px" height="50px">Alamat</td>
							<td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->alamat; ?></td>
						</tr>
						<tr>
							<td width="80px" height="50px">Telepon</td>
							<td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->telp; ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		</div>
	</div>


	<script type="text/javascript">
		function profile(){
			$("#myProfile").modal();
		}
		function ChangePT(id){
			$.ajax({
				url : "<?php echo base_url('main/changept'); ?>/"+id,
				type : "POST",
				success : function(data){
					location.reload();
				},
				error : function(jqXHR, textStatus, errorThrown){
					alert("Oops Something went wrong !");
				} 
			})
		}
	</script>

</body>
</html>