<div class="page-content">
    <div class="breadcrumbs">
        <h1>Download/Upload List Achievement</h1>
    </div>
    <div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-sharp">Download List Data Achievement</div>
				</div>
				<div class="portlet-body">
					<div class="form-group">
						<label>Cluster</label>
							<?php echo form_dropdown('lwok',$lwok,'','id="lwok" class="form-control"') ?>					
						</div>
					<div class="form-group">
						<label>Periode</label>
							<?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
						</div>
					<div class="form-group">
						<button type="button" onclick="download()" class="btn btn-primary">Download</button>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="portlet light bordered">
				<div class="portlet-title">
					<div class="caption font-green-sharp">Import Data Achievement</div>
				</div>
				<div class="portlet-body">
					<form id="uploaddata" action="<?php echo base_url('upload/upload_ach'); ?>" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label>Periode</label>
								<?php echo form_dropdown('qd_id_ach',$periode,'','id="qd_id_ach" class="form-control"'); ?>
						</div>
						<div class="form-group">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<label>File Import</label>
		                        <div class="input-group">
		                            <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
		                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
		                                <span class="fileinput-filename"> </span>
		                            </div>
		                            <span class="input-group-addon btn default btn-file">
		                                <span class="fileinput-new"> Select file </span>
		                                <span class="fileinput-exists"> Change </span>
		                                <input type="file" name="userfile" id="userfile"> </span>
		                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
		                        </div>
		                    </div>
						</div>
						
						<div class="form-group">
							<button type="submit" class="btn btn-primary">Upload</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- 	</main>

    </div>
	<footer id="footer">Copyright © 2016 <a href="#" title="Perkasa App">Perkasa App</a></footer>
</body>
</html> -->

	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © Perkasa App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</diV>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>

	<?php echo isset($js)?$js:''; ?>

	<?php echo isset($javascript)?$javascript:''; ?>

	<?php echo $this->session->flashdata('message'); ?>

	<div id="myProfile" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
		<div class="modal-header">
			<h4 class="modal-title"><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->nama; ?></h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<table>
						<tr>
							<td width="80px" height="50px">NIP</td>
							<td><?php echo $this->session->userdata('nip'); ?></td>
						</tr>
						<tr>
							<td width="80px" height="50px">Alamat</td>
							<td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->alamat; ?></td>
						</tr>
						<tr>
							<td width="80px" height="50px">Telepon</td>
							<td><?php echo $this->Main_Model->get_username($this->session->userdata('nip'))->telp; ?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
		</div>
	</div>


	<script type="text/javascript">
		function profile(){
			$("#myProfile").modal();
		}
		function ChangePT(id){
			$.ajax({
				url : "<?php echo base_url('main/changept'); ?>/"+id,
				type : "POST",
				success : function(data){
					location.reload();
				},
				error : function(jqXHR, textStatus, errorThrown){
					alert("Oops Something went wrong !");
				} 
			})
		}
	</script>

</body>
</html>

