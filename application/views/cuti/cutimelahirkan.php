<div class="page-content">
	<div class="breadcrumbs">
		<h1>Data Cuti Khusus</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="btn-group">
					<button id="btn" class="btn btn-primary" type="button" onclick="reset();" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data </button>
					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
				</div>
			</div>
			<br>
			<div id="myTable"></div>
		</div>
	</div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
        <h4 class="modal-title">Form Cuti Hamil dan Melahirkan</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            	<form id="cutikhusus">
					<div class="form-group">
						<label>Nama</label>
							<input type="hidden" name="id" id="id" class="kosong">
							<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2 kosong"'); ?>
					</div>
					<div class="form-group">
						<label>Tipe Cuti</label>
							<?php echo form_dropdown('tipe',$tipe,'','id="tipe" class="form-control kosong"'); ?>
					</div>
					<div class="form-group">
						<label>Tgl Cuti Awal</label>
							<input type="text" class="form-control date-picker kosong" id="tgl_awal" name="tgl_awal" data-date-format="dd/mm/yyyy">
					</div>
					<div class="form-group">
						<label>Tgl Cuti Akhir</label>
							<input type="text" class="form-control date-picker kosong" id="tgl_akhir" name="tgl_akhir" data-date-format="dd/mm/yyyy">
					</div>
				</form>
			</div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>