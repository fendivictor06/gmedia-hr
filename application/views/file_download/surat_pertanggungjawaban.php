<body style="font-family:verdana,geneva,sans-serif;font-size:14px;margin-left: 40px;margin-top: 30px; margin-bottom: 20px;margin-right: 20px;">	
<p style="text-align: center;"><span style="font-family:verdana,geneva,sans-serif"><strong><u>SURAT PERTANGGUNG JAWABAN SUAMI / ISTRI / ORANGTUA</u></strong></span></p>

<p><span style="font-family:verdana,geneva,sans-serif">Yang bertanda tangan di bawah ini kami suami/istri/orangtua ;</span></p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td style="width:20%"><span style="font-family:verdana,geneva,sans-serif">Nama</span></td>
			<td style="width:2%"><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><span style="font-family:verdana,geneva,sans-serif">Pekerjaan</span></td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><span style="font-family:verdana,geneva,sans-serif">Alamat</span></td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p>Adalah penanggungjawab mutlak atas seorang karyawan/wati tersebut di bawah ini ;</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td style="width:20%"><span style="font-family:verdana,geneva,sans-serif">Nama</span></td>
			<td style="width:2%"><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tempat/Tgl Lahir</td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Pekerjaan</td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Alamat Kantor</td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p>Bahwa kami bertanggung jawab mutlak untuk menanggung segala akibat dari perbuatannya dan sanggup untuk mengganti kerugian keuangan, apabila sewaktu-waktu suami/istri/anak yang kami tanggung tersebut :</p>

<ol>
	<li>Melanggar ketentuan peraturan perusahaan CV. PUTMASARI PRATAMA, sehingga merugikan perusahaan tersebut baik disengaja maupun tidak, baik untuk diri sendiri maupun pihak lain.</li>
	<li>Bahwa saya secara sadar dan tanpa paksaan melaksanakan penggantian kerugian keuangan, baik yang kami tanggung tersebut berada di rumah maupun tidak ada di rumah</li>
	<li>Bahwa saya sudah rela, bukan atas tekanan pihak lain, atas dasar kesadaran, sehubungan dengan tindakan suami/istri/anak yang kami tanggung tersebut ternyata merugikan perusahan CV. PUTMASARI PRATAMA baik berupa :
	<ol style="list-style-type:lower-alpha">
		<li>Memalsukan surat yang menimbulkan kerugian perusahan CV. PUTMASARI PRATAMA&nbsp;</li>
		<li>Menipu atau memalsukan nama orang lain</li>
		<li>Membawa lari uang atau sejenisnya yang berakibat merugikan perusahan CV. PUTMASARI PRATAMA</li>
		<li>Atau tindakan-tindakan lain yang berakibat merugikan perusahan CV. PUTMASARI PRATAMA&nbsp;</li>
	</ol>
	</li>
</ol>

<p>Jika ternyata saya ingkar janji dan tidak menepati surat pernyataan TANGGUNG JAWAB MENGGANTI KEUANGAN, sebagaimana tersebut di atas, kami sanggup dituntut sesuai dengan peraturan hukum dan perundang-undangan yang berlaku di Negara Republik Indonesia.</p>

<p>Demikian surat pernyataan ini kami buat atas dasar kesadaran bukan atas paksaaan dari pihak lain demi membina kejujuran suami/istri/anak yang kami tanggung tersebut di atas.</p>

<p>&nbsp;</p>

<p>................., ........................................</p>

<p>&nbsp;</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td style="text-align:center"><strong><u>Yang Ditanggung</u></strong></td>
			<td style="text-align:center"><strong><u>Yang Menanggung</u></strong></td>
		</tr>
		<tr>
			<td style="height: 100px; text-align: center;">&nbsp;</td>
			<td style="text-align: center;">MATERAI 3000</td>
		</tr>
		<tr>
			<td style="width:50%">&nbsp;</td>
			<td style="width:50%">&nbsp;</td>
		</tr>
	</tbody>
</table>
</body>