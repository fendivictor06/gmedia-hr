<body style="font-family:verdana,geneva,sans-serif;font-size:14px;margin-left: 40px;margin-top: 50px; margin-bottom: 20px;margin-right: 20px;">	
<p style="text-align:center"><span style="font-family:verdana,geneva,sans-serif"><strong><u>SURAT PERTANGGUNG JAWABAN SUAMI / ISTRI / ORANGTUA</u></strong></span></p>

<p>Yang bertanda tangan di bawah ini saya ;</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td style="width:20%"><span style="font-family:verdana,geneva,sans-serif">Nama</span></td>
			<td style="width:2%"><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><span style="font-family:verdana,geneva,sans-serif">Pekerjaan</span></td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><span style="font-family:verdana,geneva,sans-serif">Alamat</span></td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p>Dengan ini kami siap menghadap dan menyatakan dengan sesungguhnya dihadapan Pimpinan perusahan CV. PUTMASARI PRATAMA Semarang bahwa kami benar-benar suami/istri/orangtua dari :</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td style="width:20%"><span style="font-family:verdana,geneva,sans-serif">Nama</span></td>
			<td style="width:2%"><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Tempat/Tgl Lahir</td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Pekerjaan</td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Alamat Kantor</td>
			<td><span style="font-family:verdana,geneva,sans-serif">:</span></td>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<p>Menyatakan pertanggung jawaban apabila suami/istri/anak yang kami tanggung ini menyalah gunakan ketentuan atau peraturan pekerjaan yang berlaku baik secara pidana maupun perdata yang dapat menimbulkan kerugian perusahaan perusahan CV. PUTMASARI PRATAMA, kami siap mempertanggung jawabkan dan mengganti jumlah kerugian secara tunai.</p>

<p>Demikian surat pernyataan ini kami buat secara sadar tanpa paksaan dari pihak manapun dan dapat digunakan sebagaimana mestinya sesuai ketentuan hukum yang berlaku.</p>

<p>................., ........................................</p>

<p>&nbsp;</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:13px;">
	<tbody>
		<tr>
			<td style="text-align:center"><span style="font-size:14px">Yang menyatakan</span></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="height:100px; text-align:center">MATERAI 3000</td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center; width:20%">.............................................................</td>
			<td style="width:50%">&nbsp;</td>
		</tr>
	</tbody>
</table>
</body>