<body style="font-family:verdana,geneva,sans-serif;font-size:14px;margin-left: 40px;margin-top: 40px; margin-bottom: 20px;margin-right: 20px;">	
<p style="text-align:center"><strong><u>PENJELASAN PASAL-PASAL YANG BERKAITAN DENGAN TINDAK PIDANA</u></strong></p>

<ul>
	<li><strong>Pasal 362 (Pencurian)</strong></li>
</ul>

<p style="margin-left:80px">Barang siapa mengambil barang sesuatu, yang seluruhnya atau sebagian kepunyaan orang lain, dengan maksud untuk dimiliki secara melawan hukum, diancam karena pencurian, dengan pidana penjara paling lama <strong>5 (Lima) Tahun.</strong></p>

<ul>
	<li><strong>Pasal 263 (Pemalsuan Surat)</strong></li>
</ul>

<ol style="margin-left:40px">
	<li>Barang siapa membuat surat palsu atau memalsukan surat yang dapat menimbulkan sesuatu hak, perikatan atau pembebasan hutang, atau yang diperuntukkan sebagai bukti daripada sesuatu hal dengan maksud untuk memakai atau menyuruh orang lain memakai surat tersebut seolah-olah isinya benar dan tidak dipalsu, diancam jika pemakaian tersebut dapat menimbulkan kerugian, karena pemalsuan surat, dengan pidana penjara paling lama <strong>6 (Enam) Tahun.</strong></li>
	<li>Diancam dengan pidana yang sama, barang siapa dengan sengaja memakai surat palsu atau yang dipalsukan seolah-olah sejati, jika pemakaian surat itu dapat menimbulkan kerugian.</li>
</ol>

<ul>
	<li><strong>Pasal 372 (Penggelapan)</strong></li>
</ul>

<p style="margin-left:80px">Barang siapa dengan sengaja dan melawan hukum memiliki barang sesuatu yang seluruhnya atau sebagian adalah kepunyaan orang lain, tetapi yang ada dalam kekuasaannya bukan karena kejahatan diancam karena penggelapan, dengan pidana penjara paling lama <strong>4 </strong>(<strong>Empat) Tahun</strong>.</p>

<ul>
	<li><strong>Pasal 378 (Perbuatan Curang)</strong></li>
</ul>

<p style="margin-left:80px">Barang siapa dengan maksud untuk menguntungkan diri sendiri atau orang lain secara melawan hukum, dengan memakai nama palsu atau martabat palsu, dengan tipu muslihat, ataupun rangkaian kebohongan, menggerakkan orang lain untuk menyerahkan barang sesuatu kepadanya, atau supaya memberi hutang rnaupun menghapuskan piutang diancam karena penipuan dengan pidana penjara paling lam <strong>4 (Empat) Tahun.</strong></p>

<p style="margin-left:40px"><strong>Berkaitan dengan Penjelasan Pasal-Pasal Mengenai Tindak Pidana tersebut Telah saya baca dan dimengerti.</strong></p>

<p style="margin-left:40px">&nbsp;</p>

<p style="margin-left: 40px;">................., ........................................</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%">
	<tbody>
		<tr>
			<td style="text-align:center"><strong><span style="font-size:14px">Ttd</span></strong></td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="height:70px; text-align:center">&nbsp;</td>
			<td style="text-align:center">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align:center; width:20%">(.........................................)</td>
			<td style="width:50%">&nbsp;</td>
		</tr>
	</tbody>
</table>
</body>