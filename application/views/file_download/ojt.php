<!DOCTYPE html>
<html lang="en">
<!-- <body style="font-family:verdana,geneva,sans-serif;font-size:12px;margin-left: 96px;margin-top: 0; margin-bottom: 96px;margin-right: 50.645669px;"> -->
<body style="font-family:verdana,geneva,sans-serif;font-size:14px;margin-left: 40px;margin-top: 120px; margin-bottom: 40px;margin-right: 20px;">
<?php //echo isset($kop_surat) ? $kop_surat : ''; ?>
<p style="text-align:center;"><span style="font-size:18px"><span style="font-family:verdana,geneva,sans-serif"><strong><u>PERJANJIAN PELATIHAN KERJA</u></strong></span></span></p>

<p style="text-align:center"><span style="font-size:18px"><span style="font-family:verdana,geneva,sans-serif"><strong>(On the Job Trainning)</strong></span></span></p>

<p>&nbsp;</p>

<p>Yang bertanda tangan di bawah ini:</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td width="30px">1.</td>
			<td width="100px">Nama</td>
			<td width="5px">:</td>
			<td><?php echo $nama; ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jenis Kelamin</td>
			<td>:</td>
			<td><?php echo $jk; ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Tanggal Lahir</td>
			<td>:</td>
			<td><?php echo $tgl_lahir; ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat</td>
			<td>:</td>
			<td><?php echo $alamat; ?></td>
		</tr>
		<tr>
			<td colspan="4">bertindak untuk dan atas nama diri sendiri, yang selanjutnya dalam Perjanjian ini disebut sebagai Pihak 1;</td>
		</tr>
	</tbody>
</table>

<p>&nbsp;</p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td width="30px">2.</td>
			<td width="100px">Nama</td>
			<td width="5px">:</td>
			<td>Hepi Yulian Furi Santoso</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jabatan</td>
			<td>:</td>
			<td>Direktur CV. Putmasari Pratama</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Jenis Usaha</td>
			<td>:</td>
			<td>Telekomunikasi dan Perdagangan Umum</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>Alamat</td>
			<td>:</td>
			<td>Rukan Mutiara Manina Kav. 31, Jl. Marina Semarang.</td>
		</tr>
		<tr>
			<td colspan="4">bertindak untuk dan atas nama CV. Putmasari Pratama, yang selanjutnya dalam Perjanjian ini disebut sebagai Pihak 2;</td>
		</tr>
	</tbody>
</table>

<p style="text-align:justify">menyatakan bahwa, pada hari <?php echo $hari; ?>, tanggal <?php echo $tgl_mulai; ?> kedua belah pihak telah sepakat untuk mengadakan Perjanjian Pelatihan Kerja, yang diatur dengan ketentuan sebagai berikut:</p>

<p style="text-align:center"><strong>Pasal 1</strong><br />
<strong>Ketentuan Umum</strong></p>

<ol>
	<li>Selama menjalani perjanjian pelatihan Pihak 1 disebut sebagai trainee.</li>
	<li>Selama Pihak 1 menjalani pelatihan, Pihak 2 akan memberikan sejumlah tugas untuk dipelajari, dilaksanakan dan diselesaikan dengan penuh tanggung jawab.</li>
</ol>

<p style="text-align:center"><strong>Pasal 2</strong><br />
<strong>Lokasi Kerja</strong></p>

<p style="margin-left:40px">Pekerjaan dikerjakan di kantor dan/atau di kantor perwakilan CV. Putmasari Pratama (<?php echo $cabang; ?>).</p>
<p style="text-align:center"><strong>Pasal 3</strong><br />
<strong>Jangka Waktu Berlakunya Perjanjian Kerja</strong></p>

<p style="margin-left:40px">Jangka waktu berlakunya perjanjian pelatihan Kerja, selama 3 (tiga) Bulan, terhitung sejak tanggal <?php echo $tgl_mulai; ?> sampai dengan <?php echo $tgl_akhir; ?>.</p>

<p style="text-align:center"><strong>Pasal 4</strong><br />
<strong>Jenis Pekerjaan</strong></p>

<!-- <p style="margin-left:40px"></p> -->
<?php echo $description; ?>

<p style="text-align:center"><strong>Pasal 5</strong><br />
<strong>Kewajiban dan Hak Pihak 1</strong></p>

<p style="text-align:center">&nbsp;</p>

<ol>
	<li>Selama menjalani pelatihan, pihak 1 mempunyai kewajiban:</li>
</ol>

<ol style="list-style-type:lower-alpha; margin-left:40px">
	<li>Menjalani Pelatihan mulai tanggal <?php echo $tgl_mulai; ?> sampai dengan <?php echo $tgl_akhir; ?>, dengan masuk setiap hari Senin sampai dengan Sabtu dan/atau sesuai dengan pembagian waktu Pelatihan&nbsp; yang&nbsp; ditetapkan untuknya.</li>
	<li>Melakukan presensi setiap kali masuk dan pulang.</li>
	<li>Mengurus surat ijin masuk apabila terlambat hadir, yaitu bila hadir setelah jam kerja yang telah ditetapkan untuknya dimulai.</li>
	<li>Menyerahkan bukti surat keterangan dokter bila tidak masuk bekerja karena sakit.</li>
	<li>Menaati semua isi perjanjian ini, tata tertib kerja dan peraturan perusahaan sebagaimana berlaku di CV. Putmasari Pratama.</li>
	<li>Mengganti dengan barang yang sama atau memberikan ganti rugi sebesar harga beli pada saat itu, apabila karena kelalaian atau kecerobohan atau kesalahan Pihak 1&nbsp;mengakibatkan barang milik Perusahaan yang menjadi tanggung jawabnya dalam menjalankan tugas rusak atau hilang.</li>
	<li>Menyerahkan seluruh hasil pelaksanaan tugas selama bekerja sebagai trainee kepada Pihak 2.</li>
	<li>Memenuhi target yang ditentukan oleh perusahaan.</li>
	<li>Mengenakan busana yang sopan selama bekerja.</li>
</ol>

<ol start="2">
	<li>Selama menjalani kerja waktu tertentu, Pihak 1 mempunyai hak :</li>
</ol>

<ol style="list-style-type:lower-alpha; margin-left:40px">
	<li>Menerima upah (gaji) dari pihak 2.</li>
	<li>Besarnya gaji yang diberikan akan diatur dalam kesepakatan tersendiri dan merupakan satu kesatuan dari perjanjian pelatihan ini</li>
</ol>

<p style="text-align:center"><strong>Pasal 6</strong><br />
<strong>Kewajiban dan Hak Pihak 2</strong></p>

<p style="text-align:center">&nbsp;</p>

<ol>
	<li>Selama Pihak 1 menjalani perjanjian pelatihan, Pihak 2 mempunyai kewajiban :
	<ol style="list-style-type:lower-alpha">
		<li>Memberikan upah (gaji) kepada Pihak 1.</li>
	</ol>
	</li>
</ol>

<ol start="2">
	<li>Selama Pihak 1 menjalani perjanjian pelatihan, Pihak 2 mempunyai hak :</li>
</ol>

<ol style="list-style-type:lower-alpha; margin-left:40px">
	<li>Memberikan pembinaan dan bimbingan, kepada Pihak 1, baik secara langsung atau tidak langsung.</li>
	<li>Memberikan sanksi pelanggaran dan/atau melakukan Pemutusan kerja, apabila Pihak 1 tidak menaati atau melanggar: ketentuan, tata tertib kerja, peraturan perusahaan dan atau tidak mencapai target yang ditetapkan bagi karyawan CV. Putmasari Pratama.&nbsp;&nbsp;&nbsp;</li>
	<li>Mengubah&nbsp; lokasi kerja dan atau jenis tugas Pihak 1 selama menjalani perjanjian pelatihan asal masih dalam lingkup pekerjaan seperti dimaksud pada pasal 4, dengan memberitahu terlebih dulu kepada Pihak 1.</li>
	<li>Memiliki seluruh hasil pelaksanaan tugas Pihak 1 selama menjalani perjanjian pelatihan</li>
</ol>

<p style="text-align:center"><strong>Pasal 7</strong><br />
<strong>Lain-lain</strong></p>

<p style="text-align:center">&nbsp;</p>

<ol>
	<li>Pihak 2 tidak berkewajiban memberikan kompensasi dalam bentuk apapun kepada Pihak 1, apabila Jangka Waktu Berlakunya Pelatihan&nbsp; berakhir sesuai pasal 3 perjanjian ini.</li>
	<li>Apabila Pihak 1 mengundurkan diri sebelum masa pelatihan ini berakhir maka kepada pihak 1 tetap wajib menyelesaikan tugas dan kewajiban yang telah diberikan kepadanya terlebih dahulu dan kepadanya perusahaan dapat meminta pertanggungjawaban.</li>
	<li>Hal-hal lain yang belum tercakup dalam Perjanjian Pelatihan Kerja ini, bila dipandang perlu dalam kaitannya dengan pelaksanaan kerja waktu tertentu, akan dibicarakan antara Pihak 1 dan Pihak 2, dan dibuat kesepakatan bersama secara tertulis atau lisan.</li>
</ol>

<p style="text-align:center"><strong>Pasal 8</strong><br />
<strong>Penutup</strong></p>

<p style="margin-left:40px">Perjanjian ini ditandatangani dengan penuh kesadaran tanpa paksaaan pihak manapun, dibuat rangkap 2, bermeterai cukup dan sah untuk ditaati dalam pelaksanaannya.</p>
<br>
<p>Semarang, <?php echo $tgl_mulai; ?></p>

<table border="0" cellpadding="1" cellspacing="1" style="width:100%;font-family:verdana,geneva,sans-serif;font-size:14px;">
	<tbody>
		<tr>
			<td style="height:100px">&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="width:70%"><u><?php echo $nama; ?></u></td>
			<td style="width:30%"><u>Hepi Yulian Furi Santoso, SH</u></td>
		</tr>
		<tr>
			<td>Pihak 1</td>
			<td>Pihak 2</td>
		</tr>
	</tbody>
</table>

</body>
</html>