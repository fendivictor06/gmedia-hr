<!DOCTYPE html>
<html>
<head>
	<title>Rekapitulasi Absensi <?php echo $periode; ?></title>
</head>
<style type="text/css">
	body {
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			font-size: 12px;
	}

	.tb_rekap {
	    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
	    border-collapse: collapse;
	    width: 100%;
	}

	.tb_rekap td, .tb_rekap th {
	    border: 1px solid #ddd;
	    padding: 3px;
	}

	.tb_rekap tr:nth-child(even){background-color: #f2f2f2;}

	.tb_rekap tr:hover {background-color: #ddd;}

	.tb_rekap th {
	    padding-top: 12px;
	    padding-bottom: 12px;
	    text-align: left;
	    background-color: #265180;
	    color: white;
	}

	.text-center {
		text-align: center;
	}

	.text-right {
		text-align: right;	
	}
</style>
<body>
	<h1 class="text-center" style="color:#265180">Rekapitulasi Absensi <?php echo $periode; ?></h1>
	<table>
		<tr>
			<td width="6%">NIK</td>
			<td width="2%">:</td>
			<td><?php echo $nip; ?></td>
		</tr>
		<tr>
			<td>NAMA</td>
			<td>:</td>
			<td><?php echo $nama; ?></td>
		</tr>
		<!-- <tr>
			<td>CABANG</td>
			<td>:</td>
			<td><?php echo $cabang; ?></td>
		</tr>
		<tr>
			<td>DIVISI</td>
			<td>:</td>
			<td><?php echo $divisi; ?></td>
		</tr> -->
	</table>
	<br> <br>
	<table border="1" class="table-bordered table tb_rekap">
		<thead>
			<tr>
				<td colspan="<?php echo $colspan; ?>" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;" ><b>UANG MAKAN</b></td>
				<td rowspan="2" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>TOTAL</b></td>
				<td colspan="<?php echo $colspan; ?>" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>UANG LEMBUR</b></td>
				<td rowspan="2" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>TOTAL</b></td>
			</tr>
			<tr>
				<?php 
					for ($a = 0; $a < 2; $a++) {
						for ($i = 0; $i < $colspan; $i++) {
							echo '<td style="background-color:#265180; color:#fff;" align="center">'.$header_tgl[$i].'</td>';
						}
					}
				?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<?php  
					$total_uang_makan = 0;
					for ($i = 0; $i < count($uang_makan); $i++) {
						$total_uang_makan = $total_uang_makan + $uang_makan[$i];
						echo '<td align="right">'.uang($uang_makan[$i]).'</td>';
					}
					echo '<td align="right">'.uang($total_uang_makan).'</td>';

					$total_uang_lembur = 0;
					for ($i = 0; $i < count($uang_lembur); $i++) {
						$total_uang_lembur = $total_uang_lembur + $uang_lembur[$i];
						echo '<td align="right">'.uang($uang_lembur[$i]).'</td>';
					}
					echo '<td align="right">'.uang($total_uang_lembur).'</td>';
				?>
			</tr>
		</tbody>
	</table>
	<br> <br>
	<table border="1" class="table-bordered table tb_rekap">
		<thead>
			<tr>
				<td colspan="<?php echo $colspan; ?>" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>UANG DENDA</b></td>
				<td rowspan="2" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>TOTAL</b></td>
				<td rowspan="2" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>BPJS KETENAGAKERJAAN</b></td> 
				<td rowspan="2" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>CICILAN PINJAMAN</b></td>
				<td rowspan="2" style="background-color:#265180; color:#fff; text-align: center; vertical-align: middle;"><b>KETERANGAN</b></td>
			</tr>
			<tr>
				<?php 
					for ($a = 0; $a < 1; $a++) {
						for ($i = 0; $i < $colspan; $i++) {
							echo '<td style="background-color:#265180; color:#fff;" align="center">'.$header_tgl[$i].'</td>';
						}
					}
				?>
			</tr>
		</thead>
		<tbody>
			<tr>
				<?php  
					$total_uang_denda = 0;
					for ($i = 0; $i < count($uang_denda); $i++) {
						$total_uang_denda = $total_uang_denda + $uang_denda[$i];
						echo '<td align="right">'.uang($uang_denda[$i]).'</td>';
					}
					echo '<td align="right">'.uang($total_uang_denda).'</td>';
					echo '<td align="right">'.uang($total_k).'</td>';
					echo '<td align="right">'.uang($cicilan).'</td>';
					echo '<td>'.$keterangan.'</td>';
				?>
			</tr>
		</tbody>
	</table>
	<br> <br>
	<h3 style="color:#265180">Detail Rekapitulasi</h3>
	<table border="1" class="table-bordered table tb_rekap">
		<thead>
			<tr>
				<th>Tanggal</th>
				<th>Uang Makan</th>
				<th>Uang Denda</th>
				<th>Status</th>
				<th>Keterangan Absen</th>
				<th>Keterangan Terlambat</th>
			</tr>
		</thead>
		<tbody>
			<?php  
				if (!empty($detail)) {
					foreach ($detail as $row) {
						echo '<tr>';
						echo '<td>'.tgl_indo($row->tgl).'</td>';
						echo '<td>'.$row->uang_makan.'</td>';
						echo '<td>'.$row->uang_denda.'</td>';
						echo '<td>'.$row->status.'</td>';
						echo '<td>'.$row->keterangan_absen.'</td>';
						echo '<td>'.$row->keterangan_terlambat.'</td>';
						echo '</tr>';
					}
				}
			?>
		</tbody>
	</table>
</body>
</html>