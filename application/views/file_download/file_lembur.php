<!DOCTYPE html>
<html>
<head>
	<title><?php echo isset($title) ? $title : ''; ?></title>
	<style type="text/css">
		body {
			font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
			font-size: 12px;
		}

		#tb_rekap {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		#tb_rekap td, #tb_rekap th {
		    border: 1px solid #ddd;
		    padding: 3px;
		}

		#tb_rekap tr:nth-child(even){background-color: #f2f2f2;}

		#tb_rekap tr:hover {background-color: #ddd;}

		#tb_rekap th {
		    padding-top: 12px;
		    padding-bottom: 12px;
		    text-align: left;
		    background-color: #265180;
		    color: white;
		}

		.text-center {
			text-align: center;
		}

		.text-right {
			text-align: right;	
		}
	</style>
</head>
<body>
	<?php echo isset($html) ? $html : ''; ?>
</body>
</html>