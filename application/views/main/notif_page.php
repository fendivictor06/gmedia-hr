<div class="page-content">
    <div class="breadcrumbs">
        <h1><?php echo isset($h1)?$h1:''; ?></h1>
    </div>
    <div class="row">       
        <div class="portlet light">
        	<?php if($isperiode == '1'){ ?>
        	<div class="portlet-body">
        		<div class="table-toolbar">
    				<div class="row">
    					<!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<label>Periode</label>
								<div class="form-group input-group">
									<?php echo form_dropdown('periode',$periode,'','id="periode" class="form-control"'); ?>
									<span class="input-group-btn">
	                					<button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
	                					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
	                					<button type="button" class="btn btn-default" id="download"><i class="fa fa-download"></i> Download</button>
	                				</span>	
								</div>
						</div> -->
                        <div class="col-md-12">
                            <?php echo form_periode(2); ?>
                        </div>
    				</div>
    			</div>
        	</div>
        	<?php } else { ?>
        		<div class="form-group">
        			<button type="button" class="btn btn-default" id="download"><i class="fa fa-download"></i> Download</button>
        		</div>
        	<?php } ?>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<?php echo isset($modal) ? $modal : ''; ?>