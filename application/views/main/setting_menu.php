<div class="page-content">
    <div class="breadcrumbs"><h1>Menu User</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <form id="f_menu">
                    <div class="form-group">
                        <label>User : <strong><?php echo isset($user) ? $user : ''; ?></strong></label>
                    </div>
                    <div id="tree"></div>
                    <br> <br>
                    <button type="button" id="simpan" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $("#tree").jstree({
        plugins: ["wholerow", "checkbox", "types"],
        core: {
            themes: {
                responsive: !1
            },
            data: <?php echo isset($json) ? json_encode($json) : ''; ?>
        },
        types: {
            "default": {
                icon: "fa fa-folder icon-state-warning icon-lg"
            },
            file: {
                icon: "fa fa-file icon-state-warning icon-lg"
            }
        }
    });

    $("#simpan").click(function () {
        var arr = $("#tree").jstree('get_checked');
        $("#tree").find(".jstree-undetermined").each(
            function(i, element) {
                arr.push( $(element).closest('.jstree-node').attr("id") );
            }
        );    

        $.ajax({
            url : "<?php echo base_url('setting_menu/menu_process'); ?>",
            data : {
                'checked' : arr,
                'user' : "<?php echo isset($user) ? $user : ''; ?>"
            },
            type : "post",
            dataType : "json",
            success : function(data) {
                bootbox.dialog({
                    message : data.message,
                    buttons : {
                        main : {
                            label : "OK",
                            className : "blue",
                            callback : function(){
                                location.reload();
                                return true;
                            }
                        }
                    }
                });
            }
        });
    })
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>