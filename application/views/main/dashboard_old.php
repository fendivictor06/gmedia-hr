<div class="page-content">
    <div class="breadcrumbs">
        <h1>Dashboard</h1>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url('informasi/habiskontrak'); ?>">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php echo $jml->jml+$jml_tdk_ada->jml; ?>">0</span>
                    </div>
                    <div class="desc"> Kontrak Habis/Kosong </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php echo $jml_no_sk; ?>">0</span> 
                    </div>
                    <div class="desc"> SK Masih Kosong </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="#">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php echo $jml_resign->jml; ?>">0</span>
                    </div>
                    <div class="desc"> Resign Periode ini </div>
                </div>
            </a>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number"> 
                        <span data-counter="counterup" data-value="<?php echo $jml_baru->jml; ?>">0</span> 
                    </div>
                    <div class="desc"> Baru Periode ini </div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
    
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-red-sunglo hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Grafik Gaji Bulanan</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <div id="site_activities_loading">
                        <img src="<?php echo base_url(); ?>assets/img/loading.gif" alt="loading" /> </div>
                    <div id="site_activities_content" class="display-none">
                        <div id="site_activities" style="height: 300px;"> </div>
                    </div>
                   
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-bar-chart font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Karyawan Baru</span>
                        </div>
                        
                    </div>
                    <div class="portlet-body">
                        <div id="site_statistics_loading">
                            <img src="<?php echo base_url(); ?>/assets/img/loading.gif" alt="loading" /> </div>
                        <div id="site_statistics_content" class="display-none">
                            <div id="site_statistics" class="chart"> </div>
                        </div>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
            
        </div>
    </div>
</div>

    