<div class="page-content">
    <div class="breadcrumbs">
        <h1>Dashboard</h1>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 blue" href="<?php echo base_url('main/kary_kontrak_habis'); ?>">
                <div class="visual">
                    <i class="fa fa-comments"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php echo $jml_kont_kosong->jml; ?>">0</span>
                    </div>
                    <div class="desc" style="font-size: 14px; max-width: 150px;"> Kontrak Habis Periode <?php echo isset($periode)?$periode : ''; ?> </div>
                </div>
            </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 red" href="<?php echo base_url('main/kary_surat_kesanggupan'); ?>">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php echo $jml_surat_kesanggupan->jml; ?>">0</span> 
                    </div>
                    <div class="desc" style="font-size: 14px; max-width: 150px;"> Surat Kesanggupan SP</div>
                </div>
            </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 green" href="<?php echo base_url('main/kary_resign'); ?>">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="<?php echo $jml_resign->jml; ?>">0</span>
                    </div>
                    <div class="desc" style="font-size: 14px; max-width: 150px;"> Resign Periode <?php echo isset($periode)?$periode : ''; ?> </div>
                </div>
            </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php echo base_url('main/kary_baru'); ?>">
                <div class="visual">
                    <i class="fa fa-globe"></i>
                </div>
                <div class="details">
                    <div class="number"> 
                        <span data-counter="counterup" data-value="<?php echo $jml_baru->jml; ?>">0</span> 
                    </div>
                    <div class="desc" style="font-size: 14px; max-width: 150px;"> Baru Periode <?php echo isset($periode)?$periode : ''; ?> </div>
                </div>
            </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 yellow" href="<?php echo base_url('main/kary_ijazah'); ?>">
                <div class="visual">
                    <i class="fa fa-pencil"></i>
                </div>
                <div class="details">
                    <div class="number"> 
                        <span data-counter="counterup" data-value="<?php echo $jml_blm_ijazah->jml; ?>">0</span> 
                    </div>
                    <div class="desc" style="font-size: 14px; max-width: 150px;"> Belum Mengumpulkan Ijazah </div>
                </div>
            </a>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <a class="dashboard-stat dashboard-stat-v2 grey" href="<?php echo base_url('main/kary_surat_komitmen'); ?>">
                <div class="visual">
                    <i class="fa fa-car"></i>
                </div>
                <div class="details">
                    <div class="number"> 
                        <span data-counter="counterup" data-value="<?php echo $jml_surat_kom->jml; ?>">0</span> 
                    </div>
                    <div class="desc" style="font-size: 14px; max-width: 150px;"> Belum Mengumpulkan Surat Komitmen </div>
                </div>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="form-group">
        <label>Select Widget</label>
        <select class="bs-select form-control" id="widget_select" multiple>
            <option value="0">Karyawan Baru Per Cabang</option>
            <option value="1">Karyawan Baru</option>
            <option value="2">Keterlambatan</option>
            <option value="3">Ulang Tahun</option>
            <option value="4">Scanlog Absensi</option>
            <option value="5">Grafik Jumlah Karyawan</option>
            <option value="6">Karyawan Resign</option>
        </select>
    </div>
    
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden" id="kary_cabang">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-share font-red-sunglo hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Grafik Karyawan Baru Percabang Periode <?php echo isset($periode)?$periode : ''; ?></span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                   <div id="dashboard_amchart_4" class="CSSAnimationChart"></div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden" id="kary_baru">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Karyawan Baru</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <div id="site_statistics" class="CSSAnimationChart"> </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden" id="kary_resign">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Karyawan Resign</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <div id="resign_chart" class="CSSAnimationChart"> </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden" id="keterlambatan">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Grafik Keterlambatan <?php echo date('d F Y'); ?></span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <div id="chartdiv" class="CSSAnimationChart"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6 hidden" id="absen">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Scanlog <?php echo date('d F Y'); ?></span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="grafik" id="absen_scanlog"></div>
                </div>
            </div>
        </div> 
        <div class="col-md-8 hidden" id="ulangtahun">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Karyawan Ulang Tahun</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <div id="calendar"></div>
                </div>
            </div>
        </div> 
        <div class="col-md-6 hidden" id="jumlah_karyawan">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bar-chart font-dark hide"></i>
                        <span class="caption-subject font-dark bold uppercase">Grafik Jumlah Karyawan <?php echo date('Y') ?></span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div style="min-width: 310px; height: 400px; margin: 0 auto" id="chart_karyawan"></div>
                </div>
            </div>
        </div>           
    </div>
</div>

    