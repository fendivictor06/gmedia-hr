<div class="page-content">
	<div class="breadcrumbs">
		<h1 class="page-header">Biaya Rekrutasi, Training & Workshop</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="btn-group">
					<button class="btn btn-primary" type="button" onclick="reset();" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data </button>
				</div>
			</div>
			<br>
			<div id="myTable"></div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
        <h4 class="modal-title">Form Rekrutasi, Training & Workshop</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="rekrutasi">
            		<div class="form-group">
            			<label>Jenis</label>
            				<input type="hidden" name="id" id="id">
            				<?php echo form_dropdown('jenis',$jenis,'','id="jenis" class="form-control"'); ?>
            		</div>
                    <div class="form-group">
                        <label for="">Nama</label>
                            <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="form-control select2"'); ?>
                    </div>
            		<div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Kontrak</label>
                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl_kontrak" id="tgl_kontrak">
                    </div>
                    <div class="form-group">
                        <label for="">Durasi (bulan)</label>
                            <input type="number" class="form-control" name="durasi" id="durasi">
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Akhir Kontrak</label>
                            <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" readonly="readonly">
                    </div>
            		<div class="form-group">
            			<label>Jumlah</label>
            				<input type="text" class="form-control" name="jumlah" id="jumlah">
            		</div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="keterangan" id="keterangan" rows="5"></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" id="simpan-data" class="btn btn-primary">Simpan</button>
    </div>
</div>