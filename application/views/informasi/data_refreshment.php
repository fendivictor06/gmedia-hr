<div class="page-content">
    <div class="breadcrumbs">
        <h1>Refreshment</h1>
    </div>
    <div class="row">
        
            <div class="portlet light">
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i> Tambah Data
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myTable"></div>
            </div>

    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Refreshment</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_refreshment">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label>Nominal</label>
                            <input type="number" class="form-control blank" name="nominal" id="nominal" />
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control date-picker blank" name="tgl" id="tgl" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control blank" name="keterangan" id="keterangan"></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
