<div class="page-content">
	<div class="breadcrumbs">
		<h1>Status Kepegawaian Karyawan</h1>
	</div>
	<div class="row">


            <div class="portlet light">
                <div class="portlet-body">
                    <?php 
                        ($this->uri->segment(3)) ? $nip = $this->uri->segment(3) : $nip = '';
                        $kary = $this->Main_Model->kary_nip($nip);
                    ?>
                   
                    <table>
                        <tr>
                            <td>NIP</td>
                            <td width="20px"> : </td>
                            <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td> : </td>
                            <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td>
                        </tr>
                        <tr>
                            <td>Status Kepegawaian</td>
                            <td> : </td>
                            <td><?php echo isset($kary->kary_stat)?$kary->kary_stat:''; ?></td>
                        </tr>
                        <?php 
                            (isset($kary->kary_stat)) ? $stat = $kary->kary_stat : $stat = '';
                            if($stat == "TETAP")
                            { 
                        ?>
                        <tr>
                            <td>Tanggal</td>
                            <td> : </td>
                            <td><?php echo $this->Main_Model->format_tgl($kary->tgl_tetap); ?></td>
                        </tr>
                        <?php } ?>
                       
                    </table>
                    <br>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning" onclick="reset();" data-toggle="modal" data-target="#gantiModal">
                                    <i class="fa fa-plus"></i> Ubah Status Karyawan
                                    </button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myTable"></div>
            </div>

    </div>

</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Status Kepegawaian</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="id_u" id="id_u">
                <div class="form-group">
                    <label>Status</label>
                        <select class="form-control kosong" id="status_u" name="status_u" onchange="status_change();">
                            <option value="MAGANG">Magang</option>
                            <option value="TRAINING">Training</option>
                            <option value="KONTRAK">Kontrak</option>
                        </select>
                </div>
                <div class="form-group">
                    <label>Tanggal Awal</label>
                        <input type="text" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" name="tgl_awal_u" id="tgl_awal_u">
                </div>
                <div class="form-group">
                    <label>Tanggal Akhir</label>
                        <input type="text" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" name="tgl_akhir_u" id="tgl_akhir_u">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

<div id="gantiModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <form method="post" action="<?php echo base_url('informasi/ubah_status'); ?>">
        <div class="modal-header">
            <h4 class="modal-title">Status Kepegawaian</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="nip" id="nip" value="<?php echo $this->uri->segment(3); ?>">
                    <div class="form-group">
                        <label>Status</label>
                            <select class="form-control kosong" id="status" name="status" onchange="status_change();">
                                <option value="MAGANG">Magang</option>
                                <option value="TRAINING">Training</option>
                                <option value="KONTRAK">Kontrak</option>
                                <option value="TETAP">Tetap</option>
                            </select>
                    </div>
                    <div class="form-group hidden" id="tgltetap">
                        <label>Tanggal Tetap</label>
                            <input type="text" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" name="tgl_tetap" id="tgl_tetap">
                    </div>
                    <div class="form-group tanggal">
                        <label>Tanggal Awal</label>
                            <input type="text" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" name="tgl_awal" id="tgl_awal">
                    </div>
                    <div class="form-group tanggal">
                        <label>Tanggal Akhir</label>
                            <input type="text" class="form-control date-picker kosong" data-date-format="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>
