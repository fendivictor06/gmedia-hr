<div class="page-content">
    <div class="breadcrumbs">
        <h1>Jumlah Karyawan Aktif</h1>
    </div>

    <div class="row">
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Perkasa</div>
				<div class="panel-body">
					Jumlah Karyawan : 257
					<div id="donut-example"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Surya</div>
				<div class="panel-body">
					Jumlah Karyawan : 34
					<div id="donut-example2"></div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">Unknown (belum ada SK)</div>
				<div class="panel-body">
					Jumlah Karyawan : 0
				</div>
			</div>
		</div>
	</div>

</div>


