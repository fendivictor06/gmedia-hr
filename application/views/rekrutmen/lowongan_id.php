<!-- Content
================================================== -->
<div class="container">
	
	<!-- Recent Jobs -->
	<div class="sixteen columns">
	<div class="padding-right">
		
		<!-- Company Info -->
		<div class="company-info">
			<!--  -->
			<div class="content">
				<h4><?php echo $content->posisi; ?></h4>
				<span><i class="fa fa-map-marker"></i> <?php echo $content->lku; ?></span>
			</div>
			<div class="clearfix"></div>
		</div>

		<?php echo $content->deskripsi; ?>

		<br>
	</div>
	</div>

	<a href="#small-dialog" class="popup-with-zoom-anim button">Apply For This Job</a>

</div>

		<div class="clearfix"></div>
		<div class="margin-bottom-40"></div>