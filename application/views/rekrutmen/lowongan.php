<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="ten columns">
			<!-- <span>We found 1,412 jobs matching:</span> -->
			<h2>Lowongan Pekerjaan</h2>
		</div>

		<!-- <div class="six columns">
			<a href="add-job.html" class="button">Post a Job, It’s Free!</a>
		</div> -->

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="sixteen columns">
	<div class="padding-right">
		
		<form action="<?php echo base_url('rekrutmen/cari_lowongan'); ?>" method="get" class="list-search">
			<button><i class="fa fa-search"></i></button>
			<input type="text" name="search" placeholder="job title, keywords or location" value=""/>
			<div class="clearfix"></div>
		</form>

			<?php 

			echo '<ul class="job-list full">';
		    foreach ($lowongan as $row) {
		    echo '<li><a href="'.base_url('rekrutmen/view_job_id').'/'.$row->id.'">
					<img src="'.base_url('assets/rekrutmen').'/images/job-list-logo-05.png" alt="">
					<div class="job-list-content">
						<h4>'.$row->posisi.'</h4>
						<div class="job-icons">
							<span><i class="fa fa-map-marker"></i> '.$row->lku.'</span>
						</div>
						<p>'.substr(strip_tags($row->deskripsi), 0, 250).' ...</p>
					</div>
					</a>
					<div class="clearfix"></div>
				</li>
				';
		    }
		    echo '</ul><div class="clearfix"></div>';

		    echo $pagination.'<br>';

			 ?>

		

	</div>
	</div>



</div>