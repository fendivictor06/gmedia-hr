<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data Lowongan Kerja</h1>
    </div>

    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="add_new();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Data
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>

</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    
    <div class="modal-header">
        <h4 class="modal-title">Form Lowongan</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_loker">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label>Posisi</label>
                        <input type="text" class="form-control" name="posisi" id="posisi">
                    </div>
                    <!-- <div class="form-group">
                        <label>Kota</label>
                        <?php echo form_dropdown('kota[]', $kota, '', 'id="kota" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tgl Awal</label>
                        <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group">
                        <label>Tgl Akhir</label>
                        <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div> -->
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="summernote" id="summernote_1"> </textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
    
</div>