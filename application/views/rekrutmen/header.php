<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
	<title>Perkasa Career</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="<?php echo base_url('assets/rekrutmen/css/style.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/rekrutmen/css/colors/green.css'); ?>" id="colors">
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<?php echo isset($style)?$style:''; ?>

</head>
<body>
	<div id="wrapper"> <!-- Begin of wrapper -->

		<!-- Header
	================================================== -->
	<header class="sticky-header">
	<div class="container">
		<div class="sixteen columns">
		
			<!-- Logo -->
			<div id="logo">
				<h1><a href="<?php echo base_url('rekrutmen'); ?>"><img src="<?php echo base_url('assets/rekrutmen/images/logo.png'); ?>" alt="Work Scout" /></a></h1>
			</div>

			<!-- Menu -->
			<nav id="navigation" class="menu">
				<ul id="responsive">

					<li><a href="<?php echo base_url('rekrutmen') ?>" <?php echo isset($menu1)?$menu1:''; ?>>Home</a>
					</li>

					<!-- <li><a href="#">Pendaftaran</a>
					</li> -->

					<li><a href="<?php echo base_url('rekrutmen/lowongan'); ?>" <?php echo isset($menu2)?$menu2:''; ?>>Lowongan</a>
					</li>

					<li><a href="<?php echo base_url('rekrutmen/pengumuman'); ?>" <?php echo isset($menu3)?$menu3:''; ?>>Pengumuman</a>
					</li>

					<li><a href="<?php echo base_url('rekrutmen/bantuan'); ?>" <?php echo isset($menu4)?$menu4:''; ?>>Bantuan</a>
					</li>

				</ul>


				<ul class="float-right">
					<li><a href="<?php echo base_url('rekrutmen/daftar'); ?>#tab2"><i class="fa fa-user"></i> Daftar</a></li>
					<li><a href="<?php echo base_url('rekrutmen/daftar'); ?>"><i class="fa fa-lock"></i> Log In</a></li>
				</ul>

			</nav>

			<!-- Navigation -->
			<div id="mobile-navigation">
				<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
			</div>

		</div>
	</div>
	</header>
	<div class="clearfix"></div>

	


	

	