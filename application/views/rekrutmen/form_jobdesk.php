<div class="page-content">
    <div class="breadcrumbs"><h1>Jobdesk</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <a class="btn btn-primary" href="<?php echo base_url('jobdesk/form_jobdesk') ?>" target="_blank"><i class="fa fa-plus"></i> Tambah Data</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Jobdesk</h4>
    </div>
    <form id="form_jobdesk">
        <div class="modal-body">
           <div class="form-group">
                <label>Jabatan</label>
                    <input type="hidden" name="id" id="id">
                    <input type="text" name="jab" id="jab" class="form-control">
           </div>
           <div class="form-group">
                <label>Jobdesk</label>
                <textarea name="summernote" id="summernote_1"> </textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        load_table();
        $("#summernote_1").summernote({
            height: 300
        });
    });

    function clearform() {
        document.getElementById("form_jobdesk").reset();
        $("#id").val("");
        $(".note-editable").html("");
    }

    $("#add_new").click(function(){
        clearform();
    });

    function load_table() {
        $.ajax({
            url : "<?php echo base_url('jobdesk/view_jobdesk') ?>",
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_jobdesk").DataTable({
                    responsive : true
                });
            }
        });
    }

    $("#form_jobdesk").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('jobdesk/process_jobdesk') ?>",
            type : "post",
            dataType : "json",
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success : function(data) {
                if(data.status == true) {
                    clearform(), load_table();
                }
                bootbox.alert(data.message);
            }
        });
        return false;
    });

    $("#form_loker").submit(function(a) {
        a.preventDefault();
        for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
        var b = new FormData($(this)[0]);
        return $.ajax({
            url: "'.base_url('rekrutmen/process_loker').'",
            type: "post",
            data: b,
            async: !1,
            cache: !1,
            contentType: !1,
            processData: !1,
            dataType: "json",
            success: function(a) {
                "true" == a.status ? bootbox.dialog({
                    message: a.message,
                    buttons: {
                        main: {
                            label: "OK",
                            className: "blue",
                            callback: function() {
                                location.href = "'.base_url('rekrutmen/dat_loker').'"
                            }
                        }
                    }
                }) : bootbox.alert(a.message)
            }
        }), !1
    });

    function get_id(id) {
        $.ajax({
            url : "<?php echo base_url('jobdesk/id_jobdesk') ?>/"+id,
            dataType : "json",
            success : function(data) {
                $("#id").val(data.id);
                $("#jab").val(data.jab);
                $("#summernote_1").val(data.description);
                $(".note-editable").html(data.description);
                $("#myModal").modal();
            }
        });
    }

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "<?php echo base_url('jobdesk/delete_jobdesk') ?>/"+id,
                            type : "POST",
                            success : function(data){
                                bootbox.alert({
                                    message: "Delete Success",
                                    size: "small"
                                });
                                load_table();
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        })
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>