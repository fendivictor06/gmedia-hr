	<!-- Banner
	================================================== -->
	<div id="banner" style="background-image: url(assets/rekrutmen/images/banner-home-01.jpg)" class="parallax background" data-img-width="2000" data-img-height="1330" data-diff="400">
		<div class="container">
			<div class="sixteen columns">
				
				<div class="search-container">

					<!-- Form -->
					<h2>Find job</h2>
					<!-- <input type="text" class="ico-01" placeholder="job title, keywords or company name" value=""/>
					<input type="text" class="ico-02" placeholder="city, province or region" value=""/>
					<button><i class="fa fa-search"></i></button> -->

					<!-- Browse Jobs -->
					<!-- <div class="browse-jobs">
						Browse job offers by <a href="browse-categories.html"> category</a> or <a href="#">location</a>
					</div> -->
					
					<!-- Announce -->
					<div class="announce">
						We’ve over <strong>15 000</strong> job offers for you!
					</div>

				</div>

			</div>
		</div>
	</div>

	<!-- Content
	================================================== -->

	<!-- Icon Boxes -->
	<div class="section-background top-0">
		<div class="container">

			<div class="one-third column">
				<div class="icon-box rounded alt">
					<i class="ln ln-icon-Folder-Add"></i>
					<h4>Daftarkan Diri Anda</h4>
					<p>Pellentesque habitant morbi tristique senectus netus ante et malesuada fames ac turpis egestas maximus neque.</p>
				</div>
			</div>

			<div class="one-third column">
				<div class="icon-box rounded alt">
					<i class="ln ln-icon-Search-onCloud"></i>
					<h4>Cari Lowongan</h4>
					<p>Pellentesque habitant morbi tristique senectus netus ante et malesuada fames ac turpis egestas maximus neque.</p>
				</div>
			</div>

			<div class="one-third column">
				<div class="icon-box rounded alt">
					<i class="ln ln-icon-Business-ManWoman"></i>
					<h4>Jadilah Bagian dari Kami</h4>
					<p>Pellentesque habitant morbi tristique senectus netus ante et malesuada fames ac turpis egestas maximus neque.</p>
				</div>
			</div>

		</div>
	</div>
	<!-- Icon Boxes / End -->

	<div class="container">
	
		<!-- Recent Jobs -->
		<div class="sixteen columns">
		<div class="padding-right">
			<h3 class="margin-bottom-25">Lowongan Kerja</h3>
			<ul class="job-list" id="joblist">
				<?php $i=1; foreach ($loker as $row) { ?>
				<li><a href="<?php echo base_url('rekrutmen/view_job_id').'/'.$row->id; ?>">
					<img src="<?php echo base_url('assets/rekrutmen/images/job-list-logo-05.png'); ?>" alt="">
					<div class="job-list-content">
						<h4><?php echo $row->posisi; ?> </h4>
						<div class="job-icons">
							<span><i class="fa fa-map-marker"></i> <?php echo $row->lku; ?></span>
						</div>
					</div>
					</a>
					<div class="clearfix"></div>
				</li>
				<?php } ?>
			</ul>

			<a href="javascript:;" id="showMore" class="button centered"><i class="fa fa-plus-circle"></i> Show More Jobs</a>
			<div class="margin-bottom-55"></div>
		</div>
		</div>

	</div>