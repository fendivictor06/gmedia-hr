<div class="page-content">
    <div class="breadcrumbs"><h1>Proses Rekrutmen</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_periode(); ?>
                        </div>
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <form id="search">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </form>
                        </div> -->
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Ubah Status</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="fstatus">
                    <input type="hidden" name="id" id="id" class="kosong">
                    <div class="form-group">
                        <label>Status</label>
                            <?php echo form_dropdown('status', $status, '', 'id="status" class="form-control kosong"'); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        // $(".date-picker").datepicker({
        //     "setDate": new Date(),
        //     "autoclose": true
        // });
        $(".select2").select2();
        load_table();
    });

    $("#tampil").click(function(){
        load_table();
    });

    function load_table() {
        var tahun = $("#tahun").val();
        var bulan = $("#bulan").val();
        $.ajax({
            url : "<?php echo base_url('rekrutmen/view_proses_rekrutmen'); ?>",
            data : {
                "tahun" : tahun,
                "bulan" : bulan
            },
            beforeSend : function(){
                $("#tampil").addClass("hidden");
                $("#imgload").removeClass("hidden");
            },
            complete : function(){
                $("#tampil").removeClass("hidden");
                $("#imgload").addClass("hidden");
            },
            success : function(data){
                $("#myTable").html(data);
                $("#dataTables-example").DataTable({
                    responsive: true
                });
            }
        });
    }

    function proses_pengajuan(id, val) {
        $.ajax({
            url : "../rekrutmen/approval_pengajuan/"+id+"/"+val,
            type : "post"
        })
    }

    function approve(id) {
        bootbox.dialog({
            message : "Yakin ingin memverifikasi Pengajuan Karyawan?",
            title : "Verifikasi Pengajuan Karyawan",
            buttons :{
                success : {
                    label : "Setuju",
                    className : "green",
                    callback : function(){
                        proses_pengajuan(id, '7');
                        bootbox.alert("Pengajuan telah disetujui !");
                        load_table();
                    }    
                },
                danger : {
                    label : "Tolak",
                    className : "red",
                    callback : function(){
                        proses_pengajuan(id, '8');
                        bootbox.alert("Pengajuan telah ditolak !");
                        load_table();
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        });
    }

    function change_status(id) {
        $("#id").val(id);
        $("#myModal").modal();
    }

    function save() {
        id = $("#id").val();
        status = $("#status").val();
        $.ajax({
            url : "../rekrutmen/change_status/"+id+"/"+status,
            success : function() {
                bootbox.alert("Status telah diubah !");
                load_table();
                $("#myModal").modal('toggle');
            }
        })
    }

    function change(id, val) {
        bootbox.dialog({
            message : "Ubah Progress Pengajuan Karyawan?",
            title : "Progress Pengajuan Karyawan",
            buttons :{
                success : {
                    label : "Ubah",
                    className : "green",
                    callback : function(){
                        $.ajax({
                            url : "../rekrutmen/change_status/"+id+"/"+val,
                            success : function() {
                                bootbox.alert("Status telah diubah !");
                                load_table();
                            }
                        })
                    }    
                },
                
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        });
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>