<div class="page-content">
    <div class="breadcrumbs">
        <h1>Form Jobdesk</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <form id="form_loker" action="#" method="post">
                    <input type="hidden" name="id" id="id" class="blank" value="<?php echo isset($q->id) ? $q->id : ''; ?>">
                    <div class="form-group">
                        <label>Posisi</label>
                        <input type="text" class="form-control blank" name="jab" id="jab" value="<?php echo isset($q->jab) ? $q->jab : ''; ?>">
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="summernote" id="summernote_1" class="ckeditor" rows="20"><?php echo isset($q->description) ? $q->description : ''; ?></textarea>
                    </div>
                    <div class="form-group pull-right">
                        <a href="<?php echo base_url('jobdesk/index') ?>"" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary" id="save">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $("#form_loker").submit(function(a) {
        a.preventDefault();
        for (instance in CKEDITOR.instances) CKEDITOR.instances[instance].updateElement();
        var b = new FormData($(this)[0]);
        return $.ajax({
            url: "<?php echo base_url('jobdesk/process_jobdesk') ?>",
            type: "post",
            data: b,
            async: !1,
            cache: !1,
            contentType: !1,
            processData: !1,
            dataType: "json",
            success: function(a) {
                true == a.status ? bootbox.dialog({
                    message: a.message,
                    buttons: {
                        main: {
                            label: "OK",
                            className: "blue",
                            callback: function() {
                                location.href = "<?php echo base_url('jobdesk/index') ?>"
                            }
                        }
                    }
                }) : bootbox.alert(a.message)
            }
        }), !1
    });
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>