<!-- 	</main>

    </div>
	<footer id="footer">Copyright © 2016 <a href="#" title="Perkasa App">Perkasa App</a></footer>
</body>
</html> -->

	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © <?php echo company(); ?> App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</diV>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/ui-toastr.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>

	<?php echo isset($js)?$js:''; ?>

	<?php echo isset($javascript)?$javascript:''; ?>

	<?php $this->session->flashdata('message'); ?>

</body>
</html>