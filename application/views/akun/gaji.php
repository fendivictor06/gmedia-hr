<div class="page-content">
	<div class="breadcrumbs">
		<h1>Gaji Karyawan</h1>
	</div>
	<div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="btn-group">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Periode</label>
                                <div class="form-group input-group">
                                    <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                    <span class="input-group-btn">
                                        <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                        <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                    </span> 
                                </div>
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>

</div>

<div id="payroll" class="modal fade" tabindex="-1" data-width="760">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Payroll Detail</h4>
    </div>
    <div class="modal-body">
        <div id="target_modal"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>