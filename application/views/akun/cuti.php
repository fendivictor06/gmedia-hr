<div class="page-content">
	<div class="breadcrumbs">
		<h1>Cuti Karyawan</h1>
	</div>
	<div class="row">     
        <div class="portlet light">
            <div class="portlet-body">
                
                    <?php  
                        $btn = '<button type="button" class="btn btn-primary pull-right"  data-target="#myModal" data-toggle="modal" id="tambah" onclick="add_new();"><i class="fa fa-plus"></i> Tambah Data</button>';
                        echo form_periode(null, $btn);
                    ?>

                <!-- <div class="btn-group">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Periode</label>
                                <div class="form-group input-group">
                                    <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                    <span class="input-group-btn">
                                        <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                        <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                    </span> 
                                </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary pull-right" style="margin-top:25px;" data-target="#myModal" data-toggle="modal" id="tambah" onclick="add_new();"><i class="fa fa-plus"></i> Tambah Data</button>
                        </div>
                    </div>
                    <br>
                </div> -->
            </div>
            <br>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Cuti</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <form id="form_cuti">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" name="cuti_dep" id="cuti_dep">
                            <input type="hidden" name="jml" id="jml">
                            <?php echo form_dropdown('nip',$nip,'','id="nip" class="select2" onchange="cek_qt();"'); ?>
                    </div>
                    <!-- <div class="form-group">
                        <label>Th Cuti</label>
                            <?php echo form_dropdown('th',$th,'','id="th" class="form-control" onchange="cek_qt();"'); ?>
                    </div> -->
                    <div class="form-group">
                        <label>Tipe Cuti</label>
                            <?php echo form_dropdown('tipe',$tipe,'','id="tipe" class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Approval</label>
                            <?php echo form_dropdown('penyetuju', $penyetuju, '', 'id="penyetuju" class="select2"'); ?>
                    </div>
                    <div id="sisa"></div>
                    <div class="form-group">
                        <label>Alasan Cuti</label>
                            <textarea class="form-control" id="alasan" name="alasan" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                            <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                            <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <!-- <div class="form-group">
                        <label>Alamat</label>
                            <textarea class="form-control" id="alamat" name="alamat" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>No HP</label>
                            <input type="text" class="form-control" id="hp" name="hp">
                    </div> -->
                    <!-- <div class="form-group">
                        <label>Tgl Cuti</label> 
                        <div id="inputs2">
                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" id="tgl1" name="tgl[]"> <br>
                            <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" id="tgl2" name="tgl[]">
                        </div>
                    </div>
                    <div class="form-group inputs"> </div> -->
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" id="simpan_cuti" class="btn btn-primary">Simpan</button>
    </div>
</div>

    <!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © <?php echo company(); ?> App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
    </div>
</diV>
    <!--[if lt IE 9]>
        <script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
    <![endif]-->

    <!-- Javascript Core -->
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/js.cookie.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/global/scripts/quick-sidebar.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.waypoints.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/counterup/jquery.counterup.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/ui-toastr.js') ?>"></script>

    <?php echo isset($js)?$js:''; ?>

    <script>

        function load_table(){
            var tahun = $("#tahun").val();
            var bulan = $("#bulan").val();
            var param = {
                "tahun" : tahun,
                "bulan" : bulan
            }
            $.ajax({
                url : "<?php echo base_url('akun/view_cuti_nip'); ?>",
                data : param,
                type : "POST",
                beforeSend : function(){
                    $("#tampil").addClass("hidden");
                    $("#imgload").removeClass("hidden");
                },
                complete : function(){
                    $("#tampil").removeClass("hidden");
                    $("#imgload").addClass("hidden");
                },
                success : function(data){
                    $("#myTable").html(data);
                    $("#dataTables-example").DataTable({
                        responsive: true
                    });
                },
                error : function(jqXHR,textStatus,errorThrown){
                    bootbox.alert("Gagal mengambil data");
                } 
            });
        }

        $(document).ready(function(){
            <?php  
                $month = date('m');
                $month = str_replace('0', '', $month);

                $year = date('Y');
            ?>

            var year = "<?php echo $year; ?>";
            var month = "<?php echo $month; ?>";

            $("#tahun").val(year).trigger("change");
            $("#bulan").val(month).trigger("change");

            cek_qt();
            load_table();
        });


        $("#tampil").click(function(){
            load_table();
        });

        <?php echo $this->Main_Model->default_select2(). $this->Main_Model->default_datepicker(); ?>
        <?php echo $this->Main_Model->default_delete_data(base_url('cuti/delete_cutibiasa')); ?>

        function cek_qt() {
            nip = $("#nip").val(), th  = $("#th").val();
            $.ajax({
                url : "<?php echo base_url('cuti/cek_qt'); ?>/"+nip+"/"+th,    
                success : function(data){
                    $("#sisa").html(data);
                }
            });
        }
                    
        function add_new() {
            save_method = "save";
            $("hidden").val("");
            document.getElementById("form_cuti").reset();
            $("#penyetuju").val('').trigger('change');
            $("#nip").val('').trigger('change');
        }

        function save() {
            (save_method=="save") ? url = "<?php echo base_url('cuti/add_cutibiasa') ?>" : url = "<?php echo base_url('cuti/update_cutibiasa') ?>";
            $.ajax({
                url : url,
                data : $("#form_cuti").serialize(),
                type : "POST",
                dataType : "JSON",
                beforeSend : function(){
                    $("#simpan_cuti").attr('disabled', true);
                },
                complete : function(){
                    $("#simpan_cuti").attr('disabled', false);
                },
                success : function(data){
                    if(data.status == true) {
                        add_new();
                        bootbox.dialog({
                            message : data.message,
                            buttons : {
                                main : {
                                    label : "OK",
                                    className : "blue",
                                    callback : function(){
                                        
                                        return true;
                                    }
                                }
                            }
                        });
                        load_table();
                        $("#myModal").modal('toggle');
                    } else {
                        bootbox.alert(data.message);
                    }
                },
                error : function(jqXHR,textStatus,errorThrown){
                    bootbox.alert("Internal Server Error");
                }
            });
        }

        function get_id(id) {
            save_method="update";
            id = {"id":id}
            $.ajax({
                url : "<?php echo base_url('cuti/get_id_cutibiasa') ?>",
                data : id,
                dataType: "JSON",
                type : "GET",
                success : function(data){
                    $("#myModal").modal();
                    $("#id").val(data[0].id_cuti_det);
                    $("#nip").val(data[0].nip).trigger("change");
                    // $("#hp").val(data[0].no_hp);
                    // $("#alamat").val(data[0].alamat_cuti);
                    $("#alasan").val(data[0].alasan_cuti);
                    // $("#th").val(data[0].th);
                    $("#penyetuju").val(data[0].pola).trigger('change');
                    $("#cuti_dep").val(data[0].cuti_dep);
                    $("#tgl_awal").val(data[0].tgl);
                    $("#tgl_akhir").val(data[data.length - 1].tgl);
                    cek_qt();
                },
                error : function(jqXHR,textStatus,errorThrown){
                    bootbox.alert("Gagal mengambil data!");
                }
            });
        }

        function del_detail(id){
            $.ajax({
                url : "'.base_url('cuti/del_detail').'/"+id,
                type : "GET",
                success : function(data){
                    $("#"+id).remove(), load_table(), cek_qt();
                }
            });
        }
    </script>
    <?php $this->session->flashdata('message'); ?>
</body>
</html>