<div class="page-content">
    <div class="breadcrumbs">
        <h1>Dashboard</h1>
    </div>

    <h4>Selamat Datang <?php echo $this->Main_Model->get_username($this->session->userdata('id_user'))->nama; ?>, di HR System <?php echo $this->Main_Model->cari_idp($this->session->userdata('idp'))->bu; ?></h4>
    <h5>Silahkan klik menu pilihan yang berada di sebelah kiri untuk mengakses informasi.</h5>
    <br>

    <div class="row">
        <?php
        for ($i = 0; $i < count($counter); $i++) {
            echo '  <div class="col-md-3">
                            <a class="dashboard-stat dashboard-stat-v2 '.$counter[$i]['warna'].'" href="'.$counter[$i]['link'].'">
                                <div class="visual">
                                    <i class="'.$counter[$i]['icon'].'"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="'.$counter[$i]['angka'].'">0</span>
                                    </div>
                                    <div class="desc" style="font-size: 14px; max-width: 150px;"> '.$counter[$i]['label'].'  </div>
                                </div>
                            </a>
                        </div>';
        }
        ?>
    </div>

    <div class="row">

        <!-- <div class="col-md-12" id="terlambat_today">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Terlambat Hari ini</span>
                </div>
                <div class="portlet-body">
                    <div id="table_terlambat"></div>
                </div>
            </div>
        </div> -->

        <!-- <div class="col-md-12" id="scandate_today">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Scan Hari ini</span>
                </div>
                <div class="portlet-body">
                    <div id="table_scan_date"></div>
                </div>
            </div>
        </div> -->

        <div class="col-md-12" id="portlet_mangkir">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Mangkir Tanggal <?php echo date('d F Y', strtotime("-2 day", strtotime(date('d F Y')))).' s/d '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y')))); ?></span>
                </div>
                <div class="portlet-body">
                    <div id="table_mangkir"></div>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="portlet_terlambat">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Terlambat Tanggal <?php echo date('d F Y', strtotime("-2 day", strtotime(date('d F Y')))).' s/d '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y')))); ?></span>
                </div>
                <div class="portlet-body">
                    <div id="table_klarifikasi_terlambat"></div>
                </div>
            </div>
        </div>

        <!-- <div class="col-md-12" id="portlet_lembur">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Verifikasi Lembur</span>
                </div>
                <div class="portlet-body">
                    <div id="table_lembur"></div>
                </div>
            </div>
        </div> -->

        <div class="col-md-12" id="portlet_cuti">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Verifikasi Cuti</span>
                </div>
                <div class="portlet-body">
                    <div id="table_cuti"></div>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="portlet_cuti_khusus">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Verifikasi Cuti Khusus</span>
                </div>
                <div class="portlet-body">
                    <div id="table_cuti_khusus"></div>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="portlet_pulang_awal">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Verifikasi Pulang Awal</span>
                </div>
                <div class="portlet-body">
                    <div id="table_pulang_awal"></div>
                </div>
            </div>
        </div>

        <div class="col-md-12" id="portlet_tinggal_kerja">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Keluar Kantor</span>
                </div>
                <div class="portlet-body">
                    <div id="table_tinggal_kerja"></div>
                </div>
            </div>
        </div>

        <!-- <div class="col-md-12" id="portlet_perdin">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Perjalanan Dinas</span>
                </div>
                <div class="portlet-body">
                    <div id="table_perdin"></div>
                </div>
            </div>
        </div> -->

       <!--  <div class="col-md-12" id="portlet_pengajuan">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Pengajuan Karyawan</span>
                </div>
                <div class="portlet-body">
                    <div id="table_pengajuan"></div>
                </div>
            </div>
        </div> -->

        <!-- <div class="col-md-12" id="portlet_finalisasi">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <span class="caption-subject font-dark bold uppercase">Finalisasi Karyawan</span>
                </div>
                <div class="portlet-body">
                    <div id="table_finalisasi"></div>
                </div>
            </div>
        </div> -->

    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Klarifkasi Absensi</h4>
    </div>
    <form id="form_presensi" enctype="multipart/form-data">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_ab" id="id_ab">
                    <input type="hidden" name="nip" id="nip">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control" readonly="readonly" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                    </div>
                    <div class="form-group">
                        <label>Presensi</label>
                            <?php echo form_dropdown('absen', $absen, '', 'class="form-control" id="absen"'); ?>
                    </div>
                    <div id="kuota_cuti"></div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Jenis Cuti Khusus</label>
                            <?php echo form_dropdown('cuti_khusus', $tipe, '', 'id="cuti_khusus" class="form-control"'); ?>
                    </div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Awal Cuti Khusus</label>
                            <input type="text" name="awal_cuti_khusus" id="awal_cuti_khusus" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Akhir Cuti Khusus</label>
                            <input type="text" name="akhir_cuti_khusus" id="akhir_cuti_khusus" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group masuk hidden">
                        <label>Scan Masuk</label>
                            <!-- <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24"> -->
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" class="form-control" id="scan_masuk" name="scan_masuk">
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                    </div>
                    <div class="form-group masuk hidden">
                        <label>Scan Pulang</label>
                            <!-- <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24"> -->
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" class="form-control" id="scan_pulang" name="scan_pulang">
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="ket" id="ket" rows="5" readonly="readonly"></textarea>
                    </div>
                    <div class="form-group">
                        <label>File Upload</label>
                            <input type="file" name="file_upload" id="file_upload" class="form-control">
                    </div>
                    <div class="form-group klarifikasi">
                        <label>Klarifikasi</label>
                            <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save_button" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>   


<div id="klarifikasi_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title" id="detailtitle">Klarifikasi Terlambat</h4>
    </div>
    <form id="form_klarifikasi">
        <div class="modal-body">
            <div class="form-group">
                <label>Nama</label>
                <input type="hidden" name="id_terlambat" id="id_terlambat">
                <input type="hidden" name="id_ab_t" id="id_ab_t">
                <input type="text" name="nama_t" id="nama_t" class="form-control" readonly="readonly">
            </div>
            <div class="form-group">
                <label>Tanggal</label>
                <input type="text" name="tgl_t" id="tgl_t" class="form-control" readonly="readonly">
            </div>
            <div class="form-group">
                <label>Scan Masuk</label>
                <input type="text" name="scan_masuk_t" id="scan_masuk_t" class="form-control timepicker timepicker-24">
            </div>
            <div class="form-group">
                <label>Uang Makan</label>
                <select class="form-control" name="makan" id="makan">
                    <option value="1">Ya</option>
                    <option value="0">Tidak</option>
                </select>
            </div>
            <div class="form-group">
                <label>File Upload</label>
                <input type="file" name="file_upload_t" id="file_upload_t" class="form-control">
            </div>
            <div class="form-group">
                <label>Klarifikasi</label>
                    <textarea class="form-control" name="klarifikasi_t" id="klarifikasi_t" rows="5">Tugas Kantor</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save_terlambat" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<div id="Mdownload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Download File</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="fdownload">
                    <input type="hidden" name="id_file" id="id_file" class="blank">
                    <div class="form-group">
                        <label>File</label>
                            <select class="form-control" name="file" id="file">
                                <option value="1">File OJT</option>
                                <option value="2">Surat Pernyataan</option>
                                <option value="3">Surat Pertanggungjawaban</option>
                                <option value="4">Pasal Pasal</option>
                                <option value="5">PKWT</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('file_nip', array(), '', 'id="file_nip" class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Lokasi Kerja</label>
                            <?php echo form_dropdown('file_cabang', $cabang, '', 'id="file_cabang" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tgl Mulai Perjanjian</label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_mulai" id="tgl_mulai" />
                    </div>
                    <div class="form-group">
                        <label>Tgl Akhir Perjanjian</label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="simpan();" class="btn btn-primary">Download</button>
    </div>
</div>
