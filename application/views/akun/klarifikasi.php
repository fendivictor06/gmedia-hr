<div class="page-content">
    <div class="breadcrumbs"><h1><?php echo $header; ?></h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo form_periode(2); ?>
                            </div>
                            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('periode',$periode,'','id="periode" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                            <button type="button" class="btn btn-default" id="download"><i class="fa fa-download"></i> Download</button>
                                        </span> 
                                    </div>
                            </div> -->
                        </div>
                    </div>
                </div>

                <div id="myTable"></div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Klarifkasi</h4>
    </div>
    <form id="form_presensi">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_ab" id="id_ab">
                    <input type="hidden" name="id_presensi" id="id_presensi">
                    <input type="hidden" name="nip" id="nip">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control" readonly="readonly" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                    </div>
                    <div class="form-group">
                        <label>Presensi</label>
                            <?php echo form_dropdown('absen', $absen, '', 'class="form-control" id="absen"'); ?>
                    </div>
                    <div id="kuota_cuti"></div>
                    <div class="form-group">
                        <label>Scan Masuk</label>
                            <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Scan Pulang</label>
                            <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="ket" id="ket" rows="5" readonly="readonly"></textarea>
                    </div>
                    <div class="form-group">
                        <label>File Upload</label>
                            <input type="file" name="file_upload" id="file_upload" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Klarifikasi</label>
                            <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div> 

<?php echo isset($footer) ? $footer : ''; ?>
    <script type="text/javascript">
       function load_table() {
            // periode = $("#periode").val();
            var tahun = $("#tahun").val();
            var bulan = $("#bulan").val();
            $.ajax({
                url : "<?php echo base_url('akun/view_'.$tag); ?>",
                data : {
                    "tahun" : tahun,
                    "bulan" : bulan
                },
                beforeSend : function() {
                    $("#tampil").addClass("hidden"), $("#imgload").removeClass("hidden"), $("#unduh").addClass("hidden");
                },
                complete : function(){
                    $("#tampil").removeClass("hidden"), $("#imgload").addClass("hidden"), $("#unduh").removeClass("hidden");
                },
                success : function(data){
                    $("#myTable").html(data);
                    $("#dataTables-example").DataTable({
                        responsive : true
                    });
                }
            });
       }

       $("#unduh").click(function(){
            var tahun = $("#tahun").val();
            var bulan = $("#bulan").val();
            window.location.href="<?php echo base_url('download_excel/laporan_klarifikasi/'.$tag); ?>/"+tahun+"/"+bulan;
       });

       $(document).ready(function(){
            $(".select2").select2();
            <?php  
                $month = date('m');
                // $month = str_replace('0', '', $month);
                $month = (int)$month;

                $year = date('Y');
            ?>

            var year = "<?php echo $year; ?>";
            var month = "<?php echo $month; ?>";

            $("#tahun").val(year).trigger("change");
            $("#bulan").val(month).trigger("change");

            load_table();
       });
       
       $("#tampil").click(function(){
            load_table();
       });

        // view kuota cuti
        $("#absen").change(function(){
            nip = $("#nip").val();
            $.ajax({
                url : "<?php echo base_url('cuti/cek_qt') ; ?>/"+nip,
                success : function(data) {
                    $("#kuota_cuti").html(data);
                }
            });
        });

       function get_id(id) {
            $.ajax({
                url : "<?php echo base_url('klarifikasi/view_edit_klarifikasi'); ?>/"+id,
                dataType : "json",
                success : function(data) {
                    $("#myModal").modal();
                    $("#id").val(data.id);
                    $("#id_ab").val(data.id_ab);
                    $("#id_presensi").val(data.id_presensi);
                    $("#nip").val(data.nip);
                    $("#nama").val(data.nama);
                    $("#tgl").val(data.tanggal);
                    $("#absen").val(data.presensi);
                    $("#scan_masuk").val(data.scan_masuk);
                    $("#scan_pulang").val(data.scan_pulang);
                    $("#ket").val(data.status);
                    $("#klarifikasi").val(data.klarifikasi);
                }
            });
        }

        $("#form_presensi").submit(function(event){
            event.preventDefault();
            var d = new FormData($(this)[0]);
            $.ajax({
                url : "<?php echo base_url('klarifikasi/edit_klarifikasi_process'); ?>",
                type : "post",
                data : d,
                dataType : "json",
                async : false,
                cache : false,
                contentType : false,
                processData : false,
                success : function(data) {
                    if(data.status == true) {
                        load_table(), $("#myModal").modal("toggle");
                    }
                    bootbox.alert(data.message);
                }
            });
            return false;
        });

        function proses(id, val) {
            $.ajax({
                url : "<?php echo base_url('klarifikasi/approval_klarifikasi'); ?>/"+id+"/"+val,
                type : "post",
                success : function() {
                    load_table();
                }
            })
        }

        function approval(id) {
            bootbox.dialog({
                message : "Yakin ingin memproses Klarifikasi Absensi?",
                title : "Proses Klarifikasi Absensi",
                buttons :{
                    success : {
                        label : "Setuju",
                        className : "green",
                        callback : function(){
                            proses(id, "approve");
                            bootbox.alert("Ijin telah disetujui !");
                        }    
                    },
                    danger : {
                        label : "Tolak",
                        className : "red",
                        callback : function(){
                            proses(id, "reject");
                            bootbox.alert("Ijin telah ditolak !");
                        }
                    },
                    main : {
                        label : "Cancel",
                        className : "blue",
                        callback : function(){
                            return true;
                        }
                    }
                }
            });
        }

        function proses_terlambat(id, val) {
            $.ajax({
                url : "<?php echo base_url('klarifikasi/approval_klarifikasi_terlambat'); ?>/"+id+"/"+val,
                type : "post",
                success : function() {
                    load_table();
                }
            })
        }

        function approval_terlambat(id) {
            bootbox.dialog({
                message : "Yakin ingin memproses Klarifikasi Terlambat?",
                title : "Proses Klarifikasi Terlambat",
                buttons :{
                    success : {
                        label : "Setuju",
                        className : "green",
                        callback : function(){
                            proses_terlambat(id, "approve");
                            bootbox.alert("Ijin telah disetujui !");
                        }    
                    },
                    danger : {
                        label : "Tolak",
                        className : "red",
                        callback : function(){
                            proses_terlambat(id, "reject");
                            bootbox.alert("Ijin telah ditolak !");
                        }
                    },
                    main : {
                        label : "Cancel",
                        className : "blue",
                        callback : function(){
                            return true;
                        }
                    }
                }
            });
        }

         function delete_data(id) {
            bootbox.dialog({
                message : "Yakin ingin menghapus Klarifikasi?",
                title : "Hapus Klarifikasi",
                buttons :{
                    success : {
                        label : "Hapus",
                        className : "green",
                        callback : function(){
                            $.ajax({
                                url :  "<?php echo base_url('klarifikasi/delete_klarifikasi'); ?>/"+id,
                                dataType : "json",
                                success : function(data) {
                                    if (data.status == 1) {
                                        load_table();
                                    } 
                                    
                                    bootbox.alert(data.message);
                                },
                                error : function() {
                                    bootbox.alert('Terjadi kesalahan saat menghapus data');
                                }
                            });
                        }    
                    },
                    main : {
                        label : "Cancel",
                        className : "blue",
                        callback : function(){
                            return true;
                        }
                    }
                }
            });
        }
    </script>
<?php echo isset($penutup) ? $penutup : ''; ?>