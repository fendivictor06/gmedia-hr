<div class="page-content">
	<div class="breadcrumbs">
		<h1 class="page-header">Ijin Meninggalkan Kerja</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
                <div class="row">
                    <div class="col-md-12">
                    <?php 
                        $btn = '<button type="button" class="btn btn-primary pull-right"  data-target="#myModal" data-toggle="modal" id="tambah" onclick="clearform();"><i class="fa fa-plus"></i> Tambah Data</button>';
                        echo form_periode(2, $btn);
                    ?>
                    </div>
                </div>
			</div>
			<br>
			<div id="myTable"></div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
        <h4 class="modal-title" id="title">Form Meninggalkan Kerja</h4>
    </div>
    <form id="form_ijin" method="post" enctype="multipart/form-data" action="#">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            		<div class="form-group">
            			<label>Nama</label>
                            <input type="hidden" name="id" id="id" class="blank">
            				<?php echo form_dropdown('nip',$nip,'','id="nip" class="select2" '); ?>
            		</div>
                    <div class="form-group">
                        <label>Approval</label>
                            <?php echo form_dropdown('penyetuju',$penyetuju,'','id="penyetuju" class="select2"'); ?>
                    </div>
            		<div class="form-group">
            			<label>Tanggal</label>
            				<input type="text" class="form-control date-picker blank" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
            		</div>
            		<!-- <div class="form-group">
            			<label>Jumlah Menit</label>
            				<input type="number" class="form-control blank" name="jmljam" id="jmljam">
            		</div> -->
                    <div class="form-group" style="margin-bottom: 0px;">
                        <label>Waktu Mulai</label>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="padding-left: 0px;">
                            <input type="text" name="starttime_hour" class="kosong form-control" id="starttime_hour" maxlength="2">
                        </div>
                        <div class="col-md-6" style="padding-right: 0px;">
                            <input type="text" name="starttime_minute" id="starttime_minute" class="kosong form-control" maxlength="2">
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0px;">
                        <label>Waktu Selesai</label>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="padding-left: 0px;">
                            <input type="text" name="endtime_hour" class="kosong form-control" id="endtime_hour" maxlength="2">
                        </div>
                        <div class="col-md-6" style="padding-right: 0px;">
                            <input type="text" name="endtime_minute" id="endtime_minute" class="kosong form-control" maxlength="2">
                        </div>
                    </div>
            		<div class="form-group">
            			<label>Keterangan</label>
                            <textarea class="form-control blank" name="keterangan" id="keterangan" rows="5"></textarea>
            		</div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
            <!-- <button type="button" onclick="addcuti();" id="cutibtn" class="btn btn-warning">Tambah ke Cuti</button> -->
        </div>
    </form>
</div>