<div class="page-content">
    <div class="breadcrumbs">
        <h1>Pengajuan Karyawan Baru</h1>
    </div>
    <div class="row">
        
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <form id="search">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </form>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary pull-right" style="margin-top:25px;" data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>
                        </div> -->
                        <div class="col-md-12">
                            <?php
                                $btn = '<button type="button" class="btn btn-primary pull-right"  data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>'; 
                                echo form_periode(null, $btn); 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>

    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Pengajuan Karyawan Baru</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_pengajuan_karyawan">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="select2"'); ?>
                    </div>
                    
                    <div class="form-group">
                        <label>Level</label>
                            <?php echo form_dropdown('sal_pos', $sal_pos, '', 'id="sal_pos" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Posisi</label>
                            <!-- <input type="text" class="form-control blank" name="posisi" id="posisi" /> -->
                            <?php echo form_dropdown('posisi', $job, '', 'id="posisi" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengajuan</label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_pengajuan" id="tgl_pengajuan" />
                    </div>
                    <div class="form-group">
                        <label>Jumlah</label>
                            <input type="number" class="form-control blank" name="jumlah" id="jumlah" />
                    </div>
                    <div class="form-group" id="pengajuan">
                        <label>Spesifikasi Khusus</label>
                            <textarea class="form-control blank" name="spesifikasi" id="spesifikasi" rows="6"></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>


<div id="Mdownload" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Download File</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="fdownload">
                    <input type="hidden" name="id_file" id="id_file" class="blank">
                    <div class="form-group">
                        <label>File</label>
                            <select class="form-control" name="file" id="file">
                                <option value="1">File OJT</option>
                                <option value="2">Surat Pernyataan</option>
                                <option value="3">Surat Pertanggungjawaban</option>
                                <option value="4">Pasal Pasal</option>
                                <option value="5">PKWT</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('file_nip', array(), '', 'id="file_nip" class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Lokasi Kerja</label>
                            <?php echo form_dropdown('file_cabang', $cabang, '', 'id="file_cabang" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tgl Mulai Perjanjian</label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_mulai" id="tgl_mulai" />
                    </div>
                    <div class="form-group">
                        <label>Tgl Akhir Perjanjian</label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_akhir" id="tgl_akhir" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="simpan();" class="btn btn-primary">Download</button>
    </div>
</div>