<div class="page-content">
    <div class="breadcrumbs"><h1>Pencatatan Terlambat</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12">
                            <?php echo form_periode(); ?>
                            </div>
                            <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('periode',$periode,'','id="periode" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </div> -->
                        </div>
                    </div>
                </div>

                <div id="myTable"></div>
            </div>
        </div>
    </div>
</div>

<div id="modaldetail" class="modal fade container" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title" id="detailtitle"></h4>
    </div>
    <div class="modal-body">
        <div id="detailresult"></div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
</div>

<div id="klarifikasi_modal" class="modal fade container" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title" id="detailtitle">Klarifikasi Terlambat</h4>
    </div>
    <form id="form_klarifikasi">
        <div class="modal-body">
            <div class="form-group">
                <label>Nama</label>
                <input type="hidden" name="id_terlambat" id="id_terlambat">
                <input type="hidden" name="id_ab" id="id_ab">
                <input type="text" name="nama" id="nama" class="form-control" readonly="readonly">
            </div>
            <div class="form-group">
                <label>Tanggal</label>
                <input type="text" name="tgl" id="tgl" class="form-control">
            </div>
            <div class="form-group">
                <label>Scan Masuk</label>
                <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24">
            </div>
            <div class="form-group">
                <label>File Upload</label>
                <input type="file" name="file_upload" id="file_upload" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
    <script type="text/javascript">
        function load_table(){
            var tahun = $("#tahun").val();
            var bulan = $("#bulan").val();
            $.ajax({
                url : "view_terlambat",
                data : {
                    "tahun" : tahun,
                    "bulan" : bulan
                },
                beforeSend : function(){
                    $("#imgload").removeClass("hidden"), $("#tampil").addClass("hidden"), $("#download").addClass("hidden");
                },
                complete : function(){
                    $("#imgload").addClass("hidden"), $("#tampil").removeClass("hidden"), $("#download").removeClass("hidden");
                },
                success : function(data) {
                    $("#myTable").html(data);
                    $("#dataTables-example").DataTable({
                        responsive : true
                    });
                }
            });
        }

        $("#tampil").click(function(){
            load_table();
        });

        $(document).ready(function(){
            load_table();
            $(".select2").select2();
        });

        function show(nip, periode) {
            $.ajax({
                url : "<?php echo base_url('absensi/show_detail_telat'); ?>/"+nip+"/"+periode,
                success : function(data) {
                    $("#modaldetail").modal();
                    $("#detailtitle").html("Detail Keterlambatan Periode "+ $("#qd_id option:selected").text());
                    $("#detailresult").html(data);
                    $("#table_detail").DataTable({
                        responsive : true
                    });
                }
            })
        }

        function klarifikasi(id) {
            $.ajax({
                url : "../terlambat/get_id_terlambat/"+id,
                dataType : "json",
                success : function(data) {
                    $("#klarifikasi_modal").modal();
                    $("#id_terlambat").val(data.id);
                    $("#id_ab").val(data.id_ab);
                    $("#nama").val(data.nama);
                    $("#tgl").val(data.tanggal);
                    $("#scan_masuk").val(data.masuk);
                }
            });
        }

        function download(nip, periode) {
            window.location.href="<?php echo base_url('download_excel/detail_terlambat'); ?>/"+nip+"/"+periode;
        }
    </script>
<?php echo isset($penutup) ? $penutup : ''; ?>