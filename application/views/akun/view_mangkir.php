<div class="page-content">
    <div class="breadcrumbs"><h1>Mangkir</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-title">
                <span class="caption-subject font-dark bold uppercase">Mangkir Tanggal <?php echo date('d F Y', strtotime("-2 day", strtotime(date('d F Y')))).' s/d '.date('d F Y', strtotime("-1 day", strtotime(date('d F Y')))); ?></span>
            </div>
            <div class="portlet-body"> <div id="table_mangkir"></div> </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Klarifkasi Absensi</h4>
    </div>
    <form id="form_presensi">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_ab" id="id_ab">
                    <input type="hidden" name="nip" id="nip">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control" readonly="readonly" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                    </div>
                    <div class="form-group">
                        <label>Sakit/Ijin/Mangkir</label>
                            <?php echo form_dropdown('absen', $absen, '', 'class="form-control" id="absen"'); ?>
                    </div>
                    <div id="kuota_cuti"></div>
                    <div class="form-group">
                        <label>Scan Masuk</label>
                            <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Scan Pulang</label>
                            <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="ket" id="ket" rows="5" readonly="readonly"></textarea>
                    </div>
                    <div class="form-group">
                        <label>File Upload</label>
                            <input type="file" name="file_upload" id="file_upload" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Klarifikasi</label>
                            <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div> 

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    function table_mangkir() {
        $.ajax({
            url : "<?php echo base_url('main/view_mangkir'); ?>",
            success : function(data) {
                $("#table_mangkir").html(data);
                $("#mangkir").DataTable({
                    responsive : true
                });
            }
        })
    }

    table_mangkir();

    // show modal klarifikasi absensi
    function get_id(id){
        $.ajax({
            url : "<?php echo base_url('absensi/presensi_id'); ?>",
            type : "GET",
            data : {"id" : id },
            dataType : "JSON",
            success : function(dat){
                $("#myModal").modal(), $("#nama").val(dat.nama), $("#id").val(dat.id), $("#nip").val(dat.nip), $("#tgl").val(dat.tanggal), $("#klarifikasi").val(dat.klarifikasi), $("#ket").val(dat.ket), $("#scan_masuk").val(dat.jam_masuk), $("#scan_pulang").val(dat.jam_pulang), $("#id_ab").val(dat.id_ab), $("#nip").val(dat.nip);
                $("#nama").val(dat.nama), $("#kuota_cuti").html("");

                (dat.sakit == 1) ? $("#absen").val("sakit") : ((dat.ijin == 1) ? $("#absen").val("ijin") : ((dat.mangkir == 1) ? $("#absen").val("mangkir") : $("#absen").val("")));

            }
        });
    }

    // submit presensi
    $("#form_presensi").submit(function(event){
        event.preventDefault();
        var d = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('absensi/presensi_process'); ?>",
            type : "post",
            data : d,
            dataType : "json",
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            success : function(data) {
                if(data.status == true) {
                    load_table(), $("#myModal").modal("toggle");
                }
                bootbox.alert(data.message);
            }
        });
        return false;
    });

    <?php echo $this->Main_Model->timepicker(); ?>
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>