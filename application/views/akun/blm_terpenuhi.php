<div class="page-content">
    <div class="breadcrumbs">
        <h1>Rekap Pengajuan Belum Terpenuhi</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>
<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$.ajax({
			url : "<?php echo base_url('pengajuan_karyawan/view_cek_pengajuan'); ?>",
			success : function(data){
				$("#myTable").html(data);
				$("#dataTables-example").DataTable({
                    responsive: true,
                    // "paging": false,
                    "dom": "Bfrtip",
			        "buttons": [
			            "copy", "csv", "excel", "pdf"
			        ]
                });
                $(".buttons-html5").addClass("btn btn-success").css("margin-right","2px");
			}
		});
	});
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>