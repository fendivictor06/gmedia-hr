<div class="page-content">
    <div class="breadcrumbs">
        <h1>Pengajuan Karyawan Promosi</h1>
    </div>
    <div class="row">
        
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php  
                                $btn = '<button type="button" class="btn btn-primary pull-right" data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>';
                                echo form_periode(null, $btn);
                            ?>
                        </div>
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <form id="search">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </form>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary pull-right" style="margin-top:25px;" data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>
                        </div> -->
                    </div>
                </div>
            </div> <!-- <br> <br> -->
            <div id="myTable"></div>
        </div>

    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Pengajuan Karyawan Promosi</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_pengajuan_karyawan">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2"'); ?>
                    </div>
                    <!-- <div class="form-group">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="select2"'); ?>
                    </div>
                     -->
                    <div class="form-group">
                        <label>Level</label>
                            <?php echo form_dropdown('sal_pos', $sal_pos, '', 'id="sal_pos" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Posisi</label>
                            <?php echo form_dropdown('posisi', $job, '', 'id="posisi" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengajuan</label>
                            <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_pengajuan" id="tgl_pengajuan" />
                    </div>
                    <div class="form-group" id="pengajuan">
                        <label>Keterangan</label>
                            <textarea class="form-control blank" name="keterangan" id="keterangan" rows="6"></textarea>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".select2").select2();
		<?php echo $this->Main_Model->default_datepicker(); ?>
	});

	function reset() {
		$(".blank").val("");
		$(".select2").val("").trigger("change");
	}

	function load_table() {
		// periode = $("#qd_id").val();
        var tahun = $("#tahun").val();
        var bulan = $("#bulan").val();
		$.ajax({
			url : "<?php echo base_url('pengajuan_karyawan/view_pengajuan/promosi'); ?>",
            data : {
                "tahun" : tahun,
                "bulan" : bulan
            },
			beforeSend : function(){
				$("#tampil").addClass("hidden");
				$("#imgload").removeClass("hidden");
			},
			complete : function(){
				$("#tampil").removeClass("hidden");
				$("#imgload").addClass("hidden");
			},
			success : function(data){
				$("#myTable").html(data);
				$("#dataTables-example").DataTable({
                    responsive: true
           //          "paging": false,
           //          "dom": "Bfrtip",
			        // "buttons": [
			        //     "copy", "csv", "excel", "pdf"
			        // ]
                });
                // $(".buttons-html5").addClass("btn btn-success").css("margin-right","2px");
			}
		});
	}
	load_table();

    $("#tampil").click(function(){
        load_table();
    });

	<?php echo 
			$this->Main_Model->post_data(base_url('pengajuan_karyawan/add_pengajuan/promosi'),'save()','$("#form_pengajuan_karyawan").serialize()','
				 	if(data.status == true) {
				 		load_table();
				 		reset();
				 	} 
				 	bootbox.alert(data.message);')

				 .$this->Main_Model->get_data(base_url('pengajuan_karyawan/id_pengajuan'),'get_id(id)','
				 	$("#myModal").modal();
				 	$("#posisi").val(data.posisi).trigger("change");
				 	$("#tgl_pengajuan").val(data.tanggal_pengajuan);
				 	$("#cabang").val(data.id_cabang).trigger("change");
				 	$("#sal_pos").val(data.sal_pos).trigger("change");
				 	$("#nip").val(data.nip).trigger("change");
				 	$("#id").val(data.id);
				 	$("#keterangan").val(data.keterangan);')

				 .$this->Main_Model->default_delete_data(base_url('pengajuan_karyawan/delete_pengajuan')); ?>
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>