<div class="page-content">
    <div class="breadcrumbs">
    	<h1>Data Karyawan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body"> 
            	<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                <button class="btn btn-primary" id="btn-download"><i class="fa fa-download"></i> Download</button>
                <br> <br>
            	<div id="data-karyawan-aktif"></div>
             </div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
	function load_aktif() {
        $.ajax({
            url : "<?php echo base_url('akun/view_karyawan_aktif'); ?>",
            success : function(data) {
                $('#data-karyawan-aktif').html(data);
                $('#active').DataTable({
                    responsive : true
                });
            }
        });
    }

    $("#btn-download").click(function(){
        window.location.href="<?php echo base_url('download_excel/download_karyawan_aktif'); ?>";
    });

    $(document).ready(function(){
        load_aktif();
    });
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>