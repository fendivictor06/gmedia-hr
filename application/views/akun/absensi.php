<div class="page-content">
	<div class="breadcrumbs">
		<h1>Absensi & Presensi Karyawan</h1>
	</div>
	<div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                    <div class="row">
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Periode</label>
                                <div class="form-group input-group">
                                    <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                    <span class="input-group-btn">
                                        <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                        <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                    </span> 
                                </div>
                        </div> -->
                        <div class="col-md-12">
                            <?php // echo form_periode(2); ?>
                            <form class="form-inline" role="form" id="form_periode">
                                <div class="form-group col-md-2" style="padding-left:0px;">
                                    <input type="text" name="tgl_awal" id="tgl_awal" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                                </div>
                                <div class="form-group col-md-2" style="padding-left:0px;">
                                    <input type="text" name="tgl_akhir" id="tgl_akhir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                                </div>
                                <button type="button" id="tampil" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                                <button type="button" id="unduh" class="btn btn-default"><i class="fa fa-download"></i> Download Absensi</button>
                                <button type="button" id="unduh_presensi" class="btn btn-default"><i class="fa fa-download"></i> Download Presensi</button>
                                <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                            </form>
                        </div>
                    </div>
                    <br>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Presensi</h4>
    </div>
    <form method="post" action="#" id="form_presensi">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_ab" id="id_ab">
                    <input type="hidden" name="nip" id="nip">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control" data-date-format="dd/mm/yyyy" name="tgl" id="tgl" readonly="readonly">
                    </div>
                    <div class="form-group">
                        <label>Sakit/Ijin/Mangkir</label>
                            <?php echo form_dropdown('absen', $absen, '', 'class="form-control" id="absen"'); ?>
                    </div>
                    <div id="kuota_cuti"></div>
                    <div class="form-group">
                        <label>Scan Masuk</label>
                            <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Scan Pulang</label>
                            <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="ket" id="ket" rows="5" readonly="readonly"></textarea>
                    </div>
                    <div class="form-group">
                        <label>File Upload</label>
                            <input type="file" name="file_upload" id="file_upload" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Klarifikasi</label>
                            <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>