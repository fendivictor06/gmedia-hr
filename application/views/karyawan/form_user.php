<div class="page-content">
    <div class="breadcrumbs">
        <h1>Form User</h1>
    </div>
    <div class="row">
        <div class="portlet light col-md-6">
            <div class="portlet-body">
                <form id="user">
                    <div class="form-group">
                        <label>NIP</label>
                        <input type="text" name="nip" class="form-control" value="<?php echo isset($nip) ? $nip : ''; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="nama" class="form-control" value="<?php echo isset($nama) ? $nama : ''; ?>" readonly>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control">
                    </div>
                    <div class="form-group">
                        <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                        <button class="btn btn-primary" id="simpan">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>