<div class="page-content">
	<div class="breadcrumbs">
		<h1>Karyawan</h1>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div class="form-group">
				<label>Cabang / Lokasi</label>
				<?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="form-control"'); ?>
			</div>
		</div>
		<!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<div id="option"></div>
		</div>	 -->
	</div>
	<div class="row">
		<div class="col-md-6">
			<form class="form-inline">
				<div class="form-group">
					<select class="form-control" name="pilihan" id="pilihan">
						<option value="1">Karyawan Aktif</option>
						<option value="0">Karyawan Resign</option>
						<!-- <option value="2">Karyawan Mutasi</option> -->
					</select>
					<!-- <button class="btn btn-primary" type="button" onclick="cari();" id="tampil">Tampilkan</button> -->
					<!-- <button class="btn green-meadow" type="button" onclick="download(1)" id="download"><i class="fa fa-download"></i> Karyawan Active</button>
					<button class="btn red-sunglo" type="button" onclick="download(0)" id="download_2"><i class="fa fa-download"></i> Karyawan Resign</button> -->
					<button class="btn blue-madison" type="button" onclick="download()" id="download_3"><i class="fa fa-download"></i> Download </button>
					<img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
				</div>
			</form>
			
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<br>
			<!-- <div id="myTable"></div> -->
			<ul class="nav nav-tabs">
				<li class="active"> <a href="#tab_1_1" data-toggle="tab"> Karyawan Aktif </a> </li>
				<li> <a href="#tab_1_2" data-toggle="tab"> Karyawan Resign </a> </li>
				<!-- <li> <a href="#tab_1_3" data-toggle="tab"> Karyawan Mutasi </a> </li> -->
			</ul>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="tab_1_1">
					<h3>Data Karyawan Aktif</h3>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="active">
						<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Posisi</th>
								<th>Cabang</th>
								<th>Tanggal Masuk</th>
								<th>Masa Kerja</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
				<div class="tab-pane fade" id="tab_1_2">
					<h3>Data Karyawan Resign</h3>
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="resign">
						<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Posisi</th>
								<th>Cabang</th>
								<th>Tanggal Masuk</th>
								<th>Tanggal Resign</th>
								<th>Masa Kerja</th>
								<th width="15%">Action</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>	
			</div>
		</div>
	</div>
</div>

	<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
		<form action="#" id="form_resign">
			<div class="modal-header">
				<h4 class="modal-title">Form Resign</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<label>Tanggal Resign</label>
							<input type="hidden" name="nip" id="nip">
							<input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl" id="tgl" required="required">
						</div>
						<div class="form-group">
							<label>Alasan Resign</label>
							<textarea class="form-control" name="alasan" id="alasan" required="required"></textarea>
						</div>
						<div class="form-group">
							<label>Rehire / Do Not Rehire</label>
							<select class="form-control" name="rehire" id="rehire" required="required">
								<option value="Rehire">Rehire</option>
								<option value="Do Not Rehire">Do Not Rehire</option>
							</select>
						</div>
						<div class="form-group">
							<label>Jenis Resign</label>
							<select class="form-control" name="tipe_resign" id="tipe_resign" required="required">
								<option value="RESIGN">Resign Kemauan Sendiri</option>
								<option value="PHK">PHK</option>
								<option value="TIDAK DIPERPANJANG">Kontrak tidak diperpanjang</option>
							</select>
						</div>
						<div class="form-group">
							<label>File Upload</label>
							<input type="file" name="file_upload" id="file_upload" class="form-control">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</form>
	</div>