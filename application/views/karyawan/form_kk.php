<div class="page-content">
	<div class="breadcrumbs">
		<h1>Form Kartu Keluarga</h1>
	</div>
	<div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <table class="margin-bottom-20">
                    <tr>
                        <td>NIP</td>
                        <td width="20px"> : </td>
                        <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td>
                    </tr>
                    <tr>
                        <td>Nama</td>
                        <td> : </td>
                        <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td>
                    </tr>
                    <tr>
                        <td>No KK</td>
                        <td> : </td>
                        <td><span id="kk">-</span></td>
                    </tr>
                </table>
                <?php 
                    // echo "zip: ", extension_loaded('zip') ? 'OK' : 'MISSING', '<br>';
                    $nip = isset($kary->nip) ? $kary->nip : '';
                    $q = $this->Karyawan_Model->view_kk($nip);
                    if($q)
                    {
                        echo '  <div class="table-toolbar">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#ModalTambah">
                                                    <i class="fa fa-plus"></i> Tambah Anggota
                                                </button>
                                                <button type="button" class="btn btn-warning" onclick="edit_kk('.$kary->nip.');">
                                                    <i class="fa fa-plus"></i> Edit KK
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                ';
                    }
                    else
                    {
                        echo '  <div class="table-toolbar">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-plus"></i> Buat KK
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>';
                    }
                 ?>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Kartu Keluarga</h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger hidden" id="warning1">Please Complete this form!</div>
        <div class="alert alert-success hidden" id="success1">Success!</div>
        <form id="formKK">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="nip" id="nip" value="<?php echo isset($kary->nip)?$kary->nip:''; ?>">
                    <div class="form-group">
                        <label>NO KK</label>
                            <input type="text" class="form-control" name="no_kk" id="no_kk">
                    </div>

                    <div class="form-group status">
                        <label>Status</label>
                            <?php echo form_dropdown('status',$status,'','id="status" class="form-control"'); ?>
                    </div>

                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="process_kk();" class="btn btn-primary">Simpan</button>
    </div>
</div>


<div id="ModalTambah" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Kartu Keluarga</h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger hidden" id="warning">Please Complete this form!</div>
        <div class="alert alert-success hidden" id="success">Success!</div>
        <div class="row">
            <form id="add_kk">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="nip2" id="nip2" value="<?php echo isset($kary->nip)?$kary->nip:''; ?>">
                <div class="form-group">
                    <label>NIK*</label>
                        <input type="text" class="form-control" name="nik" id="nik">
                </div>
                <div class="form-group">
                    <label>Nama*</label>
                        <input type="text" class="form-control" name="nama" id="nama">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin*</label>
                        <?php echo form_dropdown('jk',$jk,'','id="jk" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>Status*</label>
                        <?php echo form_dropdown('status2',$status,'','id="status2" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir</label>
                        <input type="text" class="form-control" name="t_lahir" id="t_lahir">
                </div>
                <div class="form-group">
                    <label>Tanggal lahir*</label>
                        <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                </div>
                <div class="form-group">
                    <label>Alamat*</label>
                        <textarea class="form-control" name="alamat" id="alamat"></textarea>
                </div>
                <div class="form-group">
                    <label>Ikut BPJS KS?</label>
                        <select name="isbpjs" id="isbpjs" class="form-control">
                            <option value="1">Ya</option>
                            <option value="0">Tidak</option>
                        </select>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>