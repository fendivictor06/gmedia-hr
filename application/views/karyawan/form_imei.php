<div class="page-content">
    <div class="breadcrumbs">
        <h1>Imei Karyawan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="form-group">
                    <button type="button" class="btn btn-primary"  id="add_new"> <i class="fa fa-plus"></i> Tambah Data</button>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Imei</h4>
    </div>
    <form id="form_imei">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>Nama</label>
                            <?php echo form_dropdown('nip', $nip, '', 'id="nip" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Imei</label>
                            <input type="text" name="imei" id="imei" class="form-control">
                            <span class="help-inline">tekan *#06# untuk melihat imei di handphone.</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        load_table();
        $(".date-picker").datepicker({
            "setDate": new Date(),
            "autoclose": true
        });

        $(".select2").select2();
    });

    function clearform() {
        document.getElementById('form_imei').reset();
        $("#id").val('');
        $("#nip").val('').trigger('change');
    }

    $("#add_new").click(function(){
        clearform(), $("#myModal").modal();
    });

    function load_table() {
        $.ajax({
            url : "<?php echo base_url('imei/view_imei'); ?>",
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_imei").DataTable({
                    responsive : true,
                    stateSave : true
                });
            }
        });
    }

    $("#form_imei").submit(function(event){
        event.preventDefault();
        formData = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('imei/process_imei'); ?>",
            type : "post",
            data : formData,
            async : false,
            cache : false,
            dataType : "json",
            contentType : false,
            processData : false,
            success : function(data) {
                if(data.status == true) {
                    load_table(), clearform();
                    $("#myModal").modal("toggle");
                    toastr.success(data.message);
                } else {
                    toastr.warning(data.message);
                }
            }
        });
        return false;
    });

    function get_id(id) {
        $.ajax({
            url : "<?php echo base_url('imei/id_imei'); ?>/"+id,
            dataType : 'json',
            success : function(data) {
                $("#id").val(data.id);
                $("#nip").val(data.nip).trigger('change');
                $("#imei").val(data.imei);
                $("#myModal").modal();
            }
        });
    }

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "<?php echo base_url('imei/delete_imei'); ?>/"+id,
                            dataType : "json",
                            success : function(data){
                                if(data.status == true) {
                                    load_table();
                                }
                                bootbox.alert(data.message);
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        });
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>