<div class="page-content">
	<div class="breadcrumbs">
		<h1>Form Karyawan</h1>
	</div>
	<div class="row">
		<div class="alert alert-danger hidden" id="warning">
            Please Complete this form!
        </div>
        <div class="alert alert-success hidden" id="success">
            Success!
        </div>
		<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
			<div class="form-group form-md-line-input">
				<label>Nama*</label>
					<input type="text" class="form-control" name="nama" id="nama">
			</div>
			<div class="form-group form-md-line-input">
				<label>Jenis Kelamin*</label>
					<?php echo form_dropdown('gend', $gend, isset($default['gend'])?$default['gend']:'', 'class="form-control" id="gend"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Pendidikan*</label>
					<?php echo form_dropdown('pend', $pend, isset($default['pend'])?$default['pend']:'', 'class="form-control" id="pend"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Level*</label>
					<?php echo form_dropdown('level', $level, isset($default['level'])?$default['level']:'', 'class="form-control" id="level"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Nama Panggilan</label>
					<input type="text" class="form-control" name="nickname" id="nickname">
			</div>
			<div class="form-group form-md-line-input">
				<label>No KTP</label>
					<input type="text" class="form-control" name="ktp" id="ktp">
			</div>
			<div class="form-group form-md-line-input">
				<label>Nama Bank</label>
					<input type="text" class="form-control" name="bank" id="bank">
			</div>
			<div class="form-group form-md-line-input">
				<label>No Rekening</label>
					<input type="text" class="form-control" name="norek" id="norek">
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
				
			<div class="form-group form-md-line-input">
				<label>Atas Nama Rekening</label>
					<input type="text" class="form-control" name="norek_an" id="norek_an">
			</div>
			<div class="form-group form-md-line-input">
				<label>Status Bank</label>
					<?php echo form_dropdown('tfinfo', $tfinfo, isset($default['tfinfo'])?$default['tfinfo']:'', 'class="form-control" id="tfinfo"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Tanggal Lahir*</label>
					<input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl_lahir" id="tgl_lahir">
			</div>
			<div class="form-group form-md-line-input">
				<label>No Telp</label>
					<input type="text" class="form-control" name="telp" id="telp">
			</div>
			<div class="form-group form-md-line-input">
				<label>Alamat</label>
					<input type="text" class="form-control" name="alamat" id="alamat">
			</div>
			<div class="form-group form-md-line-input">
				<label>E-Mail</label>
					<input type="text" class="form-control" name="mail" id="mail">
			</div>
			<div class="form-group form-md-line-input">
				<label>Pin BB</label>
					<input type="text" class="form-control" name="bb" id="bb">
			</div>
		</div>
		<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
			
			<div class="form-group form-md-line-input">
				<label>Tgl Masuk*</label>
					<input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl_masuk" id="tgl_masuk">
			</div>
			<div class="form-group form-md-line-input">
				<label>Status Kawin</label>
					<?php echo form_dropdown('mar_stat', $mar_stat, isset($default['mar_stat'])?$default['mar_stat']:'', 'class="form-control" id="mar_stat"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Keterangan</label>
					<input type="text" class="form-control" name="ket" id="ket">
			</div>
			<div class="form-group form-md-line-input">
				<label>Perusahaan</label>
					<?php echo form_dropdown('idp', $idp, '','class="form-control" id="idp" onchange="idp()"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Lokasi Kerja*</label>
					<div id="lok_krj"></div>
			</div>
			<div class="form-group form-md-line-input">
				<label>Asuransi BPJS Kesehatan</label>
					<?php echo form_dropdown('ikut_asuransi', $ikut_asuransi, isset($default['ikut_asuransi'])?$default['ikut_asuransi']:'', 'class="form-control" id="ikut_asuransi"'); ?>
			</div>
			<div class="form-group form-md-line-input">
				<label>Karyawan Status*</label>
					<?php echo form_dropdown('kary_stat', $kary_stat, isset($default['kary_stat'])?$default['kary_stat']:'', 'class="form-control" id="kary_stat"'); ?>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="form-group">
				<button class="btn btn-primary" type="button" onclick="save();">Simpan</button>
				<button class="btn btn-default" onclick="window.location.href='<?php echo base_url('karyawan/datakaryawan'); ?>'">Cancel</button>
			</div>
		</div>
	</div>
</div>


