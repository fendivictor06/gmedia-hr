<div class="page-content">
	<div class="breadcrumbs">
		<h1 class="page-header">Data Calon Karyawan Baru</h1>
	</div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body"> 
                <?php echo $this->session->flashdata('message'); ?>
            </div>

            <form class="form-inline" id="uploaddata" action="<?php echo base_url('upload/upload_rekrutmen'); ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>File Import</label>
                    <div class="fileinput fileinput-new" data-provides="fileinput" style="margin-bottom: 0px">
                        <!-- <label>File Import</label> -->
                        <div class="input-group">
                            <div class="form-control uneditable-input input-fixed" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename"> </span>
                            </div>
                            <span class="input-group-addon btn default btn-file">
                                <span class="fileinput-new"> Select file </span>
                                <span class="fileinput-exists"> Change </span>
                                <input type="file" name="userfile" id="userfile" required> </span>
                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                    </div>
                </div>
                
                <div class="form-group" style="padding-top: 23px;">
                    <button type="submit" class="btn btn-default"><i class="fa fa-upload"></i> Upload</button>
                </div>
                <div class="form-group pull-right" style="padding-top: 23px;">
                    <button class="btn btn-primary" type="button" onclick="reset();" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Tambah Data </button>
                </div>
            </form>

			<br>
			<div id="myTable"></div>
		</div>
	</div>
</div>

<div id="myModal" class="modal container fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
        <h4 class="modal-title">Form Calon Karyawan Baru</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        		<div class="form-group">
        			<label>Nama <span class="required"> * </span></label>
                        <input type="hidden" name="id" id="id">
                        <input type="text" name="nama" id="nama" class="form-control">
        		</div>
                <div class="form-group">
                    <label>Tanggal Lahir <span class="required"> * </span></label>
                        <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin <span class="required"> * </span></label>
                        <?php echo form_dropdown('jenkel',$jenkel,'','id="jenkel" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>No. HP <span class="required"> * </span></label>
                        <input type="text" class="form-control" name="hp" id="hp">
                </div>
                <div class="form-group">
                    <label>Email</label>
                        <input type="text" class="form-control" name="email" id="email">
                </div>
        		<div class="form-group">
        			<label>Alamat <span class="required"> * </span></label>
        				<textarea name="alamat" id="alamat" class="form-control" rows="5"></textarea>
        		</div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <div class="form-group">
                    <label>Pendidikan Terakhir <span class="required"> * </span></label>
                        <?php echo form_dropdown('pend',$pend,'','id="pend" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>Nama Institusi <span class="required"> * </span></label>
                        <input type="text" class="form-control" name="institusi" id="institusi">
                </div>
                
                <div class="form-group">
                    <label>Pekerjaan yang dilamar <span class="required"> * </span></label>
                        <?php echo form_dropdown('loker',$loker,'','id="loker" class="select2"'); ?>
                </div>
                <div class="form-group">
                    <label>Cabang <span class="required"> * </span></label>
                        <?php echo form_dropdown('cabang', $cabang, '', 'class="select2" id="cabang"'); ?>
                </div>
                <div class="form-group">
                    <label>Status <span class="required"> * </span></label>
                        <?php echo form_dropdown('status', $status, '', 'id="status" class="form-control"'); ?>
                </div>
                <div class="form-group">
                    <label>Pengalaman Kerja Terakhir</label>
                       <textarea name="pengalaman" id="pengalaman" class="form-control" rows="5"></textarea>
                </div>
                
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

<div id="ModalDetail" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Detail Pelamar</h4>
    </div>
    <div class="modal-body">
        <table class="table">
            <tr>
                <td>Nama</td>
                <td>:</td>
                <td id="textNama"></td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td id="textJenisKelamin"></td>
            </tr>
            <tr>
                <td>Tanggal Lahir</td>
                <td>:</td>
                <td id="textTanggalLahir"></td>
            </tr>
            <tr>
                <td>No HP</td>
                <td>:</td>
                <td id="textHP"></td>
            </tr>
            <tr>
                <td>Email</td>
                <td>:</td>
                <td id="textEmail"></td>
            </tr>
            <tr>
                <td>Alamat</td>
                <td>:</td>
                <td id="textAlamat"></td>
            </tr>
            <tr>
                <td>Pendidikan</td>
                <td>:</td>
                <td id="textPendidikan"></td>
            </tr>
            <tr>
                <td>Institusi</td>
                <td>:</td>
                <td id="textInstitusi"></td>
            </tr>
            <tr>
                <td>Pengalaman</td>
                <td>:</td>
                <td id="textPengalaman"></td>
            </tr>
            <tr>
                <td>Posisi yang dilamar</td>
                <td>:</td>
                <td id="textLoker"></td>
            </tr>
            <tr>
                <td>Cabang</td>
                <td>:</td>
                <td id="textCabang"></td>
            </tr>
            <tr>
                <td>Status</td>
                <td>:</td>
                <td id="textStatus"></td>
            </tr>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
    </div>
    
</div>