<div class="page-content">
	<div class="breadcrumbs"><h1>Surat Keputusan</h1></div>
		<div class="portlet light">
			<div class="portlet body">
				
				<?php $kary = $this->Main_Model->kary_nip($this->uri->segment(3)); ?>
                    <table>
                        <tr>
                            <td>NIP</td>
                            <td width="20px"> : </td>
                            <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td> : </td>
                            <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td>
                        </tr>
                    </table>
	                    <br>

	            <form method="POST" action="<?php echo base_url('karyawan/add_sk'); ?>" enctype="multipart/form-data" id="form_sk">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<input type="hidden" name="nip" id="nip" value="<?php echo $this->uri->segment(3); ?>">
							<input type="hidden" name="id_sk" id="id_sk">
							<input type="hidden" name="isaktif" id="isaktif">
							
							<div class="form-group">
								<label>Tipe SK</label>
									<?php echo form_dropdown('tipe_sk',$tipe_sk,'','class="select2" style="width:100%;" id="tipe_sk" onchange="change_tipe();" required'); ?>
							</div>
							<div class="form-group">
								<div id="tipe_value"></div>
							</div>
							<div class="form-group">
								<div id="status_upah"></div>
							</div>
							<div class="form-group">
								<div id="file_upah"></div>
							</div>
							<div class="form-group">
								<label>Tanggal SK</label>
									<input type="text" class="form-control date-picker blank" name="tgl_sk" id="tgl_sk" data-date-format="dd/mm/yyyy" required>
							</div>
							<div class="form-group">
								<label>Upload SK</label>
									<input type="file" class="form-control blank" name="upload_sk" id="upload_sk">
							</div>
							<div class="form-group">
								<label>No SK</label>
									<input type="text" class="form-control blank" name="no_sk" id="no_sk">
							</div>
							<div class="form-group">
								<label>Departemen</label>
									<?php echo form_dropdown('departemen', $departemen, '', 'id="departemen" class="select2" style="width:100%" onchange="cari_cabang(this.value)" required'); ?>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							
							<div class="form-group">
								<label>Cabang</label>
									<select class="select2" style="width:100%" name="cabang" id="cabang" onchange="cari_divisi(this.value)" required></select>
							</div>
							<div class="form-group">
								<label>Divisi</label>
									<select class="select2" style="width:100%" name="divisi" id="divisi" onchange="cari_jabatan(this.value)" required></select>
							</div>
							<div class="form-group">
								<label>Jabatan</label>
									<select class="select2" style="width:100%" name="jab" id="jab" required></select>
							</div>
							<div id="kolom_sales"></div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<button type="button" onclick="window.location.href='<?php echo base_url('karyawan/datakaryawan'); ?>'" class="btn btn-outline dark">Back</button>
		        			<button type="submit" id="btnsimpan" class="btn btn-primary">Simpan</button>
		        			<button type="button" id="btnupdate" class="btn btn-warning hidden">Update</button>
						</div>
					</div>
				</form>

			</div>
				<div id="myTable"></div>
		</div>
</div>
