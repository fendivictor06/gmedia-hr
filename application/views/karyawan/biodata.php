<div class="page-content">
	<div class="breadcrumbs">
		<h1>Biodata Karyawan</h1>
	</div>
	
    <div class="portlet">
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="form-section">Biodata</h4>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>NIP</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="nip"> <?php echo isset($kary->nip)?$kary->nip:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Nama</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="nama"> <?php echo isset($kary->nama)?$kary->nama:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>No KTP</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="ktp"> <?php echo isset($kary->ktp)?$kary->ktp:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Tempat Lahir</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="t_lahir"> <?php echo isset($kary->t_lahir)?$kary->t_lahir:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Tanggal Lahir</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="tgl_lahir"> 
                                <?php 
                                    $tgl_lahir = isset($kary->tgl_lahir)?$kary->tgl_lahir:'';
                                    if($tgl_lahir) {
                                        echo $this->Main_Model->format_tgl($tgl_lahir);
                                    } else {
                                        echo "-";
                                    }
                                ?> 
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Agama</strong></label>
                            <div class="col-md-12">
                                <p class="form-control-static" data-display="agama"><?php echo isset($kary->agama)?$kary->agama:'-'; ?></p>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Jenis Kelamin</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="gend"> 
                                <?php 
                                    $gend = isset($kary->gend)?$kary->gend:'-';
                                    if($gend=='L') {
                                        $jk="Laki-laki";
                                    } else {
                                        $jk="Perempuan";
                                    }
                                    echo $jk;
                                ?> 
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Golongan Darah</strong></label>
                            <div class="col-md-12">
                                <p class="form-control-static" data-display="golongan_darah"><?php echo isset($kary->golongan_darah)?$kary->golongan_darah:'-'; ?></p>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>No Telp</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="telp"> <?php echo isset($kary->telp)?$kary->telp:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Email</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="email"> <?php echo isset($kary->mail)?$kary->mail:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Alamat</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="alamat"> <?php echo isset($kary->alamat)?$kary->alamat:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Emergency Contact</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="emergency"> 
                                <?php 
                                        if(isset($kary->telp_emergency)) {
                                            $telp = $kary->telp_emergency;
                                            ($kary->contact_emergency) ? $telp .= ' ('.$kary->contact_emergency.')' : $telp = $telp;
                                        } else {
                                            $telp = '-';
                                        }

                                        echo isset($telp)?$telp : '-'; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Status Marital</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="mar_stat"> 
                                <?php 

                                    $mar_stat = isset($kary->mar_stat)?$kary->mar_stat:'-'; 
                                    
                                    echo $mar_stat;
                                ?> 
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Surat Komitmen</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="surat_komitmen">
                                <?php echo isset($kary->surat_komitmen) ? $kary->surat_komitmen : '-'; ?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <h4 class="form-section">Akun Bank</h4>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Nama Bank</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="nama_bank"> <?php echo isset($kary->bank)?$kary->bank:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>No Rekening</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="no_rek"> <?php echo isset($kary->norek)?$kary->norek:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Nama Akun</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="nama_akun"> <?php echo isset($kary->norek_an)?$kary->norek_an:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Status Rekening</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="tfinfo"> <?php echo isset($kary->tfinfo)?$kary->tfinfo:'-'; ?> </p>
                        </div>
                    </div>

                    <h4 class="form-section">Pendidikan</h4>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Pendidikan</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="pend"> <?php echo isset($kary->pend)?$kary->pend:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Nama Sekolah/Universitas</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="nama_sklh"> <?php echo isset($kary->nama_sklh)?$kary->nama_sklh:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Jurusan</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="jurusan"> <?php echo isset($kary->jurusan)?$kary->jurusan:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Tahun Lulus</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="th_lulus"> <?php echo isset($kary->th_lulus)?$kary->th_lulus:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>No Ijazah</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="no_ijazah"> <?php echo isset($kary->no_ijazah)?$kary->no_ijazah:'-'; ?> </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>Status Ijazah</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="status_ijazah"> <?php echo isset($kary->status_ijazah)?$kary->status_ijazah:'-'; ?> </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <h4 class="form-section">Asuransi & Pajak</h4>
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>BPJS Kesehatan</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="ikut_asuransi"> 
                                <?php 
                                    $bpjs = isset($kary->ikut_asuransi)?$kary->ikut_asuransi:'-'; 
                                    if($bpjs=='1') {
                                        $st = 'Ya';
                                    } else {
                                        $st = 'Tidak';
                                    }

                                    echo $st;
                                ?> 
                            </p>
                        </div>
                    </div>
                    <?php
                    $alasan_bpjsks = isset($kary->alasan_bpjsks) ? $kary->alasan_bpjsks : '';
                    if ( $alasan_bpjsks != '') 
                                echo '  <div class="form-group">
                                            <label class="control-label col-md-12"><strong>Alasan</strong></label>
                                            <div class="col-md-12">
                                                <p class="form-control-static" data-display="alasan_bpjsks"> '.$alasan_bpjsks.' </p>
                                            </div>
                                        </div>';
                     ?>
                    
                    <div class="form-group">
                        <label class="control-label col-md-12"><strong>No NPWP</strong></label>
                        <div class="col-md-12">
                            <p class="form-control-static" data-display="npwp"> <?php echo isset($kary->npwp)?$kary->npwp:'-'; ?> </p>
                        </div>
                    </div>
                    <h4 class="form-section">History Penyakit</h4>
                    <?php 
                        if(isset($kary->th1) == "" && isset($kary->penyakit1) == "" && isset($kary->th2) == "" && isset($kary->penyakit2) == "" && isset($kary->th3) == "" && isset($kary->penyakit3) == "") {
                            echo "Tidak ada history penyakit";
                        } else {
                            ($kary->th1 && $kary->penyakit1) ? $p1 = $kary->th1.', '.$kary->penyakit1 : $p1 = '';
                            echo $p1.'<br>';
                            ($kary->th2 && $kary->penyakit2) ? $p2 = $kary->th2.', '.$kary->penyakit2 : $p2 = '';
                            echo $p2.'<br>';
                            ($kary->th3 && $kary->penyakit3) ? $p3 = $kary->th3.', '.$kary->penyakit3 : $p3 = '';
                            echo $p3.'<br>'; 
                        }
                    ?>
                </div>
                <div class="col-md-3">
                    <h4 class="form-section">Data Pekerjaan</h4>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>PIN / Nomor Absen</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->pin) ? $kary->pin : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Kantor GMedia</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->cabang) ? $kary->cabang : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Divisi</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->divisi) ? $kary->divisi : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Bagian</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->jab) ? $kary->jab : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Jabatan</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->sal_pos) ? $kary->sal_pos : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Departemen</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->departemen) ? $kary->departemen : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Status Karyawan</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->kary_stat) ? $kary->kary_stat : ''; ?>
                            </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-12">
                            <strong>Tanggal Masuk</strong>
                        </label>
                        <div class="col-md-12">
                            <p class="form-control-static">
                                <?php echo isset($kary->tgl_masuk) ? $this->Main_Model->format_tgl($kary->tgl_masuk) : ''; ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
