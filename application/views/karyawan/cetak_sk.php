<html>
<head>
	<title>Cetak SK</title>
	<style type="text/css">
		body{
			font-family: 'Arial';
		}
	</style>
</head>
<body onload="print();">
	<table border='0' width="100%">
		<tr>
			<td><img src="<?php echo base_url('assets/rekrutmen/images/logo.png') ?>"></td>
		</tr>
		<tr>
			<td align="center">
				<h3 style="line-height:70%">SURAT KEPUTUSAN</h3>
				<h3 style="line-height:70%">POH MANAGER HR</h3>
				<h3 style="line-height:70%">CV. PERKASA TELKOMSELINDO</h3>
				<h3 style="line-height:70%">NOMOR : SK. 010/HP20000/I/2015</h3>
				<h3>TENTANG</h3>
				<h3>PENERIMAAN CALON PEGAWAI</h3>
				<h3>POH MANAGER HR</h3>
				<u><h3 style="line-height:1%">CV. PERKASA TELKOMSELINDO</h3></u>
			</td>
		</tr>
		<tr>
			<td>
				<table border='0'>
					<tr>
						<td>Menimbang</td>
						<td>:</td>
						<td>a.</td>
						<td><p align="justify">bahwa sejalan dengan Visi dan Misi perusahaan yang sudah ditetapkan dalam Keputusan Direksi Nomor 001//DIR-000/IV/2013</p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>b.</td>
						<td><p align="justify">bahwa guna mendukung komitmen sebagaimana dimaksud butir a  di atas, diperlukan kordinasi semua pengelolaan bisnis yang terintegrasi dalam satu unit organisasi;</p></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>c.</td>
						<td><p align="justify">bahwa sebagai tindak lanjut butir a dan b telah dilaksanakan rekrutasi dan placement test bagi para kandidat calon pegawai</p></td>
					</tr>
					<tr>
						<td>Mengingat</td>
						<td>:</td>
						<td></td>
						<td><p align="justify">Penetapan peraturan perusahaan No. 001/DIR-000/VI/2013 tentang STRUKTUR ORGANISASI CV. PERKASA TELKOMSELINDO</p></td>
					</tr>
					<tr>
						<td>Memperhatikan</td>
						<td>:</td>
						<td></td>
						<td><p align="justify">Kebutuhan pengisian posisi "Staff Collector"</p></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<br>
				<h3 style="line-height:70%">MEMUTUSKAN:</h3>
				<br>
			</td>
		</tr>
		<tr>
			<td>
				<table border='0'>
					<tr>
						<td>Menetapkan</td>
						<td>:</td>
						<td align="justify"><b>POH MANAGER HUMAN RESOURCE CV. PERKASA TELKOMSELINDO TENTANG PENERIMAAN CALON PEGAWAI</b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border='0'>
					<tr>
						<td>Kesatu</td>
						<td>:</td>
						<td>Menetapkan Posisi Karyawan</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>
							<table border='1'>
								<tr>
									<td>1.</td>
									<td>
										<table>
											<tr>
												<td>a.</td>
												<td>Nama</td>
											</tr>
											<tr>
												<td>b.</td>
												<td>NIK</td>
											</tr>
										</table>
									<td><table>
											<tr>
												<td><?php echo $sk->nama; ?></td>
											</tr>
											<tr>
												<td><?php echo $sk->nip; ?></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>2.</td>
									<td>
										<table>
											<tr>
												<td>a.</td>
												<td>Terhitung mulai tanggal</td>
											</tr>
											<tr>
												<td>b.</td>
												<td>Jabatan</td>
											</tr>
											<tr>
												<td>c.</td>
												<td>Object ID</td>
											</tr>
											<tr>
												<td>d.</td>
												<td>Band Posisi</td>
											</tr>
											<tr>
												<td>e.</td>
												<td>Lokasi Posisi</td>
											</tr>
											<tr>
												<td>f.</td>
												<td>Lokasi Kerja</td>
											</tr>
										</table>
									</td>
									<td>
										<table>

											<tr>
												<td><?php echo date_format(date_create($sk->tgl),'d F Y'); ?></td>
											</tr>
											<tr>
												<td><?php echo $sk->jab; ?></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><?php echo $sk->posisi; ?></td>
											</tr>
											<tr>
												<?php $q = $this->db->query("select * from lku where id_lku = '$sk->id_lku'")->row(); ?>
												<td><?php echo $q->lku; ?></td>
											</tr>
											<tr>
												<?php $w = $this->db->query("select * from lka where id_lka = '$sk->id_lka'")->row(); ?>
												<td><?php echo $w->lka; ?></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>3.</td>
									<td>
										<table>
											<tr>
												<td>a.</td>
												<td>Take Home Pay</td>
											</tr>
											<tr>
												<td>b.</td>
												<td>Penerimaan 80%</td>
											</tr>
										</table>
									</td>
									<td>
										<table>
											<tr>
												<td><?php echo $this->Main_Model->uang($thp->thp); ?></td>
											</tr>
											<tr>
												<td><?php echo $this->Main_Model->uang($thp->thp*80/100); ?></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>Kedua</td>
						<td>:</td>
						<td align="justify">Memberikan penghasilan dan emulemen lainnya yang berkaitan dengan jabatan tersebut sesuai dengan ketentuan yang berlaku dan administrasi kepegawaian yang bersangkutan berada di HR Remuneration & Benefit.</td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td align="right">
							<table>
								<tr>
									<td>Ditetapkan di </td>
									<td>:</td>
									<td>Semarang</td>
								</tr>
								<tr>
									<td>Pada tanggal</td>
									<td>:</td>
									<td><?php echo date("d F Y"); ?></td>
								</tr>
								<tr>
									<td align="center" colspan="3">POH MANAGER HR</td>
								</tr>
								<tr>
									<td align="center" colspan="3">CV. PERKASA TELKOMSELINDO</td>
								</tr>

								<tr>
									<td align="center" colspan="3"><br><br><br><br>IRMA KURNIA</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="left" colspan='3'>
							<table>
								<tr>
									<td colspan="2">Tembusan</td>
								</tr>
								<tr>
									<td>1.</td>
									<td>VP BDO</td>
								</tr>
								<tr>
									<td>2.</td>
									<td>HR Remuneration & Benefit</td>
								</tr>
								<tr>
									<td>3.</td>
									<td>HR Development & Data Management</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>