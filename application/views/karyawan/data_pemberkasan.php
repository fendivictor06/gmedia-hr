<div class="page-content">
    <div class="breadcrumbs">
        <h1>Pemberkasan Karyawan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <form id="formpemberkasan" method="post" action="<?php echo $action; ?>" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="lamaran" name="lamaran" value="1" <?php ($lamaran == 1) ? $a = 'checked="checked"' : $a = ''; echo $a; ?>> Aplikasi Lamaran Kerja/CV
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_lamaran" id="file_lamaran" class="form-control">
                            <?php if($file_lamaran) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_lamaran; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="ojt" name="ojt" value="1" <?php ($ojt == 1) ? $b = 'checked="checked"' : $b = ''; echo $b; ?>> File OJT
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_ojt" id="file_ojt" class="form-control">
                            <?php if($file_ojt) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_ojt; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="pertanggungjawaban" name="pertanggungjawaban" value="1" <?php ($pertanggungjawaban == 1) ? $c = 'checked="checked"' : $c = ''; echo $c; ?>> Pertanggungjawaban (Surat Komitmen dari Penjamin)
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_pertanggungjawaban" id="file_pertanggungjawaban" class="form-control">
                            <?php if($file_pertanggungjawaban) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_pertanggungjawaban; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="kesanggupan" name="kesanggupan" value="1" <?php ($kesanggupan == 1) ? $d = 'checked="checked"' : $d = ''; echo $d; ?>> Surat Komitmen (Kesanggupan Kerja dari Pelamar Kerja)
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_kesanggupan" id="file_kesanggupan" class="form-control">
                            <?php if($file_kesanggupan) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_kesanggupan; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="ijazah" name="ijazah" value="1" <?php ($ijazah == 1) ? $e = 'checked="checked"' : $e = ''; echo $e; ?>> Surat Serah Terima Ijazah
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_ijazah" id="file_ijazah" class="form-control">
                            <?php if($file_ijazah) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_ijazah; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="upah" name="upah" value="1" <?php ($upah == 1) ? $f = 'checked="checked"' : $f = ''; echo $f; ?>> Kesepakatan Upah
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_upah" id="file_upah" class="form-control">
                            <?php if($file_upah) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_upah; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" id="rekening" name="rekening" value="1" <?php ($rekening == 1) ? $g = 'checked="checked"' : $g = ''; echo $g; ?>> Rekening Mandiri
                                <span></span>
                            </label>
                        </div>
                        <div class="form-group input-group">
                            <input type="file" name="file_rekening" id="file_rekening" class="form-control">
                            <?php if($file_rekening) { ?>
                                <span class="input-group-btn">
                                    <a class="btn btn-default" href="<?php echo base_url('assets/pemberkasan').'/'.$file_rekening; ?>"><i class="fa fa-download"></i> Download</a>
                                </span>
                            <?php } ?>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary" id="simpan">Simpan</button>
                    <a href="<?php echo base_url('karyawan/datakaryawan'); ?>" class="btn btn-default">Batal</a>
                </form>
            </div>
                <div id="myTable"></div>
        </div>
    </div>
</div>