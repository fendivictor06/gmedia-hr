<div class="page-content">
    <div class="row">
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase"> Form Karyawan </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form class="form-horizontal" action="#" onsubmit="save();" id="submit_form" method="POST" enctype="multipart/form-data">
                        <div class="form-wizard">
                            <div class="form-body">
                                <ul class="nav nav-pills nav-justified steps">
                                    <li>
                                        <a href="#tab1" data-toggle="tab" class="step">
                                        <span class="number"> 1 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> Biodata Karyawan </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab" class="step">
                                        <span class="number"> 2 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> Akun Bank </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab" class="step">
                                        <span class="number"> 3 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> Pendidikan Terakhir </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab4" data-toggle="tab" class="step active">
                                        <span class="number"> 4 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> Asuransi & Pajak </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab5" data-toggle="tab" class="step active">
                                        <span class="number"> 5 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> History Penyakit </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab6" data-toggle="tab" class="step active">
                                        <span class="number"> 6 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> Surat Keputusan </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#tab7" data-toggle="tab" class="step">
                                        <span class="number"> 7 </span>
                                        <span class="desc">
                                        <i class="fa fa-check"></i> Confirm Data </span>
                                        </a>
                                    </li>
                                </ul>
                                <div id="bar" class="progress progress-striped" role="progressbar">
                                    <div class="progress-bar progress-bar-success"> </div>
                                </div>
                                <div class="tab-content">
                                    <div class="alert alert-danger display-none">
                                        <button class="close" data-dismiss="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-none">
                                        <button class="close" data-dismiss="alert"></button> Your form validation is successful! 
                                    </div>
                                    <div class="tab-pane active" id="tab1">
                                        <h3 class="block">Biodata Karyawan</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">
                                            Cabang <span class="required">*</span>
                                            </label>
                                            <div class="col-md-4">
                                                <?php
                                                    echo form_dropdown('region', $cabang, '', 'id="region" class="form-control"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">NIP
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" onblur="ceknip()" name="nip" id="nip" value="<?php echo isset($nip_auto) ? $nip_auto : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PIN
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="pin" id="pin" value="<?php echo isset($pin) ? $pin : ''; ?>" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="nama" id="nama" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No KTP
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="ktp" id="ktp" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tempat Lahir
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="t_lahir" id="t_lahir" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Lahir
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control date-picker" data-date-format="dd/mm/yyyy" name="tgl_lahir" id="tgl_lahir" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Agama
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <?php echo form_dropdown('agama', $agama, isset($default['agama'])?$default['agama']:'', 'class="form-control" id="agama"'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        	<label class="control-label col-md-3">Jenis Kelamin
                                        	<span class="required"> * </span>
                                        	</label>
                                        	<div class="col-md-4">
                                        		<?php echo form_dropdown('gend', $gend, isset($default['gend'])?$default['gend']:'', 'class="form-control" id="gend"'); ?>
                                        	</div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Golongan Darah </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="golongan_darah" id="golongan_darah">
                                                    <option value="">Tidak Tahu</option>
                                                    <option value="A">A</option>
                                                    <option value="B">B</option>
                                                    <option value="O">O</option>
                                                    <option value="AB">AB</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Telp
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="telp" id="telp" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alamat
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <textarea id="alamat" name="alamat" class="form-control" rows="5"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Telp Emergency
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="telp_emergency" id="telp_emergency" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Contact Emergency</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="contact_emergency" id="contact_emergency">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        	<label class="control-label col-md-3">Status Kawin
                                        	<span class="required"> * </span>
                                        	</label>
                                        	<div class="col-md-4">
                                        		<?php echo form_dropdown('mar_stat', $mar_stat, isset($default['mar_stat'])?$default['mar_stat']:'', 'class="form-control" id="mar_stat"'); ?>
                                        	</div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="email" id="email" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Surat Komitmen
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="surat_komitmen" id="surat_komitmen">
                                                    <option value="BELUM">BELUM</option>
                                                    <option value="SUDAH">SUDAH</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Upload Surat Komitmen </label>
                                            <div class="col-md-4">
                                                <input type="file" class="form-control" name="file_surat_komitmen" id="file_surat_komitmen" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Salary</label>
                                            <div class="col-md-4">
                                                <input type="number" name="salary" id="salary" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <h3 class="block">Akun Bank</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Bank
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="nama_bank" id="nama_bank" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Rekening
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number" name="no_rek" id="no_rek" />
                                            </div>
                                        </div>
                                       
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Akun
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="nama_akun" id="nama_akun" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Rekening
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <?php echo form_dropdown('tfinfo', $tfinfo, isset($default['tfinfo'])?$default['tfinfo']:'', 'class="form-control" id="tfinfo"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab3">
                                        <h3 class="block">Pendidikan</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jenjang Pendidikan
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <?php echo form_dropdown('pend', $pend, isset($default['pend'])?$default['pend']:'', 'class="form-control" id="pend"'); ?>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Sekolah/Universitas
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="nama_sklh" id="nama_sklh" />
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jurusan
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="jurusan" id="jurusan" />
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tahun Lulus
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="th_lulus" id="th_lulus" />
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Ijazah</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="no_ijazah" id="no_ijazah" />
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Ijazah</label>
                                            <div class="col-md-4">
                                                <select class="form-control" id="status_ijazah" name="status_ijazah">
                                                    <option value="BELUM MENGUMPULKAN">BELUM MENGUMPULKAN</option>
                                                    <option value="SUDAH MENGUMPULKAN">SUDAH MENGUMPULKAN</option>
                                                </select>
                                                <span class="help-block"> </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Upload Ijazah </label>
                                            <div class="col-md-4">
                                                <input type="file" class="form-control" name="file_sertifikat1" id="file_sertifikat1" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Upload Sertifikat </label>
                                            <div class="col-md-4">
                                                <input type="file" class="form-control" name="file_sertifikat2" id="file_sertifikat2" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Upload Sertifikat </label>
                                            <div class="col-md-4">
                                                <input type="file" class="form-control" name="file_sertifikat3" id="file_sertifikat3" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab4">
                                        <h3 class="block">Asuransi & Pajak</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No NPWP
                                            <!-- <span class="required"> * </span> -->
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number" name="npwp" id="npwp" />
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">No BPJS TK
                                            <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number" name="no_bpjstk" id="no_bpjstk" />
                                            </div>
                                        </div> -->
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">Upload Kartu BPJS TK </label>
                                            <div class="col-md-4">
                                                <input type="file" class="form-control" name="file_bpjs_tk" id="file_bpjs_tk" />
                                            </div>
                                        </div> -->
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">No BPJS KS </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number bpjs" name="no_bpjsks" id="no_bpjsks" />
                                            </div>
                                        </div> -->
                                        <!-- <div class="form-group">
                                            <label class="control-label col-md-3">Upload Kartu BPJS KS </label>
                                            <div class="col-md-4">
                                                <input type="file" class="form-control" name="file_bpjs_ks" id="file_bpjs_ks" />
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Ikut BPJS ?</label>
                                            <div class="col-md-4">
                                                <select class="form-control" name="ikut_asuransi" id="ikut_asuransi">
                                                    <option value="1">Ya</option>
                                                    <option value="0">Tidak</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alasan Tidak Ikut BPJS</label>
                                            <div class="col-md-4">
                                                <textarea class="form-control bpjs" name="alasan_bpjsks" id="alasan_bpjsks" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab5">
                                        <h3 class="block">History Penyakit</h3>
                                        <div class="form-group">
                                            <label class="control-label col-md-3"><strong>History Penyakit </strong></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tahun
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number" name="th_hist_penyakit_1" id="th_hist_penyakit_1" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Keterangan
                                            </label>
                                            <div class="col-md-4">
                                                <textarea class="form-control" name="desc_penyakit_1" id="desc_penyakit_1" rows="5"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"><strong>History Penyakit </strong></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tahun
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number" name="th_hist_penyakit_2" id="th_hist_penyakit_2" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Keterangan
                                            </label>
                                            <div class="col-md-4">
                                                <textarea class="form-control" name="desc_penyakit_2" id="desc_penyakit_2" rows="5"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3"><strong>History Penyakit </strong></label>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tahun
                                            </label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control mask_number" name="th_hist_penyakit_3" id="th_hist_penyakit_3" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Keterangan
                                            </label>
                                            <div class="col-md-4">
                                                <textarea class="form-control" name="desc_penyakit_3" id="desc_penyakit_3" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab6">
                                        <h3 class="block">Surat Keputusan</h3>
                                            <input type="hidden" name="id_sk" id="id_sk">
                                            <input type="hidden" name="isaktif" id="isaktif">
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Tipe SK 
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <?php echo form_dropdown('tipe_sk',$tipe_sk,'','class="form-control" id="tipe_sk" onchange="change_tipe();"'); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div id="tipe_value"></div>
                                            </div>

                                            <!-- <div class="form-group">
                                                <label class="control-label col-md-3">Tanggal SK
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control date-picker" name="tgl_sk" id="tgl_sk" data-date-format="dd/mm/yyyy">
                                                </div>
                                            </div> -->

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Status Kepegawaian </label>
                                                <div class="col-md-4">
                                                    <?php echo form_dropdown('status_pegawai', $kary_stat, '', 'id="status_pegawai" class="form-control"'); ?>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Awal Kontrak </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control date-picker" name="tgl_awal_kontrak" id="tgl_awal_kontrak" data-date-format="dd/mm/yyyy">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">Akhir Kontrak </label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control date-picker" name="tgl_akhir_kontrak" id="tgl_akhir_kontrak" data-date-format="dd/mm/yyyy">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">No SK</label>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" name="no_sk" id="no_sk" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Upload SK 
                                                </label>
                                                <div class="col-md-4">
                                                    <input type="file" class="form-control" name="upload_sk" id="upload_sk" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Departemen<span class="required"> * </span></label>
                                                    <div class="col-md-4">
                                                        <?php echo form_dropdown('departemen', $departemen, '', 'id="departemen" class="select2" style="width:100%" onchange="cari_cabang(this.value)"'); ?>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Cabang<span class="required"> * </span></label>
                                                    <div class="col-md-4">
                                                        <select class="select2" style="width:100%" name="cabang" id="cabang" onchange="cari_divisi(this.value)"></select>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Divisi<span class="required"> * </span></label>
                                                    <div class="col-md-4">
                                                        <select class="select2" style="width:100%" name="divisi" id="divisi" onchange="cari_jabatan(this.value)"></select>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Jabatan<span class="required"> * </span></label>
                                                    <div class="col-md-4">
                                                        <select class="select2" style="width:100%" name="jab" id="jab"></select>
                                                    </div>
                                            </div>
                                            <div id="kolom_sales"></div>
                                    </div>
                                    <div class="tab-pane" id="tab7">
                                        <h3 class="block">Confirm</h3>
                                        <h4 class="form-section">Biodata</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">NIP</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="nip"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">PIN</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="pin"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="nama"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No KTP</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="ktp"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tempat Lahir</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="t_lahir"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Lahir</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tgl_lahir"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Agama</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="agama"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jenis Kelamin</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="gend"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Telp</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="telp"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Alamat</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="alamat"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Telp Emergency</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="telp_emergency"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Contact Emergency</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="contact_emergency"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Kawin</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="mar_stat"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Email</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="email"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Surat Komitmen</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="surat_komitmen"> </p>
                                            </div>
                                        </div>
                                        
                                        <h4 class="form-section">Akun Bank</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Bank</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="nama_bank"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Rekening</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="no_rek"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Akun</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="nama_akun"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Rekening</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tfinfo"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Pendidikan</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pendidikan</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="pend"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nama Sekolah/Universitas</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="nama_sklh"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jurusan</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="jurusan"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tahun Lulus</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="th_lulus"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No Ijazah</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="no_ijazah"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Status Ijazah</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="status_ijazah"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Asuransi & Pajak</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">BPJS Kesehatan</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="ikut_asuransi"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No NPWP</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="npwp"> </p>
                                            </div>
                                        </div>
                                        <h4 class="form-section">History Penyakit</h4>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <span class="form-control-static" data-display="th_hist_penyakit_1"> </span> <span class="form-control-static" data-display="desc_penyakit_1"> </span> 
                                                <br>
                                                <span class="form-control-static" data-display="th_hist_penyakit_2"> </span> <span class="form-control-static" data-display="desc_penyakit_2"> </span>  
                                                <br>
                                                <span class="form-control-static" data-display="th_hist_penyakit_3"> </span> <span class="form-control-static" data-display="desc_penyakit_3"> </span> 
                                            </div>
                                        </div>
                                        <h4 class="form-section">Surat Keputusan</h4>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tipe SK</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tipe_sk"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Kerja</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tgl_kerja"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal SK</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tgl_sk"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Awal Magang</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tgl_awal_kontrak"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Tanggal Akhir Magang</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="tgl_akhir_kontrak"> </p>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="control-label col-md-3">No SK</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="no_sk"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Departemen</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="departemen"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Cabang</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="cabang"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Divisi</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="divisi"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Jabatan</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="jab"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">BPKB</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="bpkb"> </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">SIM A/C</label>
                                            <div class="col-md-4">
                                                <p class="form-control-static" data-display="sim"> </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <a href="javascript:;" class="btn default button-previous">
                                        <i class="fa fa-angle-left"></i> Back </a>
                                        <a href="javascript:;" class="btn btn-outline green button-next"> Continue
                                        <i class="fa fa-angle-right"></i>
                                        </a>
                                        <button type="submit" class="btn green button-submit"> Submit
                                        <i class="fa fa-check"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>