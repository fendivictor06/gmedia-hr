<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data User</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" id="add_new" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Data
                                </button>
                                <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form User</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_user">
                    <input type="hidden" name="id" id="id" class="blank">
                    <div class="form-group">
                        <label>Level</label>
                            <select class="select2" name="level" id="level">
                                <option value="Administrator">Head Office</option>
                                <option value="Operator">Branch Office</option>
                            </select>
                    </div>
                    <div class="form-group hidden cabang_group">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang[]', $cabang, '', 'id="cabang" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>

                    <!--<div class="form-group hidden cabang_group">
                        <label>Maninfo Cabang</label>
                            <?php echo form_dropdown('cabang_maninfo[]', $cabang, '', 'id="cabang_maninfo" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>-->

                    <div class="form-group hidden cabang_group">
                        <label>Divisi</label>
                            <?php echo form_dropdown('divisi[]', $divisi, '', 'id="divisi" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>

                    <div class="form-group hidden cabang_group">
                        <label>Pengajuan</label>
                            <?php echo form_dropdown('pengajuan[]', $level, '', 'id="pengajuan" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>

                    <div class="form-group hidden cabang_group">
                        <label>Approval</label>
                            <select class="form-control blank" id="approval" name="approval">
                                <option value="1">Ya</option>
                                <option value="0">Tidak</option>
                            </select>
                    </div>

                    <div class="form-group hidden level_group">
                        <label>Posisi</label>
                            <?php echo form_dropdown('privilege[]', $level, '', 'id="privilege" class="bs-select form-control" multiple data-live-search="true"'); ?>
                    </div>

                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" name="nama" id="nama" class="form-control blank">
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                            <input type="text" name="username" id="username" class="form-control blank">
                    </div>
                    
                    <div class="form-group">
                        <label>Password</label>
                            <input type="password" name="password" id="password" class="form-control blank">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
