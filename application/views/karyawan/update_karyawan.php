<div class="page-content">
	<div class="breadcrumbs">
		<h1>Update Biodata Karyawan by Excel</h1>
	</div>

	<div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="portlet light bordered">
            	<div class="portlet-title"><i class="caption fa fa-file-excel-o font-green-sharp" style="margin-right:5px;"></i> <div class="caption font-green-sharp"> Import Data Karyawan</div></div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 form-inline">
                                <div class="form-group">
									<!-- <label>File</label><br> -->
									<label class="btn btn-primary btn-file">
				    					Choose File <input type="file" style="display: none;">
									</label>
								</div>
								<div class="form-group">
									<button class="btn btn-primary">Import</button>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myTable"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="portlet light bordered">
            	<div class="portlet-title">
            		<i class="caption fa fa-user font-green-sharp" style="margin-right:5px;"></i> <div class="caption font-green-sharp"> Data Karyawan Temporary</div>
            		<div class="pull-right"><button class="btn btn-primary">Update Data Karyawan</button></div>
            	</div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        
                    </div>
                </div>
                <table style="width:100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>
						<tr>
							<th>NIP</th>
							<th>Nama</th>
							<th>JK</th>
							<th>Pendidikan</th>
							<th>Level</th>
							<th>Nama Panggilan</th>
							<th>No KTP</th>
							<th>Bank</th>
							<th>No Rekening</th>
							<th>Nama Rekening</th>
							<th>Transfer Info</th>
							<th>Tgl Lahir</th>
							<th>No Telp</th>
							<th>Alamat</th>
							<th>Mail</th>
							<th>BB</th>
							<th>IM</th>
							<th>Tgl Masuk</th>
							<th>Tgl Resign</th>
							<th>Status</th>
							<th>Keterangan</th>
							<th>Lokasi Asuransi</th>
							<th>Ikut Asuransi</th>
							<th>Karyawan Status</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>
