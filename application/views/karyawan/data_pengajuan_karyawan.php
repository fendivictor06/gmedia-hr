<div class="page-content">
    <div class="breadcrumbs">
        <h1>Pengajuan Karyawan Baru</h1>
    </div>
    <div class="row">
        
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php  
                                $btn = '<button type="button" class="btn btn-primary pull-right"  data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>';
                                echo form_periode(2, $btn);
                            ?>
                        </div>
                        <!-- <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <form id="search">
                                <label>Periode</label>
                                    <div class="form-group input-group">
                                        <?php echo form_dropdown('qd_id',$periode,'','id="qd_id" class="form-control"'); ?>
                                        <span class="input-group-btn">
                                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                            <button id="download" class="btn btn-default" type="button"><i class="fa fa-download"></i> Download</button>
                                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                        </span> 
                                    </div>
                            </form>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <button type="button" class="btn btn-primary pull-right" style="margin-top:25px;" data-target="#myModal" data-toggle="modal" id="tambah" onclick="reset();"><i class="fa fa-plus"></i> Tambah Data</button>
                        </div> -->
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>

    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Pengajuan Karyawan Baru</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_pengajuan_karyawan">
                    <input type="hidden" name="id" id="id" class="blank">
                    <input type="hidden" name="idcabang" id="idcabang" class="blank">
                    <div class="form-group">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="form-control"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Posisi</label>
                            <!-- <input type="text" class="form-control blank" name="posisi" id="posisi" /> -->
                            <?php echo form_dropdown('posisi', $job, '', 'id="posisi" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                            <?php echo form_dropdown('sal_pos', $sal_pos, '', 'id="sal_pos" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengajuan</label>
                            <input type="text" class="form-control blank" data-date-format="dd/mm/yyyy" name="tgl_pengajuan" id="tgl_pengajuan" />
                    </div>
                    <div class="form-group">
                        <label>Jumlah</label>
                            <input type="number" class="form-control blank" name="jumlah" id="jumlah" />
                    </div>
                    <div class="form-group" id="pengajuan">
                        <label>Spesifikasi Khusus</label>
                            <textarea class="form-control blank" name="spesifikasi" id="spesifikasi" rows="6"></textarea>
                    </div>
                    <div id="pemenuhan">
                        <div class="form-group">
                            <label>Jenis</label>
                                <select name="jenis" id="jenis" class="form-control">
                                    <option value="baru">Baru</option>
                                    <option value="promosi">Promosi</option>
                                    <option value="mutasi">Mutasi</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Pemenuhan</label>
                                <input type="text" class="form-control blank date-picker" data-date-format="dd/mm/yyyy" name="tgl_pemenuhan" id="tgl_pemenuhan" />
                        </div>
                        <div class="form-group">
                            <label>Atas Nama</label>
                                <?php echo form_dropdown('nip', array(), '', 'id="nip" class="select2 form-control" style="width:100%;"'); ?>
                        </div>
                    </div>
                    <div id="append_pemenuhan"></div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
