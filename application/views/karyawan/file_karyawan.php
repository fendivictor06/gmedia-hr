<div class="page-content">
    <div class="breadcrumbs"><h1>File Karyawan</h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="form-group">
                    <label>Pilih file</label>
                    <div class="form-group input-group">
                        <?php echo form_dropdown('file', $list, '', 'id="file" class="select2" style="width:100%"'); ?>
                        <span class="input-group-btn">
                            <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                            <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                        </span>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".select2").select2();
    });

    function load_table() {
        list = $("#file").val();
        $.ajax({
            url : "view_file/"+list,
            beforeSend : function() {
                $("#imgload").removeClass("hidden"), $("#tampil").addClass("hidden");
            },
            complete : function() {
                $("#imgload").addClass("hidden"), $("#tampil").removeClass("hidden");
            },
            success : function(data) {
                $("#myTable").html(data);
                $("#tbl_file").DataTable({
                    responsive : true
                })
            }
        });
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>