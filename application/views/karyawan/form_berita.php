<div class="page-content">
    <div class="breadcrumbs">
        <h1>Berita</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="form-group">
                    <button type="button" class="btn btn-primary"  id="add_new"> <i class="fa fa-plus"></i> Tambah Data</button>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Berita</h4>
    </div>
    <form id="form_berita">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>Judul</label>
                            <input type="text" name="judul" id="judul" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" name="tgl" id="tgl" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group">
                        <label>Gambar</label>
                            <input type="file" name="gambar" id="gambar" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Pesan</label>
                            <textarea class="form-control" name="keterangan" id="keterangan" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" id="close" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    $(document).ready(function(){
        load_table();
        $(".date-picker").datepicker({
            "setDate": new Date(),
            "autoclose": true
        });
    });

    function clearform() {
        document.getElementById('form_berita').reset();
        $("#id").val('');
    }

    $("#add_new").click(function(){
        clearform(), $("#myModal").modal();
    });

    function load_table() {
        date = $("#tgl").val();
        $.ajax({
            url : "<?php echo base_url('berita/view_berita'); ?>",
            success : function(data) {
                $("#myTable").html(data);
                $("#tb_berita").DataTable({
                    responsive : true
                });
            }
        });
    }

    $("#form_berita").submit(function(event){
        event.preventDefault();
        formData = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('berita/process_berita'); ?>",
            type : "post",
            data : formData,
            async : false,
            cache : false,
            dataType : "json",
            contentType : false,
            processData : false,
            success : function(data) {
                if(data.status == true) {
                    load_table(), clearform();
                    $("#myModal").modal("toggle");
                    toastr.success(data.message);
                } else {
                    toastr.warning(data.message);
                }
            }
        });
        return false;
    });

    function get_id(id) {
        $.ajax({
            url : "<?php echo base_url('berita/berita_id'); ?>/"+id,
            dataType : 'json',
            success : function(data) {
                $("#id").val(data.id);
                $("#judul").val(data.judul);
                $("#tgl").val(data.tgl);
                $("#keterangan").val(data.text);
                $("#myModal").modal();
            }
        });
    }

    function delete_data(id) {
        bootbox.dialog({
            message : "Yakin ingin menghapus data?",
            title : "Hapus Data",
            buttons :{
                danger : {
                    label : "Delete",
                    className : "red",
                    callback : function(){
                        $.ajax({
                            url : "<?php echo base_url('berita/delete_berita'); ?>/"+id,
                            dataType : "json",
                            success : function(data){
                                if(data.status == true) {
                                    load_table();
                                }
                                bootbox.alert(data.message);
                            }
                        });
                    }
                },
                main : {
                    label : "Cancel",
                    className : "blue",
                    callback : function(){
                        return true;
                    }
                }
            }
        });
    }
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>