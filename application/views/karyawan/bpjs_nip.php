<div class="page-content">
	<div class="breadcrumbs">
		<h1>BPJS Karyawan</h1>
	</div>
	<div class="row">

            <div class="portlet light">
                <div class="portlet-body">
                    <table>
                        <tr>
                            <td>NIP</td>
                            <td width="20px"> : </td>
                            <td><?php echo isset($kary->nip)?$kary->nip:''; ?></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td> : </td>
                            <td><?php echo isset($kary->nama)?$kary->nama:''; ?></td>
                        </tr>
                    </table>
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <div class="btn-group">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#tab_1_1" data-toggle="tab"> BPJS TK </a>
                        </li>
                        <li>
                            <a href="#tab_1_2" data-toggle="tab"> BPJS KS </a>
                        </li> 
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1">  
                            <?php echo $bpjs_tk; ?>
                        </div>
                        <div class="tab-pane fade" id="tab_1_2">
                            <?php echo $bpjs_ks; ?>
                        </div>
                    </div>
                    <div class="clearfix margin-bottom-20"> </div>
            </div>

    </div>

</div>
