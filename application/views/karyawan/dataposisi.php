<div class="page-content">
    <div class="breadcrumbs">
        <h1>Posisi Kerja</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Data
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Posisi Kerja</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="form_posisi">
                    <input type="hidden" name="id_pos" id="id_pos" class="blank">

                    

                	<!-- <div class="form-group">
                        <label>Staff/Non Staff</label>
                        <select class="form-control" id="sns" name="sns" onchange="mySelect(this.value<?php echo $this->uri->segment(3); ?>);">
                        	<option value="NON-STAFF">Non Staff</option>
                        	<option value="STAFF">Staff</option>
                        </select>
                    </div> -->

                    
                    
                    <div class="form-group">
                        <label>Cabang</label>
                            <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Divisi</label>
                            <?php echo form_dropdown('divisi', $divisi, '', 'id="divisi" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Departemen</label>
                            <?php echo form_dropdown('departemen', $departemen, '', 'id="departemen" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Posisi</label>
                        <!-- <input type="text" class="form-control blank" name="jab" id="jab"> -->
                        <?php echo form_dropdown('jab', $jab, '', 'id="jab" class="select2"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Jabatan</label>
                        <?php echo form_dropdown('mySelect', $sal_pos, '', 'id="mySelect" class="select2"'); ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
