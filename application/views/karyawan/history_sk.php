<div class="page-content">
    <div class="breadcrumbs">
        <h1>History SK</h1>
    </div>
    <div class="row">
        
            <div class="portlet light">
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                <form id="laporan">
                                    <div class="form-group">
                                        <label>Tanggal Mulai <span class="required">*</span></label>
                                            <input type="text" class="form-control date-picker" name="date_start" id="date_start" data-date-format="dd/mm/yyyy" />
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Selesai <span class="required">*</span></label>
                                            <input type="text" class="form-control date-picker" name="date_end" id="date_end" data-date-format="dd/mm/yyyy" />
                                    </div>
                                </form>
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary" id="download">
                                    <i class="fa fa-download"></i> Download
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myTable"></div>
            </div>

    </div>
</div>