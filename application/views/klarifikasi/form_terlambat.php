<div class="page-content">
    <div class="breadcrumbs">
        <h1>Klarifikasi Terlambat</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <!-- <div class="table-toolbar">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                            <label>Periode</label>
                                <div class="form-group input-group">
                                    <?php echo form_dropdown('periode',$periode,'','id="periode" class="form-control"'); ?>
                                    <span class="input-group-btn">
                                        <button id="tampil" class="btn btn-primary" type="button" onclick="load_table();"><i class="fa fa-search"></i> Cari</button>
                                        <img id="imgload" src="<?php echo base_url('assets/img/loading.gif'); ?>" class="hidden">
                                    </span> 
                                </div>
                        </div>
                    </div>
                </div> -->
                <?php echo form_periode(); ?> <br>
            </div>
                <div id="myTable"></div>
        </div>
    </div>
</div>



<div id="klarifikasi_modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title" id="detailtitle">Klarifikasi Terlambat</h4>
    </div>
    <form id="form_klarifikasi">
        <div class="modal-body">
            <div class="form-group">
                <label>Nama</label>
                <input type="hidden" name="id_terlambat" id="id_terlambat">
                <input type="hidden" name="id_ab_t" id="id_ab_t">
                <input type="text" name="nama_t" id="nama_t" class="form-control" readonly="readonly">
            </div>
            <div class="form-group">
                <label>Tanggal</label>
                <input type="text" name="tgl_t" id="tgl_t" class="form-control" readonly="readonly">
            </div>
            <div class="form-group">
                <label>Scan Masuk</label>
                <input type="text" name="scan_masuk_t" id="scan_masuk_t" class="form-control timepicker timepicker-24" autocomplete="off"> 
            </div>
            <div class="form-group">
                <label>Uang Makan</label>
                <select class="form-control" name="makan" id="makan">
                    <option value="1">Ya</option>
                    <option value="0">Tidak</option>
                </select>
            </div>
            <div class="form-group">
                <label>File Upload</label>
                <input type="file" name="file_upload_t" id="file_upload_t" class="form-control">
            </div>
            <div class="form-group">
                <label>Klarifikasi</label>
                    <textarea class="form-control" name="klarifikasi_t" id="klarifikasi_t" rows="5">Tugas Kantor</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save_button" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
     <?php  
        $month = date('m');
        // $month = str_replace('0', '', $month);
        $month = (int)$month;
        $year = date('Y');
    ?>

    function load_table() {
        // periode = $("#periode").val();
        $.ajax({
            url : "<?php echo base_url('klarifikasi/view_klarifikasi_terlambat'); ?>",
            data : {
                "tahun" : $("#tahun").val(),
                "bulan" : $("#bulan").val()
            },
            beforeSend : function() {
                $("#tampil").addClass("hidden"), $("#imgload").removeClass("hidden");
            },
            complete : function(){
                $("#tampil").removeClass("hidden"), $("#imgload").addClass("hidden");
            },
            success : function(data){
                $("#myTable").html(data);
                $("#terlambat").DataTable({
                    stateSave : true,
                    responsive : true
                });
            }
        });
    }

    $(document).ready(function(){
        $('.select2').select2();
        var year = "<?php echo $year; ?>";
        var month = "<?php echo $month; ?>";
        
        $("#tahun").val(year).trigger("change");
        $("#bulan").val(month).trigger("change");
        $(".select2").select2();
        load_table();
    });

    $("#tampil").click(function(){
        load_table();
    });

    function get_terlambat(id) {
        $.ajax({
            url : "<?php echo base_url('terlambat/get_id_terlambat'); ?>/"+id,
            dataType : "json",
            success : function(data) {
                $("#id_terlambat").val(data.id), $("#id_ab_t").val(data.id_ab), $("#nama_t").val(data.nama), $("#tgl_t").val(data.tanggal), $("#scan_masuk_t").val(data.masuk), $("#klarifikasi_modal").modal();
            }
        });
    }

    // submit terlambat 
    $("#form_klarifikasi").submit(function(event){
        event.preventDefault();
        var d = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('terlambat/klarifikasi'); ?>",
            type : "post",
            data : d,
            dataType : "json",
            async : false,
            chache : false,
            contentType : false,
            processData : false,
            beforeSend : function() {
                $("#save_button").attr('disabled', true);
            },
            complete : function() {
                $("#save_button").attr('disabled', false);
            },
            success : function(data) {
                if(data.status == true) {
                    load_table(), $("#klarifikasi_modal").modal("toggle");
                    toastr.success("", data.message);
                } else {
                    toastr.warning("", data.message);
                }
                // bootbox.alert(data.message);
            }
        });
        return false;
    });

    <?php echo $this->Main_Model->timepicker(); ?>
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>