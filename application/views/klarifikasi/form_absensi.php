<div class="page-content">
    <div class="breadcrumbs">
        <h1>Klarifikasi Absensi</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                            <?php echo form_periode(); ?>
                        </div>
                    </div>
                </div>
            </div>
                <div id="myTable"></div>
        </div>
    </div>
</div>



<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Klarifkasi Absensi</h4>
    </div>
    <form id="form_presensi">
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="id_ab" id="id_ab">
                    <input type="hidden" name="nip" id="nip">
                    <div class="form-group">
                        <label>Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama" readonly>
                    </div>
                    <div class="form-group">
                        <label>Tanggal</label>
                            <input type="text" class="form-control" readonly="readonly" data-date-format="dd/mm/yyyy" name="tgl" id="tgl">
                    </div>
                    <div class="form-group">
                        <label>Presensi</label>
                            <?php echo form_dropdown('absen', $absen, '', 'class="form-control" id="absen"'); ?>
                    </div>
                    <div id="kuota_cuti"></div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Jenis Cuti Khusus</label>
                            <?php echo form_dropdown('cuti_khusus', $tipe, '', 'id="cuti_khusus" class="form-control"'); ?>
                    </div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Awal Cuti Khusus</label>
                            <input type="text" name="awal_cuti_khusus" id="awal_cuti_khusus" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group cuti-khusus hidden">
                        <label>Akhir Cuti Khusus</label>
                            <input type="text" name="akhir_cuti_khusus" id="akhir_cuti_khusus" class="form-control date-picker" data-date-format="dd/mm/yyyy">
                    </div>
                    <div class="form-group masuk hidden">
                        <label>Scan Masuk</label>
                            <!-- <input type="text" name="scan_masuk" id="scan_masuk" class="form-control timepicker timepicker-24"> -->
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" class="form-control" id="scan_masuk" name="scan_masuk" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                    </div>
                    <div class="form-group masuk hidden">
                        <label>Scan Pulang</label>
                            <!-- <input type="text" name="scan_pulang" id="scan_pulang" class="form-control timepicker timepicker-24"> -->
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" class="form-control" id="scan_pulang" name="scan_pulang" autocomplete="off">
                                <span class="input-group-btn">
                                    <button class="btn default date-set" type="button">
                                        <i class="fa fa-calendar"></i>
                                    </button>
                                </span>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                            <textarea class="form-control" name="ket" id="ket" rows="5" readonly="readonly"></textarea>
                    </div>
                    <div class="form-group">
                        <label>File Upload</label>
                            <input type="file" name="file_upload" id="file_upload" class="form-control">
                    </div>
                    <div class="form-group klarifikasi">
                        <label>Klarifikasi</label>
                            <textarea class="form-control" name="klarifikasi" id="klarifikasi" rows="5"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save_button" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div> 

<?php echo isset($footer) ? $footer : ''; ?>
<script type="text/javascript">
    <?php  
        $month = date('m');
        $month = str_replace('0', '', $month);
        $year = date('Y');
    ?>

    function load_table() {
        var tahun = $("#tahun").val();
        var bulan = $("#bulan").val();
        $.ajax({
            url : "<?php echo base_url('klarifikasi/view_klarifikasi_absensi'); ?>",
            data : {
                'tahun' : tahun,
                'bulan' : bulan
            },
            beforeSend : function() {
                $("#tampil").addClass("hidden"), $("#imgload").removeClass("hidden");
            },
            complete : function(){
                $("#tampil").removeClass("hidden"), $("#imgload").addClass("hidden");
            },
            success : function(data){
                $("#myTable").html(data);
                $("#mangkir").DataTable({
                    stateSave : true,
                    responsive : true
                });
            }
        });
    }

    $(document).ready(function(){
        $('.select2').select2();
        var year = "<?php echo $year; ?>";
        var month = "<?php echo $month; ?>";
        
        $("#tahun").val(year).trigger("change");
        $("#bulan").val(month).trigger("change");
        
        $(".form_datetime").datetimepicker({
            autoclose : !0,
            format : "dd/mm/yyyy hh:ii",
            pickerPosition : "bottom-left"
        });

        $(".date-picker").datepicker({
            rtl: App.isRTL(), 
            orientation: "left", 
            autoclose: true
        });

        load_table();
    });

    $("#tampil").click(function(){
        load_table();
    });

    // view kuota cuti
    $("#absen").change(function(){
        value = $(this).val();
        if (value == 'cuti') {
            nip = $("#nip").val();
            $.ajax({
                url : "<?php echo base_url('cuti/cek_qt'); ?>/"+nip,
                success : function(data) {
                    $("#kuota_cuti").html(data);
                }
            });
            $(".masuk").addClass('hidden');
            $(".cuti-khusus").addClass('hidden');
            $(".klarifikasi").removeClass('hidden');
        } else if (value == 'cuti_khusus') {
            $(".masuk").addClass('hidden');
            $("#kuota_cuti").html('');
            $(".cuti-khusus").removeClass('hidden');
            $(".klarifikasi").addClass('hidden');
        } else if (value == 'masuk') {
            $(".masuk").removeClass('hidden');
            $("#kuota_cuti").html('');
            $(".cuti-khusus").addClass('hidden');
            $(".klarifikasi").removeClass('hidden');
        } else {
            $(".masuk").addClass('hidden');
            $("#kuota_cuti").html('');
            $(".cuti-khusus").addClass('hidden');
            $(".klarifikasi").removeClass('hidden');
        }
    });

    // show modal klarifikasi absensi
    function get_id(id){
        $.ajax({
            url : "<?php echo base_url('absensi/presensi_id'); ?>",
            type : "GET",
            data : {"id" : id },
            dataType : "JSON",
            success : function(dat){
                var jam_masuk = (dat.jam_masuk == '00/00/0000 00:00') ? '' : dat.jam_masuk;
                var jam_pulang = (dat.jam_pulang == '00/00/0000 00:00') ? '' : dat.jam_pulang;
                $("#myModal").modal(), 
                $("#nama").val(dat.nama), 
                $("#id").val(dat.id), 
                $("#nip").val(dat.nip), 
                $("#tgl").val(dat.tanggal), 
                $("#klarifikasi").val(dat.klarifikasi), 
                $("#ket").val(dat.ket), 
                $("#scan_masuk").val(jam_masuk), 
                $("#scan_pulang").val(jam_pulang), 
                $("#id_ab").val(dat.id_ab),
                $("#nip").val(dat.nip);
                $("#nama").val(dat.nama), 
                $("#kuota_cuti").html("");

                (dat.sakit == 1) ? $("#absen").val("sakit") : ((dat.ijin == 1) ? $("#absen").val("ijin") : ((dat.mangkir == 1) ? $("#absen").val("mangkir") : $("#absen").val("")));
            }
        });
    }

    // submit presensi
    $("#form_presensi").submit(function(event){
        event.preventDefault();
        var d = new FormData($(this)[0]);
        $.ajax({
            url : "<?php echo base_url('absensi/presensi_process'); ?>",
            type : "post",
            data : d,
            dataType : "json",
            async : false,
            cache : false,
            contentType : false,
            processData : false,
            beforeSend : function() {
                $("#save_button").attr('disabled', true);
            },
            complete : function() {
                $("#save_button").attr('disabled', false);
            },
            success : function(data) {
                if(data.status == true) {
                    load_table(), $("#myModal").modal("toggle");
                    toastr.success("", data.message);
                    document.getElementById("form_presensi").reset();
                } else {
                    toastr.warning("", data.message);
                }
                // bootbox.alert(data.message);
            }
        });
        return false;
    });

    <?php echo $this->Main_Model->timepicker(); ?>
</script>
<?php echo isset($penutup) ? $penutup : ''; ?>