<div class="page-content">
	<div class="breadcrumbs">
		<h1>BBM & BBP</h1>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="portlet light">
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-plus"></i> Tambah BBM/BBP
                            </button>
                        </div>
                    </div>
                </div>
                <div id="myTable"></div>
            </div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form BBM&BBP</h4>
    </div>
    <div class="modal-body">
        <div id="notif"></div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="msbbmp">
                    <input type="hidden" name="id" id="id" class="kosong">
                    <div class="form-group">
                        <label>Tipe</label>
                            <select class="form-control kosong" id="tipe" name="tipe">
                                <option value="bbm">BBM</option>
                                <option value="bbp">BBP</option>
                            </select>
                    </div>
                    <div class="form-group">
                        <label>Jabatan</label>
                            <?php echo form_dropdown('jab_bbmp',$opt_bbmp,'','id="jab_bbmp" class="select2 kosong"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Nominal</label>
                            <input type="text" class="form-control kosong" name="nominal" id="nominal">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>