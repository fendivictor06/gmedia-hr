<div class="page-content">
	<div class="breadcrumbs">
		<h1>Faktor Kemahalan</h1>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="portlet-body">
				<div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Faktor Kemahalan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
			<div id="myTable"></div>
		</div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Faktor Kemahalan</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
         
                <div class="form-group">
                    <label>Tahun</label>
                        <select name="th" id="th" class="form-control">
                    		<?php for ($i=2010; $i <= 2050; $i++) { 
                    				echo '<option value="'.$i.'">'.$i.'</option>';
                    		} ?>
                    	</select>
                </div>
            </div>
                <?php 
                	foreach ($lku as $row) {
                		echo '	<div class="col-md-12">	
	                				<div class="form-group">
	                					<label>Lokasi</label>
	                						<input type="hidden" id="id_lku'.$row->id_lku.'" value="'.$row->id_lku.'">
	                						<input type="text" id="lku'.$row->id_lku.'" value="'.$row->lku.'" class="form-control" readonly>
	                				</div>
	                			</div>
	                			<div class="col-md-12">	
	                				<div class="form-group">
	                					<label>UMK</label>
	                						<input type="number" id="umk'.$row->id_lku.'" class="form-control">
	                				</div>
	                			</div>
	                			';	
                	}
                 ?>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

<div id="editModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
	<div class="modal-header">
		<h4 class="modal-title">Form Hari Libur</h4>
	</div>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<input type="hidden" name="id_fk" id="id_fk">
				<div class="form-group">
                    <label>Tahun</label>
                        <select name="e_th" id="e_th" class="form-control">
                    		<?php for ($i=2010; $i <= 2050; $i++) { 
                    				echo '<option value="'.$i.'">'.$i.'</option>';
                    		} ?>
                    	</select>
                </div>
				<div class="form-group">
					<label>Lokasi</label>
						<input type="hidden" class="form-control" name="e_lku" id="e_lku">
						<input type="text" class="form-control" id="lok">
				</div>
				<div class="form-group">
					<label>UMK</label>
						<input type="text" name="e_umk" id="e_umk" class="form-control">
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="update();" class="btn btn-primary">Simpan</button>
	</div>
</div>
