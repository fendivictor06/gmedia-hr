<div class="page-content">
	<div class="breadcrumbs">
		<h1>BPJS Ketenagakerjaan</h1>
	</div>
	<div class="row">
		<div class="portlet light">
            <div class="col-md-6" style="padding-left: 0px; padding-bottom: 30px;">
                <form class="form-inline">
                    <div class="form-group">
                        <?php echo form_dropdown('cabang', $cabang, '', 'id="cabang" class="form-control"'); ?>
                        <a href="javascript:;" id="unduh" class="btn btn-primary">
                            <i class="fa fa-download"></i>
                            Download
                        </a>
                    </div>
                </form>
            </div>
            
            <div id="bpjs"></div>
            <!-- <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Aturan BPJS TK </a>
                </li>
                <li>
                    <a href="#tab_1_2" data-toggle="tab"> Tunjangan BPJS TK </a>
                </li> 
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_1_1"> 
                    <div id="bpjs"></div>
                </div>
                <div class="tab-pane fade" id="tab_1_2">
                    <div id="umk"></div>
                </div>
            </div> -->
        </div>
            <!-- <div class="clearfix margin-bottom-20"> </div> -->
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Aturan BPJS</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <form id="formbpjs">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label>Tahun</label>
                        	<select name="th" id="th" class="form-control">
                        		<?php for ($i=2010; $i <= 2050; $i++) { 
                        				echo '<option value="'.$i.'">'.$i.'</option>';
                        		} ?>
                        	</select>
                    </div>
                    <div class="form-group">
                        <label>JKK</label>
                     		<input type="text" class="form-control" name="jkk" id="jkk">
                    </div>
                    <div class="form-group">
                        <label>JKM</label>
                        	<input type="text" class="form-control" name="jkm" id="jkm">
                    </div>
                    <div class="form-group">
                        <label>JHTTK</label>
                        	<input type="text" class="form-control" name="jhttk" id="jhttk">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>JHTP</label>
                        	<input type="text" class="form-control" name="jhtp" id="jhtp">
                    </div>
                    <div class="form-group">
                        <label>JPTK</label>
                        	<input type="text" class="form-control" name="jptk" id="jptk">
                    </div>
                    <div class="form-group">
                        <label>JPP</label>
                        	<input type="text" class="form-control" name="jpp" id="jpp">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>