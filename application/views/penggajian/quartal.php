<div class="page-content">
    <div class="breadcrumbs">
        <h1>Quartal</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Quartal
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            	<div id="myTable"></div>             
		</div>
    </div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Quartal</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Quartal</label>
                        <input type="hidden" name="id" id="id">
                        <select class="form-control" id="q" name="q">
                            <option value="Q1">Q1</option>
                            <option value="Q2">Q2</option>
                            <option value="Q3">Q3</option>
                            <option value="Q4">Q4</option>
                        </select> 
                </div>
                <div class="form-group">
                    <label>Tahun</label>
                        <select class="select2" id="thn" name="thn">
                            <?php for($i='2010';$i<='2050';$i++){echo '<option value="'.$i.'">'.$i.'</option>';} ?>
                        </select>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                        <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
