<div class="page-content">
	<div class="breadcrumbs">
		<h1>BPJS Kesehatan</h1>
	</div>
	<div class="row">
		<div class="portlet light">
            <div class="portlet-body">    
            </div>
                <div id="myTable"></div>
        </div>
	</div>
</div>


<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form BPJS KS</h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger hidden" id="warning">Please Complete this form!</div>
        <div class="alert alert-success hidden" id="success">Success!</div>
        <div class="row">
            <form id="form_bpjs">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" name="id" id="id" class="kosong">
                <input type="hidden" name="nip" id="nip" class="kosong">
                <div class="form-group">
                    <label>NO KK</label>
                        <select name="kelas" id="kelas" class="form-control kosong">
                            <option value="1">Kelas 1</option>
                            <option value="2">Kelas 2</option>
                        </select>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>