<div class="page-content">
    <div class="breadcrumbs"><h1>Non Staff Rule <?php echo $header->row()->bu; ?></h1></div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Rule
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Rules Baru</h4>
    </div>
    <div class="modal-body">
        <form id="nonstaffrule">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <input type="hidden" name="id_nsc" id="id_nsc" class="kosong">
                    <div class="form-group">
                        <label>Posisi Salary</label>
                        <input type="text" class="form-control kosong" name="sal_pos" id="sal_pos">
                    </div>
                    <div class="form-group">
                        <label>Performance</label>
                        <input type="text" class="form-control kosong" name="perf" id="perf">
                    </div>
                    <div class="form-group">
                        <label>Operator</label>
                        <?php echo form_dropdown('operator', $operator, '', 'class="form-control kosong" id="operator"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Nilai 1</label>
                        <input type="text" class="form-control kosong" name="val1" id="val1">
                    </div>
                    <div class="form-group">
                        <label>Nilai 2</label>
                        <input type="text" class="form-control kosong" name="val2" id="val2">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Gaji Dasar</label>
                        <input type="text" class="form-control kosong" name="gd" id="gd">
                    </div>
                    <div class="form-group">
                        <label>TOPS</label>
                        <input type="text" class="form-control kosong" name="tops" id="tops">
                    </div>
                    <div class="form-group">
                        <label>Bonus Bulanan</label>
                        <input type="text" class="form-control kosong" name="bb" id="bb">
                    </div>
                    <div class="form-group">
                        <label>Bonus Triwulan</label>
                        <input type="text" class="form-control kosong" name="btw" id="btw">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>
