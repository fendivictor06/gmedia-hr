<div class="page-content">
    <div class="breadcrumbs">
        <h1>Download Laporan BPJS</h1>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light">
                <div class="portlet-body">
                        <div class="form-group">
                            <label>BPJS</label>
                                <select class="form-control" name="tipe" id="tipe">
                                    <option value="tk">BPJS TK</option>
                                    <option value="ks">BPJS KS</option>
                                </select>
                        </div>
                        <!-- <div class="form-group">
                            <label>Periode</label>
                                <?php echo form_dropdown('periode', $periode, '', 'id="periode" class="form-control"'); ?>
                        </div> -->
                        <?php echo form_periode(1); ?>
                        <button type="button" class="btn btn-primary" id="unduh"><i class="fa fa-download"></i> Download</button>
                </div>
            </div>
        </div>
    </div>
</div>