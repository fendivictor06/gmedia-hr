<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data Jabatan</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary" onclick="reset();" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> Tambah Jabatan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="myTable"></div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Jabatan</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <form id="staffrule">
                <div class="col-md-12">
                    <input type="hidden" name="id_ssc" id="id_ssc" class="kosong">
                    <div class="form-group">
                        <label>Jabatan</label>
                        <input type="text" class="form-control kosong" name="sal_pos" id="sal_pos">
                    </div>
                    <!-- <div class="form-group">
                        <label>Band*</label>
                        <?php //echo form_dropdown('id_band', $band, isset($default['id_band'])?$default['id_band']:'', 'class="form-control kosong" id="id_band"'); ?>
                    </div>
                    <div class="form-group">
                        <label>Tunjangan Dasar(tanpa%)*</label>
                        <input type="text" class="form-control kosong" name="td" id="td">
                    </div> -->
                
                    <!-- <div class="form-group">
                        <label>Tunjangan Jabatan</label>
                        <input type="text" class="form-control kosong" name="tpj" id="tpj">
                    </div>
                    <div class="form-group">
                        <label>Tunjangan Komunikasi</label>
                        <input type="text" class="form-control kosong" name="tk" id="tk">
                    </div> -->
                </div>
            </form>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>

