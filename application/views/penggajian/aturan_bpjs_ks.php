<div class="page-content">
	<div class="breadcrumbs">
		<h1>Aturan BPJS KS</h1>
	</div>
	<div class="row">
		<div class="portlet light">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Aturan BPJS Kesehatan </a>
                </li>
                <li>
                    <a href="#tab_1_2" data-toggle="tab"> Tunjangan BPJS Kesehatan </a>
                </li> 
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_1_1"> 
                    <div id="bpjs"></div>
                </div>
                <div class="tab-pane fade" id="tab_1_2">
                    <div id="umk"></div>
                </div>
            </div>
        </div>
            <div class="clearfix margin-bottom-20"> </div>
        </div>
	</div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form Aturan BPJS</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form id="bpjsks">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label>Tahun</label>
                        	<select name="th" id="th" class="form-control">
                        		<?php for ($i=2010; $i <= 2050; $i++) { 
                        				echo '<option value="'.$i.'">'.$i.'</option>';
                        		} ?>
                        	</select>
                    </div>
                    <div class="form-group">
                        <label>TKS</label>
                     		<input type="text" class="form-control" name="tks" id="tks">
                    </div>
                    <div class="form-group">
                        <label>PKS</label>
                        	<input type="text" class="form-control" name="pks" id="pks">
                    </div>
                    <div class="form-group">
                        <label>Min Kelas 1</label>
                        	<input type="text" class="form-control" name="kelas" id="kelas">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
        <button type="button" onclick="save();" class="btn btn-primary">Simpan</button>
    </div>
</div>