<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo company(); ?> HRD Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="theme-color" content="#364150">
        <link rel="icon" href="<?php echo base_url('assets/img/ikon.png') ?>" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="<?php echo base_url('assets/plugins/simple-line-icons/simple-line-icons.min.css'); ?>" rel="stylesheet" type="text/css" /> -->
        <link href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- <link href="<?php echo base_url('assets/plugins/bootstrap-switch/css/bootstrap-switch.min.css'); ?>" rel="stylesheet" type="text/css" /> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/select2/css/select2.min.css'); ?>"> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/select2/css/select2-bootstrap.min.css'); ?>"> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'); ?>"> -->
        <link href="<?php echo base_url('assets/css/components-md.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url('assets/css/plugins-md.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/login.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
    </head>
    <body class=" login">
        <div class="logo">
            <a href="index.html">
                <img src="<?php echo base_url('assets/img/logo-big.png'); ?>" alt="" /> </a>
        </div>
        <div class="content">
            <form id="formlogin" action="<?php echo base_url('main/login'); ?>" method="post" class="login-form">
                <h3 class="form-title font-green">Sign In</h3>
                <div id="notification"></div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" id="username" required /> 
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password" required /> 
                </div>
                <div class="form-actions">
                    <button type="submit" id="login" class="btn green uppercase">Login</button>
                </div>
                <div class="create-account">
                    <p style="height: 30px;">
                        <!-- <a href="#" id="register-btn" data-toggle="modal" data-target="#myModal" class="uppercase" onclick="reset()">Create an account</a> -->
                    </p>
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
            
        <div class="copyright"> <?php echo date("Y"); ?> © <?php echo company(); ?> HRD. </div>
        <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
        </script>
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- <script src="<?php echo base_url('assets/plugins/select2/js/select2.full.min.js'); ?>"></script> -->
        <!-- <script src="<?php echo base_url('assets/plugins/bootbox/bootbox.min.js'); ?>"></script> -->
        <!-- <script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>"></script> -->
        <!--<script src="<?php echo base_url('assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') ?>"></script>
            <script src="<?php echo base_url('assets/plugins/bootstrap-modal/js/bootstrap-modal.js') ?>"></script>-->
        <!-- END CORE PLUGINS -->
        <script src="<?php echo base_url('app.js') ?>"></script>
        <script type="text/javascript">
            if ('serviceWorker' in navigator) {
                navigator.serviceWorker
                     .register(base_url+'service-worker.js')
                     .then(function() { console.log('Service Worker Registered'); });
            }
        </script>
    </body>
</html>