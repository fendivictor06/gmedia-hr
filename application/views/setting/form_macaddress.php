<div class="page-content">
    <div class="breadcrumbs">
        <h1>Data IP & Mac Address</h1>
    </div>
    <div class="row">
        <div class="portlet light">
            <div class="portlet-body">
                <div class="form-group"> 
                    <button type="button" class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Tambah Data</button> 
                </div>
                <div id="myTable"></div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false" data-attention-animation="false">
    <div class="modal-header">
        <h4 class="modal-title">Form IP & Mac Address</h4>
    </div>
    <form id="form_data">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" >
                    <div class="form-group">
                        <label>SSID</label>
                            <input type="text" name="ssid" id="ssid" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Mac Address</label>
                            <input type="text" name="mac" id="mac" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>IP Public</label>
                            <input type="text" name="ip" id="ip" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
            <button type="submit" id="save" class="btn btn-primary">Simpan</button>
        </div>
    </form>
</div>

<?php echo isset($footer) ? $footer : ''; ?>
    <script type="text/javascript">
        var base_url = "<?php echo isset($base_url) ? $base_url : ''; ?>"
    </script>
    <script type="text/javascript" src="<?php echo base_url('app.js'); ?>"></script>
<?php echo isset($penutup) ? $penutup : ''; ?>