<div class="page-content">
	<div class="breadcrumbs">
		<h1>Register Akun</h1>
	</div>
	<div class="row">

		<form class="register-form" action="#" method="post">
			<div class="col-md-12 col-xs-12 col-sm-12">
            	<p id="notif"></p>
            </div>
           	<div class="col-md-6 col-xs-12 col-sm-12">
           		<input type="hidden" name="id" id="id">
	            <div class="form-group">
	                <label class="control-label visible-ie8 visible-ie9">Karyawan</label>
	                <?php echo form_dropdown('nip',$kary,'','id="nip" class="select2" style="width:100%"'); ?>
	            </div>
	            <div class="form-group">
	                <label class="control-label visible-ie8 visible-ie9">Username</label>
	                <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" id="uname" name="uname"  /> </div>
            </div>
            <div class="col-md-6 col-xs-12 col-sm-12">
	            <div class="form-group">
	                <label class="control-label visible-ie8 visible-ie9">Password</label>
	                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="pwd" placeholder="Password" name="pwd"  /> </div>
	            <div class="form-group">
	                <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
	                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="rpassword" id="rpassword"  /> </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12">
	            <div class="form-actions">
	                <button type="button" id="register-submit-btn" class="btn btn-success uppercase" onclick="register();">Submit</button>
	            </div>
        	</div>
        </form>
			

	</div>
	<br><br>
	<div id="myTable"></div>
</div>