<div class="page-content">
	<div class="breadcrumbs"><h1><?php echo isset($h1) ? $h1 : ''; ?></h1></div>
	<div class="row">
		<div class="portlet light">
			<div class="portlet-body">
				<div class="form-group">
					<button type="button" class="btn btn-primary" id="add"><i class="fa fa-plus"></i> Tambah Data</button>
				</div>
				<div id="myTable"></div>
			</div>
		</div>
	</div>
</div>

<?php echo isset($modal) ? $modal : ''; ?>