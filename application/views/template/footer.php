	<!-- BEGIN FOOTER -->
        <p class="copyright"><?php echo date("Y"); ?> © <?php echo company(); ?> App</p>
        <a href="#index" class="go2top">
            <i class="icon-arrow-up"></i>
        </a>
    <!-- END FOOTER -->
	</div>
</div>
    <!--[if lt IE 9]>
		<script src="<?php echo base_url('assets/js/respond.min.js'); ?>"></script>
		<script src="<?php echo base_url('assets/js/excanvas.min.js'); ?>"></script> 
	<![endif]-->

	<!-- Javascript Core -->
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/app.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/layout5/scripts/layout.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/ui-toastr.js') ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.blockui.min.js'); ?>"></script>
	<script type="text/javascript">
		var base_url = '<?php echo base_url(); ?>';
	</script>
	<?php echo isset($js)?$js:''; ?>
	<?php echo isset($javascript)?$javascript:''; ?>
	<?php echo $this->session->flashdata('message_upload'); ?>
		<?php 
			$javascript = '<script type="text/javascript">function ChangePT(id){
			$.ajax({
				url : "'.base_url('main/changept').'/"+id,
				success : function(data){
					location.reload();
				},
				error : function(jqXHR, textStatus, errorThrown){
					alert("Oops Something went wrong !");
				} 
			})
		}</script>';
			echo minifyjs($javascript); 
		?>
</body>
</html>