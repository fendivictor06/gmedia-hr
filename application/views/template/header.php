<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8 no-js">
    <![endif]-->
    <!--[if IE 9]> 
    <html lang="en" class="ie9 no-js">
        <![endif]-->
        <!--[if !IE]><!--> 
        <html lang="en">
            <!--<![endif]-->
            <head>
                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="Aplikasi HRD <?php echo company(); ?>">
                <title><?php echo company(); ?> HRD</title>
                <!-- Stylesheet Core -->
                <link rel="icon" href="<?php echo base_url('assets/img/ikon.png') ?>" />
                <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald:400,300,700">
                <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all">
                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.min.css'); ?>">
                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/simple-line-icons/simple-line-icons.min.css'); ?>">
                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap/css/bootstrap.min.css'); ?>">
                <link href="<?php echo base_url('assets/css/components-md.min.css'); ?>" rel="stylesheet" id="style_components" type="text/css" />
                <link href="<?php echo base_url('assets/css/plugins-md.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/layout5/css/layout.min.css'); ?>">
                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/bootstrap-toastr/toastr.min.css'); ?>">
    <?php echo isset($style)?$style:''; ?>
            </head>
            <body class="page-header-fixed page-sidebar-closed-hide-logo page-md">
                <!-- Begin Container -->
                <div class="wrapper">
                <!-- BEGIN HEADER -->
                <header class="page-header">
                    <nav class="navbar mega-menu" role="navigation">
                        <div class="container-fluid">
                            <div class="clearfix navbar-fixed-top">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </span>
                                </button>
                                <!-- End Toggle Button -->
                                <!-- BEGIN LOGO -->
                                <a id="index" class="page-logo" href="javascript:;" style="text-decoration:none;color:white;">
                                <?php echo '<h3>'.company().'</h3>'; ?>
                                </a>
                                <!-- END LOGO -->
                                <!-- BEGIN TOPBAR ACTIONS -->
                                <div class="topbar-actions">
                                    <!-- BEGIN USER PROFILE -->
                                    <div class="btn-group-img btn-group">
                                        <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span>Hi, <?php echo username(); ?></span>
                                        <img src="<?php echo base_url('assets/plugins/layout5/img/avatar1.png'); ?>" alt=""> </button>
                                        <ul class="dropdown-menu-v2" role="menu">
                                            <?php echo list_company(); ?>
                                            <li>
                                                <a href="<?php echo base_url('main/logout'); ?>">
                                                <i class="icon-key"></i> Log Out </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <!-- END USER PROFILE -->
                                </div>
                                <!-- END TOPBAR ACTIONS -->
                            </div>
                            <!-- BEGIN HEADER MENU -->
                            <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse ">
                                <?php echo isset($menu)?$menu:''; ?>
                            </div>
                            <!-- END HEADER MENU -->
                        </div>
                        <!--/container-->
                    </nav>
                </header>
                <div class="container-fluid">
                <!-- END HEADER -->