var siteurl = 'https://erpsmg.gmedia.id/hrd/';
var cacheStorage = 'pwa';
var fileToChace = [
	siteurl,
	siteurl+'assets/img/ikon.png',
	siteurl+'assets/plugins/bootstrap/css/bootstrap.min.css',
	siteurl+'assets/plugins/bootstrap/css/components-md.min.css',
	siteurl+'assets/plugins/bootstrap/css/plugins-md.min.css',
	siteurl+'assets/plugins/bootstrap/css/login.min.css',
	siteurl+'assets/js/jquery.min.js',
	siteurl+'assets/plugins/bootstrap/js/bootstrap.min.js'
];

self.addEventListener('install', function(e) {
	console.log('[ServiceWorker] Install');
	e.waitUntil(
		caches.open(cacheStorage).then(function(cache){
			console.log('[ServiceWorker] Caching App');
			console.log(fileToChace);
			return cache.addAll(fileToChace);
		})
	);
});

self.addEventListener('activate', function(event) {
	console.log('[ServiceWorker] activate');
	event.waitUntil(
		caches.keys().then(function(cacheNames){
			return Promise.all(
				cacheNames.filter(function(cacheName) {
					return cacheName != cacheStorage;
				}).map(function(cacheName){
					return caches.delete(cacheName);
				})
			);
		})
	);
});

self.addEventListener('fetch', function(event) {
	console.log('[ServiceWorker] fetch');
 	event.respondWith(
   		caches.match(event.request).then(function(response) {
     		return response || fetch(event.request);
   		})
 	);
});